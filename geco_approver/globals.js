/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"391E870F-7E8A-4692-A7E6-D5CA925BE5BD",variableType:-4}
 */
var treeviewDataSetTS;

/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"281E910D-7C55-40FC-8A95-3DCF6EF2A58D",variableType:-4}
 */
var treeviewDataSetEX;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"16679D5B-24BF-4DF3-98AD-4069FA4A9D72"}
 */
var myStyle = 'treeview {background-color: #FFFFFF; border-style: none; }';

/**
 * Callback method for when solution is opened.
 * @properties={typeid:24,uuid:"A0F86193-3DE2-4108-B17C-0006C2F55EB6"}
 */
function onSolutionOpenApprover() {
	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		application.putClientProperty(APP_WEB_PROPERTY.WEBCLIENT_TEMPLATES_DIR, 'custom');
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_DEFAULT_SCROLLABLE, true);
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_SCROLLABLE_KEEP_LOADED_ROWS, true);
	}
}

/**
 * @properties={typeid:24,uuid:"4B3436AD-C995-4A20-8269-E08962EF66E6"}
 */
function createDataSetTimesheet() {
	// create initial dataset createEmptyDataSet( 0,  ['id', 'pid', 'treeColumn', 'column1', 'column2', 'column3', 'icon', 'editable']);
	treeviewDataSetTS = databaseManager.createEmptyDataSet(0, ['id', 'pid', 'treeColumn', 'column1', 'column2', 'column3', 'column4', 'column5', 'column6','column7','column8','icon','editable']);
	//treeviewDataSetTS.addRow([null,	null,	'Nome',	'ORD', 'SSS', 'HVI', 'RORD', 'RFES', 'TRF', 'ASS','UID']);	// header labels
	  treeviewDataSetTS.addRow([null,	null,	'Nome',	'ORD', 'SSS', 'HVI', 'RGET', 'RFF', 'TRF', 'ASS','UID']);	// header labels
	var maxReturnedRows = 10000; //useful to limit number of rows
//	var server_name = databaseManager.getDataSourceServerName(controller.getDataSource());
	var sql_query = 'call sp_ts_approver_month (?, ? , ?, ?)';
	
	
	
	var arguments = new Array();
	arguments[0] = forms.approver_events_log_filter.selectedYear;
	arguments[1] = forms.approver_events_log_filter.selectedMonth; 
	arguments[2] = scopes.globals.currentUserId; //approverId or user ownerId o rdc o jo_rpc
	arguments[3] =  (forms.approver_events_log_filter.selectedJobOrder==null)?0:forms.approver_events_log_filter.selectedJobOrder; //job order id
	application.output(arguments);
	var dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, maxReturnedRows)
	var countRow = 0;
//	application.output(dataset);
	for (var index = 1; index <= dataset.getMaxRowIndex(); index++) {
		var riga = dataset.getRowAsArray(index);
		countRow++;
		var pid = countRow;
		var richieste = '';
		var work_count = '';
		var not_work_count = '';
		if(riga[10]!=null && riga[10]>0) richieste = '<b>&nbsp;&nbsp;&nbsp;*</b>';
		if (riga[11]!=null && riga[11]>0) work_count = '<b>&nbsp;&nbsp;&nbsp;*</b>';
		if (riga[12]!=null && riga[12]>0) not_work_count = '<b>&nbsp;&nbsp;&nbsp;*</b>';
		/** @type {Number} */
		var totOre= riga[4]+riga[5]+riga[6]+riga[8]+riga[10];
//		var totOre= riga[4]+riga[5]+riga[6]+riga[7]+riga[8]+riga[10];$	tolto il 7 che è rep. gett
		var totN = riga[7]+riga[9]; //tot numero eventi a conteggio
		treeviewDataSetTS.addRow([countRow, null, riga[1],work_count, '', '', '', '', '', not_work_count,riga[0],'1blank.png','false']);	// nome
		countRow++;
		treeviewDataSetTS.addRow([countRow, pid, 'ORE = '+ totOre + ' / N. = ' +  totN, riga[4], riga[5], riga[6], riga[7], riga[8], riga[9],riga[10],'','1blank.png','false']);	// ore
		
	}
	//application.output(treeviewDataSet);
	return treeviewDataSetTS;

}

/**
 * @properties={typeid:24,uuid:"7AB215AE-49B9-4DD1-816D-28F3F2596041"}
 */
function createDataSetExpenses() {
	
	treeviewDataSetEX = databaseManager.createEmptyDataSet(0, ['id', 'pid', 'treeColumn', 'column1', 'column2', 'column3', 'column4', 'column5', 'column6','column7','column8','column9','icon','editable']);
	treeviewDataSetEX.addRow([null,	null,	'Nome',	'RAPP', 'KFC', 'KNC', 'FATT', 'SFC', 'SNC', 'CARB','TNC','UID']);	// header labels
	var maxReturnedRows = 10000; //useful to limit number of rows
//	var server_name = databaseManager.getDataSourceServerName(controller.getDataSource());
	var sql_query = 'call sp_expenses_approver_month (?, ? , ?,?)';	
	
	var arguments = new Array();
	arguments[0] = forms.approver_expenses_log_filter.selectedYear;
	arguments[1] = forms.approver_expenses_log_filter.selectedMonth; 
	arguments[2] = scopes.globals.currentUserId; //approverId or user ownerId o rdc o jo_rpc
	arguments[3] =  (forms.approver_expenses_log_filter.selectedJobOrder==null)?0:forms.approver_expenses_log_filter.selectedJobOrder; //job order id
	application.output(arguments);
	var dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, maxReturnedRows)
	var countRow = 0;
//	application.output(dataset);
	for (var index = 1; index <= dataset.getMaxRowIndex(); index++) {
		var riga = dataset.getRowAsArray(index);
		countRow++;
		var pid = countRow;
		var tot_exp = '';
		if (riga[13]!=null && riga[13]>0) tot_exp = '<b>&nbsp;&nbsp;&nbsp;*</b>';
		treeviewDataSetEX.addRow([countRow, null, riga[1],tot_exp, '', '', '', '', '', '','',riga[0],'1blank.png','false']);	// nome
		countRow++;
		treeviewDataSetEX.addRow([countRow, pid, 'TOT= '+riga[12], riga[4], riga[5], riga[6], riga[7], riga[8], riga[9],riga[10],riga[11],'','1blank.png','false']);	// ore
		
	}
	//application.output(treeviewDataSet);
	return treeviewDataSetEX;
	
}

