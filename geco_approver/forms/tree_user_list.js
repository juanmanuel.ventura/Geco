/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"65A16D40-6D34-4127-AB24-5DFC4056E3E6"}
 */
function initTreeview(event) {
	elements.treeview.setDataSet(globals.createDataSetTimesheet());
	//apply css style
	elements.treeview.setStyleSheet(globals.myStyle);

	// setup columns size and format
	elements.treeview.setColumnWidth('treeColumn', 155);
	elements.treeview.setColumnWidth('column1', 31);
	elements.treeview.setColumnWidth('column2', 31);
	elements.treeview.setColumnWidth('column3', 31);
	elements.treeview.setColumnWidth('column4', 31);
	elements.treeview.setColumnWidth('column5', 31);
	elements.treeview.setColumnWidth('column6', 31);
	elements.treeview.setColumnWidth('column7', 31);
	elements.treeview.setColumnWidth('column8', 0);
	//elements.treeview.setColumnCount(5);
	elements.treeview.setRowHeight(100);
	elements.treeview.setColumnHeaderTextFormat('treeColumn',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 12px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column1',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column2',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column3',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column4',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column5',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column6',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column7',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	
	elements.treeview.setColumnTextFormat('treeColumn',"<html><font style='font-family: sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column1',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column2',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column3',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column4',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column5',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column6',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column7',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	
	
	elements.treeview.onNodeClicked(onNodeClicked);
	elements.treeview.onNodeSelected(onNodeClicked);
	arguments[0] = 1;
	elements.treeview.setSelectedNodes([1]);
	//application.output('init selected ' + elements.treeview.getSeletedNodes());
	onNodeSelected(event);

}

/**
 *@param {Object} obj
 * @properties={typeid:24,uuid:"F24F6CEA-5A0B-40B8-81B7-D15D6A10976A"}
 */
function getRowForId(obj) {
	// find the dataset row for the argument id
	for (var i = 2; i <= globals.treeviewDataSetTS.getMaxRowIndex(); i++) {
		var row = globals.treeviewDataSetTS.getRowAsArray(i);
		if (arguments[0] == row[0]) {
			return i;
		}
	}

	return -1;
}

/**
 * @properties={typeid:24,uuid:"73F92766-27CF-4715-B581-7F8C0709E014"}
 */
function onNodeClicked() {
	//application.output('Clicked');
	var riga = null;
	var row = getRowForId(arguments[0])
	var uid = null;
	var user = null;
	if (row > -1) {
		riga = globals.treeviewDataSetTS.getRowAsArray(row);
		//application.output('riga ' + riga);
		var pid = riga[1];
		if (pid == null) {
			user = riga[2];
			uid = riga[10];
		} else {
			var padre = globals.treeviewDataSetTS.getRowAsArray(arguments[0]);
			uid = padre[10];
			user = padre[2];
		}
	}
	//set selectedUser
	forms.approver_events_log_filter.selectedUser = uid;
	forms.approver_events_log_filter.applyFilter();
	forms.approver_events_log_list.labelUser=user;
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"0992E1AC-6755-46E8-BD4D-608CCC101E87"}
 */
function onNodeSelected(event)
{ 
	//application.output('selected');
	var riga = null;
	var row = getRowForId(arguments[0])
	var uid = null;
	var user = null;
	if (row > -1) {
		riga = globals.treeviewDataSetTS.getRowAsArray(row);
		//application.output('riga ' + riga);
		var pid = riga[1];
		if (pid == null) {
			user = riga[2];
			uid = riga[10];
		} else {
			var padre = globals.treeviewDataSetTS.getRowAsArray(arguments[0]);
			uid = padre[10];
			user = padre[2];
		}
	}
	//set selectedUser
	forms.approver_events_log_filter.selectedUser = uid;
	forms.approver_events_log_filter.applyFilter();
	forms.approver_events_log_list.labelUser=user;
}
