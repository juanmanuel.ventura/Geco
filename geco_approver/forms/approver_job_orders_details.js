/**
 * Handle record selected.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"A9BA3D2F-7B3C-4B28-B637-C5C655433196"}
 */
function onRecordSelection(event) {
	_super.onRecordSelection(event)

	toggleButton();
}

/**
 * @protected
 * @properties={typeid:24,uuid:"E3619848-6078-48F0-A55D-439719AB0A0B"}
 */
function toggleButton() {
	if (foundset.getSelectedRecord() != null) {
		if (foundset.getSelectedRecord().is_to_close == 1 || foundset.getSelectedRecord().is_enabled == 0) {
			elements.buttonClose.visible = false;
			elements.buttonClose.enabled = false;
			elements.buttonEdit.enabled = false;
		} else {
			elements.buttonClose.visible = !isEditing();
			elements.buttonClose.enabled = !isEditing();
			elements.buttonEdit.enabled = !isEditing();
		}
	} else {
		elements.buttonClose.visible = false;
		elements.buttonClose.enabled = false;
		elements.buttonEdit.enabled = false;
	}
}

/**
 * Perform the action disable job order.
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"8549152D-9D4B-48C1-A402-40FA930D5AB6"}
 * @SuppressWarnings(wrongparameters)
 */
function disableJobOrder(event) {

	var response = globals.DIALOGS.showWarningDialog("Conferma", "Procedere con la disattivazione della commessa?", "SI", "NO");
	if (response == "SI") {
		/** @type {JSRecord<db:/geco/job_orders>} */
		var record = foundset.getSelectedRecord();
		if (record.is_enabled == 1 && record.is_to_close == 0) {
			record.is_to_close = 1;
			_super.saveEdits(event);
			toggleButton();
		}
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"90697212-B9BC-4370-A9AD-9770400A842D"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
//	var record = foundset.getSelectedRecord();
//	var isLoggable = false;
//	if (record.job_order_type == 4) {
//		var dataset = record.getChangedData();
//		for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
//			// se la commessa gestionale diventa consuntivabile
//			if (dataset.getValue(i, 1) == 'is_loggable' && dataset.getValue(i, 2) != dataset.getValue(i, 3) && dataset.getValue(i,3) == 1) {
//				application.output('is_loggable della commessa ' + record.job_order_id + ' cambiato');
//				isLoggable = true;
//				break;
//			}
//		}
//	}
	_super.saveEdits(event);
	toggleButton();
//	//mando mail al controller
//	if(isLoggable == true && !globals.hasRole('Controllers')) {
//			globals.sendJobOrdersIsLoggableMail(17,record.external_code_navision);
//	}
}

/**
 * Perform the element default action. *
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"60A5258F-E9BB-4F34-BF16-5D084775E31E"}
 */
function startEditing(event) {
	_super.startEditing(event);
	toggleButton();
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"7D5CCFB4-D1BF-4CFB-9166-2E77090EFA3E"}
 */
function cancelEditRecord(event) {
	_super.stopEditing(event)
	toggleButton();

}
