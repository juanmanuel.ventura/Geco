dataSource:"db:/geco/expenses_log",
extendsID:"8D2801FD-40A3-430E-AC9C-F71E340C2CE1",
items:[
{
extendsID:"D53FBF98-55DF-42CD-B1DB-C0336069D1EE",
location:"5,32",
typeid:4,
uuid:"01568CF0-52CF-41F7-A31A-46B14A4587B9"
},
{
formIndex:6,
horizontalAlignment:2,
location:"894,32",
name:"descrizione",
onActionMethodID:"-1",
showClick:false,
showFocus:false,
size:"87,20",
styleClass:"small",
text:"Descrizione",
transparent:true,
typeid:7,
uuid:"09F6CF3F-052B-4DC5-9144-776DD4B00591"
},
{
extendsID:"0FB37070-BE9D-42FF-9344-D58C62600BC9",
formIndex:5,
location:"15,127",
typeid:7,
uuid:"15E05793-F119-4C59-A548-558D21B7C52D"
},
{
extendsID:"7478E0FF-5456-4278-B0E5-C29A67979D63",
location:"362,58",
size:"68,20",
typeid:7,
uuid:"164E9585-06AE-4C6E-B737-CE6A427FDBD0"
},
{
extendsID:"7C438F49-1B1F-4AA7-9E13-C46BD70E7408",
location:"362,32",
typeid:7,
uuid:"1D0A1638-8073-4337-A101-036870A17A91"
},
{
extendsID:"FF133E9E-CC6B-4C29-9B98-2966DCE961EB",
location:"30,58",
size:"30,20",
typeid:7,
uuid:"23F684FD-60DA-4244-A858-99FD17D3B1E4"
},
{
extendsID:"202B1611-69E9-45F6-B12B-2D1B29E4A1D2",
location:"163,32",
typeid:7,
uuid:"2E2B0794-7ECF-46C2-BE14-7953F36C0E6B"
},
{
extendsID:"3D652CFC-0533-4295-9A7C-67797603A0F3",
formIndex:2,
location:"766,127",
typeid:7,
uuid:"38BED5F9-CCB5-40AF-9302-C587623DFC27"
},
{
dataProviderID:"expenses_log_to_job_orders.job_orders_owner_to_users.users_to_contacts.last_name",
formIndex:4,
location:"809,58",
name:"userc",
onRenderMethodID:"-1",
showClick:false,
showFocus:false,
size:"80,20",
styleClass:"small",
transparent:true,
typeid:7,
uuid:"3F52B603-A077-4288-B5FE-6B824264D97D"
},
{
extendsID:"B2B9D4EF-B3FD-4B06-85F4-6245E56E9EB2",
formIndex:6,
location:"1180,127",
typeid:7,
uuid:"42FC3B2D-2ED3-41C6-8441-47AD3F6F50FE"
},
{
extendsID:"9CD53585-C8FD-43BD-AC16-8F045A3A44A4",
location:"431,32",
typeid:7,
uuid:"45BFDCB8-99DE-4AAC-9E2D-604BC627E898"
},
{
extendsID:"CF681843-783E-4DE2-8944-330E1C810A27",
location:"676,58",
size:"24,20",
typeid:4,
uuid:"490CC9C8-8EC1-4AB6-867F-A5E22554CE6C"
},
{
extendsID:"8E68034C-6454-453A-B24A-A5EC0F7F9002",
formIndex:8,
location:"180,127",
typeid:7,
uuid:"4A860CF3-D230-4AD1-A342-7E2EA5913018"
},
{
enabled:false,
extendsID:"4D1B12D1-9C43-4FEF-8C51-06DD796CE347",
formIndex:1,
location:"669,129",
typeid:7,
uuid:"520717A0-3F10-4244-8D67-582706EF57F8",
visible:false
},
{
formIndex:6,
horizontalAlignment:2,
location:"809,32",
name:"label_userc",
onActionMethodID:"-1",
showClick:false,
showFocus:false,
size:"80,20",
styleClass:"small",
text:"RDC",
transparent:true,
typeid:7,
uuid:"5599BF97-87A2-4E40-BA3B-5B151BE983E3"
},
{
extendsID:"4B2B0EEC-9CE6-4514-91E8-4A95BA09B529",
location:"5,60",
typeid:4,
uuid:"5EBFDBA0-A3ED-4C79-B9FB-63C3FC65F83A"
},
{
extendsID:"EDF54C3A-2154-48D8-B81F-D7B4DE6490FF",
location:"725,32",
onActionMethodID:"-1",
size:"80,20",
text:"Approvatore",
typeid:7,
uuid:"69DE5100-1AA2-4DEA-891B-C148BE805D9F"
},
{
extendsID:"DD49E15E-F7C7-4291-AEE9-3960A7B5DDFE",
formIndex:7,
location:"1214,127",
typeid:7,
uuid:"6B230273-2D92-4D05-80F0-B8DE11E0E019"
},
{
anchors:9,
extendsID:"0A67EC2C-53D2-4A54-AFED-61FAA97C4C66",
location:"388,92",
typeid:7,
uuid:"6D8C129B-F105-4D58-AAB2-0A0A0B38DB6D"
},
{
dataProviderID:"description",
formIndex:4,
location:"894,58",
name:"usercc",
onRenderMethodID:"-1",
showClick:false,
showFocus:false,
size:"379,20",
styleClass:"small",
transparent:true,
typeid:7,
uuid:"8C1384C6-5D6A-45ED-94D4-4CBFD89EE2C9"
},
{
extendsID:"12E51030-035C-4E66-A688-5D6B640E5A66",
location:"163,58",
size:"195,20",
typeid:7,
uuid:"989838BF-B4EF-4441-B318-49AA9C1C51CB"
},
{
extendsID:"CA2720D0-35E7-4EBC-89F4-10A91DF3C92F",
location:"677,32",
typeid:7,
uuid:"995757A1-C556-4223-91A9-BD3294D5C129"
},
{
extendsID:"1344AC9E-37B2-48BD-A374-903D1BC84DF1",
location:"619,32",
typeid:7,
uuid:"9CAEACF6-D134-4AD8-B17B-F2EB0A6A8237"
},
{
anchors:9,
extendsID:"D18DDC28-2181-4F87-988B-FA473CD07D51",
location:"603,92",
typeid:4,
uuid:"9F9BE76A-9E44-40EF-B6B8-3D5CE4A1C470"
},
{
extendsID:"D134F575-D0E7-4C5D-82AE-CDD1B908C550",
location:"88,32",
typeid:7,
uuid:"B01BBA92-8AF8-4640-AEB2-9CEE2845E4E9"
},
{
extendsID:"E7A8B2A4-33CF-4FF3-8A8B-9E02F5B2047F",
formIndex:9,
location:"356,127",
typeid:7,
uuid:"B6FB5856-0541-4626-AFA0-594A222EAF22"
},
{
dataProviderID:"expenses_log_approver_assigned.users_to_contacts.last_name",
extendsID:"382A4636-A1CB-4EDC-8AA4-B890237F2B0C",
location:"725,58",
size:"80,20",
typeid:7,
uuid:"C34C6B72-1793-4A68-9446-5589DCA422B1"
},
{
extendsID:"0094B6DC-EF33-49F2-A2EF-A8B9CE28533B",
height:54,
typeid:19,
uuid:"C45B02C9-8BD4-43F0-93A2-873AD1413EA1"
},
{
extendsID:"00AF8FA3-F4E3-439B-9614-C478F495969B",
location:"88,58",
size:"72,20",
typeid:7,
uuid:"C6AE0EC3-782E-44FC-8E3A-102FF437D08C"
},
{
extendsID:"507AE6FF-0BE0-4A35-BA25-04772C51149C",
formIndex:4,
location:"0,116",
size:"1300,48",
typeid:7,
uuid:"C8E3D5B3-FDF3-4413-9589-9292CAC4536C"
},
{
borderType:"SpecialMatteBorder,0.0,0.0,1.0,0.0,null,null,#cccccc,null,0.0,",
dataProviderID:"labelUser",
formIndex:4,
horizontalAlignment:0,
location:"5,5",
name:"labelUser",
onRenderMethodID:"-1",
showClick:false,
showFocus:false,
size:"293,24",
styleClass:"bold",
transparent:true,
typeid:7,
uuid:"CEE05028-9C93-41F6-821E-52CFC21C33CF"
},
{
extendsID:"E48B8D80-1E9C-486C-B7FA-C8514F9F2332",
location:"63,58",
size:"19,20",
typeid:7,
uuid:"D5EDA6D5-CC04-4622-8B0C-419B9485B56B"
},
{
extendsID:"1E294424-D591-43DB-8946-137FC47277BC",
location:"431,58",
size:"184,20",
typeid:7,
uuid:"D75CC61B-A74F-4F7D-9F10-F5B9A1A65E12"
},
{
enabled:false,
extendsID:"270912C1-3A27-4676-8977-FBF938CDDBA5",
formIndex:3,
location:"715,129",
typeid:7,
uuid:"DF9E17AB-3312-4EBC-9212-F5173BBBB523",
visible:false
},
{
extendsID:"BDC25A78-D323-499C-A645-9DC36F4804CA",
height:164,
typeid:19,
uuid:"E878DB5A-3D9A-43A5-BDA5-DC7EFEAAC2D6"
},
{
extendsID:"FB162E9A-56DD-4A6F-9C7F-196CCA1CC51A",
formIndex:0,
location:"700,129",
typeid:7,
uuid:"E953251E-1235-4BA5-B1AB-90CF28AAE6A8"
},
{
extendsID:"041188E2-43FB-48F3-B66E-627E32748814",
height:86,
typeid:19,
uuid:"F18F0E22-25CA-4902-BF82-5A8D885979E9"
},
{
extendsID:"935FC85B-337D-4A39-B233-10977B26CDB7",
location:"709,65",
typeid:7,
uuid:"F315AFD9-E822-4154-B638-EC86517BAC0E"
},
{
extendsID:"0C91DD6D-8F80-476D-93E4-8D19840A3C74",
location:"619,58",
size:"51,20",
typeid:7,
uuid:"F477F069-0E57-41F1-B0F2-CD4646AF42A1"
},
{
extendsID:"485830BF-D7F7-428C-8099-4D716ECD9256",
location:"30,32",
typeid:7,
uuid:"F7A5C275-9A61-432A-8DDE-D442B54B4A33"
}
],
name:"approver_expenses_log_list",
size:"1300,164",
styleName:"GeCo",
typeid:3,
uuid:"9FD4F529-94B0-4A1A-A855-31F55D5B32B1"