/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} [formFilterTopName]
 * @param {String} formFilterBottomName
 * @param {String} formListName
 * @param {String} formDetailsName
 * @param {String} [relation]
 * @protected
 * @properties={typeid:24,uuid:"0BDE3CDE-48E9-47C9-B8BA-9718BF998003"}
 */
function initialize(event, formFilterTopName, formFilterBottomName, formListName, formDetailsName, relation) {

	try {
		if (elements.split.dividerLocation) {
			elements.split.dividerSize = 0;
			elements.split.dividerLocation = 100;
		}
	} catch (e) {

	}
	if (formFilterTopName && forms[formFilterTopName]) {
		elements.split.setLeftForm(forms[formFilterTopName]);
		// force to apply filter (seems not to work on form's onLoad event)
		if (forms[formFilterTopName].applyFilter) forms[formFilterTopName].applyFilter();
		// load the list
		elements.split.setRightForm(forms[formFilterBottomName]);
	}

	try {
		if (elements.split_bottom.dividerLocation) {
			elements.split_bottom.dividerSize = 0;
			elements.split_bottom.dividerLocation = 400;
		}
	} catch (e) {
				application.output(e);
				application.output('errore initialize');
	}
	
	// load the list
	if (formListName && forms[formListName]) {
		elements.split_bottom.setLeftForm(forms[formListName]);
	}
	
	// load the Details
	if (formDetailsName && forms[formDetailsName]) {
		elements.split_bottom.setRightForm(forms[formDetailsName]);
	}
}
