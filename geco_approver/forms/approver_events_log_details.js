/**
 * @param event
 * @properties={typeid:24,uuid:"87590684-CA34-4454-90C7-FB4421E3A3F1"}
 */
function updateUI(event) {
	
	_super.updateUI(event);
	// buttons
	elements.buttonEdit.visible = false;
	elements.buttonSave.visible = false;
	elements.buttonCancel.visible = false;
}

