dataSource:"db:/geco/expenses_log",
extendsID:"EFB5B343-A839-4226-9BBA-D62D181E5906",
items:[
{
extendsID:"A14E015B-EEEA-4C9F-80B2-DFCC23C0293B",
typeid:7,
uuid:"0BECDF80-FDFF-4665-9F8B-66887AE90425",
visible:true
},
{
customProperties:"methods:{
onActionMethodID:{
arguments:[
null,
\"'approver_report_bundle'\"
]
}
}",
extendsID:"EA579005-097A-476D-B041-1E0CAAC62796",
onActionMethodID:"59C79DCE-D8FD-4CE8-BC0B-A249909C931F",
text:"Report",
typeid:7,
uuid:"38394123-4028-4496-AFE8-1B5EBA241DEA",
visible:true
},
{
anchors:3,
formIndex:3,
location:"656,48",
onActionMethodID:"D6256CDE-FF93-465C-9050-77637CF18E7B",
rolloverCursor:12,
showClick:false,
showFocus:false,
size:"120,20",
styleClass:"nav-action",
text:"Cambio Password",
transparent:true,
typeid:7,
uuid:"5D07EFC4-A03A-439C-8428-101C62A1115D"
},
{
customProperties:"methods:{
onActionMethodID:{
arguments:[
null,
\"'approver_events_log_bundle'\"
]
}
}",
extendsID:"D2EBD284-C528-478E-B491-44B19DC48EC2",
onActionMethodID:"59C79DCE-D8FD-4CE8-BC0B-A249909C931F",
text:"Timesheet",
typeid:7,
uuid:"AA27A6CE-667E-4FE4-8E0F-ADA6B0B628A0"
},
{
enabled:false,
extendsID:"A5A23C7B-0AA9-40F5-9187-222016450F41",
typeid:7,
uuid:"CA1779BB-B88A-4704-BD3A-13276C297713"
},
{
customProperties:"methods:{
onActionMethodID:{
arguments:[
null,
\"'approver_expenses_log_bundle'\"
]
}
}",
displaysTags:false,
extendsID:"8A1D43FD-CD10-47F4-82A3-CF5097C6403B",
onActionMethodID:"59C79DCE-D8FD-4CE8-BC0B-A249909C931F",
text:"Nota Spese",
typeid:7,
uuid:"E43BE29A-B9E1-478B-80F8-AC3EEDB9F944",
visible:true
},
{
customProperties:"methods:{
onActionMethodID:{
arguments:[
null,
\"'approver_job_orders_bundle'\"
]
}
}",
extendsID:"F91562A9-520A-4CCD-A68D-2DAAC0EF5BC8",
onActionMethodID:"59C79DCE-D8FD-4CE8-BC0B-A249909C931F",
text:"Commesse",
typeid:7,
uuid:"F8E0B5BB-38DF-442F-AB59-DE8B39B666CD",
visible:true
}
],
name:"main_approval",
onLoadMethodID:"AB8B9AE2-A75C-4838-A9BC-FF6DC45329B4",
onShowMethodID:"6BCBF0D6-1D6E-4ED7-9634-4C08CB99E5B6",
styleName:"GeCo",
typeid:3,
uuid:"26EA8EC4-E3F1-4773-97AE-3BF50B090E97"