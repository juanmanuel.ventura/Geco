/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"7CE4B429-C695-450E-B6C0-B2410797208E"}
 */
function initTreeview(event) {
	elements.treeview.setDataSet(globals.createDataSetExpenses());
	//apply css style
	elements.treeview.setStyleSheet(globals.myStyle);

	// setup columns size and format
	elements.treeview.setColumnWidth('treeColumn', 140);
	elements.treeview.setColumnWidth('column1', 31);
	elements.treeview.setColumnWidth('column2', 31);
	elements.treeview.setColumnWidth('column3', 31);
	elements.treeview.setColumnWidth('column4', 31);
	elements.treeview.setColumnWidth('column5', 31);
	elements.treeview.setColumnWidth('column6', 31);
	elements.treeview.setColumnWidth('column7', 31);
	elements.treeview.setColumnWidth('column8', 31);
	elements.treeview.setColumnWidth('column9', 0);
	//elements.treeview.setColumnCount(5);
	elements.treeview.setRowHeight(100);
	elements.treeview.setColumnHeaderTextFormat('treeColumn',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 12px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column1',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column2',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column3',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column4',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column5',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column6',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column7',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnHeaderTextFormat('column8',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	
	elements.treeview.setColumnTextFormat('treeColumn',"<html><font style='font-family: sans-serif; font-size: 11px; color: #727784;'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column1',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column2',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column3',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column4',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column5',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column6',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column7',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	elements.treeview.setColumnTextFormat('column8',"<html><font style='font-family: Calibri, Arial, sans-serif; font-size: 11px; color: #727784;text-align: right'>$text</font></html>");
	
	
	elements.treeview.onNodeClicked(onNodeClicked);
	elements.treeview.onNodeSelected(onNodeClicked);
	arguments[0] = 1;
	elements.treeview.setSelectedNodes([1]);
	//application.output('init selected ' + elements.treeview.getSeletedNodes());
	onNodeSelected(event);

}

/**
 *@param {Object} obj
 * @properties={typeid:24,uuid:"50CC6D23-15AE-492C-BB1C-2CE7AAE92B0A"}
 */
function getRowForId(obj) {
	// find the dataset row for the argument id
	for (var i = 2; i <= globals.treeviewDataSetEX.getMaxRowIndex(); i++) {
		var row = globals.treeviewDataSetEX.getRowAsArray(i);
		if (arguments[0] == row[0]) {
			return i;
		}
	}

	return -1;
}

/**
 * @properties={typeid:24,uuid:"CE9A93AD-C43B-4A14-B55E-19ADAC5473BA"}
 */
function onNodeClicked() {
	//application.output('Clicked');
	var riga = null;
	var row = getRowForId(arguments[0])
	var uid = null;
	var user = null;
	if (row > -1) {
		riga = globals.treeviewDataSetEX.getRowAsArray(row);
		//application.output('riga ' + riga);
		var pid = riga[1];
		if (pid == null) {
			user = riga[2];
			uid = riga[11];
		} else {
			var padre = globals.treeviewDataSetEX.getRowAsArray(arguments[0]);
			uid = padre[11];
			user = padre[2];
		}
	}
	//set selectedUser
	forms.approver_expenses_log_filter.selectedUser = uid;
	forms.approver_expenses_log_filter.applyFilter();
	forms.approver_expenses_log_list.labelUser=user;
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"9BD37396-F604-48DE-BCB3-4CBA1FC11682"}
 */
function onNodeSelected(event)
{ 
	//application.output('selected');
	var riga = null;
	var row = getRowForId(arguments[0])
	var uid = null;
	var user = null;
	if (row > -1) {
		riga = globals.treeviewDataSetEX.getRowAsArray(row);
		//application.output('riga ' + riga);
		var pid = riga[1];
		if (pid == null) {
			user = riga[2];
			uid = riga[11];
		} else {
			var padre = globals.treeviewDataSetEX.getRowAsArray(arguments[0]);
			uid = padre[11];
			user = padre[2];
		}
	}
	//set selectedUser
	forms.approver_expenses_log_filter.selectedUser = uid;
	forms.approver_expenses_log_filter.applyFilter();
	forms.approver_expenses_log_list.labelUser=user;
}
