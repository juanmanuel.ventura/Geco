/**
 * @type {Number}
 * @properties={typeid:35,uuid:"6CA1F8B5-2A47-493C-B749-4C8580965933",variableType:8}
 */
var selectedRDC = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"A12D6558-3EB3-4B4E-9898-C1E1E25B9BB8",variableType:8}
 */
var selectedProfitCenter = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"FCED09B0-8B0E-4047-BBA1-ACAE5152E2C3",variableType:8}
 */
var selectedJobOrder = null;

/**
 * @type {Number[]}
 *
 * @properties={typeid:35,uuid:"320ECD57-27CE-4252-A94A-AD0711BBE0AE",variableType:-4}
 */
var pcListAppr = globals.getProfitCenterByUserManager(scopes.globals.currentUserId);
/**
 * @type {String}
 * @properties={typeid:35,uuid:"B8735CED-4035-4A99-8EB9-8FE6E08A3562"}
 */
var pcToSearchAppr = (pcListAppr.length != 0) ? pcListAppr.join('||') : null;


/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"728F21C4-CC3C-4334-9A20-E83BD76400F0"}
 * @AllowToRunInFind
 */
function onLoad(event) {
	_super.onLoad(event);
	applyFilter(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"6D580370-B6E6-40CA-B2E1-AB35987A0E12"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	var isPcManager = globals.isProfitCenterManager(globals.currentUserId);
	var isMarketManager = (scopes.globals.hasRole('Resp. Mercato'));
	var isVice = (scopes.globals.hasRole('Vice Resp. Mercato'));
application.output('isPcManager '+isPcManager);
application.output( globals.isProfitCenterManager(globals.currentUserId));
application.output(scopes.globals.hasRole('Resp. Mercato'));
	if (foundset.find()) {
		
		foundset.job_order_id = selectedJobOrder;
		foundset.user_owner_id = globals.currentUserId;
		foundset.profit_center_id = selectedProfitCenter;	
		
		foundset.newRecord();		
		
		foundset.job_order_id = selectedJobOrder;
		foundset.user_owner_id = selectedRDC;
		
		if (isPcManager && isMarketManager) {
//			//logged user is pc-manager
//			foundset.user_owner_id = selectedRDC;
//			foundset.profit_center_id = selectedProfitCenter;
//			foundset.job_order_id = selectedJobOrder;
//			foundset.job_orders_to_profit_centers.user_owner_id = globals.currentUserId;
//			// OR clause
//			foundset.newRecord();
//			foundset.user_owner_id = (selectedRDC) ? selectedRDC : globals.currentUserId;
//			foundset.profit_center_id = selectedProfitCenter;
//			foundset.job_order_id = selectedJobOrder;
//			foundset.job_orders_to_profit_centers.user_owner_id = (selectedRDC) ? globals.currentUserId : null;
//			foundset.job_orders_to_profit_centers.profit_centers_to_owners_markets.user_id = globals.currentUserId;
			
			// Comm. da Ciro
			/*
			foundset.user_owner_id = selectedRDC;
			foundset.profit_center_id = selectedProfitCenter ? selectedProfitCenter : pcToSearchAppr;
			foundset.job_order_id = selectedJobOrder;
			foundset.job_orders_to_profit_centers.profit_centers_to_owners_markets.user_id = globals.currentUserId;
			
			foundset.newRecord();
			
			foundset.user_owner_id = selectedRDC ? selectedRDC : globals.currentUserId;
			foundset.profit_center_id = selectedProfitCenter ? selectedProfitCenter : pcToSearchAppr;
			foundset.job_order_id = selectedJobOrder;
			foundset.job_orders_to_profit_centers.profit_centers_to_owners_markets.user_id = globals.currentUserId;
			*/
			
			foundset.profit_center_id = selectedProfitCenter ? selectedProfitCenter : pcToSearchAppr;
			foundset.job_orders_to_profit_centers.profit_centers_to_owners_markets.user_id = globals.currentUserId;					
		}
		else if (isPcManager && !isMarketManager) {			
			// Comm. da Ciro
			/*
			foundset.user_owner_id = selectedRDC;
			foundset.profit_center_id = selectedProfitCenter;
			foundset.job_order_id = selectedJobOrder;
			foundset.job_orders_to_profit_centers.user_owner_id = globals.currentUserId;

			foundset.newRecord();
			
			foundset.user_owner_id = (selectedRDC) ? selectedRDC : globals.currentUserId;
			foundset.profit_center_id = selectedProfitCenter;
			foundset.job_order_id = selectedJobOrder;
			foundset.job_orders_to_profit_centers.user_owner_id = (selectedRDC) ? globals.currentUserId : null;
			*/
			foundset.profit_center_id = selectedProfitCenter;
			if (!isVice) {
			
			foundset.job_orders_to_profit_centers.user_owner_id = globals.currentUserId;
			}
			else{
				foundset.job_orders_to_profit_centers.profit_centers_to_vice_owner.user_id = globals.currentUserId;
			}
		}
		else {	
			// Comm. da Ciro
			/*
			foundset.job_order_id = selectedJobOrder;
			foundset.user_owner_id = globals.currentUserId;
			foundset.profit_center_id = selectedProfitCenter;	
			*/
			
			foundset.profit_center_id = selectedProfitCenter ? selectedProfitCenter : pcToSearchAppr;
			foundset.job_orders_to_profit_centers.profit_centers_to_vice_owner.user_id = globals.currentUserId;
		}
		foundset.search();
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} [goTop] true if the cursor must be placed at the first index
 * @properties={typeid:24,uuid:"0226A82E-6E5C-4858-893A-19055DDD1E9F"}
 */
function resetFilter(event, goTop) {
	selectedRDC = null;
	selectedProfitCenter = null;
	selectedJobOrder = null;
	_super.resetFilter(event, goTop);
	applyFilter(event);
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"F1373825-022C-403C-968D-C174964B2135"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
}
