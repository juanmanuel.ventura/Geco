/**
 * @param event
 *
 * @properties={typeid:24,uuid:"179B49C3-9D5A-4320-BBBA-21354BB4BC2A"}
 */
function updateUI(event) {
	_super.updateUI(event);

	// buttons
	elements.buttonEdit.visible = false;
	elements.buttonSave.visible = false;
	elements.buttonCancel.visible = false;

}