/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"EB8ADB76-62B5-4845-BB05-0969A33E0F61"}
 */
function resetFilter(event) {
	selectedProfitCenter = null;
	selectedStatus = 1;
	selectedJobOrder = null;
	selectedMonth = new Date().getMonth() + 1;
	selectedYear = new Date().getFullYear();
	forms.tree_expenses_user_list.initTreeview(event);
	toggleButton(selectedStatus != null);
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @protected
 *
 * @properties={typeid:24,uuid:"04E9B688-2769-48B3-B918-58F8A7AE396E"}
 */
function onDataChangeDate(oldValue, newValue, event) {
	forms.tree_expenses_user_list.elements.treeview.refresh(true);
	forms.tree_expenses_user_list.initTreeview(event);
	return true;
}

/**
 * @param {Boolean} status
 * @properties={typeid:24,uuid:"E591CB4A-27E2-4C73-BE98-62999D67729A"}
 */
function toggleButton(status) {
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		bundle.elements['split_bottom']['getRightForm']().elements['buttonApprove'].visible = status;
		bundle.elements['split_bottom']['getRightForm']().elements['buttonReject'].visible = status;
		bundle.elements['split_bottom']['getRightForm']().elements['buttonClose'].visible = status;
	}
}
