dataSource:"db:/geco/job_orders",
extendsID:"22140138-4F6E-42E1-9DFD-BDB0BAF23C8E",
items:[
{
formIndex:3,
horizontalAlignment:2,
location:"10,106",
size:"96,20",
text:"Profit Center",
transparent:true,
typeid:7,
uuid:"160ED724-AA6F-49C9-A1FE-D7D741885D7B"
},
{
anchors:3,
customProperties:"",
formIndex:2,
imageMediaID:"B432FC13-6422-4A74-956D-4474A7CD5FF0",
location:"374,15",
onActionMethodID:"0226A82E-6E5C-4858-893A-19055DDD1E9F",
rolloverImageMediaID:"B2C0D6C5-8456-4477-BFD5-825161DD2756",
showClick:false,
showFocus:false,
size:"16,17",
transparent:true,
typeid:7,
uuid:"17D2BC08-280F-40D6-A2A8-E1F86AE5B34E"
},
{
formIndex:3,
horizontalAlignment:2,
location:"10,50",
size:"96,20",
text:"Codice",
transparent:true,
typeid:7,
uuid:"30BFDD7E-DD51-4348-9349-0764073C69FB"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"selectedRDC",
displayType:10,
formIndex:1,
horizontalAlignment:2,
location:"111,78",
onActionMethodID:"-1",
onDataChangeMethodID:"F1373825-022C-403C-968D-C174964B2135",
size:"279,20",
text:"User Id",
typeid:4,
uuid:"3E28CC37-2DFF-45BB-845D-3693AA3A6161",
valuelistID:"3AB8DD5C-F02E-40DF-879B-9BFBCE22AF8D"
},
{
formIndex:3,
horizontalAlignment:2,
location:"10,78",
size:"96,20",
text:"RDC",
transparent:true,
typeid:7,
uuid:"560011B0-D957-43FB-9807-C1005695A905"
},
{
borderType:"SpecialMatteBorder,1.0,0.0,0.0,0.0,#c0c0c0,#c0c0c0,#c0c0c0,#c0c0c0,0.0,",
formIndex:4,
horizontalAlignment:4,
location:"0,40",
size:"400,1",
text:"",
transparent:true,
typeid:7,
uuid:"6025908D-DA56-454A-9147-980DF8B9FEE2"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"selectedProfitCenter",
displayType:10,
formIndex:1,
horizontalAlignment:2,
location:"111,106",
onActionMethodID:"-1",
onDataChangeMethodID:"F1373825-022C-403C-968D-C174964B2135",
size:"279,20",
text:"User Id",
toolTipText:"Filtra per ...",
typeid:4,
uuid:"97C5E45C-F1E1-4C54-AF43-E5B944CE82D3",
valuelistID:"73312BF3-989F-48F4-85E1-AB96BDEA9A3F"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"isEnabled",
displayType:4,
formIndex:1,
horizontalAlignment:2,
location:"10,11",
onActionMethodID:"6D580370-B6E6-40CA-B2E1-AB35987A0E12",
onDataChangeMethodID:"-1",
size:"153,20",
text:"Attivi",
toolTipText:"Mostra solo attivi",
transparent:true,
typeid:4,
uuid:"DBC257F9-3724-445D-9C4C-1592F42D56CD"
},
{
extendsID:"5C441A2C-B69C-4578-BB2D-7EB8DAFF0A08",
height:134,
typeid:19,
uuid:"E8A571C0-5690-49A5-ABBE-B329D293F477"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"selectedJobOrder",
displayType:10,
formIndex:1,
horizontalAlignment:2,
location:"111,50",
onActionMethodID:"-1",
onDataChangeMethodID:"F1373825-022C-403C-968D-C174964B2135",
size:"279,20",
text:"User Id",
typeid:4,
uuid:"EFAC3F8C-9F14-408D-B387-7A87A23A2EE6",
valuelistID:"54327DE3-DF2E-4FC7-8DF5-163FF1221191"
}
],
name:"approver_job_orders_filter",
onLoadMethodID:"728F21C4-CC3C-4334-9A20-E83BD76400F0",
transparent:false,
typeid:3,
uuid:"4138EE82-4D65-45FC-9251-B94198F8375F"