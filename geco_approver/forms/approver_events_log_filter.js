/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"92D33CA2-9EF7-47E1-9CE8-6FFCE2BAB77F"}
 */
function resetFilter(event) {
	selectedProfitCenter = null;
	selectedStatus = 1;
	selectedJobOrder = null;
	selectedMonth = new Date().getMonth() + 1;
	selectedYear = new Date().getFullYear();
	forms.tree_user_list.initTreeview(event);
	toggleButton(selectedStatus != null);
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @protected
 *
 * @properties={typeid:24,uuid:"3486C735-CCDD-4E2C-973D-7407F8AF4264"}
 */
function onDataChangeDate(oldValue, newValue, event) {
	forms.tree_user_list.elements.treeview.refresh(true);
	forms.tree_user_list.initTreeview(event);
	return true;
}

/**
 * @param {Boolean} status
 * @properties={typeid:24,uuid:"434D2ED4-83B7-4ECF-9420-8C33CAB0884C"}
 */
function toggleButton(status) {
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		bundle.elements['split_bottom']['getRightForm']().elements['buttonApprove'].visible = status;
		bundle.elements['split_bottom']['getRightForm']().elements['buttonReject'].visible = status;
		bundle.elements['split_bottom']['getRightForm']().elements['buttonClose'].visible = status;
	}
}
