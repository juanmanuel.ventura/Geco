/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"C8DAE551-F4F6-4EF6-B93B-D29AA01D3593"}
 */
function updateUI(event) {
	_super.updateUI(event);

	controller.readOnly = !isEditing();

	// get the bundle
	var mainForm = forms[controller.getFormContext().getValue(2, 2)] || null;
	if (mainForm) {
		// get the split pane
		/** @type {RuntimeSplitPane} */
		var split = mainForm.elements['split'];

		// call updateUI in contained forms
		if (split!=null) {
			split.getLeftForm()['updateUI']();
			split.getRightForm()['updateUI']();
		}
	}
}
