/**
 * @type {Date}
 * @properties={typeid:35,uuid:"089B13C9-FA06-449F-BC6A-43279A229027",variableType:93}
 */
var selectedDatefrom = null;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"CFB88B3B-1701-41D4-8A80-0A34AE599A85",variableType:93}
 */
var selectedDateAt = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"54FE8935-BC40-465E-A0A0-0AB753731AAC",variableType:8}
 */
var selectedJobOrder = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"8C330826-E441-403D-A4BD-42E054721C38",variableType:8}
 */
var selectedUser = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"3379B7E1-D65E-48F4-B834-67FB1941A644",variableType:8}
 */
var selectedRDC = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"7F1451EE-51A9-4B02-91F5-762A4664F3CA",variableType:8}
 */
var selectedProfCenterUser = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"E17D6C74-86A1-4943-AEA5-8A3F2CBCAF15",variableType:8}
 */
var selectedProfCenterJob = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"FF1F0247-22A8-46E8-B6BA-042C442CB6BA",variableType:4}
 */
var isDeliveryDirector = 0;

/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"477653FC-16D6-4225-BAA7-9CAA2145A6A0",variableType:-4}
 */
var vl_joborders = null;

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number}   queryType
 * @properties={typeid:24,uuid:"B62464B0-2F42-4069-9F47-67910C3AFC60"}
 */
function exportReport(event, queryType) {
	application.output(queryType);
	var _query_download = null;
	var nameExport = null;
	if (queryType != 4 && (globals.isEmpty(selectedDatefrom) || globals.isEmpty(selectedDateAt))) {
		globals.DIALOGS.showWarningDialog("Errore", "Inserire la data di inizio e fine", "OK");
		return
	}
	//	if (globals.isEmpty(selectedDateAt)) {
	//		globals.DIALOGS.showWarningDialog("Errore", "Inserire la data di fine", "OK");
	//		return
	//	}
	if (queryType == 1) {
		_query_download = 'call sp_getReportPresence ( ?, ?, ?, ?, ?, ?, ?, ?)';
		nameExport = 'Presenze_'
	} else if (queryType == 2) {
		//non ancora è stata creata questa stored procedure
		_query_download = 'call sp_getReportExpenses (  ?, ?, ?, ?, ?, ?, ?, ?)';
		nameExport = 'NS_'
	} else if (queryType == 3) {
		//non ancora è stata creata questa stored procedure
		_query_download = 'call sp_getReportJobOrder ( ?, ?, ?, ?, ?, ?, ?, ?)';
		nameExport = 'Commesse_'
	} else if (queryType == 4) {
		//non ancora è stata creata questa stored procedure
		_query_download = 'call sp_get_users_for_pc ( ?, ?, ?, ?, ?, ?, ?, ?)';
		nameExport = 'UserPC_'
		selectedDatefrom = new Date();
		selectedDateAt = new Date();
			
	}
	//iFrom, iTo, iUserId, iJobOrderId, iUserOwnerId, iProfCenterUserId, iProfCenterJobId, iApproverId
	var arguments = new Array();
	arguments[0] = selectedDatefrom;
	arguments[1] = selectedDateAt;
	arguments[2] = (selectedUser != null) ? selectedUser : 0;
	arguments[3] = (selectedJobOrder != null) ? selectedJobOrder : 0;
	arguments[4] = (selectedRDC != null) ? selectedRDC : 0;
	arguments[5] = (selectedProfCenterUser != null) ? selectedProfCenterUser : 0;
	arguments[6] = (selectedProfCenterJob != null) ? selectedProfCenterJob : 0;
	arguments[7] = (isDeliveryDirector == 0) ? globals.currentUserId : 0;
	application.output(_query_download+ ' ' +arguments);
	var dataset = databaseManager.getDataSetByQuery('geco', _query_download, arguments, 10000);

	var htmlTable = '<html>' + dataset.getAsHTML(true, true, false, true, true) + '</html>';

	forms.approver_report_list.tableReport = htmlTable;
	forms.approver_report_list.datasetReport = dataset;
	forms.approver_report_list.nameExport = nameExport;

}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"04B74751-885C-489D-8940-4B442FAD8FC9"}
 */
function refresh(event) {
	selectedDatefrom = null;
	selectedDateAt = null;
	selectedJobOrder = null;
	selectedUser = null;
	selectedRDC = null;
	selectedProfCenterUser = null;
	selectedProfCenterJob = null;

	forms.approver_report_list.tableReport = '';
	forms.approver_report_list.datasetReport = null;
	forms.approver_report_list.nameExport = '';
}

/**
 * @param event
 * @properties={typeid:24,uuid:"26CCF06D-D661-411F-B79D-7C1E068954B8"}
 */
function updateUI(event) {
	_super.updateUI(event);
	if (globals.hasRole("Delivery Admin")) {
		elements.slectReport.visible = true;
	} else elements.slectReport.visible = false;

	if (globals.isProfitCenterManager(scopes.globals.currentUserId) || globals.hasRole("Delivery Admin")) {
		elements.buttonPresence.enabled = true;
		elements.buttonUserPC.enabled = true;
		elements.buttonUserPC.visible = true;
		elements.buttonReset.setLocation(777,70);
	} else {
		elements.buttonPresence.enabled = false;
		elements.buttonUserPC.visible = false;
		elements.buttonReset.setLocation(590,70);
	}
}
