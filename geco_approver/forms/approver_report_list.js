/**
 * @type {String}
 * @properties={typeid:35,uuid:"30316B39-8086-4B91-82C9-07C041C973FC"}
 */
var tableReport = '';

/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"DBFB0D86-094F-4C93-941C-D2771A9C8140",variableType:-4}
 */
var datasetReport = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"560515ED-0CDA-4E36-A19B-1739E2456413"}
 */
var nameExport= '';
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"23C5E777-D154-4704-B079-72ADC435F8D8"}
 */
function saveReport(event) {
	var success = false;
	var line = '';
	if (datasetReport != null) {

		var colArray = new Array()
		for (var i = 1; i <= datasetReport.getMaxColumnIndex(); i++)
		{
			colArray[i-1] = datasetReport.getColumnName(i)
			//note the -1, because an array is zero based and dataset is 1 based.
		}
		//application.output(colArray);
		line = colArray.join('\t')+ '\r\n';
		for (var index = 1; index <= datasetReport.getMaxRowIndex(); index++) {
			var row = datasetReport.getRowAsArray(index);
			var rowstring = row.join('\t') ;
			line = line + rowstring + '\n';
		}
		//application.output(line);
		if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
			/** @type {String} */
			var yearFrom = '' + forms.approver_report_filter.selectedDatefrom.getFullYear();
			/** @type {String} */
			var monthFrom = '' + (forms.approver_report_filter.selectedDatefrom.getMonth() + 1);
			/** @type {String} */
			var dayFrom = '' + forms.approver_report_filter.selectedDatefrom.getDate();
			/** @type {String} */
			var dateFrom = yearFrom + ((monthFrom.length > 1) ? monthFrom : '0' + monthFrom)+ ((dayFrom.length > 1) ? dayFrom : '0' + dayFrom);
			/** @type {String} */
			var yearTo = '' + forms.approver_report_filter.selectedDateAt.getFullYear();
			/** @type {String} */
			var monthTo = '' + (forms.approver_report_filter.selectedDateAt.getMonth() + 1);
			/** @type {String} */
			var dayTo = '' + forms.approver_report_filter.selectedDateAt.getDate();
			/** @type {String} */
			var dateTo = yearTo + (monthTo.length > 1 ? monthTo : '0' + monthTo) + (dayTo.length > 1 ? dayTo : '0' + dayTo);

			var fileName = nameExport + dateFrom + '_' + dateTo + '.xls';
			success = plugins.file.writeTXTFile(fileName, line, 'UTF-8', 'application/vnd.ms-excel');
		}
	}
	if (!success) application.output('Scrittura file non riuscita');
}
