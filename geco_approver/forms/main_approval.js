///**@type {String}
// * @properties={typeid:35,uuid:"18C07910-326A-4A12-A4CA-EB2908C80C12"}
// */
//var dynamicValuelist = null;
//
///**
// * @type {JSDataSet}
// * @properties={typeid:35,uuid:"B83921AD-02DB-45CF-A8F6-2EB1B6C24805",variableType:-4}
// */
//var dynamicDataset = null;
//
///**@type {Array<String>}
// * @properties={typeid:35,uuid:"C2967AE1-322E-4EAE-BD9D-54E5734EEEC2",variableType:-4}
// */
//var solutionsList = [];

/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"50E96F24-84B7-4A17-82CF-7BAA1129C7CE",variableType:-4}
 */
var vl_joborders = null;

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"AB8B9AE2-A75C-4838-A9BC-FF6DC45329B4"}
 */
function initialize(event) {
	if (!globals.grantAccess(['Approvers'])) {
		logOut(event);
	}
	_super.initialize(event);
//	//setValueList(event);
//	dynamicDataset = databaseManager.createEmptyDataSet();
////	if (globals.hasRole('Approvers')) solutionsList.push('Approver');
//	if (globals.hasRole('Controllers')) solutionsList.push('Controller');
//	if (globals.hasRole('HR Administrators') || globals.hasRole('HR Expenses') || globals.hasRole('Admin Readers')) solutionsList.push('Personnel');
//	if (globals.hasRole('Delivery Admin') || globals.hasRole('Responsabili Commessa') || globals.hasRole('Controllers') || globals.hasRole('Resp. Mercato') || globals.hasRole('Orders Admin') || globals.hasRole('Planner') || globals.hasRole('Supp. Commerciale')) solutionsList.push('Management');
//	if (globals.currentUserDisplayName == 'STAFFA JONATHAN' || globals.currentUserDisplayName == 'FRAU DANIELA') solutionsList.push('Configurator');
//	solutionsList.push('Logger');
//	application.output('solutionsList.length: ' + solutionsList.length + '; solutionsList: ' + solutionsList);
//	if (solutionsList.length > 0) {
//		solutionsList.sort();
//		for (var index = 0; index < solutionsList.length; index++) {
//			dynamicDataset.addRow(new Array(solutionsList[index]));
//		}
//		elements.dynamicSolutionsChoice.setValueListItems(dynamicDataset);
//		elements.lSolutionName.enabled = true;
//		elements.lSolutionName.visible = true;
//		elements.dynamicSolutionsChoice.enabled = true;
//		elements.dynamicSolutionsChoice.visible = true;
//	}
	//application.output('globals.hasRole(\'Responsabili Commessa\'): ' + globals.hasRole('Responsabili Commessa') + '; globals.hasRole(\'Resp. Mercato\'): ' + globals.hasRole('Resp. Mercato'));
	loadPanel(event, 'approver_events_log_bundle');
	_super.checkPwdExpiration();
}

/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"515EEB85-9B30-415A-B30E-4F4F1018836C"}
 */
function setValueList(event) {
	var user = globals.currentUserId;
	var query = null;
	//Se l'utente loggato � RDC e RPC contemporaneamente
	if(globals.isJobOrderManager(user) && globals.isProfitCenterManager(user)){
		query = ' select concat(j.external_code_navision, \' - \', j.job_order_title), \
					j.job_order_id \
				from job_orders j \
				where j.profit_center_id in ( \
					select p.profit_center_id \
					from profit_centers p \
					where p.user_owner_id = ' + user +' ) \
					or j.user_owner_id = ' + user+' \
				order by j.external_code_navision';
	}
	//Se l'utente loggato � soltanto RDC
	else if(globals.isJobOrderManager(user) && !globals.isProfitCenterManager(user)){
		query = ' select concat(external_code_navision, \' - \', \
					job_order_title),job_order_id \
				from job_orders \
				where user_owner_id = ' + user;
	}
	//Se l'utente loggato � solo RPC
	else if(!globals.isJobOrderManager(user) && globals.isProfitCenterManager(user)){
		query = 'select concat(j.external_code_navision, \' - \', j.job_order_title),j.job_order_id \
		from job_orders j \
		where j.profit_center_id in ( \
			select p.profit_center_id \
			from profit_centers p \
			where p.user_owner_id = ' + user +')'
	}
	
	vl_joborders = databaseManager.getDataSetByQuery('geco', query , null,10000);
	//Imposto i valori della valuelist con quelli del dataset della query
	application.setValueListItems('currentOwnerUserJobOrderList',vl_joborders);
}

/**
 * Callback method for when form is shown.
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"6BCBF0D6-1D6E-4ED7-9634-4C08CB99E5B6"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
//	dynamicValuelist = null;
}
