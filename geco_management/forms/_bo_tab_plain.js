/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"F6994B3F-3602-4712-8085-6C57A4621002"}
 */
function updateUI(event) {
	_super.updateUI(event);
	controller.readOnly = !isEditing();
	
	// buttons
	elements.buttonEdit.visible = !isEditing();
	elements.buttonSave.visible = isEditing();
	elements.buttonCancel.visible = isEditing();
	
	elements.buttonAdd.visible = !isEditing();
	elements.buttonDelete.visible = !isEditing();
	
	elements.button_singleplain.enabled = isEditing();
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} direction
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"35B630F3-A0CE-47D9-9B33-769C6DB909C8"}
 */
function changeYear(event, direction) {
	//controller.readOnly = false;

	if (direction == 'avanti')
		scopes.globals.selectedYear = scopes.globals.selectedYear + 1;
	else if (direction == 'indietro')
		scopes.globals.selectedYear = scopes.globals.selectedYear - 1;
	foundset.loadRecords();
	updateUI(event);
	
	return true
}