/**
 * @type {Number}
 * @properties={typeid:35,uuid:"BE3198B4-03B7-4140-9610-70DB360A2309",variableType:8}
 */
var boFilter = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"54A2A89F-A9D7-405A-878F-17032522CB4D",variableType:8}
 */
var boID = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"D33BD8C2-B929-4D9B-9B64-61C2C2555923",variableType:8}
 */
var totProfit = 0.0;
/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"88A3F8A1-877D-447B-8597-91542CA4E039",variableType:8}
 */
var totCost = 0.0;
/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"CF86723F-06BD-4A21-8DA2-9F38259B56C3",variableType:8}
 */
var totOffer = 0.0;
/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"F5C3C019-3091-4F22-92A1-D8D6BD24D9EF",variableType:8}
 */
var totOrder = 0.0;

/**
 * @param {JSEvent} event The event that triggered the action.
 *
 * @properties={typeid:24,uuid:"E676F5C9-49C4-4B26-A947-24BF9EA2C5ED"}
 */
function updateUI(event) {
	controller.enabled = true;
	var canGoToBo = globals.hasRole('Delivery Admin') || globals.hasRole('Controllers') || globals.hasRole('Resp. Mercato') || globals.hasRole('Planner') || globals.hasRole('Orders Admin') || globals.hasRole('Vice Resp. Mercato');
	elements.showBo.visible = canGoToBo;
}

/**
 * @private
 *
 * @properties={typeid:24,uuid:"B93DBADC-F795-4F49-A0C7-0722ADDD3C46"}
 * @AllowToRunInFind
 */
function applyFilter() {
	totProfit = 0.0;
	totCost = 0.0;
	totOrder = 0.0;
	totOffer = 0.0;
	if (foundset.find()) {
		foundset.bo_id = boFilter;
		foundset.search();
	}
	var totalRecord = databaseManager.getFoundSetCount(foundset);
	
	for (var i = 1; i<=totalRecord; i++){
		var rec= foundset.getRecord(i);
		totProfit = totProfit + rec.bo_to_bo_details.total_return;
		totCost = totCost + rec.bo_to_bo_details.total_cost;
		totOrder = totOrder + rec.order_amount;
		totOffer = totOffer + rec.offer_amount;
	}
	//boFilter=null;
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"628D15FE-D6B1-47A8-99B0-26984A14FCAB"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter();
	return true
}

/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"6DBD7327-38C7-450F-8E78-AF1981EA5857"}
 */
function goToSelect(event) {
	globals.bo_link_selected = boID;
	forms.main_bo.loadPanel(event, 'bo_bundle');
	scopes.globals.showDetailsList(event, 'bo_details', true, 'bo')
}

/**
 * Handle record selected.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"F36D9F34-AF73-4294-B68A-DE83F7E30B88"}
 */
function onRecordSelection(event) {
	_super.onRecordSelection(event);
	if (foundset.getSize() > 0) {
		boID = foundset.getSelectedRecord().bo_id;
//		application.output('onRecordSelection bo selezionata ' + boID)
	}
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"D1356459-8D73-40A6-A55E-1808B50CB7DF"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
	applyFilter();
}
