/**
 * @properties={typeid:35,uuid:"B08212E6-45C4-4A7E-A998-449ABCE632FB",variableType:-4}
 */
var isMgmrPc = globals.hasRole('Resp. Mercato') || globals.hasRole('Vice Resp. Mercato');

/**
 * @properties={typeid:35,uuid:"21587DF8-7B8B-4063-8C34-2EC3F19E4301",variableType:-4}
 */
var isSupp = globals.hasRole('Supp. Commerciale');

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"308A8926-1CDC-48A7-9E1D-EC2C5C429DC2",variableType:8}
 */
var customer = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"9EAFD200-8A5A-47CA-865D-764F77C31AF0"}
 */
var rif_customer = null;
/**
 * @type {Date}
 * @properties={typeid:35,uuid:"1CA9B78A-E676-4CA8-977F-4829B3210E41",variableType:93}
 */
var dateFrom = null;
/**
 * @type {Date}
 * @properties={typeid:35,uuid:"6D0BE760-501D-4A3A-8E98-10CD46701254",variableType:93}
 */
var dateTo = null;
/**
 * @type {Number}
 * @properties={typeid:35,uuid:"5EC8A5EE-A336-4E02-B00A-E9AA589C5645",variableType:8}
 */
var rdcJo = null;
/**
 * @type {Number}
 * @properties={typeid:35,uuid:"9B593C8B-F53B-4276-91B5-F9779F159D34",variableType:8}
 */
var statusBo = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"6CB2168C-B6D6-4F66-84B3-CE187720D3B8",variableType:8}
 */
var yearBo = scopes.globals.selectedYear;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"A2EAF1E1-D550-42CA-89DB-13B6D82D4EE1",variableType:8}
 */
var jobOrderId = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"5051064C-B0C9-4231-A9EC-C71300E45863",variableType:8}
 */
var prob = null;
/**
 * @type {Number}
 * @properties={typeid:35,uuid:"BC6ED7A2-347A-4B0C-9C08-EDA0899E3DAC",variableType:8}
 */
var statusJo = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"886CBA91-5612-4FA8-A5D6-AD0A07979B65",variableType:8}
 */
var docType = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F1997E4D-4CA7-4042-9AB1-3840A536B3BD",variableType:8}
 */
var orderType = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F3086C4A-29EE-4C35-8C18-602DB3651891",variableType:8}
 */
var boID = null;

/**
 * lista CP di cui è vice o responsabile
 * @type {Number[]}
 * @properties={typeid:35,uuid:"93281373-CEE2-46EB-B44F-29D5A03ACDA4",variableType:-4}
 */
var pcList = (isMgmrPc) ? (globals.getProfitCenterByUserManager(scopes.globals.currentUserId)) : [];

/**
 * @type {String}
 * @properties={typeid:35,uuid:"EFD74649-C647-49E6-8094-2A4B77C1149D"}
 */
var pcToSearch = (pcList.length != 0) ? pcList.join('||') : null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"EF1168DC-6CB0-4159-A62F-F0D385467D4B",variableType:8}
 */
var profitCenterId = (pcList.length != 0) ? pcList[0] : null;

/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"4DB44DD0-00C9-42A6-BDD7-281135230993",variableType:-4}
 */
var pcOwnerList = (isMgmrPc) ? (globals.getPcOwnerByUserManager(scopes.globals.currentUserId)) : [];

/**
 * @type {String}
 * @properties={typeid:35,uuid:"81A8E876-0DCF-4DB1-97A4-E51253BBC0FB"}
 */
var pcOwnerToSearch = (pcOwnerList.length != 0) ? pcOwnerList.join('||') : null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"396A3AC0-630A-403D-BA48-CE6165EB5482",variableType:8}
 */
var pcOwnerID = globals.hasRole('Resp. Mercato') ? scopes.globals.currentUserId : (pcOwnerList.length != 0) ? pcOwnerList[0] : null;

/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"84B9BF34-DD75-43B8-A2E4-3BBD4FB9D962",variableType:-4}
 */
var pcListSupp = (isSupp) ? (globals.getProfitCenterSupportByUserManager(scopes.globals.currentUserId)) : [];

/**
 * @type {String}
 * @properties={typeid:35,uuid:"4F74566D-0D15-40D4-A2A0-D28C14F0FDF7"}
 */
var pcToSearchSupp = (pcListSupp.length != 0) ? pcListSupp.join('||') : null;

/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"8641B330-C568-4A5D-B5B9-8EA32520229A",variableType:-4}
 */
var pcOwnerListSupport =(isSupp) ? (globals.getPcOwnerSupportByUserManager(scopes.globals.currentUserId)) : [];

/**
 * @type {String}
 * @properties={typeid:35,uuid:"4F6D2176-B8D7-4560-A414-B9376080F966"}
 */
var pcOwnerToSearchSupport = (pcOwnerListSupport.length != 0) ? pcOwnerListSupport.join('||') : null;

/**
 * @properties={typeid:35,uuid:"1A303F13-8E79-4241-9E86-EF35816F267E",variableType:-4}
 */
var pcOwnerIDSupport = (pcOwnerListSupport.length != 0) ? pcOwnerListSupport[0] : null;

/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"D4F66EF7-0A2A-4317-8148-3DFC17A13A58",variableType:-4}
 */
var listCustomers = scopes.globals.getUserClientsForPCSelected(scopes.globals.currentUserId, null);

/**
 * @type {String}
 * @properties={typeid:35,uuid:"D84980C4-BEAB-4B32-BC71-1BE63D685947"}
 */
var customerToSearch = (listCustomers.length != 0) ? listCustomers.join('||') : [];

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"660CE8CD-C21E-49C3-896B-DA9E5A4A77E5",variableType:4}
 */
var roleSelected = 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"A1DD3DFB-9C0F-4702-9DBD-A95F8AF65234",variableType:8}
 */
var supportUserId = null;

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"00F28695-2DB9-41E4-845D-72EDF8618C44",variableType:-4}
 */
var isController = globals.hasRole('Controllers');

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"FD9853C5-7AE2-48EB-BD47-6175B7770BF3"}
 */
function updateUI(event) {
	//	_super.updateUI(event);
	elements.profitCenterContr.visible = globals.hasRole('Controllers') || globals.hasRole('Planner') || globals.hasRole('Orders Admin') || globals.hasRole('Steering Board');
	elements.profitCenter.visible = !globals.hasRole('Controllers') && !globals.hasRole('Planner') && !globals.hasRole('Orders Admin') && !globals.hasRole('Steering Board');
	elements.lSupport.visible = isController;
	elements.support.visible = isController;
	elements.rSuppResp.visible = isMgmrPc && isSupp
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @properties={typeid:24,uuid:"6B388C8A-8097-4EDC-BDCD-071CB81BE37B"}
 */
function onDataChange(oldValue, newValue, event) {
	globals.filterIntemediate = false;
	applyFilter(event);
	updateUI(event);
	return true;
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"FD2FBE88-3A51-4592-A5C6-D239C3F2C751"}
 */
function applyFilter(event) {
	application.output('vice o resp ' + pcToSearch);
	application.output('owner vice o resp ' + pcOwnerToSearch);
	application.output('supp ' + pcToSearchSupp);
	application.output('owner supp ' + pcOwnerToSearchSupport);

	if (foundset.find()) {
		if (globals.hasRole('Controllers') || globals.hasRole('Planner') || globals.hasRole('Orders Admin') || globals.hasRole('Steering Board')) {
			foundset.pc_owner_id = pcOwnerID;
			foundset.profit_center_id = profitCenterId;
			foundset.created_by = supportUserId;
		} else if (isMgmrPc) {
			if (roleSelected == 1) {
				if (profitCenterId == null) {
					foundset.profit_center_id = pcToSearch;
				} else if (pcList.indexOf(profitCenterId) > -1) {
					foundset.profit_center_id = profitCenterId;
				} else {
					foundset.profit_center_id = -1;
					foundset.bo_to_view_user_pc_mgm.user_id = pcOwnerToSearch;
				}
				if (pcOwnerID == null) {
					foundset.bo_to_view_user_pc_mgm.user_id = pcOwnerToSearch;
				} else if (pcOwnerList.indexOf(pcOwnerID) > -1) {
					foundset.bo_to_view_user_pc_mgm.user_id = pcOwnerID;
				} else {
					foundset.profit_center_id = pcToSearch;
					foundset.bo_to_view_user_pc_mgm.user_id = -1;
				}
				foundset.company_id = customer;
			}
			else {
				if (profitCenterId == null) {
					foundset.bo_to_view_user_pc_mgm.profit_center_id = pcToSearchSupp;
				} else if (pcListSupp.indexOf(profitCenterId) > -1) {
					foundset.profit_center_id = profitCenterId;
				} else {
					foundset.profit_center_id = -1;
					foundset.bo_to_view_user_pc_mgm.user_id = pcOwnerID;
				}
				if (pcOwnerID == null) {
					foundset.bo_to_view_user_pc_mgm.user_id = pcOwnerToSearchSupport;
				} else if (pcOwnerListSupport.indexOf(pcOwnerID) > -1) {
					foundset.bo_to_view_user_pc_mgm.user_id = pcOwnerID;
				} else {
					foundset.profit_center_id = pcToSearchSupp;
					foundset.bo_to_view_user_pc_mgm.user_id = -1;
				}
				if (customer == null) {
					foundset.company_id = customerToSearch;
				}
				else if (listCustomers.indexOf(customer) > -1) {
					foundset.company_id = customer;
				} else {
					foundset.company_id = -1;
				}
			}
		}
		//		controllo su cliente finale
		//		if (finalCustomer != null) foundset.final_company_id = finalCustomer;
		//		else foundset.final_company_id = null;
		if (dateFrom != null && dateTo != null) foundset.created_at = '#' + utils.dateFormat(dateFrom, "yyyy-MM-dd") + "..." + utils.dateFormat(dateTo, "yyyy-MM-dd") + "|yyyy-MM-dd";
		foundset.bo_year = yearBo;
		foundset.status = statusBo;
		foundset.customer_person = rif_customer;
		foundset.bo_to_job_orders.is_enabled = statusJo;
		foundset.job_order_id = jobOrderId;
		foundset.bo_id = boID;

		switch (docType) {
		case 1: // tutti
			foundset.business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.bo_document_type = '1||2||3||4';
			break;
		case 2: //offerta
			foundset.business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.bo_document_type = 2;
			break;
		case 3: // ordine
			foundset.business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.bo_document_type = 3;
			break;
		case 4://altro
			foundset.business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.bo_document_type = '1||3';
			break;
		case 5://5 nessuna offerta
			foundset.business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.bo_document_type = '!' + 2;
			break;
		case 6://6 nessuno
			foundset.business_opportunities_to_bo_bo_documents.totalDoc = 0;
			break;
		default:
			break;
		}
		foundset.bo_probability = prob
		foundset.bo_to_job_orders.user_owner_id = rdcJo;
		switch (orderType) {
		case 1: // Con ordini|1.0
			foundset.bo_to_bo_orders.totalOrders = '!' + 0;
			break;
		case 2: //Senza ordini|2.0
			foundset.bo_to_bo_orders.totalOrders = 0;
			break;
		default:
			break;
		}
		//		var totSearch = foundset.search();
		foundset.search();
		application.output(databaseManager.getSQL(foundset));
	}
	var totalRecord = databaseManager.getFoundSetCount(foundset);
	controller.setSelectedIndex(totalRecord);
	application.output(globals.messageLog + 'bo_filter.applyFilter() ultimo record: ' + controller.getSelectedIndex(), LOGGINGLEVEL.DEBUG);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"C3FE2CEB-47E6-4C42-9BBE-607CDDD99BFF"}
 */
function resetFilter(event) {
	globals.intermediate = true;
	globals.filterIntemediate = false;
	//	application.output('RESET ' + globals.intermediate + ' ' + globals.filterIntemediate);
	pcOwnerID = globals.hasRole('Resp. Mercato') ? scopes.globals.currentUserId : (pcOwnerList.length != 0) ? pcOwnerList[0] : null;
	dateFrom = null;
	dateTo = null;
	statusBo = null;
	rif_customer = null;
	jobOrderId = null;
	profitCenterId = (pcList.length != 0) ? pcList[0] : null;
	prob = null;
	statusJo = null;
	yearBo = scopes.globals.selectedYear;
	docType = null;
	rdcJo = null;
	boID = null;
	orderType = null;
	customer = null;
	roleSelected = 1;
	//	finalCustomer = null;
	applyFilter(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"67BB24F0-C2A1-4D83-9D98-D386F80C1503"}
 */
function onLoad(event) {
	foundset.removeFoundSetFilterParam('enabled');
	elements.lSupport.visible = isController;
	elements.support.visible = isController;
}

/**
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"5E399DAC-2B64-4ED3-B665-88B8D729B9E4"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
	elements.lSupport.visible = isController;
	elements.support.visible = isController;
	applyFilter(event);
}
