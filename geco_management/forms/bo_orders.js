/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"60DCE9E6-36AE-450B-99B3-44B9FE84A335"}
 */
function updateUI(event) {
	
	var isController = globals.hasRole('Controllers')|| globals.hasRole('Orders Admin');
	//var canModify = globals.isBOOwner(scopes.globals.currentUserId, globals.bo_selected) || globals.isBOProfitCenterManager(scopes.globals.currentUserId, globals.bo_selected) || isController;
	
	elements.buttonAdd.visible = !isEditing() && isController;
	elements.buttonDelete.visible = foundset!=null && foundset.getSize()>0 && !isEditing() && isController;
	elements.ckAreAllSelected.visible = isController;
	elements.ckIsSelected.visible = isController;
	//FS
	//campi tabella ordini editabili solo dal controller o order admin
	elements.orderFields.enabled=isController;
	
//	if (globals.hasRole('Controllers')) {
//		elements.buttonAdd.visible = isEditing();
//		elements.buttonDelete.visible = isEditing();
//	} else {
//		//controller.enabled = false;
//		elements.buttonAdd.visible = false;
//		elements.buttonDelete.visible = false;
//		elements.ckAreAllSelected.visible = false;
//		elements.ckIsSelected.visible = false;
//	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"4C7E2C17-A67D-4E64-B0F1-050C77491ECE"}
 */
function newOrder(event) {
	var win = application.createWindow("Nuovo Ordine", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 450, 350);
	win.title = 'Nuovo Ordine'
	win.show(forms.dialog_new_bo_order);
}