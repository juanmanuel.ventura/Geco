/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"47C5E79A-9B9B-4895-AA16-5A020983D65B",variableType:-4}
 */
var editingStarted = false;

/**
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"AFF9845B-4142-49EB-848E-13D3FAF57AF0"}
 * @AllowToRunInFind
 */
function updateUI(event) {
	_super.updateUI(event);

	var canModify = globals.isOwnerForObject(scopes.globals.currentUserId, globals.bo_selected, 'bo') || globals.isPCManagerForObject(scopes.globals.currentUserId, globals.bo_selected, 'bo') || globals.hasRole('Controllers');

	var isEditable = forms.bo_details.isEnabled;
	var bo = forms.bo_details.foundset.getSelectedRecord();
	var jo = bo.bo_to_job_orders;
	//application.output('BO  ' + bo);
	//application.output('JO ' + jo.getSize());
	if (bo && bo.job_order_type_id == 4 && bo.bo_to_job_orders && bo.bo_to_job_orders.is_enabled == 0) {
		isEditable = true;
	}
	application.output('is_editable  ' + isEditable);

	//application.output('canModify ' + canModify + ' isEditable ' + isEditable);

	elements.buttonEdit.visible = canModify && isEditable && !isEditing();
	elements.buttonAdd.visible = canModify && isEditable && !isEditing();
	elements.buttonDelete.visible = canModify && isEditable && !isEditing();
	elements.createPlanning.visible = canModify && isEditable && !isEditing() && foundset.getSize() > 0;
	var record = foundset.getSelectedRecord();
	if (foundset && foundset.bo_details_to_profit_cost_types) {
		switchPanels(record.bo_details_to_profit_cost_types.profit_cost_acr, record.bo_details_to_profit_cost_types.profit_cost_types_id);

	} else
		switchPanels(null, null);

	if (!isEditing()) {
		foundset.sort('bo_details_to_profit_cost_types.profit_cost_acr desc, bo_details_to_profit_cost_types.profit_cost_description asc');
	}

	//se non ci sono voci di costo e ricavo, nascondo i bottoni di modifica e elimina voce di dettaglio.
	if (foundset.getSize() == 0) {
		elements.buttonEdit.visible = false;
		elements.buttonDelete.visible = false;
	}
	//application.output('update '+record.isNew())
	if (isEditing()) {
		if (record && !record.isNew() && record.bo_details_to_bo_planning.getSize() > 0) {
			elements.ProfitCostDescription.enabled = false;
			elements.realP.enabled = false;
			elements.realTM.enabled = false;
		} else if ( (record && record.isNew()) || (record && record.bo_details_to_bo_planning.getSize() == 0)) {
			elements.ProfitCostDescription.enabled = true;
			elements.descr.enabled = true;
			elements.totalAmountR.enabled = true;
			elements.realP.enabled = true;
			elements.realTM.enabled = true;
		}
	}
	forms.bo_details.updateUI(event);
}

/**
 * Handle record selected.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"6846F8BB-959D-41D0-ADA3-AB77A35052DF"}
 */
function onRecordSelection(event) {
	application.output(globals.messageLog + 'START bo_tab_list_details.onRecordSelection()', LOGGINGLEVEL.DEBUG);
	updateUI(event);
	/** @type {Boolean} */
	var afterSwitchPanelOnRealFigureNull = false;
	if (foundset && foundset.getSize() > 0) {
		var record = foundset.getSelectedRecord();
		application.output(globals.messageLog + 'bo_tab_list_details.onRecordSelection() isNew() ' + record.isNew(), LOGGINGLEVEL.DEBUG);
		//application.output(record)

		if (record.real_figure != null) {
			application.output(globals.messageLog + 'bo_tab_list_details.onRecordSelection() figura reale ' + record.real_figure + ' - ' + record.enrollment_number, LOGGINGLEVEL.DEBUG);
			user_id_er = record.real_figure + ' - ' + record.enrollment_number;
			var vname = elements.realP.getValueListName();
			application.output(globals.messageLog + 'bo_tab_list_details.onRecordSelection() valueList name ' + vname, LOGGINGLEVEL.DEBUG);
			//userCostID = globals.getCostID(foundset.real_figure, foundset.enrollment_number, foundset.days_import);
			//globals.setValuelist('userCostList', foundset.real_figure, foundset.enrollment_number);
			//setValuelist('userTmCostList', null);
		} else if (record.real_tm_figure != null) {
			user_id_er = record.real_tm_figure + ' - ' + record.enrollment_number;
			//application.output(user_id_er);
			//userCostID = globals.getCostID(foundset.real_tm_figure, foundset.enrollment_number, foundset.days_import)
			//globals.setValuelist('userCostList', foundset.real_tm_figure);
			//setValuelist('userCostList', null);
		} else if (foundset.real_figure == null) {
			//			user_id_er = !globals.isEmpty(foundset.getSelectedRecord().user_id) && !globals.isEmpty(foundset.getSelectedRecord().enrollment_number) && globals.isEmpty(foundset.getSelectedRecord().standard_figure)?foundset.getSelectedRecord().real_figure + ' - ' + foundset.getSelectedRecord().enrollment_number:'';
			user_id_er = '';
			afterSwitchPanelOnRealFigureNull = foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 9?true:false;
		}

		if (record.bo_details_to_profit_cost_types)
			switchPanels(record.bo_details_to_profit_cost_types.profit_cost_type, record.bo_details_to_profit_cost_types.profit_cost_types_id);
		else switchPanels(null, null);

		if (foundset.profit_cost_type_id == 9 || foundset.profit_cost_type_id == 5) {
			if (foundset.figure != null) {
				elements.labale_st_cost.visible = false;
				elements.standAmount.visible = false;
			} else {
				elements.labale_st_cost.visible = true;
				elements.standAmount.visible = true;
			}
		}
		
		if (afterSwitchPanelOnRealFigureNull && !editingStarted) {
			application.output('---------record_id: ' + foundset.bo_details_id);
			elements.labale_st_cost.visible = true;
			elements.standAmount.visible = true;
			elements.gg.visible = true;
		} else if (afterSwitchPanelOnRealFigureNull && editingStarted) {
			if (globals.isEmpty(foundset.real_figure) && (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 9)) {
				elements.labale_st_cost.visible = true;
				elements.standAmount.visible = true;
			}
		}
	} else switchPanels(null, null);

	application.output(globals.messageLog + ' STOP bo_tab_list_details.onRecordSelection()', LOGGINGLEVEL.DEBUG);

}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"43D76A56-EB56-4EDD-8DF7-799F7AF4CB88"}
 */
function onDataChangeType(oldValue, newValue, event) {
	var record = foundset.getSelectedRecord();
	record.real_figure = null;
	record.real_tk_supplier = null;
	record.real_tm_figure = null;
	record.standard_figure = null;
	record.standard_import = null;
	record.days_import = null;
	record.days_number = null;
	record.figure = null;
	record.cost_amount = null;
	record.return_amount = null;
	record.total_amount = null;
	record.enrollment_number = null;
	//userCostID = null;
	user_id_er = null;
	return switchPanels(record.bo_details_to_profit_cost_types.profit_cost_type, record.bo_details_to_profit_cost_types.profit_cost_types_id);
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"9B436333-73BA-446B-B659-A6A36A34C16F"}
 */
function onDataChangeStandardFigure(oldValue, newValue, event) {
	//se già presente il totale dei giorni e non è presente la figura reale modifica il totale
	if (oldValue != newValue) {
		if (!globals.isEmpty(newValue)) {
			foundset.standard_import = foundset.bo_details_to_standard_professional_figures.standard_cost;
			//FS
			//		nuovo calcolo costi standard selezionando il profilo della figura professionale
			//		var newCostProfile=foundset.standard_import = foundset.bo_details_to_view_profiles_costs_with_max_date.prof_cost_daily;
			//		application.output("NUovo costo dal profilo "+newCostProfile);

			if (foundset.days_number != null && (foundset.real_figure == null && foundset.real_tm_figure == null && foundset.days_import == null)) {
				//				JStaffa non setto la variabile days_import qui perchè al prossimo cambio della fig. professionale non viene aggiornato
				//				foundset.days_import = foundset.standard_import;

				//				Cambio la total_amount perchè non essendoci days_import devo prendere come riferimento days_number
				//				foundset.total_amount = globals.roundNumberWithDecimal( (foundset.days_import * foundset.days_number), 2);
				foundset.total_amount = globals.roundNumberWithDecimal( (foundset.standard_import * foundset.days_number), 2);
			}
			if (foundset.real_figure == null && foundset.real_tm_figure == null) {
				//				COM'ERA
				//				//se non sto inserendo una spesa, gli lascio assegnare standard_figure a figure (cioè profilo professionale = descrizione)
				//				if (foundset.profit_cost_type_id != 10) {
				//					foundset.figure = foundset.bo_details_to_standard_professional_figures.description;
				//				}
				foundset.figure = foundset.bo_details_to_standard_professional_figures.description;
			}
		} else {
			foundset.standard_import = null;
			foundset.figure = foundset.real_figure ? foundset.figure : null;
			foundset.total_amount = foundset.real_figure && foundset.days_import && foundset.days_number ? globals.roundNumberWithDecimal( (foundset.days_import * foundset.days_number), 2) : 0;
			foundset.days_import = foundset.real_figure ? foundset.days_import : null;
		}
	}
	return true;
}

///**
// * Handle changed data.
// *
// * @param {Number} oldValue old value
// * @param {Number} newValue new value
// * @param {JSEvent} event the event that triggered the action
// *
// * @returns {Boolean}
// *
// * @properties={typeid:24,uuid:"DA5CDBDE-EC39-4C98-AC29-52D93BB9B36E"}
// * @AllowToRunInFind
// */
//function onDataChangeRealFigure(oldValue, newValue, event) {
//	application.output(event);
//	application.output(elements.realTM.getValueListName())
//	if (oldValue != newValue) {
//		//se la tipologia scelta NON è SPESA, metto il total_amount a 0, altrimenti lascio il valore indicato da utente.
//		if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id != 10) {
//			foundset.total_amount = 0;
//		}
//		foundset.days_import = null;
//		//personale
//		if ( (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9)) {
//			/** @type {globals.objUserCost} */
//			var obj = globals.getUserCost(userCostID);
//			foundset.figure = obj.name;
//			foundset.days_import = obj.cost;
//			foundset.enrollment_number = obj.enrollNum;
//
//			if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10) {
//				foundset.real_figure = obj.uid;
//				if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10) foundset.days_import = null;
//			} else if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9) {
//				foundset.real_tm_figure = obj.uid;
//			}
//			//foundset.figure = foundset.bo_details_to_real_figure.users_to_contacts.real_name;
//		}
//		//		if ( (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10) &&
//		//				foundset.bo_details_to_real_figure != null && foundset.bo_details_to_real_figure.users_to_contacts.real_name != null) {
//		//			foundset.figure = foundset.bo_details_to_real_figure.users_to_contacts.real_name;
//		//			//Fornitura TM
//		//		} else if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9) {
//		//			if (foundset.bo_details_to_tm_figure != null && foundset.bo_details_to_tm_figure.users_to_contacts.real_name != null) {
//		//				foundset.figure = foundset.bo_details_to_tm_figure.users_to_contacts.real_name;
//		//			}
//		//		}
//		//		if ( (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9)) {
//		//			application.output(' ' + oldValue + ' ' + newValue)
//		//			/** @type {globals.objUserCost} */
//		//			var obj = globals.getUserCost(userCostID);
//		//			globals.setValuelist('userCostList', newValue);
//		//			if (globals.valueListDS != null && globals.valueListDS.getMaxRowIndex() != 0 && globals.valueListDS.getMaxRowIndex() == 1) {
//		//				//foundset.real_import = valueListDS.getValue(1, 1);
//		//				foundset.days_import = globals.valueListDS.getValue(1, 1);
//		//			}
//		//		}
//		if ( (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10) && (foundset.figure != null && newValue != null)) {
//			elements.labale_st_cost.visible = false;
//			elements.standAmount.visible = false;
//		} else {
//			//se non è una spesa e real_figure è null, allora ti mostro Costo Standard/gg, altrimenti no.
//			if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id != 10 && newValue == null) {
//				elements.labale_st_cost.visible = true;
//				elements.standAmount.visible = true;
//			}
//			foundset.figure = null;
//		}
//
//		if (foundset.days_number != null) foundset.total_amount = foundset.days_import * foundset.days_number;
//	}
//	return true;
//}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"B0C5A5CE-434C-4DE5-AB7B-A4BD8FB9729A"}
 */
function onDataChangeDaysNumber(oldValue, newValue, event) {
	//se presente la figura reale/standard calcola il total amount
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue);

		if (standard_import != null)
			foundset.total_amount = globals.roundNumberWithDecimal( (foundset.standard_import * foundset.days_number), 2);
		if (foundset.days_import != null)
			foundset.total_amount = globals.roundNumberWithDecimal( (foundset.days_import * foundset.days_number), 2);
		//		JStaffa analogamento a quanto fatto per JO, non assegno a days_import il valore di standard_import! Se modificato, il valore NON viene ricalcolato!
		//		if (standard_import != null && foundset.days_import == null)
		//			foundset.days_import = standard_import
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
		foundset.cost_amount = foundset.total_amount;
	}
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"BD67EACF-203C-474C-9E6E-2782D15B2189"}
 */
function onDataChangeAmountPersonnel(oldValue, newValue, event) {
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (foundset.days_number != null)
			foundset.total_amount = globals.roundNumberWithDecimal( (foundset.days_import * foundset.days_number), 2);
		//foundset.cost_amount = foundset.total_amount
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
		foundset.standard_import = null;
	}
	return true
}

///**
// * @param oldValue
// * @param newValue
// * @param event
// *
// * @properties={typeid:24,uuid:"10C0BA77-B4ED-4CDC-AF89-4A136E7B8ADA"}
// */
//function onDataChangeAmountStandard(oldValue, newValue, event) {
//	if (oldValue != newValue && newValue != null) {
//		//application.output(' ' + oldValue + ' ' + newValue)
//		if (foundset.days_number != null && (foundset.real_figure == null && foundset.real_tm_figure == null)) {
//			foundset.total_amount = globals.roundNumberWithDecimal( (foundset.standard_import * foundset.days_number), 2);
//			foundset.days_import = foundset.standard_import;
//			//globals.setValuelist('userCostList', null);
//		}
//		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
//	}
//	return true
//}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"91A23076-FEFE-4F53-8C2B-BA1DB0794B3E"}
 */
function onDataChangeTK(oldValue, newValue, event) {
	if (oldValue != newValue && newValue != null) {
		foundset.figure = foundset.bo_details_to_suppliers.company_name;
	} else if (newValue == null) {
		foundset.figure = null;
	}
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"443B9C74-DBF2-4FE3-AD83-8759EB7950A6"}
 */
function onDataChangeTotalAmount(oldValue, newValue, event) {
	if (oldValue != newValue) {
		if (foundset.bo_details_to_profit_cost_types.profit_cost_type == 'Ricavo')
			foundset.return_amount = foundset.total_amount;
		else
			foundset.cost_amount = foundset.total_amount;
	}
	return true
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"452C52BD-2982-4FD5-8FA3-DC2DE734D9E8"}
 * @AllowToRunInFind
 */
function createPlain(event) {
	application.output(globals.messageLog + 'START bo_tab_list_details.createPlain() ', LOGGINGLEVEL.INFO);
	if (foundset == null || foundset.getSize() == 0) {
		globals.DIALOGS.showErrorDialog('Errore', 'Non hai inserito voci di costo e ricavo, non è possibile pianificare', 'OK');
		return;
	}
	if (foundset != null && foundset.getSize() > 0 && (globals.isEmpty(foundset.bo_details_to_bo.start_date) || globals.isEmpty(foundset.bo_details_to_bo.end_date))) {
		globals.DIALOGS.showErrorDialog('Errore', 'Per la pianificazione devi indicare la data di inizio e di fine', 'OK');
		return;
	}
	var answer = globals.DIALOGS.showWarningDialog('Attenzione', 'Verrà sovrascritta la pianificazione esistente,\nvuoi proseguire?', 'SI', 'NO')
	//var answer = 'SI'
	if (answer == 'NO') return;

	/** @type {Array} */
	var listObjTimeSplit = globals.countDayMonthsYears(foundset.bo_details_to_bo.start_date, foundset.bo_details_to_bo.end_date);
	application.output(globals.messageLog + 'bo_tab_list_details.createPlain() numero mesi pianificazione ' + listObjTimeSplit.length, LOGGINGLEVEL.DEBUG);
	//var totMonths = listObjTimeSplit.length;
	var totDays = 0;
	//conto i mesi/gg lavorabili su cui spalmare costi e ricavi
	for (var x = 0; x < listObjTimeSplit.length; x++) {
		totDays = totDays + listObjTimeSplit[x].dayWorkable;
	}
	var lastMonth = listObjTimeSplit.length - 1;
	for (var idx = 1; idx <= foundset.getSize(); idx++) {
		var recDet = foundset.getRecord(idx);
		recDet.bo_details_to_bo_planning.deleteAllRecords();
		application.output(globals.messageLog + 'bo_tab_list_details.createPlain() Eliminati ' + recDet.bo_details_to_bo_planning.getSize(), LOGGINGLEVEL.DEBUG);
	}

	application.output(globals.messageLog + 'bo_tab_list_details.createPlain() totale gg  ' + totDays, LOGGINGLEVEL.DEBUG);
	// per ogni dettaglio della BO
	for (var index = 1; index <= foundset.getSize(); index++) {
		var record = foundset.getRecord(index);
		application.output('ID DETTAGLIO ----------------------------------' + record.bo_details_id, LOGGINGLEVEL.DEBUG);
		var totAmInserted = 0;
		var totDaysIserted = 0;
		// per ogni mese di durata della BO
		for (var i = 0; i < listObjTimeSplit.length; i++) {
			application.output('**** i ' + i + ' lastMonth ' + lastMonth, LOGGINGLEVEL.DEBUG)
			application.output('**** mese anno ' + listObjTimeSplit[i].monthDet + '-' + listObjTimeSplit[i].yearDet, LOGGINGLEVEL.DEBUG)

			var dayAmount = globals.roundNumberWithDecimal( (record.total_amount / totDays), 2);
			application.output(record.days_number)
			var dayNumber = null;
			if (record.bo_details_to_profit_cost_types.profit_cost_acr == 'C')
				dayNumber = globals.roundNumberWithDecimal( ( (record.days_number / totDays) * listObjTimeSplit[i].dayWorkable), 2);
			application.output('**** calcolati arrotondati giorni mensili ' + dayNumber, LOGGINGLEVEL.DEBUG)
			application.output('**** importo giornaliero ' + dayAmount, LOGGINGLEVEL.DEBUG);

			//numero di piani trovati nel DB
			var totDetailPlain = 0;
			var totAm = 0;
			// controllo che non esista già una pianificazione di dettaglio
			if (record.bo_details_to_bo_planning.find()) {
				record.bo_details_to_bo_planning.bo_details_id = record.bo_details_id;
				record.bo_details_to_bo_planning.bo_id = record.bo_id;
				record.bo_details_to_bo_planning.bo_month = listObjTimeSplit[i].monthDet;
				record.bo_details_to_bo_planning.bo_year = listObjTimeSplit[i].yearDet;
				totDetailPlain = record.bo_details_to_bo_planning.search();
			}
			// se non esiste una pianificazione mensile per il dettaglio la creo
			if (totDetailPlain == 0) {
				record.bo_details_to_bo_planning.newRecord();
				record.bo_details_to_bo_planning.bo_details_id = record.bo_details_id;
				record.bo_details_to_bo_planning.bo_id = record.bo_id;
				record.bo_details_to_bo_planning.bo_month = listObjTimeSplit[i].monthDet;
				record.bo_details_to_bo_planning.bo_year = listObjTimeSplit[i].yearDet;
				record.bo_details_to_bo_planning.days_import = record.days_import;
				application.output('**** creo piano days_import ' + record.bo_details_to_bo_planning.days_import, LOGGINGLEVEL.DEBUG);
				//se ultimo mese inserisco l'avanzo
				if (i == lastMonth) {

					dayNumber = globals.roundNumberWithDecimal( (record.days_number - totDaysIserted), 2);
					application.output('**** Nuovo record dayNumber ' + dayNumber + ' giorni inseriti ' + totDaysIserted + ' totale gg ' + record.days_number, LOGGINGLEVEL.DEBUG)
				}
				record.bo_details_to_bo_planning.days_number = dayNumber != 0 ? dayNumber : null;

				if (record.days_import != null)

					totAm = globals.roundNumberWithDecimal( (dayNumber * record.days_import), 2);
				else

					totAm = globals.roundNumberWithDecimal( ( (i == lastMonth) ? (record.total_amount - totAmInserted) : (dayAmount * listObjTimeSplit[i].dayWorkable)), 2);
				application.output('**** Nuovo record totAm ' + totAm + ' inserito ' + totAmInserted + ' totale ' + record.total_amount, LOGGINGLEVEL.DEBUG);
				record.bo_details_to_bo_planning.total_amount = totAm;
				if (record.bo_details_to_profit_cost_types.profit_cost_acr == 'R')
					record.bo_details_to_bo_planning.return_amount = totAm;
				else
					record.bo_details_to_bo_planning.cost_amount = totAm;
			}
			//altrimenti modifico l'importo
			else {
				//se ultimo mese inserisco l'avanzo
				if (i == lastMonth) {
					dayNumber = globals.roundNumberWithDecimal( (record.days_number - totDaysIserted), 2);
					application.output('**** Modifica record dayNumber ' + dayNumber + ' giorni inseriti ' + totDaysIserted + ' totale gg ' + record.days_number, LOGGINGLEVEL.DEBUG)
				}
				record.bo_details_to_bo_planning.days_number = (dayNumber != 0) ? dayNumber : null;
				if (record.days_import != null)
					totAm = globals.roundNumberWithDecimal( (dayNumber * record.days_import), 2);
				else
					totAm = globals.roundNumberWithDecimal( ( (i == lastMonth) ? (record.total_amount - totAmInserted) : (dayAmount * listObjTimeSplit[i].dayWorkable)), 2);
				application.output('**** Modifica record totAm ' + totAm + ' inserito ' + totAmInserted + ' totale ' + record.total_amount, LOGGINGLEVEL.DEBUG)
				record.bo_details_to_bo_planning.total_amount = totAm;
				record.bo_details_to_bo_planning.days_import = record.days_import
				application.output('**** modificato piano days_import ' + record.bo_details_to_bo_planning.days_import, LOGGINGLEVEL.DEBUG);
				if (record.bo_details_to_profit_cost_types.profit_cost_acr == 'R')
					record.bo_details_to_bo_planning.return_amount = totAm;
				else
					record.bo_details_to_bo_planning.cost_amount = totAm;
			}
			//fine if-else
			totAmInserted = totAmInserted + totAm;
			totDaysIserted = totDaysIserted + dayNumber;
			application.output('**** totAmInserted ' + totAmInserted + ' totDaysIserted ' + totDaysIserted, LOGGINGLEVEL.DEBUG)
		}
		//fine for mesi durata BO
		databaseManager.saveData(record.bo_details_to_bo_planning);
		record.bo_details_to_bo_planning.loadRecords();
		application.output('FINE DETTAGLIO--------------------------------------------', LOGGINGLEVEL.DEBUG)
	}
	databaseManager.saveData(foundset.bo_details_to_bo_planning);
	foundset.loadRecords();
	application.output(foundset)
	application.output(globals.messageLog + 'STOP bo_tab_list_details.createPlain() ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @private
 *
 * @properties={typeid:24,uuid:"D59B90BB-E314-4329-A90B-CD52CB306868"}
 * @AllowToRunInFind
 */
function deleteRecord(event, index) {
	application.output(globals.messageLog + 'START bo_tab_list_details.deleteRecord() ', LOGGINGLEVEL.INFO);
	var record = foundset.getSelectedRecord();
	var listToDelete = [];
	var answer = globals.DIALOGS.showWarningDialog("Conferma", "Se il dettaglio ha una pianificazione associata, questa verrà cancellata.\nQuesta operazione non può essere annullata", "Elimina", "Annulla");
	if (answer == "Elimina") {
		application.output(globals.messageLog + 'bo_tab_list_details.deleteRecord() confermata cancelazione record ', LOGGINGLEVEL.INFO);
		var tot = 0;

		/** @type {JSFoundSet<db:/geco/bo_planning>} */
		var bo_planning = databaseManager.getFoundSet('geco', 'bo_planning');
		bo_planning.loadAllRecords();
		if (bo_planning.find()) {
			bo_planning.bo_details_id = record.bo_details_id;
			tot = bo_planning.search();
			application.output(tot)
		}
		for (var i = 1; i <= bo_planning.getSize(); i++) {
			var rec = bo_planning.getRecord(i);
			application.output(rec);
			listToDelete.push(rec);
		}

		for (var t = 0; t < listToDelete.length; t++) {
			/** @type {JSRecord<db:/geco/bo_planning>} */
			var rec3 = listToDelete[t];
			bo_planning.deleteRecord(rec3);
		}

		databaseManager.saveData(bo_planning);
		//if (deleted)
		try {
			var deleted = foundset.deleteRecord(record);
			application.output(globals.messageLog + 'bo_tab_list_details.deleteRecord() Record cancellato ' + deleted, LOGGINGLEVEL.INFO);
		} catch (e) {
			application.output(globals.messageLog + 'bo_tab_list_details.deleteRecord() ERRORE GESTITO ', LOGGINGLEVEL.INFO)
		}

		//application.output(globals.messageLog + 'STOP bo_tab_list_details.deleteRecord() ' + deleted, LOGGINGLEVEL.INFO);
		updateUI(event);
		foundset.loadRecords();
	} else {
		application.output(globals.messageLog + 'STOP bo_tab_list_details.deleteRecord() NON confermata cancelazione record ', LOGGINGLEVEL.INFO);
		return;
	}
}

/**
 * Handle changed data.
 *
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"0A0ECC8F-60DA-45B8-9CCF-C44183E4F8BB"}
 * @AllowToRunInFind
 */
function onDataChangeRealFigureNew(oldValue, newValue, event) {

	application.output(event);
	application.output(newValue)
	var ar = newValue != null ? newValue.split(' - ') : null;
	application.output(ar);

	//application.output(elements.realTM.getValueListName())
	if (oldValue != newValue) {
		//		//se la tipologia scelta NON è SPESA, metto il total_amount a 0, altrimenti lascio il valore indicato da utente.
		//		if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id != 10) {
		//			foundset.total_amount = 0;
		//		}
		//		foundset.days_import = null;
		//personale
		if (globals.isEmpty(newValue)) {
			foundset.real_figure = null;
			foundset.enrollment_number = null;
			foundset.days_import = null;
			foundset.days_number = foundset.days_number ? foundset.days_number : null;
			foundset.figure = foundset.standard_figure ? foundset.bo_details_to_standard_professional_figures.description : null;
			foundset.total_amount = foundset.standard_figure && foundset.standard_import && foundset.days_number ? globals.roundNumberWithDecimal( (foundset.standard_import * foundset.days_number), 2) : null;
			updateUI(event);
		} else {
			if ( (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9 && newValue != null)) {
				/** @type {globals.objUserCost} */
				var obj = globals.getLastUserCost(ar[0], ar[1]);
				application.output(obj);
				//FS
				//al cambio della persona inserire nel campo(per ora) realAmount il costo standard associato alla persona
				//getStandardCostByUserId(ar[0]);

				foundset.days_import = obj.cost;
				foundset.enrollment_number = ar[1];
				foundset.figure = obj.name;

				if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10) {
					foundset.real_figure = ar[0];
					if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10) foundset.days_import = null;
				} else if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9) {
					foundset.real_tm_figure = ar[0];
				}
			}

			if ( (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10) && (foundset.figure != null && newValue != null)) {
				elements.labale_st_cost.visible = false;
				elements.standAmount.visible = false;
			} else {
				//se non è una spesa e real_figure è null, allora ti mostro Costo Standard/gg, altrimenti no.
				if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id != 10 && newValue == null) {
					elements.labale_st_cost.visible = true;
					elements.standAmount.visible = true;
				}
				foundset.figure = null;
			}

			if (foundset.days_number != null) foundset.total_amount = globals.roundNumberWithDecimal( (foundset.days_import * foundset.days_number), 2);
		}
	}
	return true;
}
/**
 * Funzione che recupera l'ultimo profilo del costo standard associato all'id dell'utente
 * @param{String} userId
 * @return {Number} userProfileCost
 * @properties={typeid:24,uuid:"0B0A2C23-EB14-4F29-9299-EA3C8BB8C843"}
 * @AllowToRunInFind
 */
function getStandardCostByUserId(userId) {
	/** @type {Number} */
	var userIdAsNumber = userId;
	application.output("START bo_tab_list_details.getStandardCostByUserId ");
	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	if (users.find()) {
		users.user_id = userIdAsNumber;
		var tot = users.search();
	}
	var userProfileCost = 0;
	if (tot > 0) {
		userProfileCost = users.users_to_view_users_profiles_with_max_date.prof_cost_daily;
	}
	application.output("Costo standard associato a " + userId + " : " + userProfileCost);

	return userProfileCost;
}
