/**
 * @type {Number}
 * @properties={typeid:35,uuid:"1EF6948A-B229-48F6-B6D4-FCF36002C31F",variableType:4}
 */
//var standard_amount = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"56FFEBFD-F611-4596-86CD-AB45B02715D6",variableType:4}
 */
//var real_amount_personnel = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"16196037-3E52-4D62-A0C4-D920D5C24709",variableType:4}
 */
//var day_number = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F864C4C7-4D16-4598-9C31-F0529E12B572",variableType:8}
 */
//var day_import = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"12D75057-2191-47F6-8003-BECA0118AA6A",variableType:4}
 */
//var total_amount_value = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"D15877A7-9E31-462A-994A-63A00DF8F336",variableType:8}
 */
//var userCostID = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"3D116E2C-CC11-40FD-84EA-D27C54398B59"}
 */
var user_id_er = null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 * @properties={typeid:24,uuid:"F7756F99-E1F1-4381-BC27-238B9AF91CF7"}
 */
function onDataChangeType(oldValue, newValue, event) {
	//userCostID = null
	user_id_er = null;
	return switchPanels();

}

/**
 * @return {Boolean}
 * @properties={typeid:24,uuid:"8ECEDE2D-598C-46BF-8749-F93CC154CC47"}
 */
function switchPanels() {
	if (foundset.profit_cost_type_id == null) {
		elements.groupGeneric.visible = false;
		elements.groupRealP.visible = false;
		elements.groupRealTK.visible = false;
		elements.groupRealTM.visible = false;
		elements.groupDaysImport.visible = false;
		return true;
	}
	if (foundset.profit_cost_type_id != null) {
		if (foundset.bo_details_to_profit_cost_types.profit_cost_acr == 'R') {

			elements.groupGeneric.visible = false;
			elements.groupRealP.visible = false;
			elements.groupRealTK.visible = false;
			elements.groupRealTM.visible = false;
			elements.groupDaysImport.visible = false;

		} else {
			//personale
			if (foundset.profit_cost_type_id == 5 ) {
				elements.groupGeneric.visible = true;
				elements.groupRealP.visible = true;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = true;
			}
			// fornitura TK
			else if (foundset.profit_cost_type_id == 8) {
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = true;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
			}
			// fornitura TM
			else if (foundset.profit_cost_type_id == 9) {
				elements.groupGeneric.visible = true;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = true;
				elements.groupDaysImport.visible = true;
			}
			else if (foundset.profit_cost_type_id == 10) {
				elements.groupGeneric.visible = true;
				elements.groupRealP.visible = true;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
			}
			else {
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
			}
		}
	}
	return true;
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"6A570F3B-08B4-4BBC-B553-2F939F39A4B0"}
 */
function onDataChangeStandardFigure(oldValue, newValue, event) {
	//se già presente il totale dei giorni e non è presente la figura reale modifica il totale
	if (oldValue != newValue && newValue != null) {
		foundset.standard_import = foundset.bo_details_to_standard_professional_figures.standard_cost;
		if (foundset.days_number != null && (foundset.real_figure == null && foundset.real_tm_figure == null && foundset.days_import == null)) {
			foundset.days_import = foundset.standard_import
			foundset.total_amount = foundset.days_import * foundset.days_number;
		}
		if (foundset.real_figure == null && foundset.real_tm_figure == null)
			foundset.figure = foundset.bo_details_to_standard_professional_figures.description;
	}

	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"5F4B2629-91FA-4B61-8B0F-260FFF959BC5"}
 * @AllowToRunInFind
 */
//function onDataChangeRealFigure(oldValue, newValue, event) {
//	if (oldValue != newValue) {
//		foundset.total_amount = 0;
//		foundset.days_import = null;
//		
//		if ( (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10 || 
//		foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9)) {
//		/** @type {globals.objUserCost} */
//		var obj = globals.getUserCost(userCostID);
//		foundset.figure = obj.name;
//		foundset.days_import = obj.cost;
//		foundset.enrollment_number = obj.enrollNum;
//		
//		if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10){
//			foundset.real_figure = obj.uid;
//			if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10) foundset.days_import = null;
//		}
//		else if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9){
//			foundset.real_tm_figure = obj.uid;
//		}
//		//foundset.figure = foundset.bo_details_to_real_figure.users_to_contacts.real_name;
//	}
//		//personale
//		if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5)
//			foundset.figure = foundset.bo_details_to_real_figure.users_to_contacts.real_name;
//		//Fornitura TM
//		if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9)
//			foundset.figure = foundset.bo_details_to_tm_figure.users_to_contacts.real_name;
//		//application.output(' ' + oldValue + ' ' + newValue)
//		globals.setValuelist('userCostList', newValue);
//		if (globals.valueListDS.getMaxRowIndex() == 1) {
//			//foundset.real_import = valueListDS.getValue(1, 1);
//			foundset.days_import = globals.valueListDS.getValue(1, 1);
//		}
//
//		if (foundset.days_number != null) foundset.total_amount = foundset.days_import * foundset.days_number;
//		//application.output('foundset.total_amount --- ' + foundset.total_amount);
//	}
//	return true
//}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"FC8AFB1B-5DD3-43F4-BF30-DAD00B7BA369"}
 */
function onDataChangeDaysNumber(oldValue, newValue, event) {
	//se presente la figura reale/standard calcola il total amount
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (standard_import != null)
			foundset.total_amount = foundset.standard_import * foundset.days_number;
		if (foundset.days_import != null)
			foundset.total_amount = foundset.days_import * foundset.days_number;
		if (standard_import != null && foundset.days_import == null) 
			foundset.days_import = standard_import
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
	}
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"86109500-8422-4BD4-AC6F-61778376B72F"}
 */
function onDataChangeAmountPersonnel(oldValue, newValue, event) {
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (foundset.days_number != null)
			foundset.total_amount = foundset.days_import * foundset.days_number;
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
	}
	return true
}


/**
 * @param oldValue
 * @param newValue
 * @param event
 *
 * @properties={typeid:24,uuid:"01FA2B0F-1DB5-4F08-91FE-2507267BE7CF"}
 */
function onDataChangeAmountStandard(oldValue, newValue, event) {
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (foundset.days_number != null && (foundset.real_figure == null && foundset.real_tm_figure == null)) {
			foundset.total_amount = foundset.standard_import * foundset.days_number;
			foundset.days_import = foundset.standard_import;
			//globals.setValuelist('userCostList', null);
		}
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
	}
	return true
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"2325FD0C-B9A9-4F36-A03A-D025F1865C8E"}
 */
function onShow(event) {
	_super.newRecord(event);
	foundset.bo_id = globals.bo_selected;
	switchPanels();
}

/**
 * @param {JSEvent} event the event that triggered the actio
 *
 * @properties={typeid:24,uuid:"ACFD1431-9E71-48F5-801A-E01BE8419019"}
 */
function updateUI(event){
	controller.readOnly = !isEditing();

	// buttons
	elements.buttonEdit.visible = !isEditing();
	elements.buttonSave.visible = isEditing();
	elements.buttonCancel.visible = isEditing();
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"1534CAF0-8879-454A-83E4-FDB9334B02D3"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
	 var saved = _super.saveEdits(event);
	 if(saved) {
		 user_id_er = null;
		 controller.getWindow().destroy();
		 forms[scopes.globals.callerForm].updateUI(event);
	 }
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"062911E8-4168-40D2-9712-5AB479434C43"}
 */
function clearNewRecord(event) {
	_super.stopEditing(event);
	 user_id_er = null;
	if (controller.getWindow())
		controller.getWindow().destroy();
}

/**
 * @properties={typeid:24,uuid:"E6D278F0-8A45-40D3-9C10-6E6E8536D804"}
 */
function clearField(){
//	standard_amount = 0;
//	real_amount_personnel = 0;
//	//day_number = 0;
//	day_import = 0;
//	total_amount_value = 0;
//	user_id_er = null;
//	globals.profit_cost_id = null ;
//	globals.standard_figure = null
//	globals.real_personnel = null;
//	globals.real_TM = null;
//	globals.real_TK = null;
}

/**
 * Handle hide window.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"688A4B70-0057-43E2-B848-B4B45A6AF91A"}
 */
function onHide(event) {
	application.output(globals.messageLog + 'START bo_new_detail_popup.onHide isEditing() ' + isEditing(),LOGGINGLEVEL.DEBUG);
	clearField();
	//_super.stopEditing(event);
	if (controller.getWindow())
		controller.getWindow().destroy();
}

/**
 * Handle changed data.
 *
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"8E7B9797-71F7-4049-B6EE-5CF562DCD181"}
 * @AllowToRunInFind
 */
function onDataChangeRealFigureNew(oldValue, newValue, event) {
	application.output(globals.messageLog + 'START bo_new_detail_popup.onDataChangeRealFigureNew() ',LOGGINGLEVEL.DEBUG);
	var ar = newValue.split(' - ');
	
	//application.output(elements.realTM.getValueListName())
	if (oldValue != newValue) {
		//se la tipologia scelta NON è SPESA, metto il total_amount a 0, altrimenti lascio il valore indicato da utente.
		if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id != 10) {
			foundset.total_amount = 0;
		}
		foundset.days_import = null;
		//personale
		if ( (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9)) {
			/** @type {globals.objUserCost} */
			var obj = globals.getLastUserCost(ar[0], ar[1]);
			application.output(globals.messageLog + 'bo_new_detail_popup.onDataChangeRealFigureNew() object found  ' +obj,LOGGINGLEVEL.DEBUG);
			foundset.days_import = obj.cost;
			foundset.enrollment_number = ar[1];
			foundset.figure = obj.name;

			if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10) {
				foundset.real_figure = ar[0];
				if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10) foundset.days_import = null;
			} else if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9) {
				foundset.real_tm_figure = ar[0];
			}
		}

		if ( (foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 9 || foundset.bo_details_to_profit_cost_types.profit_cost_types_id == 10) && (foundset.figure != null && newValue != null)) {
			elements.label_st_cost.visible = false;
			elements.standAmount.visible = false;
		} else {
			//se non è una spesa e real_figure è null, allora ti mostro Costo Standard/gg, altrimenti no.
			if (foundset.bo_details_to_profit_cost_types.profit_cost_types_id != 10 && newValue == null) {
				elements.label_st_cost.visible = true;
				elements.standAmount.visible = true;
			}
			foundset.figure = null;
		}

		if (foundset.days_number != null) foundset.total_amount = foundset.days_import * foundset.days_number;
	}
	application.output(globals.messageLog + 'STOP bo_new_detail_popup.onDataChangeRealFigureNew() ',LOGGINGLEVEL.DEBUG);
	return true;
}
