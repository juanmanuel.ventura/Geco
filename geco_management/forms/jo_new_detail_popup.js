/**
 * @type {String}
 * @properties={typeid:35,uuid:"DDA6A816-0097-4CF4-8F18-5A928595CDFB"}
 */
var user_id_er = null;

/**
 * @properties={typeid:35,uuid:"4F25F6A8-FB23-4259-9D17-9CF4E304C1E0",variableType:-4}
 */
var isController = (globals.hasRole('Controllers') || scopes.globals.hasRole('Orders Admin'));
/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 * @properties={typeid:24,uuid:"1A59519A-EA99-4624-B812-CC5B894F7E84"}
 */
function onDataChangeType(oldValue, newValue, event) {
	
	//userCostID = null;
	real_figure = null;
	real_tk_supplier = null;
	real_tm_figure = null;
	standard_figure = null;
	standard_import = null;
	days_import = null;
	days_number = null;
	description = null;
	cost_amount = null;
	return_amount = null;
	total_amount = null;
	enrollment_number = null;
	user_id_er = null;
	return switchPanels();

}

/**
 * @return {Boolean}
 * @properties={typeid:24,uuid:"A9EFE129-A02D-4605-84FB-8B2256269E6F"}
 */
function switchPanels() {
	if (foundset.profit_cost_type_id == null) {
		elements.groupGeneric.visible = false;
		elements.groupRealP.visible = false;
		elements.groupRealTK.visible = false;
		elements.groupRealTM.visible = false;
		elements.groupDaysImport.visible = false;
		return false;
	}
	if (foundset.profit_cost_type_id != null) {
		if (foundset.jo_details_to_profit_cost_types.profit_cost_acr == 'R') {

			elements.groupGeneric.visible = false;
			elements.groupRealP.visible = false;
			elements.groupRealTK.visible = false;
			elements.groupRealTM.visible = false;
			elements.groupDaysImport.visible = false;

		} else {
			//personale
			if (foundset.profit_cost_type_id == 5) {
				elements.groupGeneric.visible = true;
				elements.groupRealP.visible = true;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = true;
			}
			// fornitura TK
			else if (foundset.profit_cost_type_id == 8) {
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = true;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
			}
			// fornitura TM
			else if (foundset.profit_cost_type_id == 9) {
				elements.groupGeneric.visible = true;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = true;
				elements.groupDaysImport.visible = true;
			} else if (foundset.profit_cost_type_id == 10) {
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = true;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
			} else {
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
			}
		}
	}
	return true;
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"C8BC028C-71BA-4BA9-B2D4-5E4E7B405ACF"}
 */
function onDataChangeStandardFigure(oldValue, newValue, event) {
	//se già presente il totale dei giorni e non è presente la figura reale modifica il totale
	if (oldValue != newValue && newValue != null) {
		foundset.standard_import = foundset.jo_details_to_standard_professional_figures.standard_cost;
		if (foundset.days_number != null && (foundset.real_figure == null && foundset.real_tm_figure == null && foundset.days_import == null)) {
			foundset.days_import = foundset.standard_import
			foundset.total_amount = foundset.days_import * foundset.days_number;
		}
		if (foundset.real_figure == null && foundset.real_tm_figure == null)
			foundset.description = foundset.jo_details_to_standard_professional_figures.description;
	}

	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"C0C4934E-0219-4CA5-828C-90D086B2028B"}
 * @AllowToRunInFind
 */
//function onDataChangeRealFigure(oldValue, newValue, event) {
//	if (oldValue != newValue) {
//		foundset.total_amount = 0;
//		foundset.days_import = null;
//
//		if ( (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 9)) {
//			/** @type {globals.objUserCost} */
//			var obj = globals.getUserCost(userCostID);
//			foundset.description = obj.name;
//			foundset.days_import = obj.cost;
//			foundset.enrollment_number = obj.enrollNum;
//
//			if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10) {
//				foundset.real_figure = obj.uid;
//				if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10) foundset.days_import = null;
//			} else if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 9) {
//				foundset.real_tm_figure = obj.uid;
//			}
//		}
//
//		//		//personale
//		//		if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 5)
//		//			foundset.description = foundset.jo_details_to_real_figure.users_to_contacts.real_name;
//		//		//Fornitura TM
//		//		if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 9)
//		//			foundset.description = foundset.jo_details_to_tm_figure.users_to_contacts.real_name;
//		//		//application.output(' ' + oldValue + ' ' + newValue)
//		//		globals.setValuelist('userCostList', newValue);
//		//		if (globals.valueListDS.getMaxRowIndex() == 1) {
//		//			//foundset.real_import = valueListDS.getValue(1, 1);
//		//			foundset.days_import = globals.valueListDS.getValue(1, 1);
//		//		}
//
//		if (foundset.days_number != null) foundset.total_amount = foundset.days_import * foundset.days_number;
//		//application.output('foundset.total_amount --- ' + foundset.total_amount);
//	}
//	return true
//}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"47C1981A-2FF7-4B64-8985-3743D60D1FC2"}
 */
function onDataChangeDaysNumber(oldValue, newValue, event) {
	//se presente la figura reale/standard calcola il total amount
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (standard_import != null)
			foundset.total_amount = foundset.standard_import * foundset.days_number;
		if (foundset.days_import != null)
			foundset.total_amount = foundset.days_import * foundset.days_number;
		if (standard_import != null && foundset.days_import == null)
			foundset.days_import = standard_import
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
	}
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"4606B627-CF4E-49D7-BB04-5B3D6152A064"}
 */
function onDataChangeAmountPersonnel(oldValue, newValue, event) {
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (foundset.days_number != null)
			foundset.total_amount = foundset.days_import * foundset.days_number;
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
	}
	return true
}

/**
 * @param oldValue
 * @param newValue
 * @param event
 *
 * @properties={typeid:24,uuid:"AB9DC973-4A1E-44A1-94B3-4EF7449C806B"}
 */
function onDataChangeAmountStandard(oldValue, newValue, event) {
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (foundset.days_number != null && (foundset.real_figure == null && foundset.real_tm_figure == null)) {
			foundset.total_amount = foundset.standard_import * foundset.days_number;
			foundset.days_import = foundset.standard_import;
			//globals.setValuelist('userCostList', null);
		}
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
	}
	return true
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"E80B2004-12C7-4160-A074-2C1FA0451ED6"}
 */
function onShow(event) {
	application.output(globals.messageLog + 'START jo_new_detail_popup.onShow() ', LOGGINGLEVEL.INFO);
	_super.newRecord(event);
	foundset.jo_id = globals.job_order_selected;
	application.output(globals.messageLog + 'jo_new_detail_popup.onShow() globals.job_order_selected '+ foundset.jo_id, LOGGINGLEVEL.DEBUG);
	switchPanels();
	application.output(globals.messageLog + 'STOP jo_new_detail_popup.onShow() ', LOGGINGLEVEL.INFO);
}

/**
 * @param {JSEvent} event the event that triggered the actio
 *
 * @properties={typeid:24,uuid:"C057E579-8B98-4D5F-B2B8-10898A2D689E"}
 */
function updateUI(event) {
	controller.readOnly = !isEditing();

	// buttons
	elements.buttonEdit.visible = !isEditing();
	elements.buttonSave.visible = isEditing();
	elements.buttonCancel.visible = isEditing();

	elements.ProfitCostDescription.visible = !isController;
	elements.ProfitCostDescriptionController.visible = isController;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"7D24F35F-3EF1-464D-B825-7A8B8B8DA469"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
	var saved = _super.saveEdits(event);
	if (saved) {
		controller.getWindow().destroy();
		forms[scopes.globals.callerForm].updateUI(event);
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B434A568-298E-457F-9B3A-96331F0571FE"}
 */
function clearNewRecord(event) {
	//	clearField();
	_super.stopEditing(event);
	if (controller.getWindow())
		controller.getWindow().destroy();
}

/**
 * @properties={typeid:24,uuid:"6D365ACE-18C0-4B38-B049-C0F983EB91EC"}
 */
function clearField() {
	//standard_amount = 0;
//	real_amount_personnel = 0;
//	day_number = 0;
//	day_import = 0;
//	total_amount_value = 0;
//	globals.profit_cost_id = null;
//	globals.standard_figure = null
//	globals.real_personnel = null;
//	globals.real_TM = null;
//	globals.real_TK = null;
}

/**
 * Handle hide window.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"F9D3FEE4-F329-4C5C-A8D5-1B6851802DB8"}
 */
function onHide(event) {
	clearField();
	databaseManager.setAutoSave(true);
	if (controller.getWindow())
		controller.getWindow().destroy();
}

/**
 * Handle changed data.
 *
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"C4F89378-C989-4E97-849B-3AACBD84C799"}
 * @AllowToRunInFind
 */
function onDataChangeRealFigureNew(oldValue, newValue, event) {
//	application.output(event);
//	application.output(newValue)
	var ar = newValue.split(' - ');
//	application.output(ar);
	
	//application.output(elements.realTM.getValueListName())
	if (oldValue != newValue) {
		//se la tipologia scelta NON è SPESA, metto il total_amount a 0, altrimenti lascio il valore indicato da utente.
		if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id != 10) {
			foundset.total_amount = 0;
		}
		foundset.days_import = null;
		//personale
		if ( (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 9)) {
			/** @type {globals.objUserCost} */
			var obj = globals.getLastUserCost(ar[0], ar[1]);
//			application.output(obj);
			foundset.days_import = obj.cost;
			foundset.enrollment_number = ar[1];
			foundset.description = obj.name;

			if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10) {
				foundset.real_figure = ar[0];
				if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10) foundset.days_import = null;
			} else if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 9) {
				foundset.real_tm_figure = ar[0];
			}
		}

		if ( (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 9 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10) && (foundset.description != null && newValue != null)) {
			elements.label_st_cost.visible = false;
			elements.standAmount.visible = false;
		} else {
			//se non è una spesa e real_figure è null, allora ti mostro Costo Standard/gg, altrimenti no.
			if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id != 10 && newValue == null) {
				elements.label_st_cost.visible = true;
				elements.standAmount.visible = true;
			}
			foundset.description = null;
		}

		if (foundset.days_number != null) foundset.total_amount = foundset.days_import * foundset.days_number;
	}
	return true;
}
