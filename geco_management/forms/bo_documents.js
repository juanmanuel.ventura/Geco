/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A8DA5489-69AF-42F0-BB45-619587A7E688"}
 */
function newDocument(event) {
	var win = application.createWindow("Nuovo Documento", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 450, 300);
	win.title = 'Nuovo Documento'
	win.show(forms.dialog_import_bo_document);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"A40D483C-43C7-41C6-B929-96A40B6A50B2"}
 */
function downloadFile(event) {
	plugins.file.writeFile(foundset.bo_bo_documents_to_bo_documents.bo_document_original_name, foundset.bo_bo_documents_to_bo_documents.bo_document, foundset.bo_bo_documents_to_bo_documents.bo_document_mimetype)
}

/**
 * @param {JSEvent}  event
 *
 * @properties={typeid:24,uuid:"16CDACDA-F0D7-4964-9113-31DE525B5435"}
 * @AllowToRunInFind
 */
function updateUI(event) {
	var isController = globals.hasRole('Controllers') || globals.hasRole('Orders Admin');
	var canModify = globals.isOwnerForObject(scopes.globals.currentUserId, globals.bo_selected, 'bo') || globals.isPCManagerForObject(scopes.globals.currentUserId, globals.bo_selected, 'bo') || isController;

	elements.buttonAdd.visible = !isEditing() && canModify;
	elements.buttonDelete.visible = !isEditing() && canModify && foundset != null && foundset.getSize() > 0;
	elements.buttonDelete.enabled = (foundset.is_eraseable == 1) || isController;

	//	if(!globals.isBOOwner(scopes.globals.currentUserId,globals.bo_selected) && !globals.isBOProfitCenterManager(scopes.globals.currentUserId,globals.bo_selected)){
	//		elements.buttonAdd.visible = false;
	//		elements.buttonDelete.visible = false;
	//	}

}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @private
 *
 * @properties={typeid:24,uuid:"79FE580D-A768-4313-879B-7F29381BE787"}
 * @AllowToRunInFind
 */
function deleteDocument(event, index) {
	if (foundset.is_eraseable == 1 || (globals.hasRole('Controllers') || globals.hasRole('Orders Admin'))) {
		application.output(globals.messageLog + 'START bo_documents.deleteDocument() ', LOGGINGLEVEL.DEBUG);
		var docId = foundset.bo_document_id;

		application.output('ID DOC ' + docId);
		//cancella dalla tabella di relazione bo_bo_documents
		var deleted = _super.deleteRecord(event, index);
		application.output(globals.messageLog + 'bo_documents.deleteDocument() bo_bo_document cancellato bo_document_id ' + docId + ', esito ' + deleted, LOGGINGLEVEL.DEBUG);
		
		if (!deleted) return;
		if (deleted) {
			//cerca i documenti (BLOB) nella tabella bo_document
			/** @type {JSFoundSet<db:/geco/bo_documents>} */
			var bo_documents = databaseManager.getFoundSet('geco', 'bo_documents');
			var totDoc = 0;
			if (bo_documents.find()) {
				bo_documents.bo_document_id = docId;
				totDoc = bo_documents.search();
			}
			application.output('tot doc trovati ' + totDoc);
			for (var i = 1; i <= totDoc; i++) {
				var record = bo_documents.getRecord(i);
				application.output('record da cancellare ' + record.bo_document_original_name);
				var delDoc = bo_documents.deleteRecord(record);
				application.output(globals.messageLog + 'bo_documents.deleteDocument() bo_document cancellato ' + docId + ', esito ' + delDoc, LOGGINGLEVEL.DEBUG);
			}
	}
		application.output(globals.messageLog + 'STOP bo_documents.deleteDocument() ', LOGGINGLEVEL.DEBUG);
	}
}
