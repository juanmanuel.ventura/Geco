/**
 * @properties={typeid:35,uuid:"F3C6476E-B37A-4F2E-A093-55ABC253576C",variableType:-4}
 */
var pcList = scopes.globals.getProfitCenterByUserManager(scopes.globals.currentUserId).length > 0 ? scopes.globals.getProfitCenterByUserManager(scopes.globals.currentUserId) : null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"EAAFA156-9116-4931-A724-16688FBF2C18",variableType:8}
 */
var profCenter = pcList ? pcList[0] : null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"66FE9747-8711-427A-B8CE-955959EE39C0",variableType:8}
 */
var commSupp = null;

/**
 *@type {Number}
 * @properties={typeid:35,uuid:"5899A616-2221-46F7-81C4-7EB0B08FA0FF",variableType:8}
 */
var userId = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"0236CA5F-E154-4818-BFBD-1DC1C4BD8302"}
 */
function updateUI(event) {
	elements.buttonAdd.visible = !isEditing() && (profCenter != null && commSupp != null);
	elements.buttonDelete.visible = !isEditing() && ( (commSupp != 0 && commSupp != null) && (foundset.getSize() > 0) ? true : false);
	elements.buttonSave.visible = isEditing();
	elements.buttonCancel.visible = isEditing();
	elements.fldCustomer.editable = isEditing();

	//	elements.ckIsSelected.enabled = !isEditing() && commSupp != null;
	//	elements.ckIsSelected.visible = !isEditing() && commSupp != null;
	//	elements.ckAreAllSelected.enabled = !isEditing() && commSupp != null;
	//	elements.ckAreAllSelected.visible = !isEditing() && commSupp != null;
	//	elements.lblSelectAll.visible = !isEditing();
	elements.fldUser.visible = !isEditing() && (commSupp == null);

	elements.vlPC.enabled = !isEditing();
	elements.fldSuppComm.enabled = !isEditing();
	elements.buttonRefresh.enabled = !isEditing();
	elements.btnClose.visible = !isEditing();

	//	elements.lblSuppComm.visible = profCenter==null?false:true;
	//	elements.fldSuppComm.visible = profCenter==null?false:true;
	//elements.buttonEdit.visible = !isEditing();
	//return _super.updateUI(event)
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"4830683D-16E5-4852-A3C5-A24026CC6AD9"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	userId = 0;

	if (foundset.find()) {
		foundset.comm_supp_customers_to_comm_supp_profit_centers.profit_center_id = profCenter;
		if (commSupp != null && commSupp != 0) {
			if (users.find()) {
				users.users_to_contacts.contact_id = commSupp;
				application.output('commSupp: ' + commSupp);
				var tot = users.search();
				application.output('tot: ' + tot);
			}

			if (tot > 0) {
				var recUser = users.getRecord(1);
				userId = recUser.user_id;
				application.output('userId: ' + userId);
			}
			foundset.comm_supp_customers_to_comm_supp_profit_centers.user_id = userId;
		}
		foundset.search();
		foundset.sort('comm_supp_customers_to_comm_supp_profit_centers.comm_supp_pc_to_users.users_to_contacts.real_name asc');
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"6608AD57-C99C-4404-8D41-BAF747DD6264"}
 */
function refresh(event) {
	profCenter = pcList ? pcList[0] : null;
	scopes.globals.selectedPCCommSupp = profCenter;
	commSupp = null;
	applyFilter(event);
}

/**
 * Handle changed data.
 *
 * @param oldValue old value
 * @param newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"FBD26B31-81CB-403E-923F-3C478A022736"}
 */
function onDataChange(oldValue, newValue, event) {
	//	if (oldValue != newValue) scopes.globals.selectedPCCommSupp = newValue;
	application.output('oldValue: ' + oldValue + '; newValue: ' + newValue);
	applyFilter(event);
	return true;
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"ABB8D3F0-2E1A-4062-92A6-FCCA9045DAA6"}
 */
function onShow(firstShow, event) {
	if (firstShow) {
		scopes.globals.selectedPCCommSupp = pcList ? pcList[0] : null;
		profCenter = scopes.globals.selectedPCCommSupp;
		applyFilter(event);
	}
	//	elements.lblSuppComm.visible = profCenter==null?false:true;
	//	elements.fldSuppComm.visible = profCenter==null?false:true;
}

/**
 * Handle changed data.
 *
 * @param oldValue old value
 * @param newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"A8BD5CDB-0295-494B-9A39-BBCF1D1C6A67"}
 */
function onDataChangePC(oldValue, newValue, event) {
	scopes.globals.selectedPCCommSupp = newValue;
	commSupp = null;
	if ( (newValue != oldValue) && (newValue != null)) applyFilter(event);
	//	applyFilter(event);
	//	if (newValue == null){
	//		profCenter = 0;
	//		commSupp = 0;
	//		applyFilter(event);
	//	}
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"DE3EDCCC-7EDC-4BC5-8E27-BD2FA04F116F"}
 * @AllowToRunInFind
 */
function newRecord(event) {
	_super.newRecord(event);
	/** @type {JSFoundSet<db:/geco/comm_supp_profit_centers>} */
	var comm_supp_pc = databaseManager.getFoundSet('geco', 'comm_supp_profit_centers');
	var totFind = 0;

	if (comm_supp_pc.find()) {
		comm_supp_pc.profit_center_id = profCenter;
		comm_supp_pc.user_id = userId;
		totFind = comm_supp_pc.search();
	}
	if (totFind > 0) {
		var rec = comm_supp_pc.getRecord(1);

		application.output('record padre nuovo ' + rec.comm_supp_profit_center_id)
		foundset.comm_supp_pc_id = rec.comm_supp_profit_center_id;

	}
	//	if (rec.isNew()) {
	//		application.output('record padre nuovo ' + rec.comm_supp_profit_center_id)
	//		foundset.comm_supp_pc_id = rec.comm_supp_profit_center_id;
	//	} else {
	//		application.output('record padre vecchio ' + rec.comm_supp_profit_center_id)
	//	}
	//
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"79587222-A554-482B-8686-2A1632D0A9F7"}
 */
function onHide(event) {
	if (isEditing())
		stopEditing(event);
	if (controller.getWindow()) 
		controller.getWindow().destroy();
}
