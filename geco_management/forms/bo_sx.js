/**
 * @param event
 * @param formFilterName
 * @param formListName
 *
 * @properties={typeid:24,uuid:"FB539C26-C203-4676-8D16-DF933D3282BD"}
 */
function onLoad(event, formFilterName, formListName) {
		
	if (globals.hasRole('Supp. Commerciale') && !(globals.hasRole('Delivery Admin') || globals.hasRole('Resp. Mercato') 
			|| globals.hasRole('Steering Board') || globals.hasRole('Controllers') || globals.hasRole('Orders Admin') || globals.hasRole('Planner') 
			|| globals.hasRole('Vice Resp. Mercato'))) formFilterName = 'bo_filter_supp_comm';

	_super.onLoad(event, formFilterName, formListName);
}

