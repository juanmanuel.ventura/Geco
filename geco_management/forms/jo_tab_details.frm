borderType:"SpecialMatteBorder,0.0,0.0,0.0,0.0,#000000,#000000,#000000,#cccccc,0.0,",
dataSource:"db:/geco/job_orders",
extendsID:"1FA35CAE-47C9-4025-BE35-E8E45A6A061F",
items:[
{
anchors:9,
location:"16,144",
size:"61,20",
text:"Cliente",
transparent:true,
typeid:7,
uuid:"03E7FD6A-06C5-4367-9BF7-FA017AC0FE9B"
},
{
background:"#ffffff",
dataProviderID:"job_orders_to_jo_planning_actual.total_return_actual",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"923,255",
margin:"0,0,0,5",
mnemonic:"",
name:"ricavo_actual",
size:"108,20",
typeid:7,
uuid:"0D609BFC-BDBC-4557-82AD-715BBF29B9B3"
},
{
background:"#ffffff",
dataProviderID:"job_orders_to_jo_planning_not_actual.total_cost",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"1033,279",
margin:"0,0,0,5",
name:"costo_forecast",
size:"108,20",
typeid:7,
uuid:"0E79E06B-4E79-4ABB-8A60-64481C1D3E5A"
},
{
dataProviderID:"profit_center_id",
displayType:10,
editable:false,
formIndex:7,
location:"474,104",
name:"ProfitCenterc",
size:"200,20",
text:"PC Type ID",
transparent:true,
typeid:4,
uuid:"25043884-02FB-4864-91FA-6E0FBFFEE4FD",
valuelistID:"73312BF3-989F-48F4-85E1-AB96BDEA9A3F"
},
{
background:"#ffffff",
dataProviderID:"job_orders_to_jo_planning_actual.total_cost_actual",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"923,279",
margin:"0,0,0,5",
name:"costo_actual",
size:"108,20",
typeid:7,
uuid:"28C27576-EC14-4B7E-9525-0EC0367186A1"
},
{
extendsID:"07AB7FEF-0560-4B82-B56C-D95051E8AD67",
location:"1178,499",
rolloverCursor:12,
text:null,
toolTipText:"Salva",
typeid:7,
uuid:"29DB63A1-EF5E-457B-A17E-587B9069ED65"
},
{
anchors:13,
borderType:"LineBorder,1,#cacaca",
formIndex:10,
items:[
{
containsFormID:"F49A274B-8D5F-42C4-89B4-67AA1BAE6662",
location:"18,287",
relationName:"job_orders_to_business_opportunities",
text:"jo_to_bo_list",
typeid:15,
uuid:"FB48E034-56AC-4838-8004-416CFF85AAFC"
}
],
location:"15,230",
name:"tab_jo_to_bo_list",
printable:false,
size:"709,211",
transparent:true,
typeid:16,
uuid:"361C23E2-69CE-432C-9F03-3B0F9CA62462"
},
{
background:"#ffffff",
dataProviderID:"eac_mol_display",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"1143,303",
margin:"0,0,0,5",
name:"mol_eac",
size:"108,20",
typeid:7,
uuid:"399DABCE-41A9-4D5C-BEAD-EADB21050A8E"
},
{
formIndex:9,
horizontalAlignment:4,
location:"1033,230",
margin:"0,0,0,5",
size:"108,20",
styleClass:"bold",
text:"Forecast",
transparent:true,
typeid:7,
uuid:"504051E1-067A-4FF9-86C9-4E80712A6999"
},
{
formIndex:9,
horizontalAlignment:4,
location:"764,327",
margin:"0,0,0,5",
size:"47,20",
text:"Mol %",
transparent:true,
typeid:7,
uuid:"53B05C96-17EF-41E7-A9D7-FC288907AE3A"
},
{
dataProviderID:"external_code_navision",
editable:false,
formIndex:2,
location:"474,64",
name:"JobOrderCode",
size:"200,20",
text:"Title",
transparent:true,
typeid:4,
uuid:"5CB90745-24CA-4F36-9380-276BB68B8EEA"
},
{
enabled:false,
horizontalAlignment:2,
location:"39,184",
size:"75,20",
text:"Billable",
transparent:true,
typeid:7,
uuid:"5D0AC32D-142F-4BCF-8513-A7FBB4440234"
},
{
dataProviderID:"user_owner_id",
displayType:10,
editable:false,
formIndex:6,
location:"78,104",
name:"RDCc",
size:"232,20",
text:"PC Type ID",
transparent:true,
typeid:4,
uuid:"5EC193CA-5D47-454D-8112-3EA2CD43B62B",
valuelistID:"3AB8DD5C-F02E-40DF-879B-9BFBCE22AF8D"
},
{
formIndex:9,
horizontalAlignment:4,
location:"764,255",
margin:"0,0,0,5",
size:"47,20",
text:"Ricavi",
transparent:true,
typeid:7,
uuid:"61D34345-E453-4272-ABE4-253A4DC2E3D0"
},
{
anchors:9,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"scopes.globals.actualYear",
foreground:"#ff6600",
formIndex:7,
format:"####",
location:"936,24",
name:"l_actualDatec",
size:"43,20",
styleClass:"bold",
transparent:true,
typeid:7,
uuid:"66E25691-A1A4-4BD8-B083-93558666E9A2"
},
{
anchors:9,
fontType:"Calibri,1,15",
foreground:"#ff6600",
formIndex:6,
location:"778,24",
text:"Data Actual: ",
transparent:true,
typeid:7,
uuid:"69FB325D-11E7-4134-BF7B-A6FC66653FD8"
},
{
location:"392,64",
size:"81,20",
text:"Codice",
transparent:true,
typeid:7,
uuid:"6AF08E8D-1944-4CC7-8FED-7DE61694D666"
},
{
allowBreakAcrossPageBounds:false,
extendsID:"6F614343-45A5-4185-BAD8-A96D7B66D21E",
typeid:19,
uuid:"6BB5E7E4-31DD-4410-890E-A3BC3D70CA12"
},
{
dataProviderID:"is_loggable",
displayType:4,
formIndex:3,
location:"151,185",
name:"isLoggable",
onActionMethodID:"-1",
onDataChangeMethodID:"-1",
size:"23,20",
transparent:true,
typeid:4,
uuid:"7089F273-0627-4CE0-AEC6-0AAA99AE0210"
},
{
formIndex:9,
horizontalAlignment:4,
location:"764,303",
margin:"0,0,0,5",
size:"47,20",
text:"Mol",
transparent:true,
typeid:7,
uuid:"71DE9F2D-C132-4A8C-89E4-0946162250FB"
},
{
formIndex:9,
horizontalAlignment:4,
location:"764,279",
margin:"0,0,0,5",
size:"47,20",
text:"Costi",
transparent:true,
typeid:7,
uuid:"7D9101D1-E6C6-4422-932A-463D5C74DE83"
},
{
anchors:9,
location:"16,64",
size:"61,20",
text:"Tipo",
transparent:true,
typeid:7,
uuid:"8D24A16B-2498-49CC-8A55-0418DE18524A"
},
{
extendsID:"DA9E3F87-A868-4257-AFFB-2214FB026E9A",
location:"1219,499",
rolloverCursor:12,
text:null,
toolTipText:"Modifica",
typeid:7,
uuid:"8EA2EFFA-D60A-4654-BE82-82B6911A8660"
},
{
background:"#ffffff",
dataProviderID:"job_orders_to_jo_planning_actual.mol_actual_display",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"923,303",
margin:"0,0,0,5",
name:"mol_actual",
size:"108,20",
typeid:7,
uuid:"90203BDB-D64E-41BE-B03D-DA838CD131BB"
},
{
formIndex:4,
location:"392,104",
size:"81,20",
text:"C. Profitto",
transparent:true,
typeid:7,
uuid:"92200BEB-832C-42B4-BD4E-DBB5342B3F32"
},
{
dataProviderID:"job_order_title",
editable:false,
location:"78,24",
name:"Title",
size:"596,20",
text:"User Owner",
transparent:true,
typeid:4,
uuid:"9AB90148-7D01-4C22-B6FC-164C65E78E58"
},
{
extendsID:"4EF01A0C-8886-4BD4-8494-4826D6F4D39C",
location:"1219,499",
rolloverCursor:12,
text:null,
toolTipText:"Annulla",
typeid:7,
uuid:"9E32D690-D065-4A00-8D5B-F6165DACB90C"
},
{
anchors:9,
dataProviderID:"company_id",
displayType:10,
editable:false,
location:"78,144",
name:"customerc",
size:"232,20",
text:"PC Type ID",
transparent:true,
typeid:4,
uuid:"9F07EEEF-A850-4364-AC5B-A36A63B1B4C0",
valuelistID:"0869922E-B486-413D-B55F-40E11B77E10D"
},
{
location:"779,104",
size:"81,20",
text:"Data fine",
transparent:true,
typeid:7,
uuid:"A0EDBE54-ADCB-4915-AFB9-2087EC4AFD0D"
},
{
anchors:9,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"scopes.globals.actualMonthStrIT",
foreground:"#ff6600",
formIndex:7,
horizontalAlignment:4,
location:"861,24",
name:"l_actualDate",
size:"68,20",
styleClass:"bold",
transparent:true,
typeid:7,
uuid:"A454ABDD-7EC8-4567-967F-5F22BBFAA409"
},
{
formIndex:2,
horizontalAlignment:2,
location:"175,185",
name:"lab_isLoggable",
size:"106,20",
text:"Consuntivabile",
transparent:true,
typeid:7,
uuid:"A62EE7CB-B311-47BE-A2EA-29558C3F5AD2"
},
{
location:"778,64",
size:"81,20",
text:"Data inizio",
transparent:true,
typeid:7,
uuid:"A8010BCF-DE7D-4BA9-ADFA-B72E25E4FE0F"
},
{
background:"#ffffff",
dataProviderID:"job_orders_to_jo_planning_actual.mol_actual_per_display",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"923,327",
margin:"0,0,0,5",
name:"molper_actual",
size:"108,20",
typeid:7,
uuid:"A8EA4A0F-F7C2-4E76-A227-814873EB760F"
},
{
background:"#ffffff",
dataProviderID:"eac_return",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"1143,255",
margin:"0,0,0,5",
mnemonic:"",
name:"ricavo_eac",
size:"108,20",
typeid:7,
uuid:"B3BBF862-E60E-4202-879A-6A6FAA84DC24"
},
{
dataProviderID:"valid_to",
displayType:5,
formIndex:2,
format:"MMMM-yyyy",
location:"861,104",
name:"JobOrderDateTo",
size:"200,20",
styleClass:"date",
text:"Title",
typeid:4,
uuid:"BC994CA1-8E9D-4540-9DC2-F1C092C5B4D3"
},
{
enabled:false,
formIndex:2,
horizontalAlignment:2,
location:"339,185",
size:"86,20",
text:"Attiva",
transparent:true,
typeid:7,
uuid:"BCAD090D-404D-4326-81AD-33BF090E38B9"
},
{
dataProviderID:"is_billable",
displayType:4,
enabled:false,
formIndex:1,
location:"16,185",
name:"is_billable",
onActionMethodID:"-1",
onDataChangeMethodID:"-1",
size:"23,20",
transparent:true,
typeid:4,
uuid:"BCF69AD6-E687-405D-A275-0FCF5CB55E67"
},
{
formIndex:9,
horizontalAlignment:4,
location:"813,230",
margin:"0,0,0,5",
size:"108,20",
styleClass:"bold",
text:"Budget",
transparent:true,
typeid:7,
uuid:"BDF0BA41-80F2-4931-AE5E-F4867EC06BC7"
},
{
background:"#ffffff",
dataProviderID:"jo_budget_mol",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"813,303",
margin:"0,0,0,5",
name:"molJo",
size:"108,20",
typeid:7,
uuid:"C36C84B0-4956-4E80-AFD2-2EAB21BBC2F8"
},
{
background:"#ffffff",
dataProviderID:"jo_budget_mol_percentage",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"813,327",
margin:"0,0,0,5",
name:"molperJo",
size:"108,20",
typeid:7,
uuid:"C521BC5A-7D7E-49F7-A4D9-E83C12ADB3C5"
},
{
background:"#ffffff",
dataProviderID:"job_orders_to_jo_planning_not_actual.mol_display",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"1033,303",
margin:"0,0,0,5",
name:"mol_forecast",
size:"108,20",
typeid:7,
uuid:"C63C255E-057C-41BC-BE44-79A459725944"
},
{
dataProviderID:"is_enabled",
displayType:4,
editable:false,
enabled:false,
formIndex:3,
location:"317,185",
name:"is_billableccc",
onActionMethodID:"-1",
onDataChangeMethodID:"-1",
size:"23,20",
transparent:true,
typeid:4,
uuid:"C8835B47-0E9B-4431-8AEB-742DAB9CC181"
},
{
background:"#ffffff",
dataProviderID:"eac_cost",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"1143,279",
margin:"0,0,0,5",
name:"costo_eac",
size:"108,20",
typeid:7,
uuid:"CA487A99-4242-4F48-B257-163A1749C1D0"
},
{
dataProviderID:"customer_order_code",
editable:false,
formIndex:3,
location:"474,144",
name:"customerOrderCode",
size:"200,20",
text:"Title",
transparent:true,
typeid:4,
uuid:"D3DB6CCF-5453-4272-ACF1-B00D4D323DF2"
},
{
background:"#ffffff",
dataProviderID:"profit",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"813,255",
margin:"0,0,0,5",
name:"ricavoJo",
size:"108,20",
typeid:7,
uuid:"D450AC01-4AEC-45E7-9549-59C3A232A07C"
},
{
formIndex:1,
location:"392,144",
size:"81,20",
text:"Codice B.O.",
transparent:true,
typeid:7,
uuid:"D977605F-3859-4586-BD64-7C50CE7EB009"
},
{
background:"#ffffff",
dataProviderID:"job_orders_to_jo_planning_not_actual.total_return",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"1033,255",
margin:"0,0,0,5",
mnemonic:"",
name:"ricavo_forecast",
size:"108,20",
typeid:7,
uuid:"DA93B676-FEC3-45E0-9CF2-8C244E8976F1"
},
{
formIndex:9,
horizontalAlignment:4,
location:"923,230",
margin:"0,0,0,5",
size:"108,20",
styleClass:"bold",
text:"Actual",
transparent:true,
typeid:7,
uuid:"DD019AC6-16AD-4D31-964B-B47948E7CBFC"
},
{
location:"16,24",
size:"61,20",
text:"Titolo",
transparent:true,
typeid:7,
uuid:"DFE1DE4D-1417-448D-9D44-43B76D513D27"
},
{
extendsID:"2FD9ADCF-E59B-4856-B8CB-C26AFF795744",
formIndex:0,
size:"1260,1",
typeid:7,
uuid:"E002B81D-A629-4D1A-AEA1-C2BA5D685707"
},
{
anchors:9,
dataProviderID:"job_order_type",
displayType:10,
editable:false,
location:"78,64",
name:"jobOrderType",
size:"232,20",
text:"PC Type ID",
transparent:true,
typeid:4,
uuid:"E1B411EE-93D2-43E4-ADC3-5C4973A76EF8",
valuelistID:"9123976F-276C-4008-B26A-33F742962BB6"
},
{
background:"#ffffff",
dataProviderID:"cost",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"813,279",
margin:"0,0,0,5",
name:"costoJo",
size:"108,20",
typeid:7,
uuid:"E484E2CB-4052-4574-8262-AB9E0C497737"
},
{
dataProviderID:"valid_from",
displayType:5,
formIndex:2,
format:"MMMM-yyyy",
location:"860,64",
name:"JobOrderDateFrom",
size:"200,20",
styleClass:"date",
text:"Title",
typeid:4,
uuid:"E5E27D3A-4CDD-4322-B163-D49EC17D19C9"
},
{
background:"#ffffff",
dataProviderID:"eac_mol_per_display",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"1143,327",
margin:"0,0,0,5",
name:"molper_eac",
size:"108,20",
typeid:7,
uuid:"EA2017CB-E82E-4FFB-8956-641F9F751A9E"
},
{
background:"#ffffff",
dataProviderID:"job_orders_to_jo_planning_not_actual.mol_per_display",
displaysTags:true,
formIndex:8,
format:",##0.00",
horizontalAlignment:4,
location:"1033,327",
margin:"0,0,0,5",
name:"molper_forecast",
size:"108,20",
typeid:7,
uuid:"F171DF5F-FD20-4B28-AA36-64556C51934D"
},
{
formIndex:5,
location:"16,104",
size:"61,20",
text:"RDC",
transparent:true,
typeid:7,
uuid:"F9E2F624-4C18-4E24-854B-34B478927B28"
},
{
formIndex:9,
horizontalAlignment:4,
location:"1143,230",
margin:"0,0,0,5",
size:"108,20",
styleClass:"bold",
text:"EAC",
transparent:true,
typeid:7,
uuid:"FCAAF4F0-5F3D-4ECF-B68A-743E619B2847"
}
],
name:"jo_tab_details",
navigatorID:"-1",
showInMenu:true,
size:"1260,536",
styleName:"GeCo",
transparent:false,
typeid:3,
uuid:"FCC33F92-EA7B-4F84-9E29-6BF371F8F6B5"