/**
 * @type {Number}
 * @properties={typeid:35,uuid:"89F1E838-544B-4644-9819-98655BCAD0AF",variableType:4}
 */
var ricavoBC = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"048A3F9A-C9F3-43DC-B50F-466D4C4F5E2E",variableType:4}
 */
var costoBC = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"08BF32B0-942F-4699-A400-3B6F9B49E62D",variableType:4}
 */
var molBC = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"56E7531F-6541-494D-8A47-02BF881F1150",variableType:4}
 */
var molperBC = 0;

/**
 * @properties={typeid:35,uuid:"9155410D-528E-447A-B03F-37CDE4C06E6B",variableType:-4}
 */
var client = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"35680BA3-20AD-43FE-A63A-130B37AB976B",variableType:8}
 */
var boFilter = null;

/**
 * @param {JSEvent} event The event that triggered the action.
 *
 * @properties={typeid:24,uuid:"8F588107-9263-431A-B0EF-62ECBB98F1B9"}
 */
function updateUI(event) {
	_super.updateUI(event);
	
	var isController = globals.hasRole('Controllers');
	
	var canModify = globals.isOwnerForObject(scopes.globals.currentUserId, globals.job_order_selected, 'jo') || globals.isPCManagerForObject(scopes.globals.currentUserId, globals.job_order_selected, 'jo') || isController;
	var isEnabled =forms.jo_details.isEnabled;
	var isBillable = forms.jo_details.isBillable;
	
	controller.readOnly = false;
	controller.enabled = true;
	elements.buttonEdit.visible = !isEditing() && isEnabled && canModify && isBillable;
	elements.buttonCancel.visible = isEditing();
	elements.buttonSave.visible = isEditing();
	
	elements.JobOrderDateFrom.enabled = isEditing() && canModify && isEnabled;
	elements.JobOrderDateTo.enabled = isEditing() && canModify && isEnabled;
	elements.isLoggable.enabled = isEditing() && canModify && isEnabled;

	//application.output('Visible ' + elements.buttonEdit.visible);
	forms.jo_details.updateUI(event);
}

