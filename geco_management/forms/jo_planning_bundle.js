/**
 * @param {JSEvent} event
 * @param {String} formFilterName
 * @param {String} formListName
 * @param {String} relationName
 *
 * @properties={typeid:24,uuid:"966ABCBE-B8E1-47D7-9F2B-52B6D1486AA5"}
 */
function onLoad(event, formFilterName, formListName, relationName) {
	application.output(globals.messageLog + 'START jo_planning_bundle.on load() ', LOGGINGLEVEL.INFO);		
//	application.output('jo_plain bundle ' + formFilterName + ' ' + formListName +' ' + relationName);
	
//	var h_filter = 134;
//
//	if (formFilterName && forms[formFilterName]) {
//
//		var form = solutionModel.getForm(formFilterName);
//		h_filter = form.getPart(JSPart.BODY).height
//	}
//	try {
//		// set navigator
//		if (elements.split.dividerLocation) {
//			elements.split.dividerSize = 0;
//			elements.split.dividerLocation = h_filter;
//		}
//	} catch (e) {
//		//		application.output(e);
//		//		application.output('errore initialize');
//	}
//	if (formFilterName && forms[formFilterName]) {
//		elements.split.setLeftForm(forms[formFilterName],relationName);
//		// force to apply filter (seems not to work on form's onLoad event)
//		if (forms[formFilterName].applyFilter) forms[formFilterName].applyFilter();
//	}
//	elements.split.setRightForm(forms[formListName],relationName);
	
	var result = null;
	if (elements.tab_filter.removeAllTabs()) {
		if (forms[formFilterName]) {
			result=elements.tab_filter.addTab(forms[formFilterName], null, null, null, null, null, null, relationName || null, -1);
			application.output(globals.messageLog + 'jo_planning_bundle.on load() caricato tab '+result, LOGGINGLEVEL.DEBUG);	
			if (forms[formFilterName].applyFilter) forms[formFilterName].applyFilter();
		} else application.output(globals.messageLog + 'jo_planning_bundle.on load() errore sul form ' + formFilterName, LOGGINGLEVEL.ERROR);
	}
	
	if (elements.tab_list.removeAllTabs()) {
		if (forms[formListName]) {
			result=elements.tab_list.addTab(forms[formListName], null, null, null, null, null, null, relationName || null, -1);
			application.output('caricato tab '+result);
			application.output(globals.messageLog + 'jo_planning_bundle.on load() caricato tab '+result, LOGGINGLEVEL.DEBUG);
			
		} else application.output(globals.messageLog + 'jo_planning_bundle.on load() errore sul form ' + formFilterName, LOGGINGLEVEL.ERROR);
	}
	
	
}
