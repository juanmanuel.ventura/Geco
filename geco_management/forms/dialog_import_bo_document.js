/**
 * @type {Number}
 * @properties={typeid:35,uuid:"33832424-6490-4360-9D8B-60EF3B15AA96",variableType:8}
 */
var doc_type = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"030EC292-31A4-470F-B630-673B5AD04FAD"}
 */
var doc_descr = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"8454EE67-B1F4-465D-B85C-364FDBE0F71C"}
 */
var document_path = '';

/**
 * @type {String}
 * @properties={typeid:35,uuid:"10F10403-A2AF-4BD0-8ED7-3B925FC33C2E"}
 */
var uploadResult = null;
/**
 * @type {String}
 * @properties={typeid:35,uuid:"0DF8AB7C-4E43-4D64-85D8-7AFDDE694CF5"}
 */
var document_name = '';

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"FD26CDA3-A103-42D5-AF37-817315129607",variableType:8}
 */
var bo_id = null

/**
 * @type {plugins.file.JSFile}
 * @properties={typeid:35,uuid:"2534DCA7-78EA-4C4F-B154-8BEB7E08D118",variableType:-4}
 */
var vJsFile = null;

/**
 * @properties={typeid:24,uuid:"A1F3F1DD-F10F-4399-AD15-28959301BDDD"}
 */
function btnChooseFile() {
	application.output(globals.messageLog + 'START dialog_import_bo_document.btnChooseFile ', LOGGINGLEVEL.INFO);
	/** @type {plugins.file.JSFile} **/
	vJsFile = plugins.file.showFileOpenDialog(1, null, false, ['*'], uploadCallBack);
	if (vJsFile) {
		application.output('file ' + vJsFile.getAbsolutePath());
		document_path = vJsFile.getAbsolutePath();
		application.output(vJsFile.getName());
	} else {
		document_path = null;
	}
	application.output(globals.messageLog + 'STOP dialog_import_bo_document.btnChooseFile ', LOGGINGLEVEL.INFO);
}

/**
 * @param {JSEvent} event *
 * @properties={typeid:24,uuid:"BF5C8F5B-EA25-49EA-9B98-450546B64D76"}
 */
function closeForm(event) {
	globals.resultImportDocuments = null;
	doc_type = null;
	doc_descr = null;
	document_path = '';
	document_name = '';
	uploadResult = null;
	controller.getWindow().destroy();
}

/**
 * @param {plugins.file.JSFile[]} fileToUpload
 *
 * @properties={typeid:24,uuid:"99331FA6-E7E3-45A2-82C9-B4DD9243F731"}
 */
function uploadCallBack(fileToUpload) {
	application.output(globals.messageLog + 'START dialog_import_bo_document.uploadCallBack ', LOGGINGLEVEL.INFO);
	/** @type {plugins.file.JSFile} **/
	var myuploadedfile = fileToUpload[0];
	application.output(globals.messageLog + 'nome file upload ' + myuploadedfile.getName(), LOGGINGLEVEL.DEBUG);
	globals.serverDir = plugins.file.convertToJSFile("documentiBO");
	application.output(globals.messageLog + 'dir path ' + globals.serverDir.getAbsolutePath(), LOGGINGLEVEL.DEBUG);
	var resultDir = plugins.file.createFolder(globals.serverDir);

	application.output(globals.messageLog + 'creata dir ' + resultDir, LOGGINGLEVEL.DEBUG);
	application.output(globals.messageLog + 'dir globale ' + globals.serverDir, LOGGINGLEVEL.DEBUG);
	globals.serverfile = plugins.file.createFile(globals.serverDir.getName() + '/' + myuploadedfile.getName());
	application.output(globals.messageLog + 'file server ' + globals.serverfile, LOGGINGLEVEL.DEBUG);
	var result = plugins.file.writeFile(globals.serverfile, myuploadedfile.getBytes());
	application.output(globals.messageLog + 'file scritto in locale ' + result, LOGGINGLEVEL.DEBUG);
	if (result) {
		document_name = myuploadedfile.getName();
		application.output(globals.messageLog + 'nome doc ' + document_name, LOGGINGLEVEL.DEBUG)
		application.output(globals.serverDir.getAbsolutePath())
		document_path = globals.serverDir.getAbsolutePath();
		application.output(globals.messageLog + 'dir path ' + document_path, LOGGINGLEVEL.DEBUG)
	}
	application.output(globals.messageLog + 'STOP dialog_import_bo_document.uploadCallBack ', LOGGINGLEVEL.INFO);
}
/**
 * @properties={typeid:24,uuid:"CF6D295B-D368-4CC5-ADA9-74D3020BFA46"}
 */
function processDocument() {
	application.output(globals.messageLog + ' START dialog_import_bo_document.processDocument ', LOGGINGLEVEL.INFO);
	//se ordine non è cancellabile
	var isEraseable = (doc_type != 3);

	var listFile = plugins.file.getFolderContents(document_path);
	var fileSaved;
	try {
		for (var i = 0; i < listFile.length; i++) {
			fileSaved = listFile[i];
			application.output(globals.messageLog + ' lista file ' + fileSaved.getName(), LOGGINGLEVEL.DEBUG);

			if (fileSaved) {
				application.output(globals.messageLog + ' nome ' + fileSaved.getName() + ' content type ' + fileSaved.getContentType() + ' size ' + fileSaved.size(), LOGGINGLEVEL.DEBUG)
				var nomeFile = fileSaved.getName();
				var index = nomeFile.lastIndexOf('.');
				var estensione = '';
				if (index > 0) estensione = nomeFile.slice(index + 1);

				application.output(estensione);

				/** @type {JSFoundSet<db:/geco/bo_documents>} */
				var documents = databaseManager.getFoundSet('geco', 'bo_documents');
				documents.newRecord(false);
				documents.bo_document = fileSaved.getBytes();
				documents.bo_document_size = fileSaved.size();
				documents.bo_document_original_name = fileSaved.getName();
				documents.bo_document_mimetype = estensione;
				//documents.bo_document_mimetype = fileSaved.getContentType();
				documents.bo_document_type = doc_type;
				var result = databaseManager.saveData(documents);

				if(result) {
					/** @type {JSFoundSet<db:/geco/bo_bo_documents>} */
					var bo_bo_documents = databaseManager.getFoundSet('geco', 'bo_bo_documents');
					bo_bo_documents.newRecord(false);
					bo_bo_documents.bo_id = globals.bo_selected;
					bo_bo_documents.bo_document_id = documents.bo_document_id;
					bo_bo_documents.bo_bo_documents_details = doc_descr;
					bo_bo_documents.is_eraseable = isEraseable;
					databaseManager.saveData(bo_bo_documents);
				}
				uploadResult = 'Documento caricato con successo';
			}
		}
	} catch (e) {
		application.output(globals.messageLog + ' ERROR  dialog_import_bo_document.processDocument Importing files failed ', LOGGINGLEVEL.ERROR);
		uploadResult = 'Documento non caricato';
	} finally {
		plugins.file.deleteFolder(globals.serverDir, false);
	}
}
