/**
 * @properties={typeid:35,uuid:"8C43814A-5589-4AD1-B35A-0F023D336209",variableType:-4}
 */
var isController = globals.hasRole('Controllers');

/**
 * @properties={typeid:35,uuid:"08E0C5F0-69D0-4501-8413-9555CC34A6FC",variableType:-4}
 */
var isOrdersAdmin = globals.hasRole('Orders Admin');

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"FA92B03A-DDC9-415F-AE3E-FE97577A629D",variableType:-4}
 */
var editingStarted = false;

/**
 * Handle record selected.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"9A326C48-8C36-4374-916C-F109F5F2CFB2"}
 */
function onRecordSelection(event) {
	updateUI(event);
	/** @type {Boolean} */
	var afterSwitchPanelOnRealFigureNull = false;
	if (foundset && foundset.getSize() > 0) {
		if (foundset.real_figure != null) {
			//			application.output(foundset.real_figure + ' - ' + foundset.enrollment_number)
			user_id_er = foundset.getSelectedRecord().real_figure + ' - ' + foundset.getSelectedRecord().enrollment_number;
			//var vname = elements.realP.getValueListName();
			//			application.output(vname);
			//			application.output(user_id_er);

			//userCostID = globals.getCostID(foundset.real_figure, foundset.enrollment_number);
			//globals.setValuelist('userCostList', foundset.real_figure);
			//setValuelist('userTmCostList', null);
		} else if (foundset.real_tm_figure != null) {
			user_id_er = foundset.real_tm_figure + ' - ' + foundset.enrollment_number;
			//			application.output(user_id_er);
			//			userCostID = globals.getCostID(foundset.real_tm_figure, foundset.enrollment_number)
			//			globals.setValuelist('userCostList', foundset.real_tm_figure);
			//setValuelist('userCostList', null);
		} else if (foundset.real_figure == null) {
			//			user_id_er = !globals.isEmpty(foundset.getSelectedRecord().user_id) && !globals.isEmpty(foundset.getSelectedRecord().enrollment_number) && globals.isEmpty(foundset.getSelectedRecord().standard_figure)?foundset.getSelectedRecord().real_figure + ' - ' + foundset.getSelectedRecord().enrollment_number:'';
			user_id_er = '';
			afterSwitchPanelOnRealFigureNull = foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 9?true:false;
		}
		if (foundset.jo_details_to_profit_cost_types)
			switchPanels(foundset.jo_details_to_profit_cost_types.profit_cost_type, foundset.jo_details_to_profit_cost_types.profit_cost_types_id);
		else switchPanels(null, null);
		if (foundset.profit_cost_type_id == 9 || foundset.profit_cost_type_id == 5) {

			if (foundset.description != null) {
				elements.labale_st_cost.visible = false;
				elements.standAmount.visible = false;
			} else {
				elements.labale_st_cost.visible = true;
				elements.standAmount.visible = true;
			}
		}

		if (afterSwitchPanelOnRealFigureNull && !editingStarted) {
			application.output('---------record_id: ' + foundset.jo_details_id);
			elements.labale_st_cost.visible = true;
			elements.standAmount.visible = true;
			elements.gg.visible = true;
		} else if (afterSwitchPanelOnRealFigureNull && editingStarted) {
			if (globals.isEmpty(foundset.real_figure) && (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 9)) {
				elements.labale_st_cost.visible = true;
				elements.standAmount.visible = true;
			}
		}
	} else switchPanels(null, null);
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"48DFBFEA-595A-4280-AFCF-0C3985FF1742"}
 */
function onDataChangeType(oldValue, newValue, event) {
	var record = foundset.getSelectedRecord();
	if (isEditing()) {
		userCostID = null;
		record.real_figure = null;
		record.real_tk_supplier = null;
		record.real_tm_figure = null;
		record.standard_figure = null;
		record.standard_import = null;
		record.days_import = null;
		record.days_number = null;
		record.description = null;
		record.cost_amount = null;
		record.return_amount = null;
		record.total_amount = null;
		record.enrollment_number = null;
		//userCostID = null;
		user_id_er = null;
	}

	//	application.output(record.jo_details_to_profit_cost_types);
	return switchPanels(record.jo_details_to_profit_cost_types.profit_cost_type, record.jo_details_to_profit_cost_types.profit_cost_types_id);
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"9A933F76-6A45-4F5B-B941-91D85A433D0A"}
 */
function onDataChangeStandardFigure(oldValue, newValue, event) {
	//se già presente il totale dei giorni e non è presente la figura reale modifica il totale

	if (oldValue != newValue) {
		if (!globals.isEmpty(newValue)) {
			foundset.standard_import = foundset.jo_details_to_standard_professional_figures.standard_cost;
			if (foundset.days_number != null && (foundset.real_figure == null && foundset.real_tm_figure == null && foundset.days_import == null)) {
//				JStaffa non setto la variabile days_import qui perchè al prossimo cambio della fig. professionale non viene aggiornato
//				foundset.days_import = foundset.standard_import;

//				Cambio la total_amount perchè non essendoci days_import devo prendere come riferimento days_number
//				foundset.total_amount = globals.roundNumberWithDecimal( (foundset.standard_import * foundset.days_import), 2);
				foundset.total_amount = globals.roundNumberWithDecimal( (foundset.standard_import * foundset.days_number), 2);
			}
			if (foundset.real_figure == null && foundset.real_tm_figure == null) {
				foundset.description = foundset.jo_details_to_standard_professional_figures.description;
			}
		}
		else {
			foundset.standard_import = null;
			foundset.description = foundset.real_figure ? foundset.description : null;
			foundset.total_amount = foundset.real_figure && foundset.days_import && foundset.days_number ? globals.roundNumberWithDecimal( (foundset.days_import * foundset.days_number), 2) : 0;
			foundset.days_import = foundset.real_figure?foundset.days_import:null;
		}
	}
	return true
}

///**
// * Handle changed data.
// *
// * @param {Number} oldValue old value
// * @param {Number} newValue new value
// * @param {JSEvent} event the event that triggered the action
// *
// * @returns {Boolean}
// *
// * @properties={typeid:24,uuid:"86389508-C52D-40D0-8B85-734BAE1BFDD9"}
// * @AllowToRunInFind
// */
//function onDataChangeRealFigure(oldValue, newValue, event) {
//	if (oldValue != newValue) {
//		foundset.total_amount = 0;
//		foundset.days_import = null;
//
//		if ( (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 9)) {
//			/** @type {globals.objUserCost} */
//			var obj = globals.getUserCost(userCostID);
//			foundset.description = obj.name;
//			foundset.days_import = obj.cost;
//			foundset.enrollment_number = obj.enrollNum;
//
//			if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10) {
//				foundset.real_figure = obj.uid;
//				if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10) foundset.days_import = null;
//			} else if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 9) {
//				foundset.real_tm_figure = obj.uid;
//			}
//			//foundset.figure = foundset.bo_details_to_real_figure.users_to_contacts.real_name;
//		}
//
//		//		//personale
//		//		if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 5)
//		//			foundset.description = foundset.jo_details_to_real_figure.users_to_contacts.real_name;
//		//		//Fornitura TM
//		//		if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 9)
//		//			foundset.description = foundset.jo_details_to_tm_figure.users_to_contacts.real_name;
//		//		//application.output(' ' + oldValue + ' ' + newValue)
//		//		globals.setValuelist('userCostList', newValue);
//		//		if (globals.valueListDS.getMaxRowIndex() == 1) {
//		//			//foundset.real_import = valueListDS.getValue(1, 1);
//		//			foundset.days_import = globals.valueListDS.getValue(1, 1);
//		//		}
//
//		if (foundset.days_number != null) foundset.total_amount = foundset.days_import * foundset.days_number;
//		//application.output('foundset.total_amount --- ' + foundset.total_amount);
//	}
//	return true
//}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"BCEED03E-E805-4424-B3F1-235BA92E56CE"}
 */
function onDataChangeDaysNumber(oldValue, newValue, event) {
	//se presente la figura reale/standard calcola il total amount
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (standard_import != null)
			foundset.total_amount = globals.roundNumberWithDecimal( (foundset.standard_import * foundset.days_number), 2);
		if (foundset.days_import != null) {
			/** @type {globals.objUserCost} */
			var obj = null;
			if(foundset.user_id != null)
				obj = globals.getLastUserCost('' + foundset.user_id, foundset.enrollment_number);
			else {
				obj = {
					uid: null,
					cost: foundset.days_import,
					name: '',
					enrollNum: ''
				}
			}
			
//			/** @type {globals.objUserCost} */
//			var obj = globals.getLastUserCost('' + foundset.user_id, foundset.enrollment_number)
			var dayToChange = foundset.days_number - foundset.days_number_actual;
			var costReal = globals.roundNumberWithDecimal( (foundset.cost_actual + (dayToChange * obj.cost)), 2);
			foundset.total_amount = costReal;
			//foundset.total_amount = foundset.days_import * foundset.days_number;

			if (foundset.days_import != obj.cost) foundset.days_import = obj.cost;
		}
		foundset.cost_amount = foundset.total_amount;
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
	}
	return true
}

/**
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"F6DD78F8-BDE8-4C22-9466-8FD8207485A8"}
 */
function updateUI(event) {
	_super.updateUI(event);

	var record = foundset.getSelectedRecord();

	var canModify = globals.isOwnerForObject(scopes.globals.currentUserId, globals.job_order_selected, 'jo') || globals.isPCManagerForObject(scopes.globals.currentUserId, globals.job_order_selected, 'jo') || isController || isOrdersAdmin;

	//application.output('------------------'+foundset.getSize());
	var isEnabled = (foundset.getSize() > 0) ? true : false // && foundset.jo_details_to_job_orders.getSize() > 0;
	var isEnabledJO = forms.jo_details.isEnabled;
	var isBillable = forms.jo_details.isBillable;
	var isAllineamento = (record && (record.profit_cost_type_id == 4 || record.profit_cost_type_id == 11)) ? true : false;
	var isInvoice = (record && (record.profit_cost_type_id == 12 || record.profit_cost_type_id == 6)) ? true : false;
	//	application.output(isEditing() +' ' +isEnabled+ ' ' + isEnabledJO + ' ' + canModify + ' ' +isBillable +' ' + isController + ' ' + isAllineamento + ' ' +  isInvoice );
	//
	//application.output(((!isController) ? (!isAllineamento && !isInvoice) : !isEditing()))
	elements.buttonEdit.visible = !isEditing() && isEnabled && isEnabledJO && canModify && isBillable && ( (!isController && !isOrdersAdmin) ? (!isAllineamento && !isInvoice) : !isEditing());
	elements.buttonAdd.visible = !isEditing() && isEnabledJO && canModify && isBillable;
	elements.buttonDelete.visible = !isEditing() && isEnabled && isEnabledJO && canModify && isBillable && ( (!isController && !isOrdersAdmin) ? (!isAllineamento && !isInvoice) : !isEditing());

	elements.ProfitCostDescription.visible = !isController && !isOrdersAdmin && isEditing();
	elements.ProfitCostDescriptionController.visible = isController || isOrdersAdmin || !isEditing();

	if (foundset && foundset.jo_details_to_profit_cost_types) {
		switchPanels(record.jo_details_to_profit_cost_types.profit_cost_type, record.jo_details_to_profit_cost_types.profit_cost_types_id);

	} else
		switchPanels(null, null);

	if (!isEditing()) {
		foundset.sort('jo_details_to_profit_cost_types.profit_cost_acr desc, jo_details_to_profit_cost_types.order_display asc, description asc');
	}

	if (isEditing()) {
		if (record && !record.isNew() && record.jo_details_to_jo_planning.getSize() > 0) {
			elements.ProfitCostDescription.enabled = false;
			elements.ProfitCostDescriptionController.enabled = false;
			elements.realP.enabled = false;
			elements.realTM.enabled = false;
			if (!isController && !isOrdersAdmin && (isAllineamento || isInvoice)) {
				elements.descr.enabled = false;
				elements.totalAmountR.enabled = false;
			} else {
				elements.descr.enabled = true;
				elements.totalAmountR.enabled = true;
			}
		} else if ( (record && record.isNew()) || (record && record.jo_details_to_jo_planning.getSize() == 0)) {
			if (!isController && !isOrdersAdmin && (isAllineamento || isInvoice)) {
				elements.ProfitCostDescription.enabled = false;
				elements.ProfitCostDescriptionController.enabled = false;
				elements.descr.enabled = false;
				elements.totalAmountR.enabled = false;
			} else {
				elements.ProfitCostDescription.enabled = true;
				elements.ProfitCostDescriptionController.enabled = true;
				elements.descr.enabled = true;
				elements.totalAmountR.enabled = true;
				elements.realP.enabled = true;
				elements.realTM.enabled = true;
			}
		}
	}

	forms.jo_details.updateUI(event);
	editingStarted = isEditing() ? true : false;
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"039BACD9-D6CE-465E-B618-3890D76DB02E"}
 */
function onDataChangeAmountPersonel(oldValue, newValue, event) {
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (foundset.days_number != null) {
			foundset.total_amount = globals.roundNumberWithDecimal( (foundset.days_import * foundset.days_number), 2);
			foundset.cost_amount = foundset.total_amount
		}
		foundset.standard_import = null;
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
	}
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"6948DF46-5CB3-4833-A410-1671D2F0EE54"}
 */
function onDataChangeTK(oldValue, newValue, event) {
	if (oldValue != newValue) {
		foundset.description = foundset.jo_details_to_suppliers.company_name;
	}
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"AC27506F-1155-4129-BFC7-E8451D45F111"}
 */
function onDataChangeTotalAmount(oldValue, newValue, event) {
	if (oldValue != newValue) {
		if (foundset.jo_details_to_profit_cost_types.profit_cost_acr == 'R')
			foundset.return_amount = foundset.total_amount;
		else
			foundset.cost_amount = foundset.total_amount;
	}
	return true
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A432F9A9-4BC2-49B2-BBAA-77810A6439F5"}
 * @AllowToRunInFind
 */
function createPlain(event) {
	if (globals.isEmpty(foundset.jo_details_to_job_orders.valid_from) || globals.isEmpty(foundset.jo_details_to_job_orders.valid_to)) {
		globals.DIALOGS.showErrorDialog('Errore', 'Per la pianificazione devi indicare la data di inizio e di fine', 'OK');
		return;
	}
	//var answer = globals.DIALOGS.showWarningDialog('Attenzione','Se esiste una pianificazione sovrascivi\nvuoi proseguire?','SI','NO')
	var answer = 'SI'
	if (answer == 'NO') return;

	var actual_date = new Date(2013, 2, 28);

	if (foundset.jo_details_to_job_orders.valid_from > actual_date)
		actual_date = foundset.jo_details_to_job_orders.valid_from;
	/** @type {Array} */
	var listObjTimeSplit = globals.countDayMonthsYears(actual_date, foundset.jo_details_to_job_orders.valid_to);

	//var totMonths = listObjTimeSplit.length;
	var totDays = 0;
	//conto i mesi/gg lavorabili su cui spalmare costi e ricavi
	for (var x = 0; x < listObjTimeSplit.length; x++) {
		totDays = totDays + listObjTimeSplit[x].dayWorkable;
	}
	application.output('totale gg ' + totDays);
	// per ogni dettaglio della Commessa
	for (var index = 1; index <= foundset.getSize(); index++) {
		var record = foundset.getRecord(index);

		application.output('-----------------------------' + record.jo_details_id);

		for (var i = 0; i < listObjTimeSplit.length; i++) {
			application.output('*****mese anno ' + listObjTimeSplit[i].monthDet + '-' + listObjTimeSplit[i].yearDet)
			//escludo il totale actual e i giorni actual dal conto dei giorni
			var dayAmount = Math.round( ( (record.total_amount - record.total_actual) / totDays) * Math.pow(10, 2)) / Math.pow(10, 2);
			application.output(record.days_number)
			var dayNumber = null;
			if (record.jo_details_to_profit_cost_types.profit_cost_acr == 'C')
				dayNumber = Math.round( ( ( (record.days_number - days_number_actual) / totDays) * listObjTimeSplit[i].dayWorkable) * Math.pow(10, 1)) / Math.pow(10, 1);
			application.output('giorni mensili ' + dayNumber)
			application.output('importo giornaliero ' + dayAmount);

			var totDetailPlain = 0;
			var totAm = 0;
			// controllo che non esista già una pianificazione di dettaglio
			// non sovrascrire se ci sono actual
			if (record.jo_details_to_jo_planning.find()) {
				record.jo_details_to_jo_planning.jo_details_id = record.jo_details_id;
				record.jo_details_to_jo_planning.jo_id = record.jo_id;
				record.jo_details_to_jo_planning.jo_month = listObjTimeSplit[i].monthDet;
				record.jo_details_to_jo_planning.jo_year = listObjTimeSplit[i].yearDet;
				record.jo_details_to_jo_planning.is_actual = 0;
				totDetailPlain = record.jo_details_to_jo_planning.search();
			}
			// se non esiste una pianificazione mensile per il dettaglio la creo
			if (totDetailPlain == 0) {
				record.jo_details_to_jo_planning.newRecord();
				record.jo_details_to_jo_planning.jo_details_id = record.jo_details_id;
				record.jo_details_to_jo_planning.jo_id = record.jo_id;
				record.jo_details_to_jo_planning.jo_month = listObjTimeSplit[i].monthDet;
				record.jo_details_to_jo_planning.jo_year = listObjTimeSplit[i].yearDet;
				record.jo_details_to_jo_planning.days_number = dayNumber;
				if (record.days_import != null)
					totAm = Math.round( (dayNumber * record.days_import) * Math.pow(10, 2)) / Math.pow(10, 2);
				else
					totAm = Math.round( (dayAmount * listObjTimeSplit[i].dayWorkable) * Math.pow(10, 2)) / Math.pow(10, 2);
				//application.output('-----arrotondato ' + totAm);
				record.jo_details_to_jo_planning.total_amount = totAm;
				if (record.jo_details_to_profit_cost_types.profit_cost_acr == 'R')
					record.jo_details_to_jo_planning.return_amount = totAm;
				else
					record.jo_details_to_jo_planning.cost_amount = totAm;
			}
			//altrimenti modifico l'importo
			else {
				record.jo_details_to_jo_planning.days_number = dayNumber;
				if (record.days_import != null)
					totAm = Math.round( (dayNumber * record.days_import) * Math.pow(10, 2)) / Math.pow(10, 2);
				else
					totAm = Math.round( (dayAmount * listObjTimeSplit[i].dayWorkable) * Math.pow(10, 2)) / Math.pow(10, 2);
				//application.output('-----arrotondato ' + totAm);
				record.jo_details_to_jo_planning.total_amount = totAm;
				if (record.jo_details_to_profit_cost_types.profit_cost_acr == 'R')
					record.jo_details_to_jo_planning.return_amount = totAm;
				else
					record.jo_details_to_jo_planning.cost_amount = totAm;
			}
		}
	}
	databaseManager.saveData(foundset.jo_details_to_jo_planning);

}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @private
 *
 * @properties={typeid:24,uuid:"EA434C91-AB2B-4869-9C5C-165444308770"}
 * @AllowToRunInFind
 */
function deleteRecord(event, index) {
	var record = foundset.getSelectedRecord();
	//application.output(record.profit_cost_type_id);
	application.output(scopes.globals.hasRole('Controllers'))
	var listToDelete = [];
	if (record == null) return;
	if (!scopes.globals.hasRole('Controllers') && [4, 11, 12, 6].indexOf(record.profit_cost_type_id) > -1) {
		globals.DIALOGS.showErrorDialog('Errore', 'Non è possibile cancellare voci di tipo Allineamento amministrativo Ricavi/Costi e Fatture Attive/Passive', 'OK');
		return;
	}
	var answer = globals.DIALOGS.showWarningDialog("Conferma", "Proseguendo, il Forecast della voce selezionata sarà eliminato e i relativi valori non saranno recuperabili.", "Elimina", "Annulla");
	if (answer == "Elimina") {
		var tot = 0;

		/** @type {JSFoundSet<db:/geco/jo_planning>} */
		//var jo_planning = databaseManager.getFoundSet('geco', 'jo_planning');
		var jo_planning = record.jo_details_to_jo_planning;
		jo_planning.loadAllRecords();
		if (jo_planning.find()) {
			jo_planning.jo_details_id = record.jo_details_id;
			jo_planning.is_actual = 0;
			tot = jo_planning.search();
			application.output('piani non actual ' + tot)
		}
		for (var i = 1; i <= jo_planning.getSize(); i++) {
			var rec = jo_planning.getRecord(i);
			application.output(rec);
			listToDelete.push(rec);
		}

		for (var t = 0; t < listToDelete.length; t++) {
			/** @type {JSRecord<db:/geco/jo_planning>} */
			var recToDel = listToDelete[t];
			jo_planning.deleteRecord(recToDel);
		}

		databaseManager.saveData(jo_planning);

		//cerco se ha mesi actual
		if (jo_planning.find()) {
			jo_planning.jo_details_id = record.jo_details_id;
			jo_planning.is_actual = 1;
			tot = jo_planning.search();
			application.output('piani actual ' + tot)
		}

		//se non ha mesi in stato actual elimino il dettaglio
		if (tot == 0) {
			application.output('record da cancellare ' + record.jo_details_id)
			try {
				var deleted = foundset.deleteRecord(record);
				application.output('cancellato ' + deleted)
			} catch (e) {
				application.output('errore in cancellazione gestito ' + e);
			}

		}
		application.output('chiamo updateUi')
		foundset.loadRecords();
		foundset.sort('jo_details_to_profit_cost_types.profit_cost_acr desc, jo_details_to_profit_cost_types.order_display asc, description asc');

	} else return;
}

/**
 * Handle changed data.
 *
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"6C0E33E9-3E89-440F-8CF7-74E37F137F9A"}
 * @AllowToRunInFind
 */
function onDataChangeRealFigureNew(oldValue, newValue, event) {
	application.output(event);
	application.output(newValue)
	var ar = newValue != null ? newValue.split(' - ') : null;
	application.output(ar);

	//application.output(elements.realTM.getValueListName())
	if (oldValue != newValue) {
		if (globals.isEmpty(newValue)) {
			foundset.real_figure = null;
			foundset.enrollment_number = null;
			foundset.days_import = null;
			foundset.days_number = foundset.days_number ? foundset.days_number : null;
			foundset.description = foundset.standard_figure ? foundset.jo_details_to_standard_professional_figures.description : null;
			foundset.total_amount = foundset.standard_figure && foundset.standard_import && foundset.days_number ? globals.roundNumberWithDecimal( (foundset.standard_import * foundset.days_number), 2) : null;
			updateUI(event);
		} else {
			/** @type {globals.objUserCost} */
			var obj = {
				uid: null,
				cost: 0.0,
				name: '',
				enrollNum: ''
			}
			if (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 9) {
				obj = globals.getLastUserCost('' + foundset.user_id, foundset.enrollment_number)
			}
			//se la tipologia scelta NON è SPESA, metto il total_amount a 0, altrimenti lascio il valore indicato da utente.
			if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id != 10) {
				foundset.total_amount = 0;
			}
			foundset.days_import = null;
			//personale
			if ( (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 9) && newValue != null) {
				obj = globals.getLastUserCost(ar[0], ar[1]);
				application.output(obj);
				foundset.days_import = obj.cost;
				foundset.enrollment_number = ar[1];
				foundset.description = obj.name;

				if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10) {
					foundset.real_figure = ar[0];
					foundset.user_id = ar[0];
					if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10) foundset.days_import = null;
				} else if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 9) {
					foundset.real_tm_figure = ar[0];
					foundset.user_id = ar[0];
				}
			}

			if ( (foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 9 || foundset.jo_details_to_profit_cost_types.profit_cost_types_id == 10) && (foundset.description != null && newValue != null)) {
				elements.labale_st_cost.visible = false;
				elements.standAmount.visible = false;
			} else {
				//se non è una spesa e real_figure è null, allora ti mostro Costo Standard/gg, altrimenti no.
				if (foundset.jo_details_to_profit_cost_types.profit_cost_types_id != 10 && newValue == null) {
					elements.labale_st_cost.visible = true;
					elements.standAmount.visible = true;
				}
				foundset.description = null;
			}

			if (foundset.days_number != null) {
				var dayToChange = foundset.days_number - foundset.days_number_actual;
				var costReal = globals.roundNumberWithDecimal( (foundset.cost_actual + (dayToChange * obj.cost)), 2);
				foundset.total_amount = costReal;
			}
		}
	}
	return true;
}
