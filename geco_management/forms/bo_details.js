/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"C24A285F-F69E-47CE-B37D-30C2F40D701B",variableType:-4}
 */
var isEnabled = false;

/**
 * @param {JSEvent} event
 * @param {String} formName
 * @param {String} relationName
 *
 * @properties={typeid:24,uuid:"3BBC5498-CBE3-4DD0-BEEE-4BA69DDEDBBB"}
 */
function loadForm(event, formName, relationName) {
	//application.output('----------------loadForm '+globals.lockForTab)
	if (foundset.getSelectedRecord() && foundset.getSelectedRecord().isNew() && isEditing() && globals.lockForTab){
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!", "OK");
		return;
	}
	loadTab(event, formName, relationName);
	if (foundset.getSelectedRecord() && foundset.getSelectedRecord().isNew()) globals.lockForTab = true;
	updateUI(event);
}

/**
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"83D05A37-4DD5-4D40-A214-5D585A3A994A"}
 */
function updateUI(event) {
	
	isEnabled = foundset.is_editable == 1;	
	if (foundset.mol < 30) {
		//application.output('bo_details.updateUI ' + foundset.bo_to_bo_details.mol_per_display);
		elements.molPer.fgcolor = 'red';
	} else elements.molPer.fgcolor = null;

	//elements.buttonExportBC.visible = globals.hasBusinessCase(foundset.bo_id);
}

/**
 * @param {JSEvent} event
 * @param {String} formName
 * @param {String} relationName
 *
 * @properties={typeid:24,uuid:"C0926437-2626-46F5-B3CC-FF7BF77776E1"}
 * @AllowToRunInFind
 */
function loadTab(event, formName, relationName) {
	_super.loadTab(event, formName, relationName)
	//application.output('load tab linked bo ' + globals.bo_link_selected + ' bo selected '+ foundset.bo_id  )
	if (globals.bo_link_selected != null){
		if(foundset.find()){
			foundset.bo_id = globals.bo_link_selected;
			foundset.search();
		}
	}
	globals.bo_selected = globals.bo_link_selected!=null?globals.bo_link_selected:foundset.bo_id;
	globals.bo_link_selected = null;
	//application.output('BO selected = ' + globals.bo_selected);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @return {String}
 *
 * @properties={typeid:24,uuid:"2A0594BB-E74C-45DC-B9EA-E5D33CED447E"}
 * @AllowToRunInFind
 */
function getBusinessCase(event) {
	application.output(globals.messageLog + 'START bo_details.getBusinessCase() codice ' + foundset.bo_code, LOGGINGLEVEL.DEBUG);
	//application.output('getBusinessCase codice ' + foundset.bo_code);

	// prepare the report the way you usually do (putting any Servoy object in a context):
	var context = new Object();
	var totDetails = 0;
	context.foundsetSintesi = foundset;
	/** @type {JSFoundSet<db:/geco/bo_details>} */
	var bo_details = databaseManager.getFoundSet('geco', 'bo_details');
	if (bo_details.find()) {
		bo_details.bo_id = foundset.bo_id;
		totDetails = bo_details.search();
	}
//	application.output('getBusinessCase dettagli trovati ' + totDetails)
//	application.output('getBusinessCase ' + bo_details)
	if (totDetails > 0) {
		bo_details.bo_details_to_bo_planning.loadAllRecords();
	}
	bo_details.sort('bo_details_to_profit_cost_types.profit_cost_acr desc ');
	for (var index = 1; index <= bo_details.getSize(); index++) {
		var recoDet = bo_details.getRecord(index);
		//application.output(recoDet.bo_details_id);
		recoDet.bo_details_to_bo_planning.loadAllRecords();
//		for (var i = 1; i <= recoDet.bo_details_to_bo_planning.getSize(); i++) {
//			var reco = recoDet.bo_details_to_bo_planning.getRecord(i);
//			application.output('plID ' + reco.bo_planning_id + ' ' + reco.bo_year + '-' + reco.bo_month + ' total ' + reco.total_amount + ' ' + reco.bo_planning_to_bo_details.bo_details_to_profit_cost_types.profit_cost_description);
//		}

	}
	context.foundsetDati = bo_details.getSize()>0 ? bo_details: null;
	//application.output('getBusinessCase ****************\n' + context.foundsetDati)

	context.ricavoTotale = foundset.bo_to_bo_details.total_return;
	//application.output('getBusinessCase ----------------------' + context.ricavoTotale)
	context.costoTotale = foundset.bo_to_bo_details.total_cost;
	context.mol = foundset.bo_to_bo_details.mol_dispaly?foundset.bo_to_bo_details.mol_dispaly:'NA';
	context.molPer = foundset.bo_to_bo_details.mol_per_display?foundset.bo_to_bo_details.mol_per_display:'NA';
	plugins.VelocityReport.installFonts();

	// we use a template that should be located in the default velocity reports folder:
	var template = 'excelTemplateBO.xml';

	// now let's render the report into a String:
	var excel = plugins.VelocityReport.renderTemplate(template, context);

	//	application.output(excel);
	
	application.output(globals.messageLog + 'STOP bo_details.getBusinessCase() ' , LOGGINGLEVEL.DEBUG);	
	return excel;

}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"58DDA31B-0143-4D0E-B694-DCF6BC0AA3F9"}
 */
function saveLocalBC(event) {
	application.output(globals.messageLog + 'START bo_details.saveLocalBC() ' , LOGGINGLEVEL.DEBUG);
	//if (!forms.bo_tab_details.hasBusinessCase()) return;
	var excel = globals.getBusinessCase(event,foundset.bo_id);
	plugins.file.writeTXTFile('BC_' + foundset.bo_code + '.xls', excel, 'UTF-8', 'application/vnd.ms-excel');
	application.output(globals.messageLog + 'STOP bo_details.saveLocalBC() ' , LOGGINGLEVEL.DEBUG);
}
