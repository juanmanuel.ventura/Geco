/**@type {String}
 *
 * @properties={typeid:35,uuid:"5F3852A8-CE38-46E8-A457-C01F75A652A7"}
 */
var dynamicValuelist = null;

/**
 * @type {JSDataSet}
 *
 * @properties={typeid:35,uuid:"84A324D2-6BB7-4707-9920-B027E5294717",variableType:-4}
 */
var dynamicDataset = null;

/**@type {Array<String>}
 *
 * @properties={typeid:35,uuid:"E7AC4B91-72A7-44D5-AA17-9E84570C1539",variableType:-4}
 */
var solutionsList = [];

/**
 * @param event
 *
 * @properties={typeid:24,uuid:"2D2E750E-F689-4F3D-A50D-BA9920B7D53D"}
 */
function initialize(event) {
	application.output(globals.messageLog + 'START main_bo.initialize() ', LOGGINGLEVEL.INFO);
	_super.initialize(event);
	//, 'Comm. Support' da aggiungere sotto quando si implementa definitivamente
	if (!globals.grantAccess(['Delivery Admin', 'Responsabili Commessa', 'Controllers', 'Resp. Mercato', 'Orders Admin', 'Planner', 'Steering Board', 'Supp. Commerciale','Vice Resp. Mercato'])) {
		logOut(event);
	}
	//&& !globals.hasRole('Comm. Support') da aggiungere sotto quando si implementa definitivamente
	if (!globals.hasRole('Delivery Admin') && !globals.hasRole('Resp. Mercato') && !globals.hasRole('Steering Board') && !globals.hasRole('Controllers') && !globals.hasRole('Orders Admin') && !globals.hasRole('Planner') && !globals.hasRole('Supp. Commerciale') && !globals.hasRole('Vice Resp. Mercato')) {
		//solo RDC
		elements.button1.visible = false;
		elements.button1.enabled = false;
		elements.button2.setLocation(elements.button1.getLocationX(), elements.button1.getLocationY());
		elements.button2.setSize(155, 36);
		elements.selector.setSize(elements.button2.getWidth(), elements.button2.getHeight());
		var x = elements.button2.getLocationX() + elements.button2.getWidth();
		elements.button3.setLocation(x, elements.button2.getLocationY());
		loadPanel(event, 'jo_bundle');
	} 
	else loadPanel(event, 'bo_bundle');

	//	scommentare quando si iplementa definitivamente
	if (globals.hasRole('Supp. Commerciale') && !globals.hasRole('Responsabili Commessa') && !globals.hasRole('Resp. Mercato') && !globals.hasRole('Vice Resp. Mercato')) {
		elements.button2.visible = false;
		elements.button2.enabled = false;
		var z = elements.button1.getLocationX() + elements.button1.getWidth();
		elements.button3.setLocation(z, elements.button1.getLocationY());
	}

	elements.buttonCaricaCosti.visible = globals.hasRole('Controllers');
	elements.buttonEnableActual.visible = globals.hasRole('Controllers');
//	dynamicDataset = databaseManager.createEmptyDataSet();
//	if (globals.hasRole('Approvers')) solutionsList.push('Approver');
//	if (globals.hasRole('Controllers')) solutionsList.push('Controller');
//	if (globals.hasRole('HR Administrators') || globals.hasRole('HR Expenses') || globals.hasRole('Admin Readers')) solutionsList.push('Personnel');
////	if (globals.hasRole('Delivery Admin') || globals.hasRole('Responsabili Commessa') || globals.hasRole('Controllers') || globals.hasRole('Resp. Mercato') || globals.hasRole('Orders Admin') || globals.hasRole('Planner') || globals.hasRole('Supp. Commerciale')) solutionsList.push('Management');
//	if (globals.currentUserDisplayName == 'STAFFA JONATHAN' || globals.currentUserDisplayName == 'FRAU DANIELA') solutionsList.push('Configurator');
//	solutionsList.push('Logger');
//	application.output('solutionsList.length: ' + solutionsList.length + '; solutionsList: ' + solutionsList);
//	if (solutionsList.length > 0) {
//		solutionsList.sort();
//		for (var index = 0; index < solutionsList.length; index++) {
//			dynamicDataset.addRow(new Array(solutionsList[index]));
//		}
//		elements.dynamicSolutionsChoice.setValueListItems(dynamicDataset);
////		elements.lSolutionName.enabled = true;
////		elements.lSolutionName.visible = true;
//		elements.dynamicSolutionsChoice.enabled = true;
//		elements.dynamicSolutionsChoice.visible = true;
//	}
	elements.buttonCommercialSupport.visible = globals.hasRole('Resp. Mercato') //&& !globals.hasRole('Vice Resp. Mercato');

	/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
	var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
	bo_actual_date.loadRecords();
	scopes.globals.actualDate = bo_actual_date.getRecord(1).actual_date;
	scopes.globals.actualMonth = bo_actual_date.getRecord(1).actual_month;
	scopes.globals.actualYear = bo_actual_date.getRecord(1).actual_year;
	scopes.globals.isOpenToActual = bo_actual_date.is_open_to_actual;
	scopes.globals.actualMonthStrIT = scopes.globals.monthName[scopes.globals.actualMonth - 1];

	if (globals.hasRole('Orders Admin') || globals.hasRole('Controllers') || globals.hasRole('Steering Board') || globals.hasRole('Delivery Admin')) {
		elements.button4.visible = true;
		elements.button4.enabled = true;
	}
	//loadValulist();
	application.output(globals.messageLog + 'STOP main_bo.initialize() ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"8BD04841-2A92-478F-B207-1CCA15AFC7F0"}
 */
function loadFile(event) {
	var win = application.createWindow("Nuovo Documento", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 900, 250);
	win.title = 'Carica File'
	win.show(forms.dialog_import_file);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"359C9E8C-704A-481D-B558-2EA051EF10FC"}
 */
function enableActual(event) {
	var win = application.createWindow("Nuovo Documento", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 425, 250);
	win.title = 'Abilita Actual'
	win.show(forms.dialog_enable_actual);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"EB5D29F3-3D49-494A-A120-A12D36B4BBE6"}
 * @private
 */
function setSelector(event) {
	_super.setSelector(event);

	if (event.getType() == JSEvent.ACTION && (event.getElementName() == 'showBo' || event.getElementName() == 'showJo')) {
		// move the selector
		/** @type {RuntimeLabel} */
		var label = event.getSource();
		if (event.getElementName() == 'showBo') {
			label = elements.button1;
		} else if (event.getElementName() == 'showJo') {
			label = elements.button2;
		}

		elements.selector.setLocation(label.getLocationX(), label.getLocationY());
		elements.selector.setSize(label.getWidth(), label.getHeight());

	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"74039156-EA30-453D-979D-D1FD8D37D460"}
 */
function openCommSupp(event) {
	var win = application.createWindow("Nuovo Documento", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 480, 400);
	win.title = 'Supporto Commerciale'
	win.show(forms.comm_supp_customers_popup);
}

/**
 * Callback method for when form is shown.
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"E0BF46D4-60FE-4C90-85AB-C6DB85693FFA"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
	dynamicValuelist = null;
}
