/**
 * @properties={typeid:35,uuid:"5E00FFA2-A763-4AA0-92DB-E2CEB3E6D9B5",variableType:-4}
 */
var passed = false;

/**
 * @properties={typeid:35,uuid:"AFBD2FB6-E006-4DE0-ACE5-864017B50C1B",variableType:-4}
 */
var passedController = false;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"FBB499D2-3C30-4D28-A5EB-601CFC38DC31",variableType:8}
 */
var totCost = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"0B11D403-6E70-41FC-85F0-D3EB07F17CF5",variableType:8}
 */
var totProfit = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"EFA61739-CF08-48F6-8F21-24AA86EAF4B8",variableType:8}
 */
var totCostActual = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"DC7467F6-BF06-487F-A5EE-1DE73BC52F57",variableType:8}
 */
var totProfitActual = 0.0;

/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"B3102F14-33C1-43FE-BE23-EBA4FCFADB58",variableType:-4}
 */
var profit_cost_forOwner = [1, 2, 3, 7, 8, 13]

/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"B2DDA5BA-4DDE-4AD7-8AC6-2668518822BE",variableType:-4}
 */
var profit_cost_forController = [4, 6, 11, 12]

/**
 * @properties={typeid:35,uuid:"77DF85DA-1F4A-46CB-8177-C591E3E477EB",variableType:-4}
 */
var isController = (globals.hasRole('Controllers') || scopes.globals.hasRole('Orders Admin'));

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"27D0AEAC-9128-4072-8F9E-97250FF52F7D",variableType:8}
 */
var recIndexOnStartEdit = null;

/**
 * @param event
 * @SuppressWarnings(unused)
 * @properties={typeid:24,uuid:"A3357CB1-C14B-4F4A-9498-DCDA4BB7C696"}
 */
function updateUI(event) {
	//JS TOLTO CONTROLLO SU QUELLO CHE PUO' MODIFICARE DAVID
	_super.updateUI(event);
	passed = false;
	passedController = false;
	application.output(globals.messageLog + 'START jo_planning_list.updateUI() ', LOGGINGLEVEL.INFO);
	//	application.output(isEditing())

	var canModify = globals.isOwnerForObject(scopes.globals.currentUserId, globals.job_order_selected, 'jo') || globals.isPCManagerForObject(scopes.globals.currentUserId, globals.job_order_selected, 'jo')
	var isEnabled = foundset.getSize() > 0 && foundset.jo_planning_to_job_orders.getSize() > 0 && foundset.jo_planning_to_job_orders.is_enabled == 1;
	var isEnabledJO = forms.jo_details.isEnabled;
	var isBillable = forms.jo_details.isBillable;

	var record = (foundset.getSelectedRecord()) ? foundset.getSelectedRecord() : null;
	var isActual = false;
	var isActualConfirmed = false;
	var isEditable = false
	var isPersonType = false;
	var isToUpdate = false;
	//	var isOnlyForController = false;
	//application.output(record)
	var isInvoice = false;

	// only if there is a record
	if (record != null) {
		var month = record.jo_month;
		var year = record.jo_year;
		var recordCompetence = record.competence_month;
		if ( (record.jo_planning_to_job_orders.job_orders_to_mgm_market_competence != null && scopes.globals.actualDate == record.jo_planning_to_job_orders.job_orders_to_mgm_market_competence.competence_month && record.jo_planning_to_job_orders.job_orders_to_mgm_market_competence.actual_confirmation == 1 && record.jo_planning_to_job_orders.job_orders_to_mgm_market_competence.competence_month == recordCompetence) || year < scopes.globals.actualYear || (year == scopes.globals.actualYear && (month < scopes.globals.actualMonth || (month == scopes.globals.actualMonth && scopes.globals.isOpenToActual == 0 && scopes.globals.statusActual == 3)))) {
			passed = true;
		}

		if (year < scopes.globals.actualYear || (year == scopes.globals.actualYear && (month < scopes.globals.actualMonth || (month == scopes.globals.actualMonth && scopes.globals.isOpenToActual == 0 && scopes.globals.statusActual == 3)))) {
			passedController = true;
		}
		//		isOnlyForController = [4, 6, 11, 12].indexOf(record.jo_planning_to_jo_details.profit_cost_type_id) > -1;
		isInvoice = record.jo_planning_to_jo_details != null && record.jo_planning_to_jo_details.profit_cost_type_id == 6 || record.jo_planning_to_jo_details.profit_cost_type_id == 12;
		isActual = (record.is_actual == 1) ? true : false;
		isActualConfirmed = (record.is_actual_confirmed == 1) ? true : false;
		//se controller
		if (isController) {
			isEditable = passedController;
			//			isOnlyForController = [4, 6, 11, 12].indexOf(record.jo_planning_to_jo_details.profit_cost_type_id) > -1
		}
		//0,1,2,5,6,7,8,9
		//per RCP non editabile se status in 2,5,8
		else if (globals.isPCManagerForObject(scopes.globals.currentUserId, foundset.jo_id, 'jo')) {
			isEditable = (record.status != 5 && record.status != 8) ? true : false;
			//isEditable = passed;

		}
		//per RDC non editabile se status in 1,2,5,6,8,
		else if (globals.isOwnerForObject(scopes.globals.currentUserId, foundset.jo_id, 'jo')) {
			isEditable = (record.status == null || record.status == 0 || record.status == 7 || record.status == 9) ? true : false;
		}

		isPersonType = (record.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || record.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_types_id == 9) ? true : false
		isToUpdate = (record.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_types_id != 5 && record.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_types_id != 9 && record.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_types_id != 10) ? true : false;
	}

	//	application.output('actualConfirmed : ' + isActualConfirmed + ' actual : ' + isActual + ' is_to_update ' + isToUpdate + ' is_editable ' + isEditable + ' passed ' + passed  + ' passedController ' + passedController)
	// buttons
	if ( (isController && passedController) || (!isController && passed)) {
		elements.buttonEdit.visible = false;
		elements.buttonDelete.visible = false;
	} else {
		//		elements.buttonEdit.visible = !isEditing() && isEnabled && isEnabledJO && canModify && isBillable && !isOnlyForController && !isActualConfirmed || (!isEditing() && isController);
		elements.buttonEdit.visible = !isEditing() && isEnabled && isEnabledJO && canModify && isBillable && !isActualConfirmed || (!isEditing() && isController);
		//		isEnabled ? elements.buttonDelete.visible = !isEditing() && isEnabled && isEnabledJO && canModify && isBillable && !isActual && !isActualConfirmed && !isOnlyForController || (!isEditing() && isController) : false;
		isEnabled ? elements.buttonDelete.visible = !isEditing() && isEnabled && isEnabledJO && canModify && isBillable && !isActual && !isActualConfirmed || (!isEditing() && isController) : false;
	}

	//	application.output('buttonEdit.visible = ' + elements.buttonEdit.visible + '; buttonDelete.visible = ' + elements.buttonDelete.visible);
	elements.buttonCancel.visible = isEditing();
	elements.buttonSave.visible = isEditing();

	elements.buttonAdd.visible = !isEditing() && isEnabledJO && canModify && isBillable || (!isEditing() && isController);
	elements.buttonCancel.visible = isEditing();

	//fileds
	elements.daysImport.visible = isPersonType;
	elements.daysNumber.visible = isPersonType;
	elements.daysImportActual.visible = isPersonType;
	elements.daysNumberActual.visible = isPersonType;

	elements.title_daysImport.visible = isPersonType;
	elements.title_daysNumber.visible = isPersonType;
	elements.title_daysNumberActual.visible = isPersonType;
	elements.title_daysNumberActual2.visible = isPersonType;
	elements.title_daysImportActual.visible = isPersonType;
	elements.title_daysImportActual2.visible = isPersonType;
	elements.title_daysNumberActual.visible = isPersonType;

	//	elements.daysImport.editable = isEditing() && !passed;
	//	elements.daysNumber.editable = isEditing() && !isActual && isEditable || (isEditing() && !passed)// && is_person_type;
	//	elements.totalAmount.editable = (isEditing() && !isPersonType && !isActual && isEditable && !isOnlyForController) || (isEditing() && !passed);

	elements.daysImport.editable = isController ? (isEditing() && !passedController) : (isEditing() && !passed && isEditable && !isActualConfirmed);
	elements.daysNumber.editable = isController ? (isEditing() && !passedController) : (isEditing() && !passed && isEditable && !isActualConfirmed);
	elements.totalAmount.editable = isController ? (isEditing() && !passedController) : (isEditing() && !passed && isEditable && !isActualConfirmed);

	elements.totalActual.editable = isController ? (isEditing() && !passedController) : (isEditing() && !passed && isEditable && !isActualConfirmed);
	elements.daysImportActual.editable = isController ? (isEditing() && !passedController) : (isEditing() && !passed && isEditable && !isActualConfirmed);
	elements.daysNumberActual.editable = isController ? (isEditing() && !passedController) : (isEditing() && !passed && isEditable && !isActualConfirmed);

	//	elements.totalActual.editable = isController ? (isEditing() && !passedController) : (isEditing() && !passed && !isActualConfirmed);
	//	elements.daysImportActual.editable = isController ? (isEditing() && !passedController) : (isEditing() && !passed && !isActualConfirmed);
	//	elements.daysNumberActual.editable = isController ? (isEditing() && !passedController) : (isEditing() && !passed && !isActualConfirmed);

	// il controller deve poter modificare
	elements.detailsc.editable = isController ? (isEditing() && !passedController) : (isEditing() && !passed);
	elements.criterion.editable = isController ? (isEditing() && !passedController) : (isEditing() && !passed);

	elements.daysImportActual.transparent = !isController && isActualConfirmed;
	elements.totalActual.transparent = !isController && isActualConfirmed;
	elements.daysNumberActual.transparent = !isController && isActualConfirmed;

	elements.isActual.visible = isEditing() && isController && (scopes.globals.isOpenToActual == 1 || (scopes.globals.isOpenToActual == 0 && scopes.globals.statusActual == 1)) && !isInvoice;
	elements.isActual.enabled = isEditing() && isController && (scopes.globals.isOpenToActual == 1 || (scopes.globals.isOpenToActual == 0 && scopes.globals.statusActual == 1)) && !isInvoice;

	if (isEnabled) {
		elements.singleReject.visible = !isEditing() && isController && (record.status == 1 || record.status == 2) && (scopes.globals.isOpenToActual == 1 || (scopes.globals.isOpenToActual == 0 && scopes.globals.statusActual == 1)) && !isInvoice;
		elements.singleReject.enabled = !isEditing() && isController && (record.status == 1 || record.status == 2) && (scopes.globals.isOpenToActual == 1 || (scopes.globals.isOpenToActual == 0 && scopes.globals.statusActual == 1)) && !isInvoice;
	} else {
		elements.singleReject.visible = false;
		elements.singleReject.enabled = false;
	}
	if (!isEditing()) {
		foundset.sort('jo_year asc, jo_month asc, jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_acr desc, jo_planning_to_jo_details.jo_details_to_profit_cost_types.order_display asc');
		//jo_year asc, jo_month asc, jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_acr desc, jo_planning_to_jo_details.jo_details_to_profit_cost_types.order_display asc
	}

	forms.jo_details.updateUI(event);
	application.output(globals.messageLog + 'STOP jo_planning_list.updateUI() ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8F5F2725-E8D1-4FFD-9083-38ECBC80BC09"}
 */
function openNewPlain(event) {
	application.output(globals.messageLog + 'START jo_planning_list.openNewPlain() ', LOGGINGLEVEL.INFO);
	//	application.output(globals.job_order_selected);
	//globals.job_order_selected = (foundset.jo_id!=null);
	//	application.output(globals.job_order_selected);
	var win = application.createWindow("Nuovo piano", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 750, 350);
	win.title = 'Nuovo piano mensile'
	win.show(forms.jo_new_planning_popup);
	scopes.globals.callerForm = event.getFormName();
	application.output(globals.messageLog + 'STOP jo_planning_list.openNewPlain() ', LOGGINGLEVEL.INFO);
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"4829964E-D82E-4112-9C71-D39F8F214AEE"}
 */
function onDataChangeDayNumber(oldValue, newValue, event) {
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (foundset.days_import != null) {
			foundset.total_amount = foundset.days_import * foundset.days_number;
			foundset.cost_amount = foundset.total_amount
		}
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
	}
	return true
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"1D8E7B1E-0146-4246-8B9F-A35B0A1BE409"}
 */
function onDataChangeDayNumberActual(oldValue, newValue, event) {
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (foundset.days_import_actual != null) {
			foundset.total_actual = foundset.days_import_actual * foundset.days_number_actual;
			foundset.cost_actual = foundset.total_actual
		}
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
	}
	return true
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 * @properties={typeid:24,uuid:"1A948FF2-147C-4C47-9DDB-54CC50BDB704"}
 */
function onDataChangeTotalAmount(oldValue, newValue, event) {
	if (oldValue != newValue) {
		if (foundset.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_acr == 'R') {
			if (foundset.jo_planning_to_jo_details.profit_cost_type_id != 12) {
				foundset.return_amount = foundset.total_amount;
			} else {
				foundset.invoice_active = foundset.total_amount;
				foundset.return_amount = null;
			}
		} else {
			if (foundset.jo_planning_to_jo_details.profit_cost_type_id != 6) {
				foundset.cost_amount = foundset.total_amount;
			} else {
				foundset.invoice_payable = foundset.total_amount;
				foundset.cost_amount = null;
			}
		}
	}
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"1939FE23-13ED-4933-918D-8FE7DC761C77"}
 */
function onDataChangeDaysImport(oldValue, newValue, event) {
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (foundset.days_number != null) {
			foundset.total_amount = foundset.days_import * foundset.days_number;
			foundset.cost_amount = foundset.total_amount
		}
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
	}
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"915E075E-A3AB-4BA3-BB22-A196B0CE4323"}
 */
function onDataChangeDaysImportActual(oldValue, newValue, event) {
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (foundset.days_number_actual != null) {
			foundset.total_actual = foundset.days_import_actual * foundset.days_number_actual;
			foundset.cost_actual = foundset.total_actual
		}
		//application.output('foundset.total_amount 2 --- ' + foundset.total_amount);
	}
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"FFCB5D49-25C7-4C8B-84A5-ED4D55129EE8"}
 */
function onDataChangeIsActual(oldValue, newValue, event) {
	if (oldValue != newValue) {

		if (newValue == 1) {
			foundset.date_actual = new Date();
			foundset.competence_month = globals.actualDate;
			foundset.user_actual = scopes.globals.currentUserId;
			foundset.status = scopes.globals.hasRole('Controllers') ? 5 : null;
			foundset.is_actual_confirmed = 1;
			if (foundset.jo_planning_to_jo_details.profit_cost_type_id != 6 && foundset.jo_planning_to_jo_details.profit_cost_type_id != 12) {
				foundset.total_actual = (foundset.total_actual == null) ? (foundset.total_amount != null ? foundset.total_amount : 0) : foundset.total_actual;
				if (foundset.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_acr == 'R') {
					foundset.return_actual = foundset.total_actual;
				} else {
					foundset.cost_actual = foundset.total_actual;
				}
				//foundset.cost_actual = foundset.cost_amount;
				//foundset.return_actual = foundset.return_amount;
				foundset.days_number_actual = (foundset.days_number_actual == null) ? (foundset.days_number != null ? foundset.days_number : 0) : foundset.days_number_actual;
				foundset.days_import_actual = (foundset.days_import_actual == null) ? (foundset.days_import != null ? foundset.days_import : 0) : foundset.days_import_actual;
				//foundset.days_number_actual = foundset.days_number;
				//foundset.days_import_actual = foundset.days_import;
			}
		} else {
			//se Controller toglie actual da un piano a carico del RDC o RM riapro il mercato
			if (profit_cost_forOwner.indexOf(foundset.jo_planning_to_jo_details.profit_cost_type_id) != -1) {
				if (foundset.jo_planning_to_job_orders.job_orders_to_mgm_market_competence.competence_month == globals.actualDate && foundset.competence_month == globals.actualDate && foundset.jo_planning_to_job_orders.job_orders_to_mgm_market_competence.actual_confirmation == 1) {
					foundset.jo_planning_to_job_orders.job_orders_to_mgm_market_competence.actual_confirmation = 0;
					foundset.is_actual_confirmed = 0;
				}
			}

			foundset.date_actual = null;
			foundset.competence_month = null;
			foundset.user_actual = null;
			foundset.status = null;
			//foundset.total_actual = 0;
			//foundset.cost_actual = 0;
			//foundset.return_actual = 0;
			//foundset.days_number_actual = 0;
			//foundset.days_import_actual = 0;
		}

	}

	return true
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @properties={typeid:24,uuid:"9C884012-D14F-488F-AD4F-8AB324055A39"}
 */
function deleteRecord(event, index) {
	if (foundset.getSize() > 0) {
		if (passed == true && !globals.hasRole('Controllers')) {
			globals.DIALOGS.showErrorDialog('ERRORE', 'Non è possibile procedere con la cancellazione di dati con competenze inferiori alla data attuale.', 'OK');
			return;
		}
		_super.deleteRecord(event, index);
	}
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"0B07F484-4E6D-42D9-8E2C-EF051DC08DF1"}
 */
function onDataChangeTotalAmountActual(oldValue, newValue, event) {
	if (oldValue != newValue) {
		if (foundset.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_acr == 'R') {
			if (foundset.jo_planning_to_jo_details.profit_cost_type_id != 12) {
				foundset.return_actual = foundset.total_actual;
			} else {
				foundset.invoice_active = foundset.total_actual;
				foundset.return_actual = null;
			}
		} else {
			if (foundset.jo_planning_to_jo_details.profit_cost_type_id != 6) {
				foundset.cost_actual = foundset.total_actual;
			} else {
				foundset.invoice_payable = foundset.total_actual;
				foundset.cost_actual = null;
			}
		}
	}
	return true
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"74AAF3BE-2462-468E-A7E4-51DED2704076"}
 * @AllowToRunInFind
 */
function rejectSiglePlain(event) {
	application.output(globals.messageLog + 'START jo_planing_lis.rejectSinglePlain ', LOGGINGLEVEL.INFO);
	var rejectionReason = globals.DIALOGS.showInputDialog("Rifiuta", "Inserisci il motivo del rifiuto (obbligatorio)");
	application.output(globals.messageLog + 'jo_planing_lis.rejectSinglePlain rejectionReason: ' + rejectionReason, LOGGINGLEVEL.DEBUG);
	if (globals.isEmpty(rejectionReason) || rejectionReason == '' || rejectionReason.equals(null)) {
//		globals.DIALOGS.showErrorDialog('Errore', 'Il motivo del rifiuto è obbligatorio', 'OK');
		application.output(globals.messageLog + 'jo_planing_lis.rejectSinglePlain motivo rifiuto non valido ', LOGGINGLEVEL.DEBUG);
		return;
	}

	startEditing(event);

	foundset.status = 9; //rifiutato da RPC o controller
	foundset.note_actual_confirmation = rejectionReason;
	foundset.is_actual_confirmed = 0;
	foundset.user_actual = scopes.globals.currentUserId;
	foundset.date_actual = new Date();
	application.output(' modificato ' + foundset.jo_planning_id);

	/** @type {JSFoundSet<db:/geco/mgm_market_competence>} */
	var mgmActualCompetence = databaseManager.getFoundSet('geco', 'mgm_market_competence');
	/** @type {Number} */
	var totMgmActualCompetence = 0;

	application.output('scopes.globals.actualDate: ' + scopes.globals.actualDate);

	if (mgmActualCompetence.find()) {
		mgmActualCompetence.competence_month = scopes.globals.actualDate;
		mgmActualCompetence.profit_center_id = foundset.jo_planning_to_job_orders.profit_center_id;
		totMgmActualCompetence = mgmActualCompetence.search();
	}

	application.output('totMgmActualCompetence: ' + totMgmActualCompetence);

	//trovato il record confermato da Responsabile, lo modifico
	if (totMgmActualCompetence != 0) {
		var rec = mgmActualCompetence.getRecord(1);
		if (rec.actual_confirmation == 1) rec.actual_confirmation = 0;
	}

	_super.saveEdits(event);

	//invio mail al RDC per notifica rifiuto!!!!!
	if (foundset.jo_planning_to_job_orders.user_owner_id != scopes.globals.currentUserId) {
		globals.sendNotifyMail(15, foundset.jo_planning_to_job_orders.job_orders_owner_to_users, foundset.jo_planning_to_job_orders.title_display, rejectionReason);
	}

	application.output(globals.messageLog + 'STOP jo_planing_lis.rejectSinglePlain ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"06366FEB-9A80-427B-BE6A-EFA502D1507A"}
 */
function saveEdits(event) {
	_super.saveEdits(event);
	forms.jo_planning_filter.applyFilter(event);
}

/**
 * Handle record selected.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"84C438C7-5FB1-4E56-ABB2-911AD7E4DF11"}
 */
function onRecordSelection(event) {
	_super.onRecordSelection(event);
	if (recIndexOnStartEdit != null && !globals.hasRole('Controllers')) {
		if (recIndexOnStartEdit != foundset.getSelectedIndex()) {
			var recToSelect = foundset.getSelectedRecord(); //record nuovo che ha cliccato
			if (recToSelect.status == 5) {
				globals.DIALOGS.showInfoDialog("Attenzione", "Non puoi editare un record in stato Actual!", "OK");
				foundset.setSelectedIndex(recIndexOnStartEdit);
				return;
			} else recIndexOnStartEdit = foundset.getSelectedIndex();
		}
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8D73CE93-5C24-4F43-8DFF-675F09A3BECD"}
 */
function startEditing(event) {
	_super.startEditing(event);
	recIndexOnStartEdit = foundset.getSelectedIndex();
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"D3F773C0-3C75-42E2-9034-6A81880C9BD1"}
 */
function stopEditing(event) {
	_super.stopEditing(event);
	recIndexOnStartEdit = null;
}
