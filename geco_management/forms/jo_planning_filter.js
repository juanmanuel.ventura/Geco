
/**
 * @type {String}
 * @properties={typeid:35,uuid:"944446D6-1823-4AAA-A3FE-326E33EF91FD"}
 */
var type = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"FBBC42CA-8336-4841-811F-B7B9D2FCBECB",variableType:8}
 */
var voice = null;

/**
 * valorizzato anno in corso
 * @type {Number}
 * @properties={typeid:35,uuid:"6CEDB942-4929-4873-8CC6-5C36A5DB49D0",variableType:8}
 */
var year = new Date().getFullYear();

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"35D9E31D-867F-4DD1-9E09-CAD95643A179",variableType:8}
 */
var month = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"A178E317-112A-4827-B1A4-814ECE6E8A8C",variableType:4}
 */
var actual = 0;


/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @properties={typeid:24,uuid:"E2025388-68C9-43E4-95BC-C2A7F8634C98"}
 */
function resetFilter(event, goTop) {
	type = null;
	voice = null;
	month = null;
	year = new Date().getFullYear();
	actual = 0;
	applyFilter(event);
}

/**
 * Handle changed data
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"D438F606-225E-40CC-B5AB-B565312554BE"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	if(foundset.find()){
		foundset.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_acr = type;
		foundset.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_types_id = voice;
		foundset.jo_month = month;
		foundset.jo_year = year;
		if (actual == 1) 
			foundset.is_actual = actual;
		//if (confirmed ==1 ) foundset.is_actual_confirmed = confirmed;
		foundset.search();
	}
	foundset.sort('jo_year asc, jo_month asc, jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_acr desc, jo_planning_to_jo_details.jo_details_to_profit_cost_types.order_display asc')
	var totalRecord = databaseManager.getFoundSetCount(foundset);
	var totCost = 0.0;
	var totProfit = 0.0;
	var totCostActual = 0.0;
	var totProfitActual = 0.0;
	for (var i = 1; i<=totalRecord; i++){
		var rec= foundset.getRecord(i);
		totProfit = totProfit + rec.return_amount;
		totCost = totCost + rec.cost_amount;
		totCostActual = totCostActual + (rec.is_actual ? rec.cost_actual : 0);
		totProfitActual = totProfitActual + (rec.is_actual ? rec.return_actual : 0);
	}
	forms.jo_planning_list.totCost = totCost;
	forms.jo_planning_list.totProfit = totProfit;
	forms.jo_planning_list.totCostActual = totCostActual;
	forms.jo_planning_list.totProfitActual = totProfitActual;
	
	return true
}
