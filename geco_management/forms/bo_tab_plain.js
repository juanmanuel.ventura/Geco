/**
 * @type {Object}
 * @properties={typeid:35,uuid:"C53DA2B1-74CF-4B0B-B6FC-21DCAFF82CBF",variableType:-4}
 */
var obj = {
	id: null,
	daysNumber: null,
	totalAmount: null,
	costAmount: null,
	returnAmount: null,
	amount: null
}

/**
 * @type {Object[]}
 * @properties={typeid:35,uuid:"27620718-12D2-4A20-898E-09CFCB843675",variableType:-4}
 */
var planningToRevert = [];

/**
 * @type {Object[]}
 * @properties={typeid:35,uuid:"EA7838C5-4D14-4B4A-8255-36F84389DFEE",variableType:-4}
 */
var planningToDelete = []

/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"6CD54021-643D-453A-B0EA-920A23D8EA82",variableType:-4}
 */
var planningChange = []
/**
 * @type {JSRecord[]}
 * @properties={typeid:35,uuid:"EF5D2A95-D0DA-4A31-92FB-65273E57C540",variableType:-4}
 */
var planningChangePrev = []

/**
 * @type {JSRecord[]}
 * @properties={typeid:35,uuid:"D2C32D2C-32C1-4B8E-9696-98B6155D8C74",variableType:-4}
 */
var planningChangeNewPrev = []

/**
 * @type {JSRecord[]}
 * @properties={typeid:35,uuid:"44A60F22-17AB-4590-B883-F614B2029BA5",variableType:-4}
 */
var planningNewToDelete = [];

/**
 * @properties={typeid:35,uuid:"B878DAF3-1B5E-45E9-915C-1BA3BE4F9274",variableType:-4}
 */
var isNewDetails = false;

/**
 * @type {Object} 
 * @properties={typeid:35,uuid:"CC6B758A-636D-487B-AB5A-A273A0575F6D",variableType:-4}
 */
var objDetType = {
	idDetail: 0,
	perDetail: 0.0,
	totalReturnDetail: 0.0,
	totReturnInserted: 0.0
}
/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"826DE1D7-1C06-41E2-8F6E-9A5D81482CDE"}
 */
function updateUI(event) {
	_super.updateUI(event);
	application.output(globals.messageLog + ' bo_tab_plain.updateUI() NUOVO DETTAGLIO da POP '+isNewDetails, LOGGINGLEVEL.DEBUG);
	var canModify = globals.isOwnerForObject(scopes.globals.currentUserId, globals.bo_selected, 'bo') || globals.isPCManagerForObject(scopes.globals.currentUserId, globals.bo_selected, 'bo') || globals.hasRole('Controllers')

	var isEditable =  forms.bo_details.isEnabled;
	var bo = forms.bo_details.foundset.getSelectedRecord();
	var jo = bo.bo_to_job_orders;
	//application.output('BO  ' + bo);
	//application.output('JO ' + jo.getSize());
	if (bo && bo.job_order_type_id == 4 && bo.bo_to_job_orders && bo.bo_to_job_orders.is_enabled == 0) {
		isEditable = true;
	}
	
	elements.buttonEdit.visible = canModify && isEditable && !isEditing();
	elements.buttonAdd.visible = canModify && isEditable && !isEditing();
	elements.buttonDelete.visible = canModify && isEditable && !isEditing();
	elements.buttonRecalculate.visible = canModify && isEditable && !isEditing();
	
	if (scopes.globals.hasRole('Controllers')) {
		elements.buttonRecalculate.visible = false;
		elements.button_singleplain.visible = false;
	}
	if (!isEditing())
		foundset.sort('bo_details_to_profit_cost_types.profit_cost_acr desc, bo_details_to_profit_cost_types.order_display asc asc, figure');
	
//	application.output(globals.roundNumberWithDecimal(foundset.total_amount, 2));
//	application.output(globals.roundNumberWithDecimal(foundset.bo_details_to_bo_planning.sum_total_amount, 2))
	if (foundset.getSize() > 0 && globals.roundNumberWithDecimal(foundset.total_amount, 2) < globals.roundNumberWithDecimal(foundset.bo_details_to_bo_planning.sum_total_amount, 2))
		elements.sumTotPlainAmount.fgcolor = 'red';
	else elements.sumTotPlainAmount.fgcolor = null;
	application.output(globals.messageLog + ' bo_tab_plain.updateUI() 2 NUOVO DETTAGLIO da POP '+isNewDetails, LOGGINGLEVEL.DEBUG);
	if (foundset.getSize() > 0) toggleTextFild();

	if (foundset.getSize() == 0) {
		elements.buttonDelete.visible = false;
		elements.buttonEdit.visible = false;
		elements.buttonRecalculate.visible = false;
		elements.button_singleplain.visible = false;
	}
}

/**
 * @properties={typeid:24,uuid:"E4D361BA-C01F-43C1-970F-828A906991A9"}
 * @AllowToRunInFind
 */
function toggleTextFild() {
//	application.output('TOGGLE ' + isEditing())
//	application.output('bo_details_id ' + foundset.bo_details_id);
//	application.output('costo/ricavo ' + foundset.profit_cost_type_id);
	var rec = foundset.getSelectedRecord();
//	application.output('toggleTextFild ' + rec)
	for (var i = 1; i <= 12; i++) {
		var month = i;

		var relationName = 'bo_details_' + month + '_to_bo_planning';
		if (rec && rec[relationName] != null) {
			var isDaily = (rec.profit_cost_type_id == 5 || rec.profit_cost_type_id == 9);
			elements['am_' + month].editable = isEditing() && !isDaily;
			elements['am_' + month].enabled = isEditing() && !isDaily;
			elements['day_' + month].editable = isEditing() && isDaily;
			//elements['day_' + month].visible = isEditing() && isDaily;
		}
		
		elements['am_' + month].bgcolor = isEditing() ? (isDaily ? '':'#ffffdd' ) : '';
		elements['day_' + month].bgcolor = isEditing() ? (isDaily ? '#ffffdd':'' ) : '';
	}
}

/**
 * Handle record selected.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"0DC5A098-5CC3-4C35-883F-814EAC777110"}
 */
function onRecordSelection(event) {
	_super.onRecordSelection(event)
//	application.output('onRecordSelection isEditing: ' + isEditing());
	application.output(globals.messageLog + ' bo_tab_plain.onRecordSelection() 3 NUOVO DETTAGLIO da POP '+isNewDetails, LOGGINGLEVEL.DEBUG);
	toggleTextFild();
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"F0E3E963-01C3-40FA-903C-9A099C8AF8E5"}
 * @AllowToRunInFind
 */
function stopEditingPlanning(event) {
	application.output(globals.messageLog + 'START bo_tab_plain.stopEditingPlanning() ', LOGGINGLEVEL.INFO);
	_super.stopEditing(event);

	/** @type {JSFoundSet<db:/geco/bo_planning>} */
	var bo_planning = databaseManager.getFoundSet('geco', 'bo_planning');
	bo_planning.loadAllRecords();

	if (isNewDetails == false) {
		//cancellazione record aggiunti
		for (var i = 0; i < planningNewToDelete.length; i++) {
			/** @type {JSRecord<db:/geco/bo_planning>} */
			var rec = planningNewToDelete[i];
			//bo_planning.loadAllRecords();
			if (bo_planning.find()) {
				bo_planning.bo_planning_id = rec.bo_planning_id;
				bo_planning.search();
			}
			bo_planning.deleteRecord();
		}

		//planningChangeNewPrev
		if (bo_planning.find()) {
			bo_planning.bo_id = foundset.bo_id;
			bo_planning.search();
		}
		//modifica record per tornare al valore iniziale dei dati
		for (var index = 1; index <= bo_planning.getSize(); index++) {
			var record = bo_planning.getRecord(index);
			record.days_number = record.tmp_days_number;
			record.total_amount = record.tmp_total_amount;
			if (record.bo_planning_to_bo_details.bo_details_to_profit_cost_types.profit_cost_acr == 'R')
				record.return_amount = record.total_amount;
			else record.cost_amount = record.total_amount;
			record.tmp_days_number = null;
			record.tmp_total_amount = null;
		}
		databaseManager.saveData(bo_planning);
		//svuoto gli array di appoggio
		planningChangePrev = [];
		planningNewToDelete = [];
		planningChangeNewPrev = [];
		updateUI(event);
	}
	else {
		application.output(globals.messageLog + 'bo_tab_plain.stopEditingPlanning() creazione nuovo dettaglio fine ', LOGGINGLEVEL.INFO);
		isNewDetails = false;
		
	}
	updateUI(event);
	application.output(globals.messageLog + 'STOP bo_tab_plain.stopEditingPlanning() ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"4A3D3928-A41A-454E-A1C9-03DF4094D90A"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
	application.output(globals.messageLog + 'START bo_tab_plain.saveEdits() ', LOGGINGLEVEL.INFO);

	/** @type {JSFoundSet<db:/geco/bo_details>} */
	var bo_details = databaseManager.getFoundSet('geco', 'bo_details');
	if (bo_details.find()) {
		bo_details.bo_id = foundset.bo_id;
		bo_details.search();
	}
	for (var idx = 1; idx <= bo_details.getSize(); idx++) {
		var recDetail = bo_details.getRecord(idx);
		/** @type {JSFoundSet<db:/geco/bo_planning>} */
		var recPlain = recDetail.bo_details_to_bo_planning;
		//application.output(recDetail.bo_details_id + ' totale dettaglio ' + globals.roundNumberWithDecimal(recDetail.total_amount, 2) + ' ' + globals.roundNumberWithDecimal(recPlain.sum_total_amount, 2))

		if (globals.roundNumberWithDecimal(recDetail.total_amount, 2) < globals.roundNumberWithDecimal(recPlain.sum_total_amount, 2)) {
			//globals.DIALOGS.showErrorDialog("Attenzione!", "La somma degli importi inseriti supera il totale indicato\n - " + recDetail.bo_details_to_profit_cost_types.profit_cost_description + ' ' + recDetail.figure, "OK");
			application.output(globals.messageLog + 'bo_tab_plain.saveEdits() ERROR Importi superiori', LOGGINGLEVEL.ERROR);
			//return;
		}
	}

	/** @type {JSFoundSet<db:/geco/bo_planning>} */
	var bo_planning = databaseManager.getFoundSet('geco', 'bo_planning');
	bo_planning.loadAllRecords();
	if (bo_planning.find()) {
		bo_planning.bo_id = foundset.bo_id;
		bo_planning.search();
	}

	for (var index = 1; index <= bo_planning.getSize(); index++) {
		var record = bo_planning.getRecord(index);
		//application.output('prima ' + record.total_amount);
		record.tmp_days_number = null;
		record.tmp_total_amount = null;
		//application.output('dopo ' + record.total_amount);
	}

	//svuoto gli array di appoggio
	planningToRevert = [];
	//planningEditedPrev = [];
	planningChangePrev = [];
	planningNewToDelete = [];

	_super.saveEdits(event);
	foundset.loadAllRecords();
	updateUI(event);
	isNewDetails = false;
	application.output(globals.messageLog + 'STOP bo_tab_plain.saveEdits() ', LOGGINGLEVEL.INFO);

}

/**
 * Handle changed data.
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"BB5B9B1C-F556-41B5-8BB6-7E3E8ED6B874"}
 */
function onDataChange(oldValue, newValue, event) {
	application.output(globals.messageLog + 'START bo_tab_plain.onDataChange() ', LOGGINGLEVEL.INFO);
	var element = event.getElementName();
	/** @type {Number} */
	var month = element.substr(element.indexOf('_') + 1);
	var dateElement = new Date(scopes.globals.selectedYear, month - 1, 1);
	//	application.output('data elemento ' + dateElement)
	//	application.output('date BO ' + foundset.bo_details_to_bo.start_date + ' ' + foundset.bo_details_to_bo.end_date)

	var relationName = 'bo_details_' + month + '_to_bo_planning';
	//foundset,bo_details_9_to_bo_planning.getSelectedRecord()
	//	application.output('piano ' + foundset[relationName].bo_planning_id);
	//	application.output('piano mese ' + foundset[relationName].bo_month);
	//	application.output('piano id dettaglio ' + foundset[relationName].bo_details_id);
	//	application.output('piano id BO ' + foundset[relationName].bo_id);
	//	application.output('costo/ricavo ' + foundset.bo_details_to_profit_cost_types.profit_cost_acr + ' id ' + foundset.profit_cost_type_id);

	//piano non compreso fra le date di inizio e fine indicate in BO
	if (foundset.bo_details_to_bo.start_date != null && foundset.bo_details_to_bo.end_date != null) {
		if (foundset.bo_details_to_bo.start_date > dateElement || dateElement > foundset.bo_details_to_bo.end_date) {
			var answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai inserendo una pianificazione fuori dal range indicato nella BO. Vuoi modificare le date?", "Si", "No");
			if (answer == 'Si') {
				application.output(globals.messageLog + 'bo_tab_plain.onDataChange() cambio date BO ', LOGGINGLEVEL.INFO);
				//modifica data della BO
				if (dateElement < foundset.bo_details_to_bo.start_date)
					foundset.bo_details_to_bo.start_date = dateElement
				if (dateElement > foundset.bo_details_to_bo.end_date) {
					foundset.bo_details_to_bo.end_date = new Date(scopes.globals.selectedYear, month, 0);
					application.output(globals.messageLog + 'bo_tab_plain.onDataChange() cambio date BO ' + foundset.bo_details_to_bo.end_date, LOGGINGLEVEL.INFO);
				}
			} else {
				// UNDO editing
				application.undo();
//				application.output(foundset[relationName])
				databaseManager.revertEditedRecords(foundset[relationName].getSelectedRecord());
				application.output(globals.messageLog + 'STOP bo_tab_plain.onDataChange() NO cambio date BO ', LOGGINGLEVEL.INFO);
				return;
			}
		}
	}

	// ricavo
	if (foundset.bo_details_to_profit_cost_types.profit_cost_acr == 'R') {
		foundset[relationName].return_amount = foundset[relationName].total_amount;
		//application.output(foundset[relationName].total_amount + ' ' + foundset[relationName].return_amount);
		//updateUI(event);
	}
	// costo personale o TM
	else if (foundset.bo_details_to_profit_cost_types.profit_cost_acr == 'C') {
//		application.output('trovato day ' + element.indexOf('day'))
		if (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 9 && element.indexOf('day') >= 0) {
			//cambia il numero di gg
			//application.output('######### ' + element.substr(0, element.indexOf('_')));
			foundset[relationName].days_import = foundset.days_import;
			if (element.substr(0, element.indexOf('_')) == 'day') {
				
				foundset[relationName].total_amount = globals.roundNumberWithDecimal( (foundset[relationName].days_number * foundset.days_import), 2);
			}
			//cambia amount ricalcolo il numero di gg
			else { 
				foundset[relationName].days_number = globals.roundNumberWithDecimal( (foundset[relationName].total_amount / foundset.days_import), 2);
			}
			updateUI(event);
		}
		foundset[relationName].cost_amount = foundset[relationName].total_amount;
		//application.output(foundset[relationName].total_amount + ' ' + foundset[relationName].cost_amount)

	}
	/** @type {JSRecord<db:/geco/bo_planning>} */
	var rec = foundset[relationName].getSelectedRecord();
	//application.output('record modificato ' + rec.bo_planning_id)
	//inserisco il record nell'array dei record modificati
	if (planningChangePrev.indexOf(rec) < 0)
		planningChangePrev.push(rec);
	for (var y1 = 0; y1 < planningChangePrev.length; y1++) {
		/** @type {JSRecord<db:/geco/bo_planning>} */
		var r = planningChangePrev[y1];
		
		application.output(globals.messageLog + 'bo_tab_plain.onDataChange() id record modificati ' + r.bo_planning_id, LOGGINGLEVEL.DEBUG);		
		
	}
	if (rec.isNew()) {
		rec.bo_id = foundset.bo_id;
		rec.bo_details_id = foundset.bo_details_id;
		planningChangeNewPrev.push(rec);
		//inserisco il record nell'array dei record da eliminare nel caso l'utente annulli le modifiche
		planningNewToDelete.push(rec);
	}
	databaseManager.saveData(rec);
	//databaseManager.recalculate(foundset.bo_details_to_bo.bo_to_bo_planning);
	foundset.bo_details_to_bo_planning.loadAllRecords();
	updateUI(event);
	application.output(globals.messageLog + 'STOP bo_tab_plain.onDataChange()', LOGGINGLEVEL.INFO);
}

/**
 * Ricalcola la pianificazione mensile in seguito a modifiche dell'utente sui singoli mesi
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"68E284FA-D15C-4897-90A0-CE428BE2A376"}
 * @AllowToRunInFind
 */
function plainRecordDetails(event) {
	application.output(globals.messageLog + 'START bo_tab_plain.plainRecordDetails() ', LOGGINGLEVEL.INFO);
	//somma giorni e amount editati dall'utente
	var sumDaysEdited = 0;
	var sumAmountEdited = 0;
	//date di inizio e fine attività previste
	var startBO = foundset.bo_details_to_bo.start_date;
	var endBO = foundset.bo_details_to_bo.end_date;

	if (startBO == null || endBO == null) {
		globals.DIALOGS.showErrorDialog("Attenzione!", "Non è possibile calcolare la pianificazione mensile, non sono state indicate le date previste di inizio e fine attività", "OK");
		application.output(globals.messageLog + 'STOP bo_tab_plain.plainRecordDetails() date non indicate ', LOGGINGLEVEL.INFO);
		return;
	}

	//array di obj: dayWorkable=giorni lavoraboli, monthDet=mese yearDet=anno per il record details selected
	/** @type {Array} */
	var listObjTimeSplit = globals.countDayMonthsYears(foundset.bo_details_to_bo.start_date, foundset.bo_details_to_bo.end_date);
	var totWorkableDays = 0;
	//conto i mesi/gg lavorabili su cui spalmare costi e ricavi
	for (var x = 0; x < listObjTimeSplit.length; x++) {
		totWorkableDays = totWorkableDays + listObjTimeSplit[x].dayWorkable;
	}

	//somma dei gg lavorabili per la durata della BO
	var dayWork = totWorkableDays;

	/** @type {Number[]} */
	var idEditedList = [];
	//estrae le somme di giorni, costi e profitti modificati del record selezionato
	for (var i = 0; i < planningChangePrev.length; i++) {
		/** @type {JSRecord<db:/geco/bo_planning>} */
		var bo_planning_edited = planningChangePrev[i];

		// se il bo_plain è relativo al bo_details selezionato altrimenti non faccio nulla
		if (bo_planning_edited.bo_details_id == foundset.bo_details_id) {

			//se l'id del record non è presente nell'array degli idPlanning
			if (idEditedList.indexOf(bo_planning_edited.bo_planning_id) < 0) {
				idEditedList.push(bo_planning_edited.bo_planning_id);
				sumDaysEdited = globals.roundNumberWithDecimal( (sumDaysEdited + bo_planning_edited.days_number), 2);
				sumAmountEdited = globals.roundNumberWithDecimal( (sumAmountEdited + bo_planning_edited.total_amount), 2);
				//application.output('\nbo_planning_edited ' + i + ' - ' + bo_planning_edited);

				for (x = 0; x < listObjTimeSplit.length; x++) {
					if (listObjTimeSplit[x].yearDet == bo_planning_edited.bo_year && listObjTimeSplit[x].monthDet == bo_planning_edited.bo_month) {
						dayWork = dayWork - listObjTimeSplit[x].dayWorkable;
					}
				}
			}
		}
	}
	application.output('ID record Modificati ' + idEditedList);
	//i giorni/amount da dividere tolti quelli modificati
	
	var dayToSplit = globals.roundNumberWithDecimal( (foundset.days_number - sumDaysEdited), 2);
	var amountTosplit = globals.roundNumberWithDecimal( (foundset.total_amount - sumAmountEdited), 2);

	if (sumAmountEdited > foundset.total_amount || sumDaysEdited > foundset.days_number) {
		//ha inserito valori superiori ai valori dei dettagli
		//globals.DIALOGS.showErrorDialog("Attenzione!", "La somma degli importi inseriti supera il totale indicato\n - " + foundset.bo_details_to_profit_cost_types.profit_cost_description + ' ' + foundset.figure, "OK");
		application.output(globals.messageLog + 'STOP bo_tab_plain.plainRecordDetails() ERROR importi inseriti superiori ', LOGGINGLEVEL.ERROR);
		//return;
	}

	//application.output('amountTosplit ' + amountTosplit + ' dayToSplit ' + dayToSplit)
	var recToUpdate = [];

	var startDate = startBO;

	//contatore per la lista di oggetti listObjTimeSplit
	var count = 0;
	if (idEditedList == null || idEditedList.length == 0) {
		// non ci sono piani modificati esco
		return
	}
	foundset.bo_details_to_bo_planning.loadAllRecords();
	var planningDetails = foundset.bo_details_to_bo_planning;
	application.output(globals.messageLog + 'bo_tab_plain.plainRecordDetails() record di pianificazione ' + planningDetails.getSize() + ' totale modificato ' + sumAmountEdited, LOGGINGLEVEL.INFO);

	if (planningDetails.getSize() == idEditedList.length && listObjTimeSplit.length == idEditedList.length) {
		// tutti modificati non ricalcolo nulla controllo i valori inseriti
		application.output(foundset.total_amount + ' - ' + planningDetails.sum_total_amount);
		if (foundset.total_amount != sumAmountEdited) {
			//globals.DIALOGS.showErrorDialog("Attenzione!", "La somma degli importi inseriti non corrisponde al totale indicato.", "OK");
			application.output(globals.messageLog + 'STOP bo_tab_plain.plainRecordDetails() ERROR importi differenti ', LOGGINGLEVEL.ERROR);
			//return;
		}
	}

	var totAmInserted = sumAmountEdited;
	var totDaysIserted = sumDaysEdited;
	var lastRecordToUpdate = listObjTimeSplit.length - idEditedList.length;
	var indexRecordUpdating = 1;
	//ciclo sui mesi di durata della BO
	while (startDate <= endBO) {
		//application.output('--------------------------------------------------------------lastRecordToUpdate ' + lastRecordToUpdate);
		var mese = startDate.getMonth() + 1;
		var anno = startDate.getFullYear();
		//application.output('anno mese ' + anno + ' ' + mese)
		var tot = 0;
		//carico tutti i piani
		/** @type {JSFoundSet<db:/geco/bo_planning>} */
		var fdPlanning = foundset.bo_details_to_bo_planning;
		fdPlanning.loadAllRecords();
		//cerco il piano dato il mese e l'anno e il dettaglio selezionato
		if (fdPlanning.find()) {
			fdPlanning.bo_details_id = foundset.bo_details_id;
			fdPlanning.bo_month = mese;
			fdPlanning.bo_year = anno;
			tot = fdPlanning.search();
		} else {
			application.output(globals.messageLog + 'ERRORE bo_tab_plain.plainRecordDetails() Non entra in find sulle pianificazioni ', LOGGINGLEVEL.ERROR);
			application.output('no find')
		}
		//application.output('trovato ' + tot);
		//dovrebbe trovare una solo piano per dettaglio mese e anno
		/** @type {JSRecord<db:/geco/bo_planning>} */
		var recPlanning = null;
		var dayWorkableMonth = 0;
		var newDayMonth = 0;
		var newAmountMonth = 0;
		var objToUpdate = null;
		dayWorkableMonth = listObjTimeSplit[count].dayWorkable;
		newDayMonth = globals.roundNumberWithDecimal( ( (dayToSplit / dayWork) * dayWorkableMonth), 2);
		newAmountMonth = globals.roundNumberWithDecimal( ( (amountTosplit / dayWork) * dayWorkableMonth), 2);
		//application.output('\nreecord da modificare ' + indexRecordUpdating + ' ultimo recorrd da modificare ' + lastRecordToUpdate);

		//ultimo record da moficiare
		if (indexRecordUpdating == lastRecordToUpdate) {
			application.output('\ndeve inserire il resto dei gg o importo\n');
			newDayMonth = globals.roundNumberWithDecimal( (foundset.days_number - totDaysIserted), 2);
			newAmountMonth = globals.roundNumberWithDecimal( (foundset.total_amount - totAmInserted), 2);
			application.output('RESTO ' + newDayMonth + ' ' + newAmountMonth);
		}

		if (tot == 1) {
			recPlanning = fdPlanning.getRecord(tot);
			application.output('record trovato  ' + recPlanning);
			application.output('edited contiene il record selezionato ' + idEditedList.indexOf(recPlanning.bo_planning_id));
			if (idEditedList.indexOf(recPlanning.bo_planning_id) < 0) {
				application.output('non modificato ');
				objToUpdate = {
					record: recPlanning,
					valueDays: newDayMonth,
					valueAmount: newAmountMonth
				}
				recToUpdate.push(objToUpdate);
				application.output('\nNUOVI VALORI giorni ' + newDayMonth + ' IMPORTI ' + newAmountMonth)
				
				totAmInserted = globals.roundNumberWithDecimal( (totAmInserted + newAmountMonth), 2);
				totDaysIserted = globals.roundNumberWithDecimal( (totDaysIserted + newDayMonth), 2);
				indexRecordUpdating++;
			}
		} else if (tot == 0) {
			/** @type {JSRecord<db:/geco/bo_planning>} */
			var newRec = null;
			var isNew = false;
			for (var n = 0; n < planningChangeNewPrev.length; n++) {
				newRec = planningChangeNewPrev[n];
				if (newRec.bo_details_id == foundset.bo_details_id && newRec.bo_id == foundset.bo_id && newRec.bo_month == mese && newRec.bo_year == anno) {
					application.output('è già creato in memory');
					isNew = true;
					recPlanning = newRec;
					databaseManager.saveData(recPlanning);
					planningNewToDelete.push(recPlanning);
					break;
				}
			}
			if (!isNew) {
				// non è stato ancora creato in memoria lo creo adesso
				application.output('NON creato in memory');
				var idx = fdPlanning.newRecord();
				fdPlanning.bo_details_id = foundset.bo_details_id;
				fdPlanning.bo_id = foundset.bo_id;
				fdPlanning.bo_month = mese;
				fdPlanning.bo_year = anno;
				fdPlanning.days_import = foundset.days_import;
				fdPlanning.days_number = (newDayMonth != 0) ? newDayMonth : null;

				if (foundset.bo_details_to_profit_cost_types.profit_cost_acr == 'R') {
					fdPlanning.total_amount = newAmountMonth;
					fdPlanning.return_amount = fdPlanning.total_amount
				} else {
					if (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 9) {
						fdPlanning.total_amount = globals.roundNumberWithDecimal( (newDayMonth * fdPlanning.days_import), 2);
						fdPlanning.cost_amount = fdPlanning.total_amount
					} else {
						fdPlanning.total_amount = newAmountMonth;
						fdPlanning.cost_amount = fdPlanning.total_amount
					}
				}
				recPlanning = fdPlanning.getRecord(idx);

				application.output('NUOVO ' + recPlanning.bo_planning_id + ' index ' + idx);

				dayWorkableMonth = listObjTimeSplit[count].dayWorkable;

				objToUpdate = {
					record: recPlanning,
					valueDays: newDayMonth,
					valueAmount: newAmountMonth
				}
				//application.output(objToUpdate.record)
				databaseManager.saveData(recPlanning);
				planningNewToDelete.push(recPlanning);
				recToUpdate.push(objToUpdate);
				
				totAmInserted = globals.roundNumberWithDecimal( (totAmInserted + newAmountMonth), 2);
				totDaysIserted = globals.roundNumberWithDecimal( (totDaysIserted + newDayMonth), 2);
				indexRecordUpdating++;
			}
			application.output('NUOVI VALORI giorni ' + newDayMonth + ' IMPORTI ' + newAmountMonth)
		}
		startDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, startDate.getDate());
		count++;
		application.output('\ntotale gg inseriti ' + totDaysIserted + ' totale importo inserito ' + totAmInserted);
		application.output(count + '\n----------------------------------------------')
	}
	//fine while
	//Stampa dei record da eliminare se dovese annullare l'opearzione
	//	for (var y = 0; y < planningNewToDelete.length; y++) {
	//		application.output('record da eliminare ' + planningNewToDelete[y]);
	//	}

	for (var el = 0; el < recToUpdate.length; el++) {

		var objUpdated = recToUpdate[el]
		application.output('objUpdated ' + objUpdated.record);
		/** @type {JSRecord<db:/geco/bo_planning>} */
		var rec = objUpdated.record;
		var newDay = objUpdated.valueDays;
		var newAmount = objUpdated.valueAmount;

		if (foundset.bo_details_to_profit_cost_types.profit_cost_acr == 'R') {
			rec.total_amount = newAmount;
			rec.return_amount = rec.total_amount
		} else if (foundset.bo_details_to_profit_cost_types.profit_cost_acr == 'C') {
			if (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 9) {
				rec.days_number = newDay;				
				rec.total_amount = globals.roundNumberWithDecimal( (newDay * rec.days_import), 2);
				rec.cost_amount = rec.total_amount
			} else {
				rec.total_amount = newAmount;
				rec.cost_amount = rec.total_amount
			}
		}
		//application.output('dopo la modifica in memory rec : ' + rec);
	}
	foundset.bo_details_to_bo_planning.loadAllRecords();
	databaseManager.saveData(foundset.bo_details_to_bo_planning);
	updateUI(event);
	application.output(globals.messageLog + 'STOP bo_tab_plain.plainRecordDetails()', LOGGINGLEVEL.INFO);
}

/**
 * @AllowToRunInFind
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @properties={typeid:24,uuid:"C2047432-E73E-43A3-A033-A65082C44007"}
 */
function deleteRecord(event, index) {
	var record = foundset.getSelectedRecord();
	var listToDelete = [];
	if (foundset == null || foundset.getSize() == 0) return;
	var answer = globals.DIALOGS.showWarningDialog("Conferma", "Se il dettaglio ha una pianificazione associata, questa verrà cancellata.\nQuesta operazione non può essere annullata", "Elimina", "Annulla");
	if (answer == "Elimina") {
		var tot = 0;

		/** @type {JSFoundSet<db:/geco/bo_planning>} */
		var bo_planning = databaseManager.getFoundSet('geco', 'bo_planning');
		bo_planning.loadAllRecords();
		if (bo_planning.find()) {
			bo_planning.bo_details_id = record.bo_details_id;
			tot = bo_planning.search();
			application.output(tot)
		}
		for (var i = 1; i <= bo_planning.getSize(); i++) {
			var rec = bo_planning.getRecord(i);
			application.output(rec);
			listToDelete.push(rec);
		}

		for (var t = 0; t < listToDelete.length; t++) {
			/** @type {JSRecord<db:/geco/bo_planning>} */
			var recToDel = listToDelete[t];
			bo_planning.deleteRecord(recToDel);
		}

		databaseManager.saveData(bo_planning);
		foundset.deleteRecord(record);
		application.output('dopo cancellazione')
		databaseManager.saveData(record)

	} else return;
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"B29EB94C-AC8E-4805-9B26-84421591C760"}
 */
function newRecord(event) {
	//globals.bo_selected = foundset.bo_id;
	application.output('bo selezionato per il nuovo record ' + globals.bo_selected);
	var win = application.createWindow("Nuovo dettaglio BO", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 750, 300);
	win.title = 'Nuovo dettaglio BO'
	win.show(forms.bo_new_detail_popup);
	isNewDetails = true;
	scopes.globals.callerForm = event.getFormName();
}

/**
 * Ricalcola i profitti di pianificazione tenendo costante il mol della BO
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"8B5A8248-796A-4250-B279-C1B7E478EA12"}
 * @AllowToRunInFind
 */
function calculateReturnByMol(event) {
	application.output(globals.messageLog + 'START bo_tab_plain.calculateReturnByMol()', LOGGINGLEVEL.INFO);

	//array di oggetti: {ID dettaglio, percentuale sul ricavo, totale ricavo dettaglio, totale ricavo inserito}
	/** @type {Object[]} */
	var objDetails = [];

	var totalCostDetails = foundset.total_cost;
	//se non ci sono record esce
	if (foundset.getSize() == 0)
		return;
	//entra in editing
	startEditing(event);
	var startDate = foundset.bo_details_to_bo.start_date;
	var endDate = foundset.bo_details_to_bo.end_date;
	if (startDate == null || endDate == null) {
		globals.DIALOGS.showErrorDialog("Attenzione!", "Non è possibile ricalcolare la pianificazione mensile, non sono state indicate le possibili date di inizio e fine attività", "OK");
		return;
	}

	//trovo i record details di tipo ricavo nel foundset
	var totReturnDetails = 0;
	if (foundset.find()) {
		foundset.bo_details_to_profit_cost_types.profit_cost_acr = 'R';
		totReturnDetails = foundset.search();
	}
	application.output('Record ricavi ' + foundset);
	// se non ci sono dettagli Ricavo
	if (totReturnDetails == 0) {
		globals.DIALOGS.showErrorDialog('Attenzione', 'Non ci sono dettagli di tipo Ricavo da ricalcolare', 'OK');
		foundset.loadAllRecords();
		return;
	} else {
		// cotruisco un oggetto con array di iD dettaglio e percentuali rispetto al ricavo totale
		for (var i = 1; i <= foundset.getSize(); i++) {
			var rec = foundset.getRecord(i);
			application.output('Record' + rec);
			
			var per = globals.roundNumberWithDecimal( ((100 * rec.return_amount) / foundset.total_return), 2);
			/** @type {objDetType} */
			var objDet = {
				idDetail: rec.bo_details_id,
				perDetail: per,
				totalReturnDetail: rec.return_amount,
				totReturnInserted: 0
			}
			objDetails.push(objDet);
			application.output('ID Dettaglio ' + objDet.idDetail + ' percentuale di ricavo  ' + objDet.perDetail + ' ' + objDet.totReturnInserted);
		}
	}

	while (startDate <= endDate) {
		application.output('-------------------------------------\nData ' + startDate + ' Fine Data ' + endDate);
		var mese = startDate.getMonth() + 1;
		var anno = startDate.getFullYear();

		var lastMonth = new Date(anno, mese, 0);
		application.output(lastMonth);

		//carico tutti i piani
		var tot = 0;
		/** @type {JSFoundSet<db:/geco/bo_planning>} */
		var fdPlanningCost = databaseManager.getFoundSet('geco', 'bo_planning');
		//cerco il piano dato il mese e l'anno e il dettaglio selezionato
		if (fdPlanningCost.find()) {
			fdPlanningCost.bo_id = foundset.bo_id
			fdPlanningCost.bo_planning_to_bo_details.bo_details_to_profit_cost_types.profit_cost_acr = 'C';
			fdPlanningCost.bo_month = mese;
			fdPlanningCost.bo_year = anno;
			tot = fdPlanningCost.search();
		} else {
			application.output('no find')
		}
		var totCost = fdPlanningCost.total_cost;
		application.output('trovato ' + tot + ' Costo totale ' + totCost);
		//ricavo totale mensile
		
		var totProfitMonth = globals.roundNumberWithDecimal( ( (foundset.total_return * totCost) / totalCostDetails), 2);
		application.output('Nuovo ricavo totale mensile ' + totProfitMonth);

		var totMonthDetail = 0;
		//ciclo su tutti gli id di tipo ricavo
		for (var x = 0; x < objDetails.length; x++) {
			/** @type {objDetType} */
			var objDetRead = objDetails[x];
			application.output(objDetRead);
			/** @type {JSFoundSet<db:/geco/bo_planning>} */
			var fdPlanningReturn = databaseManager.getFoundSet('geco', 'bo_planning');
			if (fdPlanningReturn.find()) {
				fdPlanningReturn.bo_details_id = objDetRead.idDetail;
				fdPlanningReturn.bo_month = mese;
				fdPlanningReturn.bo_year = anno;
				totMonthDetail = fdPlanningReturn.search();
			}
			
			var total = globals.roundNumberWithDecimal( ((totProfitMonth * objDetRead.perDetail) / 100), 2);
			if (lastMonth == endDate) {
				//se ultimo mese della BO calcolo l'avanzo
				application.output('ULTIMO MESE')
				total = globals.roundNumberWithDecimal( (objDetRead.totalReturnDetail - objDetRead.totReturnInserted), 2);
				application.output('AVANZO ' + total)
			}
			application.output('totale da inserire ' + total)
			//objDetRead.totReturnInserted = globals.roundNumberWithDecimal( (objDetRead.totReturnInserted + total), 2);

			if (totMonthDetail == 1) {
				//ricavoMensile1 = Rtot1*%diR1sulTotale/100
				//modifico i valori del profitto mensile in base alle percentuali del secondo array
				application.output(objDetRead.idDetail + ' Trovo un piano mensile lo modifico nuovo ricavo mensile del dettaglio ' + Math.round( ( (totProfitMonth * objDetRead.perDetail) / 100) * Math.pow(10, 2)) / Math.pow(10, 2));

				fdPlanningReturn.total_amount = total;
				fdPlanningReturn.return_amount = fdPlanningReturn.total_amount;
				databaseManager.saveData(fdPlanningReturn);

			} else if (totMonthDetail == 0) {
				//non esiste la pianificazione del ricavo per il mese deve essere creato
				application.output(objDetRead.idDetail + ' NON Trovo un piano mensile lo creo per il dettaglio ' + Math.round( ( (totProfitMonth * objDetRead.perDetail) / 100) * Math.pow(10, 2)) / Math.pow(10, 2));
				var idx = fdPlanningReturn.newRecord();
				fdPlanningReturn.bo_id = foundset.bo_id;
				fdPlanningReturn.bo_details_id = objDetRead.idDetail;
				fdPlanningReturn.bo_month = mese;
				fdPlanningReturn.bo_year = anno;
				fdPlanningReturn.total_amount = total;
				fdPlanningReturn.return_amount = fdPlanningReturn.total_amount;

				var recPlanning = fdPlanningReturn.getRecord(idx);
				application.output(recPlanning);
				//salvo il nuovo record
				databaseManager.saveData(recPlanning);
				//lo inserisco nell'array dei record da cancellare nel caso in cui l'utente annulli l'operazione
				planningNewToDelete.push(recPlanning);
			} else {
				//errore deve esistere una sola pianificazione per dettaglio per mese.
				application.output('Trovo più piani mensili ERRORE')
			}
			objDetRead.totReturnInserted = globals.roundNumberWithDecimal( (objDetRead.totReturnInserted + total), 2);

			application.output(x + ' INSERITO ' + objDetRead.totReturnInserted);
		}
		//mese successivo
		startDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, startDate.getDate());
		application.output('END WHILE')
	}

	foundset.loadAllRecords();
	//salvo le modifiche alla pianificazione
	databaseManager.saveData(foundset.bo_details_to_bo_planning);
	foundset.loadAllRecords();
	foundset.bo_details_to_bo_planning.loadRecords();
	updateUI(event);
	application.output(globals.messageLog + 'STOP bo_tab_plain.calculateReturnByMol()', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"E42E413F-1831-4A6E-92E2-B88B9DA31C99"}
 * @AllowToRunInFind
 */
function startEditing(event) {
	//ciclare sul foundset planning di tutti i dettagli della bo
	//settare i campi tmp coi valori dei corrispondenti
	//salvare il foundset

	/** @type {JSFoundSet<db:/geco/bo_planning>} */
	var bo_planning = databaseManager.getFoundSet('geco', 'bo_planning');
	if (bo_planning.find()) {
		bo_planning.bo_id = foundset.bo_id;
		bo_planning.search();
	}
	application.output(bo_planning.getSize());
	for (var index = 1; index <= bo_planning.getSize(); index++) {
		var rec = bo_planning.getRecord(index);
		rec.tmp_days_number = rec.days_number;
		rec.tmp_total_amount = rec.total_amount;
	}
	databaseManager.saveData(bo_planning);
	// inizio lo start editing
	return _super.startEditing(event)
}
