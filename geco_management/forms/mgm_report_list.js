/**
 * @type {String}
 * @properties={typeid:35,uuid:"365F9F37-DE72-4EA5-93A4-EF9DF3F372C7"}
 */
var tableReport = '';

/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"5A4CA9C6-2185-4D4D-A885-21FD3AA7DF64",variableType:-4}
 */
var datasetReport = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"F2315D0D-1512-4E50-AAAA-FAF64247F02C"}
 */
var nameExport = '';

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F53D7DD6-9A66-4B9A-8FCC-F11023B2AE2B"}
 */
function saveReport(event) {
	var success = false;
	var line = '';
	if (datasetReport != null) {

		var colArray = new Array()
		for (var i = 1; i <= datasetReport.getMaxColumnIndex(); i++)
		{
			colArray[i-1] = datasetReport.getColumnName(i)
			//note the -1, because an array is zero based and dataset is 1 based.
		}
		//application.output(colArray);
		line = colArray.join('\t')+ '\r\n';
		for (var index = 1; index <= datasetReport.getMaxRowIndex(); index++) {
			var row = datasetReport.getRowAsArray(index);
			var rowstring = row.join('\t') ;
			line = line + rowstring + '\n';
		}
		//application.output(line);
		if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
			/** @type {String} */
			var yearFrom = '' + forms.mgm_report_filter.selectedDateFrom.getFullYear();
			/** @type {String} */
			var monthFrom = '' + (forms.mgm_report_filter.selectedDateFrom.getMonth() + 1);
			/** @type {String} */
			var dayFrom = '' + forms.mgm_report_filter.selectedDateFrom.getDate();
			/** @type {String} */
			var dateFrom = yearFrom + ((monthFrom.length > 1) ? monthFrom : '0' + monthFrom)+ ((dayFrom.length > 1) ? dayFrom : '0' + dayFrom);
			/** @type {String} */
			var yearTo = '' + forms.mgm_report_filter.selectedDateAt.getFullYear();
			/** @type {String} */
			var monthTo = '' + (forms.mgm_report_filter.selectedDateAt.getMonth() + 1);
			/** @type {String} */
			var dayTo = '' + forms.mgm_report_filter.selectedDateAt.getDate();
			/** @type {String} */
			var dateTo = yearTo + (monthTo.length > 1 ? monthTo : '0' + monthTo) + (dayTo.length > 1 ? dayTo : '0' + dayTo);

			var fileName = nameExport + dateFrom + '_' + dateTo + '.xls';
			success = plugins.file.writeTXTFile(fileName, line, 'UTF-8', 'application/vnd.ms-excel');
		}
	}
	if (!success) application.output('Scrittura file non riuscita');
}
