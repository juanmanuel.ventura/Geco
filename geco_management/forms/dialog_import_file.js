/**
 * @type {Number}
 * @properties={typeid:35,uuid:"FFBAEE27-E982-4D00-A5F3-77EDE5154A86",variableType:8}
 */
var doc_type = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"D1F2E27D-35C5-49A0-AA61-46A407A5D08B"}
 */
var document_path = '';

/**
 * @type {String}
 * @properties={typeid:35,uuid:"8D685967-5088-4388-A9B5-49F9976FBC14"}
 */
var uploadResult = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"73C046CC-70D6-4E47-ADF9-E4D07C68EEA2"}
 */
var document_name = '';

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"1F2EE45A-95FC-4DAE-9383-AF633ECAA8C8",variableType:8}
 */
var bo_id = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"49812E14-89C5-46A8-8FDE-B3E430CE8131",variableType:8}
 */
var month = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"068D833F-F3CD-431A-A788-824265EE0656",variableType:8}
 */
var year = null;

/**
 * @type {plugins.file.JSFile}
 * @properties={typeid:35,uuid:"BF2D2D21-36EA-40A2-BB25-F6D2D080D932",variableType:-4}
 */
var vJsFile = null;

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"98AAE63A-9299-430E-A7FD-F1EB607B21E1",variableType:-4}
 */
var isLoad = false;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"0E43B20E-BDA4-4483-83DE-8F107D7459D8",variableType:8}
 */
var skippedLine = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"0622EB79-0EB0-40B2-8EFA-AA28778C6457",variableType:8}
 */
var conteggioTMPrecords = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"8CB7D234-8E3D-4927-A498-1B9E96A94272",variableType:8}
 */
var conteggioStandProfFigures = 0;

/**
 * @type {Object} 
 * @properties={typeid:35,uuid:"FD32E337-6242-4918-8E3C-9D59AD30B9B7",variableType:-4}
 */
var userCost = {
	uid: 0,
	navCode: '',
	cost: 0
}

/**
 * @properties={typeid:24,uuid:"BC488542-DD4F-429A-973A-3CBA32FACCD2"}
 */
function btnChooseFile() {
	application.output(globals.messageLog + 'START dialog_import_file.btnChooseFile', LOGGINGLEVEL.DEBUG);
	/** @type {plugins.file.JSFile} **/
	vJsFile = plugins.file.showFileOpenDialog(1, null, false, ['*'], uploadCallBack);
	if (vJsFile) {
		//application.output('file ' + vJsFile.getAbsolutePath());
		document_path = vJsFile.getAbsolutePath();
		//application.output(vJsFile.getName());
		application.output('file ' + vJsFile.getAbsolutePath() + '; document_path ' + vJsFile.getName());
	} else {
		document_path = null;
	}
	application.output(globals.messageLog + 'STOP dialog_import_file.btnChooseFile', LOGGINGLEVEL.DEBUG);
}

/**
 * @param {JSEvent} event *
 * @properties={typeid:24,uuid:"8119F3D8-8DA7-44F8-B8A6-930512D5284B"}
 */
function closeForm(event) {
	clearField();
	controller.getWindow().destroy();
	forms.jo_details.updateUI(event);
}

/**
 * @param {plugins.file.JSFile[]} fileToUpload
 *
 * @properties={typeid:24,uuid:"CBA4F5D2-53DA-447F-8FC9-80531DB9AD80"}
 */
function uploadCallBack(fileToUpload) {
	application.output(globals.messageLog + 'START dialog_import_file.uploadCallBack', LOGGINGLEVEL.DEBUG);
	/** @type {plugins.file.JSFile} **/
	var myuploadedfile = fileToUpload[0];
	//application.output('nome file upload ' + myuploadedfile.getName());
	globals.serverDir = plugins.file.convertToJSFile("documentiController");
	//application.output('dir path ' + globals.serverDir.getAbsolutePath());
	var resultDir = plugins.file.createFolder(globals.serverDir);

	//application.output('creata dir ' + resultDir);
	//application.output('dir globale ' + globals.serverDir);
	globals.serverfile = plugins.file.createFile(globals.serverDir.getName() + '/' + myuploadedfile.getName());
	//application.output('file server ' + globals.serverfile);
	var result = plugins.file.writeFile(globals.serverfile, myuploadedfile.getBytes());
	//application.output('file scritto in locale ' + result);

	application.output('nome file upload: ' + myuploadedfile.getName() + ';\ndir path: ' + globals.serverDir.getAbsolutePath() + ';\ncreata dir? ' + resultDir + ';\nDir globale: ' + globals.serverDir + ';\nFile server: ' + globals.serverfile + ';\nfile scritto in locale? ' + result)
	if (result == true) {
		document_name = myuploadedfile.getName();
		//application.output('nome doc ' + document_name);
		//application.output(globals.serverDir.getAbsolutePath());
		document_path = globals.serverDir.getAbsolutePath();
		//application.output('dir path ' + document_path);
		application.output('estensione: ' + myuploadedfile.getContentType());
		elements.buttonLoad.enabled = true;
		uploadResult = '';
	} else {
		uploadResult = 'File non caricato nella directory.';
		application.output(globals.messageLog + 'ERROR dialog_import_file.uploadCallBack KO NON SONO RIUSCITO A METTERE IL FILE NELLA CARTELLA', LOGGINGLEVEL.DEBUG);
	}

	if (myuploadedfile.getContentType() != 'text/csv' && myuploadedfile.getContentType() != 'text/plain') {
		uploadResult = 'File non caricato.\nCarica un File in formato \'.csv\', \"delimitato dal separatore di elenco\".';
		elements.buttonLoad.enabled = false;
	}
	application.output(globals.messageLog + 'STOP dialog_import_file.uploadCallBack', LOGGINGLEVEL.DEBUG);
}

/**
 *
 * @properties={typeid:24,uuid:"8999D524-6B64-44D9-99EE-2C56A2615E56"}
 *
 * @AllowToRunInFind
 */
function readFile() {
	application.output(globals.messageLog + 'START dialog_import_file.readFile', LOGGINGLEVEL.DEBUG);

	//verifico che tutti i dati obbligatori siano stati inseriti
	if (doc_type == null || month == null || year == null || document_path == '') {
		uploadResult = 'Inserire i campi obbligatori:\n' + 'Tipo di documento, Mese, Anno, File da caricare.';
		return;
	}

	var listFile = plugins.file.getFolderContents(document_path);
	var fileSaved;
	//var success;

	try {
		for (var i = 0; i < listFile.length; i++) {
			fileSaved = listFile[i];

			if (fileSaved) {
				var nomeFile = fileSaved.getName();
				//var index = nomeFile.lastIndexOf('.');
				//				var estensione = '';
				//				if (index > 0) estensione = nomeFile.slice(index + 1);

				//se il file appena caricato è quello che trova nella directory, allora procedo; altrimenti no.
				if (document_name == nomeFile) {
					switch (doc_type) {
					case 1:
						uploadUsersCost(fileSaved);
						break;
					case 2:
						//uploadJobOrdersActual(fileSaved);
						uploadJobOrdersActualSP(fileSaved)
						break;
					case 3:
						addProfessionalCost(fileSaved);
						break;
					default:
						break;
					}
				}
			}
		}
	} catch (e) {
		uploadResult = 'File non caricato.';
		application.output(globals.messageLog + 'ERROR dialog_import_file.readFile Importazione del file fallita.', LOGGINGLEVEL.DEBUG);
		application.output(e);
	} finally {
		var deletedTemporaryRecords = deleteTemporaryRecords();
		//skippedLine = 0;
		application.output('Deleted temporary records? ' + deletedTemporaryRecords);
		//var folderDeleted = plugins.file.deleteFolder(globals.serverDir, false);
		//application.output('folderDeleted? ' + folderDeleted);
	}
	application.output(globals.messageLog + 'STOP dialog_import_file.readFile', LOGGINGLEVEL.DEBUG);
}

/**
 * @param {plugins.file.JSFile} file
 * @properties={typeid:24,uuid:"0BAA15C6-D4E4-4C2A-90A0-D00AA6094F99"}
 * @AllowToRunInFind
 */
function uploadUsersCost(file) {
	application.output(globals.messageLog + 'START dialog_import_file.uploadUsersCost ', LOGGINGLEVEL.DEBUG);

	var fr = new Packages.java.io.FileReader(file.getAbsolutePath());
	var br = new Packages.java.io.BufferedReader(fr);
	var errorMessage = 'Modifica non effettuata per: ';
	var line = '';
	var count = 0;
	var totUsers = 0;
	var userExists = 0;
	var caseErrorMessage = 0;

	/** @type {Object[]} */
	var userCostList = [];

	while ( (line = br.readLine()) != null) {
		var arrayLine = line.split(';');
		if (count > 0) {
			var navCode = arrayLine[0];
			var name = arrayLine[1];
			var costStr = arrayLine[3];
//			var costStr = arrayLine[8];
			/** @type {Number} */
			var cost = (costStr != null && costStr != '') ? costStr.replace(',', '.') : 0; //separo decimali con il "." anzichè con la ","
			/** @type {JSFoundSet<db:/geco/users>} */
			var users = databaseManager.getFoundSet('geco', 'users');
			/** @type {JSFoundSet<db:/geco/bo_users_cost>} */
			var bo_users_cost = databaseManager.getFoundSet('geco', 'bo_users_cost');

			//cerco l'utente in bo_users_cost per navCode
			if (bo_users_cost.find()) {
				bo_users_cost.enrollment_number = navCode;
				bo_users_cost.search();
			}

			//ordino i record per data di creazione, dal più recente
			bo_users_cost.sort('user_cost_date_from desc');

			//non ho trovato l'utente nella tabella dei costi, lo cerco in users sempre per navCode
			if (bo_users_cost.getSize() == 0) {
				if (users.find()) {
					users.enrollment_number = navCode;
					totUsers = users.search();
				}
				//non l'ho trovato in users per navCode, lo cerco per nome e cognome nella tabella contacts
				if (totUsers == 0) {
					if (users.find()) {
						users.users_to_contacts.real_name = name;
						userExists = users.search();
					}
					//utente trovato, metto costo, utente e data in bo_users_cost
					if (userExists != 0) {
						//application.output('Utente trovato per cognome e nome: ' + users.users_to_contacts.real_name+', Sto per inserirlo.\n----------------------------------------------------------------------');
						bo_users_cost.newRecord();
						bo_users_cost.enrollment_number = navCode;
						bo_users_cost.user_cost_date_from = new Date(year, month - 1, 1);
						bo_users_cost.user_cost = cost;
						bo_users_cost.user_id = users.user_id;
						databaseManager.saveData(bo_users_cost);
					}//L'utente NON è stato trovato nemmeno per cognome e nome nella tabella users
					else {
						errorMessage = errorMessage + '\n' + navCode + ' ' + name + ': non trovato.';
						caseErrorMessage = 1;
						application.output(globals.messageLog + 'dialog_import_file.uploadUsersCost KO ' + navCode + ' ' + name + ' non trovato per real_name nella tab. contacts', LOGGINGLEVEL.DEBUG);
					}
				}//trovato per codice navision in users metto costo, utente e data in bo_users_cost
				else {
					//application.output('-*-*-*-*-*-Ho trovato l\'utente per NavCode sulla tab. users');
					bo_users_cost.newRecord();
					bo_users_cost.enrollment_number = navCode;
					bo_users_cost.user_cost_date_from = new Date(year, month - 1, 1);
					bo_users_cost.user_cost = cost;
					bo_users_cost.user_id = users.user_id;
					databaseManager.saveData(bo_users_cost);
				}
			}//trovato nella tabella bo_users_cost per codice navision
			else {
				//potrebbero essere di più; ciclo su bo_users_cost per prendere quello con data maggiore
				for (var x = 1; x <= bo_users_cost.getSize(); x++) {
					var recBoUserCost = bo_users_cost.getRecord(x);
					var uid = recBoUserCost.user_id;
					if (cost != null && globals.trimString('' + cost)) {
						if (recBoUserCost.user_cost != cost) {
							//cerco in tabella users se esiste;
							//							if (users.find()) {
							//								users.enrollment_number = navCode;
							//								totUsers = users.search();
							//							}//trovato, devo creare nuovo record con stessi dati ma costo diverso;
							//							if (totUsers == 1) {
							bo_users_cost.newRecord();
							bo_users_cost.enrollment_number = navCode;
							bo_users_cost.user_cost_date_from = new Date(year, month - 1, 1);
							bo_users_cost.user_cost = cost;
							bo_users_cost.user_id = uid;
							/** @type {userCost} */
							var userCostObj = {
								uid: uid,
								navCode: navCode,
								cost: cost
							}
							userCostList.push(userCostObj);
							//} else {

							//non ho trovato l'utente nella tabella users.
							//errorMessage = errorMessage + '\n' + navCode + ' ' + name + ': non trovato per NavCode in users.';
							//application.output(globals.messageLog + 'dialog_import_file.uploadUsersCost KO ' + navCode + ' ' + name + ' non trovato.', LOGGINGLEVEL.DEBUG);
							//}
							databaseManager.saveData(bo_users_cost);
						}
					} else {
						//non devo fare nulla
						errorMessage = errorMessage + '\n' + navCode + ' ' + name + ': costo nullo.';
						caseErrorMessage = 2;
						application.output(globals.messageLog + 'dialog_import_file.uploadUsersCost KO ' + navCode + ' ' + name + ': costo nullo.', LOGGINGLEVEL.DEBUG);
						//application.output('-------Utente con costo nullo, non faccio nulla');
					}
					break;
				}
			}
		}
		count++;
	}
	br.close();
	isLoad = true;

	//cambio per i piani già creati e non actuale le BO ancora aperte
	for (var y = 0; y < userCostList.length; y++) {
		//cerco i piani forecast e modifico gli importi per gli utenti modificati
		/** @type {userCost} */
		var obj = userCostList[y];
		application.output(obj.cost + ' ' + obj.navCode + ' ' + obj.uid)
		/** @type {JSFoundSet<db:/geco/jo_planning>} */
		var jo_planning = databaseManager.getFoundSet('geco', 'jo_planning');
		var totJoP = 0;
		if (jo_planning.find()) {
			jo_planning.is_actual = 0;
			jo_planning.jo_planning_to_jo_details.profit_cost_type_id = '5||9';
			jo_planning.jo_planning_to_jo_details.enrollment_number = obj.navCode;
			jo_planning.jo_planning_to_jo_details.user_id = obj.uid;
			jo_planning.user_id = obj.uid;
			totJoP = jo_planning.search();
		}
		jo_planning.sort('jo_id asc, jo_details_id asc');
		if (totJoP > 0) {
			var listJO = [];
			var listJODet = [];
			var recP = null;
			for (var index = 1; index <= jo_planning.getSize(); index++) {
				recP = jo_planning.getRecord(index);
				if (listJO.indexOf(recP.jo_id) < 0) listJO.push(recP.jo_id);
				if (listJODet.indexOf(recP.jo_details_id) < 0) listJODet.push(recP.jo_details_id)
			}
			application.output('id commesse ' + listJO + ' id dettagli ' + listJODet);
			for (var t = 0; t < listJO.length; t++) {
				var idJO = listJO[t]
				//INSERIMENTO STORICO
				var query = 'call sp_insert_history_jo(?,?,?,?,?,?)';
				var args = [idJO, scopes.globals.currentUserId, 'before actual user cost by ' + scopes.globals.currentUserDisplayName, 0, 0, 1];
				//var dataset =
				databaseManager.getDataSetByQuery('geco', query, args, -1);
				//var lastVersionID = dataset.getValue(1, 1);
				application.output(globals.messageLog + 'dialog_import_file.saveEdits ultima version inserita', LOGGINGLEVEL.DEBUG);
				//FINE
			}
			var lastDet = 0;
			for (index = 1; index <= jo_planning.getSize(); index++) {
				recP = jo_planning.getRecord(index);
				recP.days_import = globals.roundNumberWithDecimal(obj.cost, 2);
				recP.cost_amount = globals.roundNumberWithDecimal( (obj.cost * recP.days_number), 2);
				recP.total_amount = recP.cost_amount;
				if (listJODet.length > 0 && listJODet.indexOf(recP.jo_details_id) >= 0 && lastDet != recP.jo_details_id) {
					application.output('ultimo dettaglio ' + lastDet + ' dettaglio attuale ' + recP.jo_details_id);
					recP.jo_planning_to_jo_details.days_import = globals.roundNumberWithDecimal(obj.cost, 2);
					var dayToChange = recP.jo_planning_to_jo_details.days_number - recP.jo_planning_to_jo_details.days_number_actual
					var costReal = recP.jo_planning_to_jo_details.cost_actual + (dayToChange * obj.cost);
					application.output('costo reale del dettaglio ' + costReal);
					recP.jo_planning_to_jo_details.cost_amount = globals.roundNumberWithDecimal( costReal, 2);
					//recP.jo_planning_to_jo_details.cost_amount = globals.roundNumberWithDecimal( (obj.cost * recP.jo_planning_to_jo_details.days_number), 2);
					recP.jo_planning_to_jo_details.total_amount = recP.jo_planning_to_jo_details.cost_amount;
					databaseManager.saveData(recP.jo_planning_to_jo_details);
					lastDet = recP.jo_details_id;
				}
			}
			databaseManager.saveData(jo_planning);
			databaseManager.saveData(jo_planning.jo_planning_to_jo_details);
		}
		/** @type {JSFoundSet<db:/geco/bo_planning>} */
		var bo_planning = databaseManager.getFoundSet('geco', 'bo_planning');
		var totBoP = 0;
		if (bo_planning.find()) {
			bo_planning.bo_planning_to_bo_details.bo_details_to_bo.is_editable = 1;
			bo_planning.bo_planning_to_bo_details.bo_details_to_bo.is_linked_to_job_order = '^=';
			bo_planning.bo_planning_to_bo_details.bo_details_to_bo.job_order_required = '^=';
			bo_planning.bo_planning_to_bo_details.bo_details_to_bo.job_order_id = '^=';
			bo_planning.bo_planning_to_bo_details.profit_cost_type_id = '5';
			bo_planning.bo_planning_to_bo_details.enrollment_number = obj.navCode;
			bo_planning.bo_planning_to_bo_details.real_figure = obj.uid;
			bo_planning.newRecord();
			bo_planning.bo_planning_to_bo_details.bo_details_to_bo.is_editable = 1;
			bo_planning.bo_planning_to_bo_details.bo_details_to_bo.is_linked_to_job_order = '^=';
			bo_planning.bo_planning_to_bo_details.bo_details_to_bo.job_order_required = '^=';
			bo_planning.bo_planning_to_bo_details.bo_details_to_bo.job_order_id = '^=';
			bo_planning.bo_planning_to_bo_details.profit_cost_type_id = '9';
			bo_planning.bo_planning_to_bo_details.enrollment_number = obj.navCode;
			bo_planning.bo_planning_to_bo_details.real_tm_figure = obj.uid;
			totBoP = bo_planning.search();
		}
		bo_planning.sort('bo_id asc, bo_details_id asc');
		if (totBoP > 0) {
			var listBO = [];
			var listBODet = [];
			var recPBo = null;
			for (index = 1; index <= bo_planning.getSize(); index++) {
				recPBo = bo_planning.getRecord(index);
				if (listBO.indexOf(recPBo.bo_id) < 0) listBO.push(recPBo.bo_id);
				if (listBODet.indexOf(recPBo.bo_details_id) < 0) listBODet.push(recPBo.bo_details_id)
			}
			application.output('id BO ' + listBO + ' id dettagli ' + listBODet);
			lastDet = 0;
			for (index = 1; index <= bo_planning.getSize(); index++) {
				recPBo = bo_planning.getRecord(index);
				application.output(recPBo);
				recPBo.days_import = globals.roundNumberWithDecimal(obj.cost, 2);
				recPBo.cost_amount = globals.roundNumberWithDecimal( (obj.cost * recPBo.days_number), 2);
				recPBo.total_amount = recPBo.cost_amount;
				if (listBODet.length > 0 && listBODet.indexOf(recPBo.bo_details_id) >= 0 && lastDet != recPBo.bo_details_id) {
					application.output('ultimo dettaglio ' + lastDet + ' dettaglio attuale ' + recPBo.bo_details_id);
					recPBo.bo_planning_to_bo_details.days_import = globals.roundNumberWithDecimal(obj.cost, 2);
					recPBo.bo_planning_to_bo_details.cost_amount = globals.roundNumberWithDecimal( (obj.cost * recPBo.bo_planning_to_bo_details.days_number), 2);
					recPBo.bo_planning_to_bo_details.total_amount = recPBo.bo_planning_to_bo_details.cost_amount;
					databaseManager.saveData(recPBo.bo_planning_to_bo_details);
					lastDet = recPBo.bo_details_id;
				}
			}
			databaseManager.saveData(bo_planning);
			databaseManager.saveData(bo_planning.bo_planning_to_bo_details);
		}
	}
	//cambio i dettagli non pianificati di commesse o BO aperte con importo giornaliero diverso
	for (y = 0; y < userCostList.length; y++) {
		/** @type {userCost} */
		var objUC = userCostList[y];
		application.output(objUC.cost + ' ' + objUC.navCode + ' ' + objUC.uid);
		/** @type {JSFoundSet<db:/geco/jo_details>} */
		var jo_details = databaseManager.getFoundSet('geco', 'jo_details');
		var totDet = 0;
		if(jo_details.find()){
			jo_details.user_id = objUC.uid;
			jo_details.enrollment_number = objUC.navCode;
			jo_details.days_import = '!'+objUC.cost;
			jo_details.profit_cost_type_id = '5||9';
			jo_details.jo_details_to_job_orders.is_enabled = 1;
			jo_details.jo_details_to_jo_planning.record_count = 0;
			totDet = jo_details.search();
		}
		application.output('Totole dettagli JO trovati senza piani ' + totDet);
		for(var i = 1; i<=jo_details.getSize(); i++){
			var det = jo_details.getRecord(i);
			det.days_import = objUC.cost;
			if (det.days_number!=null && det.days_number!=0) {
				det.total_amount = globals.roundNumberWithDecimal((det.days_import*det.days_number),2)
				det.cost_amount = det.total_amount;
			}
		}
		databaseManager.saveData(jo_details);
		/** @type {JSFoundSet<db:/geco/bo_details>} */
		var bo_details = databaseManager.getFoundSet('geco', 'bo_details');
		var totBoDet = 0;
		if (bo_details.find()) {
			bo_details.bo_details_to_bo.is_editable = 1;
			bo_details.bo_details_to_bo.is_linked_to_job_order = '^=';
			bo_details.bo_details_to_bo.job_order_required = '^=';
			bo_details.profit_cost_type_id = '5';
			bo_details.enrollment_number = objUC.navCode;
			bo_details.real_figure = obj.uid;
			bo_details.days_import = '!'+objUC.cost;
			bo_details.newRecord();
			bo_details.bo_details_to_bo.is_editable = 1;
			bo_details.bo_details_to_bo.is_linked_to_job_order = '^=';
			bo_details.bo_details_to_bo.job_order_required = '^=';
			bo_details.profit_cost_type_id = '9';
			bo_details.enrollment_number = objUC.navCode;
			bo_details.real_tm_figure = obj.uid;
			bo_details.days_import = '!'+objUC.cost;			
			totBoDet = bo_details.search();
		}
		application.output('Totole dettagli BO trovati senza piani ' + totBoDet);
		for(i = 1; i<=bo_details.getSize(); i++){
			var detBO = bo_details.getRecord(i);
			detBO.days_import = objUC.cost;
			if (detBO.days_number!=null && detBO.days_number!=0) {
				detBO.total_amount = globals.roundNumberWithDecimal((detBO.days_import*detBO.days_number),2)
				detBO.cost_amount = detBO.total_amount;
			}
		}
		databaseManager.saveData(bo_details);
	}
	
	elements.buttonLoad.enabled = !isLoad;
	(caseErrorMessage==0)?uploadResult = 'File caricato con successo.':uploadResult = errorMessage;
	application.output(globals.messageLog + 'STOP dialog_import_fil	e.uploadUsersCost OK', LOGGINGLEVEL.DEBUG);
}

/**
 * @param {plugins.file.JSFile} file
 *
 * @properties={typeid:24,uuid:"F798A61D-5EF2-4A87-BB92-674B1C22558E"}
 * @AllowToRunInFind
 */
function uploadJobOrdersActualSP(file) {
	application.output(globals.messageLog + 'START dialog_import_file.uploadJobOrdersActualSP(file) OK', LOGGINGLEVEL.DEBUG);

	/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
	var boActualDate = databaseManager.getFoundSet('geco', 'bo_actual_date');
	boActualDate.loadRecords(); //per sicurezza, così dovrebbe tirare giù il record
	var recActualDate = boActualDate.getRecord(1);
	var selectedDate = new Date(year, month, 0);
	var question = '';
	/** @type {Number} */
	var caso = 0;

	if (recActualDate != null) {
		if (year < recActualDate.actual_year) {
			question = globals.DIALOGS.showInfoDialog('ATTENZIONE', 'Il file che stai per caricare ha una data actual antecedente a quella attuale (' + recActualDate.actual_month + '/' + recActualDate.actual_year + ').\n\nVuoi procedere comunque? ', 'OK', 'ANNULLA');
			if (question != 'OK') {
				return;
			}
			caso = 1;
		} else if (year == recActualDate.actual_year) {
			if ( (month - recActualDate.actual_month) >= 2) {
				globals.DIALOGS.showErrorDialog('ERRORE', 'Il file che stai cercando di caricare ha una data actual superiore di ' + (month - recActualDate.actual_month) + ' mesi rispetto a quella attuale.', 'OK');
				return;
			}

			if (month < recActualDate.actual_month) {
				question = globals.DIALOGS.showInfoDialog('ATTENZIONE', 'Il file che stai per caricare ha una data actual antecedente a quella attuale (' + recActualDate.actual_month + '/' + recActualDate.actual_year + ').\n\nVuoi procedere comunque? ', 'OK', 'ANNULLA');
				if (question != 'OK') {
					return;
				}
				caso = 1;
			}
			application.output('month: ' + month + '; recActualDate.actual_month: ' + recActualDate.actual_month);
		} else if (year > recActualDate.actual_year) {
			if (month == 1 && recActualDate.actual_month == 12 && (year - recActualDate.actual_year) == 1) {
			} else {
				globals.DIALOGS.showErrorDialog('ERRORE', 'Il file che stai per caricare ha una data actual superiore rispetto a quella attuale.', 'OK');
				return;
			}
		}
	}

	application.output('caso: ' + caso);
	//	var x = 0;
	//	if(x == 0) return;
	if (addTemporaryRecords(file) == true) {
		uploadResult = 'File Caricato con successo.';
		try {
			//INSERIMENTO ACTUAL
			var query = 'call sp_loadActualJobOrder(?)';
			var args = [scopes.globals.currentUserId];
			var dataset = databaseManager.getDataSetByQuery('geco', query, args, -1);
			var rows = dataset.getMaxRowIndex();
			for (var i = 1; i <= rows; i++) {
				application.output(dataset.getRowAsArray(i));
				uploadResult = uploadResult + '\n' + dataset.getRowAsArray(i);
			}
			plugins.rawSQL.flushAllClientsCache("geco", "jo_planning");
			plugins.rawSQL.flushAllClientsCache("geco", "jo_details");
			plugins.rawSQL.flushAllClientsCache("geco", "job_orders");
		} catch (e) {
			application.output(globals.messageLog + 'ERROR dialog_import_file.uploadJobOrdersActualSP(file) ' + e, LOGGINGLEVEL.ERROR);
			uploadResult = 'errore caricamento dati';
			return
		}

		//se non ci sono record lo creo
		if (recActualDate == null) {
			boActualDate.newRecord();
			//nuovi valori
			boActualDate.actual_date = selectedDate;
			boActualDate.actual_month = month;
			boActualDate.actual_year = year;
			//boActualDate.is_open_to_actual = 1;
			boActualDate.is_open_to_actual = 0;
			boActualDate.month_status_actual = 1;
			//valori precedenti, in questo caso gli stessi
			boActualDate.previous_actual_date = selectedDate;
			boActualDate.previous_actual_month = month;
			boActualDate.previous_actual_year = year;

			var savedData = databaseManager.saveData(boActualDate);
			application.output('savedData: ' + savedData);
		}//altrimento lo modifico salvando i precedenti valori di actual nei rispettivi previous SE sto inserendo gli actual per il mese successivo (caso 0)
		else {
			if (caso == 0) {
				recActualDate.previous_actual_date = recActualDate.actual_date;
				recActualDate.previous_actual_month = recActualDate.actual_month;
				recActualDate.previous_actual_year = recActualDate.actual_year;

				recActualDate.actual_date = selectedDate;
				recActualDate.actual_month = month;
				recActualDate.actual_year = year;
				recActualDate.is_open_to_actual = 0;
				recActualDate.month_status_actual = 1;

				var saveModifiedRecords = databaseManager.saveData(recActualDate);
				application.output('saveModifiedRecords: ' + saveModifiedRecords);
			}
		}
	} else {
		//addTemporaryRecords è false; sono qui solo quando tutte le righe del file hanno la data != da quella selezionata.
		uploadResult = 'File caricato con successo.\nErrore: data su file diversa dalla data scelta.\nNessun record caricato.';
	}
	application.output(globals.messageLog + 'STOP dialog_import_file.uploadJobOrdersActualSP(file) OK', LOGGINGLEVEL.DEBUG);
}

/**
 * @param {plugins.file.JSFile} file
 *
 * @properties={typeid:24,uuid:"967E6FF0-CC56-4ACD-8F88-397CED34A0ED"}
 * @AllowToRunInFind
 */
function uploadJobOrdersActual(file) {
	application.output(globals.messageLog + 'START dialog_import_file.uploadJobOrdersActual(file) OK', LOGGINGLEVEL.DEBUG);
	//	var success = updateUploadResult();
	//	if (success == false){
	//		return;
	//	}

	//I record sono stati aggiunti alla tabella temporanea, procedo.
	if (addTemporaryRecords(file) == true) {
		/** @type {JSFoundSet<db:/geco/job_orders>} */
		var jobOrders = databaseManager.getFoundSet('geco', 'job_orders');
		/** @type {JSFoundSet<db:/geco/bo_users_cost>} */
		var boUsersCost = databaseManager.getFoundSet('geco', 'bo_users_cost');
		/** @type {JSFoundSet<db:/geco/users>} */
		var users = databaseManager.getFoundSet('geco', 'users');
		/** @type {JSFoundSet<db:/geco/jo_details>} */
		var joDetails = databaseManager.getFoundSet('geco', 'jo_details');
		/** @type {JSFoundSet<db:/geco/tmp_actual_costing>} */
		var tmpActualCosting = databaseManager.getFoundSet('geco', 'tmp_actual_costing');
		/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
		var boActualDate = databaseManager.getFoundSet('geco', 'bo_actual_date');
		/** @type {JSRecord<db:/geco/jo_details>} */
		var recDetails = null;

		//ricarico e ordino i record per cod_commessa ascendente
		tmpActualCosting.loadAllRecords();
		tmpActualCosting.sort('nome_risorsa asc, cod_commessa asc, voce_costo_ricavo asc');

		//totali da verificare dopo la find();
		/** @type {Number} */
		var totJobOrders = 0;
		/** @type {Number} */
		var totBoUsersCost = 0;
		/** @type {Number} */
		var totUsers = 0;
		/** @type {Number} */
		var totJoDetails = 0;
		/** @type {Number} */
		var totJoPlanning = 0;
		/** @type {Number} */
		var totBoActualDate = 0;

		/** @type {Number} */
		var uID = 0; //user_id da users
		/** @type {Number} */
		var joCode = 0; //jo_id da job_orders
		/** @type {Number} */
		var joCostTypeID = 0; //profit_cost_types_id da profit_cost_types

		var selectedDate = new Date(year, month, 0);
		var plainFound = false;
		var lastDetId = null;
		var isInternal = false;

		application.output('totale record = ' + tmpActualCosting.getSize())
		application.output('totale record = ' + databaseManager.getFoundSetCount(tmpActualCosting))

		for (var v = 1; v <= tmpActualCosting.getSize(); v++) {
			var selectedTMPRecord = tmpActualCosting.getRecord(v); //salvo il record all'iterazione v in una variabile
			application.output('--------------------------------------------------------------------------------------\nrecord temporaneo ' + selectedTMPRecord);
			var joNavCode = selectedTMPRecord.cod_commessa;
			//var joBO = selectedTMPRecord.cod_bo;
			//var joStatus = selectedTMPRecord.stato_commessa;
			var joType = selectedTMPRecord.voce_costo_ricavo;
			var userNavCode = selectedTMPRecord.cod_risorsa;
			var realName = selectedTMPRecord.nome_risorsa;
			//var payType = selectedTMPRecord.pay_type;
			var daysNumberActual = selectedTMPRecord.gg_effort;
			//var dateOnFile = selectedTMPRecord.mese_competenza;
			var daysImportActual = selectedTMPRecord.costo_std_gg;
			var costActual = selectedTMPRecord.costo_euro_mese;
			var meseCompetenza = selectedTMPRecord.mese_competenza;
			//var joCompetenceYear = selectedTMPRecord.anno_competenza;

			//cerco la commessa in job_orders per codice navision della commessa
			if (jobOrders.find()) {
				jobOrders.external_code_navision = globals.trimString(joNavCode);
				totJobOrders = jobOrders.search();
			}

			if (totJobOrders != 0) {
				//salvo il job_order_id
				joCode = jobOrders.job_order_id;
				isInternal = (jobOrders.job_order_type == 0) ? true : false;
				try {
					//INSERIMENTO STORICO
					var query = 'call sp_insert_history_jo(?,?,?,?,?,?)';
					var args = [joCode, scopes.globals.currentUserId, ('before actual by user ' + scopes.globals.currentUserDisplayName), month, year, 1];
					var dataset = databaseManager.getDataSetByQuery('geco', query, args, -1);
					application.output(dataset.getMaxRowIndex());
					//FINE INSERIMENTO STORICO
				} catch (e) {
					application.output('Error: ' + e);
				}

				//cerco l'utente in bo_users_cost per userNavCode
				if (boUsersCost.find()) {
					boUsersCost.enrollment_number = userNavCode;
					totBoUsersCost = boUsersCost.search();
				}
				var nameToDB = '';

				if (totBoUsersCost != 0) {
					uID = boUsersCost.user_id;
					nameToDB = boUsersCost.bo_users_cost_to_users.users_to_contacts.real_name;
					//application.output('****Utente trovato in bo_users_cost: ' + realName + '; uID: ' + uID +'; NavCode: '+userNavCode);
				}
				//devo cercare l'utente in users, sempre per userNavCode o in contacts per real_name
				else {
					if (users.find()) {
						users.enrollment_number = userNavCode;
						users.newRecord();
						users.users_to_contacts.real_name = realName;
						totUsers = users.search();
					}

					if (totUsers != 0) {
						uID = users.user_id;
						nameToDB = users.users_to_contacts.real_name;
						application.output('****Utente trovato in users: ' + realName + '; uID: ' + uID);
					} else {
						application.output('-----ERRORE: UTENTE ' + realName + ' NON TROVATO DA NESSUNA PARTE.');
						break;
					}
				}

				//Assegno al tipo di costo il suo typeID dal file
				application.output('TIPOLOGIA ' + joType);
				if (joType == 'Spese trasferta') {
					//joType = 'Spese';
					joCostTypeID = 10;
				} else if (joType == 'Personale') {
					//joType = 'Personale';
					joCostTypeID = 5;
				} else if (joType == 'FornitureTM') {
					//joType = 'Fornitura TM';
					joCostTypeID = 9;
				}

				//vado in find su jo_details e cerco se esiste il dettaglio per jo_id, profit_cost_type_id, user_id
				if (joDetails.find()) {
					joDetails.jo_id = joCode;
					joDetails.profit_cost_type_id = joCostTypeID;
					if (joCostTypeID == 5 || joCostTypeID == 10) {
						joDetails.real_figure = uID;
					} else {
						joDetails.real_tm_figure = uID;
					}
					totJoDetails = joDetails.search();
				}

				//application.output('UID: ' + uID + '; ' + realName + '; JoCode: ' + joCode + '; JoType: ' + joType + '; JoID: ' + joCostTypeID + '; Costo Actual: ' + costActual + '; daysNumberActual: ' + daysNumberActual + '; daysImportActual: ' + daysImportActual);

				//se ci sono più dettagli, prendo e modifico il primo
				plainFound = false;
				if (totJoDetails > 0) {
					for (var i = 1; i <= totJoDetails; i++) {
						recDetails = joDetails.getRecord(i);
						application.output('DETTAGLIO ' + recDetails);

						//Esiste il dettaglio, verifico che esiste anche la pianificazione
						if (recDetails.jo_details_to_jo_planning.find()) {
							recDetails.jo_details_to_jo_planning.jo_month = month;
							recDetails.jo_details_to_jo_planning.jo_year = year;
							totJoPlanning = recDetails.jo_details_to_jo_planning.search();
						}
						application.output('Pianificazioni trovate pe ril dettaglio ' + totJoPlanning);
						//Esiste anche la pianificazione, aggiorno i valori di quest'ultima
						if (totJoPlanning > 0) {
							application.output('Piano trovato da modificare: -' + recDetails.jo_details_to_jo_planning + '-');
							for (var jp = 1; jp <= totJoPlanning; jp++) {
								var joPl = recDetails.jo_details_to_jo_planning.getRecord(jp);
								application.output('RECORD PRIMA ' + joPl);
								//se ultimo dettaglio = dettaglio attuale, importi = somma degli ATTUALI + RIGA FILE
								if (lastDetId == joPl.jo_details_id) {
									costActual = joPl.cost_actual + costActual;
									daysNumberActual = joPl.days_number_actual + daysNumberActual;
								}

								joPl.days_number_actual = daysNumberActual;
								joPl.days_import_actual = daysImportActual;
								joPl.total_actual = costActual;
								joPl.cost_actual = costActual;
								joPl.date_actual = new Date();
								joPl.user_actual = globals.currentUserId;
								joPl.status = 8;
								joPl.is_actual = 1;
								joPl.competence_month = meseCompetenza;
								joPl.user_id = uID;
								//se intena aggiorno anche i budget
								if (isInternal == true) {
									joPl.cost_amount = joPl.cost_actual;
									joPl.total_amount = joPl.cost_actual;
									joPl.days_number = joPl.days_number_actual
									joPl.days_import = joPl.days_import_actual
								}
								var s = databaseManager.saveData(joPl);
								application.output(s + ' RECORD DOPO  ' + joPl);

							}
							plainFound = true;
						}
					}

					//NON esiste la pianificazione ma solo il dettaglio, creo la pianificazione passando per relazione sul dettaglio
					//imposto anche i valori non actual
					application.output('plainFound ' + plainFound)
					if (plainFound == false) {
						recDetails = joDetails.getRecord(1);
						recDetails.jo_details_to_jo_planning.newRecord();
						recDetails.jo_details_to_jo_planning.jo_id = recDetails.jo_id;
						recDetails.jo_details_to_jo_planning.bo_details_id = recDetails.bo_details_id;
						recDetails.jo_details_to_jo_planning.bo_id = recDetails.bo_id;
						recDetails.jo_details_to_jo_planning.jo_year = year;
						recDetails.jo_details_to_jo_planning.jo_month = month;
						recDetails.jo_details_to_jo_planning.days_number_actual = daysNumberActual;
						recDetails.jo_details_to_jo_planning.days_import_actual = daysImportActual;
						recDetails.jo_details_to_jo_planning.days_number = daysNumberActual;
						recDetails.jo_details_to_jo_planning.days_import = daysImportActual;
						recDetails.jo_details_to_jo_planning.total_actual = costActual;
						recDetails.jo_details_to_jo_planning.cost_actual = costActual;
						recDetails.jo_details_to_jo_planning.total_amount = costActual;
						recDetails.jo_details_to_jo_planning.cost_amount = costActual;
						recDetails.jo_details_to_jo_planning.date_actual = new Date();
						recDetails.jo_details_to_jo_planning.user_actual = globals.currentUserId;
						recDetails.jo_details_to_jo_planning.status = 8;
						recDetails.jo_details_to_jo_planning.is_actual = 1;
						recDetails.jo_details_to_jo_planning.user_id = uID;
						recDetails.jo_details_to_jo_planning.competence_month = meseCompetenza;

						//se intena aggiorno anche i budget
						if (isInternal == true) {
							recDetails.jo_details_to_jo_planning.cost_amount = recDetails.jo_details_to_jo_planning.cost_actual;
							recDetails.jo_details_to_jo_planning.total_amount = recDetails.jo_details_to_jo_planning.cost_actual;
							recDetails.jo_details_to_jo_planning.days_number = recDetails.jo_details_to_jo_planning.days_number_actual
							recDetails.jo_details_to_jo_planning.days_import = recDetails.jo_details_to_jo_planning.days_import_actual
						}

						databaseManager.saveData(recDetails.jo_details_to_jo_planning);
						databaseManager.saveData(recDetails);
						application.output('Piano non trovato creato: ' + recDetails.jo_details_to_jo_planning);
					}
					var foundsetSaved = databaseManager.saveData(joDetails);
					application.output('foundsetSaved ' + foundsetSaved);
				} else {
					//non esiste il dettaglio, devo aggiungerlo e aggiungere anche la pianificazione
					application.output(joDetails.getSize());
					var idx = joDetails.newRecord(false);
					//var idx = jobOrders.job_orders_to_jo_details.newRecord(false);
					//					jobOrders.job_orders_to_jo_details.date_actual = selectedDate;
					//					jobOrders.job_orders_to_jo_details.user_actual = globals.currentUserId;
					//					//joDetails.jo_id = joCode;
					//					jobOrders.job_orders_to_jo_details.profit_cost_type_id = joCostTypeID;
					//					if (joCostTypeID == 5 || joCostTypeID == 10) {
					//						jobOrders.job_orders_to_jo_details.real_figure = uID;
					//					} else {
					//						jobOrders.job_orders_to_jo_details.real_tm_figure = uID;
					//					}
					//					jobOrders.job_orders_to_jo_details.description = nameToDB;
					joDetails.date_actual = selectedDate;
					joDetails.user_actual = globals.currentUserId;
					joDetails.jo_id = joCode;
					joDetails.profit_cost_type_id = joCostTypeID;
					if (joCostTypeID == 5 || joCostTypeID == 10) {
						joDetails.real_figure = uID;
					} else {
						joDetails.real_tm_figure = uID;
					}
					joDetails.description = nameToDB;
					joDetails.days_number = daysNumberActual;
					joDetails.days_import = daysImportActual;
					joDetails.total_amount = costActual;
					joDetails.cost_amount = costActual;
					//application.output(jobOrders.job_orders_to_jo_details);
					var recNewDet = joDetails.getRecord(idx);
					application.output(recNewDet);
					application.output(idx + ' dettaglio creato ' + joDetails.jo_details_id + ' commessa ' + joDetails.jo_id)
					joDetails.jo_details_to_jo_planning.newRecord();
					joDetails.jo_details_to_jo_planning.jo_id = joDetails.jo_id;
					joDetails.jo_details_to_jo_planning.bo_details_id = joDetails.bo_details_id;
					joDetails.jo_details_to_jo_planning.bo_id = joDetails.bo_id;
					joDetails.jo_details_to_jo_planning.jo_year = year;
					joDetails.jo_details_to_jo_planning.jo_month = month;
					joDetails.jo_details_to_jo_planning.days_number_actual = daysNumberActual;
					joDetails.jo_details_to_jo_planning.days_import_actual = daysImportActual;
					joDetails.jo_details_to_jo_planning.total_actual = costActual;
					joDetails.jo_details_to_jo_planning.cost_actual = costActual;
					joDetails.jo_details_to_jo_planning.days_number = daysNumberActual;
					joDetails.jo_details_to_jo_planning.days_import = daysImportActual;
					joDetails.jo_details_to_jo_planning.total_amount = costActual;
					joDetails.jo_details_to_jo_planning.cost_amount = costActual;
					joDetails.jo_details_to_jo_planning.date_actual = new Date();
					joDetails.jo_details_to_jo_planning.user_actual = globals.currentUserId;
					joDetails.jo_details_to_jo_planning.status = 8;
					joDetails.jo_details_to_jo_planning.is_actual = 1;
					joDetails.jo_details_to_jo_planning.user_id = uID;
					joDetails.jo_details_to_jo_planning.competence_month = meseCompetenza;

					//se intena aggiorno anche i budget
					if (isInternal == true) {
						joDetails.jo_details_to_jo_planning.cost_amount = joDetails.jo_details_to_jo_planning.cost_actual;
						joDetails.jo_details_to_jo_planning.total_amount = joDetails.jo_details_to_jo_planning.cost_actual;
						joDetails.jo_details_to_jo_planning.days_number = joDetails.jo_details_to_jo_planning.days_number_actual
						joDetails.jo_details_to_jo_planning.days_import = joDetails.jo_details_to_jo_planning.days_import_actual
					}

					databaseManager.saveData(joDetails);
					databaseManager.saveData(joDetails.jo_details_to_jo_planning);

					application.output('Dettaglio non trovato creato: ' + joDetails);
					application.output('Piano creato: ' + joDetails.jo_details_to_jo_planning);
				}//fine dettaglio non trovato
				//application.output('UID: ' + uID + '; ' + realName + '; JoCode: ' + joCode + '; JoType: ' + joType + '; JoID: ' + joTypeID + '; Costo Actual: ' + costActual);

			} else {
				application.output('-----COMMESSA ' + joNavCode + ' NON TROVATA in job_orders.');
			}
			if (recDetails != null)
				lastDetId = recDetails.jo_details_id;
			application.output('lastDetId: ' + lastDetId);
		}
		application.output('PRIMA DEL SAVE GLOBALE')
		var savedAll = databaseManager.saveData();
		//var commitAll = databaseManager.commitTransaction();
		application.output('DOPO DEL SAVE GLOBALE ' + savedAll)

		//tutto è andato bene, aggiungo in bo_actual_date le informazioni mancanti..
		if (boActualDate.find()) {
			totBoActualDate = boActualDate.search();
		}

		var recBoActualDate = null;
		//se non ci sono record lo creo
		if (totBoActualDate == 0) {
			boActualDate.newRecord();
			//nuovi valori
			boActualDate.actual_date = selectedDate;
			boActualDate.actual_month = month;
			boActualDate.actual_year = year;
			//boActualDate.is_open_to_actual = 1;
			boActualDate.is_open_to_actual = 0;
			//valori precedenti, in questo caso gli stessi
			boActualDate.previous_actual_date = selectedDate;
			boActualDate.previous_actual_month = month;
			boActualDate.previous_actual_year = year;

			var savedData = databaseManager.saveData(boActualDate);
			application.output('savedData: ' + savedData);
		}//altrimento lo modifico, salvando i precedenti valori di actual nei rispettivi previous
		else {
			recBoActualDate = boActualDate.getRecord(1);
			recBoActualDate.previous_actual_date = recBoActualDate.actual_date;
			recBoActualDate.previous_actual_month = recBoActualDate.actual_month;
			recBoActualDate.previous_actual_year = recBoActualDate.actual_year;

			recBoActualDate.actual_date = selectedDate;
			recBoActualDate.actual_month = month;
			recBoActualDate.actual_year = year;
			//recBoActualDate.is_open_to_actual = 1;
			recBoActualDate.is_open_to_actual = 0;
		}

		//è andato tutto bene, come ultima cosa cancello i record nella tabella temporanea.
		//var deletedTemporaryRecords = deleteTemporaryRecords();
		//application.output('deletedTemporaryRecords? ' + deletedTemporaryRecords);

		if (skippedLine != 0) {
			uploadResult = 'File caricato con successo.\n' + ( (conteggioTMPrecords - 1) - (skippedLine + 1)) + ' record caricati su ' + (conteggioTMPrecords - 1) + ' totali.';
		} else {
			uploadResult = 'File caricato con successo.\n'; //+ (conteggioTMPrecords-1) + ' record caricati.';
		}
		application.output(globals.messageLog + 'STOP dialog_import_file.uploadJobOrdersActual(file) OK', LOGGINGLEVEL.DEBUG);

	} else {
		//addTemporaryRecords è false; sono qui solo quando tutte le righe del file hanno la data != da quella selezionata.
		uploadResult = 'File caricato con successo.\nErrore: data su file diversa dalla data scelta.\nNessun record caricato.';

		application.output(globals.messageLog + 'STOP dialog_import_file.uploadJobOrdersActual(file) OK', LOGGINGLEVEL.DEBUG);
	}

}

/**
 * Handle hide window.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"9BE3B5D5-5EC4-4FF1-BA52-CA7420CCBCE4"}
 */
function onHide(event) {
	//application.output('sono sull\'on hide');
	clearField();
	databaseManager.setAutoSave(true);
	if (controller.getWindow())
		controller.getWindow().destroy();
}

/**
 * @properties={typeid:24,uuid:"322515F3-3BA2-4FC3-A285-66CB45E06FDF"}
 */
function clearField() {
	globals.resultImportDocuments = null;
	doc_type = null;
	document_path = '';
	document_name = '';
	year = null;
	month = null;
	uploadResult = null;
	skippedLine = 0;
}

/**
 *
 * @param {plugins.file.JSFile} file
 *
 * @returns {Boolean} true if the records have been inserted into the database
 *
 * @properties={typeid:24,uuid:"BF700526-D907-4F69-A26F-2A80F3361C24"}
 *
 * @AllowToRunInFind
 */
function addTemporaryRecords(file) {
	application.output(globals.messageLog + 'START dialog_import_file.addTemporaryRecords(file)', LOGGINGLEVEL.DEBUG);
	var fr = new Packages.java.io.FileReader(file.getAbsolutePath());
	var br = new Packages.java.io.BufferedReader(fr);
	var line = '';
	conteggioTMPrecords = 0;

	while ( (line = br.readLine()) != null) {
		//parto dalla seconda iterazione, perchè alla prima ci sono i nomi delle colonne
		if (conteggioTMPrecords > 0) {
			//application.output(line);
			/** @type {Number} */
			var selectedDate = new Date(year, month, 0);
			var arrayLine = line.split(';');
			/** @type {String} */
			var dateStr = arrayLine[8];
			var dateArr = dateStr.split('/');
			var isEntireDate = dateArr[2];
			var dateOnFile = new Date(0, 0, 0);
			if (isEntireDate != null) {
				/** @type {Number} */
				var yearFile = isEntireDate.length == 2 ? '20' + dateArr[2] : dateArr[2];
				/** @type {Number} */
				var monthFile = dateArr[1] - 1;
				/** @type {Number} */
				var dayFile = dateArr[0];
				dateOnFile = new Date(yearFile, monthFile, dayFile);
			}
			//application.output('selectedDate: ' + selectedDate + '; dateOnFile: ' + dateOnFile);

			//se la Data selezionata corrisponde alla data sul file, copio i dati dal file nella tab. temporanea.
			if (selectedDate == dateOnFile) {
				//variabili del file da salvare
				/** @type {String} */
				var joNavCode = arrayLine[0];
				/** @type {Number} */
				var joBO = arrayLine[1];
				/** @type {String} */
				var joStatus = arrayLine[2];
				/** @type {String} */
				var joType = arrayLine[3];
				/** @type {String} */
				var userNavCode = arrayLine[4];
				/** @type {String} */
				var realName = arrayLine[5];
				/** @type {String} */
				var payType = arrayLine[6];
				/** @type {String} */
				var daysNumberActualStr = arrayLine[7].replace(',', '.');
				/** @type {Number} */
				var daysNumberActual = daysNumberActualStr;

				/** @type {String} */
				var daysImportActualStr = arrayLine[9].replace(',', '.');
				/** @type {Number} */
				var daysImportActual = daysImportActualStr;
				/** @type {String} */
				var costActualNeg = arrayLine[10].replace(',', '.');
				/** @type {Number} */
				var costActual = -costActualNeg;
				/** @type {Number} */
				var joCompetenceYear = arrayLine[11];

				/** @type {JSFoundSet<db:/geco/tmp_actual_costing>} */
				var tmpActualCosting = databaseManager.getFoundSet('geco', 'tmp_actual_costing');

				try {
					tmpActualCosting.newRecord();
					tmpActualCosting.cod_commessa = joNavCode;
					tmpActualCosting.cod_bo = joBO;
					tmpActualCosting.stato_commessa = joStatus;
					tmpActualCosting.voce_costo_ricavo = joType;
					tmpActualCosting.cod_risorsa = userNavCode;
					tmpActualCosting.nome_risorsa = realName;
					tmpActualCosting.pay_type = payType;
					tmpActualCosting.gg_effort = daysNumberActual;
					tmpActualCosting.mese_competenza = dateOnFile;
					tmpActualCosting.costo_std_gg = daysImportActual;
					tmpActualCosting.costo_euro_mese = costActual;
					tmpActualCosting.anno_competenza = joCompetenceYear;

					databaseManager.saveData(tmpActualCosting);
				} catch (e) {
					application.output(globals.messageLog + 'ERROR dialog_import_file.addTemporaryRecords(file) KO - Non riesco a creare record nella tabella temporanea: ' + e, LOGGINGLEVEL.DEBUG);
					uploadResult = 'File non caricato.\nNon riesco a creare record nella tabella temporanea.';
					return false;
				}
			} else {
				//se tutte le righe del file hanno la data != da quella selezionata, dovrò ritornare false alla fine.

				skippedLine++;
				//application.output('skippedLine: ' + skippedLine + '; count: ' + count);
			}
		}
		conteggioTMPrecords++;
	}
	isLoad = true;
	elements.buttonLoad.enabled = !isLoad;
	//uploadResult = 'Operazione in corso';

	//se tutte le righe del file hanno la data != da quella selezionata, dovrò stampare qualcosa dopo.
	if (skippedLine == conteggioTMPrecords - 1) {
		uploadResult = '';
		application.output(globals.messageLog + ' STOP dialog_import_file.addTemporaryRecords(file) OK', LOGGINGLEVEL.DEBUG);
		return false;
	} else {
		application.output(globals.messageLog + ' STOP dialog_import_file.addTemporaryRecords(file) OK', LOGGINGLEVEL.DEBUG);
		return true;
	}

}

/**
 * @returns {Boolean} true when record was deleted from database
 *
 * @properties={typeid:24,uuid:"AEA375B6-F04B-46C8-8040-BB28111E86F0"}
 */
function deleteTemporaryRecords() {
	application.output(globals.messageLog + 'START dialog_import_file.deleteTemporaryRecords() ', LOGGINGLEVEL.DEBUG);
	/** @type {JSFoundSet<db:/geco/tmp_actual_costing>} */
	var tmpActualCosting = databaseManager.getFoundSet('geco', 'tmp_actual_costing');
	try {
		tmpActualCosting.loadAllRecords();
		tmpActualCosting.deleteAllRecords();
		application.output(globals.messageLog + 'STOP dialog_import_file.deleteTemporaryRecords() OK', LOGGINGLEVEL.DEBUG);
		return true;
	} catch (e) {
		application.output(globals.messageLog + 'STOP dialog_import_file.deleteTemporaryRecords() ERROR ' + e.message, LOGGINGLEVEL.DEBUG);
		//globals.DIALOGS.showErrorDialog('Error', globals.validationExceptionMessage, 'OK');
		return false;
	}
}

/**
 * @param {plugins.file.JSFile} file
 *
 * @properties={typeid:24,uuid:"27904E7A-A39B-4566-BA1F-A9FCBDA112BF"}
 * @AllowToRunInFind
 */
function addProfessionalCost(file) {
	application.output(globals.messageLog + 'START dialog_import_file.addProfessionalCost(file)', LOGGINGLEVEL.DEBUG);
	var fr = new Packages.java.io.FileReader(file.getAbsolutePath());
	var br = new Packages.java.io.BufferedReader(fr);
	var line = '';
	conteggioStandProfFigures = 0;
	var createdRecord = 0;

	while ( (line = br.readLine()) != null) {
		//parto dalla seconda iterazione, perchè alla prima ci sono i nomi delle colonne
		if (conteggioStandProfFigures > 0) {
			var arrayLine = line.split(';');
			/** @type {String} */
			var jobPosition = arrayLine[0];
			/** @type {Number} */
			var mediumCostByDay = arrayLine[1];
			/** @type {Number} */
			var totStandProfFigures = 0;

			/** @type {JSFoundSet<db:/geco/standard_professional_figures>} */
			var standProfFigures = databaseManager.getFoundSet('geco', 'standard_professional_figures');

			if (standProfFigures.find()) {
				standProfFigures.description = jobPosition;
				totStandProfFigures = standProfFigures.search();
			}
			//application.output('totStandProfFigures: ' + totStandProfFigures);

			//modifico quello trovato mettendo il nuovo costo
			if (totStandProfFigures != 0) {
				standProfFigures.standard_cost = mediumCostByDay;
				databaseManager.saveData(standProfFigures);
			} else if (totStandProfFigures > 1) { //ho trovato due record con la stessa descrizione, che faccio?
				application.output('Trovati 2 record con la stessa descrizione, non faccio nulla.');
			}//creo record con jobPosition e mediumCostByDay
			else {
				try {
					standProfFigures.newRecord(false);
					standProfFigures.description = jobPosition;
					standProfFigures.standard_cost = mediumCostByDay;
					databaseManager.saveData(standProfFigures);

					createdRecord++;
				} catch (e) {
					application.output(globals.messageLog + 'ERROR dialog_import_file.addProfessionalCost(file) Non riesco a creare nuovo record nella tabella \'standard_professional_figures\: ' + e, LOGGINGLEVEL.DEBUG);
					uploadResult = 'File caricato in parte:\nNon sono riuscito a inserire il record ' + (conteggioStandProfFigures - 1) + ' nella tabella \'standard_professional_figures\'.';
				}
			}
		}
		conteggioStandProfFigures++;
	}
	isLoad = true;
	elements.buttonLoad.enabled = !isLoad;
	(createdRecord == 0) ? uploadResult = 'File caricato con successo.' : uploadResult = 'File caricato con successo.\n' + createdRecord + ' nuovi record caricati su ' + (conteggioStandProfFigures - 1) + ' totali.';
	application.output(globals.messageLog + 'STOP dialog_import_file.addProfessionalCost(file) OK', LOGGINGLEVEL.DEBUG);
}

///**
// * @returns {Boolean} true
// * @properties={typeid:24,uuid:"7F766995-62FE-495E-9E48-6B365CC63E3A"}
// */
//function updateUploadResult(){
//	uploadResult = 'Il documento sta per essere caricato.\nAttendere prego..';
//
//	if(uploadResult != ''){
//		return true;
//	}else{
//		return false;
//	}
//}
