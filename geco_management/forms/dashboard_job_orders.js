/**
 * @type {Number}
 * @properties={typeid:35,uuid:"5690D588-08BB-49AC-9EB6-C4D9D74F01E8",variableType:8}
 */
var jobOrderId = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"625398FD-D86F-4B5A-B3B9-A35A20F19394",variableType:8}
 */
var marketId = null;

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"0B09551F-3874-49C8-B445-F9353ED91251"}
 */
function updateUI(event) {
	_super.updateUI(event);
	//controller.readOnly = true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} direction
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"9C7A17E3-7F3F-48E9-9248-F11CDDB75D9F"}
 */
function changeYear(event, direction) {
	//controller.readOnly = false;

	if (direction == 'avanti')
		scopes.globals.selectedYear = scopes.globals.selectedYear + 1;
	else if (direction == 'indietro')
		scopes.globals.selectedYear = scopes.globals.selectedYear - 1;
	applyFilter(event);
	updateUI(event);
	
	return true
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"9640FCD3-AF94-439A-8D75-3830A53EAF35"}
 */
function applyFilter(event) {
	if (foundset.find()) {
		foundset.is_enabled = 1;
		foundset.job_order_id = jobOrderId;
		foundset.profit_center_id = marketId;
		
		foundset.search();
	}
	//application.output(databaseManager.getFoundSetCount(foundset));
	//foundset.getRecord(databaseManager.getFoundSetCount(foundset));
	foundset.sort('job_order_id asc');
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"D2F8A359-1CA3-40B0-A44E-A2E95199EC48"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"76DE1F48-A81D-4181-B985-BEDF1D10AF39"}
 */
function onLoad(event) {
	applyFilter(event);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"AFD7BFB7-07FA-459E-A62E-D83FFE0BC440"}
 */
function resetFilter(event) {
	marketId = null;
	jobOrderId = null;
	applyFilter(event);
}
