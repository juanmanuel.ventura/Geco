/**
 * @properties={typeid:35,uuid:"2CCD719D-AB8F-4061-BFA6-1F274DA1B6E0",variableType:-4}
 */
var competenceMonth = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"7F9D026B-CC2E-4CAC-AE51-73F0DBC3F92B",variableType:4}
 */
var ev1 = 0;

/**
 * @type {Object}
 * @properties={typeid:35,uuid:"DAEEA3E9-7A11-4404-9C60-C74425F48F7B",variableType:-4}
 */
var obj = {
	id: null,
	daysNumber: null,
	totalAmount: null,
	costAmount: null,
	returnAmount: null,
	amount: null
};

/**
 * @type {JSRecord[]}
 * @properties={typeid:35,uuid:"AF51EF5A-63C7-4807-B5D3-8BC2858CCED1",variableType:-4}
 */
var planningChangePrev = []

/**
 * @type {Object[]}
 * @properties={typeid:35,uuid:"9F5AB683-103B-42F5-9A0B-89BD1619ADF1",variableType:-4}
 */
var planningToRevert = [];

/**
 * @type {JSRecord[]}
 * @properties={typeid:35,uuid:"90522F60-C6D5-4812-B80A-602BAAF54A29",variableType:-4}
 */
var planningChangeNewPrev = []

/**
 * @type {JSRecord[]}
 * @properties={typeid:35,uuid:"7A900E32-08CD-461E-84BB-1BF034E1B338",variableType:-4}
 */
var planningNewToDelete = [];

/**
 * @properties={typeid:35,uuid:"4F29311B-6131-48B2-973E-B8108B6A7AA1",variableType:-4}
 */
var isNewDetails = false;

/**
 * @properties={typeid:35,uuid:"63C5F992-B0BE-469C-B5D3-47864740FB68",variableType:-4}
 */
var isController = globals.hasRole('Controllers');

/**
 * @properties={typeid:35,uuid:"2A2F20DC-BA8E-4A5C-AC5C-7DAC27A7F7C3",variableType:-4}
 */
var isOrdersAdmin = globals.hasRole('Orders Admin');


/**
 * @type {Object}
 * @properties={typeid:35,uuid:"AF4145E5-1603-40E9-A8B2-03EEC27D4E8B",variableType:-4}
 */
var objDetType = {
	idDetail: 0,
	perDetail: 0.0,
	totalReturnDetail: 0.0,
	totReturnInserted: 0.0
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"F6CA86CF-26CE-4CF6-BB3E-9454C3232453"}
 */
function updateUI(event) {
	application.output(globals.messageLog + 'START jo_tab_plain.updateUI()', LOGGINGLEVEL.INFO);
	_super.updateUI(event);
	
//	var isController = globals.hasRole('Controllers');
//	var isOrdersAdmin = globals.hasRole('Orders Admin');

	var canModify = globals.isOwnerForObject(scopes.globals.currentUserId, globals.job_order_selected, 'jo') || globals.isPCManagerForObject(scopes.globals.currentUserId, globals.job_order_selected, 'jo') || isController || isOrdersAdmin
	var isEnabled = foundset.getSize() > 0 && foundset.jo_details_to_job_orders.getSize() > 0;
	var isEnabledJO = forms.jo_details.isEnabled;
	var isBillable = forms.jo_details.isBillable;

	//application.output('canModify ' + canModify + ' isEnabled num rec ' + isEnabled + ' isEnabledJO ' +isEnabledJO + ' isBillable ' + isBillable + ' isEditing() ' + isEditing())

	elements.buttonEdit.visible = canModify && isEnabled && isEnabledJO && !isEditing() && isBillable;
	elements.buttonCancel.visible = isEditing();
	elements.buttonSave.visible = isEditing();
	elements.buttonAdd.visible = canModify && isEnabledJO && !isEditing() && isBillable;
	elements.buttonDelete.visible = canModify && isEnabled && isEnabledJO && !isEditing() && isBillable;
	elements.buttonRecalculate.visible = canModify && isEnabled && isEnabledJO && !isEditing() && isBillable;

	if (isController || isOrdersAdmin) {
		elements.buttonRecalculate.visible = false;
		elements.button_singleplain.visible = false;
	}
	if (!isEditing())
		foundset.sort('jo_details_to_profit_cost_types.profit_cost_acr desc, jo_details_to_profit_cost_types.order_display asc, description asc');

	var rec = foundset.getSelectedRecord();
	//foundset.jo_details_to_jo_planning.loadRecords();
	if (rec != null && rec.total_amount != null && globals.roundNumberWithDecimal(rec.total_amount, 2) < globals.roundNumberWithDecimal(rec.jo_details_to_jo_planning.sum_total_amount, 2))
		elements.sumTotPlainAmount.fgcolor = 'red';
	else elements.sumTotPlainAmount.fgcolor = null;

	//label colorata sul mese actual
	var isActualDate = false;

	/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
	var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
	bo_actual_date.loadRecords();
	var monthActual = bo_actual_date.getRecord(1).actual_month;
	var yearActual = bo_actual_date.getRecord(1).actual_year;
	competenceMonth = bo_actual_date.getRecord(1).actual_date;

	for (var i = 1; i <= 12; i++) {
		//application.output(i+' selectedYear ' + scopes.globals.selectedYear +' anno attuale ' + globals.actualYear + ' mese acual ' +globals.actualMont );
		isActualDate = (yearActual == scopes.globals.selectedYear && monthActual == i)
		//application.output(isActualDate)
		elements['lab' + i].transparent = !isActualDate;
		elements['lab' + i].bgcolor = isActualDate ? '#ff6600' : '';
		elements['lab' + i].fgcolor = isActualDate ? '#ffffff' : '';
	}
	if (foundset.getSize() > 0) toggleTextFild();
	forms.jo_details.updateUI(event);
	application.output(globals.messageLog + 'STOP jo_tab_plain.updateUI()', LOGGINGLEVEL.INFO);
	//se nuova commessa di tipo interno o non porta soldi, devo far vedere all'utente i dati senza farglieli modificare
	//	if(foundset.jo_details_to_job_orders.is_billable == 0 || foundset.jo_details_to_job_orders.job_order_type == 0){
	//		elements.buttonRecalculate.visible = false;
	//		elements.buttonAdd.visible = false;
	//		elements.buttonDelete.visible = false;
	//		elements.button_singleplain.visible = false;
	//		elements.buttonSave.visible = false;
	//		elements.buttonEdit.visible = false;
	//		elements.buttonCancel.visible = false;
	//	}
}

/**
 * @properties={typeid:24,uuid:"57470475-B091-45BA-A85B-C1A793CD4721"}
 * @AllowToRunInFind
 */
function toggleTextFild() {
	var rec = foundset.getSelectedRecord();

	if (rec != null && rec.total_amount != null && globals.roundNumberWithDecimal(rec.total_amount, 2) < globals.roundNumberWithDecimal(rec.jo_details_to_jo_planning.sum_total_amount, 2))
		elements.sumTotPlainAmount.fgcolor = 'red';
	else elements.sumTotPlainAmount.fgcolor = '';
	//application.output('toggleTextFild jo_details_id ' + rec.jo_details_id);
	//	application.output('toggleTextFild costo/ricavo ' + rec.profit_cost_type_id);

	/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
	var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
	bo_actual_date.loadRecords();
	var monthActual = bo_actual_date.getRecord(1).actual_month;
	var yearActual = bo_actual_date.getRecord(1).actual_year;
	var isOpenActual = bo_actual_date.getRecord(1).is_open_to_actual;
	var statusMonthActual = bo_actual_date.getRecord(1).month_status_actual;
	var isAllineamento = (rec && (rec.profit_cost_type_id == 4 || rec.profit_cost_type_id == 11));

	//var isPerson = (rec.profit_cost_type_id == 5 || rec.profit_cost_type_id == 9);
	//application.output(rec.jo_details_id);
	for (var i = 1; i <= 12; i++) {
		//	application.output(i+' toggleTextFild selectedYear ' + scopes.globals.selectedYear + 'i ' + i+' anno actual ' + yearActual + ' mese acual ' +monthActual );

		var month = i;
		var dateElement = new Date(scopes.globals.selectedYear, month , 0);
//		application.output(dateElement);
		var relationName = 'jo_details_' + month + '_to_jo_planning';
		//	application.output(relationName)
		//application.output(rec['jo_'+month+'_to_jo_planning'] +' ' +rec['jo_'+month+'_to_jo_planning'].record_count);
		if (rec && rec[relationName] != null) {
			var actual = (foundset[relationName].is_actual == 1);
			var passed = false;
//			application.output('rec.jo_details_to_job_orders.job_orders_to_mgm_market_competence ' + rec.jo_details_to_job_orders.job_orders_to_mgm_market_competence.competence_month + ' '  +rec.jo_details_to_job_orders.job_orders_to_mgm_market_competence.actual_confirmation)
			
			if ((rec.jo_details_to_job_orders.job_orders_to_mgm_market_competence != null && dateElement == rec.jo_details_to_job_orders.job_orders_to_mgm_market_competence.competence_month && rec.jo_details_to_job_orders.job_orders_to_mgm_market_competence.actual_confirmation == 1) ||
				scopes.globals.selectedYear < yearActual || 
			   (scopes.globals.selectedYear == yearActual && (i < monthActual || (i == monthActual && isOpenActual == 0 && statusMonthActual == 3))))
				passed = true;
			//application.output('toggleTextFild status ' + foundset[relationName].status);
			var is_editable = (foundset[relationName].status == null || foundset[relationName].status == 0 || foundset[relationName].status == 6 || foundset[relationName].status == 7 || foundset[relationName].status == 9);
			var isDaily = (rec.profit_cost_type_id == 5 || rec.profit_cost_type_id == 9);

			var isInvoice = (rec.profit_cost_type_id == 12 || rec.profit_cost_type_id == 6)
				//			elements['l_am_' + month].bgcolor = actual ? '#f5f5f5' : '';
			//			elements['l_day_' + month].bgcolor = actual ? '#f5f5f5' : '';
			elements['l_am_' + month + 'a'].visible = true;
			elements['l_day_' + month + 'a'].visible = true;
			//			elements['l_am_' + month + 'e'].visible = false;
			//			elements['l_day_' + month + 'e'].visible = false;

			//elements['am_' + month].enabled = !actual && isEditing() && !isDaily;
			if (isController|| isOrdersAdmin) {
				elements['am_' + month].editable = isEditing() && !isDaily && !passed;
				elements['day_' + month].editable = (isEditing() && isDaily) && !passed;
				elements['am_' + month + 'a'].editable = (isEditing() && !isDaily) && !passed;
				elements['day_' + month + 'a'].editable = (isEditing() && isDaily) && !passed;
			} else {
				elements['am_' + month].editable = (!actual && !passed && isEditing() && !isDaily && is_editable && !isAllineamento && !isInvoice);
				elements['day_' + month].editable = (!actual && !passed && isEditing() && isDaily);
				elements['am_' + month + 'a'].editable = false;
				elements['day_' + month + 'a'].editable = false;

			}

			elements['am_' + month].bgcolor = (!is_editable || passed || isAllineamento || isInvoice) && isEditing() ? '#f5f5f5' : isEditing() ? (isDaily ? '' : '#ffffdd') : '';
			elements['day_' + month].bgcolor = (!is_editable || passed || isAllineamento || isInvoice) && isEditing() ? '#f5f5f5' : isEditing() ? (isDaily ? '#ffffdd' : '') : '';
			elements['am_' + month + 'a'].visible = actual;
			elements['day_' + month + 'a'].visible = actual;

		}
	}
}

/**
 * Handle record selected.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"46A74F23-03DD-4D6E-A656-643410231E5A"}
 */
function onRecordSelection(event) {
	//_super.onRecordSelection(event)
	//application.output('onRecordSelection isEditing: ' + isEditing());
	toggleTextFild();
	//var isController = globals.hasRole('Controllers');
	var isAllineamento = (foundset && (foundset.profit_cost_type_id == 4 || foundset.profit_cost_type_id == 11));
	var isInvoice = (foundset.profit_cost_type_id == 12 || foundset.profit_cost_type_id == 6)
	elements.buttonEdit.visible = (!isController && !isOrdersAdmin) ? (!isAllineamento && !isInvoice && !isEditing()) : !isEditing();
	elements.buttonDelete.visible = (!isController && !isOrdersAdmin) ? (!isAllineamento && !isInvoice && !isEditing()) : !isEditing();
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C2C1E5EB-3101-4E96-B20B-6E5D8AE950C9"}
 * @AllowToRunInFind
 */
function startEditing(event) {
	application.output(globals.messageLog + 'START jo_tab_plain.startEditing() ', LOGGINGLEVEL.INFO);
	//ciclare sul foundset planning di tutti i dettagli del jo
	//settare i campi tmp coi valori dei corrispondenti
	//salvare il foundset

	//	/** @type {JSFoundSet<db:/geco/jo_planning>} */
	//	var jo_planning = databaseManager.getFoundSet('geco', 'jo_planning');
	//	if (jo_planning.find()) {
	//		jo_planning.jo_id = foundset.jo_id;
	//		jo_planning.is_actual = 0;
	//		//TODO aggiungere il filtro sullo stato nel caso di actual inserito e confermato
	//		jo_planning.search();
	//	}
	//	application.output('startEditing record trovati non actual ' + jo_planning.getSize());
	//	for (var index = 1; index <= jo_planning.getSize(); index++) {
	//		var rec = jo_planning.getRecord(index);
	//		rec.tmp_days_number = rec.days_number;
	//		rec.tmp_total_amount = rec.total_amount;
	//	}
	//	databaseManager.saveData(jo_planning);

	var done = plugins.rawSQL.executeSQL("geco", "jo_planning", "call sp_update_jo_planning_tmp(?,?)", [foundset.jo_id, 1])
	if (done) {
		//flush is required when changes are made in db
		plugins.rawSQL.flushAllClientsCache("geco", "jo_planning")
	} else {
		var msg = plugins.rawSQL.getException().getMessage(); //see exception node for more info about the exception obj
		plugins.dialogs.showErrorDialog('Error', 'SQL exception: ' + msg, 'Ok')
	}
	//	var query = 'call sp_update_jo_planning_tmp(?,?)';
	//	var args = [foundset.jo_id,1];
	//	var dataset = databaseManager.getDataSetByQuery('geco', query, args, -1);
	//	query = 'select * from jo_planning where jo_id = ?';
	//	args = [foundset.jo_id];
	//	dataset = databaseManager.getDataSetByQuery('geco', query, args, -1);

	// inizio lo start editing
	application.output(globals.messageLog + 'STOP jo_tab_plain.startEditing() ', LOGGINGLEVEL.INFO);
	return _super.startEditing(event)
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"4351E68D-0D11-41C9-8FA4-A9BDB94AC821"}
 * @AllowToRunInFind
 */
function stopEditingPlanning(event) {
	application.output(globals.messageLog + 'START jo_tab_plain.stopEditingPlanning() ', LOGGINGLEVEL.INFO);
	var start = Date.now();
	_super.stopEditing(event);

	/** @type {JSFoundSet<db:/geco/jo_planning>} */
	var jo_planning = databaseManager.getFoundSet('geco', 'jo_planning');
	jo_planning.loadAllRecords();
	if (isNewDetails == false) {
		application.output(globals.messageLog + ' jo_tab_plain.stopEditingPlanning() da cancellare NUOVI ' + planningNewToDelete.length, LOGGINGLEVEL.DEBUG);
		//cancellazione record aggiunti
		for (var i = 0; i < planningNewToDelete.length; i++) {
			/** @type {JSRecord<db:/geco/jo_planning>} */
			var rec = planningNewToDelete[i];
			if (jo_planning.find()) {
				jo_planning.jo_planning_id = rec.jo_planning_id;
				jo_planning.search();
			}
			jo_planning.deleteRecord();
		}

		//planningChangeNewPrev
		//	if (jo_planning.find()) {
		//		jo_planning.jo_id = foundset.jo_id;
		//		jo_planning.is_actual = 0;
		//		jo_planning.search();
		//	}
		//	application.output('da modificare vecchi ' + jo_planning.getSize())
		//modifica record per tornare al valore iniziale dei dati
		//	for (var index = 1; index <= jo_planning.getSize(); index++) {
		//		var record = jo_planning.getRecord(index);
		//		record.days_number = record.tmp_days_number;
		//		record.total_amount = record.tmp_total_amount;
		//		if (record.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_acr == 'R')
		//			record.return_amount = record.total_amount;
		//		else record.cost_amount = record.total_amount;
		//		record.tmp_days_number = null;
		//		record.tmp_total_amount = null;
		//	}

		var done = plugins.rawSQL.executeSQL("geco", "jo_planning", "call sp_update_jo_planning_tmp(?,?)", [foundset.jo_id, 2])
		if (done) {
			//flush is required when changes are made in db
			plugins.rawSQL.flushAllClientsCache("geco", "jo_planning")
		} else {
			var msg = plugins.rawSQL.getException().getMessage(); //see exception node for more info about the exception obj
			plugins.dialogs.showErrorDialog('Error', 'SQL exception: ' + msg, 'Ok')
		}

		databaseManager.saveData(jo_planning);
		//svuoto gli array di appoggio
		planningChangePrev = [];
		planningNewToDelete = [];
		planningChangeNewPrev = [];
		updateUI(event);
		var stepTime = Date.now() - start;
		application.output(globals.messageLog + 'STOP jo_tab_plain.stopEditingPlanning() ' + stepTime + ' ms ', LOGGINGLEVEL.INFO);
	} else {
		application.output(globals.messageLog + 'bo_tab_plain.stopEditingPlanning() creazione nuovo dettaglio fine ', LOGGINGLEVEL.INFO);
		isNewDetails = false;
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"76141564-AB25-4C42-B6B3-15AA901719C1"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
	application.output(globals.messageLog + 'START jo_tab_plain.saveEdits() ', LOGGINGLEVEL.INFO);
	/** @type {JSFoundSet<db:/geco/jo_details>} */
	var jo_details = databaseManager.getFoundSet('geco', 'jo_details');
	if (jo_details.find()) {
		jo_details.jo_id = foundset.jo_id;
		jo_details.search();
	}
	var errorList = '';
	var recDetail = null;
	var recPlain = null;

	for (var idx = 1; idx <= jo_details.getSize(); idx++) {
		recDetail = jo_details.getRecord(idx);
		recPlain = recDetail.jo_details_to_jo_planning;
		var totalDet = globals.roundNumberWithDecimal(recDetail.total_amount, 2);
		var totPlainSum = globals.roundNumberWithDecimal(recPlain.sum_total_amount, 2);
		//application.output(recDetail.jo_details_id + ' totale dettaglio ' + totalDet + ' totale piani ' + totPlainSum);
		if (totalDet < totPlainSum) {
			errorList = errorList + '\n ' + recDetail.jo_details_to_profit_cost_types.profit_cost_description + ' ' + recDetail.description;
			//globals.DIALOGS.showErrorDialog("Attenzione!", "La somma degli importi inseriti per il dettaglio '"+ recDetail.description + "' supera il totale indicato", "OK");
			application.output(globals.messageLog + 'jo_tab_plain.saveEdits() ERROR Importi differenti ' + recDetail.description, LOGGINGLEVEL.ERROR);
			//return;
		}
	}

	if (errorList != '') {
		//globals.DIALOGS.showErrorDialog("Attenzione!", "La somma degli importi inseriti supera il totale indicato per i dettagli " + errorList, "OK");
	}

	var done = plugins.rawSQL.executeSQL("geco", "jo_planning", "call sp_update_jo_planning_tmp(?,?)", [foundset.jo_id, 3])
	if (done) {
		//flush is required when changes are made in db
		plugins.rawSQL.flushAllClientsCache("geco", "jo_planning")
	} else {
		var msg = plugins.rawSQL.getException().getMessage(); //see exception node for more info about the exception obj
		plugins.dialogs.showErrorDialog('Error', 'SQL exception: ' + msg, 'Ok')
	}

	//svuoto gli array di appoggio
	planningToRevert = [];
	planningChangePrev = [];
	planningNewToDelete = [];

	//COMMENTATA perchè cambia lo stato del mercato per qualunque mese venga inserito il dato non solo per il mese actual
	//Verificare dal dettaglio se è stata inserita una voce di tipo 1,2,7,8; se si, actual_confirmation per quel profit center passa a 0
//	if ([1, 2, 7, 8, 13].indexOf(foundset.profit_cost_type_id) > -1) {
//		/** @type {JSFoundSet<db:/geco/mgm_market_competence>} */
//		var mgmActualCompetence = databaseManager.getFoundSet('geco', 'mgm_market_competence');
//		/** @type {Number} */
//		var totMgmActualCompetence = 0;
//
//		if (mgmActualCompetence.find()) {
//			mgmActualCompetence.profit_center_id = foundset.jo_details_to_job_orders.profit_center_id;
//			mgmActualCompetence.competence_month = competenceMonth;
//			totMgmActualCompetence = mgmActualCompetence.search();
//		}
//
//		//lo trovo, verifico che actual_confirmation sia a 1; in tal caso lo setto a 0
//		if (totMgmActualCompetence != 0) {
//			var rec = mgmActualCompetence.getRecord(1);
//			if (rec.actual_confirmation == 1) {
//				rec.actual_confirmation = 0;
//			}
//		}
//	}

	_super.saveEdits(event);
	updateUI(event);
	isNewDetails = false;
	application.output(globals.messageLog + 'STOP jo_tab_plain.saveEdits() ', LOGGINGLEVEL.INFO);
}

/**
 * Handle changed data.
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"F573096E-2465-447C-A51A-810C5EA2D093"}
 */
function onDataChange(oldValue, newValue, event) {
	application.output(globals.messageLog + 'START jo_tab_plain.onDataChange() ', LOGGINGLEVEL.INFO);
	application.output(foundset.jo_details_id, LOGGINGLEVEL.INFO);
	application.output(foundset);
	var element = event.getElementName();
	/** @type {Number} */
	var month = element.substr(element.indexOf('_') + 1);
	var dateElement = new Date(scopes.globals.selectedYear, month - 1, 1);
	//	application.output('data elemento ' + dateElement)
	//	application.output('date JO ' + foundset.jo_details_to_job_orders.valid_from + ' ' + foundset.jo_details_to_job_orders.valid_to)

	var relationName = 'jo_details_' + month + '_to_jo_planning';
	//	application.output('piano ' + foundset[relationName].jo_planning_id);
	//	application.output('piano mese ' + foundset[relationName].jo_month);
	//	application.output('piano id dettaglio ' + foundset[relationName].jo_details_id);
	//	application.output('piano id JO ' + foundset[relationName].jo_id);
	//	application.output('costo/ricavo ' + foundset.jo_details_to_profit_cost_types.profit_cost_acr + ' id ' + foundset.profit_cost_type_id);

	//piano non compreso fra le date di inizio e fine indicate in JO
	var validFrom = foundset.jo_details_to_job_orders.valid_from;
	var validTo = foundset.jo_details_to_job_orders.valid_to;
	var answer = '';
	if (foundset.jo_details_to_job_orders.valid_from != null && foundset.jo_details_to_job_orders.valid_to != null) {
		if (foundset.jo_details_to_job_orders.valid_from > dateElement || dateElement > foundset.jo_details_to_job_orders.valid_to) {
			answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai inserendo una pianificazione fuori dal range di date indicato nella Commessa. Vuoi modificare le date?", "Si", "No");
			if (answer == 'Si') {
				application.output(globals.messageLog + 'jo_tab_plain.onDataChange() cambio date JO ', LOGGINGLEVEL.INFO);
				//modifica data della BO
				if (dateElement < foundset.jo_details_to_job_orders.valid_from)
					foundset.jo_details_to_job_orders.valid_from = dateElement
				if (dateElement > foundset.jo_details_to_job_orders.valid_to) {
					foundset.jo_details_to_job_orders.valid_to = new Date(scopes.globals.selectedYear, month, 0);
					//					application.output(foundset.jo_details_to_job_orders.valid_to);
				}
			} else {
				// UNDO editing
				application.undo();
				//				application.output(foundset[relationName])
				databaseManager.revertEditedRecords(foundset[relationName].getSelectedRecord());
				application.output(globals.messageLog + 'STOP jo_tab_plain.onDataChange() NO cambio date JO ', LOGGINGLEVEL.INFO);
				stopEditing(event);
				return;
			}
		}
	}else if(validFrom != null && dateElement < validFrom){
		if (foundset.jo_details_to_job_orders.valid_from > dateElement || dateElement > foundset.jo_details_to_job_orders.valid_to) {
			answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai inserendo una pianificazione fuori dal range di date indicato nella Commessa. Vuoi modificare le date?", "Si", "No");
			if (answer == 'Si') {
				application.output(globals.messageLog + 'jo_tab_plain.onDataChange() cambio date JO ', LOGGINGLEVEL.INFO);
				//modifica data della BO
				if (dateElement < foundset.jo_details_to_job_orders.valid_from)
					foundset.jo_details_to_job_orders.valid_from = dateElement;
			} else {
				// UNDO editing
				application.undo();
				//				application.output(foundset[relationName])
				databaseManager.revertEditedRecords(foundset[relationName].getSelectedRecord());
				application.output(globals.messageLog + 'STOP jo_tab_plain.onDataChange() NO cambio date JO ', LOGGINGLEVEL.INFO);
				stopEditing(event);
				return;
			}
		}
	}else if(validTo != null && dateElement > validTo){
		if (foundset.jo_details_to_job_orders.valid_from > dateElement || dateElement > foundset.jo_details_to_job_orders.valid_to) {
			answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai inserendo una pianificazione fuori dal range di date indicato nella Commessa. Vuoi modificare le date?", "Si", "No");
			if (answer == 'Si') {
				application.output(globals.messageLog + 'jo_tab_plain.onDataChange() cambio date JO ', LOGGINGLEVEL.INFO);
				//modifica data della BO
				if (dateElement < foundset.jo_details_to_job_orders.valid_from)
					foundset.jo_details_to_job_orders.valid_from = dateElement
				if (dateElement > foundset.jo_details_to_job_orders.valid_to) {
					foundset.jo_details_to_job_orders.valid_to = new Date(scopes.globals.selectedYear, month, 0);
					//					application.output(foundset.jo_details_to_job_orders.valid_to);
				}
			} else {
				// UNDO editing
				application.undo();
				//				application.output(foundset[relationName])
				databaseManager.revertEditedRecords(foundset[relationName].getSelectedRecord());
				application.output(globals.messageLog + 'STOP jo_tab_plain.onDataChange() NO cambio date JO ', LOGGINGLEVEL.INFO);
				stopEditing(event);
				return;
			}
		}
	}

	// ricavo
	if (foundset.jo_details_to_profit_cost_types.profit_cost_acr == 'R') {
		if (foundset.profit_cost_type_id != 12)
			foundset[relationName].return_amount = foundset[relationName].total_amount;
		else
			foundset[relationName].invoice_active = foundset[relationName].total_amount
		//application.output(foundset[relationName].total_amount + ' ' + foundset[relationName].return_amount);
		//updateUI(event);
	}
	// costo personale o TM
	else if (foundset.jo_details_to_profit_cost_types.profit_cost_acr == 'C') {
		//		application.output('trovato day ' + element.indexOf('day'))
		if (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 9 && element.indexOf('day') >= 0) {
			//cambia il numero di gg
			//application.output('######### ' + element.substr(0, element.indexOf('_')));
			var userCost = null;
			if (foundset.user_id!=null)
				userCost = globals.getLastUserCost(''+foundset.user_id,foundset.enrollment_number);
			else {
				userCost = {
					uid: null,
					cost: foundset.days_import,
					name: '',
					enrollNum: ''
				}
			}
				
//			if (userCost.cost != foundset.days_import) {
//				globals.DIALOGS.showErrorDialog("Attenzione!", "Il costo giornaliero è stato aggiornato.", "OK");
//			}
			foundset[relationName].days_import = userCost.cost;
			if (element.substr(0, element.indexOf('_')) == 'day') {
				//Math.round( (foundset[relationName].days_number * foundset.days_import) * Math.pow(10, 2)) / Math.pow(10, 2);
				foundset[relationName].total_amount = globals.roundNumberWithDecimal( (foundset[relationName].days_number * userCost.cost), 2);
			}
			//cambia amount ricalcolo il numero di gg
			else {
				Math.round( (foundset[relationName].total_amount / userCost.cost) * Math.pow(10, 1)) / Math.pow(10, 2);
				foundset[relationName].days_number = globals.roundNumberWithDecimal( (foundset[relationName].total_amount / userCost.cost), 2);
			}
			//updateUI(event);
		}
		foundset[relationName].user_id = (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 10) ? foundset.real_figure : ( (foundset.profit_cost_type_id == 9) ? foundset.real_tm_figure : null);
		if (foundset.profit_cost_type_id != 6)
			foundset[relationName].cost_amount = foundset[relationName].total_amount;
		else
			foundset[relationName].invoice_payable = foundset[relationName].total_amount
		//application.output(foundset[relationName].total_amount + ' ' + foundset[relationName].cost_amount)

	}
	/** @type {JSRecord<db:/geco/jo_planning>} */
	var rec = foundset[relationName].getSelectedRecord();
	//application.output('record modificato ' + rec.jo_planning_id)
	//inserisco il record nell'array dei record modificati
	if (planningChangePrev.indexOf(rec) < 0)
		planningChangePrev.push(rec);
	//	for (var y1 = 0; y1 < planningChangePrev.length; y1++) {
	//		/** @type {JSRecord<db:/geco/jo_planning>} */
	//		var r = planningChangePrev[y1];
	//		application.output(globals.messageLog + 'jo_tab_plain.onDataChange() record modificati ' + r.jo_planning_id, LOGGINGLEVEL.DEBUG);
	//	}
	if (rec.isNew()) {
		rec.jo_id = foundset.jo_id;
		rec.bo_details_id = foundset.bo_details_id;
		rec.bo_id = foundset.bo_id;
		rec.return_actual = null;
		rec.cost_actual = null;
		rec.total_actual = null;
		rec.user_id = (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 10) ? foundset.real_figure : ( (foundset.profit_cost_type_id == 9) ? foundset.real_tm_figure : null);
		planningChangeNewPrev.push(rec);
		//inserisco il record nell'array dei record da eliminare nel caso l'utente annulli le modifiche
		planningNewToDelete.push(rec);
	}
	databaseManager.saveData(rec);
	foundset.jo_details_to_jo_planning.loadRecords();
	//updateUI(event);
	application.output(globals.messageLog + 'STOP jo_tab_plain.onDataChange()', LOGGINGLEVEL.INFO);
}

/**
 * Ricalcola la pianificazione mensile in seguito a modifiche dell'utente sui singoli mesi
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"92712084-E43A-487A-BE8E-28820710F86D"}
 * @AllowToRunInFind
 */
function plainRecordDetails(event) {
	application.output(globals.messageLog + 'START jo_tab_plain.plainRecordDetails() ', LOGGINGLEVEL.INFO);
	//somma giorni e amount editati dall'utente
	var sumDaysEdited = 0;
	var sumAmountEdited = 0;
	//date di inizio e fine attività previste
	var endJO = foundset.jo_details_to_job_orders.valid_to;
	var startJo = foundset.jo_details_to_job_orders.valid_from;

	/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
	var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
	bo_actual_date.loadRecords();
	var dateActual = bo_actual_date.getRecord(1).actual_date;
	var monthActual = bo_actual_date.getRecord(1).actual_month;
	var yearActual = bo_actual_date.getRecord(1).actual_year;
	var isOpenActual = bo_actual_date.getRecord(1).is_open_to_actual;
	var statusActual = bo_actual_date.getRecord(1).month_status_actual;

	var startDate = null;
	if (startJo < dateActual) {
		
		if (isOpenActual == 1 || (isOpenActual == 0 && statusActual == 1) || foundset.jo_details_to_job_orders.job_orders_to_mgm_market_competence == null ||
				(foundset.jo_details_to_job_orders.job_orders_to_mgm_market_competence != null && 
				dateActual == foundset.jo_details_to_job_orders.job_orders_to_mgm_market_competence.competence_month && 
				foundset.jo_details_to_job_orders.job_orders_to_mgm_market_competence.actual_confirmation != 1))
			startDate = dateActual;
		else
			startDate = new Date(yearActual, monthActual, 1);
	} else startDate = startJo;
	application.output(globals.messageLog + 'jo_tab_plain.plainRecordDetails() nuova data per iniziare i clacoli ' + startDate, LOGGINGLEVEL.INFO);

	//if (startJO == null || endJO == null) {
	if (startDate == null || endJO == null) {
		globals.DIALOGS.showErrorDialog("Attenzione!", "Non è possibile calcolare la pianificazione mensile, non sono state indicate le date di inizio e fine attività", "OK");
		application.output(globals.messageLog + 'STOP jo_tab_plain.plainRecordDetails() date non indicate ', LOGGINGLEVEL.INFO);
		return;
	}

	//array di obj: dayWorkable=giorni lavoraboli, monthDet=mese yearDet=anno per il record details selected
	/** @type {Array} */
	var listObjTimeSplit = globals.countDayMonthsYears(startDate, endJO);
	var totWorkableDays = 0;
	//conto i mesi/gg lavorabili su cui spalmare costi e ricavi
	for (var x = 0; x < listObjTimeSplit.length; x++) {
		totWorkableDays = totWorkableDays + listObjTimeSplit[x].dayWorkable;
	}

	//somma dei gg lavorabili per la durata del JO
	var dayWork = totWorkableDays;

	//lista ID dei record modificati dall'utente
	/** @type {Number[]} */
	var idEditedList = [];
	//estrae le somme di giorni, costi e profitti modificati del record selezionato
	for (var i = 0; i < planningChangePrev.length; i++) {
		/** @type {JSRecord<db:/geco/jo_planning>} */
		var jo_planning_edited = planningChangePrev[i];
		application.output(globals.messageLog + 'jo_tab_plain.plainRecordDetails() profitCostType '  + jo_planning_edited.jo_planning_to_jo_details.profit_cost_type_id, LOGGINGLEVEL.DEBUG);

		// se il jo_plain è relativo al jo_details selezionato altrimenti non faccio nulla
		if (jo_planning_edited.jo_details_id == foundset.jo_details_id) {

			//se l'id del record non è presente nell'array degli idPlanning
			if (idEditedList.indexOf(jo_planning_edited.jo_planning_id) < 0) {
				//aggiungo id del record modificato
				idEditedList.push(jo_planning_edited.jo_planning_id);
				//somme dei gg/amount modificati dall'utente
				sumDaysEdited = sumDaysEdited + jo_planning_edited.days_number;
				sumAmountEdited = sumAmountEdited + jo_planning_edited.total_amount;
				//application.output('\nbo_planning_edited ' + i + ' - ' + jo_planning_edited);

				for (x = 0; x < listObjTimeSplit.length; x++) {
					if (listObjTimeSplit[x].yearDet == jo_planning_edited.jo_year && listObjTimeSplit[x].monthDet == jo_planning_edited.jo_month) {
						//sottraggo dai giorni totali lavorabili quelli del mese modificato dall'utente
						dayWork = dayWork - listObjTimeSplit[x].dayWorkable;
					}
				}
			}
		}
	}
	var daysActual = foundset.jo_details_to_jo_planning.sum_total_days_actual;
	var amountActual = foundset.jo_details_to_jo_planning.sum_total_amount_actual;
	//application.output('ID record Modificati ' + idEditedList);
	//i giorni/amount da dividere tolti quelli modificati e quelli già actual
	var dayToSplit = globals.roundNumberWithDecimal( (foundset.days_number - sumDaysEdited - daysActual), 2);
	var amountTosplit = globals.roundNumberWithDecimal( (foundset.total_amount - sumAmountEdited - amountActual), 2);
	//application.output('Importo da dividere ' + amountTosplit)

	if ( (sumAmountEdited + amountActual) > foundset.total_amount || (sumDaysEdited + daysActual) > foundset.days_number) {
		//ha inserito valori superiori ai valori dei dettagli
		var answer = globals.DIALOGS.showErrorDialog("Attenzione!", "La somma degli importi inseriti supera il totale indicato.\nIl piano verrà ricalcolato per mantenere il totale indicato.", "ANNULLA", "PROCEDI");

		if (answer != 'PROCEDI') {
			application.output(globals.messageLog + 'STOP jo_tab_plain.plainRecordDetails() ERROR importi inseriti superiori ', LOGGINGLEVEL.ERROR);
			return;
		}
		//return; NON Blocco perchè potrebbe essere una decisione del RDC
	}

	//application.output('amountTosplit ' + amountTosplit + ' dayToSplit ' + dayToSplit)
	var recToUpdate = [];

	//contatore per la lista di oggetti listObjTimeSplit
	var count = 0;
	if (idEditedList == null || idEditedList.length == 0) {
		// non ci sono piani modificati esco
		return
	}
	foundset.jo_details_to_jo_planning.loadRecords();
	var planningDetails = foundset.jo_details_to_jo_planning;
	application.output(globals.messageLog + 'jo_tab_plain.plainRecordDetails() record di pianificazione ' + planningDetails.getSize() + ' totale modificato ' + sumAmountEdited, LOGGINGLEVEL.DEBUG);
	//TODO VERIFICARE contare i mesi se ho un solo piano sbaglia!!!!!
	application.output('*** piani mensili ' + planningDetails.getSize() + ' editati ' + idEditedList.length + ' numero mesi da ricalcolare ' + listObjTimeSplit.length)
	if (planningDetails.getSize() == idEditedList.length && listObjTimeSplit.length == idEditedList.length) {
		// tutti modificati non ricalcolo nulla controllo i valori inseriti
		application.output(globals.messageLog + 'jo_tab_plain.plainRecordDetails() ' + foundset.total_amount + ' - ' + planningDetails.sum_total_amount, LOGGINGLEVEL.DEBUG);
		if (foundset.total_amount != sumAmountEdited) {
			globals.DIALOGS.showErrorDialog("Attenzione!", "La somma degli importi inseriti non corrisponde al totale indicato.", "OK");
			//return; NON Blocco perchè potrebbe essere una decisione del RDC
		}
	}

	var totAmInserted = sumAmountEdited;
	var totDaysIserted = sumDaysEdited;
	var lastRecordToUpdate = listObjTimeSplit.length - idEditedList.length;
	var indexRecordUpdating = 1;
	//ciclo sui mesi di durata della BO
//	application.output(startDate)
//	application.output(idEditedList)
	/** @type {globals.objUserCost} */
	var userCost =  {
		uid: null,
		cost: 0.0,
		name: '',
		enrollNum:''
	}
	if (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 9) {
		userCost = globals.getLastUserCost(''+foundset.user_id,foundset.enrollment_number)
	}
	while (startDate <= endJO) {
		application.output(globals.messageLog + 'jo_tab_plain.plainRecordDetails() tipo ' + foundset.profit_cost_type_id, LOGGINGLEVEL.DEBUG);
		//		application.output('indexRecordUpdating ' + indexRecordUpdating)
		var mese = startDate.getMonth() + 1;
		var anno = startDate.getFullYear();
		application.output(globals.messageLog + 'jo_tab_plain.plainRecordDetails() anno mese ' + anno + ' ' + mese, LOGGINGLEVEL.DEBUG);
		//		application.output('nuova data con lo 0 '  + new Date(startDate.getFullYear(),startDate.getMonth()+1,0));
		//		application.output('nuova data con lo 0 +2 mesi '  + new Date(startDate.getFullYear(),startDate.getMonth()+2,0));
		var tot = 0;
		//carico tutti i piani
		/** @type {JSFoundSet<db:/geco/jo_planning>} */
		var fdPlanning = foundset.jo_details_to_jo_planning;
		fdPlanning.loadAllRecords();
		//cerco il piano dato il mese e l'anno e il dettaglio selezionato se non è actual
		if (fdPlanning.find()) {
			fdPlanning.jo_details_id = foundset.jo_details_id;
			fdPlanning.jo_month = mese;
			fdPlanning.jo_year = anno;
			tot = fdPlanning.search();
		} else {
			application.output(globals.messageLog + 'ERRORE jo_tab_plain.plainRecordDetails() Non entra in find sulle pianificazioni ', LOGGINGLEVEL.ERROR);
		}
		//application.output('trovato ' + tot);
		//dovrebbe trovare una solo piano per dettaglio mese e anno
		if (fdPlanning.is_actual == 1) {
			count++;
			indexRecordUpdating++;
			application.output(globals.messageLog + 'Mese actual totale giorni inseriti ' + totDaysIserted + ' totale importo inserito ' + totAmInserted, LOGGINGLEVEL.DEBUG);
		} else if (fdPlanning.is_actual != 1) {
			/** @type {JSRecord<db:/geco/jo_planning>} */
			var recPlanning = null;
			var dayWorkableMonth = 0;
			var newDayMonth = 0;
			var newAmountMonth = 0;
			var objToUpdate = null;
			dayWorkableMonth = listObjTimeSplit[count].dayWorkable;

			newDayMonth = globals.roundNumberWithDecimal( ( (dayToSplit / dayWork) * dayWorkableMonth), 2);
			newAmountMonth = globals.roundNumberWithDecimal( ( (amountTosplit / dayWork) * dayWorkableMonth), 2);
			//application.output('\nreecord da modificare ' + indexRecordUpdating + ' ultimo recorrd da modificare ' + lastRecordToUpdate);

			//ultimo record da moficiare
			if (indexRecordUpdating == lastRecordToUpdate) {

				newDayMonth = globals.roundNumberWithDecimal( (foundset.days_number - totDaysIserted - daysActual), 2);
				newAmountMonth = globals.roundNumberWithDecimal( (foundset.total_amount - totAmInserted - amountActual), 2);
				application.output(globals.messageLog + 'jo_tab_plain.plainRecordDetails() Ultimo mese Resto ' + newDayMonth + ' ' + newAmountMonth, LOGGINGLEVEL.DEBUG);
			}

			if (tot == 1) {
				recPlanning = fdPlanning.getRecord(tot);
				application.output(globals.messageLog + 'jo_tab_plain.plainRecordDetails() recPlanning.days_import ' +recPlanning.days_import, LOGGINGLEVEL.DEBUG);
				//				application.output('record trovato  ' + recPlanning);
				//				application.output('edited contiene il record selezionato ' + idEditedList.indexOf(recPlanning.jo_planning_id));
				if (idEditedList.indexOf(recPlanning.jo_planning_id) < 0) {
//					application.output('non modificato ');
					objToUpdate = {
						record: recPlanning,
						valueDays: newDayMonth,
						valueAmount: newAmountMonth
					}
					recToUpdate.push(objToUpdate);
					application.output(globals.messageLog + 'jo_tab_plain.plainRecordDetails() NUOVI VALORI giorni ' + newDayMonth + ' IMPORTI ' + newAmountMonth, LOGGINGLEVEL.DEBUG);					

					totAmInserted = globals.roundNumberWithDecimal( (totAmInserted + newAmountMonth), 2);
					totDaysIserted = globals.roundNumberWithDecimal( (totDaysIserted + newDayMonth), 2);
					indexRecordUpdating++;
				}
			} else if (tot == 0) {
				/** @type {JSRecord<db:/geco/jo_planning>} */
				var newRec = null;
				var isNew = false;
				for (var n = 0; n < planningChangeNewPrev.length; n++) {
					newRec = planningChangeNewPrev[n];
					if (newRec.jo_details_id == foundset.jo_details_id && newRec.jo_id == foundset.jo_id && newRec.jo_month == mese && newRec.jo_year == anno) {
						//application.output('è già creato in memory');
						isNew = true;
						recPlanning = newRec;
						databaseManager.saveData(recPlanning);
						planningNewToDelete.push(recPlanning);
						break;
					}
				}
				if (!isNew) {
					// non è stato ancora creato in memoria lo creo adesso
					//application.output('NON creato in memory');
					var idx = fdPlanning.newRecord();
					fdPlanning.jo_details_id = foundset.jo_details_id;
					fdPlanning.jo_id = foundset.jo_id;
					fdPlanning.bo_id = foundset.bo_id;
					fdPlanning.bo_details_id = foundset.bo_details_id;
					fdPlanning.jo_month = mese;
					fdPlanning.jo_year = anno;
					fdPlanning.days_import = null;
					fdPlanning.days_number = (newDayMonth != 0) ? newDayMonth : null;

					if (foundset.jo_details_to_profit_cost_types.profit_cost_acr == 'R') {
						fdPlanning.total_amount = newAmountMonth;
						fdPlanning.return_amount = fdPlanning.total_amount
					} else {
						if (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 9) {
							//userCost = globals.getLastUserCost(''+foundset.user_id,foundset.enrollment_number);
							fdPlanning.days_import = userCost.cost;
							fdPlanning.total_amount = globals.roundNumberWithDecimal( (newDayMonth * fdPlanning.days_import), 2);
							fdPlanning.cost_amount = fdPlanning.total_amount
						} else {
							fdPlanning.total_amount = newAmountMonth;
							fdPlanning.cost_amount = fdPlanning.total_amount
						}
					}
					recPlanning = fdPlanning.getRecord(idx);

					//application.output('NUOVO ' + recPlanning.jo_planning_id + ' index ' + idx);

					dayWorkableMonth = listObjTimeSplit[count].dayWorkable;

					objToUpdate = {
						record: recPlanning,
						valueDays: newDayMonth,
						valueAmount: newAmountMonth
					}
					//application.output(objToUpdate.record)
					databaseManager.saveData(recPlanning);
					planningNewToDelete.push(recPlanning);
					recToUpdate.push(objToUpdate);

					totAmInserted = globals.roundNumberWithDecimal( (totAmInserted + newAmountMonth), 2);
					totDaysIserted = globals.roundNumberWithDecimal( (totDaysIserted + newDayMonth), 2);
					indexRecordUpdating++;
				}
				application.output(globals.messageLog + 'jo_tab_plain.plainRecordDetails() NUOVI VALORI giorni ' + newDayMonth + ' IMPORTI ' + newAmountMonth, LOGGINGLEVEL.DEBUG);
			}

			count++;
			application.output(globals.messageLog + 'jo_tab_plain.plainRecordDetails() totale gg inseriti ' + totDaysIserted + ' totale importo inserito ' + totAmInserted, LOGGINGLEVEL.DEBUG);
		}
		//FINE !ACTUAL
		startDate = new Date(startDate.getFullYear(), startDate.getMonth() + 2, 0);
		application.output(globals.messageLog + 'jo_tab_plain.plainRecordDetails() prossima data ' + startDate, LOGGINGLEVEL.DEBUG);
	}
	//fine while

	for (var el = 0; el < recToUpdate.length; el++) {

		var objUpdated = recToUpdate[el]
		/** @type {JSRecord<db:/geco/jo_planning>} */
		var rec = objUpdated.record;
		var newDay = objUpdated.valueDays;
		var newAmount = objUpdated.valueAmount;

		if (foundset.jo_details_to_profit_cost_types.profit_cost_acr == 'R') {
			rec.total_amount = newAmount;
			rec.return_amount = rec.total_amount
		} else if (foundset.jo_details_to_profit_cost_types.profit_cost_acr == 'C') {
			if (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 9) {
				rec.days_number = newDay;
				rec.days_import = userCost.cost;
				rec.total_amount = globals.roundNumberWithDecimal( (newDay * rec.days_import), 2);
				rec.cost_amount = rec.total_amount
			} else {
				rec.total_amount = newAmount;
				rec.cost_amount = rec.total_amount
			}
			rec.user_id = (foundset.profit_cost_type_id == 5 || foundset.profit_cost_type_id == 10) ? foundset.real_figure : ( (foundset.profit_cost_type_id == 9) ? foundset.real_tm_figure : null);
		}
		//application.output('dopo la modifica in memory rec : ' + rec);
	}
	foundset.jo_details_to_jo_planning.loadRecords();
	databaseManager.saveData(foundset.jo_details_to_jo_planning);
	//	application.output('dopo salva ' + foundset.jo_details_to_jo_planning);
	updateUI(event);
	application.output(globals.messageLog + 'STOP jo_tab_plain.plainRecordDetails()', LOGGINGLEVEL.INFO);
}

/**
 * @AllowToRunInFind
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"CE813DE1-9525-4D19-BACA-5A92E5BCD80D"}
 */
function calculateReturnByMol(event) {
	application.output(globals.messageLog + 'START jo_tab_plain.calculateReturnByMol()', LOGGINGLEVEL.INFO);

	//array di oggetti: {ID dettaglio, percentuale sul ricavo, totale ricavo dettaglio, totale ricavo inserito}
	/** @type {Object[]} */
	var objDetails = [];

	var totalCostDetails = foundset.total_cost_d;
	//se non ci sono record esce
	if (foundset.getSize() == 0)
		return;

	/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
	var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
	bo_actual_date.loadRecords();
	var dateActual = bo_actual_date.getRecord(1).actual_date;
	var monthActual = bo_actual_date.getRecord(1).actual_month;
	var yearActual = bo_actual_date.getRecord(1).actual_year;
	var isOpenActual = bo_actual_date.getRecord(1).is_open_to_actual;
	var statusActual = bo_actual_date.getRecord(1).month_status_actual;

	var startDate = foundset.jo_details_to_job_orders.valid_from;
	var endDate = foundset.jo_details_to_job_orders.valid_to;

	if (startDate == null || endDate == null) {
		globals.DIALOGS.showErrorDialog("Attenzione!", "Non è possibile ricalcolare la pianificazione mensile, non sono state indicate le possibili date di inizio e fine attività", "OK");
		return;
	}

	if (startDate != null) {
		if (startDate <= new Date(yearActual, monthActual - 1, 1)) {
			if (isOpenActual == 1 || (isOpenActual == 0 && statusActual == 1) || foundset.jo_details_to_job_orders.job_orders_to_mgm_market_competence == null ||
			(foundset.jo_details_to_job_orders.job_orders_to_mgm_market_competence != null && 
			dateActual == foundset.jo_details_to_job_orders.job_orders_to_mgm_market_competence.competence_month && 
			foundset.jo_details_to_job_orders.job_orders_to_mgm_market_competence.actual_confirmation != 1)) {
				startDate = new Date(yearActual, monthActual - 1, 1);
				application.output(globals.messageLog + 'jo_tab_plain.calculateReturnByMol() startDate: ' + startDate, LOGGINGLEVEL.DEBUG);
			} else {
				startDate = new Date(yearActual, monthActual, 1);
				application.output(globals.messageLog + 'jo_tab_plain.calculateReturnByMol() startDate: ' + startDate, LOGGINGLEVEL.DEBUG);
			}
		}
		application.output(globals.messageLog + 'jo_tab_plain.calculateReturnByMol() startDate: ' + startDate, LOGGINGLEVEL.DEBUG);

	}

	//entra in editing
	startEditing(event);
	//var startDate = foundset.jo_details_to_job_orders.valid_from;

	//trovo i record details di tipo ricavo nel foundset
	var totReturnDetails = 0;
	if (foundset.find()) {
		foundset.jo_details_to_profit_cost_types.profit_cost_acr = 'R';
		//escludo allineamento amministratico ricavi
		foundset.jo_details_to_profit_cost_types.profit_cost_types_id = '1||2||3';
		totReturnDetails = foundset.search();
	}
	application.output(globals.messageLog + 'jo_tab_plain.calculateReturnByMol() Numero Record ricavi ' + foundset.getSize(), LOGGINGLEVEL.DEBUG);
	// se non ci sono dettagli Ricavo
	if (totReturnDetails == 0) {
		globals.DIALOGS.showErrorDialog('Attenzione', 'Non ci sono dettagli di tipo Ricavo da ricalcolare', 'OK');
		foundset.loadRecords();
		return;
	} else {
		// cotruisco un oggetto con array di iD dettaglio e percentuali rispetto al ricavo totale
		for (var i = 1; i <= foundset.getSize(); i++) {
			var rec = foundset.getRecord(i);
			//application.output('Record' +rec);

			var per = globals.roundNumberWithDecimal( ( (100 * rec.return_amount) / foundset.total_return_d), 2);
			var objDet = {
				idDetail: rec.jo_details_id,
				perDetail: per,
				totalReturnDetail: rec.return_amount,
				totReturnInserted: rec.return_actual
			}
			objDetails.push(objDet);
			application.output(globals.messageLog + 'jo_tab_plain.calculateReturnByMol() ID Dettaglio ' + objDet.idDetail + ' percentuale di ricavo  ' + objDet.perDetail + ' ricavo totale ' + objDet.totalReturnDetail + ' totale inserito ' + objDet.totReturnInserted, LOGGINGLEVEL.DEBUG);
		}
	}

	while (startDate <= endDate) {
		application.output('--------\nData ' + startDate + ' Fine Data ' + endDate, LOGGINGLEVEL.DEBUG);
		var mese = startDate.getMonth() + 1;
		var anno = startDate.getFullYear();

		var lastMonth = new Date(anno, mese, 0);
		application.output('*** Ultimo mese ' + lastMonth + ' data actual ' + dateActual, LOGGINGLEVEL.DEBUG);
		//carico tutti i piani
		var tot = 0;
		/** @type {JSFoundSet<db:/geco/jo_planning>} */
		var fdPlanningCost = databaseManager.getFoundSet('geco', 'jo_planning');
		//cerco il piano dato il mese e l'anno e il dettaglio selezionato
		if (fdPlanningCost.find()) {
			fdPlanningCost.jo_id = foundset.jo_id
			fdPlanningCost.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_acr = 'C';
			fdPlanningCost.jo_month = mese;
			fdPlanningCost.jo_year = anno;
			fdPlanningCost.jo_planning_to_jo_details.profit_cost_type_id = '!6';
			//fdPlanningCost.is_actual = 0;
			tot = fdPlanningCost.search();
		} else {
			application.output('*** no find', LOGGINGLEVEL.DEBUG)
		}
		var totCost = fdPlanningCost.total_cost;
		application.output('*** trovato *' + tot + '* Costo totale *' + totCost + '*', LOGGINGLEVEL.DEBUG);
		//ricavo totale mensile

		var totProfitMonth = globals.roundNumberWithDecimal( ( (foundset.total_return_d * totCost) / totalCostDetails), 2);
		application.output('*** Nuovo ricavo totale mensile ' + totProfitMonth, LOGGINGLEVEL.DEBUG);

		var totMonthDetail = 0;
		//ciclo su tutti gli id di tipo ricavo
		for (var x = 0; x < objDetails.length; x++) {
			/** @type {objDetType} */
			var objDetRead = objDetails[x];
			application.output(objDetRead);
			/** @type {JSFoundSet<db:/geco/jo_planning>} */
			var fdPlanningReturn = databaseManager.getFoundSet('geco', 'jo_planning');
			if (fdPlanningReturn.find()) {
				fdPlanningReturn.jo_details_id = objDetRead.idDetail;
				fdPlanningReturn.jo_month = mese;
				fdPlanningReturn.jo_year = anno;
				//fdPlanningReturn.is_actual = 0;
				totMonthDetail = fdPlanningReturn.search();
			}

			var total = globals.roundNumberWithDecimal( ( (totProfitMonth * objDetRead.perDetail) / 100), 2);
			if (lastMonth == endDate) {
				//se ultimo mese della BO calcolo l'avanzo
				application.output('*** ULTIMO MESE', LOGGINGLEVEL.DEBUG)

				total = globals.roundNumberWithDecimal( (objDetRead.totalReturnDetail - objDetRead.totReturnInserted), 2);
				application.output('*** AVANZO ' + total, LOGGINGLEVEL.DEBUG)
			}

			application.output('*** totale da inserire ' + total, LOGGINGLEVEL.DEBUG)
			//se trovo il piano e non è actual e sono nel mese da attualizzare aperto
			if (totMonthDetail == 1 && fdPlanningReturn.is_actual != 1 && lastMonth == dateActual && (isOpenActual == 1 || (isOpenActual == 0 && statusActual == 1))) {
				//ricavoMensile1 = Rtot1*%diR1sulTotale/100
				//modifico i valori del profitto mensile in base alle percentuali del secondo array
				application.output('*** '+ objDetRead.idDetail + ' Trovo un piano mensile lo modifico: nuovo ricavo mensile del dettaglio ' + total, LOGGINGLEVEL.DEBUG);

				fdPlanningReturn.total_amount = total;
				fdPlanningReturn.return_amount = fdPlanningReturn.total_amount;

			} else if (totMonthDetail == 1 && fdPlanningReturn.is_actual == 1) {
				total = 0;
			} else if (totMonthDetail == 1 && fdPlanningReturn.is_actual != 1 && lastMonth > dateActual) {
				application.output('*** '+ objDetRead.idDetail + ' Trovo un piano mensile con data maggiore alla data actual, lo modifico: nuovo ricavo mensile del dettaglio ' + total, LOGGINGLEVEL.DEBUG);

				fdPlanningReturn.total_amount = total;
				fdPlanningReturn.return_amount = fdPlanningReturn.total_amount;
			}
			//se non trovo piani e la data è maggiore dell'ultima data actual
			else if (totMonthDetail == 0 && (lastMonth > dateActual || 
					(lastMonth == dateActual && (isOpenActual == 1 || (isOpenActual == 0 && statusActual == 1))))) {
				//non esiste la pianificazione del ricavo per il mese deve essere creato
				application.output('*** '+objDetRead.idDetail + ' NON Trovo un piano mensile lo creo per il dettaglio ' + total, LOGGINGLEVEL.DEBUG);
				var idx = fdPlanningReturn.newRecord();
				fdPlanningReturn.jo_id = foundset.jo_id;
				fdPlanningReturn.jo_details_id = objDetRead.idDetail;
				fdPlanningReturn.jo_month = mese;
				fdPlanningReturn.jo_year = anno;
				fdPlanningReturn.total_amount = total;
				fdPlanningReturn.return_amount = fdPlanningReturn.total_amount;

				var recPlanning = fdPlanningReturn.getRecord(idx);
				//salvo il nuovo record
				databaseManager.saveData(recPlanning);
				//lo inserisco nell'array dei record da cancellare nel caso in cui l'utente annulli l'operazione
				planningNewToDelete.push(recPlanning);
			} else {
				//deve esistere una sola pianificazione per dettaglio per mese.
				application.output(globals.messageLog + 'ERROR bo_tab_plain.calculateReturnByMol() Trovo più piani mensili', LOGGINGLEVEL.ERROR)
			}

			objDetRead.totReturnInserted = globals.roundNumberWithDecimal( (objDetRead.totReturnInserted + total), 2);

			application.output('*** '+x + ' INSERITO ' + objDetRead.totReturnInserted, LOGGINGLEVEL.DEBUG);
			databaseManager.saveData(fdPlanningReturn);
		}
		//mese successivo
		startDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, startDate.getDate());
		application.output('----- END WHILE')
	}

	foundset.loadRecords();
	foundset.jo_details_to_jo_planning.loadRecords();
	//salvo le modifiche alla pianificazione
	//databaseManager.saveData(foundset.jo_details_to_jo_planning);
	updateUI(event);
	foundset.sort('jo_details_to_profit_cost_types.profit_cost_acr desc, jo_details_to_profit_cost_types.order_display asc, description asc');
	application.output(globals.messageLog + 'STOP bo_tab_plain.calculateReturnByMol()', LOGGINGLEVEL.INFO);
}

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"C485BF7F-3DFF-469F-B299-F49397172D7E"}
 */
function onLoad(event) {
	foundset.sort('jo_details_to_profit_cost_types.profit_cost_acr desc, jo_details_to_profit_cost_types.order_display asc, description asc');
	updateUI(event);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"43CDE4B2-190C-4B9F-BBC0-A8A6D38C9C01"}
 */
function newRecord(event) {
	//globals.bo_selected = foundset.bo_id;
//	application.output('jo selezionato per il nuovo record ' + globals.job_order_selected);
	var win = application.createWindow("Nuovo dettaglio Commessa", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 750, 300);
	win.title = 'Nuovo dettaglio Commessa'
	win.show(forms.jo_new_detail_popup);
	isNewDetails = true;
	scopes.globals.callerForm = event.getFormName();
}

/**
 * @AllowToRunInFind
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @properties={typeid:24,uuid:"14EFCD8B-C8EC-438A-A590-D6FE3CBF35E8"}
 */
function deleteRecord(event, index) {
	application.output(globals.messageLog + 'START bo_tab_plain.deleteRecord()', LOGGINGLEVEL.INFO);	
	var record = foundset.getSelectedRecord();
	var listToDelete = [];
	if (!scopes.globals.hasRole('Controllers') && [4, 11, 12, 6].indexOf(record.profit_cost_type_id) > -1) {
		globals.DIALOGS.showErrorDialog('Errore', 'Non è possibile cancellare voci di tipo Allineamento amministrativo Ricavi/Costi e Fatture Attive/Passive', 'OK');
		return;
	}
	var answer = globals.DIALOGS.showWarningDialog("Conferma", "Proseguendo, il Forecast della voce selezionata sarà eliminato e i relativi valori non saranno recuperabili.", "Elimina", "Annulla");
	if (answer == "Elimina") {
		var tot = 0;

		//cerco i piani in stato non actual
		/** @type {JSFoundSet<db:/geco/jo_planning>} */
		//var jo_planning = databaseManager.getFoundSet('geco', 'jo_planning');
		var jo_planning = record.jo_details_to_jo_planning;
		jo_planning.loadRecords();
		if (jo_planning.find()) {
			jo_planning.jo_details_id = record.jo_details_id;
			jo_planning.is_actual = 0;
			tot = jo_planning.search();
			application.output(globals.messageLog + 'bo_tab_plain.deleteRecord() piani trovati ' + tot, LOGGINGLEVEL.DEBUG);	
		}
		for (var i = 1; i <= jo_planning.getSize(); i++) {
			var rec = jo_planning.getRecord(i);
//			application.output(rec);
			listToDelete.push(rec);
		}

		for (var t = 0; t < listToDelete.length; t++) {
			/** @type {JSRecord<db:/geco/bo_planning>} */
			var recToDel = listToDelete[t];
			jo_planning.deleteRecord(recToDel);
		}

		//cerco se ha mesi actual
		if (jo_planning.find()) {
			jo_planning.jo_details_id = record.jo_details_id;
			jo_planning.is_actual = 1;
			tot = jo_planning.search();
			application.output(globals.messageLog + 'bo_tab_plain.deleteRecord() piani trovati actual ' + tot, LOGGINGLEVEL.DEBUG);	
		}

		databaseManager.saveData(jo_planning);
		//se non ha mesi in stato actual elimino il dettaglio
		try {
			if (tot == 0) foundset.deleteRecord(record);
		} catch (e) {
			application.output(e);
		}
		foundset.loadRecords();
		updateUI(event);
	} else return;
	application.output(globals.messageLog + 'STOP bo_tab_plain.deleteRecord()', LOGGINGLEVEL.INFO);
}
