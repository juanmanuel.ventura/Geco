/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} form_name - form to open
 * @param {Boolean} is_details -  form for details
 * @param {String} type - foundset type (bo or jo)
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"84B7D951-6740-46E8-8E7D-50D81B17B27E"}
 */
function newRecord(event, form_name, is_details, type) {
	_super.newRecord(event);
	globals.lockForTab = false;
	globals.showDetailsList(event,form_name,is_details,type);
}

/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"C8EF8557-5ECE-4A3D-A717-61F28C8CDCB4"}
 */
function updateUI(event) {
	elements.buttonAdd.visible = !isEditing();
	elements.buttonDelete.visible = false;
}
