/**
 * @type {Number}
 * @properties={typeid:35,uuid:"FD94ACAF-04CA-46E6-978E-5109C8B01DF2",variableType:8}
 */
var descritpion = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"14C68A19-D4E4-4D70-A3CA-AC6A7D5EA86C"}
 */
var criterio = null;


/**
 * Handle hide window.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"E469CC81-640B-4E79-AAA6-644EF675A676"}
 */
function onHide(event) {
	clearFieldAndClose(event);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"612EF04C-61AC-4960-A7A1-B270CECD468E"}
 */
function saveEdits(event) {
	globals.callerForm['criterio'] = criterio;
	globals.callerForm['description'] = descritpion;
	clearFieldAndClose(event);
	
}

/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"287CE3A3-1C89-40AA-A2EB-41A8A42284F9"}
 */
function clearFieldAndClose(event) {
	descritpion = '';
	criterio = '';
	if (controller.getWindow())
		controller.getWindow().destroy();
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"E62A308A-F7E5-4694-A69A-C8DD4B2E0668"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
	elements.buttonEdit.visible = false;
}
