/**
 * @type {String}
 * @properties={typeid:35,uuid:"73D1576B-7923-4C73-B897-D24F8C2AA077"}
 */
var sortDirection = 'asc';

/**
 * @type {RuntimeLabel}
 * @properties={typeid:35,uuid:"A1CCF020-249B-45D0-843F-C4EB8CFA49A4",variableType:-4}
 */
var label;

/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"27343A13-CD95-495B-8A75-FEB62941A73A"}
 */
function sort(event) {
	// change color back to the original one
	if (label) label.fgcolor = '#727784';
	// (re) set the triggered label
	label = event.getSource();
	// (re) change color to indicate selection
	label.fgcolor = '#990000';
	var sortKey = label.text;

	switch (sortKey) {
	case 'Codice':
		controller.sort('bo_code' + ' ' + sortDirection);
		break;
	case 'Cliente':
		controller.sort('bo_to_companies.company_name' + ' ' + sortDirection);
		break;
	case 'Cliente Finale':
		controller.sort('bo_to_final_companies.company_name' + ' ' + sortDirection);
		break;
	case 'Mercato':
		controller.sort('bo_to_profit_centers.profit_center_name' + ' ' + sortDirection);
		break;
	case 'Descrizione':
		controller.sort('bo_description' + ' ' + sortDirection);
		break;
	case 'Stato':
		controller.sort('business_opportunities_to_bo_status.bo_status_description' + ' ' + sortDirection);
		break;
	case 'Data Creaz.':
		controller.sort('created_at' + ' ' + sortDirection);
		break;
	//	case 'AM':
	//		controller.sort('bo_to_account_manager.users_to_contacts.real_name' + ' ' + sortDirection);
	//		break;
	case 'Resp. Mercato':
		controller.sort('bo_to_client_manager.users_to_contacts.real_name' + ' ' + sortDirection);
		break;
	case '€ Offerta':
		controller.sort('offer_amount' + ' ' + sortDirection);
		break;
	case 'Integr. Offerta':
		controller.sort('extra_offer_amount' + ' ' + sortDirection);
		break;
	case '€ Extra Offerta':
		controller.sort('extra_offer_amount' + ' ' + sortDirection);
		break;
	case '€ Ordine':
		controller.sort('order_amount' + ' ' + sortDirection);
		break;
	//	case '€ Credito':
	//		controller.sort('credit_amount' + ' ' + sortDirection);
	//		break;
	case 'Commessa':
		controller.sort('bo_to_job_orders.external_code_navision' + ' ' + sortDirection);
		break;
	}

	// toggle the sort order
	sortDirection = (sortDirection == 'asc') ? 'desc' : 'asc';
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"FEF6BCB1-E13F-415E-BB21-5F7FCE5ADFDF"}
 * @AllowToRunInFind
 */
function goToIntermediate(event) {
	//application.output('filtro ' + globals.intermediate);
	var formFilter = forms.bo_sx.elements.split.getLeftForm();
	//application.output(formFilter);
	//application.output('PRIMA intermediate ' + globals.intermediate + ' filterIntemediate ' +globals.filterIntemediate);
	if (globals.intermediate && !globals.filterIntemediate && foundset.bo_to_job_orders && foundset.bo_to_job_orders.getSize() > 0 && foundset.bo_to_job_orders.job_orders_to_business_opportunities.countBO > 1) {
		formFilter['jobOrderId'] = foundset.job_order_id;
		formFilter['boID'] = null;
		formFilter['yearBo'] = null;
		formFilter['applyFilter']();

		globals.intermediate = false;
		globals.filterIntemediate = true;
		//application.output('DOPO intermediate ' + globals.intermediate + ' filterIntemediate ' +globals.filterIntemediate);
	} else {
		scopes.globals.showDetailsList(event, 'bo_details', true, 'bo');
		globals.intermediate = true;
	}
	//application.output('dopo filtro ' + globals.intermediate);
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"B3A8FEC4-B00C-45A0-9759-F7CF0F408680"}
 * @AllowToRunInFind
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
	//application.output(globals.hasRole('Resp. Mercato'));

	//Aggiunto tasto "+" per il Supporto Commerciale
	if (!globals.hasRole('Supp. Commerciale')) {
		elements.buttonAdd.visible = (!globals.hasRole('Resp. Mercato') || globals.hasRole('Vice Resp. Mercato') ? false : true);
	} else {
		elements.buttonAdd.visible = true;
	}
	elements.buttonAdd.visible = (globals.hasRole('Supp. Commerciale') || globals.hasRole('Resp. Mercato')) ? true:false;
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"E17DB0F8-7316-4C69-95FE-0DDB1CABFE6A"}
 */
function onLoad(event) {
	//application.output(globals.hasRole('Resp. Mercato'));

	//Aggiunto tasto "+" per il Supporto Commerciale
	if (!globals.hasRole('Supp. Commerciale')) {
		elements.buttonAdd.visible = (!globals.hasRole('Resp. Mercato') || globals.hasRole('Vice Resp. Mercato') ? false : true);
	} else {
		elements.buttonAdd.visible = true;
	}
	elements.buttonAdd.visible = (globals.hasRole('Supp. Commerciale') || globals.hasRole('Resp. Mercato')) ? true:false;
}

/**
 * @param event
 *
 * @properties={typeid:24,uuid:"09C6DF19-D627-4B6B-80EB-EF42FD58473A"}
 */
function updateUI(event) {
	_super.updateUI(event);
	elements.buttonAdd.visible = (globals.hasRole('Supp. Commerciale') || globals.hasRole('Resp. Mercato')) ? true:false;
//	if (scopes.globals.hasRole('Supp. Commerciale')) {
//		elements.idc.visible = false;
//		elements.jobOrder.visible = false;
//		elements.label_accountccccc.visible = false;
//		elements.label_jobOrder.visible = false;
//		elements.linkJoImg.visible = false;
//		elements.tick.visible = false;
//		elements.reqJoImg.visible = false;
//	}
}
