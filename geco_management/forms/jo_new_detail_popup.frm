dataSource:"db:/geco/jo_details",
extendsID:"1FA35CAE-47C9-4025-BE35-E8E45A6A061F",
items:[
{
customProperties:"design:{
elementType:\"'filed'\"
}",
dataProviderID:"profit_cost_type_id",
displayType:2,
editable:false,
formIndex:14,
horizontalAlignment:2,
location:"125,10",
name:"ProfitCostDescription",
onDataChangeMethodID:"1A59519A-EA99-4624-B812-CC5B894F7E84",
size:"249,20",
typeid:4,
uuid:"096CA137-EF94-4612-833E-0F9F26470733",
valuelistID:"C9D69D44-204D-4348-841B-4BB19B92875D"
},
{
customProperties:"design:{
elementType:\"'filed'\"
}",
dataProviderID:"standard_import",
displaysTags:true,
formIndex:8,
groupID:"groupGeneric",
horizontalAlignment:2,
location:"467,40",
name:"standAmount",
onDataChangeMethodID:"-1",
selectOnEnter:true,
size:"123,20",
typeid:4,
uuid:"1EBD71E8-FC45-40C9-B9B9-4C7296A742D2"
},
{
extendsID:"4EF01A0C-8886-4BD4-8494-4826D6F4D39C",
formIndex:1,
location:"677,129",
onActionMethodID:"B434A568-298E-457F-9B3A-96331F0571FE",
typeid:7,
uuid:"28A80CBE-0B27-4E87-9ED8-51553EB5221A"
},
{
dataProviderID:"total_amount",
formIndex:3,
format:"#0.00",
location:"467,10",
margin:"0,5,0,0",
name:"totalAmountR",
onDataChangeMethodID:"-1",
printable:false,
size:"75,20",
typeid:4,
uuid:"2DB74637-25DD-443F-BD3D-1429C31F67BF"
},
{
formIndex:9,
groupID:"groupRealTM",
horizontalAlignment:4,
location:"10,66",
size:"105,20",
text:"Figura Reale TM",
transparent:true,
typeid:7,
uuid:"30BEDACB-3B3E-4F1B-AEA5-09E160C72A15"
},
{
formIndex:12,
groupID:"groupGeneric",
horizontalAlignment:4,
location:"10,40",
size:"105,20",
text:"Figura generica",
transparent:true,
typeid:7,
uuid:"40D0B773-E458-41D2-B36D-D64044715BA3"
},
{
customProperties:"design:{
elementType:\"'filed'\"
}",
dataProviderID:"profit_cost_type_id",
displayType:2,
editable:false,
formIndex:14,
horizontalAlignment:2,
location:"125,10",
name:"ProfitCostDescriptionController",
onDataChangeMethodID:"1A59519A-EA99-4624-B812-CC5B894F7E84",
size:"249,20",
typeid:4,
uuid:"412FC500-40C7-453F-8E50-EA819B03AEF3",
valuelistID:"80728F31-DF87-427B-B5D0-9082A0A779E6",
visible:false
},
{
dataProviderID:"real_tk_supplier",
displayType:10,
formIndex:3,
groupID:"groupRealTK",
location:"125,40",
margin:"0,5,0,0",
name:"realTK",
onDataChangeMethodID:"-1",
size:"249,20",
typeid:4,
uuid:"4CD73E41-CF6F-4B24-BAC3-093CEC377D08",
valuelistID:"4A29FDF3-F326-4BCD-A9AC-C670BE3EB03B"
},
{
formIndex:19,
horizontalAlignment:4,
location:"10,10",
size:"105,20",
text:"Tipologia",
transparent:true,
typeid:7,
uuid:"5F6D2C81-9426-44C8-B570-FD50617E4C89"
},
{
customProperties:"design:{
elementType:\"'filed'\"
}",
dataProviderID:"standard_figure",
displayType:10,
displaysTags:true,
formIndex:8,
groupID:"groupGeneric",
horizontalAlignment:2,
location:"125,40",
name:"standFigure",
onDataChangeMethodID:"C8BC028C-71BA-4BA9-B2D4-5E4E7B405ACF",
selectOnEnter:true,
size:"249,20",
typeid:4,
uuid:"60D53C33-0F56-44E5-8748-2244E5B132F1",
valuelistID:"980676F8-4907-48DF-8B93-BC59EC44EB5A"
},
{
extendsID:"07AB7FEF-0560-4B82-B56C-D95051E8AD67",
location:"636,129",
onActionMethodID:"7D24F35F-3EF1-464D-B825-7A8B8B8DA469",
typeid:7,
uuid:"83F95360-0177-410A-AFD4-40A5C6E06FAF"
},
{
extendsID:"DA9E3F87-A868-4257-AFFB-2214FB026E9A",
formIndex:0,
location:"677,129",
typeid:7,
uuid:"87F70C18-4F88-4B7E-A6AE-D079825694D6",
visible:false
},
{
dataProviderID:"days_number",
formIndex:3,
groupID:"groupDaysImport",
location:"647,66",
margin:"0,5,0,0",
name:"gg",
onDataChangeMethodID:"47C1981A-2FF7-4B64-8985-3743D60D1FC2",
size:"55,20",
typeid:4,
uuid:"92ADD12D-62F5-4658-A3BB-D0F2F6FF95CD"
},
{
formIndex:13,
horizontalAlignment:4,
location:"386,10",
name:"l_total",
size:"71,20",
text:"Totale",
transparent:true,
typeid:7,
uuid:"9BC6F595-AA04-467D-A5D9-33EC3593F3EC"
},
{
formIndex:12,
groupID:"groupDaysImport",
horizontalAlignment:4,
location:"605,66",
size:"35,20",
text:"gg",
transparent:true,
typeid:7,
uuid:"CE72ED17-B886-4FBA-9B6F-D462C487F455"
},
{
customProperties:"design:{
elementType:\"'filed'\"
}",
dataProviderID:"user_id_er",
displayType:10,
displaysTags:true,
formIndex:7,
groupID:"groupRealP",
horizontalAlignment:2,
location:"125,66",
name:"realP",
onDataChangeMethodID:"C4F89378-C989-4E97-849B-3AACBD84C799",
printable:false,
size:"249,20",
typeid:4,
uuid:"CEFD4BF3-E53B-447D-823B-5638457F9AF6",
valuelistID:"69391181-D98C-48DE-A9CE-786D46C23C85"
},
{
extendsID:"6F614343-45A5-4185-BAD8-A96D7B66D21E",
height:166,
typeid:19,
uuid:"D34CBF97-CE86-482A-8532-8DC0F8F0E6FB"
},
{
dataProviderID:"user_id_er",
displayType:10,
formIndex:3,
groupID:"groupRealTM",
location:"125,66",
margin:"0,5,0,0",
name:"realTM",
onDataChangeMethodID:"C4F89378-C989-4E97-849B-3AACBD84C799",
size:"249,20",
typeid:4,
uuid:"D4D3E1F8-7508-4D5D-8FC5-817DA42FCCC3",
valuelistID:"02E61854-DB7B-4FA7-93D8-CF0EDE1FF19F"
},
{
extendsID:"2FD9ADCF-E59B-4856-B8CB-C26AFF795744",
location:"0,118",
size:"718,2",
typeid:7,
uuid:"DDDD428A-B908-44DF-88DE-94B9F522A35B"
},
{
formIndex:12,
groupID:"groupGeneric",
horizontalAlignment:4,
location:"386,40",
name:"label_st_cost",
size:"71,20",
text:"Importo",
transparent:true,
typeid:7,
uuid:"E5C36CAF-5FAF-46F8-8434-10C18EDFFE6B"
},
{
customProperties:"design:{
elementType:\"'filed'\"
}",
dataProviderID:"days_import",
displaysTags:true,
editable:false,
formIndex:8,
groupID:"groupDaysImport",
horizontalAlignment:2,
location:"467,66",
name:"realAmountP",
onDataChangeMethodID:"-1",
selectOnEnter:true,
size:"123,20",
typeid:4,
uuid:"E736D1FA-A363-43EE-A3B4-BAD9907C2656"
},
{
formIndex:12,
groupID:"groupDaysImport",
horizontalAlignment:4,
location:"386,66",
size:"71,20",
text:"Importo",
transparent:true,
typeid:7,
uuid:"F049789F-9EA7-461E-BB37-39FE078A3B31"
},
{
formIndex:9,
groupID:"groupRealP",
horizontalAlignment:4,
location:"10,66",
size:"105,20",
text:"Figura Reale",
transparent:true,
typeid:7,
uuid:"F63B0F52-5E87-46EB-9768-A163898DD542"
},
{
formIndex:9,
groupID:"groupRealTK",
horizontalAlignment:4,
location:"10,40",
size:"105,20",
text:"Fornitore TK",
transparent:true,
typeid:7,
uuid:"FB0244A3-D15F-4877-B71E-218C056B6616"
},
{
extendsID:"3DDC34AE-9103-4FD9-A661-254F61AAC65E",
height:118,
typeid:19,
uuid:"FF23D37B-8CE9-4421-80F7-CC441D373037"
}
],
name:"jo_new_detail_popup",
onHideMethodID:"B434A568-298E-457F-9B3A-96331F0571FE",
onLoadMethodID:"-1",
showInMenu:true,
size:"718,218",
styleName:"GeCo",
typeid:3,
uuid:"DD553B47-2817-46FF-8D20-1AB86D5337C5"