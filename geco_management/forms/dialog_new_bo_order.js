/**
 * @type {String}
 * @properties={typeid:35,uuid:"08F0EA74-8F06-4CF0-8BEE-90493F0ED1B5"}
 */
var orderNumber = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"27514F79-FB2E-490D-AD7D-46AA6E5945C8"}
 */
var orderDescr = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"720AEFE7-419F-4F27-9813-85A81113B175",variableType:8}
 */
var orderAmount = null;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"5423AA59-0874-45B7-81B4-300FC38917E5",variableType:93}
 */
var orderDate = new Date();

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"2D04B38A-F89D-4118-AA09-902662759B00"}
 */
function clearNewRecord(event) {
	clearField(event)
	_super.stopEditing(event);
	if (controller.getWindow())
		controller.getWindow().destroy();
}

/**
 * Callback method for when form is shown.
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"A10DE063-52E1-4B91-81A0-EB5A8EA92582"}
 */
function onShow(firstShow, event) {
	_super.startEditing(event);
}

/**
 * Handle hide window.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"01A08BC1-44D8-4205-84E9-66A00FD9C5D2"}
 */
function onHide(event) {
	clearField(event);
	if (isEditing()) 
		_super.stopEditing(event);
	//databaseManager.setAutoSave(true);
	if (controller.getWindow())
		controller.getWindow().destroy();
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"3FAB8782-52E9-4754-877D-7BA009D3983E"}
 */
function saveEditsOrder(event) {
	foundset.newRecord();
	foundset.bo_id = globals.bo_selected;
	foundset.order_number = orderNumber;
	foundset.description_order = orderDescr;
	foundset.order_amount = orderAmount;
	foundset.order_date = orderDate;
	foundset.bo_orders_to_business_opportunities.action = 'insert/update order';
//	application.output('nuovo record '+foundset);	
	if (_super.saveEdits(event)) {
		clearField(event)
		if (controller.getWindow())
			controller.getWindow().destroy();
		forms.bo_orders.updateUI(event);
	}//fondamentale perchè se dà errore una prima volta e dopo si modificano i campi, il record non si aggiorna e quindi darebbe errore finchè
	//non si annulla e si ricrea un ordine da zero
	else {
		databaseManager.revertEditedRecords();
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"9944353C-B809-4079-926A-035C14F35DE9"}
 */
function clearField(event) {
	orderNumber = null;
	orderDate = null;
	orderDescr = null;
	orderAmount = null;
}
