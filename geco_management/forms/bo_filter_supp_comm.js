/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"4623D5DD-497F-41DA-B721-3C3295F49716",variableType:-4}
 */
var pcList = globals.getProfitCenterByUserManager(scopes.globals.currentUserId);

/**
 * @type {String}
 * @properties={typeid:35,uuid:"0C8A2699-7DBE-484F-9009-B2DF5306AF77"}
 */
var pcToSearch = (pcList.length != 0) ? pcList.join('||') : null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"FB0330C1-35C4-443B-A684-B5679D7797A5",variableType:8}
 */
var customer = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"55306CDB-233D-4629-B38A-ABDA535C6CA5",variableType:8}
 */
var finalCustomer = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"7C7EB32D-4748-404E-97F4-7E15896F1020",variableType:8}
 */
var customerSuppComm = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"25448C18-F5C6-44D0-B0CC-CF042A2A8862"}
 */
var rif_customer = null;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"123FE001-49F7-4FC2-9D27-69FA3DCC26BE",variableType:93}
 */
var dateFrom = null;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"BB28E960-B11F-475D-85FC-DE0BB20EFA12",variableType:93}
 */
var dateTo = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"43033099-F1A8-46D0-AAD7-2162AA36DFE1",variableType:8}
 */
var statusBo = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"41054545-0B9A-4474-8D25-45F230A74498",variableType:8}
 */
var yearBo = scopes.globals.selectedYear;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"75536F2F-C354-4129-80F8-5C8F7518B142",variableType:8}
 */
var jobOrderId = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"A0204B7C-E9E1-49F9-AF17-27ACAF416DDB",variableType:8}
 */
var profitCenterId = (pcList.length != 0) ? pcList[0] : null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"CC34803E-1694-4227-8A63-BA2477AFB8EE",variableType:8}
 */
var prob = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"4F3492A7-BA2F-4B04-B08B-8B1236D9F34F",variableType:8}
 */
var docType = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"428DDB20-E0AA-4270-B242-95697C5C5C58",variableType:8}
 */
var orderType = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"4C07C5C9-5482-4BBC-A551-EB2E52598EEB",variableType:8}
 */
var boID = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"202B8B0F-4F86-4884-8065-1DDCD0F13FEE",variableType:8}
 */
var pcOwnerID = null;

/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"2A7456F2-1118-4467-A968-34C207988E98",variableType:-4}
 */
var listCustomers = [];

/**
 * @type {String}
 * @properties={typeid:35,uuid:"9AF18AD4-C68E-4035-AB66-9E777883DBF7"}
 */
var customerToSearch = null;

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @properties={typeid:24,uuid:"C258E524-294E-4A96-8666-25200E254C3A"}
 */
function onDataChange(oldValue, newValue, event) {
	globals.filterIntemediate = false;
	applyFilter(event);
	updateUI(event);
	return true
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"10719A83-E4C4-4F2A-9B82-DE963779E7BA"}
 */
function applyFilter(event) {
	if (foundset.find()) {
		foundset.pc_owner_id = pcOwnerID;
		foundset.profit_center_id = profitCenterId ? profitCenterId : pcToSearch;
		listCustomers = [];
		listCustomers = scopes.globals.getUserClientsForPCSelected(scopes.globals.currentUserId, profitCenterId);
		customerToSearch = (listCustomers.length != 0) ? listCustomers.join('||') : -1;
		application.output('listCustomers[]: ' + listCustomers.length + '; ' + listCustomers);
		//controllo su cliente iniziale
		if (customer != null) {
			if (scopes.globals.isClientPCSuppComm(customer, profitCenterId))
				foundset.company_id = customer;
			else foundset.company_id = -1;
		}//cliente non selezionato, lo cerco nella lista dei suoi clienti
		else foundset.company_id = customerToSearch;

		//controllo su cliente finale
		if (finalCustomer != null) {
			if (scopes.globals.isClientPCSuppComm(finalCustomer, profitCenterId))
				foundset.final_company_id = finalCustomer;
			else foundset.final_company_id = -1;
		}//cliente non selezionato, lo cerco nella lista dei suoi clienti
		else foundset.final_company_id = customerToSearch;

		if (dateFrom != null && dateTo != null) //dateFrom + '...' + dateTo+'|yyyy-MM-dd';
			foundset.created_at = '#' + utils.dateFormat(dateFrom, "yyyy-MM-dd") + "..." + utils.dateFormat(dateTo, "yyyy-MM-dd") + "|yyyy-MM-dd";
		foundset.bo_year = yearBo;
		foundset.status = statusBo;
		foundset.customer_person = rif_customer;
		foundset.job_order_id = jobOrderId;
		foundset.bo_id = boID;

		switch (docType) {
		case 1: // tutti
			foundset.business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.bo_document_type = '1||2||3||4';
			break;
		case 2: // offerta
			foundset.business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.bo_document_type = 2;
			break;
		case 3: // ordine
			foundset.business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.bo_document_type = 3;
			break;
		case 4:// altro
			foundset.business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.bo_document_type = '1||3';
			break;
		case 5://5 nessuna offerta
			foundset.business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.bo_document_type = '!' + 2;
			break;
		case 6://6 nessuno
			foundset.business_opportunities_to_bo_bo_documents.totalDoc = 0;
			break;
		default:
			break;
		}

		foundset.bo_probability = prob;

		switch (orderType) {
		case 1: // Con ordini|1.0
			foundset.bo_to_bo_orders.totalOrders = '!' + 0;
			break;
		case 2: //Senza ordini|2.0
			foundset.bo_to_bo_orders.totalOrders = 0;
			break;
		default:
			break;
		}
		foundset.search();
	}
	var totalRecord = databaseManager.getFoundSetCount(foundset);
	controller.setSelectedIndex(totalRecord);
	application.output(globals.messageLog + 'SC bo_filter.applyFilter() ultimo record: ' + controller.getSelectedIndex(), LOGGINGLEVEL.DEBUG);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"6F56570C-DB1D-459D-9D88-830770C8A474"}
 */
function resetFilter(event) {
	globals.intermediate = true;
	globals.filterIntemediate = false;
	pcOwnerID = null;
	dateFrom = null;
	dateTo = null;
	statusBo = null;
	rif_customer = null;
	jobOrderId = null;
	profitCenterId = (pcList.length != 0) ? pcList[0] : null;
	scopes.globals.selectedPCCommSupp = pcList[0];
	prob = null;
	yearBo = scopes.globals.selectedYear;
	docType = null;
	boID = null;
	orderType = null;
	customer = null;
	finalCustomer = null;
	applyFilter(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"B41C41A1-46D6-4F5A-9B76-7AFDF9C4A9A1"}
 */
function onLoad(event) {
	foundset.removeFoundSetFilterParam('enabled');
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @properties={typeid:24,uuid:"4A27CBC8-E400-4283-BEC7-D338C2980BA8"}
 */
function onDataChangePC(oldValue, newValue, event) {
	globals.filterIntemediate = false;
	scopes.globals.selectedPCCommSupp = newValue;
	applyFilter(event);
	_super.updateUI(event);
	return true
}

/**
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"915F4891-D1C8-42C5-A9DD-DC1469D5C3C0"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
	scopes.globals.selectedPCCommSupp = pcList.length > 0 ? pcList[0] : null;
	applyFilter(event);
}
