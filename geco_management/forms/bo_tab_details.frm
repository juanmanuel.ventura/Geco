borderType:"SpecialMatteBorder,0.0,0.0,0.0,0.0,#000000,#000000,#000000,#cccccc,0.0,",
dataSource:"db:/geco/business_opportunities",
extendsID:"1FA35CAE-47C9-4025-BE35-E8E45A6A061F",
items:[
{
anchors:9,
groupID:"g_notEditable",
location:"16,89",
name:"l_rifCliente",
showFocus:false,
size:"106,20",
text:"Rif. Cliente",
transparent:true,
typeid:7,
uuid:"00011E20-9610-4B88-A9B9-3AEE488BEAB4"
},
{
dataProviderID:"required_jo_date",
displayType:5,
editable:false,
enabled:false,
formIndex:3,
format:"dd-MM-yyyy|dd-MM-yyyy",
location:"550,399",
name:"reqiredJODate",
size:"120,20",
styleClass:"date",
transparent:true,
typeid:4,
uuid:"024E16C8-78E1-45B5-8BBB-0ABF62C334C5"
},
{
anchors:3,
dataProviderID:"note",
displayType:1,
formIndex:4,
location:"767,14",
size:"511,53",
typeid:4,
uuid:"025C4927-C446-4886-9B88-C9A2183B9925"
},
{
anchors:9,
formIndex:1,
groupID:"g_notEditable",
location:"16,154",
name:"l_respMercato",
showFocus:false,
size:"106,20",
text:"Resp. Mercato",
transparent:true,
typeid:7,
uuid:"0479EBA7-FF24-401D-99D4-8A7348B05F7C"
},
{
groupID:"g_notEditable",
location:"360,271",
name:"l_stato",
showFocus:false,
size:"106,20",
text:"Stato",
transparent:true,
typeid:7,
uuid:"0530E353-8A5B-4DAC-8ECF-78EABFC09BA7"
},
{
dataProviderID:"geographical_area",
displayType:10,
groupID:"g_notEditable",
location:"470,129",
name:"area",
size:"200,20",
typeid:4,
uuid:"0F5904C8-5531-4F6D-9F17-2C9ECF5C2246",
valuelistID:"C9D70F9D-3E4C-4216-AA0B-4FFED3E60929"
},
{
formIndex:10008,
groupID:"g_notEditable",
location:"7,64",
name:"lbl1",
size:"9,20",
text:"*",
transparent:true,
typeid:7,
uuid:"1381759A-968B-4DAE-BDA3-26B69BBF0DC8",
visible:false
},
{
dataProviderID:"bo_to_job_orders.job_order_id",
displayType:10,
editable:false,
enabled:false,
location:"126,374",
name:"jobOrderId",
size:"523,20",
transparent:true,
typeid:4,
uuid:"1449FD08-CC1A-4E3F-9269-3F9C6D66A594",
valuelistID:"54327DE3-DF2E-4FC7-8DF5-163FF1221191"
},
{
anchors:3,
location:"693,269",
showFocus:false,
size:"74,20",
styleClass:"bold",
text:"Documenti",
transparent:true,
typeid:7,
uuid:"152B33A5-2DC3-4441-BAED-76DEE81B1D66"
},
{
formIndex:10008,
groupID:"g_notEditable",
location:"351,64",
name:"lbl2",
size:"9,20",
text:"*",
transparent:true,
typeid:7,
uuid:"154F0A64-1B96-424A-A73C-E1F7B5E11D01",
visible:false
},
{
groupID:"g_notEditable",
location:"16,193",
name:"l_dataCreazioneOfferta",
showFocus:false,
size:"151,20",
text:"Data creazione Offerta",
transparent:true,
typeid:7,
uuid:"182C325D-3B3C-4BD5-B3F2-DB8CE208A571"
},
{
anchors:9,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"bo_to_is_linked_job_order_media.media",
formIndex:7,
horizontalAlignment:0,
location:"654,377",
name:"linkJoImg",
showClick:false,
size:"16,16",
text:"type",
typeid:7,
uuid:"1BCE8134-0E37-402F-BDF4-3D8A8148ED7C",
verticalAlignment:0
},
{
extendsID:"2FD9ADCF-E59B-4856-B8CB-C26AFF795744",
formIndex:0,
location:"0,479",
size:"1300,1",
typeid:7,
uuid:"1D78EA4C-7EE4-4B1F-89AA-01649371FCBD"
},
{
customProperties:"methods:{
onActionMethodID:{
arguments:[
null,
\"true\"
]
}
}",
imageMediaID:"4F294F47-4C57-4465-B0E9-85C7A1C23F47",
location:"15,489",
name:"buttonNewJobOrder",
onActionMethodID:"D827140A-F820-44D7-AD2F-970F157AB824",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
rolloverCursor:12,
showClick:false,
size:"217,26",
text:"Richiedi nuova commessa",
typeid:7,
uuid:"20883E36-4D9A-42E0-9208-6E249372C049"
},
{
location:"360,424",
showFocus:false,
size:"106,20",
text:"Tipo Commessa",
transparent:true,
typeid:7,
uuid:"240F0E30-CA61-4FDA-96E4-8A1BED34AA36"
},
{
extendsID:"4EF01A0C-8886-4BD4-8494-4826D6F4D39C",
location:"1259,489",
onActionMethodID:"58F0D32F-607F-4F1E-8438-4056187F69A7",
rolloverCursor:12,
text:null,
toolTipText:"Annulla",
typeid:7,
uuid:"250E82CD-0401-42EF-8BA1-6E663DE43C79"
},
{
extendsID:"3DDC34AE-9103-4FD9-A661-254F61AAC65E",
height:478,
typeid:19,
uuid:"2719B06C-7C14-4454-BCC1-DDD8D3F3A959"
},
{
anchors:9,
groupID:"g_notEditable",
location:"16,64",
name:"l_cliente",
showFocus:false,
size:"106,20",
text:"Cliente",
transparent:true,
typeid:7,
uuid:"2798273B-202B-49A8-9201-C6F724233BBB"
},
{
groupID:"g_notEditable",
location:"16,129",
name:"l_mercato",
showFocus:false,
size:"106,20",
text:"Mercato",
transparent:true,
typeid:7,
uuid:"2D9491CC-35D5-4515-9A20-73BAC8E716F2"
},
{
groupID:"g_notEditable",
location:"360,89",
name:"l_tipoBO",
showFocus:false,
size:"106,20",
text:"Tipo BO",
transparent:true,
typeid:7,
uuid:"2DAA75B1-0026-4A4C-99DD-C6E89DA3C140",
visible:false
},
{
dataProviderID:"bo_to_job_orders.job_order_type",
displayType:10,
editable:false,
enabled:false,
location:"470,424",
name:"joType",
size:"200,20",
transparent:true,
typeid:4,
uuid:"3292806E-6C61-4578-8C81-540EFE3AF796",
valuelistID:"9123976F-276C-4008-B26A-33F742962BB6"
},
{
location:"16,374",
showFocus:false,
size:"92,20",
text:"Commessa",
transparent:true,
typeid:7,
uuid:"329940E0-3C07-4441-A1B7-9FCF696056B1"
},
{
extendsID:"DA9E3F87-A868-4257-AFFB-2214FB026E9A",
location:"1259,489",
rolloverCursor:12,
text:null,
toolTipText:"Modifica",
typeid:7,
uuid:"367F1548-238C-448F-9F13-1B74BA12628E"
},
{
groupID:"g_notEditable",
location:"16,304",
name:"l_inizioAttivita",
showFocus:false,
size:"175,20",
text:"Previsione inizio Attività",
transparent:true,
typeid:7,
uuid:"38BCD701-FFA8-4AFB-9648-EB7A6469A6A9"
},
{
location:"17,244",
name:"l_extraOfferAmount",
showFocus:false,
size:"106,20",
text:"Integraz. Offerta",
transparent:true,
typeid:7,
uuid:"41A1C019-2C4C-431F-AFAE-89C41AAF9B9F",
visible:false
},
{
anchors:3,
borderType:"LineBorder,1,#cacaca",
items:[
{
containsFormID:"DC85C23B-3524-4DCC-8110-1A6EB325DA39",
location:"772,304",
relationName:"business_opportunities_to_bo_bo_documents",
text:"bo_documents",
typeid:15,
uuid:"1B44FC2F-DBE3-4637-BF98-16BD7BF529A3"
}
],
location:"767,261",
name:"tab_bo_documents",
printable:false,
size:"511,183",
transparent:true,
typeid:16,
uuid:"4229D58D-496F-4072-BF14-1083673FCA45"
},
{
dataProviderID:"bo_to_job_orders.lob_id",
displayType:10,
editable:false,
enabled:false,
location:"126,399",
name:"lob",
size:"200,20",
transparent:true,
typeid:4,
uuid:"4328086A-A870-4F73-AC47-F3822A6C6805",
valuelistID:"FED305B8-9E96-4607-B119-221D774363FE"
},
{
location:"16,424",
showFocus:false,
size:"106,20",
text:"RDC",
transparent:true,
typeid:7,
uuid:"440D68DD-39CE-4366-96A5-42C47F01A84C"
},
{
dataProviderID:"profit_center_id",
displayType:10,
groupID:"g_notEditable",
location:"126,129",
name:"ProfitCenterController",
onDataChangeMethodID:"D4AD238C-D864-4366-92C1-742F458DABD2",
size:"200,20",
typeid:4,
uuid:"44886722-A208-4794-8139-21655BDF4EFC",
valuelistID:"73312BF3-989F-48F4-85E1-AB96BDEA9A3F",
visible:false
},
{
groupID:"g_notEditable",
location:"16,218",
name:"l_importoOfferta",
showFocus:false,
size:"106,20",
text:"Importo Offerta",
transparent:true,
typeid:7,
uuid:"45A38CD6-0D8A-4328-A1D2-1184AC1B7BA3"
},
{
dataProviderID:"bo_to_job_orders.user_owner_id",
displayType:10,
editable:false,
enabled:false,
location:"126,424",
name:"RDC",
size:"200,20",
transparent:true,
typeid:4,
uuid:"4B29E511-B603-4E33-94CE-D841579E1044",
valuelistID:"3AB8DD5C-F02E-40DF-879B-9BFBCE22AF8D"
},
{
dataProviderID:"bo_probability",
displayType:2,
editable:false,
groupID:"g_notEditable",
location:"126,271",
name:"probabilty",
onDataChangeMethodID:"0925D8FC-A8F8-4964-9DD3-AEC5E62188E0",
size:"200,20",
typeid:4,
uuid:"50BB94E6-58D3-4FDE-9BEF-15E986DF7BCF",
valuelistID:"27B47AEB-90CD-4B9C-8834-A1A504DCEECF"
},
{
groupID:"g_notEditable",
location:"360,218",
mnemonic:"",
name:"l_importoOrdine",
showFocus:false,
size:"106,20",
text:"Importo Ordine",
transparent:true,
typeid:7,
uuid:"5A3B9389-54CE-45D9-9CF2-2D3F36026A08"
},
{
anchors:9,
groupID:"g_notEditable",
location:"16,114",
name:"l_accountManager",
showFocus:false,
size:"106,20",
text:"Account Manager",
transparent:true,
typeid:7,
uuid:"5AC7BC66-48AE-4FE8-B55E-D72C032F648E",
visible:false
},
{
dataProviderID:"job_order_id",
formIndex:10003,
location:"678,398",
size:"49,20",
transparent:true,
typeid:7,
uuid:"6428AB28-03AF-4A8E-939B-848C84C58258",
visible:false
},
{
anchors:9,
dataProviderID:"company_id",
displayType:10,
groupID:"g_notEditableSuppComm",
location:"126,64",
name:"customerSuppComm",
size:"200,20",
typeid:4,
uuid:"645578CB-5558-40BB-90D2-2AD6D047F4C9",
valuelistID:"A1FBA56C-E800-4353-9B51-C8921FE4FADA"
},
{
anchors:9,
groupID:"g_notEditable",
location:"360,64",
name:"l_clienteFinale",
showFocus:false,
size:"106,20",
text:"Cliente Finale",
transparent:true,
typeid:7,
uuid:"68B98FBE-647D-424C-8DDD-DAF809CE7EA4"
},
{
groupID:"g_notEditable",
location:"16,271",
name:"l_prob",
showFocus:false,
size:"106,20",
text:"Probabilità",
transparent:true,
typeid:7,
uuid:"69DC4A27-4DC6-4FB7-ABA2-CBCE7E769196"
},
{
anchors:9,
groupID:"g_notEditable",
location:"16,39",
name:"l_descrizione",
showFocus:false,
size:"106,20",
text:"Descrizione",
transparent:true,
typeid:7,
uuid:"6C394BD3-5C5C-499B-8C9C-AC665CCA9FB2"
},
{
anchors:9,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"bo_to_job_order_reqired_media.media",
formIndex:7,
horizontalAlignment:0,
location:"654,377",
name:"reqJoImg",
showClick:false,
size:"16,16",
typeid:7,
uuid:"6EA3EF93-C994-4664-A487-0403F94DF19F",
verticalAlignment:0
},
{
displaysTags:true,
formIndex:2,
imageMediaID:"A645E912-3A73-47EE-9C1F-A5DDC85B3929",
location:"16,348",
name:"buttonNote",
onActionMethodID:"DA6C3A3E-A680-40E2-8D50-E1308BEFC6C5",
onRenderMethodID:"-1",
printable:false,
showClick:false,
showFocus:false,
size:"151,20",
styleClass:"bold-red",
text:"Richiesta annullata",
toolTipText:"Clicca per le note",
transparent:true,
typeid:7,
uuid:"77B408D7-7CA3-41DD-AA41-B28BD6CC67B6"
},
{
groupID:"g_notEditable",
location:"360,114",
name:"l_anticipo",
showFocus:false,
size:"106,20",
text:"Anticipo",
transparent:true,
typeid:7,
uuid:"782C8ADA-27FE-40F1-930E-CF6A1910D5F8",
visible:false
},
{
dataProviderID:"tow_id",
displayType:10,
enabled:false,
location:"470,448",
name:"tow",
size:"200,20",
typeid:4,
uuid:"78613023-181C-47B3-98E5-50788AA913FE",
valuelistID:"C3B2A573-45D6-4090-80DE-0E00690C7A6C",
visible:false
},
{
anchors:9,
dataProviderID:"final_company_id",
displayType:10,
groupID:"g_notEditable",
location:"470,64",
name:"finalCustomer",
size:"200,20",
typeid:4,
uuid:"787C2896-B0ED-483F-905C-E68628F4233D",
valuelistID:"A92648D5-45BF-465D-A307-6EDA6AD80919"
},
{
extendsID:"07AB7FEF-0560-4B82-B56C-D95051E8AD67",
location:"1218,489",
onActionMethodID:"57E52147-1342-4C9C-A021-7EDFA4EC77DF",
rolloverCursor:12,
text:null,
toolTipText:"Salva",
typeid:7,
uuid:"79016C6E-33D1-4AE0-934A-E8A595175723"
},
{
anchors:3,
dataProviderID:"is_editable",
displayType:4,
formIndex:10004,
location:"588,493",
name:"isEnable",
size:"511,20",
text:"Abilitata in modifica",
transparent:true,
typeid:4,
uuid:"7A13174B-DAC6-46B2-95A3-A42B746D5B82"
},
{
dataProviderID:"business_unit_id",
displayType:10,
location:"126,448",
name:"BusinessUnit",
onDataChangeMethodID:"6BB418E6-50F0-4942-B718-DB8AFE420761",
size:"200,20",
typeid:4,
uuid:"809C958E-71B9-4D58-B37B-94A6F2F26276",
valuelistID:"A1173C2D-6099-4334-BA1F-693FCEF767C5",
visible:false
},
{
anchors:9,
dataProviderID:"customer_person",
displayType:10,
groupID:"g_notEditable",
location:"126,89",
name:"customerRif",
size:"200,20",
typeid:4,
uuid:"823C6BE9-BA79-4B39-A01F-BCB06EE7B86E"
},
{
formIndex:2,
location:"360,399",
showFocus:false,
size:"167,20",
text:"Data richiesta commessa",
transparent:true,
typeid:7,
uuid:"8383921A-FDBE-42E1-9092-A689A4EA202D"
},
{
location:"16,12",
showFocus:false,
size:"106,20",
styleClass:"bold",
text:"Progressivo BO",
transparent:true,
typeid:7,
uuid:"83D49A11-D6FC-48C9-B6E6-B4047F9BED40"
},
{
dataProviderID:"status",
displayType:2,
editable:false,
groupID:"g_notEditable",
location:"470,271",
name:"status",
size:"200,20",
typeid:4,
uuid:"8456EFB5-A816-4DB7-87C0-17769C8217A6",
valuelistID:"09A13B52-A2BF-4135-9307-A23904A68C53"
},
{
dataProviderID:"profit_center_id",
displayType:10,
groupID:"g_notEditableSuppComm",
location:"127,129",
name:"ProfitCenterSuppComm",
onDataChangeMethodID:"D4AD238C-D864-4366-92C1-742F458DABD2",
size:"200,20",
typeid:4,
uuid:"84F74DCD-DEB3-40CD-A540-57D060339111",
valuelistID:"B94470CC-43E3-4397-8476-BAAE3F1857C8"
},
{
dataProviderID:"bo_probability",
displayType:2,
editable:false,
groupID:"g_notEditable",
location:"126,271",
name:"probabiltyController",
onDataChangeMethodID:"0925D8FC-A8F8-4964-9DD3-AEC5E62188E0",
size:"200,20",
typeid:4,
uuid:"8ACD843A-3A09-4AD3-805E-D0B5E53AE53B",
valuelistID:"16E35284-0ED2-4080-A74C-947475706FA0"
},
{
anchors:3,
location:"722,85",
showFocus:false,
size:"45,20",
styleClass:"bold",
text:"Ordini",
transparent:true,
typeid:7,
uuid:"902B5553-C810-47A0-8212-AF292A8DC8C4"
},
{
dataProviderID:"bo_type",
groupID:"g_notEditable",
location:"470,89",
name:"boType",
size:"200,20",
typeid:4,
uuid:"957C7EEB-4B6F-4974-BE54-571CBF64820F",
valuelistID:"BDB54C8A-7B85-446A-8AA0-5B3F1D121335",
visible:false
},
{
anchors:9,
dataProviderID:"final_company_id",
displayType:10,
groupID:"g_notEditableSuppComm",
location:"470,64",
name:"finalCustomerSuppComm",
size:"200,20",
typeid:4,
uuid:"9686566B-0E59-4B1C-9857-CAFB697EE694",
valuelistID:"A1FBA56C-E800-4353-9B51-C8921FE4FADA"
},
{
location:"16,399",
showFocus:false,
size:"106,20",
text:"Lob",
transparent:true,
typeid:7,
uuid:"97AEE85F-4F62-4C8E-86C6-FAFA2D17A496"
},
{
formIndex:1,
location:"16,348",
name:"labelJobRequired",
size:"240,20",
styleClass:"bold-blu",
text:"Richiesta nuova commessa inviata",
transparent:true,
typeid:7,
uuid:"A525670F-E070-493F-8116-1C6C52639959"
},
{
formIndex:10006,
imageMediaID:"D2902910-9EA5-4800-927E-232505089207",
location:"16,489",
name:"buttonReject",
onActionMethodID:"73259573-256A-40A9-A8B1-FCEF28036608",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
rolloverCursor:12,
rolloverImageMediaID:"D7A4C49C-3518-4A47-B4C2-739DB8D71C8C",
showClick:false,
size:"144,26",
text:"Annulla richiesta",
typeid:7,
uuid:"A722495E-8BBB-4C08-B4C6-A9C4C8AF3BBA"
},
{
allowBreakAcrossPageBounds:false,
extendsID:"6F614343-45A5-4185-BAD8-A96D7B66D21E",
height:526,
typeid:19,
uuid:"B312E541-B03C-4D22-B1C4-1D7AC9F77789"
},
{
location:"16,449",
showFocus:false,
size:"106,20",
text:"Direzione Tecnica",
transparent:true,
typeid:7,
uuid:"B5B9BBB9-F149-4CE9-8E1C-7085D39FAC15",
visible:false
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"bo_code",
editable:false,
enabled:false,
fontType:"Calibri,1,15",
location:"471,12",
name:"boCode",
size:"106,20",
transparent:true,
typeid:4,
uuid:"B7022807-5D5D-4BFA-A108-A943C0E42B64"
},
{
dataProviderID:"offering_date",
displayType:5,
format:"dd-MM-yyyy|dd-MM-yyyy",
groupID:"g_notEditable",
location:"550,193",
name:"dateOffer",
size:"120,20",
styleClass:"date",
typeid:4,
uuid:"B7DC0669-E636-47B5-9187-4ED79A0E84A4"
},
{
anchors:9,
dataProviderID:"company_id",
displayType:10,
groupID:"g_notEditable",
location:"126,64",
name:"customer",
size:"200,20",
typeid:4,
uuid:"B873E9E8-F332-42BA-9B48-902273DA1FF8",
valuelistID:"A92648D5-45BF-465D-A307-6EDA6AD80919"
},
{
dataProviderID:"order_amount",
editable:false,
format:",##0.00",
groupID:"g_notEditable",
location:"470,218",
name:"orderAmount",
size:"200,20",
typeid:4,
uuid:"BAF032AB-A471-4375-9E53-C7B506A0F751"
},
{
dataProviderID:"end_date",
displayType:5,
format:"MMMMMM yyyy",
groupID:"g_notEditable",
location:"536,304",
name:"dateEndJo",
size:"134,20",
styleClass:"date",
typeid:4,
uuid:"BC56F2DA-39E2-4746-A38F-966C0C452198"
},
{
groupID:"g_notEditable",
location:"360,193",
name:"l_dataOferta",
showFocus:false,
size:"151,20",
text:"Data emissione Offerta",
transparent:true,
typeid:7,
uuid:"BDD81713-8F98-480D-B65B-0C2F21D0CE88"
},
{
dataProviderID:"start_date",
displayType:5,
format:"MMMMMM yyyy",
groupID:"g_notEditable",
location:"192,304",
name:"dateStartJo",
size:"134,20",
styleClass:"date",
typeid:4,
uuid:"C3D2CB13-0493-4048-A63C-835985E5E1C9"
},
{
dataProviderID:"credit_amount",
groupID:"g_notEditable",
location:"470,114",
name:"creditValue",
size:"200,20",
typeid:4,
uuid:"CB8FEC53-CFE1-4A48-8A04-F60A1C9BF326",
visible:false
},
{
formIndex:3,
imageMediaID:"ABFDEA0F-945D-45AD-A283-65442D975701",
location:"680,374",
mediaOptions:14,
name:"showJo",
onActionMethodID:"92166B98-C3BB-45B5-9F52-724FB966598D",
rolloverCursor:12,
showClick:false,
size:"20,20",
toolTipText:"vai alla commessa",
transparent:true,
typeid:7,
uuid:"CEFBB2B0-8643-4527-9E46-0C2ED49804E5"
},
{
dataProviderID:"created_at",
displayType:5,
editable:false,
enabled:false,
format:"dd-MM-yyyy|dd-MM-yyyy",
groupID:"g_notEditable",
location:"206,193",
name:"creationDate",
size:"120,20",
styleClass:"date",
transparent:true,
typeid:4,
uuid:"CF9E18A1-BF1F-4A39-ACE4-B31F2E7BFA86"
},
{
dataProviderID:"extra_offer_amount",
format:",##0.00",
location:"127,244",
name:"extraOfferAmount",
size:"200,20",
typeid:4,
uuid:"D70E9507-48B2-4FF2-98A5-BF6AE5750487",
visible:false
},
{
groupID:"g_notEditable",
location:"361,304",
name:"l_dataFineAttivita",
showFocus:false,
size:"174,20",
text:"Previsione fine Attività",
transparent:true,
typeid:7,
uuid:"D9DBDD1C-DBD5-47D4-8535-7B00FEFD850F"
},
{
groupID:"g_notEditable",
location:"360,244",
mnemonic:"",
name:"l_lastOrder",
showFocus:false,
size:"167,20",
text:"Importo Ordine Definitivo",
transparent:true,
typeid:7,
uuid:"E209BDB1-5373-4696-BCE8-F24C95CE2598"
},
{
location:"360,12",
showFocus:false,
size:"106,20",
styleClass:"bold",
text:"Codice BO",
transparent:true,
typeid:7,
uuid:"E3A991D1-A3BD-4BBC-A26E-D96560E8492D"
},
{
anchors:9,
dataProviderID:"pc_owner_id",
displayType:2,
editable:false,
groupID:"g_notEditable",
location:"126,154",
name:"clientManager",
size:"200,20",
typeid:4,
uuid:"E7BDD90D-C880-43EF-9900-DBB41A9899A5",
valuelistID:"900F9C58-6569-4539-84E5-CB553DCEF2F7"
},
{
dataProviderID:"last_order_amount",
displayType:4,
formIndex:10007,
groupID:"g_notEditable",
location:"528,244",
name:"lastOrder",
size:"23,20",
transparent:true,
typeid:4,
uuid:"EBF2A993-A296-4DE9-99A6-9A637A925D56"
},
{
customProperties:"methods:{
onActionMethodID:{
arguments:[
null,
\"false\"
]
}
}",
imageMediaID:"BE2CBB0B-3E71-41F4-BD19-B85150618D70",
location:"257,489",
name:"buttonLinkJobOrder",
onActionMethodID:"D827140A-F820-44D7-AD2F-970F157AB824",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
rolloverCursor:12,
showClick:false,
size:"160,26",
text:"Associa a commessa",
typeid:7,
uuid:"EDCCEDF2-27FF-448E-AD64-8E01ABA5140B"
},
{
dataProviderID:"offer_amount",
format:",##0.00",
groupID:"g_notEditable",
location:"126,218",
name:"offerAmount",
size:"200,20",
typeid:4,
uuid:"EDF7B370-28DF-420F-A5DB-90BAFB1CC3C5"
},
{
anchors:3,
location:"724,14",
showFocus:false,
size:"43,20",
styleClass:"bold",
text:"Note",
transparent:true,
typeid:7,
uuid:"F170CB9E-246D-4369-AFC4-58B1D5E9CABC"
},
{
anchors:9,
dataProviderID:"bo_description",
groupID:"g_notEditable",
location:"126,39",
name:"boDescription",
size:"544,20",
typeid:4,
uuid:"F1AA21ED-F22B-4326-8B4F-F040DF15769E"
},
{
anchors:3,
borderType:"LineBorder,1,#cacaca",
formIndex:10001,
items:[
{
containsFormID:"CA7B15EE-9D09-485F-9895-8226F8A458B9",
location:"769,133",
relationName:"bo_to_bo_orders",
text:"bo_orders",
typeid:15,
uuid:"AA857BFC-FAE3-4C32-AA45-84D8E6BE7611"
}
],
location:"767,85",
name:"tab_bo_orders",
printable:false,
size:"511,160",
transparent:true,
typeid:16,
uuid:"F330ABE5-7803-49D9-8269-37CA4D3288E1"
},
{
dataProviderID:"profit_center_id",
displayType:2,
editable:false,
groupID:"g_notEditable",
location:"126,129",
name:"ProfitCenter",
onDataChangeMethodID:"D4AD238C-D864-4366-92C1-742F458DABD2",
printable:false,
size:"200,20",
typeid:4,
uuid:"F81622A5-9D56-46EB-808B-EEE40A1B51BD",
valuelistID:"1FF47B65-84F5-45BC-9096-89D5827C10E3"
},
{
displaysTags:true,
groupID:"g_notEditable",
location:"360,129",
name:"l_geo",
showFocus:false,
size:"106,20",
text:"Area Geografica",
transparent:true,
typeid:7,
uuid:"F880F84D-9CE1-40D5-A0FD-7AF5B6268550"
},
{
anchors:9,
dataProviderID:"account_manager_id",
displayType:10,
groupID:"g_notEditable",
location:"126,114",
name:"accountManager",
size:"200,20",
typeid:4,
uuid:"FA5AAF16-6EA1-443B-8E13-B828C8BB8A5C",
valuelistID:"9AD80147-D825-4FE6-A4E7-76B41C997CFB",
visible:false
},
{
formIndex:1,
location:"360,448",
showFocus:false,
size:"106,20",
text:"Tow",
transparent:true,
typeid:7,
uuid:"FAC2BA60-F42A-4D64-8FCD-5B21FF2FF2EF",
visible:false
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"bo_number",
editable:false,
enabled:false,
fontType:"Calibri,1,15",
format:"#####",
location:"126,12",
name:"boNumber",
size:"106,20",
transparent:true,
typeid:4,
uuid:"FB113892-AC02-4120-84FC-6E7A54115164"
},
{
formIndex:10008,
groupID:"g_notEditable",
location:"351,89",
name:"lbl3",
size:"314,20",
text:"* Per selezionare i clienti, seleziona prima un Mercato.",
transparent:true,
typeid:7,
uuid:"FCFF2827-0627-47D2-928A-C138656FFE4D",
visible:false
}
],
name:"bo_tab_details",
navigatorID:"-1",
onLoadMethodID:"-1",
onShowMethodID:"D18A2785-3C6A-47E2-9BE4-25E834732F9A",
showInMenu:true,
size:"1300,536",
styleName:"GeCo",
transparent:false,
typeid:3,
uuid:"F3949832-6B76-4386-8ACB-258F206659F6"