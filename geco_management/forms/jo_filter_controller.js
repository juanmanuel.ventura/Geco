/**
 * @type {Number}
 * @properties={typeid:35,uuid:"737A926F-281B-4E2A-AD0F-FA3810352B7F",variableType:8}
 */
var customer = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"63195DAB-8DE8-414F-A677-BB18846B75AE",variableType:8}
 */
var statusJo = 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"0A00A4AF-9CF5-48D3-8E96-CF292C7D1998",variableType:8}
 */
var jobOrderId = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"3EC4D169-A42C-42D8-ADD4-0C55CF0C6851",variableType:8}
 */
var profitCenterId = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"971E831F-3EDC-43AA-B7AA-108D410C327B",variableType:8}
 */
var rdcJo =  null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"81ACC98A-24BE-4478-A9F5-6FEAB76600EA"}
 */
var type = null;

/**
 * @properties={typeid:35,uuid:"6C4ADB05-68C0-4517-87E9-B0A0D997614F",variableType:-4}
 */
var actualDate = null;


/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"C6449B7A-920C-4469-817A-B49B008B0ED5"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"B2B2B6EC-4841-4DF4-979C-BF50C9CA5D61"}
 */
function applyFilter(event) {
application.output('APPLY ' + scopes.globals.actualMonthFilterController + ' ' + scopes.globals.actualYearFilterController);
	//verifico se è responsabile di mercato
	if (foundset.find()) {
		foundset.is_enabled = statusJo;
		foundset.company_id = customer;
		foundset.job_order_type = type == null ? '!0' : type;
		foundset.job_order_id = jobOrderId;
		foundset.profit_center_id = profitCenterId;
		foundset.user_owner_id = rdcJo;
		foundset.job_orders_to_jo_planning.jo_month = scopes.globals.actualMonthFilterController;
		foundset.job_orders_to_jo_planning.jo_year = scopes.globals.actualYearFilterController
		
		//application.output(' pcToSearch ' + pcToSearch + '; foundset.profit_center_id ' + foundset.profit_center_id + '; foundset.user_owner_id ' + foundset.user_owner_id)
		foundset.search();
	}
	foundset.sort('job_order_id asc');
	var totalRecord = databaseManager.getFoundSetCount(foundset);
	application.output('---------------- '+totalRecord);
	var totalCostMonth = 0.00;
	var totalProfitMonth = 0.00;
	var totalProfitActual = 0.00;
	var totalProfitForecast = 0.00;
	var totalBudgetBO = 0.00;
	for (var i = 1; i<=totalRecord; i++){
		var rec= foundset.getRecord(i);
		totalCostMonth = totalCostMonth + rec.job_orders_to_jo_planning_controller.total_cost_actual;
		totalProfitMonth = totalProfitMonth + rec.job_orders_to_jo_planning_controller.total_return_actual;
		totalProfitActual = totalProfitActual + rec.job_orders_to_jo_planning_actual.total_return_actual;
		totalProfitForecast = totalProfitForecast + rec.job_orders_to_jo_planning_not_actual.total_return;
		totalBudgetBO = totalBudgetBO + rec.total_offer;
		
	}
	forms.jo_list_controller.totalCostMonth = totalCostMonth;
	forms.jo_list_controller.totalProfitMonth = totalProfitMonth;
	forms.jo_list_controller.totalBudgetBO = totalBudgetBO;
	forms.jo_list_controller.totalProfitActual = totalProfitActual;
	forms.jo_list_controller.totalProfitForecast = totalProfitForecast;
//	application.output(totalRecord);
//	application.output(foundset.getRecord(totalRecord));
	controller.setSelectedIndex(totalRecord)
	application.output(globals.messageLog + 'jo_filter_controller.applyFilter() ultimo record: ' +  controller.getSelectedIndex(),LOGGINGLEVEL.DEBUG);
}

/**
 * @param event
 *
 * @properties={typeid:24,uuid:"5E78BC1C-09D6-4E9A-9519-3A6F6660D4A9"}
 */
function resetFilter(event) {
//	application.output('RESET  ')
	customer = null;
	//statusJo = null;
	profitCenterId = null;
	rdcJo = null
	jobOrderId = null;
	type = null;
	statusJo = 1;
//	application.output('RESET ')
	
	applyFilter(event);
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"B545C5C7-9095-4EB8-AA4A-B9AFFAE9B7D0"}
 */
function updateUI(event) {
	_super.updateUI(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"C1198582-2F83-4432-BA26-D8ABEDAFCFB1"}
 */
function onLoad(event) {
	foundset.removeFoundSetFilterParam('enabled');
}
