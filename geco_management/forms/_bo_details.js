/**
 * @properties={typeid:35,uuid:"62710B46-3498-4245-AAC0-8A5EA9B48A74",variableType:-4}
 */
var tab_from = null;

/**
 * @properties={typeid:35,uuid:"F88F0BCB-1B3B-4A11-BD50-12AA60538AFD",variableType:-4}
 */
var first = false;

/**
 * @param {JSEvent} event
 * @param {String} formName
 * @param {String} relationName
 *
 * @properties={typeid:24,uuid:"D7F5FF15-FDA4-4F43-979C-32E75D8D3429"}
 */
function loadTab(event, formName, relationName) {
	application.output(globals.messageLog + 'START _bo_details.loadTab form: ' + formName + ', realzione: ' + relationName, LOGGINGLEVEL.DEBUG);
	// if in editing mode DO NOT move away!
	if (foundset.getSelectedRecord() && (!foundset.getSelectedRecord().isNew())) {
		if (isEditing()) {
			globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!", "OK");
			return;
		}

	}

	if (!relationName) relationName = null;
	// move selector
	setSelector(event);
	if (elements.tabless.removeAllTabs()) {
		if (forms[formName]) {
			var result = elements.tabless.addTab(forms[formName], null, null, null, null, null, null, relationName || null, -1);
			application.output(globals.messageLog + '_bo_details.loadTab form caricata: ' + result, LOGGINGLEVEL.DEBUG);
		} else application.output(globals.messageLog + 'ERROR _bo_details.loadTab errore caricamento form: ' + formName, LOGGINGLEVEL.ERROR);
	}
	//updateUI(event);
	plugins.WebClientUtils.executeClientSideJS('window.location.reload()');
	application.output(globals.messageLog + 'STOP _bo_details.loadTab ', LOGGINGLEVEL.DEBUG);
}

/**
 * @param {JSEvent} event
 * @param {String} formName
 * @param {String} relationName
 *
 * @properties={typeid:24,uuid:"99D218BD-A101-4BE4-9ED5-CFC5C70E1055"}
 */
function loadForm(event, formName, relationName) {
	application.output(globals.messageLog + 'START _bo_details.loadForm form: ' + formName + ', realzione: ' + relationName, LOGGINGLEVEL.DEBUG);
	loadTab(event, formName, relationName);
	updateUI(event);
	application.output(globals.messageLog + 'STOP _bo_details.loadForm', LOGGINGLEVEL.DEBUG);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"7C47B8FB-3A0B-4922-8554-AD7136A50892"}
 * @private
 */
function setSelector(event) {
	var label = '';
	var name = 'button_details';

	if (event.getType() == JSEvent.ACTION) {
		// move the selector
		/** @type {RuntimeLabel} */
		label = event.getSource();
		name = label.getName();
		if (name == 'button_show' || name == 'buttonAdd' || name == 'showBo' || name == 'showJo') {
			name = 'button_details';
		}
	}
	if (tab_from != null && tab_from != 'showBo' && tab_from != 'showJo')
		elements[tab_from].bgcolor = null;
	tab_from = name;
	//application.output(name)
	elements[name].bgcolor = '#c9c9c9';
}

