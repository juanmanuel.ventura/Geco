/**
 * @properties={typeid:35,uuid:"3E1A7C65-BF47-4097-BF52-85811CA86E47",variableType:-4}
 */
var isMgmrPc = globals.hasRole('Resp. Mercato') || globals.hasRole('Vice Resp. Mercato');

/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"E3D54C61-5511-42B2-854C-B6F3294F3F2B",variableType:-4}
 */
var pcList =  isMgmrPc ?(globals.getProfitCenterByUserManager(scopes.globals.currentUserId)):[];
//var pcList =  (scopes.globals.hasRole('Resp. Mercato')) ?(globals.getProfitCenterByUserManager(scopes.globals.currentUserId)):[];

/**
 * @type {String}
 * @properties={typeid:35,uuid:"8D514E30-7590-4410-936A-A2BF9B5B3649"}
 */
var pcToSearch = (pcList.length!=0)?pcList.join('||'):null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"CFBDA72D-33E1-417F-A900-A56997C16C0D",variableType:8}
 */
var customer = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"27F482F4-3C71-48D0-AE59-178AD1E6939F",variableType:8}
 */
var statusJo = 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"82947FB8-DCAF-4C9A-B1AC-8052BBC1689A",variableType:8}
 */
var yearJo = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"B8CE65E4-C130-4EC3-A122-C6C1B6F24EE4",variableType:8}
 */
var jobOrderId = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"38B103FC-0703-4B98-8FE6-A2EEF2A37592",variableType:8}
 */
var profitCenterId = (pcList.length!=0)?pcList[0]:null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F2F18300-C016-4EEC-A065-BE58C5485E4F",variableType:8}
 */
var rdcJo = (globals.hasRole('Responsabili Commessa') && !isMgmrPc && !globals.hasRole('Steering Board'))?globals.currentUserId:null;
//var rdcJo = (globals.hasRole('Responsabili Commessa') && !globals.hasRole('Resp. Mercato') && !globals.hasRole('Steering Board'))?globals.currentUserId:null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"785789FC-650F-466C-80C9-9298EF5DF425"}
 */
var type = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"E0D0B8FD-EB07-4484-B5C5-F9CAA7C8D2EE",variableType:8}
 */
var pcOwnerID = (scopes.globals.hasRole('Resp. Mercato')) ? scopes.globals.currentUserId : null;

/**
 * @properties={typeid:35,uuid:"F59EAB86-04F5-4B24-9C1B-DEA32F1C5441",variableType:-4}
 */
var actualDate = null;
/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"C503304A-7EC9-49F2-8C67-494F65DF835F"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"62CF4B24-B75A-4A3F-AEB4-E9DBC5D1A722"}
 */
function applyFilter(event) {
//	application.output('APPLY Responsabile mercato? ' + globals.hasRole('Resp. Mercato') + '; pcToSearch ' + pcToSearch + '; PC ' + profitCenterId + '; rdcJo ' + rdcJo)
	//verifico se è responsabile di mercato
	if (foundset.find()) {
		foundset.is_enabled = statusJo;
		foundset.company_id = customer;
		foundset.job_order_year = yearJo;
		foundset.job_order_type = type==null?'!0':type;
		foundset.job_order_id = jobOrderId;
		//solo RDC !globals.hasRole('Resp. Mercato')
		if ( !isMgmrPc && !globals.hasRole('Planner') && !globals.hasRole('Steering Board') && globals.hasRole('Responsabili Commessa')) {
			foundset.user_owner_id = globals.currentUserId;
			foundset.profit_center_id = profitCenterId;
		}
		//Responsabili di mercato globals.hasRole('Resp. Mercato')
		else if (isMgmrPc  && !globals.hasRole('Planner') && !globals.hasRole('Steering Board') ){
//			application.output(pcList);
//			application.output('indice ' + (pcList.indexOf(profitCenterId)));
			if (profitCenterId != null && pcList.indexOf(profitCenterId) != -1){
				foundset.profit_center_id = profitCenterId;
				foundset.user_owner_id = rdcJo;
			}
			else if (profitCenterId != null) {
				foundset.profit_center_id = profitCenterId;
				foundset.user_owner_id = globals.currentUserId;
			}
			else {
				//se PC null allora i suoi pc o le commesse di altri pc di cui è rdc 
				foundset.profit_center_id = pcToSearch;
				foundset.user_owner_id = rdcJo;
				foundset.newRecord();
				foundset.is_enabled = statusJo;
				foundset.company_id = customer;
				foundset.job_order_year = yearJo;
				foundset.job_order_type = type==null?'!0':type;
				foundset.job_order_id = jobOrderId;
				foundset.profit_center_id = '!'+pcToSearch;
				foundset.user_owner_id = globals.currentUserId;
			}
		}
		else if (globals.hasRole('Planner') || globals.hasRole('Steering Board') ){
			foundset.profit_center_id = profitCenterId;
			foundset.user_owner_id = rdcJo;
		}
		//application.output(' pcToSearch ' + pcToSearch + '; foundset.profit_center_id ' + foundset.profit_center_id + '; foundset.user_owner_id ' + foundset.user_owner_id)
		foundset.search();
	}
	foundset.sort('job_order_id asc');
	var totalRecord = databaseManager.getFoundSetCount(foundset);
	var totalCostMonth = 0;
	var totalProfitMonth = 0;
	for (var i = 1; i<=totalRecord; i++){
		var rec= foundset.getRecord(i);
		totalCostMonth = totalCostMonth + rec.job_orders_to_jo_planning_controller.total_cost_actual;
		totalProfitMonth = totalProfitMonth + rec.job_orders_to_jo_planning_controller.total_return_actual;
	}
	forms.jo_list.totalCostMonth = totalCostMonth;
	forms.jo_list.totalProfiMonth = totalProfitMonth;
	
//	application.output(totalRecord);
//	application.output(foundset.getRecord(totalRecord));
	controller.setSelectedIndex(totalRecord)
	application.output(globals.messageLog + 'jo_filter.applyFilter() ultimo record: ' +  controller.getSelectedIndex(),LOGGINGLEVEL.DEBUG);
	
}

/**
 * @param event
 *
 * @properties={typeid:24,uuid:"15DC339F-C142-4A25-B233-D137490404D9"}
 */
function resetFilter(event) {
	application.output('RESET Responsabile mercato? ' + globals.hasRole('Resp. Mercato') + ' pcToSearch  ' + pcToSearch + ' PC ' + profitCenterId)
	customer = null;
	statusJo = 1;
	yearJo = null;
	if (globals.hasRole('Resp. Mercato')) {
		profitCenterId = (pcList!=null && pcList.length!=0)?pcList[0]:null;
	}
	else {
		profitCenterId = null;
	}
	rdcJo = (globals.hasRole('Responsabili Commessa') && !globals.hasRole('Resp. Mercato') && !globals.hasRole('Steering Board'))? globals.currentUserId:null;
	jobOrderId = null;
	type = null;
	application.output('RESET Responsabile mercato? ' + globals.hasRole('Resp. Mercato') + ' pcToSearch  ' + pcToSearch + ' PC ' + profitCenterId + ' rdcJo ' + rdcJo)
	applyFilter(event);
}


/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"1E880228-B3E4-4A49-B746-AADF948D029C"}
 */
function updateUI(event) {
	_super.updateUI(event);
	//if (globals.hasRole('Responsabili Commessa') && !globals.hasRole('Delivery Manager') && !globals.hasRole('Steering Board') && !globals.hasRole('Resp. Mercato') && !globals.hasRole('Planner'))
	if (globals.hasRole('Responsabili Commessa') && !globals.hasRole('Delivery Manager') && 
			!globals.hasRole('Steering Board') && !isMgmrPc && !globals.hasRole('Planner')) {
		elements.rdc.enabled = false;
		elements.rdc.editable = false;
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"834413F4-05B4-4B0F-8AAE-CA79DC6071EF"}
 */
function onLoad(event) {
	foundset.removeFoundSetFilterParam('enabled');
	scopes.globals.actualMonthFilterController = scopes.globals.actualMonth;
	scopes.globals.actualYearFilterController = scopes.globals.actualYear;
	
}
