/**
 * @type {Number}
 * @properties={typeid:35,uuid:"425C5EDB-23EA-46AB-8967-6AAE513B9BD3",variableType:8}
 */
var monthAt = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"B7F9E555-5F42-4C14-9E65-4E225457D378",variableType:8}
 */
var yearFrom = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"57859847-17AC-470C-BE40-644FD18A3072",variableType:8}
 */
var monthFrom = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"56F8E25D-D0AF-473D-8C75-19861AE3EB4C",variableType:8}
 */
var yearAt = null;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"DFAA18C5-1EC6-4A4B-B9AF-2621267903E9",variableType:93}
 */
var selectedDateFrom = null;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"6ACFC40C-A2F0-4C3F-B73F-9F08AB137CC6",variableType:93}
 */
var selectedDateAt = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"4D1189AA-5E1F-4AB0-B172-B71F27004D7A",variableType:8}
 */
var selectedQueryParam = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"ADBE51F0-9A7D-4236-B5BC-E12F73B96B91",variableType:8}
 */
var selectedJobOrder = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"B63EB155-9773-41A8-B8A3-EFAB9D0FD8DF",variableType:8}
 */
var selectedBO = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"91DA995B-BCEC-4E4D-907A-F55EF14737D2",variableType:8}
 */
var selectedRespMercato = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"BEB8E0E2-F2F8-41CC-80D1-EC70B84A9752",variableType:8}
 */
var selectedRDC = (globals.hasRole('Responsabili Commessa') && !globals.hasRole('Resp. Mercato') && !globals.hasRole('Vice Resp. Mercato')) ? globals.currentUserId : null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F850D2A4-2E0C-4204-8405-E5C6172E3785",variableType:8}
 */
var selectedProfCenterJob = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"64396EB7-3ABE-44E9-9C6A-7CAB5D16D79C",variableType:4}
 */
var isDeliveryDirector = 0;

/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"1337E1A0-5A6A-452E-B1D1-ED33EF4814AE",variableType:-4}
 */
var vl_joborders = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"6B397C84-269E-4066-AD23-89DB44C7020E",variableType:4}
 */
var selectedCompanyStart = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"0FF76BA0-FD62-4C2B-B009-B23E7B00C42A",variableType:4}
 */
var selectedCompanyEnd = null;

/**
 * @properties={typeid:35,uuid:"4DE40BFC-7851-4567-BD0F-BE8A0AF945B7",variableType:-4}
 */
var isCommercialSupport = false//scopes.globals.hasRole('Supp. Commerciale');

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"65A8E60A-C97D-44C2-A4F2-F88B1FC135BF"}
 */
var isMgm = scopes.globals.hasRole('Resp. Mercato') || scopes.globals.hasRole('Vice Resp. Mercato');

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number}   queryType
 * @properties={typeid:24,uuid:"B2EE44E0-1C75-42A9-BAB3-44DDCB3A2DFB"}
 * @AllowToRunInFind
 */
function exportReport(event, queryType) {
	
	var errorMessage = '';
	if (yearFrom == null) errorMessage = '-Il campo \"Anno Inizio\" è obbligatorio.';

	if (selectedQueryParam == null) errorMessage = errorMessage + '\n-Il campo \"Tipo di ricerca\" è obbligatorio.'
	
	if (!scopes.globals.isEmpty(errorMessage)){
		globals.DIALOGS.showErrorDialog('Attenzione', errorMessage, 'OK');
		return
	}
	
	/*---CONTROLLI SU MESE DAL E ANNO DAL---*/
	if (yearFrom != null) {
		//mese è valorizzato, prendo anno, mese e metto il primo giorno
		if (monthFrom != null) {
			selectedDateFrom = new Date(yearFrom, monthFrom-1, 1);
			application.output('selectedDateFrom: ' + selectedDateFrom);
		}//non ho il mese di inizio, prendo anno, gennaio e il primo del mese 
		else {
			selectedDateFrom = new Date(yearFrom, 0, 1);
			monthFrom = 1;
			application.output('selectedDateFrom: ' + selectedDateFrom);
		}
	}
	
	/*---CONTROLLI SU MESE AL E ANNO AL---*/
	if (monthAt != null) {
		//anno e mese sono entrambi valorizzati, setto la data ultima come anno, mese e ultimo giorno del mese
		if(yearAt != null){
			selectedDateAt = new Date(yearAt, monthAt, 0);
			application.output('selectedDateAt: ' + selectedDateAt);
		}//ho il mese ma non ho l'anno, metto mese, anno di inizio e ultimo giorno del mese
		else{
			selectedDateAt = new Date(yearFrom, monthAt, 0);
			yearAt = yearFrom;
			application.output('selectedDateAt: ' + selectedDateAt);
		}
	}//non ho il mese:
	else{
		//ma ho l'anno, metto anno scelto, mese dal e ultimo giorno del mese scelto
		if(yearAt != null){
			selectedDateAt = new Date(yearAt, monthFrom+11, 0);
			monthAt = selectedDateAt.getMonth()+1;
			application.output('selectedDateAt: ' + selectedDateAt);
		}//non ho nè mese nè anno, mi metto la data a come ultimo giorno dell'ultimo mese dell'anno di inizio
		else{
			selectedDateAt = new Date(yearFrom, 12, 0);
			monthAt = 12;
			yearAt = yearFrom;
			application.output('selectedDateAt: ' + selectedDateAt);
		}
	}

	/** @type {JSFoundSet<db:/geco/mgm_report_list>} */
	var mgmReportList = databaseManager.getFoundSet('geco', 'mgm_report_list');
	var totMgmReportList = 0;
	
	application.output(mgmReportList);
	if (mgmReportList.find()) {
		mgmReportList.id_report = selectedQueryParam;
		totMgmReportList = mgmReportList.search();
	}
	application.output('totMgmReportList: ' + totMgmReportList);

	if (totMgmReportList > 0) {
		forms.mgm_report_list.nameExport = mgmReportList.getRecord(1).report_name;
		application.output('forms.mgm_report_list.nameExport: ' + forms.mgm_report_list.nameExport);
		application.output('SP: ' +  mgmReportList.sp_name);
		for (var xx = 1; xx<= totMgmReportList; xx++){
			var rec = mgmReportList.getRecord(xx)
			if (rec.group_id == 27) isCommercialSupport = true;
		}
			
	}
	
	//iQueryParam, iDatefrom, iDateAt, iJobOrderId, iBoId, iMarketId, iMarketManagerId, iRDC, iUserId
	arguments = new Array();
	arguments[0] = isCommercialSupport;
	arguments[1] = selectedQueryParam; //iQueryParam
	arguments[2] = selectedDateFrom; //iDatefrom
	arguments[3] = selectedDateAt; //iDateAt
	arguments[4] = (selectedJobOrder != null) ? selectedJobOrder : 0; //iJobOrderId
	arguments[5] = (selectedBO != null) ? selectedBO : 0; //iBoId
	arguments[6] = (selectedProfCenterJob != null) ? selectedProfCenterJob : 0; //iMarketId
	arguments[7] = (selectedRespMercato != null) ? selectedRespMercato : 0; //iMarketManagerId
	arguments[8] = (selectedRDC != null) ? selectedRDC : 0; //iRDC
	arguments[9] = (globals.currentUserId != null) ? globals.currentUserId : 0; //iUserId
	arguments[10] = (selectedCompanyStart != null) ? selectedCompanyStart : 0;
	arguments[11] = (selectedCompanyEnd != null) ? selectedCompanyEnd : 0;
	application.output(arguments);
//	var _query_download = 'call sp_mgm_report( ?, ?, ?, ?, ?, ?, ?, ?, ?)';
	var _query_download = 'call sp_mgm_report( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
	var dataset = databaseManager.getDataSetByQuery('geco', _query_download, arguments, 10000);

	var htmlTable = '<html>' + dataset.getAsHTML(true, true, false, true, true) + '</html>';

	forms.mgm_report_list.tableReport = htmlTable;
	forms.mgm_report_list.datasetReport = dataset;
	isCommercialSupport = false;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"7078C234-F099-45DD-AFE2-6F20EE37186B"}
 */
function refresh(event) {
	selectedQueryParam = null;
	yearAt = null;
	yearFrom = null;
	monthAt = null;
	monthFrom = null;
	selectedJobOrder = null;
	selectedRDC = null;
	selectedProfCenterJob = null;
	selectedBO = null;
	selectedRespMercato = null;
	
	if (isCommercialSupport){
		selectedCompanyStart = null;
		selectedCompanyEnd = null;
	}

	application.output('Fields cleared.');
	//	selectedDateFrom = null;
	//	selectedDateAt = null;
	
	forms.mgm_report_list.tableReport = '';
	forms.mgm_report_list.datasetReport = null;
	//	forms.mgm_report_list.nameExport = '';
}

/**
 * @param event
 * @properties={typeid:24,uuid:"D763A289-1687-4F8F-B6C2-076BCDFC15B1"}
 */
function updateUI(event) {
	_super.updateUI(event);
	if (scopes.globals.hasRole('Responsabili Commessa') && !scopes.globals.hasRole('Delivery Manager') && !scopes.globals.hasRole('Client Manager') 
		&& !scopes.globals.hasRole('Resp. Mercato') && !scopes.globals.hasRole('Planner') && !scopes.globals.hasRole('Orders Admin') && !globals.hasRole('Vice Resp. Mercato')) {
		elements.selectedRDC.enabled = false;
		elements.selectedRDC.editable = false;
	}
	
//	if (scopes.globals.hasRole('Supp. Commerciale')){
//		//nascondo campi che non mi servono
//		elements.lblJO.visible = false;
//		elements.selectedJobOrder.visible = false;
//		elements.lblRDC.visible = false;
//		elements.selectedRDC.visible = false;
//		elements.selectedPC.visible = false;
//		elements.selBO.visible = false;
//		
//		//mostro solo campi di interesse
//		elements.lblCompanyStart.visible = true;
//		elements.lblCompanyEnd.visible = true;
//		elements.selectedPCSuppComm.visible = true;
//		elements.selCompanyStart.visible = true;
//		elements.selCompanyEnd.visible = true;
//		elements.selBOSuppComm.visible = true;
//	}
}
