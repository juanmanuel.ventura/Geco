/**
 * @type {Number}
 * @properties={typeid:35,uuid:"561D395B-117D-42CC-BB27-5B402C646D79",variableType:8}
 */
var selectedYear = new Date().getFullYear();

/**
 * Filter records
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"095A5AAD-1DB8-40A5-B949-3E8BDD1C3174"}
 */
function applyFilter(event) {
}

/**
 * Clear selection, revert db changes and close popup
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"61F48558-B609-4405-8D5D-EC3F94DAA653"}
 */
function rollbackAndClose(event) {
	databaseManager.revertEditedRecords(foundset)
	databaseManager.setAutoSave(true);
	var currentWin = controller.getWindow();
	if(currentWin)
		currentWin.destroy();
}

/**
 * Save edits and close popup
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"69F09575-2985-4899-9B6E-EDF0FEAACEFC"}
 */
function saveEdits(event) {
	application.output(globals.messageLog + 'START dialog_enable_actual.saveEdits ', LOGGINGLEVEL.INFO);
//	application.output('foundset.month_status_actual PRIMA: ' + foundset.month_status_actual);
	
	if(foundset.is_open_to_actual == 0){
		if(foundset.month_status_actual == 2 || foundset.month_status_actual == 4) foundset.month_status_actual = 3;
	}else{
		if(foundset.month_status_actual == 1) foundset.month_status_actual = 2;
		if(foundset.month_status_actual == 3) foundset.month_status_actual = 4;
	}
	
//	application.output('foundset.month_status_actual DOPO: ' + foundset.month_status_actual);
	_super.saveEdits(event);
	controller.getWindow().destroy();
	application.output(globals.messageLog + 'STOP dialog_enable_actual.saveEdits nuovo valore mese actual '+ foundset.month_status_actual , LOGGINGLEVEL.INFO);
}

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @protected 
 * @properties={typeid:24,uuid:"82EAA6CC-E48C-4A3B-B6D2-9F6FC22134CA"}
 */
function onLoad(event) {
	foundset.loadAllRecords();
	//applyFilter(event);
	databaseManager.setAutoSave(false);
}

/**
 * Handle hide window.
 * @param event
 *
 * @properties={typeid:24,uuid:"C2E3AF6F-A527-481E-A41F-96227C3330DA"}
 */
function onHide(event) {
	rollbackAndClose(event)
}
