/**
 * @type {Number}
 * @properties={typeid:35,uuid:"338D515B-B24B-49B1-A93C-C08CAB5B3A83",variableType:8}
 */
var userCostID = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"7F01A966-F72D-4866-95E6-3180119AA81A"}
 */
var user_id_er = null;

/**
 * Handle record selected.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"49B46EC4-1378-4996-8A76-B2264DA457D7"}
 */
function onRecordSelection(event) {
	updateUI(event);
	switchPanels(null, null);
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"EDD745FB-712C-4EA0-AE90-8634103EFF85"}
 */
function onDataChangeType(oldValue, newValue, event) {
	return switchPanels(null, null);
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"B8223270-725E-406B-B179-094B0BC59D97"}
 */
function onDataChangeStandardFigure(oldValue, newValue, event) {
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"387E3804-3C53-41C6-A378-96EAA21FC1C7"}
 * @AllowToRunInFind
 */
function onDataChangeRealFigure(oldValue, newValue, event) {
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"0287E183-2FAD-4F4F-A701-33B71008DB28"}
 */
function onDataChangeDaysNumber(oldValue, newValue, event) {
	return true
}

/**
 * @param {String} profit_cost_type
 * @param {Number} profit_cost_id
 * @return {Boolean}
 * @properties={typeid:24,uuid:"AB609E75-8DBD-4D12-B751-8E98930104EF"}
 */
function switchPanels(profit_cost_type, profit_cost_id) {
	// if new record unload panels and exit
	if (foundset.getSize() == 0 || profit_cost_type == null) {
		elements.groupGeneric.visible = false;
		elements.groupRealP.visible = false;
		elements.groupRealTK.visible = false;
		elements.groupRealTM.visible = false;
		elements.groupDaysImport.visible = false;
		elements.groupDescription.visible = false;
		elements.groupTotal.visible = false;
		return false;
	}
	if (profit_cost_type != null) {
		if (profit_cost_type == 'Ricavo') {

			elements.groupGeneric.visible = false;
			elements.groupRealP.visible = false;
			elements.groupRealTK.visible = false;
			elements.groupRealTM.visible = false;
			elements.groupDaysImport.visible = false;
			elements.groupDescription.visible = true;
			elements.groupTotal.visible = true;
			elements.groupTotal.setLocation(elements.groupTotal.getLocationX(), elements.label_tipologia.getLocationY())

		} else {
			//personale
			if (profit_cost_id == 5) {
				elements.groupGeneric.visible = true;
				elements.groupRealP.visible = true;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = true;
				elements.groupDescription.visible = false;
				elements.groupTotal.visible = true;
				elements.groupTotal.setLocation(elements.groupTotal.getLocationX(), elements.groupDaysImport.getLocationY())
			}
			//acquisto materiali
			else if (profit_cost_id == 7) {
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
				elements.groupDescription.visible = true;
				elements.groupTotal.visible = true;
				elements.groupTotal.setLocation(elements.groupTotal.getLocationX(), elements.label_tipologia.getLocationY())
			}
			// fornitura TK
			else if (profit_cost_id == 8) {
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = true;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
				elements.groupDescription.visible = false;
				elements.groupTotal.visible = true;
				elements.groupTotal.setLocation(elements.groupTotal.getLocationX(), elements.groupRealTK.getLocationY())
			}
			// fornitura TM
			else if (profit_cost_id == 9) {
				elements.groupGeneric.visible = true;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = true;
				elements.groupDaysImport.visible = true;
				elements.groupDescription.visible = false;
				elements.groupTotal.visible = true;
				elements.groupTotal.setLocation(elements.groupTotal.getLocationX(), elements.groupDaysImport.getLocationY())
			}

			else if (profit_cost_id == 10) {
				elements.groupGeneric.visible = true;
				elements.labale_st_cost.visible = false;
				elements.standAmount.visible = false;
				elements.groupRealP.visible = true;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
				elements.groupDescription.visible = false;
				elements.groupTotal.visible = true;
				elements.groupTotal.setLocation(elements.groupTotal.getLocationX(), elements.groupDaysImport.getLocationY())
			}
			else {
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
				elements.groupDescription.visible = true;
				elements.groupTotal.visible = true;
				elements.groupTotal.setLocation(elements.groupTotal.getLocationX(), elements.label_tipologia.getLocationY())

			}
		}
	}
	return true;
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"41F14A13-059C-4E17-872A-0A2B04508D3D"}
 */
function onDataChangeAmountPersonnel(oldValue, newValue, event) {
	return true
}

/**
 * @param oldValue
 * @param newValue
 * @param event
 *
 * @properties={typeid:24,uuid:"C37893FE-8E5F-4ED7-B893-FC312291D449"}
 */
function onDataChangeAmountStandard(oldValue, newValue, event) {
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"672E53C1-2ECD-42AD-81EF-9C80FF851704"}
 */
function onDataChangeTK(oldValue, newValue, event) {
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"98F7901A-5D24-443D-A536-C34D4F82DE1A"}
 */
function onDataChangeTotalAmount(oldValue, newValue, event) {
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B2979CC6-CAE5-4C6E-AC43-7C05000A855D"}
 * @AllowToRunInFind
 */
function createPlain(event) { }

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @private
 *
 * @properties={typeid:24,uuid:"6360F307-0A23-4D6F-98BA-4498E44D3737"}
 * @AllowToRunInFind
 */
function deleteRecord(event, index) { }
