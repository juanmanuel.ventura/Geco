
/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formFilterName
 * @param {String} formListName
 *
 * @properties={typeid:24,uuid:"AFACC117-5B3A-4019-94F7-D3261B792B5A"}
 */
function onLoad(event, formFilterName, formListName) {
	if(globals.hasRole('Controllers') || globals.hasRole('Orders Admin') ){
		formFilterName = 'jo_filter_controller';
		formListName = 'jo_list_controller';
	}
	_super.onLoad(event, formFilterName, formListName);
}
