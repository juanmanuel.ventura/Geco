/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F58288D1-B214-4FDA-A260-B001E5E84AA8",variableType:4}
 */
var selectedMonth = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"FE736C27-DC8C-49EC-94B4-296EEEAA5978",variableType:8}
 */
var selectedYear = null;


/**
 * Filter records
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"2F837E6D-8E7D-4369-8F33-6FFAE8F6651C"}
 */
function applyFilter(event) {
	var monthSelected = false;
	var selDate = null;
	var fromDate = null;
	var toDate = null;
	if (selectedMonth == null) {
		fromDate = ''+selectedYear+'-01-01';
		toDate = ''+selectedYear+'-12-31';
		application.output('fromDate: ' + fromDate + '; toDate: ' + toDate);
	} else {
		selDate = new Date(selectedYear, selectedMonth, 0);
		monthSelected = true;
		application.output('selDate: ' + selDate);
	}
	if (foundset.find()) {
		if (monthSelected) foundset.competence_month = selDate;
		else foundset.competence_month = fromDate + '...' + toDate+' |yyyy-MM-dd';
		foundset.mgm_market_competence_to_profit_centers.is_to_check_market_competence = 1;
		var tot = foundset.search();
		application.output('query: ' + databaseManager.getSQL(foundset)+'\nTot: ' + tot);
	}
	application.output('month: ' + selectedMonth + '; year: ' + selectedYear + ' ; selDate: ' + selDate);
}

/**
 * Clear selection, revert db changes and close popup
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"98CB4F47-4905-4050-A84F-9649F82CFD04"}
 */
function rollbackAndClose(event) {
	var currentWin = controller.getWindow();
	if (currentWin)
		currentWin.destroy();
}

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"8D2EA311-DF50-45B3-A61C-D21446586474"}
 */
function onLoad(event) {
	foundset.loadAllRecords();
}

/**
 * Handle hide window.
 * @param event
 *
 * @properties={typeid:24,uuid:"9FB8C1AC-BC94-44B4-8F36-6ABE81718901"}
 */
function onHide(event) {
	rollbackAndClose(event);
}

/**
 * @param {JSEvent} event
 *
 *
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"BEF2FB85-87D0-41AB-A407-67861C3A3B82"}
 */
function updateUI(event) {
	_super.updateUI(event);
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B1DD4C80-11CA-495F-B26B-DA1BA7D7A2E6"}
 */
function onShow(firstShow, event) {
	updateUI(event);
	if(firstShow) {
		selectedMonth = scopes.globals.actualMonth;
		selectedYear = scopes.globals.actualYear;
	}
	applyFilter(event);
}
