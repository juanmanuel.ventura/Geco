/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D559BECB-35FB-47E5-8C21-1125AE350191"}
 */
var criterio = '';
/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D023D0CC-3A48-4611-BEAD-D649F36395F9"}
 */
var description = '';

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"2CCE7076-EAFA-4775-ABC1-158E3397318B",variableType:-4}
 */
var isBillable = false;

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"71C64FCD-EFF2-4C64-8CE5-D0D7FCCB4EA9",variableType:-4}
 */
var isEnabled = false;

/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"FCB03519-36F4-4B07-92F9-C31D04EB8046",variableType:-4}
 */
var profit_cost_forOwner = [1,2,3,7,8,13]

/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"A4A9D253-B236-4B77-B63C-93EF6BDE830A",variableType:-4}
 */
var profit_cost_forController = [4,6,11,12]

/**
 * @param {JSEvent} event
 * @param {String} formName
 * @param {String} relationName
 *
 * @properties={typeid:24,uuid:"E2968476-2711-4D3D-A14C-D714FF63455E"}
 */
function loadForm(event, formName, relationName) {
	//application.output('jo_details.loadForm ' + formName + ' RELAZIONE ' + relationName);
	loadTab(event, formName, relationName);
	updateUI(event);
}

/**
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"AC7CF7E0-9681-4A20-A806-1DF074F71596"}
 */
function updateUI(event) {
	isBillable = foundset.is_billable != 0 && foundset.job_order_type > 0
	// togliere il controllo sul flag "da chiudere"
	isEnabled = foundset.is_enabled == 1 // && foundset.is_to_close == 0;
	if (foundset.job_orders_to_jo_details.mol_per_display < 30) {
		//application.output('jo_details.updateUI ' + foundset.job_orders_to_jo_details.mol_per_display);
		elements.molPer.fgcolor = 'red';
		elements.molPerA.fgcolor = 'red';
	} else {
		elements.molPer.fgcolor = null;
		elements.molPerA.fgcolor = null;
	}

	/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
	var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
	bo_actual_date.loadRecords();
	scopes.globals.actualDate = bo_actual_date.getRecord(1).actual_date;
	scopes.globals.actualMonth = bo_actual_date.getRecord(1).actual_month;
	scopes.globals.actualYear = bo_actual_date.getRecord(1).actual_year;
	scopes.globals.isOpenToActual = bo_actual_date.is_open_to_actual;
	scopes.globals.statusActual = bo_actual_date.getRecord(1).month_status_actual;
	scopes.globals.actualMonthStrIT = scopes.globals.monthName[scopes.globals.actualMonth - 1];
	//application.output(scopes.globals.actualMonthStrIT);

	//market is closed
	//var dateConfirmMarket = foundset.job_orders_to_mgm_market_competence.competence_month
	var isMarketConfirmed = (foundset.job_orders_to_mgm_market_competence && scopes.globals.actualDate == foundset.job_orders_to_mgm_market_competence.competence_month && foundset.job_orders_to_mgm_market_competence.actual_confirmation == 1);
	if (scopes.globals.hasRole('Controllers')) {
		elements.buttonActualConfirm.visible = isBillable && !isEditing() && isEnabled && (scopes.globals.isOpenToActual == 1 || (scopes.globals.isOpenToActual == 0 && scopes.globals.statusActual == 1));
		elements.buttonActualReject.visible = isBillable && !isEditing() && isEnabled && (scopes.globals.isOpenToActual == 1 || (scopes.globals.isOpenToActual == 0 && scopes.globals.statusActual == 1));
	} else {
		elements.buttonActualConfirm.visible = scopes.globals.isOpenToActual == 1 && !isMarketConfirmed && isBillable && !isEditing() && isEnabled && (globals.isPCManagerForObject(scopes.globals.currentUserId, foundset.job_order_id, 'jo') || globals.isOwnerForObject(scopes.globals.currentUserId, foundset.job_order_id, 'jo'))
		elements.buttonActualReject.visible = scopes.globals.isOpenToActual == 1 && !isMarketConfirmed && isBillable && !isEditing() && isEnabled && (globals.isPCManagerForObject(scopes.globals.currentUserId, foundset.job_order_id, 'jo'));
	}

}

/**
 * @param {JSEvent} event
 * @param {String} formName
 * @param {String} relationName
 *
 * @properties={typeid:24,uuid:"072BB611-4077-4BBB-A4F8-D2FC09336507"}
 * @AllowToRunInFind
 */
function loadTab(event, formName, relationName) {
	_super.loadTab(event, formName, relationName)
	//application.output('load tab linked jo ' + globals.jo_link_selected + ' jo selected ' + foundset.job_order_id)
	if (globals.jo_link_selected != null) {
		if (foundset.find()) {
			foundset.job_order_id = globals.jo_link_selected;
			foundset.search();
		}
		forms[formName].updateUI(event);
	}
	globals.job_order_selected = globals.jo_link_selected != null ? globals.jo_link_selected : foundset.job_order_id;
	globals.jo_link_selected = null;
	//forms[formName].updateUI(event);

	//application.output('JO selected = ' + globals.job_order_selected);
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"9FAB67D6-74F2-4A89-91F9-4E8437B6B4A4"}
 * @AllowToRunInFind
 */
function confirmJOActual(event) {
	application.output(globals.messageLog + 'START jodetails.confirmJOActual ',LOGGINGLEVEL.INFO);
	/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
	var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
	bo_actual_date.loadRecords();
	var actual_date = bo_actual_date.actual_date;
	var month = bo_actual_date.actual_month;
	var year = bo_actual_date.actual_year;
	var answer = globals.DIALOGS.showWarningDialog("Conferma", "Inseriti i valori actual, i piani mensili non saranno modificabili.\nProcedere per il mese " + scopes.globals.monthName[month - 1] + "-" + year + "?", "Si", "No");
//	var answer = 'SI'
	if (answer == 'No') return;

	/** @type {JSFoundSet<db:/geco/jo_planning>} */
	var jo_planning = foundset.job_orders_to_jo_planning;
	var totPlain = 0;
	var index = 0;
	var recPlain = null;
	//0-Inserito da RDC; 1-Confermato da RDC; 2-Confermato da RCP; 5-Actual;6:Inserito da RCP;
	//7-Respinto da RCP; 8-Actual Posting; 9-Rifiutato da Controller;
	if (jo_planning.find()) {
		if (scopes.globals.hasRole('Controllers')) {
			jo_planning.is_actual = 0;
			jo_planning.jo_month = month;
			jo_planning.jo_year = year;
			jo_planning.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_types_id = [4, 11];
		} else {
			jo_planning.is_actual = 0;
			jo_planning.jo_month = month;
			jo_planning.jo_year = year;
			jo_planning.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_types_id = [1, 2, 3, 7, 8, 13];
			jo_planning.newRecord();
			jo_planning.is_actual = 1;
			jo_planning.jo_month = month;
			jo_planning.jo_year = year;
			jo_planning.status = [0, 1, 6, 7, 9]
			jo_planning.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_types_id = [1, 2, 3, 7, 8, 13];
		}
		totPlain = jo_planning.search();
	}
	if (totPlain == 0) {
	//	globals.DIALOGS.showInfoDialog("Esito", "Nessuna pianificazione da attualizzare per il mese " + scopes.globals.monthName[month - 1] + "-" + year, "OK");
//		application.output(elements.tabless.getTabFormNameAt(1))
		if (elements.tabless.getTabFormNameAt(1) == 'jo_planning_bundle'){
			forms.jo_planning_filter.applyFilter(event);
		}
		return;
	}
	var missingCriterion = false;
	var missingDescriptionPlain = false
	//per singola descrizione
	for (index = 1; index <= jo_planning.getSize(); index++) {
		recPlain = jo_planning.getRecord(index);
		if (recPlain.jo_plain_description == null ) {
			missingDescriptionPlain = true;
		}
		if (recPlain.jo_plain_criterion == null) {
			missingCriterion = true;
		}
		
	}
	if (missingDescriptionPlain) {
		description = globals.DIALOGS.showInputDialog("Criterio", "Inserire la descrizione di actual");
		//application.output(description);
	}
	if (missingCriterion) {
		criterio = globals.DIALOGS.showInputDialog("Criterio", "Inserire il criterio di actual");
		//application.output(criterio);
	}
	//controllare descrizione e criterio se  null o stringa vuota uscire con messaggio di errore: descrizione e criterio obbligatori
	 if((globals.isEmpty(description) || globals.isEmpty(criterio)) && (missingCriterion && missingDescriptionPlain)) { 
		 globals.DIALOGS.showErrorDialog("Conferma", "La descrizione e il criterio di actual sono obbligatori", "OK");
		 forms.jo_planning_filter.applyFilter(event);
		 return;
	 }
	
	startEditing(event);
	if (jo_planning.getSize() > 0) {
		//INSERIMENTO STORICO
		var query = 'call sp_insert_history_jo(?,?,?,?,?,?)'; 
		var args = [foundset.job_order_id, scopes.globals.currentUserId, 'before actual by user ' + scopes.globals.currentUserDisplayName, month, year, 1];
		var dataset = databaseManager.getDataSetByQuery('geco', query, args, -1);
		application.output('inserito storico '+dataset.getMaxRowIndex());
		//FINE
	}
	var countActual = 0;
	//application.output('piani trovati ' + totPlain);
	if (globals.isPCManagerForObject(scopes.globals.currentUserId, foundset.job_order_id, 'jo')) {
		for (index = 1; index <= jo_planning.getSize(); index++) {
			recPlain = jo_planning.getRecord(index);
			//0-Inserito da RDC; 1-Confermato da RDC; 6:Inserito da RCP;
			//7-Respinto da RCP; 9-Rifiutato da Controller;
			//tipo costo ricavo [1, 2, 3, 6, 7, 8, 13]
			if (profit_cost_forOwner.indexOf(recPlain.jo_planning_to_jo_details.profit_cost_type_id)!=-1 &&
			(recPlain.status < 2 || recPlain.status == 6 || recPlain.status == 7 || recPlain.status == 9 || recPlain.status == null))
//			if ( (recPlain.jo_planning_to_jo_details.profit_cost_type_id == 1 || recPlain.jo_planning_to_jo_details.profit_cost_type_id == 2 || 
//					recPlain.jo_planning_to_jo_details.profit_cost_type_id == 3 || recPlain.jo_planning_to_jo_details.profit_cost_type_id == 7 || 
//					recPlain.jo_planning_to_jo_details.profit_cost_type_id == 8) &&
//					(recPlain.status < 2 || recPlain.status == 6 || recPlain.status == 7 || recPlain.status == 9 || recPlain.status == null))
 			{
				recPlain.status = 2; //confermato da RDC non più modificabile da RCP diventa actual
				recPlain.total_actual = (recPlain.total_actual == null) ? (recPlain.total_amount != null ? recPlain.total_amount : 0): recPlain.total_actual;
				if (recPlain.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_acr == 'R'){
					recPlain.return_actual = recPlain.total_actual;
				}
				else {
					recPlain.cost_actual = recPlain.total_actual;
				}
//				recPlain.cost_actual = recPlain.cost_amount != null ? recPlain.cost_amount : 0;
//				recPlain.return_actual = recPlain.return_amount != null ? recPlain.return_amount : 0;
				recPlain.user_actual = scopes.globals.currentUserId;
				if (recPlain.jo_plain_description == null) recPlain.jo_plain_description = description;
				if (recPlain.jo_plain_criterion == null) recPlain.jo_plain_criterion = criterio;
				recPlain.date_actual = new Date();
				recPlain.note_actual_confirmation = '';
				recPlain.is_actual = 1;
				recPlain.competence_month = actual_date;
				countActual++;
			}
		}
	} else if (globals.isOwnerForObject(scopes.globals.currentUserId, foundset.job_order_id, 'jo')) {
		//ciclo per tutti i piani del mese da attualizzare in stato 0
		for (index = 1; index <= jo_planning.getSize(); index++) {
			recPlain = jo_planning.getRecord(index);
			//0-Inserito da RDC;
			//7-Respinto da RCP; 9-Rifiutato da Controller;
			//tipo costo ricavo [1, 2, 3, 6, 7, 8,13]
//			if ( (recPlain.jo_planning_to_jo_details.profit_cost_type_id == 1 || recPlain.jo_planning_to_jo_details.profit_cost_type_id == 2 || 
//					recPlain.jo_planning_to_jo_details.profit_cost_type_id == 3 || recPlain.jo_planning_to_jo_details.profit_cost_type_id == 7 || 
//					recPlain.jo_planning_to_jo_details.profit_cost_type_id == 8) && 
//					(recPlain.status == 0 || recPlain.status == 7 || recPlain.status == 9 || recPlain.status == null)) {
			if (profit_cost_forOwner.indexOf(recPlain.jo_planning_to_jo_details.profit_cost_type_id)!=-1 && (recPlain.status == 0 || recPlain.status == 7 || recPlain.status == 9 || recPlain.status == null)){
				recPlain.status = 1; //confermato da RDC non più modificabile da RDC
				recPlain.total_actual = (recPlain.total_actual == null) ? (recPlain.total_amount != null ? recPlain.total_amount : 0): recPlain.total_actual;
//				recPlain.total_actual = recPlain.total_amount != null ? recPlain.total_amount : 0;
				if (recPlain.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_acr == 'R'){
					recPlain.return_actual = recPlain.total_actual;
				}
				else {
					recPlain.cost_actual = recPlain.total_actual;
				}
//				recPlain.cost_actual = recPlain.cost_amount != null ? recPlain.cost_amount : 0;
//				recPlain.return_actual = recPlain.return_amount != null ? recPlain.return_amount : 0;
				recPlain.user_actual = scopes.globals.currentUserId;
				if (recPlain.jo_plain_description == null) recPlain.jo_plain_description = description;
				if (recPlain.jo_plain_criterion == null) recPlain.jo_plain_criterion = criterio;
				recPlain.date_actual = new Date();
				recPlain.is_actual = 1;
				recPlain.competence_month = actual_date;
				countActual++;
			}
		}
	} else if (globals.hasRole('Controllers')) {
		for (index = 1; index <= jo_planning.getSize(); index++) {
			recPlain = jo_planning.getRecord(index);
			//status == null;
			//tipo costo ricavo [4, 6, 11, 12]
//			if ( (recPlain.jo_planning_to_jo_details.profit_cost_type_id == 4 || recPlain.jo_planning_to_jo_details.profit_cost_type_id == 6 || 
//					recPlain.jo_planning_to_jo_details.profit_cost_type_id == 11) && recPlain.status == null) {
			if (profit_cost_forController.indexOf(recPlain.jo_planning_to_jo_details.profit_cost_type_id)!=-1 && recPlain.status == null){
				recPlain.status = 5; //confermato da Controller
				recPlain.total_actual = (recPlain.total_actual == null) ? (recPlain.total_amount != null ? recPlain.total_amount : 0): recPlain.total_actual;
//				recPlain.total_actual = recPlain.total_amount != null ? recPlain.total_amount : 0;
				if (recPlain.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_acr == 'R'){
					recPlain.return_actual = recPlain.total_actual;
				}
				else {
					recPlain.cost_actual = recPlain.total_actual;
				}
//				recPlain.cost_actual = recPlain.cost_amount != null ? recPlain.cost_amount : 0;
//				recPlain.return_actual = recPlain.return_amount != null ? recPlain.return_amount : 0;
				recPlain.user_actual = scopes.globals.currentUserId;
				if (recPlain.jo_plain_description == null) recPlain.jo_plain_description = description;
				if (recPlain.jo_plain_criterion == null) recPlain.jo_plain_criterion = criterio;
				recPlain.date_actual = new Date();
				recPlain.is_actual = 1;
				recPlain.competence_month = actual_date;
				countActual++;
			}
		}
	}
	_super.saveEdits(event);
	jo_planning.loadRecords();
	//updateUI(event);
	globals.DIALOGS.showInfoDialog("Conferma", "Sono stati confermati i valori actual per " + countActual + " dettagli", "OK");
	updateUI(event);
	application.output(elements.tabless.getTabFormNameAt(0));
	if (elements.tabless.getTabFormNameAt(1) == 'jo_planning_bundle'){
		forms.jo_planning_filter.applyFilter(event);
	}
	application.output(globals.messageLog + 'STOP jodetails.confirmJOActual ',LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"F75E15DB-5DCA-4AEB-85E3-50199ABF7A5A"}
 * @AllowToRunInFind
 */
function rejectActualMonth(event) {
	application.output(globals.messageLog + 'START jodetails.rejectActualMonth ',LOGGINGLEVEL.INFO);
	var rejectionReason = globals.DIALOGS.showInputDialog("Rifiuta", "Inserisci il motivo del rifiuto");
	application.output(globals.messageLog + 'jodetails.rejectActualMonth rejectionReason: ' + rejectionReason,LOGGINGLEVEL.DEBUG);
	if (globals.isEmpty(rejectionReason) || rejectionReason == '' || rejectionReason == null) {
		globals.DIALOGS.showInfoDialog('INFO','Il rifiuto della commessa è stato annullato.','OK');
		return;
	}
	
	var joTitle = foundset.title_display;

	// application.output(rejectionReason);
	/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
	var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
	bo_actual_date.loadRecords();
	var month = bo_actual_date.actual_month;
	var year = bo_actual_date.actual_year;
	/** @type {JSFoundSet<db:/geco/jo_planning>} */
	var jo_planning = foundset.job_orders_to_jo_planning;
	//var totPlain = 0;
	var index = 0;
	var recPlain = null;

	if (jo_planning.find()) {
		jo_planning.is_actual = 1;
		if (globals.isPCManagerForObject(scopes.globals.currentUserId, foundset.job_order_id, 'jo')) {
			jo_planning.status = 1;

		} else if (globals.hasRole('Controllers')) {
			jo_planning.status = [1,2];
		}
		jo_planning.jo_month = month;
		jo_planning.jo_year = year;
		jo_planning.jo_planning_to_jo_details.jo_details_to_profit_cost_types.profit_cost_types_id = [1, 2, 3, 7, 8, 13];
		jo_planning.search();
	}

	//_super.startEditing(event);
	startEditing(event);
	//application.output('piani trovati ' + totPlain);
	if (jo_planning.getSize() > 0) {
		//INSERIMENTO STORICO
		var query = 'call sp_insert_history_jo(?,?,?,?,?,?)';
		var args = [foundset.job_order_id, scopes.globals.currentUserId, 'before reject actual by user ' + scopes.globals.currentUserDisplayName, month, year, 1];
		var dataset = databaseManager.getDataSetByQuery('geco', query, args, -1);
		application.output('inserito storico ' + dataset.getMaxRowIndex());
		//FINE
	}
	var count = 0;
	if (globals.isPCManagerForObject(scopes.globals.currentUserId, foundset.job_order_id, 'jo') || globals.hasRole('Controllers')) {
		var statusJP = 7;
		statusJP = globals.hasRole('Controllers') ? 9 : statusJP;
		for (index = 1; index <= jo_planning.getSize(); index++) {
			recPlain = jo_planning.getRecord(index);
			if ( (recPlain.status == 1) || recPlain.status == 2) {
				recPlain.status = statusJP; //rifiutato da RPC o controller
				recPlain.note_actual_confirmation = rejectionReason;
				recPlain.is_actual_confirmed = 0;
				recPlain.user_actual = scopes.globals.currentUserId;
				recPlain.date_actual = new Date();
				application.output(index + ' modificato ' + recPlain.jo_planning_id)
				count++;
			}
		}
		application.output('fine for');

		if (count > 0) {
			/** @type {JSFoundSet<db:/geco/mgm_market_competence>} */
			var mgmActualCompetence = databaseManager.getFoundSet('geco', 'mgm_market_competence');
			/** @type {Number} */
			var totMgmActualCompetence = 0;

			application.output('scopes.globals.actualDate: ' + scopes.globals.actualDate);

			if (mgmActualCompetence.find()) {
				mgmActualCompetence.competence_month = scopes.globals.actualDate;
				mgmActualCompetence.profit_center_id = foundset.profit_center_id;
				totMgmActualCompetence = mgmActualCompetence.search();
			}

			application.output('totMgmActualCompetence: ' + totMgmActualCompetence);

			//trovato il record confermato da Responsabile, lo modifico
			if (totMgmActualCompetence != 0) {
				var rec = mgmActualCompetence.getRecord(1);
				if (rec.actual_confirmation == 1) rec.actual_confirmation = 0;
			}
		}
	}
	_super.saveEdits(event);

	//invio mail al RDC per notifica rifiuto!!!!!
	if (foundset.user_owner_id != scopes.globals.currentUserId && count > 0) {
		globals.sendNotifyMail(15, foundset.job_orders_owner_to_users, joTitle, rejectionReason);
	}
	jo_planning.loadRecords();
	if (elements.tabless.getTabFormNameAt(1) == 'jo_planning_bundle'){
		forms.jo_planning_filter.applyFilter(event);
	}
	application.output(globals.messageLog + 'STOP jodetails.rejectActualMonth ',LOGGINGLEVEL.INFO);	
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"118267FD-3888-4058-A01D-B8BC013B7EFB"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event)
	if (firstShow) {
		plugins.WebClientUtils.executeClientSideJS('window.location.reload()');
	}
}
