/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"72363C62-8A5A-4902-8EBF-EF6551ED518A"}
 * @AllowToRunInFind
 */
function plainSingleDetails(event) {
	/** @type {Array} */
	var edited = databaseManager.getEditedRecords();

	//planningToRevert = [];

	var rec_details = foundset.getSelectedRecord();
	application.output(rec_details.bo_details_id);
	//se il dettaglio non � stato ancora pianificato
	if ( (edited == null || edited.length == 0) && (rec_details.bo_details_to_bo_planning == null || rec_details.bo_details_to_bo_planning.getSize() == 0)) {
		application.output('devi pianificare');
		insertNewDetailsPlain(rec_details);
		application.output('pianificato dettaglio');
	}
	var days = 0;
	/** @type {Number[]} */
	var id_changed = [];
	var tot = edited.length;
	var modified = '';
	//ciclo su tutti i record  modificati
	for (var i = 0; i < tot; i++) {
		/** @type {JSFoundSet<db:/geco/bo_planning>} */
		var bo_planning_edited = edited[i];

		// se il bo_plain � relativo al bo_details selezionato altrimenti non faccio nulla
		if (bo_planning_edited.bo_details_id == rec_details.bo_details_id) {
			application.output('NUOVO ' + bo_planning_edited.isNew());
			if (bo_planning_edited.isNew() || bo_planning_edited.bo_id == null) {
				application.output('nuovo record ' + bo_planning_edited.bo_planning_id + ' del dettaglio ' + bo_planning_edited.bo_details_id)
				bo_planning_edited.bo_id = rec_details.bo_id;
			}

			var id_planning = bo_planning_edited.bo_planning_id;

			var dataset = edited[i].getChangedData();
			application.output(dataset);
			var importDay = bo_planning_edited.bo_planning_to_bo_details.days_import;

			for (var x = 1; x <= dataset.getMaxRowIndex(); x++) {
				if (dataset.getValue(x, 1) == 'days_number') {
					modified = 'days_number';
					id_changed.push(id_planning);

					obj = {
						id: bo_planning_edited.bo_planning_id,
						daysNumber: dataset.getValue(x, 2),
						totalAmount: importDay * dataset.getValue(x, 2),
						costAmount: importDay * dataset.getValue(x, 2),
						returnAmount: null,
						amount: null
					}
					planningToRevert.push(obj);
					//somma dei giorni modificati
					days = days + dataset.getValue(x, 3);

					bo_planning_edited.total_amount = Math.round( (dataset.getValue(x, 3) * importDay) * Math.pow(10, 2)) / Math.pow(10, 2);
					bo_planning_edited.cost_amount = Math.round( (dataset.getValue(x, 3) * importDay) * Math.pow(10, 2)) / Math.pow(10, 2);
					bo_planning_edited.days_import = importDay;
					application.output('bo_planning-day----' + bo_planning_edited.days_number);
					application.output('bo_planning-totalAmount-----' + bo_planning_edited.total_amount);
					application.output('bo_planning-costAmount-----' + bo_planning_edited.cost_amount);
					databaseManager.saveData(bo_planning_edited);
				}
			}
			application.output('somma giorni modificati ' + days);
			application.output('id plain modificati ' + id_changed);
		}
		var table = databaseManager.getTable(edited[i]);
		application.output(table);
	}
	// cerco gli altri details_plain e ricalcolo i giorni e amount
	if (modified == 'days_number') {
		//numero di giorni rimanenti
		var days_to_split = rec_details.days_number - days;

		foundset.bo_details_to_bo_planning.loadAllRecords();
		application.output('size ' + foundset.bo_details_to_bo_planning.getSize());
		//numero record non modificati
		var count = foundset.bo_details_to_bo_planning.getSize() - id_changed.length;
		application.output(count)
		var newDaysNumber = 0;
		if (count > 0) newDaysNumber = Math.round( (days_to_split / count) * Math.pow(10, 2)) / Math.pow(10, 2);
		application.output(newDaysNumber);

		//		application.output(result)
		for (var index = 1; index <= foundset.bo_details_to_bo_planning.getSize(); index++) {
			var rec = foundset.bo_details_to_bo_planning.getRecord(index);
			if (id_changed.indexOf(rec.bo_planning_id) < 0) {
				application.output('non trovato ' + rec.bo_planning_id);
				obj = {
					id: rec.bo_planning_id,
					daysNumber: newDaysNumber,
					totalAmount: foundset.days_import * newDaysNumber,
					costAmount: foundset.days_import * newDaysNumber,
					returnAmount: null,
					amount: null
				}
				planningToRevert.push(obj);
				rec.days_number = newDaysNumber;
				rec.days_import = importDay;

				rec.total_amount = Math.round( (newDaysNumber * importDay) * Math.pow(10, 2)) / Math.pow(10, 2);
				rec.cost_amount = Math.round( (newDaysNumber * importDay) * Math.pow(10, 2)) / Math.pow(10, 2);
				application.output('bo_planning-day---- ' + rec.days_number);
				application.output('bo_planning-totalAmount----- ' + rec.total_amount);
				application.output('bo_planning-costAmount----- ' + rec.cost_amount);
				databaseManager.saveData(bo_planning_edited);
			}
			application.output(rec.bo_planning_id);
		}

	}
}