/**
 * @type {String}
 * @properties={typeid:35,uuid:"DFEA9B88-85AE-4EFD-B4FA-970A18086DBE"}
 */
var focus = null;

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formSx
 * @param {String} formDetailsName
 *
 * @properties={typeid:24,uuid:"C8FC51D5-04C6-4A71-9C28-D43BAA56D6BD"}
 */
function initialize(event, formSx, formDetailsName) {
	application.output(globals.messageLog + 'START _bo_bundle.initialize() form sinistra: ' +  formSx + ', form dettaglio: ' + formDetailsName ,LOGGINGLEVEL.DEBUG);
	var sizeWindows = 2000;
	var current = controller.getWindow();
	if (current) sizeWindows = current.getWidth();

	
	elements.split_vertical.dividerSize = 0;
	elements.split_vertical.dividerLocation = sizeWindows - 1;
	elements.split_vertical.removeAllTabs();
	elements.split_vertical.setLeftForm(forms[formSx]);
	
	application.output(globals.messageLog + 'STOP _bo_bundle.initialize() ', LOGGINGLEVEL.DEBUG);
}
