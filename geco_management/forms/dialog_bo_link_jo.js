/**
 * @type {Number}
 * @properties={typeid:35,uuid:"E155CD24-5487-4C6E-B052-8FBFFF378B0A",variableType:8}
 */
var job_id = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C67FCBD8-9422-4985-A783-9BFDDA01D5D2"}
 */
var message = '';

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"362F113B-6E54-4949-8952-0694F7AF5D88",variableType:-4}
 */
var isNew = false;

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"84A83D2E-5F10-48F1-9755-E758FC441836"}
 */
function onShow(firstShow, event) {
	var query = 'select concat(external_code_navision, " - ", job_order_title) ,job_order_id  from job_orders where company_id in (?,?) and is_enabled=1 and job_order_type !=4 and profit_center_id = ?';
	var valueListJO = databaseManager.getDataSetByQuery(globals.gecoDb, query, [foundset.company_id,foundset.final_company_id, foundset.profit_center_id], -1);
	globals.selected_client = foundset.company_id;
	application.output(valueListJO);
	application.setValueListItems('selectedJoToLink', valueListJO);
	application.output('valuelist ' + application.getValueListArray('selectedJoToLink'))
	_super.startEditing(event);
}

/**
 * @param event
 *
 * @properties={typeid:24,uuid:"D88AF643-BE33-4A8C-95D9-98F9691B861D"}
 */
function stopEditing(event) {
	clearField();
	_super.stopEditing(event);
	if (controller.getWindow() != null)
		controller.getWindow().destroy();
}

/**
 * @properties={typeid:24,uuid:"797D083C-CCAE-43EC-8C58-E44561019549"}
 */
function clearField() {
	job_id = null;
	isNew = false;
	message = '';

}

/**
 * @AllowToRunInFind
 * @param event
 *
 * @properties={typeid:24,uuid:"978DC1AE-9995-49F9-B41D-414D6221F846"}
 */
function saveEdits(event) {
	application.output(globals.messageLog + 'START dialog_bo_link.saveEdits isNew =  ' + isNew, LOGGINGLEVEL.INFO);
	var mail_type = 0;
	var save = false;
	var errorMessage = 'Non è possibile proseguire con la richiesta:';
	var errorResult = false;
	if (checkInputValues() == false) {
		application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits: Inserire tutti i campi', LOGGINGLEVEL.INFO);
		globals.DIALOGS.showWarningDialog('Attenzione', 'Inserire tutti i campi.', 'OK');
		return;
	}
	/** @type {JSFoundSet<db:/geco/job_orders>} */
	var job_orders = databaseManager.getFoundSet('geco', 'job_orders');
	if (!isNew) {
		if (job_orders.find()) {
			job_orders.job_order_id = job_id;
			job_orders.search();
		}
	}
	application.output(globals.messageLog + 'dialog_bo_link.saveEdits isNew=' + isNew + ', offerta=' + foundset.offer_amount + ', ordine=' + foundset.order_amount + ', ordine definitivo=' + foundset.last_order_amount + ', ricavi=' + foundset.profit, LOGGINGLEVEL.INFO);
	//richiesta nuova commessa non gestionale
	if (isNew == true && foundset.job_order_type_id != null && foundset.job_order_type_id != 4) {
		//controllo BC
		if (globals.hasBusinessCase(foundset.bo_id) == false) {
			errorResult = true;
			errorMessage = errorMessage + '\n- non è stato inserito un Business Case valido';
			application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits: Nuova NON gestionale BC non valido', LOGGINGLEVEL.ERROR);
			foundset.bo_to_bo_details.loadAllRecords();
			foundset.bo_to_bo_planning.loadAllRecords();
			//globals.DIALOGS.showErrorDialog("Error", "Non è possibile proseguire con la richiesta, non è stato inserito un Business Case valido.", 'OK');
			//return;
		}
		//controllo importo offerta = ricavo
		if ( (foundset.last_order_amount == 1 && foundset.order_amount != foundset.profit) || (foundset.last_order_amount != 1 && foundset.offer_amount != foundset.profit)) {
			errorResult = true;
			errorMessage = errorMessage + '\n- la somma dei ricavi inseriti è diversa dall\''+((foundset.last_order_amount == 1) ? 'ordine' : 'offerta');
			application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits: Nuova NON gestionale somma ricavi diversa dall\'offerta o dall\'ordine', LOGGINGLEVEL.ERROR);
			//globals.DIALOGS.showErrorDialog("Error", "La somma dei ricavi inseriti è diversa dall\'offerta, modificare i dati.", 'OK');
			//return;
		}
		if (foundset.bo_probability < 90) {
			errorResult = true;
			errorMessage = errorMessage + '\n- modificare la percentuale di probabilità al 90%';
			application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits: Nuova NON gestionale percentuale inferiore al 90%', LOGGINGLEVEL.ERROR);
			//globals.DIALOGS.showErrorDialog("Error", "Non è possibile proseguire con la richiesta, modificare la percentule di probabilità al 90%.", 'OK');
			//return;
		}
	}
	//BO che si collega a commessa esistente non gestionale per la prima volta
	if (isNew == false && foundset.job_order_type_id == null && job_orders != null && job_orders.job_order_type != 4) {
		//presenza BC
		if (globals.hasBusinessCase(foundset.bo_id) == false) {
			errorResult = true;
			errorMessage = errorMessage + '\n- non è stato inserito un Business Case valido';
			application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits: Link NON gestionale BC non valido', LOGGINGLEVEL.ERROR);
			foundset.bo_to_bo_details.loadAllRecords();
			foundset.bo_to_bo_planning.loadAllRecords();
			//globals.DIALOGS.showErrorDialog("Error", "Non è possibile proseguire con la richiesta, non è stato inserito un Business Case valido.", 'OK');
			//return;
		}
		//offerta = ricavi
		if ( (foundset.last_order_amount == 1 && foundset.order_amount != foundset.profit) || (foundset.last_order_amount != 1 && foundset.offer_amount != foundset.profit)) {
			errorResult = true;
			errorMessage = errorMessage + '\n- la somma dei ricavi inseriti è diversa dall\''+((foundset.last_order_amount == 1) ? 'ordine' : 'offerta');
			application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits: Link NON gestionale somma dei ricavi diversa dall\'offerta o dall\'ordine', LOGGINGLEVEL.ERROR);
			//globals.DIALOGS.showErrorDialog("Error", "La somma dei ricavi inseriti è diversa dall\'offerta, modificare i dati.", 'OK');
			//return;
		}
		if (foundset.bo_probability < 90) {
			errorResult = true;
			errorMessage = errorMessage + '\n- modificare la percentule di probabilità al 90%';
			application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits: Link NON gestionale percentuale inferiore al 90%', LOGGINGLEVEL.ERROR);
			//globals.DIALOGS.showErrorDialog("Error", "Non è possibile proseguire con la richiesta, modificare la percentule di probabilità al 90%.", 'OK');
			//return;
		}
	}
	//nuova richiesta di commessa gestionale
	if (isNew == true && foundset.job_order_type_id == 4) {
		if (globals.isEmpty(foundset.offer_amount) || foundset.offer_amount == 0) {
			application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits: Nuova gestionale valore offerta mancante', LOGGINGLEVEL.ERROR);
			globals.DIALOGS.showErrorDialog('Error', 'Non è possibile proseguire con la richiesta, non è presente il valore dell\'offerta', 'OK');
			return;
		}
		//commentato senza BC non può cambiare la probabilità, ma se chiede gestionale non è obbligatorio il BC
		//		if (foundset.bo_probability < 90){
		//			errorResult = true;
		//			errorMessage = errorMessage+ '\n- modificare la percentule di probabilità al 90%';
		//			application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits: Non è possibile proseguire con la richiesta: modificare la percentule di probabilità al 90%', LOGGINGLEVEL.ERROR);
		//			//globals.DIALOGS.showErrorDialog("Error", "Non è possibile proseguire con la richiesta, modificare la percentule di probabilità al 90%.", 'OK');
		//			//return;
		//		}
	}
	//BO gestionale cambio link commessa
	if (isNew == false && foundset.job_order_type_id == 4) {
		if ( (foundset.last_order_amount != 1 && (globals.isEmpty(foundset.offer_amount) || foundset.offer_amount == 0)) || (foundset.last_order_amount == 1 && (globals.isEmpty(foundset.order_amount) || foundset.order_amount == 0))) {
			application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits: Link gestionale valore offerta/ordine mancante', LOGGINGLEVEL.ERROR);
			globals.DIALOGS.showErrorDialog('Error', 'Non è possibile proseguire con la richiesta, non è presente il valore dell\'offerta o dell\'ordine', 'OK');
			return;
		}
		//controllo BC ricavi
		if (!globals.hasBusinessCaseProfit(foundset.bo_id)) {
			errorResult = true;
			errorMessage = errorMessage + '\n- non sono stati indicati i ricavi';
			application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits: Link gestionale ricavi mancanti', LOGGINGLEVEL.ERROR);
			//globals.DIALOGS.showErrorDialog("Error", "Non è possibile proseguire con la richiesta, non sono stati inseriti i ricavi.", 'OK');
			//return;
		}
		//controllo ordinato = ricavi
		if (foundset.order_amount != foundset.profit) {
			errorResult = true;
			errorMessage = errorMessage + '\n- la somma dei ricavi inseriti è diversa dall\'ordinato';
			application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits: Link gestionale somma ricavi diversa dall\'ordinato', LOGGINGLEVEL.ERROR);
			//globals.DIALOGS.showErrorDialog("Error", "La somma dei ricavi inseriti è diversa dall\'ordinato, modificare i dati.", 'OK');
			//return;
		}
		if (foundset.bo_probability < 90) {
			errorResult = true;
			errorMessage = errorMessage + '\n- modificare la percentule di probabilità al 90%';
			application.output(globals.messageLog + 'Error dialog_bo_link.saveEdits:  Link gestionale probabilità inferiore al 90%', LOGGINGLEVEL.ERROR);
			//globals.DIALOGS.showErrorDialog("Error", "Non è possibile proseguire con la richiesta, modificare la percentule di probabilità al 90%.", 'OK');
			//return;
		}
	}

	if (errorResult) {
		globals.DIALOGS.showErrorDialog("Error", errorMessage, 'OK');
		application.output(globals.messageLog + 'Error ' + errorMessage, LOGGINGLEVEL.ERROR );
		foundset.bo_to_bo_details.loadAllRecords();
		foundset.bo_to_bo_planning.loadAllRecords();
		return
	}

	if (!isNew) {
		if (job_id != null) {
			//INSERIMENTO STORICO
			var query = 'call sp_insert_history_jo(?,?,?,?,?,?)';
			var args = [job_id, scopes.globals.currentUserId, 'before link bo to jo by user ' + scopes.globals.currentUserDisplayName, 0, 0, 1];
			//var dataset =
			databaseManager.getDataSetByQuery('geco', query, args, -1);
			//var lastVersionID = dataset.getValue(1, 1);
			application.output(globals.messageLog + 'dialog_bo_link.saveEdits ultima version inserita', LOGGINGLEVEL.DEBUG);
			//FINE
		}
		mail_type = 14;
		foundset.job_order_id = job_id;
		foundset.is_linked_to_job_order = 1;
		foundset.job_order_required = 0;

		if (job_orders.find()) {
			job_orders.job_order_id = job_id;
			job_orders.search();
		}
		foundset.required_jo_date = new Date();
		foundset.lob_id = job_orders.lob_id;
		foundset.tow_id = job_orders.tow_id;
		foundset.rdc = job_orders.user_owner_id;
		foundset.cod_commessa = job_orders.external_code_navision;
		foundset.is_editable = 0;
		foundset.job_order_type_id = job_orders.job_order_type;
		foundset.reject_reason = null;
		save = _super.saveEdits(event);
		application.output(globals.messageLog + 'dialog_bo_link.saveEdits: prima della mail salvata link a commessa esistente ' + save, LOGGINGLEVEL.DEBUG);
	} else {
		mail_type = 13;
		foundset.required_jo_date = new Date();
		foundset.job_order_required = 1;
		foundset.is_linked_to_job_order = 0;
		foundset.is_editable = 0;
		foundset.bo_probability = 90;
		foundset.reject_reason = null;
		save = _super.saveEdits(event);
		application.output(globals.messageLog + 'dialog_bo_link.saveEdits: prima della mail salvata nuova commessa ' + save, LOGGINGLEVEL.DEBUG);
	}
	var send = forms.bo_tab_details.requireJobOrder(event, mail_type);
	//	var send = false;
	application.output(globals.messageLog + 'dialog_bo_link.saveEdits: mail inviata ' + send, LOGGINGLEVEL.DEBUG);
	if (send) {
		if (!isNew) {
			try {

				foundset.bo_to_bo_details.loadRecords();
				var bo_details = foundset.bo_to_bo_details;
//				application.output(bo_details)
				/** @type {Number[]} */
				var arrayStaus = [0, 1, 6, 7, 9];
				if (job_orders.getSize() == 1) {

					var newEndJO = null;
					newEndJO = job_orders.valid_to;
					if (job_orders.valid_to != null) {
						if (foundset.end_date != null && foundset.end_date > job_orders.valid_to) newEndJO = foundset.end_date;
						else newEndJO = job_orders.valid_to;
					} else if (foundset.end_date != null) newEndJO = foundset.end_date;
					job_orders.valid_to = newEndJO;
					//modifica di costo e ricavo budget della commessa
					job_orders.profit = scopes.globals.roundNumberWithDecimal( (job_orders.profit + foundset.profit),2);
					job_orders.cost = scopes.globals.roundNumberWithDecimal( (job_orders.cost + foundset.cost),2);
					
					application.output(globals.messageLog + 'dialog_bo_link.saveEdits: Commessa profit ' + job_orders.profit + ' cost ' + job_orders.cost, LOGGINGLEVEL.DEBUG);
					/** @type {JSFoundSet<db:/geco/jo_details>} */
					var jobDetails = job_orders.job_orders_to_jo_details;

					/** @type {JSFoundSet<db:/geco/bo_planning>} */
					var listBoPlain = null;
					/** @type {JSFoundSet<db:/geco/jo_planning>} */
					var listJobPlain = null;

					var recBoPlain = null;
					var dateBoPl = null;
					var pl = 0;

					var totJOPlain = 0;
					var totAmountBOPl = 0;
					var totReturnBOPl = 0;
					var totCostBOPl = 0;
					var totDayNumBOPl = 0;

					/** @type {JSRecord<db:/geco/bo_details>[]} */
					var detailsNew = [];
					/** @type {JSRecord<db:/geco/bo_details>[]} */
					var detailsUpdate = [];
					/** @type {JSRecord<db:/geco/jo_details>[]} */
					var detailsUpdateJO = [];

					/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
					var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
					bo_actual_date.loadRecords();
					var dateActual = bo_actual_date.getRecord(1).actual_date;
					//					var monthActual = bo_actual_date.getRecord(1).actual_month;
					//					var yearActual = bo_actual_date.getRecord(1).actual_year;
					var isOpenActual = bo_actual_date.getRecord(1).is_open_to_actual;
					var statusDateActual = bo_actual_date.getRecord(1).month_status_actual;

					//per ogni dettaglio della BO
					for (var index = 1; index <= bo_details.getSize(); index++) {
						//record
						var recordBoDet = bo_details.getRecord(index);
						//pianificazione del dettaglio
						application.output('------\nrecord dettaglio ' + recordBoDet)
						var tot = 0;
						//cerco il dettaglio della Commessa
						jobDetails.loadRecords();
						application.output('--------\njodetails ' + jobDetails)
						application.output('cerco tipo ' + recordBoDet.profit_cost_type_id + ' real_figure ' + recordBoDet.real_figure + ' real_tk_supplier ' + recordBoDet.real_tk_supplier + ' real_tm_figure ' + recordBoDet.real_tm_figure + ' figure ' + recordBoDet.figure + ' days_import ' + recordBoDet.days_import)
						if (jobDetails.find()) {
							jobDetails.jo_id = job_id;
							jobDetails.profit_cost_type_id = recordBoDet.profit_cost_type_id;
							if (recordBoDet.bo_details_to_profit_cost_types.profit_cost_acr == 'C') {
								jobDetails.real_figure = recordBoDet.real_figure;
								jobDetails.real_tk_supplier = recordBoDet.real_tk_supplier;
								jobDetails.real_tm_figure = recordBoDet.real_tm_figure;
								//jobDetails.days_import = recordBoDet.days_import;
								jobDetails.enrollment_number = recordBoDet.enrollment_number;
							} else {
								jobDetails.description = recordBoDet.figure;
							}
							tot = jobDetails.search();
						}
						if (tot == 1) {
							detailsUpdate.push(recordBoDet);
							detailsUpdateJO.push(jobDetails.getRecord(1))
						} else if (tot == 0) {
							detailsNew.push(recordBoDet);
						}
					}
					application.output('BO DETAILS TO UPDATE ' + detailsUpdate);
					application.output('JO DETAILS TO UPDATE ' + detailsUpdateJO);
					application.output('BO DETAILS TO NEW ' + detailsNew);
					application.output('--------------------------------------DETTAGLIO ESISTENTE----------------------------------------------');
					jobDetails.loadRecords();
					for (var idx = 0; idx < detailsUpdate.length; idx++) {
						/** @type {JSRecord<db:/geco/bo_details>} */
						var recBoDet = detailsUpdate[idx];
						/** @type {JSRecord<db:/geco/jo_details>} */
						var jobDetailsToUpdate = detailsUpdateJO[idx];
						application.output('DETTAGLIO BO ID ' + recBoDet.bo_details_id)
						application.output('###############dettagli trovati ' + tot + ':  ' + jobDetailsToUpdate)
						//if (tot == 1) {
						//esiste solo un dettaglio lo modifico
						application.output('-----------Esiste un dettaglio lo modifico ' + jobDetailsToUpdate.jo_details_id);
						jobDetailsToUpdate.cost_amount = jobDetailsToUpdate.cost_amount + recBoDet.cost_amount;
						jobDetailsToUpdate.return_amount = jobDetailsToUpdate.return_amount + recBoDet.return_amount;
						jobDetailsToUpdate.total_amount = jobDetailsToUpdate.total_amount + recBoDet.total_amount;
						jobDetailsToUpdate.days_number = jobDetailsToUpdate.days_number + recBoDet.days_number;

						//PIANIFICAZIONE MENSILE
						listBoPlain = recBoDet.bo_details_to_bo_planning;
						listJobPlain = jobDetailsToUpdate.jo_details_to_jo_planning;

						//					var totJOPlain = 0;
						//					var totAmountBOPl = 0;
						//					var totReturnBOPl = 0;
						//					var totCostBOPl = 0;
						//					var totDayNumBOPl = 0;
						application.output('Lista piani size ' + listBoPlain.getSize());
						application.output('Dettaglio JO ' + jobDetailsToUpdate.jo_details_id)
						for (pl = 1; pl <= listBoPlain.getSize(); pl++) {
							application.output('*****Pianificazione mensile ' + pl);
							recBoPlain = listBoPlain.getRecord(pl);
							application.output('recBoPlain ' + recBoPlain.bo_month + ' ' + recBoPlain.bo_year)
							dateBoPl = new Date(recBoPlain.bo_year, recBoPlain.bo_month, 0);
							application.output('Data Piano ' + dateBoPl + ' recBoPlain.bo_year ' + recBoPlain.bo_year + ' recBoPlain.bo_month ' + recBoPlain.bo_month);
							application.output('Data actual ' + dateActual);
							if (dateBoPl < dateActual || (dateBoPl == dateActual && isOpenActual == 0 && statusDateActual != 1)) {
								application.output('Piano minore della data actual o mese chiuso per actual o mese non ancora aperto');
								//piano precedente all'ultima data actual
								//sommo gli importi per poi copiarli nel primo piano utile
								totAmountBOPl = scopes.globals.roundNumberWithDecimal( (totAmountBOPl + recBoPlain.total_amount), 2);
								totReturnBOPl = scopes.globals.roundNumberWithDecimal( (totReturnBOPl + recBoPlain.return_amount), 2);
								totCostBOPl = scopes.globals.roundNumberWithDecimal( (totCostBOPl + recBoPlain.cost_amount), 2);
								totDayNumBOPl = scopes.globals.roundNumberWithDecimal( (totDayNumBOPl + recBoPlain.days_number), 1);
								application.output('somme totAmountBOPl ' + totAmountBOPl + ' totReturnBOPl ' + totReturnBOPl + ' totCostBOPl ' + totCostBOPl + ' totDayNumBOPl ' + totDayNumBOPl);
							} else {
								application.output('Data valida ');
								totAmountBOPl = scopes.globals.roundNumberWithDecimal( (totAmountBOPl + recBoPlain.total_amount), 2);
								totReturnBOPl = scopes.globals.roundNumberWithDecimal( (totReturnBOPl + recBoPlain.return_amount), 2);
								totCostBOPl = scopes.globals.roundNumberWithDecimal( (totCostBOPl + recBoPlain.cost_amount), 2);
								totDayNumBOPl = scopes.globals.roundNumberWithDecimal( (totDayNumBOPl + recBoPlain.days_number), 2);
								application.output('somme totAmountBOPl ' + totAmountBOPl + ' totReturnBOPl ' + totReturnBOPl + ' totCostBOPl ' + totCostBOPl + ' totDayNumBOPl ' + totDayNumBOPl);
								//piano bo dopo data actual
								//cercare la pianificazione eventualmente aggiungerla
								if (listJobPlain != null && listJobPlain.find()) {
									listJobPlain.jo_id = job_id;
									listJobPlain.jo_details_id = jobDetails.jo_details_id;
									listJobPlain.jo_month = recBoPlain.bo_month;
									listJobPlain.jo_year = recBoPlain.bo_year;
									//listJobPlain.is_actual = 0;
									//0 inserito da rdc, 1 confermato da rdc, 6 inserito da rpc, 7 respinto da rpc,9 rifiutato da controller
									//listJobPlain.status = '[0,1,6,7,9]'
									totJOPlain = listJobPlain.search();
								}
								if (totJOPlain == 0) {
									//non ho trovato il piano lo creo
									application.output('Nessun piano per il mese lo creo ')
									listJobPlain.newRecord();
									listJobPlain.jo_id = jobDetails.jo_id;
									listJobPlain.jo_details_id = jobDetails.jo_details_id
									listJobPlain.jo_month = recBoPlain.bo_month;
									listJobPlain.jo_year = recBoPlain.bo_year;
									listJobPlain.bo_id = recBoPlain.bo_id;
									listJobPlain.bo_details_id = recBoPlain.bo_details_id;
									listJobPlain.return_amount = totReturnBOPl;
									listJobPlain.cost_amount = totCostBOPl;
									listJobPlain.days_import = recBoPlain.days_import;
									listJobPlain.days_number = totDayNumBOPl;
									listJobPlain.total_amount = totAmountBOPl;
									listJobPlain.user_id = jobDetails.user_id;
									totAmountBOPl = 0;
									totReturnBOPl = 0;
									totCostBOPl = 0;
									totDayNumBOPl = 0;
								} else {
									//trovato lo modifico
									if (listJobPlain.is_actual == 1 || arrayStaus.indexOf(listJobPlain.status) != -1) {
										//il piano esiste ed è actual non lo modifico
										application.output('Piano trovato ACTUAL non posso modificarlo ')
									} else {
										application.output('Piano trovato lo modifico ')
										listJobPlain.return_amount = scopes.globals.roundNumberWithDecimal( (listJobPlain.return_amount + totReturnBOPl), 2);
										listJobPlain.cost_amount = scopes.globals.roundNumberWithDecimal( (listJobPlain.cost_amount + totCostBOPl), 2);
										listJobPlain.days_import = recBoPlain.days_import;
										listJobPlain.days_number = scopes.globals.roundNumberWithDecimal( (listJobPlain.days_number + totDayNumBOPl), 2);
										listJobPlain.total_amount = scopes.globals.roundNumberWithDecimal( (listJobPlain.total_amount + totAmountBOPl), 2);
										application.output('somme Amount ' + listJobPlain.total_amount + ' Return ' + totReturnBOPl + ' Costo ' + listJobPlain.cost_amount + ' NumGiorni ' + listJobPlain.days_number);
										totAmountBOPl = 0;
										totReturnBOPl = 0;
										totCostBOPl = 0;
										totDayNumBOPl = 0;
									}
								}//fine trovato piano mensile JO

							}
						}
					}// fine for record to update
					application.output('--------------------------------------NUOVO DETTAGLIO----------------------------------------------')
					for (var idxNew = 0; idxNew < detailsNew.length; idxNew++) {
						/** @type {JSRecord<db:/geco/bo_details>} */
						var recBoDetNew = detailsNew[idxNew];

						//non esiste nessun dettaglio lo creo
						application.output("-----------Non esiste il dettaglio lo creo ID DET BO " + recBoDetNew.bo_details_id);
						jobDetails.newRecord();
						jobDetails.profit_cost_type_id = recBoDetNew.profit_cost_type_id;
						jobDetails.bo_id = recBoDetNew.bo_id;
						jobDetails.bo_details_id = recBoDetNew.bo_details_id;
						jobDetails.jo_id = job_orders.job_order_id;
						jobDetails.cost_amount = recBoDetNew.cost_amount;
						jobDetails.total_amount = recBoDetNew.total_amount;
						jobDetails.return_amount = recBoDetNew.return_amount;
						if (recBoDetNew.bo_details_to_profit_cost_types.profit_cost_acr == 'C') {
							jobDetails.real_figure = recBoDetNew.real_figure;
							jobDetails.real_tk_supplier = recBoDetNew.real_tk_supplier;
							jobDetails.real_tm_figure = recBoDetNew.real_tm_figure;
							jobDetails.description = recBoDetNew.figure;
							jobDetails.days_import = recBoDetNew.days_import;
							jobDetails.days_number = recBoDetNew.days_number;
							jobDetails.standard_figure = recBoDetNew.standard_figure;
							jobDetails.standard_import = recBoDetNew.standard_import;
							jobDetails.enrollment_number = recBoDetNew.enrollment_number;
						}
						databaseManager.saveData(jobDetails);
						application.output('Nuovo dettaglio JO ' + jobDetails.jo_details_id + ' commessa ' + jobDetails.jo_id)
						listBoPlain = recBoDetNew.bo_details_to_bo_planning;
						application.output('Lista piani size ' + listBoPlain.getSize())
						for (pl = 1; pl <= listBoPlain.getSize(); pl++) {
							application.output('*****Pianificazione mensile ' + pl);
							recBoPlain = listBoPlain.getRecord(pl);
							application.output('recBoPlain ' + recBoPlain.bo_month + ' ' + recBoPlain.bo_year)
							dateBoPl = new Date(recBoPlain.bo_year, recBoPlain.bo_month, 0);
							application.output('Data Piano ' + dateBoPl + ' recBoPlain.bo_year ' + recBoPlain.bo_year + ' recBoPlain.bo_month ' + recBoPlain.bo_month);
							application.output('Data actual ' + dateActual);
							if (dateBoPl < dateActual || (dateBoPl == dateActual && isOpenActual == 0 && statusDateActual != 1)) {
								//piano precedente all'ultima data actual
								//sommo gli importi per poi copiarli nel primo piano utile
								application.output('Piano minore della data actual ');
								totAmountBOPl = scopes.globals.roundNumberWithDecimal( (totAmountBOPl + recBoPlain.total_amount), 2);
								totReturnBOPl = scopes.globals.roundNumberWithDecimal( (totReturnBOPl + recBoPlain.return_amount), 2);
								totCostBOPl = scopes.globals.roundNumberWithDecimal( (totCostBOPl + recBoPlain.cost_amount), 2);
								totDayNumBOPl = scopes.globals.roundNumberWithDecimal( (totDayNumBOPl + recBoPlain.days_number), 2);
								application.output('somme totAmountBOPl ' + totAmountBOPl + ' totReturnBOPl ' + totReturnBOPl + ' totCostBOPl ' + totCostBOPl + ' totDayNumBOPl ' + totDayNumBOPl);
							} else {
								application.output('Data valida ');
								totAmountBOPl = scopes.globals.roundNumberWithDecimal( (totAmountBOPl + recBoPlain.total_amount), 2);
								totReturnBOPl = scopes.globals.roundNumberWithDecimal( (totReturnBOPl + recBoPlain.return_amount), 2);
								totCostBOPl = scopes.globals.roundNumberWithDecimal( (totCostBOPl + recBoPlain.cost_amount), 2);
								totDayNumBOPl = scopes.globals.roundNumberWithDecimal( (totDayNumBOPl + recBoPlain.days_number), 2);
								application.output('somme totAmountBOPl ' + totAmountBOPl + ' totReturnBOPl ' + totReturnBOPl + ' totCostBOPl ' + totCostBOPl + ' totDayNumBOPl ' + totDayNumBOPl);

								//piano bo dopo data actual
								//cercare la pianificazione eventualmente aggiungerla
								//non ho trovato il piano lo creo
								application.output('Nuovo dettaglio JO ' + jobDetails.jo_details_id + ' commessa ' + jobDetails.jo_id)
								var idxJP = jobDetails.jo_details_to_jo_planning.newRecord();
								application.output('indice piano creato ' + idxJP)
								jobDetails.jo_details_to_jo_planning.jo_id = jobDetails.jo_id;
								jobDetails.jo_details_to_jo_planning.jo_details_id = jobDetails.jo_details_id
								jobDetails.jo_details_to_jo_planning.jo_month = recBoPlain.bo_month;
								jobDetails.jo_details_to_jo_planning.jo_year = recBoPlain.bo_year;
								jobDetails.jo_details_to_jo_planning.bo_id = recBoPlain.bo_id;
								jobDetails.jo_details_to_jo_planning.bo_details_id = recBoPlain.bo_details_id;
								jobDetails.jo_details_to_jo_planning.return_amount = totReturnBOPl;
								jobDetails.jo_details_to_jo_planning.cost_amount = totCostBOPl;
								jobDetails.jo_details_to_jo_planning.days_import = recBoPlain.days_import;
								jobDetails.jo_details_to_jo_planning.days_number = totDayNumBOPl;
								jobDetails.jo_details_to_jo_planning.total_amount = totAmountBOPl;
								jobDetails.jo_details_to_jo_planning.user_id = jobDetails.user_id;
								totAmountBOPl = 0;
								totReturnBOPl = 0;
								totCostBOPl = 0;
								totDayNumBOPl = 0;
							}
						}
					}//fine for New plain
					application.output(globals.messageLog + 'dialog_bo_link.saveEdits: Commessa nuovo profit ' + job_orders.profit + ' nuovo cost ' + job_orders.cost, LOGGINGLEVEL.DEBUG);
					save = _super.saveEdits(event);
					application.output(globals.messageLog + 'dialog_bo_link.saveEdits: Link commessa modifica foundset ' + save, LOGGINGLEVEL.DEBUG);
					job_orders.job_orders_to_jo_details.loadRecords();
					job_orders.job_orders_to_jo_planning.loadRecords();
					job_orders.job_orders_to_jo_details.jo_details_to_jo_planning.loadRecords();
				} // fine trovata 1 commessa
			} catch (e) {
				application.output(globals.messageLog + 'ERROR dialog_bo_link.saveEdits ' + e.message, LOGGINGLEVEL.ERROR);
			}

		} else {
			//la creazione dei dettagli e piani per il job order lo farà dopo che il controller ha creato la commessa
		}
		stopEditing(event);
	} else {
		//MAIL NON INVIATA
		application.output('ERRORE INVIO MAIL  ' + send);
		foundset.job_order_id = null;
		foundset.is_linked_to_job_order = null;
		foundset.cod_commessa = null;
		foundset.required_jo_date = null;
		foundset.lob_id = null;
		foundset.tow_id = null;
		foundset.rdc = null;
		foundset.job_order_type_id = null;
		foundset.is_editable = 1;
		foundset.job_order_required = null;
		foundset.reject_reason = null;
		save = _super.saveEdits(event);
		globals.DIALOGS.showInfoDialog("Info", "Errore invio mail, richiesta non effettuata.", 'OK');
	}
	foundset.bo_to_bo_details.loadAllRecords();
	foundset.bo_to_bo_planning.loadAllRecords();
	application.output(globals.messageLog + 'STOP dialog_bo_link.saveEdits ', LOGGINGLEVEL.INFO);
}
/**
 * @param event
 *
 * @properties={typeid:24,uuid:"83B8B700-370D-4C43-8D1E-F971DA2F89D7"}
 */
function updateUI(event) {
	controller.readOnly = !isEditing();

	// buttons
	elements.buttonEdit.visible = !isEditing();
	elements.buttonSave.visible = isEditing();
	elements.buttonCancel.visible = isEditing();

	elements.g_link.visible = !isNew;
	elements.g_new.visible = isNew;

}
/**
 * Handle changed data.
 *
 * @param oldValue old value
 * @param newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"62C5806B-F872-4BAB-9437-5F40A5CE7DB6"}
 * @AllowToRunInFind
 */
function onDataChange(oldValue, newValue, event) {
	application.output('commessa ' + newValue + ' ' + job_id);
	var cost_pre = 0;
	var return_pre = 0;
	foundset.bo_to_bo_details.loadRecords();
	if (newValue != null) {
		//cerca la pianificazione esistente della commessa scelta
		/** @type {JSFoundSet<db:/geco/jo_details>} */
		var jo_details = databaseManager.getFoundSet('geco', 'jo_details');
		jo_details.loadAllRecords();
		//application.output(jo_details)
		if (jo_details.find()) {
			jo_details.jo_id = newValue;
			//trova dei dettagli di costo e ricavo e la commessa non è di tipo gestionale
			if (jo_details.search() > 0 && jo_details.jo_details_to_job_orders.job_order_type != 4) {

				return_pre = jo_details.total_return_d;
				cost_pre = jo_details.total_cost_d;

				var cost_post = (cost_pre ? cost_pre : 0) + (foundset.bo_to_bo_details.total_cost ? foundset.bo_to_bo_details.total_cost : 0);
				var return_post = (return_pre ? return_pre : 0) + (foundset.bo_to_bo_details.total_return ? foundset.bo_to_bo_details.total_return : 0);

				message = 'Hai scelto di associare la BO alla commessa ' + jo_details.jo_details_to_job_orders.external_code_navision + ',\nquesto varierà i ricavi e i costi totali: \n\n\tRICAVI : ' + return_pre + ' ---> ' + return_post + '\n\tCOSTI : ' + cost_pre + ' ---> ' + cost_post
			} 
			else 
				message = 'La BO verrà associata alla commessa';
		}
	}
	return true
}

/**
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"06B2FD20-A19B-4A39-86C3-2A90646D1342"}
 */
function checkInputValues() {
	if (isNew) {
		if (foundset.rdc == null || foundset.job_order_type_id == null || foundset.lob_id == null || foundset.tow_id == null) {
			return false;
		}
	} else {
		if (job_id == null) {
			return false;
		}
	}
	return true;
}
