/**
 * @type {String}
 * @properties={typeid:35,uuid:"2A9E7FE4-662A-4B7A-93F3-2BAAB4D654D2"}
 */
var document_path = '';

/**
 * @type {String}
 * @properties={typeid:35,uuid:"FABA8AA8-9371-4A33-9E22-72C8F761C2E3"}
 */
var document_name = '';

/**
 * @properties={typeid:35,uuid:"2E15A8C0-AC6A-42D5-8997-4DDF5198A8B4",variableType:-4}
 */
var client = null;

/**
 * @param event
 *
 * @properties={typeid:24,uuid:"0ECBBA9B-3630-4170-9D6C-A60100579B35"}
 */
function updateUI(event) {

	_super.updateUI(event);
	if (!scopes.globals.hasRole('Supp. Commerciale')) {
		if (foundset && foundset.bo_to_bo_details) foundset.bo_to_bo_details.loadRecords(); 
	}
	elements.labelJobRequired.visible = foundset && (foundset.job_order_id == null && foundset.job_order_required == 1 && foundset.reject_reason == null);
	elements.buttonNote.visible = foundset && foundset.reject_reason != null;
	var isController = globals.hasRole('Controllers');
	var isOrderAdmin = globals.hasRole('Orders Admin');
	var isPlanner = globals.hasRole('Planner');
	var isSteering = globals.hasRole('Steering Board');
	var isMarketManager = globals.hasRole('Resp. Mercato');
	var isCommercialSupport = globals.hasRole('Supp. Commerciale');
	var isVice = globals.hasRole('Vice Resp. Mercato');
	
	var is_supp = false;
	if (foundset.profit_center_id != null ) {
		
		for (var i = 1; i <= currentuserid_to_comm_supp_pc.getSize();i++){
			var pc = currentuserid_to_comm_supp_pc.getRecord(i);
			if (pc.profit_center_id == foundset.profit_center_id) {
				is_supp = true;
				break;
			}
		}
	}
	
	elements.ProfitCenterController.visible = isController || isPlanner || isSteering || isOrderAdmin;
	elements.ProfitCenter.visible = !isController && !isPlanner && !isSteering && !isOrderAdmin && (!isCommercialSupport || isMarketManager || isVice);
	elements.probabiltyController.visible = !isEditing() || (isController || isOrderAdmin)

	//aggiunto Supp. Commerciale
	elements.ProfitCenterSuppComm.visible = isCommercialSupport && !isMarketManager && !isVice ;
	elements.customer.visible = !is_supp && ( isMarketManager || isVice || isController || isPlanner || isSteering || isOrderAdmin);
	elements.finalCustomer.visible = !is_supp && ( isMarketManager || isVice || isController || isPlanner || isSteering || isOrderAdmin);
	elements.customerSuppComm.visible = is_supp // && ( !isMarketManager && !isVice) ;
	elements.finalCustomerSuppComm.visible = is_supp // && ( !isMarketManager && !isVice) ;
	elements.lbl1.visible = isCommercialSupport && !isMarketManager && !isVice  && profit_center_id == null;
	elements.lbl2.visible = isCommercialSupport && !isMarketManager && !isVice  && profit_center_id == null;
	elements.lbl3.visible = isCommercialSupport && !isMarketManager && !isVice  && profit_center_id == null;

	if (isEditing() && (isMarketManager || isCommercialSupport || isVice)) {
		elements.ProfitCenterController.visible = false;
		elements.ProfitCenter.visible = true;
		elements.probabiltyController.visible = false;
		elements.probabilty.visible = true;
	}

	var canModify = globals.isOwnerForObject(scopes.globals.currentUserId, globals.bo_selected, 'bo') || globals.isPCManagerForObject(scopes.globals.currentUserId, globals.bo_selected, 'bo') || isController || isOrderAdmin || is_supp;
	var isEditable = foundset && foundset.is_editable == 1;
	var hasJobOrder = foundset && (foundset.job_order_id != null || foundset.job_order_required == 1);
	var okPercent = true//(foundset && foundset.bo_probability >= 90);
	//application.output('canModify ' + canModify + ' isEditable ' + isEditable + ' hasJobOrder ' + hasJobOrder + ' isController  ' + isController);
	elements.buttonNewJobOrder.visible = !isController && !isOrderAdmin && canModify && !isEditing() && !hasJobOrder && okPercent && (!isCommercialSupport || (isCommercialSupport && !is_supp));
	elements.buttonLinkJobOrder.visible = !isController && !isOrderAdmin && canModify && !isEditing() && !hasJobOrder && okPercent && (!isCommercialSupport || (isCommercialSupport && !is_supp));

	//elements.buttonEdit.visible = !isEditing() && (isController || (canModify && (isEditable || !hasJobOrder)));
	elements.buttonEdit.visible = !isEditing() && canModify //&& (isController || (canModify && (isEditable || !hasJobOrder)));

	elements.showJo.enabled = !isEditing() && !is_supp;

	elements.buttonReject.visible = !isEditing() && isController && foundset && (foundset.job_order_id == null && foundset.job_order_required == 1);
	elements.isEnable.visible = isController;

	elements.tab_bo_orders.enabled = true;
	//job_order_id >=10000 sono commesse vecchie che non esistono su geco
	elements.showJo.visible = (foundset.job_order_id != null && job_order_id < 10000) && (!is_supp);
	elements.g_notEditable.enabled = isEditable || isController || isOrderAdmin;
	//Gruppo ad hoc per Supp. Comm
	elements.g_notEditableSuppComm.enabled = isEditable || isController || isOrderAdmin;
	elements.lastOrder.enabled = isEditing() && (isController || isOrderAdmin);
	//application.output(isEditable && ( isController || isOrderAdmin ))
	elements.l_extraOfferAmount.visible = !isEditable;
	elements.extraOfferAmount.visible = !isEditable;
	if (foundset && foundset.job_order_type_id == 4 && foundset.bo_to_job_orders && foundset.bo_to_job_orders.is_enabled == 0) {
		elements.buttonLinkJobOrder.visible = canModify && !isEditing();
	}

}

/**
 * @properties={typeid:35,uuid:"31D28273-0D40-4BD3-A71F-C4959B15DA29",variableType:-4}
 */
var mailSubstitution = {
	codiceBO: '',
	commessa: '',
	descrizione: '',
	lob: '',
	tow: '',
	area: '',
	direzione: '',
	rdc: '',
	tipo: '',
	cliente: '',
	cliente_finale: ''
};

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} type
 *
 * @properties={typeid:24,uuid:"D3444671-8222-46BF-B70D-BC1DF17EDBE9"}
 */
function requireJobOrder(event, type) {
	application.output(globals.messageLog + 'START bo_tab_details.requireJobOrder() ', LOGGINGLEVEL.INFO);
	var excel = null;
	var filePath = null;
	var fileName = null;
	//application.output('dettagli prima del BC ' + foundset.bo_to_bo_details);
	// se non è gestionale opppure gestionale con BC ricavo il BC
	if (foundset.job_order_type_id != 4 || (foundset.job_order_type_id == 4 && globals.hasBusinessCase(foundset.bo_id))) {
		foundset.bo_to_bo_details.loadAllRecords();
		//TODO se link a commessa esistente il BC deve essere la commessa + la nuova BO
		excel = globals.getBusinessCase(event, foundset.bo_id);

		/**@type {Array<byte>}*/
		var bytes = new Array();
		var dateReq = new Date();
		/** @type {String} */
		var monthStr = '' + (dateReq.getMonth() + 1);
		var dayStr = '' + dateReq.getDate()
		var dateReqStr = dateReq.getFullYear() + ( (monthStr.length > 1) ? monthStr : '0' + monthStr) + ( (dayStr.length > 1) ? dayStr : '0' + dayStr)
		var fileExcel = plugins.file.createFile('BC_' + foundset.bo_code + '_' + dateReqStr + '.xls');
		var result = plugins.file.appendToTXTFile(fileExcel, excel);
		application.output(globals.messageLog + 'bo_tab_details.requireJobOrder() fileExcel.getBytes() ' + fileExcel.getBytes(), LOGGINGLEVEL.DEBUG);
		bytes = fileExcel.getBytes();
		application.output(globals.messageLog + 'bo_tab_details.requireJobOrder() fileExcel.size() ' + fileExcel.size(), LOGGINGLEVEL.DEBUG);
		application.output(globals.messageLog + 'bo_tab_details.requireJobOrder() appendToTXTFile result ' + result, LOGGINGLEVEL.DEBUG);
		application.output(globals.messageLog + 'bo_tab_details.requireJobOrder() fileExcel.getContentType() ' + fileExcel.getContentType(), LOGGINGLEVEL.DEBUG);

		if (!plugins.file.writeFile(fileExcel, bytes, 'application/vnd.ms-excel'))
			application.output(globals.messageLog + 'ERROR bo_tab_details.requireJobOrder() Failed to write the file.', LOGGINGLEVEL.ERROR);
		application.output(globals.messageLog + 'bo_tab_details.requireJobOrder() fileExcel.getAbsolutePath() ' + fileExcel.getAbsolutePath(), LOGGINGLEVEL.DEBUG);
		filePath = fileExcel.getAbsolutePath();
		fileName = fileExcel.getName();

		globals.insertBCtoBO(event, globals.bo_selected, fileExcel);
		//		/** @type {JSFoundSet<db:/geco/bo_documents>} */
		//		var documents = databaseManager.getFoundSet('geco', 'bo_documents');
		//		documents.newRecord(false);
		//		documents.bo_document = bytes;
		//		documents.bo_document_size = fileExcel.size();
		//		documents.bo_document_original_name = fileName;
		//		documents.bo_document_mimetype = 'xls';
		//		//documents.bo_document_mimetype = fileSaved.getContentType();
		//		documents.bo_document_type = 1;
		//		var resultDoc = databaseManager.saveData(documents);
		//
		//		if (resultDoc) {
		//			/** @type {JSFoundSet<db:/geco/bo_bo_documents>} */
		//			var bo_bo_documents = databaseManager.getFoundSet('geco', 'bo_bo_documents');
		//			bo_bo_documents.newRecord(false);
		//			bo_bo_documents.bo_id = globals.bo_selected;
		//			bo_bo_documents.bo_document_id = documents.bo_document_id;
		//			bo_bo_documents.bo_bo_documents_details = 'Richiesta commessa';
		//			bo_bo_documents.is_eraseable = 0;
		//			databaseManager.saveData(bo_bo_documents);
		//		}
	}
	try {
		var objMail = globals.getMailType(type);

		var recipient = objMail.distrList;
		var ccRecipient = currentuserid_to_users.users_to_contacts.default_email.channel_value;
		//		application.output('CC mail prima ' + ccRecipient);
		//		application.output(foundset.bo_to_profit_centers.profit_centers_to_owner_users);
		for (var index = 1; index <= foundset.bo_to_profit_centers.profit_centers_to_owner_users.getSize(); index++) {
			var resp = foundset.bo_to_profit_centers.profit_centers_to_owner_users.getRecord(index);
			//			application.output(resp.user_id);
			ccRecipient = ccRecipient + ', ' + resp.users_to_contacts.default_email.channel_value;
		}

		//		application.output('CC mail ' + ccRecipient);
		var subject = objMail.subject;
		mailSubstitution.codiceBO = foundset.bo_code;
		if (type == 14)
			mailSubstitution.commessa = foundset.bo_to_job_orders.external_code_navision;
		if (type == 13) {
			mailSubstitution.commessa = foundset.bo_code;
			mailSubstitution.descrizione = foundset.bo_description;
			mailSubstitution.lob = foundset.bo_to_bo_lob.bo_lob_description;
			mailSubstitution.tow = foundset.bo_to_bo_tow.bo_tow_description;
			mailSubstitution.area = (foundset.geographical_area ? foundset.bo_to_bo_aree_geo.bo_area_geo_description : '');
			mailSubstitution.direzione = (foundset.bo_to_profit_centers.profit_center_name ? foundset.bo_to_profit_centers.profit_center_name : '');
			mailSubstitution.rdc = foundset.bo_rdc_to_users.users_to_contacts.real_name;
			mailSubstitution.tipo = foundset.bo_to_job_orders_types.job_order_type_description;
			mailSubstitution.cliente = (foundset.bo_to_companies.company_name ? foundset.bo_to_companies.company_name : '');
			mailSubstitution.cliente_finale = (foundset.bo_to_final_companies.company_name ? foundset.bo_to_final_companies.company_name : '');
		}
		//var message = 'prova invio bc';
		var message = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubstitution);
		//		application.output('dopo preparazione mail ' + foundset.bo_to_bo_details);
	} catch (error) {
		application.output(globals.messageLog + 'ERROR invio BC errore di sostituzione valori', LOGGINGLEVEL.ERROR);
		return false;
	}
	if (client == null || !client.isValid()) {
		application.output('client==null || !client.isValid() ricreo client');
		client = plugins.headlessclient.createClient("geco_queue", null, null, null);
	}
	if (client != null && client.isValid()) {
		application.output('Cient OK ' + client.getClientID());
		client.queueMethod(null, "sendMailAttach", [recipient, subject, message, ccRecipient, filePath, fileName], enqueueMailLog);
		excel = '';
		updateUI(event);
		application.output(globals.messageLog + 'STOP bo_tab_details.requireJobOrder() ', LOGGINGLEVEL.INFO);
		if (fileName != null)
			plugins.file.deleteFile(fileName);
		return true
	} else {
		excel = '';
		if (fileName != null)
			plugins.file.deleteFile(fileName);
		application.output(globals.messageLog + 'STOP bo_tab_details.requireJobOrder() Client is not valid', LOGGINGLEVEL.INFO);
		return false
	}

}

/**
 * @param {JSEvent} headlessCallback
 *
 * @properties={typeid:24,uuid:"00D4E39C-3799-471E-8111-7891655E7350"}
 */
function enqueueMailLog(headlessCallback) {
	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		application.output('-------------SENDING MAIL----------------------');
		application.output('timestamp: ' + headlessCallback.getTimestamp());
		application.output('recipient: ' + headlessCallback.data.recipient);
		application.output('subject: ' + headlessCallback.data.subject);
		application.output('message: ' + headlessCallback.data.message);
		application.output('success: ' + headlessCallback.data.success.toString().toUpperCase());
		application.output('-----------------------------------------------\n');
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output("exception callback, name: " + headlessCallback.data);
	}
	if (client != null && client.isValid()) {
		//application.output('chiudo client ' + client.getClientID());
		//		client.shutdown();
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} isNew
 * @private
 * @properties={typeid:24,uuid:"D827140A-F820-44D7-AD2F-970F157AB824"}
 */
function openLinkToJobOrder(event, isNew) {
	//	if (!globals.hasBusinessCase(foundset.bo_id)) {
	//		var message = 'Operazione non consentita:\nnon è presente un Business Case valido';
	//		globals.DIALOGS.showErrorDialog("Errore", message, "OK");
	//		return;
	//	}
	scopes.globals.selectedPC = foundset.profit_center_id;
	var win = application.createWindow("Link", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 515, 300);
	win.title = 'Link'
	win.show(forms.dialog_bo_link_jo);
	forms.dialog_bo_link_jo.isNew = isNew;
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"6BB418E6-50F0-4942-B718-DB8AFE420761"}
 */
function onDataChangeBU(oldValue, newValue, event) {
	foundset.profit_center_id = foundset.business_opportunities_to_bo_business_unit.profit_center_id;
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"0925D8FC-A8F8-4964-9DD3-AEC5E62188E0"}
 */
function onDataChangeProbability(oldValue, newValue, event) {
	// Controlli per poter cambiare la probabilità della BO
	//50% inserire BC --> valorizare almeno una voce di costo e una di ricavo
	//70% inserire i valori di importo offerta e data offerta --> se importo offerta diverso dal precedente alert di modifica BC
	//90% controllare i valori del BC e dell'importo offerta --> OK abilitaree richiesta commessa
	//100% no delivery o client ma controller almeno un ordine allegato

	return true
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"D18A2785-3C6A-47E2-9BE4-25E834732F9A"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
	//	scopes.globals.selectedPCCommSuppBoDetails = null;

	//SE E' NUOVO RECORD!!!!!
	if (foundset.getSelectedRecord() && foundset.getSelectedRecord().isNew()) {
		//		if (globals.hasRole('Client Manager')) {
		//			foundset.client_manager_id = globals.currentUserId;
		//		}
		if (globals.hasRole('Resp. Mercato')) {
			foundset.client_manager_id = globals.currentUserId;
			foundset.pc_owner_id = globals.currentUserId;
			if (foundset.bo_to_owner_markets.getSize() == 1) {
				foundset.profit_center_id = foundset.bo_to_owner_markets.profit_center_id;
			}
		} else if (globals.hasRole('Supp. Commerciale')) {
//			elements.ProfitCenterSuppComm.visible = true;
//			elements.customer.visible = false;
//			elements.customerSuppComm.visible = true;
//			elements.finalCustomerSuppComm.visible = true;
			elements.customerSuppComm.enabled = false;
			elements.finalCustomerSuppComm.enabled = false;
			elements.lbl1.visible = true;
			elements.lbl2.visible = true;
			elements.lbl3.visible = true;
		}
	}

	//RECORD GIA' ESISTENTE
	if (foundset.getSelectedRecord() && !foundset.getSelectedRecord().isNew() && globals.hasRole('Supp. Commerciale')) {
		elements.customerSuppComm.enabled = true;
		elements.finalCustomerSuppComm.enabled = true;
		elements.lbl1.visible = false;
		elements.lbl2.visible = false;
		elements.lbl3.visible = false;
	}
}

/**
 * Se il record è nuovo torna alla lista delle BO
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"58F0D32F-607F-4F1E-8438-4056187F69A7"}
 */
function stopEditing(event) {
	if (foundset.getSelectedRecord().isNew()) {
		_super.stopEditing(event);
		globals.showDetailsList(event, 'bo_sx', false, 'bo');
	}
	_super.stopEditing(event);
}

/**
 * Manda una mail al rdm dove si avvisa che la richiesta apertura commessa è stata respinta
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"73259573-256A-40A9-A8B1-FCEF28036608"}
 */
function rejectRequestNewJO(event) {
	application.output(globals.messageLog + 'START bo_tab_details.rejectRequestNewJO() ', LOGGINGLEVEL.INFO);

	startEditing(event);
	var rejectionReason = globals.DIALOGS.showInputDialog("Annulla", "Annullando la richiesta di nuova commessa la BO sarà modificabile.\nInserisci il motivo del rifiuto");
	application.output(" codice bo : "+bo_code+" Resp. mercato : "+foundset.bo_to_owner_markets.owners_markets_to_users.user_real_name +" mail rdm : "+foundset.bo_to_owner_markets.owners_markets_to_users.users_to_contacts.contacts_channels.channel_value);
	application.output(rejectionReason);
	if (rejectionReason != null && rejectionReason != '') {
		databaseManager.setAutoSave(true);
		foundset.reject_reason = rejectionReason;
		foundset.job_order_required = 0;
		foundset.is_editable = 1;
		var boTitle = foundset.bo_code + ' - ' + foundset.bo_description;
		//JS manda mail a resp mercato bo indicando il motivo del rifiuto.
		globals.notifyMailForRejectedBO(19,rejectionReason,boTitle,foundset.bo_to_owner_markets.owners_markets_to_users.users_to_contacts.contacts_channels.channel_value,foundset.bo_to_owner_markets.owners_markets_to_users.users_to_contacts.first_name);
		application.output(globals.messageLog + 'STOP bo_tab_details.rejectRequestNewJO() OK', LOGGINGLEVEL.INFO);
		saveEdits(event);
	} else {
		application.output(globals.messageLog + 'STOP bo_tab_details.rejectRequestNewJO() KO rejectReason non valorizzata', LOGGINGLEVEL.INFO);
		stopEditing(event);
	}

}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"DA6C3A3E-A680-40E2-8D50-E1308BEFC6C5"}
 */
function showNote(event) {
	globals.DIALOGS.showInfoDialog("Note", foundset.reject_reason, 'OK');
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"92166B98-C3BB-45B5-9F52-724FB966598D"}
 */
function goToSelect(event) {
	globals.jo_link_selected = foundset.job_order_id;
	forms.main_bo.loadPanel(event, 'jo_bundle');
	scopes.globals.showDetailsList(event, 'jo_details', true, 'jo')
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"D4AD238C-D864-4366-92C1-742F458DABD2"}
 * @AllowToRunInFind
 */
function onDataChangePC(oldValue, newValue, event) {
	scopes.globals.selectedPC = newValue;
	//	scopes.globals.selectedPCCommSuppBoDetails = newValue;
	var is_supp = false;
	if (newValue != null ) {
		
		for (var i = 1; i <= currentuserid_to_comm_supp_pc.getSize();i++){
			var pc = currentuserid_to_comm_supp_pc.getRecord(i);
			if (pc.profit_center_id == newValue) {
				is_supp = true;
				break;
			}
		}
		
		/** @type {JSFoundSet<db:/geco/comm_supp_profit_centers>} */
		var supp_pc = databaseManager.getFoundSet('geco', 'comm_supp_profit_centers');
		supp_pc.loadRecords();

		if (supp_pc.find()) {
			supp_pc.user_id = globals.currentUserId;
			supp_pc.profit_center_id = newValue;
			if (supp_pc.search()>0) is_supp = true;
		}
	}
	
	if (is_supp) {
		company_id = null;
		final_company_id = null;
	}
		elements.customerSuppComm.enabled = is_supp;
		elements.finalCustomerSuppComm.enabled = is_supp;
		elements.customerSuppComm.visible = is_supp;
		elements.finalCustomerSuppComm.visible = is_supp;
		elements.customer.enabled = !is_supp;
		elements.finalCustomer.enabled = !is_supp;
		elements.customer.visible = !is_supp;
		elements.finalCustomer.visible = !is_supp;
//		elements.lbl1.visible = !is_supp;
//		elements.lbl2.visible = !is_supp;
//		elements.lbl3.visible = !is_supp;
	
	return true
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"57E52147-1342-4C9C-A021-7EDFA4EC77DF"}
 */
function saveBO(event) {
	var isVice = globals.hasRole('Vice Resp. Mercato');
	var can_add = true;
	if (isVice) {
		for(var i= 1; i<= currentownerid_to_vice_owner_profit_center.getSize(); i++) {
			var rec = currentownerid_to_vice_owner_profit_center.getRecord(i);
			if (rec.profit_center_id == foundset.profit_center_id) {
				can_add = false;
				break;
			}
		}
	}
	if (foundset.getSelectedRecord().isNew() && !can_add){
		globals.DIALOGS.showErrorDialog("Errore", "Non puoi creare BO per mercati di cui sei ViceResponsabile", 'OK');
		return
	}
	_super.saveEdits(event);

}
