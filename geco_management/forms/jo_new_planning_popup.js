/**
 * @type {Number}
 * @properties={typeid:35,uuid:"3EC95506-E34D-4DE7-897E-8050EA9391F0",variableType:8}
 */
var standard_amount = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"FFC8A635-5C21-4A0C-9B8C-AC4AB40E5807",variableType:8}
 */
var real_amount_personnel = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"093E77EE-A9EA-4375-AAE7-87CFC41AF5B0",variableType:8}
 */
var day_number = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"26A7F445-4008-40EE-8E99-482F0239EA1E",variableType:8}
 */
var day_import = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"B24F62FC-0059-46C2-A08B-5422E8E1E3DE",variableType:8}
 */
var total_amount_value = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"DF07B447-BE24-4E7E-8030-620DBD6DCD33",variableType:8}
 */
var month_jo_planning = new Date().getMonth() + 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"CCA25B78-B72C-4929-918F-19C90BED5203",variableType:8}
 */
var year_jo_planning = new Date().getFullYear()

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"13917FD2-8ED0-4568-8F72-A8E9AC2F96F4",variableType:8}
 */
var detailsID = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"92F84CA7-69FD-487F-B81D-0BC853AE5E1B",variableType:8}
 */
var userCostID = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"25DC92D6-43B7-4D86-8A6F-7E2157BBF559"}
 */
var enrollNumber = '';

/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"ED739126-5115-4FA3-A8D8-0585059EB800",variableType:-4}
 */
var valueListDS = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"42C5D8DB-0978-4CF0-96FC-DA47757AFCF5"}
 */
var user_id_er = null;

/**
 * @properties={typeid:35,uuid:"BAAE0A49-1288-4E82-A692-2DB385CC4F12",variableType:-4}
 */
var isController = (globals.hasRole('Controllers') || scopes.globals.hasRole('Orders Admin'));

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 * @properties={typeid:24,uuid:"DCC165B9-FB11-4DEA-B7FE-27EB4872AC4B"}
 */
function onDataChangeType(oldValue, newValue, event) {
//	application.output('cambiato ' + globals.profit_cost_id);

	standard_amount = 0;
	real_amount_personnel = 0;
	day_number = 0;
	day_import = null;
	total_amount_value = 0;
	detailsID = null;
	userCostID = null;
//	application.setValueListItems('detailsPresent', [])
	globals.standard_figure = null
	globals.real_personnel = null;
	globals.real_TM = null;
	globals.real_TK = null;
	//globals.setValuelist('userCostList', null);
	application.output('cambiato detailsID ' + detailsID);
	loadValulistDetailsPresent();

	return switchPanels();

}

/**
 * @return {Boolean}
 * @properties={typeid:24,uuid:"53D3C2EF-91C3-42D6-9088-D996E70284EF"}
 */
function switchPanels() {
	if (globals.profit_cost_id == null) {
		elements.groupGeneric.visible = false;
		elements.groupRealP.visible = false;
		elements.groupRealTK.visible = false;
		elements.groupRealTM.visible = false;
		elements.groupDaysImport.visible = false;
		return true;
	}
	if (globals.profit_cost_id != null) {
		if (profit_cost_types_to_profit_cost_types.profit_cost_acr == 'R') {

			elements.groupGeneric.visible = false;
			elements.groupRealP.visible = false;
			elements.groupRealTK.visible = false;
			elements.groupRealTM.visible = false;
			elements.groupDaysImport.visible = false;

		} else {
			//personale
			if (profit_cost_types_to_profit_cost_types.profit_cost_types_id == 5) {
				elements.groupGeneric.visible = true;
				elements.groupRealP.visible = true;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = true;
			}
			// fornitura TK
			else if (profit_cost_types_to_profit_cost_types.profit_cost_types_id == 8) {
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = true;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
			}
			// fornitura TM
			else if (profit_cost_types_to_profit_cost_types.profit_cost_types_id == 9) {
				elements.groupGeneric.visible = true;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = true;
				elements.groupDaysImport.visible = true;
			} else if (profit_cost_types_to_profit_cost_types.profit_cost_types_id == 10) {
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = true;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
			} else {
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = false;
				elements.groupRealTK.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupDaysImport.visible = false;
			}

		}
	}
	return true;
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"04BEE022-B85E-4313-9C7F-9E48433E7E7D"}
 */
function onDataChangeStandardFigure(oldValue, newValue, event) {
	//se già presente il totale dei giorni e non è presente la figura reale modifica il totale
	if (oldValue != newValue && newValue != null) {
		standard_amount = selected_st_prof_to_standard_professional_figures.standard_cost;
		globals.standard_figure = newValue;
		if (day_number != null && (globals.real_personnel == null && globals.real_TM == null && day_import == null)) {
			day_import = standard_amount
			total_amount_value = scopes.globals.roundNumberWithDecimal( (day_import * day_number), 2);
		}
	}
	return true
}

/**
 * Handle changed data.
 *
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"FE211187-2DE4-42B4-B9AA-C24546A3E7C2"}
 * @AllowToRunInFind
 */
function onDataChangeRealFigure(oldValue, newValue, event) {
//	application.output('change real figure ' + globals.profit_cost_id)
	var ar = [null, null];
	if (newValue != null)
		ar = newValue.split(' - ');
//	application.output(ar)
	if (oldValue != newValue && newValue != null && (globals.profit_cost_id == 5 || globals.profit_cost_id == 10 || globals.profit_cost_id == 9)) {
		//		/** @type {globals.objUserCost} */
		//		var obj = globals.getUserCost(newValue);

		/** @type {globals.objUserCost} */
		var obj = globals.getLastUserCost(ar[0], ar[1]);
//		application.output(obj);
		day_import = obj.cost;
		if (globals.profit_cost_id == 5) {
			total_amount_value = 0;
			enrollNumber = ar[1];
			globals.real_personnel = ar[0];
			globals.real_TM = null
		}
		if (globals.profit_cost_id == 9) {
			total_amount_value = 0;
			enrollNumber = ar[1];
			globals.real_TM = ar[0];
			globals.real_personnel = null
		}
		if (globals.profit_cost_id == 10) {
			globals.real_personnel = ar[0];
			enrollNumber = ar[1];
			globals.real_TM = null
			day_import = null;
		}
		if (day_number != null && day_import != null) {
//			application.output(scopes.globals.roundNumberWithDecimal( (day_import * day_number), 2))
			total_amount_value = scopes.globals.roundNumberWithDecimal( (day_import * day_number), 2);
		}
	}

	//	if (oldValue != newValue && globals.profit_cost_id != 10) {
	//		total_amount_value = 0;
	//		day_import = null;
	//		globals.setValuelist('userCostList', newValue);
	//		if (globals.valueListDS.getMaxRowIndex() == 1) {
	//			day_import = globals.valueListDS.getValue(1, 1);
	//		}
	//		if (day_number != null) total_amount_value = day_import * day_number;
	//	}
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"E30EC751-7DAC-49AB-AD48-065C88115E1E"}
 */
function onDataChangeDaysNumber(oldValue, newValue, event) {
	//se presente la figura reale/standard calcola il total amount
	//if (oldValue != newValue) {
	//application.output(' ' + oldValue + ' ' + newValue)
	if (standard_amount != 0)
		total_amount_value = scopes.globals.roundNumberWithDecimal( (standard_amount * day_number), 2);
	if (day_import != 0 && day_import != null) {
//		application.output('2 ' + scopes.globals.roundNumberWithDecimal( (day_import * day_number), 2))
		total_amount_value = scopes.globals.roundNumberWithDecimal( (day_import * day_number), 2);
//		application.output(total_amount_value);
	}
	//}
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"8CC60ACE-B3EA-46EB-AEC1-C01630CE3A11"}
 */
function onDataChangeAmountPersonel(oldValue, newValue, event) {
	if (oldValue != newValue) {
		//application.output(' ' + oldValue + ' ' + newValue)
		if (day_number != null) {
//			application.output('1 ' + scopes.globals.roundNumberWithDecimal( (day_import * day_number), 2))
			total_amount_value = scopes.globals.roundNumberWithDecimal( (day_import * day_number), 2);
		}
	}
	return true
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"AC53B8DA-385B-4377-8AA7-AF1F79C07F5E"}
 */
function onShow(event) {
	application.output(globals.messageLog + 'START jo_new_planning_popup.onShow() ', LOGGINGLEVEL.INFO);
	_super.startEditing(event);

	application.setValueListItems('detailsPresent', []);
	detailsID = null;
	switchPanels();
	application.output(globals.messageLog + 'STOP jo_new_planning_popup.onShow() ', LOGGINGLEVEL.INFO);
}

/**
 * @param {JSEvent} event the event that triggered the actio
 *
 * @properties={typeid:24,uuid:"0AF3D9F9-D39D-4726-A347-4751BC6011C5"}
 */
function updateUI(event) {
	controller.readOnly = !isEditing();

	// buttons
	elements.buttonEdit.visible = !isEditing();
	elements.buttonSave.visible = isEditing();
	elements.buttonCancel.visible = isEditing();

	elements.ProfitCostDescription.visible = !isController;
	elements.ProfitCostDescriptionController.visible = isController;
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"60F3FA57-56F6-4266-A4A5-235E3A882242"}
 * @AllowToRunInFind
 */
function saveEditsPlain(event) {
	application.output(globals.messageLog + 'START jo_new_planning_popup.saveEdits() ', LOGGINGLEVEL.INFO);

	if (month_jo_planning == null || year_jo_planning == null) {
		globals.DIALOGS.showErrorDialog('Errore', 'Selezionare anno e mese di pianificazione', 'OK');
		return;
	}

	/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
	var boActualDate = databaseManager.getFoundSet('geco', 'bo_actual_date');
	boActualDate.loadRecords(); //per sicurezza, così dovrebbe tirare giù il record
	var recActualDate = boActualDate.getRecord(1);
	var actualDate = null;
	var insertedDate = new Date(year_jo_planning, month_jo_planning, 0);
	var answer = '';
	var recSaved = null;
	if (recActualDate != 0) actualDate = recActualDate.actual_date;

	application.output('insertedDate: ' + insertedDate + '; recActualDate: ' + actualDate);

	//Controllo: inserisco piani per date precedenti alla data di inizio della commessa
	/** @type {JSFoundSet<db:/geco/job_orders>} */
	var job_orders = databaseManager.getFoundSet('geco', 'job_orders');
	var totJo = 0;
	if (job_orders.find()) {
		job_orders.job_order_id = globals.job_order_selected;
		totJo = job_orders.search();
	}
	var recJO = null;

	//application.output('totJo: ' + totJo);

	if (totJo != 0) {
		recJO = job_orders.getRecord(1);
	}
	if(!globals.hasRole('Controllers') && recJO.job_orders_to_mgm_market_competence.competence_month == insertedDate && recJO.job_orders_to_mgm_market_competence.actual_confirmation == 1){
		globals.DIALOGS.showErrorDialog('ERRORE', 'Non è possibile inserire piani per il mese scelto:\n hai già confermato il mercato.', 'OK');
		return;
	}
	application.output('recJO.valid_from: ' + recJO.valid_from);

	//se inserisco piani per date precedenti all'ultima di actual
	if (insertedDate < actualDate) {
		globals.DIALOGS.showErrorDialog('ERRORE', 'Non è possibile inserire piani per date precedenti all\'ultima data di actual.', 'OK');
		return;
	}

	//se mese non è aperto per attualizzare e non siamo nella situazione post caricamento costi di David
	if (insertedDate == actualDate && recActualDate.is_open_to_actual == 0 && recActualDate.month_status_actual != 1 && recActualDate.month_status_actual != null) {
		globals.DIALOGS.showErrorDialog('ERRORE', 'Il mese per attualizzare è chiuso, non è possibile inserire piani per date precedenti o uguali all\'ultima data di actual.', 'OK');
		return;
	}

	//se inserisco piano su un mese precedente a quello di inizio della commessa
	if (recJO && recJO.valid_from != null && insertedDate < recJO.valid_from) {
		answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai inserendo una pianificazione fuori dal range indicato nella Commessa. Vuoi modificare le date?", "SI", "NO");
		if (answer == 'SI') {
			recJO.valid_from = insertedDate;
			recSaved = databaseManager.saveData(recJO);
			application.output(globals.messageLog + 'jo_new_planning_popup.saveEdits() modifica data inizio commessa ', LOGGINGLEVEL.INFO);
		} else return;
	}

	//se inserisco piano su un mese successivo a quello di fine della commessa
	if (recJO && recJO.valid_to != null && insertedDate > recJO.valid_to) {
		answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai inserendo una pianificazione fuori dal range indicato nella Commessa. Vuoi modificare le date?", "SI", "NO");
		if (answer == 'SI') {
			recJO.valid_to = insertedDate;
			recSaved = databaseManager.saveData(recJO);
			application.output(globals.messageLog + 'jo_new_planning_popup.saveEdits() modifica data fine commessa ', LOGGINGLEVEL.INFO);
		} else return;
	}

	if (globals.profit_cost_id == null) {
		globals.DIALOGS.showErrorDialog('Errore', 'Selezionare la tipologia di costo o ricavo', 'OK');
		return;
	}
	if (globals.profit_cost_id != null && (globals.profit_cost_id == 5 || globals.profit_cost_id == 10) && (globals.real_personnel == null && globals.standard_figure == null)) {
		globals.DIALOGS.showErrorDialog('Errore', 'Indicare la figura professionale o la persona reale ', 'OK');
		return;
	}

	if (globals.profit_cost_id != null && (globals.profit_cost_id == 9) && (globals.real_TM == null && globals.standard_figure == null)) {
		globals.DIALOGS.showErrorDialog('Errore', 'Indicare la figura professionale o la persona reale ', 'OK');
		return;
	}

	if (globals.profit_cost_id != null && (globals.profit_cost_id == 8) && globals.real_TK == null) {
		globals.DIALOGS.showErrorDialog('Errore', 'Indicare il fornitore ', 'OK');
		return;
	}
	application.output(globals.messageLog + 'jo_new_planning_popup.saveEdits() ' + globals.profit_cost_id + ' real: ' + globals.real_personnel + ' tk: ' + globals.real_TK + ' tm: ' + globals.real_TM + ' standard: ' + globals.standard_figure, LOGGINGLEVEL.INFO);
	var totDetails = 0;

	/** @type {globals.objUserCost} */
	//var obj = globals.getUserCost(userCostID);
	/** @type {JSFoundSet<db:/geco/jo_details>} */
	var jo_details = databaseManager.getFoundSet('geco', 'jo_details');
//	application.output('enr Numb ' + enrollNumber);
//	application.output('dettaglio esistente ' + detailsID)
	if (jo_details.find()) {
		jo_details.jo_id = globals.job_order_selected;
		jo_details.profit_cost_type_id = globals.profit_cost_id;
		//dettaglio esistente VoceCostoricavo già presente per la commessa
		if (detailsID != null) {
			application.output(globals.messageLog + 'jo_new_planning_popup.saveEdits() dettaglio costoRicavo esistente ', LOGGINGLEVEL.INFO);
			jo_details.jo_details_id = detailsID
			//jo_details.days_import = (day_import != 0 && day_import != null) ? day_import : null;
			if ( (globals.profit_cost_id == 5 || globals.profit_cost_id == 9)) {
				if (enrollNumber != null)
					jo_details.enrollment_number = enrollNumber;
			}
			//dettaglio non esistente
		} else {
			application.output(globals.messageLog + 'jo_new_planning_popup.saveEdits() nuovo dettaglio costoRicavo ', LOGGINGLEVEL.INFO);
			jo_details.real_figure = globals.real_personnel
			jo_details.real_tk_supplier = globals.real_TK;
			jo_details.real_tm_figure = globals.real_TM;
			jo_details.standard_figure = globals.standard_figure;
			//jo_details.days_import = (day_import != 0 && day_import != null) ? day_import : null;
			if ( (globals.profit_cost_id == 5 || globals.profit_cost_id == 9)) {
				if (enrollNumber != null)
					jo_details.enrollment_number = enrollNumber;
			}

		}
		totDetails = jo_details.search();
//		application.output('esistente ' + jo_details.profit_cost_type_id);
//		application.output(totDetails + ' ID ' + jo_details.jo_details_id);
		var arrayNotDubleType = [5, 9, 10];
//		application.output('contiene ' + arrayNotDubleType.indexOf(globals.profit_cost_id))
		if (detailsID != null) {
			application.output(' DET ID trovato')
		}
		//se il dettaglio è personale, TM o spese non può essere duplicato
		else if (arrayNotDubleType.indexOf(globals.profit_cost_id) != -1 && jo_details.real_figure == globals.real_personnel && jo_details.real_tm_figure == globals.real_TM && jo_details.real_tk_supplier == globals.real_TK ) {
			if (jo_details.standard_figure != null && jo_details.standard_figure == globals.standard_figure && ( globals.real_personnel == null && globals.real_TM == null && globals.real_TK == null)){
				application.output('trovato con tutto null ma figura standard definita e esistente')
				totDetails = 0;
			}
			//application.output('trovato')
		} else {
//			application.output('Non trovato')
			totDetails = 0;
		}
	}
	//non trovo dettagli lo creo
	if (totDetails == 0) {
//		application.output(globals.job_order_selected)
		//creare il dettaglio con tutti i campi valorizzati
		var idx = jo_details.newRecord(false);
		jo_details.jo_id = globals.job_order_selected;
		jo_details.profit_cost_type_id = globals.profit_cost_id

		jo_details.real_figure = globals.real_personnel;
		jo_details.real_tk_supplier = globals.real_TK;
		jo_details.real_tm_figure = globals.real_TM;
		jo_details.standard_figure = globals.standard_figure;
		if (enrollNumber != null)
			jo_details.enrollment_number = enrollNumber;

		if (day_import != 0 && day_number != 0) {
			jo_details.days_import = day_import;
			jo_details.days_number = day_number;
		}
		jo_details.total_amount = total_amount_value;
//		application.output(idx + ' nuovo ' + jo_details.profit_cost_type_id);
//		application.output(jo_details.getRecord(idx));
		//application.output(totDetails + ' ID ' + jo_details.jo_details_id);
		//creare il piano per il mese e anno selezionati
		jo_details.jo_details_to_jo_planning.newRecord();
		jo_details.jo_details_to_jo_planning.jo_details_id = jo_details.jo_details_id;
		jo_details.jo_details_to_jo_planning.jo_id = jo_details.jo_id;
		jo_details.jo_details_to_jo_planning.bo_id = jo_details.bo_id;
		jo_details.jo_details_to_jo_planning.bo_details_id = jo_details.bo_details_id;
		jo_details.jo_details_to_jo_planning.jo_month = month_jo_planning;
		jo_details.jo_details_to_jo_planning.jo_year = year_jo_planning;
		jo_details.jo_details_to_jo_planning.days_import = jo_details.days_import;
		jo_details.jo_details_to_jo_planning.days_number = (day_number != 0) ? day_number : null;
		jo_details.jo_details_to_jo_planning.return_actual = null;
		jo_details.jo_details_to_jo_planning.cost_actual= null;
		jo_details.jo_details_to_jo_planning.total_actual= null;
		if (jo_details.jo_details_to_profit_cost_types.profit_cost_acr == 'R') {
			jo_details.jo_details_to_jo_planning.total_amount = total_amount_value;
			jo_details.jo_details_to_jo_planning.return_amount = jo_details.jo_details_to_jo_planning.total_amount
		} else {
			if (jo_details.profit_cost_type_id == 5 || jo_details.profit_cost_type_id == 9) {
				jo_details.jo_details_to_jo_planning.total_amount = scopes.globals.roundNumberWithDecimal( (day_number * jo_details.jo_details_to_jo_planning.days_import), 2);
				jo_details.jo_details_to_jo_planning.cost_amount = jo_details.jo_details_to_jo_planning.total_amount
//				application.output('1 ' + jo_details.jo_details_to_jo_planning.total_amount + ' ' + jo_details.jo_details_to_jo_planning.cost_amount)
			} else {
				jo_details.jo_details_to_jo_planning.total_amount = total_amount_value;
				jo_details.jo_details_to_jo_planning.cost_amount = jo_details.jo_details_to_jo_planning.total_amount
			}
		}
		//trovato il dettaglio
	} else {
		var totPlain = 0;
		/** @type {JSFoundSet<db:/geco/jo_planning>} */
		var jo_planning = databaseManager.getFoundSet('geco', 'jo_planning');
		if (jo_planning.find()) {
			jo_planning.jo_id = globals.job_order_selected;
			jo_planning.jo_details_id = jo_details.jo_details_id;
			jo_planning.jo_year = year_jo_planning;
			jo_planning.jo_month = month_jo_planning;
			totPlain = jo_planning.search();
			if (totPlain > 0) {
				globals.DIALOGS.showErrorDialog('Attenzione', 'Per il dettaglio selezionato esiste già una pianificazione per il mese/anno ' + month_jo_planning + ' ' + year_jo_planning, 'OK');
				application.output(globals.messageLog + 'STOP jo_new_planning_popup.saveEdits() PIANO ESISTENTE ', LOGGINGLEVEL.INFO);
				return;
			} else {
				//creare il piano per il mese e anno selezionati
				jo_details.jo_details_to_jo_planning.newRecord();
				jo_details.jo_details_to_jo_planning.jo_details_id = jo_details.jo_details_id;
				jo_details.jo_details_to_jo_planning.jo_id = jo_details.jo_id;
				jo_details.jo_details_to_jo_planning.bo_id = jo_details.bo_id;
				jo_details.jo_details_to_jo_planning.bo_details_id = jo_details.bo_details_id;
				jo_details.jo_details_to_jo_planning.jo_month = month_jo_planning;
				jo_details.jo_details_to_jo_planning.jo_year = year_jo_planning;
				//nuovo costo aziendale!!!!!!!
				jo_details.jo_details_to_jo_planning.days_import = day_import
				jo_details.jo_details_to_jo_planning.days_number = (day_number != 0) ? day_number : null;
				jo_details.jo_details_to_jo_planning.user_id = jo_details.user_id;
				jo_details.jo_details_to_jo_planning.return_actual = null;
				jo_details.jo_details_to_jo_planning.cost_actual = null;
				jo_details.jo_details_to_jo_planning.total_actual = null;

				if (jo_details.jo_details_to_profit_cost_types.profit_cost_acr == 'R') {
					jo_details.jo_details_to_jo_planning.total_amount = total_amount_value;
					jo_details.jo_details_to_jo_planning.return_amount = jo_details.jo_details_to_jo_planning.total_amount
				} else {
					if (jo_details.profit_cost_type_id == 5 || jo_details.profit_cost_type_id == 9) {
						jo_details.jo_details_to_jo_planning.total_amount = scopes.globals.roundNumberWithDecimal( (day_number * jo_details.jo_details_to_jo_planning.days_import), 2);
						jo_details.jo_details_to_jo_planning.cost_amount = jo_details.jo_details_to_jo_planning.total_amount
//						application.output('1 ' + jo_details.jo_details_to_jo_planning.total_amount + ' ' + jo_details.jo_details_to_jo_planning.cost_amount)
					} else {
						jo_details.jo_details_to_jo_planning.total_amount = total_amount_value;
						jo_details.jo_details_to_jo_planning.cost_amount = jo_details.jo_details_to_jo_planning.total_amount
					}
				}
			}
		}

	}
	var saved = _super.saveEdits(event);
	application.output(globals.messageLog + 'jo_new_planning_popup.saveEdits() nuovo dettaglio/piano salvato ', LOGGINGLEVEL.INFO);
	if (!saved) {
		databaseManager.revertEditedRecords();
	}//saved a true,
	//prima di caricare i record avendo il dettaglio del record vado sulla tabella mgm_actual_competence
	else {
		//Verificare dal dettaglio se è stata inserita una voce di tipo 1,2,7,8,13; se si, actual_confirmation per quel profit center passa a 0
//		if (!globals.hasRole('Controllers')) {
//			if ([1, 2, 7, 8, 13].indexOf(jo_details.profit_cost_type_id) > -1) {
//				/** @type {JSFoundSet<db:/geco/mgm_market_competence>} */
//				var mgmActualCompetence = databaseManager.getFoundSet('geco', 'mgm_market_competence');
//				/** @type {Number} */
//				var totMgmActualCompetence = 0;
//
//				var formElement = forms.jo_sx.elements.split.getRightForm();
//				var competenceMonth = formElement['competenceMonth'];
//
//				//controllo se per quella data actual c'è già un record con quel pc
//				if (mgmActualCompetence.find()) {
//					mgmActualCompetence.profit_center_id = jo_details.jo_details_to_job_orders.profit_center_id;
//					mgmActualCompetence.competence_month = competenceMonth;
//					totMgmActualCompetence = mgmActualCompetence.search();
//				}
//
//				//lo trovo, verifico che actual_confirmation sia a 1; in tal caso lo setto a 0
//				if (totMgmActualCompetence != 0) {
//					var rec = mgmActualCompetence.getRecord(1);
//					if (rec.actual_confirmation == 1) {
//						rec.actual_confirmation = 0;
//
//						var savedMarket = databaseManager.saveData(rec);
//						application.output(globals.messageLog + 'jo_new_planning_popup.saveEdits() modificato stato del mercato ' +savedMarket, LOGGINGLEVEL.INFO);
//						globals.DIALOGS.showInfoDialog('Info', 'Ricordati di riconfermare la chiusura del mercato', 'OK');
//						
//					}
//				}
//			}
//		}
		enrollNumber = null;
		jo_details.jo_details_to_jo_planning.loadRecords();
	}
	detailsID = null;
	application.setValueListItems('detailsPresent', [])
	forms[scopes.globals.callerForm].updateUI(event);
	application.output(globals.messageLog + 'STOP jo_new_planning_popup.saveEdits() ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"517D6620-7827-4E53-8B28-71F3A43FAFD7"}
 */
function stopEditing(event) {
	application.output(globals.messageLog + 'START jo_new_planning_popup.stopEditing() ', LOGGINGLEVEL.INFO);
	clearField();
	_super.stopEditing(event);
	controller.getWindow().destroy();
//	application.output(scopes.globals.callerForm)
	forms[scopes.globals.callerForm].updateUI(event);
	application.output(globals.messageLog + 'STOP jo_new_planning_popup.stopEditing() ', LOGGINGLEVEL.INFO);
}

/**
 * @properties={typeid:24,uuid:"19B576B2-7D18-41B6-864A-68A2C8DCAAB1"}
 */
function clearField() {
	standard_amount = 0;
	real_amount_personnel = 0;
	day_number = 0;
	day_import = 0;
	total_amount_value = 0;
	month_jo_planning = new Date().getMonth() + 1;
	year_jo_planning = new Date().getFullYear();
	detailsID = null;
	//userCostID = null;
	user_id_er = null;
	valueListDS = null;
	globals.profit_cost_id = null;
	globals.standard_figure = null
	globals.real_personnel = null;
	globals.real_TM = null;
	globals.real_TK = null;
	enrollNumber = null;
}

/**
 * @properties={typeid:24,uuid:"CEB38546-1FFA-49AE-9782-8ABF8730F70B"}
 */
function loadValulistDetailsPresent() {
	application.output(globals.messageLog + 'START jo_new_planning_popup.loadValulistDetailsPresent() ', LOGGINGLEVEL.INFO);	
	/** @type {JSDataSet} */
	valueListDS = null;
	var sql = '';
	sql = "select concat(pt.profit_cost_description, ' - ', ifnull(jd.enrollment_number,''), ' - ' , ifnull(jd.description,'')), jd.jo_details_id from jo_details jd, profit_cost_types pt where jd.jo_id = ? and jd.profit_cost_type_id = ? and jd.profit_cost_type_id = pt.profit_cost_types_id";
	var args = [];
	args[0] = globals.job_order_selected;
	args[1] = globals.profit_cost_id;
	application.output(sql)
	application.output(args)
	valueListDS = databaseManager.getDataSetByQuery(globals.gecoDb, sql, args, -1);
	application.setValueListItems('detailsPresent', valueListDS)
	
	application.output(globals.messageLog + 'STOP jo_new_planning_popup.loadValulistDetailsPresent() ', LOGGINGLEVEL.INFO);		
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"BAAA2C76-866E-457A-B1C6-8F29317656F9"}
 * @AllowToRunInFind
 */
function onDataChangePCold(oldValue, newValue, event) {
	application.output('dettaglio scelto ' + newValue)
	var totDet = 0;
	if (newValue != null) {
		/** @type {JSFoundSet<db:/geco/jo_details>} */
		var jo_details = databaseManager.getFoundSet('geco', 'jo_details');
		if (jo_details.find()) {
			jo_details.jo_details_id = newValue;
			totDet = jo_details.search();
		}
		application.output(totDet);
		application.output(jo_details);
		if (totDet > 0) {
			if (globals.profit_cost_id == 5 || globals.profit_cost_id == 9) {
				//var costId = globals.getCostID(jo_details.user_id, jo_details.enrollment_number);
				/** @type {globals.objUserCost} */
				var obj = null;
				if(jo_details.user_id != null)
					obj = globals.getLastUserCost('' + jo_details.user_id, jo_details.enrollment_number);
				else {
					obj = {
						uid: null,
						cost: jo_details.days_import,
						name: '',
						enrollNum: ''
					}
				}
				application.output(obj);
				//			/** @type {globals.objUserCost} */
				//			var obj = globals.getUserCost(costId);
				day_import = obj.cost;

				if (day_number != null && day_import != null) total_amount_value = scopes.globals.roundNumberWithDecimal( (day_import * day_number), 2);

				if (globals.profit_cost_id == 5) {
					scopes.globals.real_personnel = jo_details.user_id;
					enrollNumber = jo_details.enrollment_number;
					scopes.globals.standard_figure = jo_details.standard_figure;
				} else if (globals.profit_cost_id == 9) {
					scopes.globals.real_TM = jo_details.user_id;
					enrollNumber = jo_details.enrollment_number;
					scopes.globals.standard_figure = jo_details.standard_figure;
				}
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = false;
				elements.groupRealTM.visible = false;
			} else if (globals.profit_cost_id == 10) {
				scopes.globals.real_personnel = jo_details.user_id;
				enrollNumber = jo_details.enrollment_number;
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = false;
				elements.groupRealTM.visible = false;
			} else if (globals.profit_cost_id == 8) {
				scopes.globals.real_TK = jo_details.real_tk_supplier;
				elements.groupGeneric.visible = false;
				elements.groupRealP.visible = false;
				elements.groupRealTM.visible = false;
				elements.groupRealTK.visible = false
			}
		}
	} else {
		enrollNumber = null;
		switchPanels();

	}
	return true
}
