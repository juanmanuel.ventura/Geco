/**
 * @type {String}
 * @properties={typeid:35,uuid:"19F1A43C-FDE5-442C-90E3-C17D057A5622"}
 */
var sortDirection = 'asc';

/**
 * @type {RuntimeLabel}
 * @properties={typeid:35,uuid:"5AAE9DFF-AF81-4966-92B4-DFE319FB3755",variableType:-4}
 */
var label;

/**
 * @properties={typeid:35,uuid:"96C974AD-185E-4B68-9205-990EED3D40EF",variableType:-4}
 */
var actualMonth = null;

/**
 * @properties={typeid:35,uuid:"215D751C-7C09-48E6-B49A-C67B4EFF451C",variableType:-4}
 */
var actualYear = null;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"7EB930C6-BA43-4F7A-A0E9-FB4F1A95CF30",variableType:93}
 */
var competenceMonth = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"BB491591-24E8-4235-BD44-E914F39E43AC",variableType:4}
 */
var isOpenToActual = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F2F315F5-B216-4FD0-88F4-A0136556BCC0",variableType:8}
 */
var totalCostMonth = 0.0;
/**
 * @type {Number}
 * @properties={typeid:35,uuid:"A8923FB7-1122-483C-A597-17FDEC909D53",variableType:8}
 */
var totalProfitMonth = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"37D1D43C-F285-47C0-BACC-DC6CAD5F89EB",variableType:8}
 */
var totalBudgetBO = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"28B3A67B-27E0-40E9-B2F3-37C72527EAB3",variableType:8}
 */
var totalProfitForecast = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"109E953A-F1EB-4303-8ED3-C2B790621842",variableType:8}
 */
var totalProfitActual = 0.0;

/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"91BDD555-40B3-4285-B9EB-0CFC30E42EB4"}
 */
function sort(event) {
	//job_order_id asc, job_orders_owner_to_users.users_to_contacts.real_name asc
	// change color back to the original one
	if (label) label.fgcolor = '#727784';
	// (re) set the triggered label
	label = event.getSource();
	// (re) change color to indicate selection
	label.fgcolor = '#990000';
	var sortKey = label.text;
	//application.output(sortKey + ' ' + sortDirection);
	switch (sortKey) {
	case 'Codice':
		controller.sort('external_code_navision' + ' ' + sortDirection);
		break;
	case 'BO':
		controller.sort('customer_order_code' + ' ' + sortDirection);
		break;
	case 'Descrizione':
		controller.sort('job_order_title' + ' ' + sortDirection);
		break;
	case 'Cliente':
		controller.sort('job_orders_to_companies.company_name' + ' ' + sortDirection);
		break;
	case 'Tipo':
		controller.sort('job_orders_to_job_orders_types.job_order_type_description' + ' ' + sortDirection);
		break;
	case 'Stato':
		controller.sort('is_enabled' + ' ' + sortDirection);
		break;
	case 'RDC':
		controller.sort('job_orders_owner_to_users.users_to_contacts.real_name' + ' ' + sortDirection);
		break;
	case 'Profit Center':
		controller.sort('job_orders_to_profit_centers.profit_center_name' + ' ' + sortDirection);
		break;
//	case 'Anno':
//		controller.sort('job_order_year' + ' ' + sortDirection);
//		break;
//	case 'Ricavo':
//		controller.sort('profit' + ' ' + sortDirection);
//		break;
//	case 'Costo':
//		controller.sort('cost' + ' ' + sortDirection);
//		break;
	}

	// toggle the sort order
	sortDirection = (sortDirection == 'asc') ? 'desc' : 'asc';
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"5FC9A0C4-3754-48A8-A5A9-3EE2EC1DA2B5"}
 * @AllowToRunInFind
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
//	elements.buttonAdd.visible = false;
//	elements.buttonDelete.visible = false;
}

///**
// * Perform the element default action.
// *
// * @param {JSEvent} event the event that triggered the action
// *
// * @properties={typeid:24,uuid:"BC2FBCAD-464B-463D-B8FE-7826D23974F0"}
// * @AllowToRunInFind
// */
//function confirmAllJOActual(event) {
//	var answer = '';
//	var pcList = scopes.globals.getProfitCenterByUserManager(scopes.globals.currentUserId);
//	application.output('pcList: ' + pcList);
//	var pcToSearch = (pcList.length != 0) ? pcList.join('||') : null;
//	var savedData = null;
//
//	if (pcList.length != 0) {
//		answer = globals.DIALOGS.showQuestionDialog('ATTENZIONE', 'Questa operazione confermerà gli actual di tutte le commesse sui mercati di cui sei responsabile.\n\nProcedere con l\'operazione?', 'SI', 'NO')
//		//answer = 'SI';
//	}
//
//	if (answer != 'SI') return;
//
//	/** @type {JSFoundSet<db:/geco/job_orders>} */
//	var job_orders = databaseManager.getFoundSet('geco', 'job_orders')
//	if (job_orders.find()) {
//		job_orders.job_orders_to_profit_centers.profit_center_id = pcToSearch;
//		job_orders.is_enabled = 1;
//		job_orders.job_order_type = '1||2||3';
//
//		job_orders.search();
//	}
//
//	//resetFilter(event);
//
//	var countJPtoConfirm = 0;
//	var countJPnotActual = 0;
//	for (var index = 1; index <= job_orders.getSize(); index++) {
//		var recJO = job_orders.getRecord(index);
//		application.output('Commessa: ' + recJO.job_order_id + '; codice: ' + recJO.external_code_navision + '; PC: ' + recJO.profit_center_id);
//		/** @type {JSFoundSet<db:/geco/jo_planning>} */
//		var joPl = recJO.job_orders_to_jo_planning;
//		if (joPl && joPl.find()) {
//			joPl.jo_month = actualMonth;
//			joPl.jo_year = actualYear;
//			joPl.search();
//		}
//
//		/** @type {JSRecord<db:/geco/jo_planning>[]} */
//		var plToConfirmList = []; //lista record da confermare
//
//		for (var i = 1; i <= joPl.getSize(); i++) {
//
//			/** @type {JSRecord<db:/geco/jo_planning>} */
//			var recPL = joPl.getRecord(i); //salvo record e controllo
//			//1=Competenze Ordinarie; 2=Spese Rifatturabili;7=Acquisti Materiali;8=Fornitura TK
//			if ([1, 2, 7, 8].indexOf(recPL.jo_planning_to_jo_details.profit_cost_type_id) > -1 && recPL.is_actual == 1 && (recPL.status == 1 || recPL.status == 2)) {
//				countJPtoConfirm++;
//				if (recPL.status == 1) {
//					plToConfirmList.push(recPL);
//					application.output('PIANO da confermare');
//				}
//			}
//			if ([1, 2, 7, 8].indexOf(recPL.jo_planning_to_jo_details.profit_cost_type_id) > -1 && (recPL.is_actual != 1 || recPL.status == 7 || recPL.status == 9)) {
//				countJPnotActual++;
//			}
//		}
//		if (countJPnotActual > 0) {
//			application.output('NON CHIUDO IL MERCATO piani non actual o respinti ' + countJPnotActual);
//			globals.DIALOGS.showErrorDialog('ERRORE', 'Non è stato possibile confermare gli actual per le commesse dei mercati.\n\nAlcuni piani non sono in stato actual e/o sono stati respinti.', 'OK')
//			return;
//		}
//	}
//	_super.startEditing(event);
//	if (plToConfirmList.length > 0) {
//		var query = 'call sp_insert_history_jo(?,?,?,?,?,?)';
//		var args = [foundset.job_order_id, scopes.globals.currentUserId, 'before actual by user ' + scopes.globals.currentUserDisplayName, actualMonth, actualYear, 1];
//		var dataset = databaseManager.getDataSetByQuery('geco', query, args, -1);
//
//		//confermo i piani actual da RDC
//		for (var y = 0; y < plToConfirmList.length; y++) {
//			var rec = plToConfirmList[y];
//			application.output('da confermare ' + rec.jo_planning_id + ' stato ' + rec.status + ' actual ' + rec.is_actual)
//			rec.status = 2;
//			rec.user_actual = scopes.globals.currentUserId;
//			rec.date_actual = new Date();
//		}
//
//	}
//	//tutto è andato a buon fine, inserisco nella tab. mgm_actual_competence un record di log.
//	for (var marketNumber = 0; marketNumber < pcList.length; marketNumber++) {
//		/** @type {JSFoundSet<db:/geco/mgm_actual_competence>} */
//		var mgmActualCompetence = databaseManager.getFoundSet('geco', 'mgm_actual_competence');
//		/** @type {Number} */
//		var totMgmActualCompetence = 0;
//
//		//vado in find sulla tab. mgm_actual_competence
//		if (mgmActualCompetence.find()) {
//			mgmActualCompetence.competence_month = competenceMonth;
//			mgmActualCompetence.profit_center_id = pcList[marketNumber];
//			totMgmActualCompetence = mgmActualCompetence.search();
//		}
//
//		//application.output('totMgmActualCompetence: ' + totMgmActualCompetence);
//
//		//se non ci sono record, lo creo
//		if (totMgmActualCompetence == 0) {
//			mgmActualCompetence.newRecord();
//			mgmActualCompetence.profit_center_id = pcList[marketNumber];
//			mgmActualCompetence.competence_month = competenceMonth;
//			mgmActualCompetence.actual_confirmation = 1;
//
//			//	savedData = databaseManager.saveData(mgmActualCompetence);
//			application.output('savedData: ' + savedData + ';\n' + mgmActualCompetence);
//		}//prendo record trovato
//		else {
//			var record = mgmActualCompetence.getRecord(1);
//
//			if (record.actual_confirmation != 1) {
//				record.actual_confirmation = 1;
//				application.output('record.actual_confirmation: ' + record.actual_confirmation);
//			}
//		}
//
//	}
//	var saved = _super.saveEdits(event);
//	if (saved) {
//		globals.DIALOGS.showInfoDialog('ESITO', 'Tutti gli actual sono stati confermati per il mese di competenza:\n' + scopes.globals.monthName[actualMonth - 1] + ' ' + actualYear + '.', 'OK')
//		/** @type {JSFoundSet<db:/geco/mgm_actual_competence>} */
//		var mgmActualComp = databaseManager.getFoundSet('geco', 'mgm_actual_competence');
//		var totalConfirm = 0;
//		if (mgmActualComp.find()) {
//			mgmActualComp.competence_month = competenceMonth;
//			mgmActualComp.actual_confirmation = 1;
//			totalConfirm = mgmActualComp.search();
//		}
//		/** @type {JSFoundSet<db:/geco/profit_centers>} */
//		var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');
//		var totMarkets = 0;
//		if(profit_centers.find()){
//			//tipo mercato
//			profit_centers.profit_center_type_id = 5;
//			totMarkets = profit_centers.search();
//		}
//		application.output('confermati ' + totalConfirm + ' mercati ' + totMarkets)
//		
//			if (totalConfirm == totMarkets){
//			//invio mail di conferma tutti mercati a Controller
//			scopes.globals.sendConfirmationActualMail(16,scopes.globals.monthName[actualMonth - 1] + ' ' + actualYear);
//		}
//	}
//	updateUI(event);
//}

///**
// * @param event
// *
// * @properties={typeid:24,uuid:"13D49E76-5EBE-44A9-82A8-556EEED7B139"}
// */
//function resetFilter(event) {
//	var formFilter = forms.jo_sx.elements.split.getLeftForm();
//	formFilter['jobOrderId'] = null;
//	formFilter['profitCenterId'] = null;
//	formFilter['rdcJo'] = null;
//	formFilter['customer'] = null;
//	formFilter['statusJo'] = 1;
//	formFilter['type'] = null;
//	formFilter['yearJo'] = null;
//	formFilter['applyFilter']();
//}

/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"23FB9369-12E0-49AC-82F4-888EE30A9525"}
 * @AllowToRunInFind
 */
function updateUI(event) {
//	elements.buttonAllActualConfirm.visible = false;
//	elements.labelAllActualConfirm.visible = false;
//	elements.buttonAllActualConfirm.enabled = false;
//	elements.labelAllActualConfirm.enabled = false;
//
//	if (globals.hasRole('Resp. Mercato')) {
//		application.output('Resp. Mercato ' + scopes.globals.currentownerid_to_user_markets.user_id);
//		/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
//		var boActualDate = databaseManager.getFoundSet('geco', 'bo_actual_date');
//		/** @type {Number} */
//		var totBoActualDate = 0;
//
//		//filtro in boActualDate, dovrei trovare almeno un valore
//		if (boActualDate.find()) {
//			totBoActualDate = boActualDate.search();
//		}
//
//		//trovo almeno un risultato
//		if (totBoActualDate != 0) {
//			//mi salvo il record alla posizione 1, in particolare il valore di isOpenToActual
//			var recBoActualDate = boActualDate.getRecord(1);
//			isOpenToActual = recBoActualDate.is_open_to_actual;
//			competenceMonth = recBoActualDate.actual_date;
//			actualMonth = recBoActualDate.actual_month;
//			actualYear = recBoActualDate.actual_year;
//
//			application.output('recBoActualDate: ' + recBoActualDate + ';\nisOpenToActual: ' + isOpenToActual);
//			
//			var pcList = scopes.globals.getProfitCenterByUserManager(scopes.globals.currentUserId);
//			application.output('pcList: ' + pcList);
//			var pcToSearch = (pcList.length != 0) ? pcList.join('||') : null;
//			
//			/** @type {JSFoundSet<db:/geco/mgm_actual_competence>} */
//			var mgmActualComp = databaseManager.getFoundSet('geco', 'mgm_actual_competence');
//			var totalConfirm = 0;
//			if (mgmActualComp.find()) {
//				mgmActualComp.competence_month = competenceMonth;
//				mgmActualComp.actual_confirmation = 1;
//				mgmActualComp.profit_center_id = pcToSearch;
//				totalConfirm = mgmActualComp.search();
//			}
//			
//			application.output('toale mercati confermati ' + totalConfirm + ' mercati propri ' + pcList.length);
//			
//			//se il mese è aperto per attualizzare, faccio vedere i bottoni al Responsabile di Mercato
//			if (isOpenToActual == 1 && totalConfirm!=pcList.length) {
//				elements.buttonAllActualConfirm.visible = true;
//				elements.labelAllActualConfirm.visible = true;
//				elements.buttonAllActualConfirm.enabled = true;
//				elements.labelAllActualConfirm.enabled = true;
//			}
//		}//non ho trovato alcun record nella tabella bo_actual_date --> Controller non ha mai attualizzato i costi.
//		else {
//			application.output('PRIMO ACTUAL COSTI.')
//		}
//	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"357B55FE-69C2-43F4-A72F-6B1DBFCED309"}
 */
function openConfirmedMarkets(event) {
	var win = application.createWindow("Lista Mercati confermati", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 430, 300);
	win.title = 'Lista Mercati confermati';
	win.show(forms.popupMarkets);
}
