/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formFilterName
 * @param {String} formListName
 * @properties={typeid:24,uuid:"09C589D6-0B02-4CEC-BD0F-9FDBDF92ACFC"}
 */
function onLoad(event, formFilterName, formListName) {
	application.output(globals.messageLog + 'START _bo_sx.onLoad() form filtro: ' +  formFilterName + ', form lista: ' + formListName ,LOGGINGLEVEL.DEBUG);
	
	var h_filter = 134;

	if (formFilterName && forms[formFilterName]) {

		var form = solutionModel.getForm(formFilterName);
		h_filter = form.getPart(JSPart.BODY).height
	}
	try {
		// set navigator
		if (elements.split.dividerLocation) {
			elements.split.dividerSize = 0;
			elements.split.dividerLocation = h_filter;
		}
	} catch (e) {
				application.output(e);
				application.output('errore initialize');
	}
	if (formFilterName && forms[formFilterName]) {
		elements.split.setLeftForm(forms[formFilterName]);
		// force to apply filter (seems not to work on form's onLoad event)
		if (forms[formFilterName].applyFilter) forms[formFilterName].applyFilter();
	}
	elements.split.setRightForm(forms[formListName]);
	plugins.WebClientUtils.executeClientSideJS('window.location.reload()');
	application.output(globals.messageLog + 'STOP _bo_sx.onLoad() ' ,LOGGINGLEVEL.DEBUG);
}



