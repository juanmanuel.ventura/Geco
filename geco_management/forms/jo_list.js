/**
 * @type {String}
 * @properties={typeid:35,uuid:"1215CD8C-14ED-4E4D-AB39-7B1249E06BD2"}
 */
var sortDirection = 'asc';

/**
 * @type {RuntimeLabel}
 * @properties={typeid:35,uuid:"805C8B75-DADE-4481-BD6E-D33F2668CC6D",variableType:-4}
 */
var label;

/**
 * @properties={typeid:35,uuid:"78E0309F-C70B-4527-9F4F-ABC78FBA1058",variableType:-4}
 */
var actualMonth = null;

/**
 * @properties={typeid:35,uuid:"97ACA92D-A129-414D-B5E3-809FCE062967",variableType:-4}
 */
var actualYear = null;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"0154DFD0-607A-4CEC-BAFE-21B322A2E268",variableType:93}
 */
var competenceMonth = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"1864F2F2-E636-4951-A784-EC4E07E97994",variableType:4}
 */
var isOpenToActual = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F5A58DE1-FA29-4AAB-8BB4-FCF74696ADCF",variableType:4}
 */
var monthStatusActual = 0;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"BAB3B323-3CCA-4890-AF02-61DF5F252F05",variableType:8}
 */
var totalCostMonth = 0.0;
/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"5E4501F1-DD99-465A-A74B-D3594F4E4B85",variableType:8}
 */
var totalProfiMonth = 0.0;

/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"FB3E9C58-F333-426B-81C1-D56334CF7C25"}
 */
function sort(event) {
	//job_order_id asc, job_orders_owner_to_users.users_to_contacts.real_name asc
	// change color back to the original one
	if (label) label.fgcolor = '#727784';
	// (re) set the triggered label
	label = event.getSource();
	// (re) change color to indicate selection
	label.fgcolor = '#990000';
	var sortKey = label.text;
	//application.output(sortKey + ' ' + sortDirection);
	switch (sortKey) {
	case 'Codice':
		controller.sort('external_code_navision' + ' ' + sortDirection);
		break;
	case 'BO':
		controller.sort('customer_order_code' + ' ' + sortDirection);
		break;
	case 'Descrizione':
		controller.sort('job_order_title' + ' ' + sortDirection);
		break;
	case 'Cliente':
		controller.sort('job_orders_to_companies.company_name' + ' ' + sortDirection);
		break;
	case 'Tipo':
		controller.sort('job_orders_to_job_orders_types.job_order_type_description' + ' ' + sortDirection);
		break;
	case 'Stato':
		controller.sort('is_enabled' + ' ' + sortDirection);
		break;
	case 'RDC':
		controller.sort('job_orders_owner_to_users.users_to_contacts.real_name' + ' ' + sortDirection);
		break;
	case 'Profit Center':
		controller.sort('job_orders_to_profit_centers.profit_center_name' + ' ' + sortDirection);
		break;
	case 'Anno':
		controller.sort('job_order_year' + ' ' + sortDirection);
		break;
	case 'Ricavo':
		controller.sort('profit' + ' ' + sortDirection);
		break;
	case 'Costo':
		controller.sort('cost' + ' ' + sortDirection);
		break;
	}

	// toggle the sort order
	sortDirection = (sortDirection == 'asc') ? 'desc' : 'asc';
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"0DF5E357-08C4-455C-8C97-5C1B314098F2"}
 * @AllowToRunInFind
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
	elements.buttonAdd.visible = false;
	elements.buttonDelete.visible = false;
	scopes.globals.actualMonthFilterController = scopes.globals.actualMonth;
	scopes.globals.actualYearFilterController = scopes.globals.actualYear;
	//application.output('actual mese '+foundset.job_orders_to_jo_planning_controller.total_return_actual);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"9BB60492-9C8A-4439-A476-B832A7AA92DA"}
 * @AllowToRunInFind
 */
function confirmAllJOActual(event) {
	application.output(globals.messageLog + 'START jo_list.confirmAllJOActual ', LOGGINGLEVEL.INFO);
	//modificata in modo che vengano inseriti in tabella solo i mercati e non tutti i PC
	var answer = '';
	/** @type {Boolean} */
	var pcFound = false;
	//var pcList = scopes.globals.getProfitCenterByUserManager(scopes.globals.currentUserId);
	var pcList = scopes.globals.getPCOwnerViceForMarket(scopes.globals.currentUserId);
	var pcSelected = forms.jo_filter.profitCenterId;
	//	application.output('pcList: ' + pcList + ' ; pcSelected: ' + pcSelected);

	//non ha selezionato alcun PC
	if (pcSelected == null) {
		application.output(globals.messageLog + 'ERROR jo_list.confirmAllJOActual mercato non selezionato', LOGGINGLEVEL.ERROR);
		globals.DIALOGS.showErrorDialog('Errore', 'Scegliere un Mercato da attualizzare.', 'OK');
		return;
	}

	//ciclo per vedere se il PC scelto è uno dei suoi
	for (var i = 0; i < pcList.length; i++) {
		if (pcList[i] == pcSelected) {
			pcFound = true;
			break;
		}
		//		application.output('pcList[i] = pcSelected: ' + (pcList[i] == pcSelected));
	}

	//PC scelto non è uno dei suoi
	if (pcFound == false) {
		application.output(globals.messageLog + 'ERROR jo_list.confirmAllJOActual non responsabile del mercato scelto', LOGGINGLEVEL.ERROR);
		globals.DIALOGS.showErrorDialog('Errore', 'Puoi attualizzare solo i Mercati di cui sei responsabile.', 'OK');
		return;
	}

	//var pcToSearch = (pcList.length != 0) ? pcList.join('||') : null;
	//	var savedData = null;

	//trovato PC Responsabile
	if (pcList.length != 0) {
		answer = globals.DIALOGS.showQuestionDialog('ATTENZIONE', 'Questa operazione confermerà gli actual di tutte le commesse sul Mercato che hai selezionato.\n\nProcedere con l\'operazione?', 'SI', 'NO')
		//answer = 'SI';
	}

	if (answer != 'SI') return;

	//ciclo sulle commesse abilitate, il cui PC sia quello scelto, che sia billable, e che sia T&M o Progetto o Fornitura
	/** @type {JSFoundSet<db:/geco/job_orders>} */
	var job_orders = databaseManager.getFoundSet('geco', 'job_orders');
	if (job_orders.find()) {
		//job_orders.job_orders_to_profit_centers.profit_center_id = pcToSearch;
		job_orders.profit_center_id = pcSelected;
		job_orders.is_enabled = 1;
		job_orders.job_order_type = '1||2||3';
		job_orders.is_billable = 1;

		job_orders.search();
	}

	/** @type {String[]} */
	var jobNotConfirmedList = []; //lista record non confermati
	/** @type {JSRecord<db:/geco/jo_planning>[]} */
	var plToConfirmList = []; //lista record da confermare
	var countJPtoConfirm = 0;
	var countJPnotActual = 0;

	//ciclo sulle commesse
	for (var index = 1; index <= job_orders.getSize(); index++) {
		var recJO = job_orders.getRecord(index);

		//cerco tra i piani della commessa per mese e anno di actual
		/** @type {JSFoundSet<db:/geco/jo_planning>} */
		var joPl = recJO.job_orders_to_jo_planning;
		if (joPl && joPl.find()) {
			joPl.jo_month = actualMonth;
			joPl.jo_year = actualYear;

			joPl.search();
		}

		//ciclo sui piani della commessa
		for (i = 1; i <= joPl.getSize(); i++) {
			/** @type {JSRecord<db:/geco/jo_planning>} */
			var recPL = joPl.getRecord(i); //salvo record e controllo

			//1=Competenze Ordinarie; 2=Spese Rifatturabili;7=Acquisti Materiali;8=Fornitura TK
			if ([1, 2, 7, 8, 13].indexOf(recPL.jo_planning_to_jo_details.profit_cost_type_id) > -1 && recPL.is_actual == 1 && (recPL.status == 1 || recPL.status == 2)) {
				countJPtoConfirm++;
				//if (recPL.status == 1) {
				plToConfirmList.push(recPL);
				//				application.output('PIANO da confermare: ' + recPL.jo_planning_id);
				//}
			}

			if ([1, 2, 7, 8, 13].indexOf(recPL.jo_planning_to_jo_details.profit_cost_type_id) > -1 && (recPL.is_actual != 1 || recPL.status == 7 || recPL.status == 9)) {
				countJPnotActual++;
				//JS per ora la soluzione del ciclo funziona.. capire perchè con gli array indexOf non va
				//				application.output('Commessa: ' + recJO.job_order_id + '; codice: ' + recJO.external_code_navision + '; PC: ' + recJO.profit_center_id);
				if (jobNotConfirmedList.length == 0) jobNotConfirmedList.push(recPL.jo_planning_to_job_orders.external_code_navision);

				for (var y = 0; y < jobNotConfirmedList.length; y++) {
					if (jobNotConfirmedList[y] != recPL.jo_planning_to_job_orders.external_code_navision) {
						jobNotConfirmedList.push(recPL.jo_planning_to_job_orders.external_code_navision);
						break;
					}
				}
			}
		}
	}
	//messaggio con la lista di commesse che ancora devono essere attualizzate per cui non è possibile confermare il mercato
	if (countJPnotActual > 0) {
		application.output(globals.messageLog + 'ERROR jo_list.confirmAllJOActual numero piani non actual o respinti ' + countJPnotActual, LOGGINGLEVEL.ERROR);
		application.output(globals.messageLog + 'ERROR jo_list.confirmAllJOActual commesse non confermate ' + jobNotConfirmedList, LOGGINGLEVEL.ERROR);
		//application.output('NON CHIUDO IL MERCATO piani non actual o respinti ' + countJPnotActual);
		//		application.output(jobNotConfirmedList);
		globals.DIALOGS.showErrorDialog('ERRORE', 'Non è stato possibile confermare gli actual per le commesse del mercato selezionato: ' + jobNotConfirmedList, 'OK')
		return;
	}

	_super.startEditing(event);
	if (plToConfirmList && plToConfirmList.length > 0) {
		for (var g = 1; g <= job_orders.getSize(); g++) {
			var query = 'call sp_insert_history_jo(?,?,?,?,?,?)';
			var args = [job_orders.job_order_id, scopes.globals.currentUserId, 'before actual by user ' + scopes.globals.currentUserDisplayName, actualMonth, actualYear, 1];
			var dataset = databaseManager.getDataSetByQuery('geco', query, args, -1);
		}

		//confermo i piani actual da RDC
		for (g = 0; g < plToConfirmList.length; g++) {
			var rec = plToConfirmList[g];
			//			application.output('da confermare ' + rec.jo_planning_id + ' stato ' + rec.status + ' actual ' + rec.is_actual)
			//cambio lo stato se 1 passo a 2;
			if (rec.status == 1) rec.status = 2;
			rec.is_actual_confirmed = 1;
			rec.user_actual = scopes.globals.currentUserId;
			rec.date_actual = new Date();
		}

	}
	//tutto è andato a buon fine, inserisco nella tab. mgm_actual_competence un record di log.
	//for (var marketNumber = 0; marketNumber < pcList.length; marketNumber++) {
	/** @type {JSFoundSet<db:/geco/mgm_market_competence>} */
	var mgmActualCompetence = databaseManager.getFoundSet('geco', 'mgm_market_competence');
	/** @type {Number} */
	var totMgmActualCompetence = 0;

	//vado in find sulla tab. mgm_actual_competence
	if (mgmActualCompetence.find()) {
		mgmActualCompetence.competence_month = competenceMonth;
		//mgmActualCompetence.profit_center_id = pcList[marketNumber];
		mgmActualCompetence.profit_center_id = pcSelected;
		totMgmActualCompetence = mgmActualCompetence.search();
	}

	//se non ci sono record, lo creo
	if (totMgmActualCompetence == 0) {
		mgmActualCompetence.newRecord();
		//mgmActualCompetence.profit_center_id = pcList[marketNumber];
		mgmActualCompetence.profit_center_id = pcSelected;
		mgmActualCompetence.competence_month = competenceMonth;
		mgmActualCompetence.actual_confirmation = 1;

		//	savedData = databaseManager.saveData(mgmActualCompetence);
		//		application.output('savedData: ' + savedData + ';\n' + mgmActualCompetence);
	}//prendo record trovato
	else {
		var record = mgmActualCompetence.getRecord(1);

		if (record.actual_confirmation != 1) {
			record.actual_confirmation = 1;
			//			application.output('record.actual_confirmation: ' + record.actual_confirmation);
		}
	}
	//}
	var saved = _super.saveEdits(event);
	if (saved) {
		globals.DIALOGS.showInfoDialog('ESITO', 'Tutti gli actual sono stati confermati per il mese di competenza:\n' + scopes.globals.monthName[actualMonth - 1] + ' ' + actualYear + '.', 'OK')

		//controllo se tutti i mercati sono stati confermati
		/** @type {JSFoundSet<db:/geco/mgm_market_competence>} */
		var mgmActualComp = databaseManager.getFoundSet('geco', 'mgm_market_competence');
		var totalConfirm = 0;
		if (mgmActualComp.find()) {
			mgmActualComp.competence_month = competenceMonth;
			mgmActualComp.actual_confirmation = 1;
			totalConfirm = mgmActualComp.search();
		}
		/** @type {JSFoundSet<db:/geco/profit_centers>} */
		var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');
		var totMarkets = 0;
		if (profit_centers.find()) {
			//tipo mercato, abilitato, nella conta di quelli da considerare per invio mail
			profit_centers.profit_center_type_id = 5;
			profit_centers.is_enabled = 1;
			profit_centers.is_to_check_market_competence = 1;
			totMarkets = profit_centers.search();
		}

		application.output('---------totMarkets: ' + totMarkets, LOGGINGLEVEL.INFO);
		var countPC = 0;

		for (var x = 1; x <= mgmActualComp.getSize(); x++) {
			var recMGM = mgmActualComp.getRecord(x);

			for (var z = 1; z <= profit_centers.getSize(); z++) {
				var recPC = profit_centers.getRecord(z);

				if (recMGM.profit_center_id == recPC.profit_center_id) {
					countPC++;
					break;
				}

			}
		}

		application.output(globals.messageLog + 'jo_list.confirmAllJOActual confermati  pc ' + countPC + ' mercati ' + totMarkets, LOGGINGLEVEL.DEBUG);

		if (countPC == totMarkets) {
			application.output(globals.messageLog + 'jo_list.confirmAllJOActual Invio mail chiusura tutti mercati ', LOGGINGLEVEL.DEBUG);
			//invio mail di conferma tutti mercati a Controller
			scopes.globals.sendConfirmationActualMail(16, scopes.globals.monthName[actualMonth - 1] + ' ' + actualYear);
		}
	}
	updateUI(event);
	application.output(globals.messageLog + 'STOP jo_list.confirmAllJOActual ', LOGGINGLEVEL.INFO);
}

/**
 * @param event
 *
 * @properties={typeid:24,uuid:"9EA8495C-5C64-4F9B-BE6F-1FACD9C40AEA"}
 */
function resetFilter(event) {
	var formFilter = forms.jo_sx.elements.split.getLeftForm();
	formFilter['jobOrderId'] = null;
	formFilter['profitCenterId'] = null;
	formFilter['rdcJo'] = null;
	formFilter['customer'] = null;
	formFilter['statusJo'] = 1;
	formFilter['type'] = null;
	formFilter['yearJo'] = null;
	formFilter['applyFilter']();
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"473EFBCA-E995-4C4A-A343-7D99D0770BAD"}
 * @AllowToRunInFind
 */
function updateUI(event) {
	application.output(globals.messageLog + 'START jo_list.updateUI() ', LOGGINGLEVEL.INFO);
	elements.buttonAllActualConfirm.visible = false;
	elements.buttonAllActualConfirm.enabled = false;
	elements.labelAllActualConfirm.visible = false;
	elements.labelAllActualConfirm.enabled = false;

	application.output('---------role Resp.Mercato? ' + globals.hasRole('Resp. Mercato') + '; role Vice? ' + globals.hasRole('Vice Resp. Mercato'));
	if (globals.hasRole('Resp. Mercato') || globals.hasRole('Vice Resp. Mercato')) {
		var pcSelected = forms.jo_filter.profitCenterId;
		//var pcList = scopes.globals.getProfitCenterByUserManagerForMarket(scopes.globals.currentUserId);
		var pcList = scopes.globals.getPCOwnerViceForMarket(scopes.globals.currentUserId);
		var pcFound = false;

		//ciclo per vedere se il PC scelto è uno dei suoi
		for (var i = 0; i < pcList.length; i++) {
			if (pcList[i] == pcSelected) {
				pcFound = true;
				break;
			}
		}

		application.output('---------pc scelto (' + pcSelected + ') uno dei suoi? ' + pcFound + '; lista dei suoi pc: ' + pcList);
		if (pcFound == true) {
			/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
			var boActualDate = databaseManager.getFoundSet('geco', 'bo_actual_date');
			/** @type {Number} */
			var totBoActualDate = 0;

			//filtro in boActualDate, dovrei trovare almeno un valore
			if (boActualDate.find()) {
				totBoActualDate = boActualDate.search();
			}

			//trovo almeno un risultato
			if (totBoActualDate != 0) {
				var recBoActualDate = boActualDate.getRecord(1);

				isOpenToActual = recBoActualDate.is_open_to_actual;
				competenceMonth = recBoActualDate.actual_date;
				actualMonth = recBoActualDate.actual_month;
				actualYear = recBoActualDate.actual_year;
				monthStatusActual = recBoActualDate.month_status_actual;

				//				application.output('recBoActualDate: ' + recBoActualDate + ';\nisOpenToActual: ' + isOpenToActual + '; monthStatusActual: ' + monthStatusActual);

				var pcToSearch = (pcList.length != 0) ? pcList.join('||') : null;

				/** @type {JSFoundSet<db:/geco/mgm_market_competence>} */
				var mgmActualComp = databaseManager.getFoundSet('geco', 'mgm_market_competence');
				var totalConfirm = 0;
				if (mgmActualComp.find()) {
					mgmActualComp.competence_month = competenceMonth;
					mgmActualComp.profit_center_id = pcToSearch;
					totalConfirm = mgmActualComp.search();
				}

				var totalConfirmedAtOne = 0;

				application.output('---------isOpenToActual: ' + isOpenToActual + ', monthStatusActual: ' + monthStatusActual);
				//se il mese è aperto per attualizzare, faccio vedere i bottoni al Responsabile di Mercato
				//if ( (isOpenToActual == 1 && totalConfirm != pcList.length) && pcList.length > 0) {
				if (isOpenToActual == 1 && (monthStatusActual != 1 && monthStatusActual != 3)) {
					if (totalConfirm == 0) {
						elements.buttonAllActualConfirm.visible = true;
						elements.labelAllActualConfirm.visible = true;
						elements.buttonAllActualConfirm.enabled = true;
						elements.labelAllActualConfirm.enabled = true;
					} else {
						for (var index = 1; index <= mgmActualComp.getSize(); index++) {
							var recMGM = mgmActualComp.getRecord(index);

							//se quello confermato e selezionato ha la conferma a 0 (quindi era stato modificato), glielo faccio vedere da ri-approvare.
							if (recMGM.actual_confirmation == 0 && recMGM.profit_center_id == pcSelected) {
								elements.buttonAllActualConfirm.visible = true;
								elements.labelAllActualConfirm.visible = true;
								elements.buttonAllActualConfirm.enabled = true;
								elements.labelAllActualConfirm.enabled = true;
								//application.output('mercato da confermare: ' + recMGM.profit_center_id);
							} 
							if (recMGM.actual_confirmation == 1) totalConfirmedAtOne++;
						}
						//se ne ho confermati per dire 1 su 2 e la lista dei PC del Resp è fatta almeno di 2 mercati
						if (totalConfirm < pcList.length) {
							mgmActualComp.loadAllRecords();
							if (mgmActualComp.find()) {
								mgmActualComp.profit_center_id = pcSelected;
								var tot = mgmActualComp.search();
							}

							if (tot == 0) {
								elements.buttonAllActualConfirm.visible = true;
								elements.labelAllActualConfirm.visible = true;
								elements.buttonAllActualConfirm.enabled = true;
								elements.labelAllActualConfirm.enabled = true;
								//								application.output('mercato mancante: ' + pcSelected);
							}
						}
					}
				}
				application.output('---------tot.mercati su mgm_market_competence: ' + totalConfirm + '/' + pcList.length + ', confermati: ' + totalConfirmedAtOne);
			}//non ho trovato alcun record nella tabella bo_actual_date --> Controller non ha mai attualizzato i costi.
			else {
				//				application.output('PRIMO ACTUAL COSTI.')
			}
		}
	}
	application.output(globals.messageLog + 'STOP jo_list.updateUI() ', LOGGINGLEVEL.INFO);
}
