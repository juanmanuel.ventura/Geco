/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"D30E9FD0-F064-4161-8D90-228C6F169B1D",variableType:-4}
 */
var valueListDS = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"6AB375B4-8E15-4764-B5EC-FA8B1E619646",variableType:8}
 */
var bo_selected = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F0F5879C-BA2D-4D32-9C39-EB7BA76104EA",variableType:8}
 */
var bo_link_selected = null;

///**
// * @type {Number}
// * @properties={typeid:35,uuid:"33306F0A-5D16-4AE3-B99F-F6F303B9869D",variableType:8}
// */
//var job_order_selected = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"17AC616E-1E37-4735-8A0D-5BE1FD5C6BE2",variableType:8}
 */
var jo_link_selected = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"A930B6B0-4344-46E8-9D63-A24B47D6E946",variableType:4}
 */
var profit_cost_id = null

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"41BD25A2-7477-4BFD-9CF0-987C4ED04EE9",variableType:8}
 */
var standard_figure = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"A8CCDBD4-C41D-457C-B25E-8B978E4E9BC8",variableType:8}
 */
var real_personnel = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"459ACAEC-D648-426E-BA75-806AD7D0656C",variableType:8}
 */
var real_TM = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"9FBEFA0C-2030-42EF-870F-2A08C195A4A9",variableType:8}
 */
var generic_figure = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"8889FEBE-0D31-436E-90E7-6E4DD1AC6AC3",variableType:8}
 */
var real_TK = null;

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"A5B21B7F-DBEC-49C1-AEAC-4F8A714872AE",variableType:-4}
 */
var lockForTab = false;

/**
 * @properties={typeid:35,uuid:"54C363FB-791F-434B-B5F3-B09A7354D10C",variableType:-4}
 */
var intermediate = true;

/**
 * @properties={typeid:35,uuid:"B3B3C5A5-C5AE-4DB2-A424-8960E6A9C900",variableType:-4}
 */
var filterIntemediate = false;

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formName
 * @param {Boolean} toDetails
 * @param {String} type
 *
 * @public
 * @properties={typeid:24,uuid:"5748C82C-DD52-4301-92AC-419256EEF527"}
 */
function showDetailsList(event, formName, toDetails, type) {

	if (toDetails == false && forms.__base.isEditing()) {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!", "OK");
		return;
	}
	application.output(scopes.globals.messageLog + ' BO globals.showDetails ' + formName + ' ' + toDetails + ' ' + type);
	if (type == 'bo') forms.bo_bundle.initialize(event, formName, null);
	if (type == 'jo') forms.jo_bundle.initialize(event, formName, null);

	if (toDetails) {
		if (type == 'bo') forms.bo_details.loadForm(event, 'bo_tab_details', null);
		if (type == 'jo') forms.jo_details.loadForm(event, 'jo_tab_details', null);
	} else {
		//forms[formName].foundset.loadAllRecords();
		// force to apply filter (seems not to work on form's onLoad event)
		var filterForm = forms[formName].elements.split.getLeftForm();
		//application.output('filterForm: ' + filterForm);

		//.getLeftForm()

		if (filterForm && filterForm.applyFilter) {
			//application.output('ritorno al filtro ' + intermediate)
			filterForm.applyFilter();
		}

	}
}
/**
 * Callback method for when solution is opened.
 *
 * @properties={typeid:24,uuid:"8B55D34D-15E0-42EB-A66C-59EED22AF6C0"}
 */
function onSolutionOpenBO() {
	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		application.putClientProperty(APP_WEB_PROPERTY.WEBCLIENT_TEMPLATES_DIR, 'custom');
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_DEFAULT_SCROLLABLE, true);
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_SCROLLABLE_KEEP_LOADED_ROWS, true);
	}
}

/**
 * @param {Number} month
 * @param {Number} year
 * @properties={typeid:24,uuid:"8685ED41-E902-4EC3-B6DB-A92C75092430"}
 * @AllowToRunInFind
 * @return {Number}
 */
function getWorkableDays(month, year) {
	//application.output('Mese ' + month + ' Anno ' + year);
	/** @type {JSFoundSet<db:/geco/calendar_days>} */
	var calendar_days = databaseManager.getFoundSet('geco', 'calendar_days');
	var totDaysMonth = 0;
	if (calendar_days.find()) {
		calendar_days.calendar_month = month;
		calendar_days.calendar_year = year;
		calendar_days.is_working_day = 1;
		calendar_days.is_holiday = 0;
		totDaysMonth = calendar_days.search()
	}
	//application.output('giorni lavorabili ' + totDaysMonth);
	return totDaysMonth;
}

/**
 * @type {Object}
 * @properties={typeid:35,uuid:"1152C538-314A-4628-B1F9-02A92C983882",variableType:-4}
 */
var objTimeSplit = {
	dayWorkable: 0,
	monthDet: 0,
	yearDet: 0
}

/**
 * Calcola i giorni lavorativi per i mesi dell'anno selezionato
 * @param {Date} startDate
 * @param {Date} endDate
 *
 * @return {Array}
 * @properties={typeid:24,uuid:"9730824D-9E15-48C8-AF45-191A6544C0BC"}
 */
function countDayMonthsYears(startDate, endDate) {
	//application.output('inizio ' + startDate + ' fine ' + endDate prova);
	var startMonth = startDate.getMonth() + 1;
	var startYear = startDate.getFullYear();

	var endMonth = endDate.getMonth() + 1;
	var endYear = endDate.getFullYear();
	/** @type {Array} */
	var listObjTimeSplit = [];

	for (var y = startYear; y <= endYear; y++) {
		//application.output(y + ' - ' + endYear);
		var fine = endMonth;
		if (y < endYear) fine = 12
		//application.output('fine ' + fine);
		for (var m = startMonth; m <= fine; m++) {
			//application.output('anno ' + y + ' mese ' + m);
			/** @type {objTimeSplit} */
			var obj = {
				dayWorkable: 0,
				monthDet: 0,
				yearDet: 0
			}
			obj.monthDet = m;
			obj.yearDet = y;
			obj.dayWorkable = getWorkableDays(m, y);
			listObjTimeSplit.push(obj);
		}
		if (y < endYear) startMonth = 1;
	}
	//	for (var x = 0; x < listObjTimeSplit.length; x++) {
	//		application.output(listObjTimeSplit[x]);
	//	}
	return listObjTimeSplit;

}

/**
 * @param {String} valueListName
 * @param {Number} userId
 * @param {String} enrNumber
 *
 * @properties={typeid:24,uuid:"1D4194BA-6B39-4825-99E7-F11CF53A617C"}
 */
function setValuelist(valueListName, userId, enrNumber) {
	valueListDS = null;
	if (userId != null) {
		var sql = '';
		sql = 'SELECT user_cost\
		FROM bo_users_cost \
		WHERE user_id = ' + userId + ' and enrollment_number = ' + enrNumber + ' \
		and user_cost_date_from in (SELECT max(user_cost_date_from) FROM bo_users_cost where user_id = ' + userId + ' and enrollment_number = ' + enrNumber + ' group by enrollment_number)';
		valueListDS = databaseManager.getDataSetByQuery(globals.gecoDb, sql, null, -1);

	} else {
		valueListDS = null;
	}
	//application.output(valueListDS);
	application.setValueListItems(valueListName, valueListDS);
	//application.output('valuelist ' + application.getValueListArray(valueListName))
}

/**
 * @type {Object}
 * @properties={typeid:35,uuid:"7D026CD4-0D7C-4376-88AD-1F3FDF6272D9",variableType:-4}
 */
var objUserCost = {
	uid: null,
	cost: 0.0,
	name: '',
	enrollNum: ''
}

/**
 * @param {Number} userCostID
 * @return {objUserCost}
 * @properties={typeid:24,uuid:"E85EC92C-A289-4076-BF06-FBB44D8CAA6E"}
 */
function getUserCost(userCostID) {
	application.output(scopes.globals.messageLog + ' BO globals.getUserCost for user ' + userCostID, LOGGINGLEVEL.INFO);
	var valueDS = null;
	/** @type {objUserCost} */
	var obj = null;
	//application.output(valueListDS.getRowAsArray(userCostID));
	if (userCostID != null) {
		obj = {
			uid: null,
			cost: null,
			name: '',
			enrollNum: ''
		}
		var sql = '';
		sql = 'select bc.enrollment_number bc_enrollment_number, c.real_name real_name, user_cost, u.user_id \
		from (users u left join bo_users_cost bc on u.user_id = bc.user_id)  \
		join contacts c on u.contact_id = c.contact_id \
		where  bo_users_cost_id = ' + userCostID;
		application.output(scopes.globals.messageLog + ' BO globals.getUserCost sql ' + sql, LOGGINGLEVEL.INFO);
		valueDS = databaseManager.getDataSetByQuery(globals.gecoDb, sql, null, -1);
		//		application.output(valueDS);
		//		application.output(valueDS.getMaxRowIndex())
		for (var index = 1; index <= valueDS.getMaxRowIndex(); index++) {
			var ar = valueDS.getRowAsArray(index);
			//			application.output(ar);
			obj = {
				uid: ar[3],
				cost: ar[2],
				name: ar[1],
				enrollNum: ar[0]
			}
		}
		return obj;
	}
	return null
}

//currentOwnerMarketsList

/**
 * @properties={typeid:24,uuid:"16C004F0-E28A-4D28-BDDE-9C15537EF0F4"}
 */
function setValuelistPC() {
	valueListDS = null;
	var sql = '';
	sql = 'SELECT concat(profit_center_name,"|",profit_center_id)\
		FROM profit_centers';
	valueListDS = databaseManager.getDataSetByQuery(globals.gecoDb, sql, null, -1);

	application.output(valueListDS);
	application.setValueListItems('currentOwnerMarketsList', valueListDS);
	application.output('valuelist ' + application.getValueListArray('currentOwnerMarketsList'))
}

/**
 * @param {Number} userId
 *
 * @properties={typeid:24,uuid:"44B1A748-B756-4C01-9E8B-8B9BA4767418"}
 */
function setValuelistBOSuppCommClients(userId) {
	var valueListSC = null;

	if (userId != null) {
		var sql = '';
		sql = 'select bo_id from business_opportunities b \
	join profit_centers c on c.profit_center_id = b.profit_center_id \
	where c.profit_center_id in  (select pc.profit_center_id from comm_supp_profit_centers pc \
		where pc.user_id = ' + userId + ' group by pc.profit_center_id) \
			and b.company_id in (select cl.customer_id from comm_supp_profit_centers pc, comm_supp_customers cl \
			where pc.comm_supp_profit_center_id = cl.comm_supp_pc_id \
				and pc.user_id = ' + userId + ') \
		and b.final_company_id in (select cl.customer_id from comm_supp_profit_centers pc, comm_supp_customers cl \
			where pc.comm_supp_profit_center_id = cl.comm_supp_pc_id \
				and pc.user_id = ' + userId + ');'
		application.output(scopes.globals.messageLog + ' setValuelistBOSuppCommClients(userId) sql ' + sql, LOGGINGLEVEL.INFO);
		valueListSC = databaseManager.getDataSetByQuery(globals.gecoDb, sql, null, 10000);

	} else {
		valueListSC = null;
	}

	application.output(valueListSC);
	application.setValueListItems('boListSuppCommForMarket', valueListSC);
	application.output('valuelist ' + application.getValueListArray('boListSuppCommForMarket'));
}

/** Se l'utente è owner della BO o del JO in input
 * @AllowToRunInFind
 * @param {Number} userId
 * @param {Number} id
 * @param {String} type
 * @return {Boolean}
 * @properties={typeid:24,uuid:"58564E97-252B-4EBC-B3E5-513A8D0F913B"}
 */
function isOwnerForObject(userId, id, type) {
	var fd;
	if (type == 'bo') {
		/** @type {JSFoundSet<db:/geco/business_opportunities>} */
		fd = databaseManager.getFoundSet('geco', 'business_opportunities');
	} else if (type == 'jo') {
		/** @type {JSFoundSet<db:/geco/job_orders>} */
		fd = databaseManager.getFoundSet('geco', 'job_orders');
	} else {
		application.output(scopes.globals.messageLog + ' globals.isOwnerForObject ERRORE type ' + type + ' non valido', LOGGINGLEVEL.DEBUG);
	}
	if (fd.find()) {
		if (type == 'bo') {
			fd.pc_owner_id = userId;
			fd.bo_id = id;
		}
		if (type == 'jo') {
			fd.user_owner_id = userId;
			fd.job_order_id = id;
		}
		if (fd.search() > 0) return true;
	}
	return false;
}

/** Utente responsabile della commessa in input
 * @AllowToRunInFind
 * @param {Number} userId
 * @param {Number} JOid
 * @return {Boolean}
 * @properties={typeid:24,uuid:"207B376E-ECBD-4839-A6FF-DBD894F4F82D"}
 */
//function isJobOrderUserOwner(userId, JOid) {
//	/** @type {JSFoundSet<db:/geco/job_orders>} */
//	var jo = databaseManager.getFoundSet('geco', 'job_orders');
//	if (jo.find()) {
//		jo.user_owner_id = userId;
//		jo.job_order_id = JOid;
//		if (jo.search() > 0) return true;
//	}
//	return false;
//}

/** Se l'utente è responsabile del centro profitto della BO associata alla commessa in input
 * @AllowToRunInFind
 * @param {Number} userId
 * @param {Number} JOid
 * @return {Boolean}
 * @properties={typeid:24,uuid:"DD53FC5C-82BE-4321-9300-01FA3EB039FB"}
 */
function isBOJobOrderManager(userId, JOid) {
	/** @type {JSFoundSet<db:/geco/job_orders>} */
	var jo = databaseManager.getFoundSet('geco', 'job_orders');
	if (jo.find()) {
		jo.job_orders_to_business_opportunities.pc_owner_id = userId;
		jo.job_order_id = JOid;
		if (jo.search() > 0) return true;
	}
	return false;
}

/** Responsabile di centro profitto della BO o del JO in input
 * @AllowToRunInFind
 * @param {Number} userId
 * @param {Number} id
 * @param {String} type BO or JO
 * @return {Boolean}
 * @properties={typeid:24,uuid:"AE40301C-047A-44B5-8EDB-7BCEE61DC9B6"}
 */
function isPCManagerForObject(userId, id, type) {
	var list = [];
		
	var pcToSearch = '';
	var fd;
	if (type == 'bo') {
		/** @type {JSFoundSet<db:/geco/business_opportunities>} */
		fd = databaseManager.getFoundSet('geco', 'business_opportunities');
		list = getAllProfitCenterByUserManager(userId);
	} else if (type == 'jo') {
		/** @type {JSFoundSet<db:/geco/job_orders>} */
		fd = databaseManager.getFoundSet('geco', 'job_orders');
		list = globals.getProfitCenterByUserManager(userId);
	} else {
		application.output(scopes.globals.messageLog + ' globals.isPCManagerForObject ERRORE type ' + type + ' non valido', LOGGINGLEVEL.DEBUG);
	}
	if (fd.find()) {
		if (list.length == 0) return false;
		pcToSearch = list.join('||');
		fd.profit_center_id = pcToSearch;
		if (type == 'bo')
			fd.bo_id = id;
		if (type == 'jo')
			fd.job_order_id = id;
		if (fd.search() > 0) return true;
	}
	return false;
}

/** Responsabile del centro profitto della commessa in input
 * @AllowToRunInFind
 * @param {Number} userId
 * @param {Number} JOid
 * @return {Boolean}
 * @properties={typeid:24,uuid:"B510A2D0-93BB-4B73-B2F0-42E89DD923EC"}
 */
//function isJOProfitCenterManager(userId, JOid) {
//	/** @type {JSFoundSet<db:/geco/job_orders>} */
//	var jo = databaseManager.getFoundSet('geco', 'job_orders');
//	if (jo.find()) {
//		var list = globals.getProfitCenterByUserManager(userId);
//		if (list.length == 0) return false;
//		var pcToSearch = list.join('||');
//		jo.profit_center_id = pcToSearch;
//		jo.job_order_id = JOid;
//		if (jo.search() > 0) return true;
//	}
//	return false;
//}

/**
 * @param {Number} mailTypeId
 * @param {JSFoundset<db:/geco/users>} user
 * @param {String} joTitle
 * @param {String} note
 * @properties={typeid:24,uuid:"2A24AA20-119D-40A0-9F0D-3D7C2DDCAE9F"}
 */
function sendNotifyMail(mailTypeId, user, joTitle, note) {
	application.output(globals.messageLog + 'START globals.sendNotifyMail() ', LOGGINGLEVEL.INFO);
	var objMail = globals.getMailType(mailTypeId);

	var mailSubs = {
		nome: '',
		commessa: '',
		approvatore: '',
		note: ''
	};

	var recipient = user.users_to_contacts.default_email.channel_value;
	var subject = objMail.subject;
	mailSubs.nome = (user.users_to_contacts.first_name != null) ? user.users_to_contacts.first_name : '';
	mailSubs.commessa = (joTitle != null) ? joTitle : '';
	mailSubs.note = (note != null) ? note : '';
	mailSubs.approvatore = currentuserid_to_users.users_to_contacts.first_name + ' ' + globals.currentuserid_to_users.users_to_contacts.last_name;
	var message = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubs);
	application.output(globals.messageLog + 'globals.sendNotifyMail() SENDING MAIL to ' + user.user_name + ' subject: ' + subject + ' message: ' + message, LOGGINGLEVEL.INFO);

	enqueueMailReminder(recipient, subject, message);
	application.output(globals.messageLog + 'STOP globals.sendNotifyMail() ', LOGGINGLEVEL.INFO);
}

/**
 * @param {Number} mailTypeId
 * @param {String} competenceMonth
 * @properties={typeid:24,uuid:"FCA76F6B-B4F3-48A3-8E67-0089190C6665"}
 */
function sendConfirmationActualMail(mailTypeId, competenceMonth) {
	application.output(globals.messageLog + 'START globals.sendNotifyMail() ', LOGGINGLEVEL.INFO);
	var objMail = globals.getMailType(mailTypeId);

	var mailSubs = {
		competenceMonth: ''
	};
	application.output(competenceMonth);
	var recipient = objMail.distrList;
	var subject = objMail.subject;
	mailSubs.competenceMonth = (competenceMonth != null) ? competenceMonth : '';
	var message = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubs);
	application.output(globals.messageLog + 'globals.sendNotifyMail() SENDING MAIL to distribution list  subject: ' + subject + ' message: ' + message, LOGGINGLEVEL.INFO);

	enqueueMailReminder(recipient, subject, message);
	application.output(globals.messageLog + 'STOP globals.sendNotifyMail() ', LOGGINGLEVEL.INFO);
}

/**
 * @param {Number} userID
 * @param {String} enrollNumber
 * @param {Number} cost
 * @return {Number}

 * @properties={typeid:24,uuid:"0355A8C2-D6E6-4C1C-AE04-FC15D6FB7BA2"}
 */
function getCostID(userID, enrollNumber, cost) {
	application.output(userID + ' ' + enrollNumber + ' ' + cost)
	var sql = 'select bo_users_cost_id, max(bc.user_cost_date_from)  date_from from bo_users_cost bc \
		where  bc.user_id = ? and user_cost=? \
		group by bc.enrollment_number';
	if (enrollNumber != null)
		sql = 'select bo_users_cost_id, max(bc.user_cost_date_from)  date_from	from bo_users_cost bc \
			where  bc.user_id = ? and user_cost=? and bc.enrollment_number = ?\
			group by bc.enrollment_number';
	application.output(sql)
	var valueDS = databaseManager.getDataSetByQuery('geco', sql, [userID, cost, enrollNumber], -1);
	application.output(valueDS)
	return valueDS.getValue(1, 1);
}

/**
 * @param {String} userID
 * @param {String} enrollNumber
 * @return {Object}
 * @properties={typeid:24,uuid:"F3E8E102-9622-438E-B9F8-305077A3ACD8"}
 */
function getLastUserCost(userID, enrollNumber) {
	application.output(messageLog + ' START globals.getLastUserCost() ' + userID + ' ' + enrollNumber)
	var valueDS = null;
	//var cost = 0;
	//application.output(valueListDS.getRowAsArray(userCostID));
	if (userID != null && enrollNumber != null) {
		var sql = '';
		sql = 'select b.user_cost, c.real_name\
			from (bo_users_cost b join users u on b.user_id = u.user_id)	left join contacts c on u.contact_id = c.contact_id  \
			where  b.user_id = ? and b.enrollment_number = ? and b.user_cost_date_from = ( \
				select max(bc.user_cost_date_from)  date_from\
				from bo_users_cost bc\
				where bc.user_id = ? and bc.enrollment_number = ?)';
		application.output(sql)
		valueDS = databaseManager.getDataSetByQuery(globals.gecoDb, sql, [userID, enrollNumber, userID, enrollNumber], -1);
		application.output(valueDS);
		application.output(valueDS.getMaxRowIndex())
		/** @type {objUserCost} */
		var obj = null;
		obj = {
			uid: null,
			cost: 0.0,
			name: '',
			enrollNum: ''
		}
		for (var index = 1; index <= valueDS.getMaxRowIndex(); index++) {
			var ar = valueDS.getRowAsArray(index);
			application.output(ar);
			//cost = ar[0];
			obj.uid = userID;
			obj.cost = ar[0];
			obj.name = ar[1];
			obj.enrollNum = enrollNumber
			break;
		}
	}
	return obj;
}
/**Invia mail al rdm quando viene rifiutato una commessa dal tab demand
 * @param {Number} mailTypeId
 * @param {String} rejectReason
 * @param {String} boTitle
 * @param {String} mailRecipient
 * @param {String} rdm
 * @properties={typeid:24,uuid:"FA99D0E7-BA99-435E-A05B-112DAA23F96B"}
 */
function notifyMailForRejectedBO(mailTypeId, rejectReason, boTitle, mailRecipient, rdm) {
	application.output(globals.messageLog + 'START globals.notifyMailForRejectedBO() ', LOGGINGLEVEL.INFO);

	var objMail = globals.getMailType(mailTypeId);
	var mailSubs = {
		rdm: '',
		bo: '',
		notaRifiuto: ''
	};
	var recipient = (mailRecipient != null) ? mailRecipient : '';
	var subject = objMail.subject;
	mailSubs.rdm = (rdm != null) ? rdm : '';
	mailSubs.notaRifiuto = (rejectReason != null) ? rejectReason : '';
	mailSubs.bo = (boTitle != null) ? boTitle : '';
	var message = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubs);
	application.output(globals.messageLog + 'globals.notifyMailForRejectedBO() SENDING MAIL to ' + recipient + ' subject: ' + subject + ' message: ' + message, LOGGINGLEVEL.INFO);
	enqueueMailReminder(recipient, subject, message);
	application.output(globals.messageLog + 'STOP globals.notifyMailForRejectedBO() ', LOGGINGLEVEL.INFO);
}
