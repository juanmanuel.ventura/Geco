/**
 * @type {String}
 * @properties={typeid:35,uuid:"AB56E340-7AB0-4214-8F5E-338DC7F0B9E6"}
 */
var recipient = "";

/**
 * @type {String}
 * @properties={typeid:35,uuid:"9B8BE670-0E12-40CF-B943-3DF498680C5D"}
 */
var username = "";

/**
 * @type {String}
 * @properties={typeid:35,uuid:"527BC396-1AAD-437F-A183-66A7DB45D033"}
 */
var subject = "";

/**
 * @type {String}
 * @properties={typeid:35,uuid:"4B443A56-B954-46C3-A672-D658B8CAC4D3"}
 */
var messagage = "";

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"FC0106A1-B1EF-41B7-BEE0-2F2E59856909"}
 */
function send(event) {
	if (plugins.mail.isValidEmailAddress(recipient) && 
		subject !=null && messagage != null) {
			globals.mpEnqueueMailReminder(recipient, subject, messagage);
	}
	else {
		application.output('Error sending mail: not valid input');
	}
}
