/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"B5EC4B1C-6148-4931-AD8D-43F1E51C9329",variableType:4}
 */
var toClose = 0;

/**
 * Handle changed data.

 * @private
 *
 * @properties={typeid:24,uuid:"9B4C5EF4-3E21-4094-9703-04BF16419163"}
 */
function checkToClose() {
	if (globals.mpSolutionToGo != '') {
		application.closeSolution(globals.mpSolutionToGo);
	}
	else {
		application.closeAllWindows();
		application.closeSolution();
	}
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"10B3F0B1-CB1B-4515-AE15-FDF497EE7D89"}
 */
function onLoad(event) {
	application.output('caricata ' +toClose);
	if (toClose == 1) {
	application.sleep(10000);
	application.output('chiudo');
	application.closeSolution();
	}
}
