/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6C3C9BC1-1E8B-41E2-8C22-410E4C6633FE"}
 */
var mpResultString = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"704C3C8D-255A-4565-8DD8-306C298D779B"}
 */
var mpSolutionToGo = '';

/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
 * @return {Object}
 *
 * @properties={typeid:24,uuid:"9D699EE8-3A01-4B31-BC41-C720A5CC8558"}
 */
function mpSendMail(recipient, subject, message) {
	var result = {
		recipient: recipient,
		subject: subject,
		message: message,
		success: true
	};

	var properties = [];
	properties.push('mail.smtp.host=smtp.gmail.com');
	properties.push('mail.smtp.auth=true');
	properties.push('mail.smtp.username=' + mpMailOptions.username);
	properties.push('mail.smtp.password=' + mpMailOptions.password);
	properties.push('mail.smtp.port=587');
	properties.push('mail.smtp.starttls.enable=true');

	var success = plugins.mail.sendMail(recipient, '[PRATICHE SANITARIE] <noreply@spindox.it>', subject, message, null, null, null, properties);
	if (!success) {
		application.output(plugins.mail.getLastSendMailExceptionMsg());
		result.success = false;
		result.message = 'Errore invio mail';
	}
	return result;
}

/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
 * @return {Object}
 *
 * @properties={typeid:24,uuid:"AC193813-E274-4A2C-9C3E-8626EDD14EFA"}
 */
function mpSendMailPassword(recipient, subject, message) {
	var result = {
		recipient: recipient,
		subject: subject,
		message: message,
		success: true
	};

	var properties = [];
	properties.push('mail.smtp.host=smtp.gmail.com');
	properties.push('mail.smtp.auth=true');
	properties.push('mail.smtp.username=' + mpMailOptions.username);
	properties.push('mail.smtp.password=' + mpMailOptions.password);
	properties.push('mail.smtp.port=587');
	properties.push('mail.smtp.starttls.enable=true');

	var success = plugins.mail.sendMail(recipient, '[PRATICHE SANITARIE] <noreply@spindox.it>', subject, message, null, null, null, properties);
	if (!success) {
		application.output(plugins.mail.getLastSendMailExceptionMsg());
		result.success = false;
	}
	return result;
}

/**
 * @param {String} username
 * @param {String} urlCaller
 * @param {String} solutionName
 * @return {Object}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"E3F6F466-D18E-4D54-B3E0-0F1AFC199192"}
 */
function mpSendMailResetPassword(username, urlCaller, solutionName) {
	var result = {
		message: '',
		success: true
	};
	application.output(globals.mpMessageLog + ' START mp_queue.mpSendMailResetPassword() username ' + username + ' serverUrl chiamante ' + urlCaller + ' solution ' + solutionName, LOGGINGLEVEL.INFO);
	var recipient, subject, message = null;
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
	var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');

	var user = null;
	if (users.find()) {
		users.user_name = username;
		if (users.search() == 1) {
			user = users.getRecord(1);
			if (user.is_enabled == 0) {
				result.success = false;
				result.message = 'Utente disabilitato';
			} else {
				var text = "";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

				for (var i = 0; i < 8; i++) {
					text += possible.charAt(Math.floor(Math.random() * possible.length));
				}
				application.output(text);
				/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_user_password_reset>} */
				var user_password_reset = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_user_password_reset');
				user_password_reset.newRecord();
				user_password_reset.user_id = user.user_id;
				user_password_reset.reset_id = text;
				databaseManager.saveData();
				//var hostname = application.getServerURL();
				var hostname = urlCaller;
				application.output(globals.mpMessageLog + ' mp_queue.mpSendMailResetPassword() serverUrl ' + hostname, LOGGINGLEVEL.INFO);
				//{solutionName}&m={methodName}&a={value}&{param1}={value1}&{param2}={value2}
				var link = hostname + '/servoy-webclient/solutions/solution/pratiche_sanitarie_mp_queue/method/metodo/argument/' + user_password_reset.user_id + '|' + user_password_reset.reset_id + '|' + solutionName;
				application.output(link);
				//mail template
				var objMail = globals.mpGetMailType(8);
//				/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_insured_users>} */
//				var mp_insured_users = databaseManager.getFoundSet('geco', 'mp_insured_users');
//				if (mp_insured_users.find()) {
//					mp_insured_users.email = username;
//					var tot = mp_insured_users.search();
//
//					if (tot > 0) {
//						recipient = mp_insured_users.getRecord(1).email;
//					} else {
//						result.success = false;
//						result.message = 'Email non trovata; Crea l\'anagrafica.';
//						application.output(globals.mpMessageLog + ' STOP mp_queue.mpSendMailResetPassword() KO: email utente non trovata', LOGGINGLEVEL.INFO);
//						return result;
//					}
//				}
				//				recipient = user.mp_default_email.channel_value;
				recipient = user.user_email;
				subject = objMail.subject;
				globals.mailSubstitution.nome = user.mp_users_to_mp_insured_users.first_name;
				//globals.mailSubstitution.nome = user.real_name;
				globals.mailSubstitution.link = link;
				message = plugins.VelocityReport.evaluateWithContext(objMail.text, globals.mailSubstitution);

				//message = "Ciao " + user.users_to_contacts.first_name + "\nper ricevere la nuova password segui il link:\n\n " + link + "\n\nHelpdesk Geco";
				application.output(globals.mpMessageLog + ' mp_queue.mpSendMailResetPassword() sending mail to ' + recipient, LOGGINGLEVEL.INFO);
				result = mpSendMail(recipient, subject, message);
			}
		} else {
			result.success = false;
			result.message = 'Utente non trovato.';
			application.output(globals.mpMessageLog + ' mp_queue.mpSendMailResetPassword() Utente non trovato ' + username, LOGGINGLEVEL.INFO);
		}
	}

	application.output(globals.mpMessageLog + ' STOP mp_queue.mpSendMailResetPassword() risultato ' + result.success + result.message, LOGGINGLEVEL.INFO);
	return result;
}

/**
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"3BED176A-D9C1-4730-B7D7-816C4A09AE9F"}
 */
function mpMetodo() {
	/** @type {Array} */
	var vArguments = arguments[0].split('|');

	var recipient = null;
	var subject = null;
	var message = null;
	/** @type {Object} */
	var result = null;
	application.output(globals.mpMessageLog + ' START mp_queue.metodo() input ' + vArguments, LOGGINGLEVEL.INFO);
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_user_password_reset>} */
	var pwd_reset = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_user_password_reset');
	//var serverUrl = getServerUrlGeco(application.getServerURL());
	//var addressPage = '/application_server/server/webapps/ROOT/servoy-webclient/templates/default/';
	// pageToRedirect = serverUrl + addressPage + '';
	if (pwd_reset.find()) {
		pwd_reset.user_id = vArguments[0];
		pwd_reset.reset_id = vArguments[1];
		pwd_reset.is_verified = 0;
		if (pwd_reset.search() == 1) {
			/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
			var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
			if (users.find()) {
				users.user_id = pwd_reset.user_id;
				users.is_enabled = 1;
				if (users.search() == 1) {
					var newPwd = "";
					var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

					for (var i = 0; i < 8; i++) {
						newPwd += possible.charAt(Math.floor(Math.random() * possible.length));
					}

					var objMail = globals.mpGetMailType(9);
					recipient = users.mp_default_email.channel_value;
					subject = objMail.subject;
					globals.mailSubstitution.nome = users.mp_users_to_mp_insured_users.first_name;
					globals.mailSubstitution.newPwd = newPwd;
					message = plugins.VelocityReport.evaluateWithContext(objMail.text, globals.mailSubstitution);
					application.output(globals.mpMessageLog + 'mp_queue.metodo() sending mail to ' + recipient, LOGGINGLEVEL.INFO);
					result = mpSendMail(recipient, subject, message);

					//					recipient = users.users_to_contacts.default_email.channel_value;
					//					subject = 'Geco invio pwd';
					//					message = 'nuova password ' + newPwd;
					//					result = globals.sendMailPassword(recipient,subject,message);
					if (result.success == true) {

						//						//inserisco la nuova password nell'history
						//						/** @type {JSFoundSet<db:/pratiche_sanitarie/users_password_expiration>} */
						//						var userspwdex = databaseManager.getFoundSet('pratiche_sanitarie', 'users_password_expiration');
						//						var insertResutl = userspwdex.insertNewPassword(users.user_id, globals.sha1(newPwd), 0, true);
						//						if (insertResutl == false) {
						//							resultString = 'Geco - Errore nel reset della password';
						//							application.output(globals.messageLog + 'mp_queue.metodo() errore inserimento history ',LOGGINGLEVEL.INFO);
						//							//pageToRedirect = serverUrl + addressPage + 'GecoSendMailKO.html'
						//						}
						users.user_password = globals.mpSha1(newPwd);
						application.output(globals.mpMessageLog + 'mp_queue.metodo() password cambiata ', LOGGINGLEVEL.INFO);
						//						users.has_requested_reset = 0;
						//						if (users.is_locked == 1) {
						//							application.output(globals.messageLog + 'mp_queue.metodo() utente sbloccato ',LOGGINGLEVEL.INFO);
						//							users.is_locked = 0;
						//							users.failed_login_attempts = 0;
						//						}
						pwd_reset.is_verified = 1;
						databaseManager.saveData(pwd_reset);
						//						databaseManager.saveData(userspwdex);
						//						application.output(globals.messageLog + 'mp_queue.metodo() salvata password expiration ',LOGGINGLEVEL.INFO);
						databaseManager.saveData(users);
						mpResultString = 'Pratiche Sanitarie - Nuova password inviata';
						if (vArguments.length > 2) {
							mpSolutionToGo = vArguments[2];
						}
						//pageToRedirect = serverUrl + addressPage + 'GecoSendMailOK.html'
					}
				} else {
					mpResultString = 'Pratiche Sanitarie - Errore nel reset della password';
					application.output(globals.mpMessageLog + 'mp_queue.metodo() ERRORE ' + mpResultString, LOGGINGLEVEL.INFO);
					mpSolutionToGo = '';
					//pageToRedirect = serverUrl + addressPage + 'GecoSendMailKO.html'
				}
			}

		} else {
			mpResultString = 'Pratiche Sanitarie - Errore nel reset della passwrod: Token gia utilizzato o non trovato';
			application.output(globals.mpMessageLog + 'mp_queue.metodo() ERRORE ' + mpResultString, LOGGINGLEVEL.INFO);
			mpSolutionToGo = '';
			//pageToRedirect = serverUrl + addressPage + 'GecoSendMailTokenKO.html'
			//application.output(pageToRedirect);
		}
	}
	forms.mp_resetPassword.toClose = 1;
}

/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
 * @param {String} bcc
 * @return {Object}
 *
 * @properties={typeid:24,uuid:"6FF377BA-61E3-450B-A801-167458E8F7AB"}
 */
function mpSendMailBcc(recipient, subject, message, bcc) {
	var result = {
		recipient: recipient,
		subject: subject,
		message: message,
		bcc: bcc,
		success: true
	};

	var properties = [];
	properties.push('mail.smtp.host=smtp.gmail.com');
	properties.push('mail.smtp.auth=true');
	properties.push('mail.smtp.username=' + mpMailOptions.username);
	properties.push('mail.smtp.password=' + mpMailOptions.password);
	properties.push('mail.smtp.port=587');
	properties.push('mail.smtp.starttls.enable=true');

	var success = plugins.mail.sendMail(recipient, '[GECO] <noreply@spindox.it>', subject, message, null, bcc, null, properties);
	if (!success) {
		application.output(plugins.mail.getLastSendMailExceptionMsg());
		result.success = false;
		result.message = 'Errore invio mail';
	}
	return result;
}

// * @param {plugins.mail.Attachment} attach
/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
 * @param {String} ccRecipient
 * @param {String} filePath
 * @param {String} filename
 * @return {Object}
 *
 * @properties={typeid:24,uuid:"5FBF9F61-D13C-46AA-9463-38C183967DD1"}
 */
function mpSendMailAttach(recipient, subject, message, ccRecipient, filePath, filename) {
	application.output(filename)
	var attachment = null
	if (filename != null)
		attachment = plugins.mail.createBinaryAttachment(filename, plugins.file.readFile(filePath), 'application/vnd.ms-excel');

	application.output('attach ' + attachment);
	var result = {
		recipient: recipient,
		subject: subject,
		message: message,
		success: true
	};

	var properties = [];
	properties.push('mail.smtp.host=smtp.gmail.com');
	properties.push('mail.smtp.auth=true');
	properties.push('mail.smtp.username=' + mpMailOptions.username);
	properties.push('mail.smtp.password=' + mpMailOptions.password);
	properties.push('mail.smtp.port=587');
	properties.push('mail.smtp.starttls.enable=true');
	application.output('prima')
	var success = plugins.mail.sendMail(recipient, '[PRATICHE SANITARIE] <noreply@spindox.it>', subject, message, ccRecipient, null, attachment, properties);
	application.output('sendMailAttach ' + success)
	if (!success) {
		application.output(plugins.mail.getLastSendMailExceptionMsg());
		result.success = false;
		result.message = 'Errore invio mail';
	}
	plugins.file.deleteFile(filePath);
	return result;
}

/**
 * @param details
 * @param {String} urlCaller
 * @return {Object}
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"EDE5DFA5-F687-4C84-895E-FF35406D2C8A"}
 */
function createMPUserFromGecoPersonnel(details, urlCaller) {
	application.output(globals.mpMessageLog + 'START mp_queue.createMPUserFromGecoPersonnel() serverUrl chiamante ' + urlCaller, LOGGINGLEVEL.INFO);
	var result = {
		message: '',
		success: true
	};
	var userName = details.user_name;
	var password = details.user_password;
	var realName = details.real_name;
	var firstName = details.first_name;
	var lastName = details.last_name;
	/** @type {String} */
	var fepCode = details.fep_code.toString();
	var companyName = details.company_name;
	var eMail = details.e_mail;
	var startDate = details.start_date;
	var endDate = details.end_date;
	var ibanCode = details.iban;
	var gecoUserId = details.geco_user_id;
	var fiscalCode = details.fiscal_code;
	//	/** @type {String} */
	//	var startDateStr = details.start_date.toString();
	//	var startDate = utils.parseDate(startDateStr, 'yyyy-MM-dd');
	//	/** @type {String} */
	//	var endDateStr = details.end_date.toString();
	//	var endDate = utils.parseDate(endDateStr, 'yyyy-MM-dd');
	application.output(globals.mpMessageLog + 'mp_queue.createMPUserFromGecoPersonnel() Parametri; userName: ' + userName + '; password: ' + password + '; realName: ' + realName + '; firstName: ' + firstName + '; lastName: ' + lastName + '; fepCode: ' + fepCode + '; companyName: ' + companyName + '; eMail: ' + eMail + '; startDate: ' + startDate + '; endDate: ' + endDate + '; iban: ' + ibanCode + '; gecoUserId: ' + gecoUserId + '; fiscalCode: ' + fiscalCode, LOGGINGLEVEL.INFO);
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
	var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_insured_users>} */
	var insured_users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_insured_users');
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users_roles>} */
	var users_roles = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users_roles');
	var userIdUsers = null;
	var userSaved = false;
	var usersRolesSaved = false;
	var insuredUserSaved = false;
	try {
		application.output(globals.mpMessageLog + 'mp_queue.createMPUserFromGecoPersonnel() Sto per creare utente in users...', LOGGINGLEVEL.DEBUG);
		if (users.find()) {
			users.user_name = userName;
			users.user_password = password;
			var totUsers = users.search();
			//paraculo se ha per sbaglio ripetuto la procedura di inserimento e in users c'è già
			if (totUsers > 0) {
				userSaved = false;
				var rec = users.getRecord(1);
				userIdUsers = rec.user_id;
				//se geco_user_id è null, vuol dire che è prima creazione come assicurato; allora gli metto la mail di SPINDOX
				if (rec.geco_user_id == null) {
					databaseManager.setAutoSave(false);
					rec.geco_user_id = gecoUserId;
					if (rec.company_name != companyName) rec.company_name = companyName;
					if (rec.fep_code != fepCode) rec.fep_code = fepCode;
					if (rec.user_password != password) rec.user_password = password;
					userSaved = databaseManager.saveData(rec);
					/** @type {Boolean} */
					var modified = userSaved;
				}
				application.output(globals.mpMessageLog + 'mp_queue.createMPUserFromGecoPersonnel() utente creato in users? ' + userSaved + ', esiste già\' in users; modificato? ' + modified, LOGGINGLEVEL.DEBUG);
			} else {
				databaseManager.setAutoSave(false);
				users.newRecord(false);
				//salvo id del record appena creato
				userIdUsers = users.user_id;
				//				scopes.globals.mpUserId = users.user_id;
				//				application.output('------------------------scopes.globals.mpUserId: ' + scopes.globals.mpUserId);
				users.user_name = userName;
				users.user_password = password;
				users.real_name = realName;
				users.fep_code = utils.stringToNumber(fepCode);
				users.company_name = companyName;
				users.geco_user_id = gecoUserId;
				userSaved = databaseManager.saveData(users);
				application.output(globals.mpMessageLog + 'mp_queue.createMPUserFromGecoPersonnel() creato utente in users? ' + userSaved, LOGGINGLEVEL.DEBUG);
			}
		}
		application.output(globals.mpMessageLog + 'mp_queue.createMPUserFromGecoPersonnel() Sto per creare utente in users_roles...', LOGGINGLEVEL.DEBUG);
		//CERCO IN USERS_ROLES SE C'E' GIA' UTENTE COME ASSICURATO
		if (users_roles.find()) {
			users_roles.user_id = userIdUsers;
			users_roles.role_id = 3; //INSURED
			var totUsersRoles = users_roles.search();
			//paraculo se ha per sbaglio ripetuto la procedura di inserimento e in users_roles c'è già
			if (totUsersRoles > 0) {
				usersRolesSaved = false;
				application.output(globals.mpMessageLog + 'mp_queue.createMPUserFromGecoPersonnel() utente creato in users_roles? ' + usersRolesSaved + ', esiste già\' in users_roles', LOGGINGLEVEL.DEBUG);
			} else {
				databaseManager.setAutoSave(false);
				users_roles.newRecord(false);
				users_roles.user_id = userIdUsers;
				users_roles.role_id = 3;
				usersRolesSaved = databaseManager.saveData(users_roles);
				application.output(globals.mpMessageLog + 'mp_queue.createMPUserFromGecoPersonnel() utente creato in users_roles? ' + usersRolesSaved, LOGGINGLEVEL.DEBUG);
			}
		}
		application.output(globals.mpMessageLog + 'mp_queue.createMPUserFromGecoPersonnel() Sto per creare utente in insured_users...', LOGGINGLEVEL.DEBUG);
		if (insured_users.find()) {
			insured_users.user_id = userIdUsers;
			var totInsuredUsers = insured_users.search();
			//paraculo se ha per sbaglio ripetuto la procedura di inserimento e in insured_users c'è già
			if (totInsuredUsers > 0) {
				insuredUserSaved = false;
				application.output(globals.mpMessageLog + 'mp_queue.createMPUserFromGecoPersonnel() utente creato in insured_users? ' + insuredUserSaved + ', esiste già\' in insured_users', LOGGINGLEVEL.DEBUG);
			} else {
				/** @type {String} */
				var numToContact = '0';
				var maxLength = 7;
				var difference = 0;
				var progressiveNumber = fepCode;
				if (progressiveNumber.length < maxLength) {
					difference = maxLength - progressiveNumber.length;
					for (var i = 0; i < difference; i++) {
						progressiveNumber = numToContact.concat(progressiveNumber);
					}
				}
				var numberCode = progressiveNumber;
				databaseManager.setAutoSave(false);
				insured_users.newRecord(false);
				insured_users.user_id = userIdUsers;
				insured_users.company_id = 1;
				insured_users.email = eMail;
				insured_users.start_date = startDate;
				insured_users.end_date = endDate;
				insured_users.benefit_package = 'SPIND';
				insured_users.is_user_enabled = 1;
				insured_users.first_name = firstName;
				insured_users.last_name = lastName;
				insured_users.real_name = realName;
				insured_users.number_code = numberCode;
				insured_users.iban_code = ibanCode != null ? ibanCode : null;
				insured_users.fiscal_code = fiscalCode != null ? fiscalCode : null;
				insuredUserSaved = databaseManager.saveData(insured_users);
				application.output(globals.mpMessageLog + 'mp_queue.createMPUserFromGecoPersonnel() utente creato in insured_users? ' + insuredUserSaved, LOGGINGLEVEL.DEBUG);
			}
		}
		application.output(globals.mpMessageLog + 'mp_queue.createMPUserFromGecoPersonnel() users correctly created in tables? ' + userSaved + ', ' + usersRolesSaved + ', ' + insuredUserSaved, LOGGINGLEVEL.DEBUG);
		application.output(globals.mpMessageLog + 'STOP mp_queue.createMPUserFromGecoPersonnel() risultato ' + result.success + ' ' + result.message, LOGGINGLEVEL.INFO);
		//		if (userSaved && usersRolesSaved && insuredUserSaved) result.message = 'Utente creato con successo.';
		if (usersRolesSaved && insuredUserSaved) {
			result.message = 'Utente creato con successo.';
			result.success = true;
		} else {
			result.message = 'L\'utente esisteva già come assicurato in Pratiche Sanitarie, non è stato salvato.';
			result.success = false;
			application.output(globals.mpMessageLog + ' STOP mp_queue.createMPUserFromGecoPersonnel() KO: ' + result.message, LOGGINGLEVEL.DEBUG);
		}
		return result;
	} catch (e) {
		result.success = false;
		result.message = e.message;
		application.output(globals.mpMessageLog + ' STOP mp_queue.createMPUserFromGecoPersonnel() KO risultato ' + result.success + result.message, LOGGINGLEVEL.INFO);
		return result;
	}
}

/**
 * @param details
 * @param {String} urlCaller
 * @return {Object}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"B1506B72-C5EB-41DD-98D8-20AE59956A05"}
 */
function modifyMPUserFromGecoPersonnel(details, urlCaller) {
	application.output(globals.mpMessageLog + 'START mp_queue.modifyMPUserFromGecoPersonnel() serverUrl chiamante ' + urlCaller, LOGGINGLEVEL.INFO);
	var result = {
		message: '',
		success: true
	};
	var userName = details.user_name;
	//	var endDateStr = details.end_date;
	var endDate = details.end_date;
	var insuredUserSaved = false;
	//	var endDate = utils.parseDate(endDateStr, 'yyyy-MM-dd');
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
	var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_insured_users>} */
	var insured_users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_insured_users');

	try {
		application.output(globals.mpMessageLog + 'mp_queue.modifyMPUserFromGecoPersonnel() Sto per cercare utente in users per username: ' + userName + '...', LOGGINGLEVEL.DEBUG);
		if (users.find()) {
			users.user_name = userName;
			var totUsers = users.search();
		}
		application.output(globals.mpMessageLog + 'mp_queue.modifyMPUserFromGecoPersonnel() users trovati: ' + totUsers, LOGGINGLEVEL.DEBUG);
		if (totUsers > 0) {
			var recUser = users.getRecord(1);
			//			scopes.globals.mpUserId = recUser.user_id;
			//			application.output('------------------------scopes.globals.mpUserId: ' + scopes.globals.mpUserId);
			application.output(globals.mpMessageLog + 'mp_queue.modifyMPUserFromGecoPersonnel() Sto per cercare utente in insured_users per user.user_id: ' + recUser.user_id + '...', LOGGINGLEVEL.DEBUG);
			if (insured_users.find()) {
				insured_users.user_id = recUser.user_id;
				var totInsuredUsers = insured_users.search();
			}
			application.output(globals.mpMessageLog + 'mp_queue.modifyMPUserFromGecoPersonnel() insured_users trovati: ' + totInsuredUsers, LOGGINGLEVEL.DEBUG);
			if (totInsuredUsers > 0) {
				var recInsuredUser = insured_users.getRecord(1);
				if (recInsuredUser.end_date != endDate) {
					databaseManager.setAutoSave(false);
					recInsuredUser.end_date = endDate;
					insuredUserSaved = databaseManager.saveData(insured_users);
				} else insuredUserSaved = false;
				application.output(globals.mpMessageLog + 'mp_queue.modifyMPUserFromGecoPersonnel() Data fine copertura modificata in insured_users? ' + insuredUserSaved, LOGGINGLEVEL.DEBUG);
			}
		}
		if (insuredUserSaved) result.message = 'Data di Fine Copertura Assicurativa modificata con successo.';
		else result.message = 'La vecchia data di Fine Copertura Assicurativa è uguale a quella inserita, non è stata salvata.';
		return result;
	} catch (e) {
		result.success = false;
		result.message = e.message;
		application.output(globals.mpMessageLog + ' STOP mp_queue.modifyMPUserFromGecoPersonnel() KO risultato ' + result.success + result.message, LOGGINGLEVEL.INFO);
		return result;
	}
}

/**
 * @param details
 * @param {String} urlCaller
 * @return {Object}
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"E4090894-D9BB-4141-A6B5-4198C525F0B4"}
 */
function modifyMPUserPasswordFromGeco(details, urlCaller) {
	application.output(globals.mpMessageLog + 'START mp_queue.modifyMPUserPasswordFromGeco() serverUrl chiamante ' + urlCaller, LOGGINGLEVEL.INFO);
	var result = {
		message: '',
		success: true
	};
	var gecoUserId = details.geco_user_id;
	var userPassword = details.user_password;
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
	var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');

	try {
		application.output(globals.mpMessageLog + 'mp_queue.modifyMPUserPasswordFromGeco() Sto per cercare utente in users per gecoUserId: ' + gecoUserId, LOGGINGLEVEL.DEBUG);
		if (users.find()) {
			users.geco_user_id = gecoUserId;
			var totUsers = users.search();
		}
		application.output(globals.mpMessageLog + 'mp_queue.modifyMPUserPasswordFromGecol() users trovati: ' + totUsers, LOGGINGLEVEL.DEBUG);
		if (totUsers > 0) {
			var recUser = users.getRecord(1);
			application.output(globals.mpMessageLog + 'mp_queue.modifyMPUserPasswordFromGeco() Password PRIMA: ' + recUser.user_password, LOGGINGLEVEL.DEBUG);
			databaseManager.setAutoSave(false);
			recUser.user_password = userPassword;
			var userSaved = databaseManager.saveData(recUser);
			application.output(globals.mpMessageLog + 'mp_queue.modifyMPUserPasswordFromGeco() Password DOPO: ' + recUser.user_password, LOGGINGLEVEL.DEBUG);
		} else {
			//not found
			result.success = false;
			result.message = 'L\'utente su Pratiche Sanitarie non è stato trovato, password non modificata.';
			return result;
		}
		if (userSaved) result.message = 'La password su Pratiche Sanitarie è stata modificata con successo.';
		else result.message = 'La password su Pratiche Sanitarie non è stata modificata. Contattare l\'helpdesk.';
		return result;
	} catch (e) {
		result.success = false;
		result.message = e.message;
		application.output(globals.mpMessageLog + ' STOP mp_queue.modifyMPUserPasswordFromGeco() KO risultato ' + result.success + result.message, LOGGINGLEVEL.INFO);
		return result;
	}
}
