/**
 * @properties={typeid:24,uuid:"2D3083C9-AD06-411B-81C9-20308255D2A1"}
 */
function initializeHRAdministration() {
	if (globals.hasRole('HR Expenses') || globals.hasRole('HR Administrators')) {
		databaseManager.addTableFilterParam('geco', 'users', 'user_type_id', 'in', [6, 7, 10]);
	}
}

/**
 * Callback method for when solution is opened.
 * @properties={typeid:24,uuid:"9F891A5F-5CAB-46B0-B9CA-9C9BDE7B8636"}
 */
function onSolutionOpenPersonnel() {
	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		application.putClientProperty(APP_WEB_PROPERTY.WEBCLIENT_TEMPLATES_DIR, 'custom');
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_DEFAULT_SCROLLABLE, true);
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_SCROLLABLE_KEEP_LOADED_ROWS, true);
	}
	initializeHRAdministration();

}

/**
 * @param{Number} t_month
 * @param{Number} t_year
 *
 * @properties={typeid:24,uuid:"B37BCAA4-78C0-4788-BA4F-89A634107EF8"}
 * @AllowToRunInFind
 */
function checkOvertime(t_month, t_year) {
	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log_rejected = databaseManager.getFoundSet('geco', 'events_log');

	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log_closed = databaseManager.getFoundSet('geco', 'events_log');

	/** @type {JSFoundSet<db:/geco/events_log_overtime>} */
	var events_log_overtime = databaseManager.getFoundSet('geco', 'events_log_overtime');

	if (events_log_rejected.find()) {
		events_log_rejected.event_id = [27, 28, 38];
		events_log_rejected.event_log_status = 3;
		events_log_rejected.event_month = t_month;
		events_log_rejected.event_year = t_year;
		events_log_rejected.search();
	}
	events_log_rejected.sort('user_id, event_log_date');
	var dateOld = null;
	var dateNext = null;
	var toDelete = [];
	var userOld = null;
	var userNext = null;
	var totRejected = events_log_rejected.getSize()

	application.output('START processDeleteOvertime - record.id: ' + events_log_rejected.event_log_id, LOGGINGLEVEL.DEBUG);

	/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
	var events_log_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');

	var tot = 0;

	for (var index = events_log_rejected.getSize(); index >= 1; index--) {
		var record = events_log_rejected.getRecord(index);
		application.output('START processDeleteOvertime - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
		//potrei chiamare la process
		databaseManager.setAutoSave(true);
		//scopes.rulesFor_events_log.processDeleteOvertime(record);
		if (!globals.hasRole('Controllers') && !globals.hasRole('HR Administrators')) {
			if (record.event_log_status != 4 && record.event_log_status != 2) {
				events_log_overtime.deleteOvertime(record);
				if (record.events_log_to_events.event_id == 28) {
					events_log_additional_charge.deleteAdditionalCharge(record, 1);
				}
			} else throw new Error('- Non è possibile cancellare eventi approvati o chiusi');
		} else {
			events_log_overtime.deleteOvertime(record);
			if ( (record.events_log_to_events.event_id == 28)) {
				events_log_additional_charge.deleteAdditionalCharge(record, 1);
			}
		}

		var totOK = 0;
		try {
			if (events_log_closed.find()) {
				events_log_closed.user_id = record.user_id;
				events_log_closed.event_log_date = record.event_log_date;
				events_log_closed.event_id = [27, 28, 38]; //lavoro straordinario o intervento straordinario di reperibilità
				events_log_closed.event_log_id = '!' + record.event_log_id;
				events_log_closed.event_log_status = '!3';
				totOK = events_log_closed.search();
			}
			application.output('da inserire ' + totOK)
			for (var i = 1; i <= totOK; i++) {
				var toDeleteOvertime = events_log_closed.getRecord(i);
				// cancellazione di tutti gli events_overtime_log
				events_log_overtime.deleteOvertime(toDeleteOvertime);
			}
			for (var j = 1; j <= totOK; j++) {
				var toReinsertOvertime = events_log_closed.getRecord(j);
				// cancellazione di tutti gli events_overtime_log
				scopes.rulesFor_events_log.processOvertime(toReinsertOvertime);
				scopes.rulesFor_events_log.processOvertimeWithCompensatoryRest(toReinsertOvertime);

				//questo serve per risolvere problema di doppi record nella tabella di appoggio
				if (toReinsertOvertime.event_id == 28)
					events_log_additional_charge.deleteAdditionalCharge(toReinsertOvertime, 1);
			}

		} catch (e) {
			throw new Error('- Errore nella cancellazione degli straordinari');
		}
	}
}

/**
 * Processa gli straordinari in base alle fasce orario
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"30375CCF-85B3-4D2B-A8BC-E864B6742604"}
 */
function processOvertime(record) {
	application.output('START processOvertime - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
	application.output('start: ' + record.time_start / 60 + '  stop ' + record.time_stop / 60, LOGGINGLEVEL.DEBUG);
	//00:00 [0] - 06:00 [360]- 09:00 [540]
	//18:00 [1080] - 22:00 [1320] 24:00 [1440]
	/** @type {JSFoundSet<db:/geco/events_log_overtime>} */
	var events_log_overtime = databaseManager.getFoundSet('geco', 'events_log_overtime');
	/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
	//var events_log_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');
	var middle_time = 0;
	var durationForS25 = 0;
	var durationTot = 0;
	var durationForSR10 = 0;
	var durationForSR35 = 0;
	var durationForS55 = 0;
	var durationForS60 = 0;
	var durationForS75 = 0;
	var dur75current = 0;
	var toCheck = false;
	var durationForcheck = 0

	var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
	//application.output('trovato contratto ' + objTimeUser.contractId + ' ' + objTimeUser.startTime + ' ' + objTimeUser.stopTime + ' ' + objTimeUser.breakStart + ' ' + objTimeUser.breakStop + ' ' + objTimeUser.workingHours);
	var break_start = objTimeUser.breakStart;
	var break_stop = objTimeUser.breakStop;
	var durationBreak = 0;
	//if (!record.isNew()) return;
	try {
		// se dipendente e  se straordinario o intervento straordinario in reperibilità
		if (record.events_log_to_users.user_type_id == 7 && (record.events_log_to_events.event_id == 27 || record.events_log_to_events.event_id == 38)) {
			//START sunday - holiday - head office user holiday
			if (record.events_log_to_calendar_days.calendar_weekday == 0 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
				//durata già inserita in tabella straordinari
				durationTot = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, null, record.event_log_id);
				middle_time = 0;
				durationForSR10 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 7, record.event_log_id);
				durationForSR35 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 8, record.event_log_id);
				durationForS55 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 4, record.event_log_id);
				durationForS60 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 5, record.event_log_id);
				durationForS75 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 6, record.event_log_id);
				application.output('durationTOT: ' + durationTot / 60);
				application.output('durationForS55: ' + durationForS55 / 60);
				application.output('durationForS60: ' + durationForS60 / 60);
				application.output('durationForS75: ' + durationForS75 / 60);
				application.output('durationForSR10: ' + durationForSR10 / 60);
				application.output('durationForSR35: ' + durationForSR35 / 60);

				//inizia prima delle 6
				if (record.time_start < 360) {
					// finisce dopo le 22
					if (record.time_stop > 1320) {
						// ha fatto più di 8 ore --> ore tra le 0 e le 6 e  ore tra le 22 e le 24 a S75 quelle tra le 6 e le 22  a s55
						events_log_overtime.insertOvertime(6, record, record.time_start, 360); //fino alle 6
						//escludere la pausa pranzo tra le 9 e le 18 dividere in più record
						events_log_overtime.insertOvertime(4, record, 360, break_start);
						events_log_overtime.insertOvertime(4, record, break_stop, 1320);
						events_log_overtime.insertOvertime(6, record, 1320, record.time_stop); // dopo le 22
						//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore
						dur75current = (360 - record.time_start) + (record.time_stop - 1320);
						toCheck = true;
						durationForcheck = record.duration - dur75current;
					}
					// inizia prima delle 6 finisce prima delle 6
					else if (record.time_stop <= 360) {
						//se durationTot > 8 tutto a S75
						if (durationTot >= 480) events_log_overtime.insertOvertime(6, record, record.time_start, record.time_stop);
						//se durata presente + durata evento non supera le 8 ore tutto in S60
						else if (durationTot + record.duration <= 480) events_log_overtime.insertOvertime(5, record, record.time_start, record.time_stop);
						//se durata presente + durata evento supera le 8h fino ad esautimento 8 ore in fascia S60 oltre in S75
						else if (durationTot + record.duration > 480) {
							middle_time = record.time_start + (record.duration + durationTot - 480);
							if (middle_time < 360) {
								events_log_overtime.insertOvertime(6, record, record.time_start, middle_time);
								events_log_overtime.insertOvertime(5, record, middle_time, record.time_stop);
								//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore (dur75current = middle_time - record.time_start)
								dur75current = middle_time - record.time_start
							} else if (middle_time >= 360) {
								events_log_overtime.insertOvertime(6, record, record.time_start, record.time_stop);
								//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore (dur75current = 360 - record.time_start)
								dur75current = record.time_stop - record.time_start
							}
							//DF aggiunta durata da controllare e flag
							toCheck = true;
							durationForcheck = record.duration - dur75current;
						}

					}// inizia prima delle 6 finisce dopo le 6 e prima delle 22
					else if (record.time_stop <= 1320) {
						// break
						durationBreak = 0;
						if (record.time_stop >= break_stop) {
							durationBreak = break_stop - break_start;
						} else if (record.time_stop > break_start && record.time_stop <= break_stop) {
							durationBreak = record.time_stop - break_start;
						}
						//if (durationTot + (record.time_stop - 360 - durationBreak) >= 480) {

						//ore fatte tra le 0 e le 6
						//se durata presente >8h le ore tra le 0 e le 6 vanno in S75
						if (durationTot >= 480) {
							events_log_overtime.insertOvertime(6, record, record.time_start, 360);
							dur75current = 360 - record.time_start
							toCheck = true;
							durationForcheck = record.duration - dur75current;
						}
						//se durata presente + durata evento non supera le 8 ore tutto in S60
						else if (durationTot + record.duration <= 480) {
							events_log_overtime.insertOvertime(5, record, record.time_start, 360);
						}
						// se durata + record > 8h fino a esaurimento prime 8h in S60 oltre in S75
						else if (durationTot + (record.duration) > 480) {
							middle_time = record.time_stop + durationTot - 480;
							//events_log_overtime.insertOvertime(6, record, record.time_start, 360);
							application.output('fine record a 75% ' + (middle_time / 60));
							if (middle_time < 360) {
								events_log_overtime.insertOvertime(6, record, record.time_start, middle_time);
								events_log_overtime.insertOvertime(5, record, middle_time, 360);
								//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore (dur75current = middle_time - record.time_start)
								dur75current = middle_time - record.time_start
							} else if (middle_time >= 360) {
								events_log_overtime.insertOvertime(6, record, record.time_start, 360);
								//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore (dur75current = 360 - record.time_start)
								dur75current = 360 - record.time_start
							}
							//else events_log_overtime.insertOvertime(6, record, record.time_start, 360);
							//DF aggiunta durata da controllare e flag
							toCheck = true;
							durationForcheck = record.duration - dur75current;
						}

						//ore tra le 6 e le 22
						if (record.time_stop <= break_start) {
							events_log_overtime.insertOvertime(4, record, 360, record.time_stop);
						}
						//finisce a cavallo della pausa pranzo tolgo la pausa pranzo
						else if (record.time_stop > break_start && record.time_stop <= break_stop) {
							events_log_overtime.insertOvertime(4, record, 360, break_start);
						}
						//finsce dopo la pausa pranzo tolgo  la pausa pranzo
						else if (record.time_stop > break_stop) {
							events_log_overtime.insertOvertime(4, record, 360, break_start);
							events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
						}
					}
				}//inizia tra le >= 6 e le 22
				else if (record.time_start < 1320) {
					// se finisce prima delle 22
					if (record.time_stop <= 1320) {
						durationForcheck = record.duration;
						//inizia prima e finisce prima della pausa pranzo o inizia e finisce dopo lapausa pranzo
						if ( (record.time_start < break_start && record.time_stop <= break_start) || (record.time_start >= break_stop && record.time_stop > break_stop)) {
							events_log_overtime.insertOvertime(4, record, record.time_start, record.time_stop);
						}
						//inizia prima della pausa e finisce in mezzo all pausa pranzo
						else if (record.time_start < break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
							events_log_overtime.insertOvertime(4, record, record.time_start, break_start);
							//durationForcheck = record.duration - (record.time_stop - break_start);
						}
						//inizia uguale o prima della pausa e finisce uguale o dopo la pausa pranzo
						else if (record.time_start <= break_start && record.time_stop >= break_stop) {
							if (record.time_start < break_start)
								events_log_overtime.insertOvertime(4, record, record.time_start, break_start);
							if (record.time_stop > break_stop)
								events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
							//durationForcheck = record.duration - (break_stop - break_start);
						}
						//inizia in mezzo alla pausa pranzo e finisce dopo la pausa
						else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop > break_stop) {
							events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
							//durationForcheck = record.duration - (break_stop - record.time_start);
						}

						//durationForcheck = record.duration;
						toCheck = true;
					}
					// inizia dopo le 6 e finisce dopo le 22
					else if (record.time_stop > 1320) {

						//ore dopo le 22
						//durationTot + durata evento fra inizio e 22 > 8 dopo le 22 S75
						durationBreak = 0;
						if (record.time_start <= break_start)
							durationBreak = break_stop - break_start;
						else if (record.time_start >= break_start && record.time_start < break_stop)
							durationBreak = break_stop - record.time_start;

						//							if (durationTot + (1320 - record.time_start - durationBreak) >= 480) {
						//								events_log_overtime.insertOvertime(6, record, 1320, record.time_stop);
						//								toCheck = true;
						//								durationForcheck = record.duration - (record.time_stop - 1320);
						//
						//							} else events_log_overtime.insertOvertime(5, record, 1320, record.time_stop);

						//durata presente >=8 tutte le ore notturne finiscono in S75
						if (durationTot >= 480) {
							events_log_overtime.insertOvertime(6, record, 1320, record.time_stop);
						}
						//durata totale + durata record <= 8 le notturne in S60
						else if (durationTot + record.duration <= 480) events_log_overtime.insertOvertime(5, record, 1320, record.time_stop);
						//durata presente + durata del record >=8 tutte le ore notturne a esaurimento delle 8 ore vanno in S60 le altre in S75
						else if (durationTot + record.duration > 480) {
							middle_time = record.time_start + (480 - durationTot);
							if (middle_time > 1320) {
								events_log_overtime.insertOvertime(5, record, 1320, middle_time);
								events_log_overtime.insertOvertime(6, record, middle_time, record.time_stop);
								//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore (dur75current = record.time_stop-middle_time)
								dur75current = record.time_stop - middle_time
							} else if (middle_time <= 1320) {
								events_log_overtime.insertOvertime(6, record, 1320, record.time_stop);
								//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore (dur75current = record.time_stop-1320)
								dur75current = record.time_stop - 1320
							}
							//else events_log_overtime.insertOvertime(6, record, 1320, record.time_stop);
						}
						//ore ta le 6 e le 22

						//inizia prima della pausa pranzo divido in 2 record
						if (record.time_start < break_start) {
							events_log_overtime.insertOvertime(4, record, record.time_start, break_start);
							events_log_overtime.insertOvertime(4, record, break_stop, 1320);
						}
						//se inizia dopo inizio pausa e prima di fine pausa e finisce dopo le 22
						else if (record.time_start >= break_start && record.time_start <= break_stop) {
							events_log_overtime.insertOvertime(4, record, break_stop, 1320);
						}
						//inizia dopo la pausa pranzo e finisce dopo le 22
						else if (record.time_start >= break_stop) {
							events_log_overtime.insertOvertime(4, record, record.time_start, 1320);
						}
						toCheck = true;
						//DF non si deve passare tutta la durata del record ma solo le ore che non sono in S75
						application.output('dur75current: ' + dur75current);
						durationForcheck = record.duration - dur75current;

					}
				}
				//inizia dopo le >=22
				else {
					// se durataTot > 8 tutte a S75
					if (durationTot >= 480)
						events_log_overtime.insertOvertime(6, record, record.time_start, record.time_stop);
					// evento + durationTot < 8 tutte a S60
					else if (durationTot + record.duration <= 480)
						events_log_overtime.insertOvertime(5, record, record.time_start, record.time_stop);
					// se durata evento + durataTot > 8 divido in S60 e S75
					else if (durationTot + record.duration > 480) {

						middle_time = record.time_start + (480 - durationTot);
						if (middle_time > 1320) {
							events_log_overtime.insertOvertime(5, record, record.time_start, middle_time);
							events_log_overtime.insertOvertime(6, record, middle_time, record.time_stop);
							//DF non si deve passare tutta la durata del record ma solo le ore che non sono in S75
							dur75current = record.time_stop - middle_time
							application.output('dur75current: ' + dur75current);
							durationForcheck = record.duration - dur75current;

						}
						if (middle_time <= 1320) events_log_overtime.insertOvertime(6, record, record.time_start, record.time_stop);
					}

					toCheck = true;
					//durationForcheck = record.duration;
				}
				if (toCheck) {
					application.output('durationForcheck ' + durationForcheck);
					events_log_overtime.checkOvertimeHoliday(record, record.user_id, record.event_log_date, durationForcheck);
					//FS check record SR10 da modificare eventualmente in SR35
					events_log_overtime.checkOvertimeHolidayRec(record, record.user_id, record.event_log_date, durationForcheck);
				}

			}//END sunday event_id = 27 - holiday - head hoffice user holiday
			//START workday - not holiday
			else if (record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0 && record.events_log_to_events.event_id != 28) {
				/** @type {JSFoundSet<db:/geco/events_log>} */
				var events_log_ord = databaseManager.getFoundSet('geco', 'events_log');
				var ordStartStop = events_log_ord.getOrdinaryStartStop(record);
				var startOrd = ordStartStop.startOrd;
				var stopOrd = ordStartStop.stopOrd;
				durationForS25 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 1, record.event_log_id);
				//6-9 or 18-22
				//straordinario prima del turno
				//if (record.events_log_to_users.shift_start >= record.time_stop) {
				if (startOrd >= record.time_stop) {
					//inizia e finisce prima delle 6
					if (record.time_start < 360 && record.time_stop <= 360)
						events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
					else {
						var begin_time = 360;
						//inizia prima delle 6, quello prima delle 6 in S50
						if (record.time_start < 360) events_log_overtime.insertOvertime(3, record, record.time_start, begin_time);
						//finisce dopo le 6 ma prima dell'inizio del turno
						else //inizia dopo le 6
							begin_time = record.time_start;

						//esistono già 2 ore a s25
						if (durationForS25 >= 120) {
							events_log_overtime.insertOvertime(2, record, begin_time, record.time_stop);
						}
						//meno di 2 ore a S25
						else {
							if (durationForS25 > 0) middle_time = begin_time + (120 - durationForS25);
							if (middle_time == 0 && record.time_stop - begin_time <= 120)
								events_log_overtime.insertOvertime(1, record, begin_time, record.time_stop);
							else if (middle_time == 0 && record.time_stop - begin_time > 120) {
								events_log_overtime.insertOvertime(1, record, begin_time, begin_time + 120);
								events_log_overtime.insertOvertime(2, record, begin_time + 120, record.time_stop);
							} else if (middle_time != 0 && middle_time < record.time_stop) {
								events_log_overtime.insertOvertime(1, record, begin_time, middle_time);
								events_log_overtime.insertOvertime(2, record, middle_time, record.time_stop);
							} else if (middle_time >= record.time_stop) events_log_overtime.insertOvertime(1, record, begin_time, record.time_stop);
							else application.output('Errore nel calcolo degli straordinari prima del turno, nessuna condizione verificata');
						}
					}
				}
				//straordinario dopo il turno
				//if (record.events_log_to_users.shift_stop <= record.time_start) {
				if (stopOrd <= record.time_start) {
					//inizia prima delle 22
					if (record.time_start < 1320) {
						var end_time = 1320;
						//finisce dopo le 22
						if (record.time_stop > 1320) {
							//tutto quello dopo le 22 va  a S50
							events_log_overtime.insertOvertime(3, record, 1320, record.time_stop);
						}
						//finisce prima delle 22
						else end_time = record.time_stop;

						//tutto quello prima delle 22
						//esistono già 2 ore a s25 mette tutto a S30
						if (durationForS25 >= 120) {
							events_log_overtime.insertOvertime(2, record, record.time_start, end_time);
						}
						//meno di 2 ore già fatte a S25 divide tra S25 e S30
						else {
							//start + duration esistente
							if (durationForS25 > 0) {
								middle_time = record.time_start + (120 - durationForS25);
							}
							//se uguale a zero (non c'erano altre ore S25) e  durata minore di 2h 1 solo a S25
							if (middle_time == 0 && (end_time - record.time_start <= 120))
								events_log_overtime.insertOvertime(1, record, record.time_start, end_time);
							//se uguale a zero (non c'erano altre ore S25) e  durata maggiore di 2h divido
							else if (middle_time == 0 && (end_time - record.time_start > 120)) {
								events_log_overtime.insertOvertime(1, record, record.time_start, record.time_start + 120);
								events_log_overtime.insertOvertime(2, record, record.time_start + 120, end_time);
							}
							//se maggiore di zero (c'erano altre ore S25) e minore di fine evento divido
							else if (middle_time != 0 && middle_time < end_time) {
								events_log_overtime.insertOvertime(1, record, record.time_start, middle_time);
								events_log_overtime.insertOvertime(2, record, middle_time, end_time);
							}
							//uguale o maggiore di fine
							else if (middle_time >= end_time) events_log_overtime.insertOvertime(1, record, record.time_start, end_time);
							else application.output('Errore nel calcolo degli straordinari dopo il turno, nussuna condizione verificata');
						}
					}
					//inizia dopo le 22 tutto a S50
					else {
						events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
					}
				}
			}
			// END workday - not holiday

			//START saturday - not holiday
			else if (record.events_log_to_calendar_days.calendar_weekday == 6 && record.events_log_to_calendar_days.is_holiday == 0 && record.events_log_to_events.event_id != 28) {
				//durationForS25 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 1);
				durationTot = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, null, record.event_log_id);
				//non ci sono altri eventi
				if (durationTot == 0) {
					//duration > 2 hours --> all hours S50
					if (record.duration > 120) {
						if (record.time_start < break_start && record.time_stop <= break_start) {
							events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
						}
						//se inizia prima della pausa e finisce in mezzo all pausa pranzo
						else if (record.time_start <= break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
							if (record.time_start < break_start)
								events_log_overtime.insertOvertime(3, record, record.time_start, break_start);
							//events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
						}
						//se inizia prima della pausa e finisce dopo la pausa pranzo
						else if (record.time_start <= break_start && record.time_stop > break_stop) {
							if (record.time_start < break_start)
								events_log_overtime.insertOvertime(3, record, record.time_start, break_start);
							events_log_overtime.insertOvertime(3, record, break_stop, record.time_stop);
						}
						//inizia in mezzo alla pausa pranzo e finisce uguale o dopo la pausa
						else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop > break_stop) {
							events_log_overtime.insertOvertime(3, record, break_stop, record.time_stop);
						}
						//inizia dopo la pausa pranzo
						else if (record.time_start >= break_stop && record.time_stop > break_stop) {
							events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
						}
						//events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
					}
					//duration <= 2 hours S25
					else {
						if (record.time_start < break_start && record.time_stop <= break_start) {
							events_log_overtime.insertOvertime(1, record, record.time_start, record.time_stop);
						}
						//se inizia prima della pausa e finisce in mezzo all pausa pranzo
						else if (record.time_start <= break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
							if (record.time_start < break_start)
								events_log_overtime.insertOvertime(1, record, record.time_start, break_start);
							//events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
						}
						//se inizia prima della pausa e finisce dopo la pausa pranzo
						else if (record.time_start <= break_start && record.time_stop > break_stop) {
							if (record.time_start < break_start)
								events_log_overtime.insertOvertime(1, record, record.time_start, break_start);
							events_log_overtime.insertOvertime(1, record, break_stop, record.time_stop);
						}
						//inizia in mezzo alla pausa pranzo e finisce uguale o dopo la pausa
						else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop > break_stop) {
							events_log_overtime.insertOvertime(1, record, break_stop, record.time_stop);
						}
						//inizia dopo la pausa pranzo
						else if (record.time_start >= break_stop && record.time_stop > break_stop) {
							events_log_overtime.insertOvertime(1, record, record.time_start, record.time_stop);
						}
						//events_log_overtime.insertOvertime(1, record, record.time_start, record.time_stop);
					}
				}
				//more overtimes and sum(all durations) > 2hours --> all S50
				else if (durationTot + record.duration > 120) {
					if (record.time_start < break_start && record.time_stop <= break_start) {
						events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
					}
					//se inizia prima della pausa e finisce in mezzo all pausa pranzo
					else if (record.time_start <= break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
						if (record.time_start < break_start)
							events_log_overtime.insertOvertime(3, record, record.time_start, break_start);
					}
					//se inizia prima della pausa e finisce dopo la pausa pranzo
					else if (record.time_start <= break_start && record.time_stop > break_stop) {
						if (record.time_start < break_start)
							events_log_overtime.insertOvertime(3, record, record.time_start, break_start);
						events_log_overtime.insertOvertime(3, record, break_stop, record.time_stop);
					}
					//inizia in mezzo alla pausa pranzo e finisce uguale o dopo la pausa
					else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop > break_stop) {
						events_log_overtime.insertOvertime(3, record, break_stop, record.time_stop);
					}
					//inizia dopo la pausa pranzo
					else if (record.time_start >= break_stop && record.time_stop > break_stop) {
						events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
					}
					//events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
					events_log_overtime.updateOvertime(1, 3, record);
				}
				//ci sono altri eventi e sum(all durations) < 2hours
				else {
					if (record.time_start < break_start && record.time_stop <= break_start) {
						events_log_overtime.insertOvertime(1, record, record.time_start, record.time_stop);
					}
					//se inizia prima della pausa e finisce in mezzo all pausa pranzo
					else if (record.time_start <= break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
						if (record.time_start < break_start)
							events_log_overtime.insertOvertime(1, record, record.time_start, break_start);
					}
					//se inizia prima della pausa e finisce dopo la pausa pranzo
					else if (record.time_start <= break_start && record.time_stop > break_stop) {
						if (record.time_start < break_start)
							events_log_overtime.insertOvertime(1, record, record.time_start, break_start);
						events_log_overtime.insertOvertime(1, record, break_stop, record.time_stop);
					}
					//inizia in mezzo alla pausa pranzo e finisce uguale o dopo la pausa
					else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop > break_stop) {
						events_log_overtime.insertOvertime(1, record, break_stop, record.time_stop);
					}
					//inizia dopo la pausa pranzo
					else if (record.time_start >= break_stop && record.time_stop > break_stop) {
						events_log_overtime.insertOvertime(1, record, record.time_start, record.time_stop);
					}
					//events_log_overtime.insertOvertime(1, record, record.time_start, record.time_stop);
				}
			}
			//END saturday - not holiday
		}
	} catch (e) {
		throw new Error("- Errore inserimento straordinari: " + e);
	}
	application.output('STOP processOvertime - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
}
