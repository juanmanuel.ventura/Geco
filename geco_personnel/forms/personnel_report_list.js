/**
 * @type {String}
 * @properties={typeid:35,uuid:"754B387A-A276-4E7F-AB43-AB9E8C66348B"}
 */
var tableReport = '';

/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"597EBE1F-6325-4573-ABB8-C3E47C740B12",variableType:-4}
 */
var datasetReport = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"7C553996-1B5D-4CB7-98FD-1353A290E1A5"}
 */
var nameExport = '';

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"793BA46F-8AA7-48A4-A9C8-BAC246E32149"}
 */
function saveReport(event) {
	var success = false;
	var line = '';
	application.output(globals.messageLog + 'START personnel_report_list.saveReport() ',LOGGINGLEVEL.INFO);
	
	if (datasetReport != null) {
		
		var rec = datasetReport.getValue(1,1);
		if(rec == null){
			globals.DIALOGS.showWarningDialog('Attenzione','Export vuoto','OK');
			return
		}

		var colArray = new Array()
		for (var i = 1; i <= datasetReport.getMaxColumnIndex(); i++)
		{
			colArray[i-1] = datasetReport.getColumnName(i)
			//note the -1, because an array is zero based and dataset is 1 based.
		}
		//application.output(colArray);
		line = colArray.join('\t')+ '\r\n';
		for (var index = 1; index <= datasetReport.getMaxRowIndex(); index++) {
			var row = datasetReport.getRowAsArray(index);
			var rowstring = row.join('\t') ;
			line = line + rowstring + '\n';
		}
		//application.output(line);
		if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
			/** @type {String} */
			var yearFrom = '' + forms.personnel_report_filter.selectedDatefrom.getFullYear();
			/** @type {String} */
			var monthFrom = '' + (forms.personnel_report_filter.selectedDatefrom.getMonth() + 1);
			/** @type {String} */
			var dayFrom = '' + forms.personnel_report_filter.selectedDatefrom.getDate();
			/** @type {String} */
			var dateFrom = yearFrom + ((monthFrom.length > 1) ? monthFrom : '0' + monthFrom)+ ((dayFrom.length > 1) ? dayFrom : '0' + dayFrom);
			/** @type {String} */
			var yearTo = '' + forms.personnel_report_filter.selectedDateAt.getFullYear();
			/** @type {String} */
			var monthTo = '' + (forms.personnel_report_filter.selectedDateAt.getMonth() + 1);
			/** @type {String} */
			var dayTo = '' + forms.personnel_report_filter.selectedDateAt.getDate();
			/** @type {String} */
			var dateTo = yearTo + (monthTo.length > 1 ? monthTo : '0' + monthTo) + (dayTo.length > 1 ? dayTo : '0' + dayTo);

			var fileName = nameExport + dateFrom + '_' + dateTo + '.xls';
			success = plugins.file.writeTXTFile(fileName, line, 'UTF-8', 'application/vnd.ms-excel');
			application.output(globals.messageLog + 'STOP personnel_report_list.saveReport() : export OK',LOGGINGLEVEL.INFO);
		}
	}
	if (!success) application.output(globals.messageLog + 'STOP personnel_report_list.saveReport() : export non riuscito',LOGGINGLEVEL.INFO);;
}

/**
 * 
 *@param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"B65EA14B-F252-48AB-BE3B-0F1BF2FFE164"}
 */
function getUsers(event){
	globals.reportUsers(event);
}

/**
 * 
 *@param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"5ABAA052-EA26-44B2-B7B4-62C2CAEF62F2"}
 */
function getProfitCenters(event){
	globals.reportPCManagers(event);
}


/**
 * 
 *@param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"93AE5950-170A-425A-8D32-750155BDC1BC"}
 */
function getEvents(event){
	globals.reportEvents(event);
}


/**
 * 
 *@param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"0D333A0E-7D81-445F-8C9C-67652EB6ED14"}
 */
function getJobOrders(event){
	globals.reportJobOrders(event);
}

/**
 * 
 *@param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"65750656-6BDC-421D-8A8F-6CCC30352D8B"}
 */
function getCompanies(event){
	globals.reportCompanies(event);
}


/**
 * 
 *@param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"1BBD90D1-D316-475D-836F-1EBC38673E85"}
 */
function updateUI(event){
	
	if (globals.hasRole('HR Expenses') && !globals.hasRole('HR Administrators')){
		elements.labelAnagr.visible = false;
		elements.buttonUsers.visible = false;
		elements.buttonJobOrders.visible = false;
		elements.buttonPC.visible = false;
		elements.buttonEvents.visible = false;
		elements.buttonCompanies.visible = false;
	}
}
