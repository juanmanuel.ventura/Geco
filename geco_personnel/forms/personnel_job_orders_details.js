/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"D18F2F72-1152-4FA1-8644-EC9176E745E6"}
 */
function updateUI(event) {
	_super.updateUI(event);

	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
		elements.buttonEdit.visible = false;
	}
}
