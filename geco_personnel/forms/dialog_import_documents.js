/**
 * @type {Number}
 * @properties={typeid:35,uuid:"489DF3B6-63F0-4733-B8E1-E6DD5C035FAC",variableType:8}
 */
var year = new Date().getFullYear();

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"BEB797E8-7EA7-484D-B7A1-A7C877D65C02",variableType:8}
 */
var month = new Date().getMonth() + 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"B410B1B9-CCA2-406D-B70F-739413E6B8B6",variableType:4}
 */
var documentTypeId = 1;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"A9B7955F-A0DB-4560-A4D8-EF91FF6D30C5"}
 */
var details = '';

/**
 * @type {plugins.file.JSFile}
 * @properties={typeid:35,uuid:"B4662873-6347-4D41-84D8-B2BCA8EE85FC",variableType:-4}
 */
var file = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"12007CE2-8CAA-4B1B-B1AB-81E2D842220D"}
 */
var fileName =  (file)? file.getName():'';

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"6DF37EE2-B5DD-46E7-ADD0-24FAA42E0AD2",variableType:4}
 */
var isZip = 0;

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"A56E67ED-3F0F-4A51-8FE6-24AA06167635"}
 */
function getZip(event) {
	// get the Zip
	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT)
		/** @type {plugins.file.JSFile} */
		file = plugins.file.showFileOpenDialog(1, null, false, ['zip'], uploadCallBack );
	else
		/** @type {plugins.file.JSFile} */
		file = plugins.file.showFileOpenDialog(1, null, false, ['zip']);
	if (file) fileName = file.getName();
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"26DF4DBF-D112-4797-A439-A71BFD964A03"}
 */
function importDocuments(event) {
	globals.resultImportDocuments = null;
	globals.callerForm = event.getFormName();
	var options = {
		year: year,
		month: month,
		zip: file,
		documentType: documentTypeId,
		documentDetails: details
	};

	// clean up and close windows
	//controller.getWindow().destroy();
	fileName = details = null;

	// process
	globals.importDocuments(options);
	clearField();
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"72E1E451-FEF9-4C61-B8AE-D204A776BE29"}
 */
function onAction(event) {
	// TODO to be implemented
	globals.DIALOGS.showInfoDialog("Avviso", "Feature da implementare", "OK");
}

/**
 * @param {JSEvent} event *
 * @properties={typeid:24,uuid:"00ED941A-8D64-42F7-97B1-17524DABD514"}
 */
function closeForm (event){
	globals.resultImportDocuments = null;
	clearField();
	controller.getWindow().destroy();
}

/**
 * @properties={typeid:24,uuid:"7C035F6C-F6CF-436E-AE3B-84A5ABF11FC1"}
 */
function clearField(){
	file = null;
	details = null;
}

/**
* @param {Array<plugins.file.JSFile>} fileToUpload
* @private
*
* @properties={typeid:24,uuid:"D175AC4F-C72F-43E9-9390-7AE46E28B347"}
*/
function uploadCallBack(fileToUpload) {
var myuploadedfile = fileToUpload[0];
application.output(myuploadedfile.getName());
globals.serverDir = plugins.file.convertToJSFile("cedolini");
application.output('dir path ' + globals.serverDir.getAbsolutePath());
var resultDir = plugins.file.createFolder(globals.serverDir);

application.output('creata dir ' + resultDir);
application.output(globals.serverDir);
globals.serverfile = plugins.file.createFile(globals.serverDir.getName()+'/'+myuploadedfile.getName() );
application.output(globals.serverfile);
var result = plugins.file.writeFile(globals.serverfile,myuploadedfile.getBytes());
application.output(result);
if (result) fileName = myuploadedfile.getName();
}
