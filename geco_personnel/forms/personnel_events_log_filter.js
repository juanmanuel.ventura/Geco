
/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"CE64F8A8-D3E6-4C8B-894D-3F739AC0641C"}
 */
function onLoad(event) {
	 isDirectApprover = null;
	 //approver = selectedApprover;
	 userType = [6,7,10];
	  _super.onLoad(event);
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"09A92F89-1102-425D-B39B-48EA2CAE2738"}
 */
function onDataChangeFEP(oldValue, newValue, event) {
	
	if (newValue == 34) {
		databaseManager.removeTableFilterParam('geco','holidayFilter');
	}
	else 
		databaseManager.addTableFilterParam('geco', 'events_log', 'event_id', 'not in', [34], 'holidayFilter');
	return _super.onDataChange(oldValue, newValue, event)
}
/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"9A628A62-40B0-44F9-A21F-E75E7C035412"}
 */
function onDataChange1(oldValue, newValue, event) {
	if(newValue == null){
		userType = [6,7,10];
	} else {
	userType = null;
	}
	applyFilter();
	if (isDirectApprover == 2) toggleButton(false)
	else
		toggleButton(selectedStatus != null);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"30C3F8D1-C2FA-4736-BEB5-1B9EC0E45919"}
 */
//function onActionFep(event) {
//	databaseManager.addTableFilterParam('geco', 'events_log', 'event_id', 'not in', [34], 'holidayFilter');
//	_super.resetFilter(event);
//	
//}


/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"8BB65C99-06A2-4E5B-820E-682D88780E32"}
 */
function resetFilter(event) {
	selectedApprover = null;
	selectedProfitCenter = null;
	selectedUser = null;
	selectedJobOrder = null;
	selectedStatus = 1;
	userTypeForFilter = null;
	userType = [6,7,10];
	selectedMonth = new Date().getMonth() + 1;;
	selectedYear = new Date().getFullYear();;
	selectedFepCode = null;
	isDirectApprover = null;
	applyFilter();
	toggleButton(selectedStatus != null);
}