/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"40EB39B3-57DE-4A6A-ADD5-CE598BDC356B"}
 */
//function onAction(event) {
//	//mese e anno di filter
//	month = null;
//	year = null;
//	// get variable status di filter
//	var bundle = forms[controller.getFormContext().getValue(2, 2)];
//	if (bundle.elements['split']) {
//		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
//		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
//	}
//
//	globals.exportPresence(event, year, month);
//}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"CB54FCF3-D1B2-4EF8-8694-C46BB5C1EA2E"}
 */
function openUserTimesheet(event) {
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) {
		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
		userId = bundle.elements['split']['getLeftForm']()['selectedUser'];
		fepCode = bundle.elements['split']['getLeftForm']()['selectedFepCode'];
		stato = bundle.elements['split']['getLeftForm']()['selectedStatus'];
	}
	if (month == null || year == null || userId == null) {
		globals.DIALOGS.showErrorDialog('Error', 'Selezionare i campi Anno Mese e Utente');
		return;
	}

//	if (forms.print_user_events_log.foundset.find()) {
//		forms.print_user_events_log.foundset.user_id = userId;
//		forms.print_user_events_log.foundset.timesheet_month = month;
//		forms.print_user_events_log.foundset.timesheet_year = year;
//		if (jobOrder != null)
//			forms.print_user_events_log.foundset.job_order_id = jobOrder;
//		forms.print_user_events_log.foundset.search();
//	}

	var win = application.createWindow('print', JSWindow.WINDOW);
	var print_form = forms.print_user_events_log;
	win.show(print_form);
}
///**
// * Perform the element default action.

// * @param {JSEvent} event the event that triggered the action
// * @private
// * @properties={typeid:24,uuid:"D7FBF015-CEF0-4578-8522-CBF0662C24E2"}
// */
//function sp_presence(event) {
//
//	var bundle = forms[controller.getFormContext().getValue(2, 2)];
//	if (bundle.elements['split']) {
//		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
//		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
//	}
//	
//	globals.exportSpPresence(event, year, month);
//	
//}


/**
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"60026793-23F0-4FAF-89E8-6EAA86566192"}
 * @AllowToRunInFind
 */
function printTickets(event) {
	
	
	
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) {
		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
	}
	
	var month_name = month;
	// foundset per mostrare il nome del mese al posto del numero
	/** @type {JSFoundSet<db:/geco/calendar_months>} */
	var calendar_month = databaseManager.getFoundSet('geco', 'calendar_months');
	if(calendar_month.find()){
		calendar_month.month_number = month;
		calendar_month.month_year = year;
		if(calendar_month.search()> 0) month_name = calendar_month.month_name;
		
	}
	
	
	var formName = 'totTicket_'+ year +'_' + month_name;
	var _query_download = 'call sp_ticket_counter (?, ?)';

	var arguments = new Array();
	arguments[0] = year;
	arguments[1] = month;

	//var _query_download = 'SELECT interface_session_nr FROM interface_session'
	var _ds_download = databaseManager.getDataSetByQuery('geco', _query_download, arguments, 10000);
	//variabili per i totali
	
	var _data_source = _ds_download.createDataSource('tc', [JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT]);
	
	var success = history.removeForm(formName);
	if (success) {
		solutionModel.removeForm(formName);
	}
	success = solutionModel.removeForm(formName);
	//application.output('solutionModel ' + success);
	if (forms[formName]) {
		
		application.output('esiste ancora');
		success = solutionModel.removeForm(formName);
		application.closeAllWindows();
	}
	var _form_download = solutionModel.newForm(formName, _data_source, null, 'GeCo', false, 600, 500);
	
	_form_download.extendsForm = 'personnel_ticket_count';
	_form_download.dataSource = _data_source;
	_form_download.scrollbars = SM_SCROLLBAR.VERTICAL_SCROLLBAR_ALWAYS;
	var _field_code = _form_download.newField('fep_code', JSField.TEXT_FIELD, 20, 103, 53, 20);
	var _field_name = _form_download.newField('nominativo', JSField.TEXT_FIELD, 73, 103, 179, 20);
	var _field_total = _form_download.newField('totale', JSField.TEXT_FIELD, 268, 103 , 62, 20);
	var _field_company = _form_download.newField('company', JSField.TEXT_FIELD, 330, 103 , 198, 20);
	
	
	//	   _form_download.view = JSForm.LOCKED_LIST_VIEW
	//	   _form_download.navigator = SM_DEFAULTS.NONE
	//	   _form_download.scrollbars = SM_SCROLLBAR.HORIZONTAL_SCROLLBAR_NEVER | SM_SCROLLBAR.VERTICAL_SCROLLBAR_AS_NEEDED

//	_field_year.editable = false;
//	_field_year.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
//	_field_year.dataProviderID = 'timesheet_year';
//	_field_year.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	
//	_field_month.editable = false;
//	_field_month.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
//	_field_month.dataProviderID = 'timesheet_month';
//	_field_month.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	
	_field_code.editable = false;
	_field_code.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_code.styleClass = 'small';
	_field_code.dataProviderID = 'fep_code';
	_field_code.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	

	_field_name.editable = false;
	_field_name.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_name.styleClass = 'small';
	_field_name.dataProviderID = 'nominativo';
	_field_name.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	
	
	
	_field_total.editable = false;
	_field_total.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_total.styleClass = 'small';
	_field_total.dataProviderID = 'totale';
	_field_total.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	
	_field_company.editable = false;
	_field_company.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_company.styleClass = 'small';
	_field_company.dataProviderID = 'company';
	_field_company.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	
	
	forms[formName]['year'] = ''+year;
	forms[formName]['month'] = month_name;
	forms[formName]['dataset'] = _ds_download;

	var win = application.createWindow('print', JSWindow.WINDOW);
	win.setInitialBounds(400,100,500,500);
	win.undecorated = true;
	win.show(_form_download);
}


/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"AE246E24-9F38-41FB-9F62-50AAD450FB11"}
 */
function updateUI(event){
	_super.updateUI(event);
	
	if(globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')){
		elements.buttonAdd.visible = false;
		elements.buttonDelete.visible = false;
		elements.buttonClose.visible = false;
		elements.buttonApprove.visible = false;
		elements.buttonReject.visible = false;	
	}
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"E38CFD59-51C0-4375-B51E-2170F86181F0"}
 */
function onCheck(event) {
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) {
		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
	}
	if (month == null || year == null) {
		globals.DIALOGS.showErrorDialog('Error', 'Selezionare i campi Anno Mese e Utente');
		return;
	}
	scopes.globals.checkOvertime(month,year);
}
