///**@type {String}
// *
// * @properties={typeid:35,uuid:"AB04590C-5BFA-4C87-8001-055E752F3C07"}
// */
//var dynamicValuelist = null;
//
///**
// * @type {JSDataSet}
// *
// * @properties={typeid:35,uuid:"7013B2A9-0A16-4362-A1AC-76FA1287635F",variableType:-4}
// */
//var dynamicDataset = null;
//
///**@type {Array<String>}
// *
// * @properties={typeid:35,uuid:"5FD42338-2D8D-4A46-B659-9E67C4BFDEB8",variableType:-4}
// */
//var solutionsList = [];

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"B9B66002-0B05-43DD-8068-8DEBD1E3F4A2"}
 */
function initialize(event) {
	if (!globals.grantAccess(['HR Administrators', 'HR Expenses', 'Admin Readers'])) {
		logOut(event);
	}
	_super.initialize(event);
//	// load first form
//	dynamicDataset = databaseManager.createEmptyDataSet();
//	if (globals.hasRole('Controllers')) solutionsList.push('Controller');
////	if (globals.hasRole('HR Administrators') || globals.hasRole('HR Expenses') || globals.hasRole('Admin Readers')) solutionsList.push('Personnel');
//	if (globals.currentUserDisplayName == 'STAFFA JONATHAN' || globals.currentUserDisplayName == 'FRAU DANIELA') solutionsList.push('Configurator');
//	if (globals.hasRole('Approvers')) solutionsList.push('Approver');
//	solutionsList.push('Logger');
//	if (globals.hasRole('Delivery Admin') || globals.hasRole('Responsabili Commessa') || globals.hasRole('Controllers') || globals.hasRole('Resp. Mercato') || globals.hasRole('Orders Admin') || globals.hasRole('Planner') || globals.hasRole('Supp. Commerciale')) solutionsList.push('Management');
//	application.output('solutionsList.length: ' + solutionsList.length + '; solutionsList: ' + solutionsList);
//	if (solutionsList.length > 0) {
//		solutionsList.sort();
//		for (var index = 0; index < solutionsList.length; index++) {
//			dynamicDataset.addRow(new Array(solutionsList[index]));
//		}
//		elements.dynamicSolutionsChoice.setValueListItems(dynamicDataset);
//		elements.lSolutionName.enabled = true;
//		elements.lSolutionName.visible = true;
//		elements.dynamicSolutionsChoice.enabled = true;
//		elements.dynamicSolutionsChoice.visible = true;
//	}
	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
		//		elements.button6.setLocation(elements.button3.getLocationX(), elements.button3.getLocationY());
		//		elements.button7.setLocation(elements.button4.getLocationX(), elements.button4.getLocationY());
		//		elements.button8.setLocation(elements.button5.getLocationX(), elements.button5.getLocationY());
		elements.button3.setLocation(elements.button5.getLocationX() + elements.button5.getWidth(), elements.button5.getLocationY());
		elements.button4.setLocation(elements.button3.getLocationX() + elements.button3.getWidth(), elements.button3.getLocationY());
		elements.button5.setLocation(elements.button4.getLocationX() + elements.button4.getWidth(), elements.button4.getLocationY());
		elements.setHoliday.visible = false;
		elements.setHoliday.enabled = false;
		elements.uploadDoc.visible = false;
		elements.uploadDoc.enabled = false;
		elements.button6.visible = true;
		elements.button6.enabled = true;
		elements.button7.visible = true;
		elements.button7.enabled = true;
		elements.button8.visible = true;
		elements.button8.enabled = true;
	}
	//se  HR Expenses vede solo i tab notaspese e report
	if (globals.hasRole('HR Expenses') && !globals.hasRole('HR Administrators')) {
		elements.button1.visible = false;
		elements.button1.enabled = false;
		elements.button2.visible = false;
		elements.button2.enabled = false;
		elements.button3.visible = false;
		elements.button3.enabled = false;
		elements.button6.visible = false;
		elements.button6.enabled = false;
		elements.button7.visible = false;
		elements.button7.enabled = false;
		elements.button8.visible = false;
		elements.button8.enabled = false;
		elements.uploadDoc.visible = false;
		elements.uploadDoc.enabled = false;
		elements.setHoliday.visible = false;
		elements.setHoliday.enabled = false;
		// move the Expenses tab to take button1's place
		elements.button4.setLocation(elements.button1.getLocationX(), elements.button1.getLocationY());
		elements.button5.setLocation(elements.button2.getLocationX(), elements.button2.getLocationY());
		loadPanel(event, 'personnel_expenses_log_bundle');
	} else {
		loadPanel(event, 'users_bundle');
	}
	_super.checkPwdExpiration();
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C41168D3-4271-41BE-BCB8-D93DACF0C8E3"}
 */
function onAction(event) {
	if (isEditing()) {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!", "OK");
		return;
	}
	var win = application.createWindow("Carica documenti", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 810, 480);
	win.show(forms.dialog_import_documents);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"D715C5F2-9268-4F58-8AAC-17224387107B"}
 */
function openPopupClendar(event) {
	if (isEditing()) {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!", "OK");
		return;
	}
	var win = application.createWindow("period enable", JSWindow.MODAL_DIALOG);
	globals.callerForm = forms[event.getFormName()];
	win.setInitialBounds(1000, 1000, 450, 550);
	win.show(forms.calendar_days_popup);
}




/**
 * Callback method for when form is shown.
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"9522883D-4953-4DAB-A8C6-C6D3978FE491"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
//	dynamicValuelist = null;
}
