/**
 * @type {String}
 * @properties={typeid:35,uuid:"40D6673D-6D92-40D6-9162-FF3D1A407D07"}
 */
var month = '';

/**
 * @type {String}
 * @properties={typeid:35,uuid:"293C771D-9A15-4952-9329-3576B0670F7B"}
 */
var year = '';

/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"8E45B15B-BB74-43C8-9E51-F8C7FF6CEBD6",variableType:-4}
 */
var dataset = null;

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"03EE834C-36F0-49AE-9CEC-99FEB3883835"}
 */
function onAction(event) {
	controller.print(false, true, plugins.pdf_output.getPDFPrinter());
}

/**
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"AA5FFF28-96B3-4D56-9EBE-C76FAE865813"}
 */
function onUnload(event) {
	solutionModel.removeForm(event.getFormName());
	application.closeAllWindows();
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A1A917DE-7480-4CEC-B677-521D4F8FE55D"}
 */
function printExcel(event) {
	if (dataset != null) {
		var line = 'fep_code\tNominativo\tTotale\t\Company\r\n';
		var success = true;
		for (var index = 1; index <= dataset.getMaxRowIndex(); index++) {
			var riga = dataset.getRowAsArray(index);
			var rigaStr = riga[0] + '\t' + riga[1]+ '\t' + riga[2];
			line = line + rigaStr + '\r\n';
		}

		if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
			/** @type String */
			var fileName = 'totTicket_' + year +'_'+ month+'.xls';
			success = plugins.file.writeTXTFile(fileName, line, 'UTF-8', 'application/vnd.ms-excel');
		} else if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
			var file = plugins.file.showFileSaveDialog()
			if (file != null && file.exists()) {
				globals.DIALOGS.showErrorDialog("Errore", "Non puoi sovrascrivere!", 'OK');
				return;
			}
			if (file && file.createNewFile()) {
				success = plugins.file.writeTXTFile(file, line, 'UTF-8');
			}
		}
	}

	if (!success) application.output('Could not write file.');
}
