/**
 * @type {Date}
 * @properties={typeid:35,uuid:"77598769-438B-4C75-8705-9D243C9615B5",variableType:93}
 */
var selectedDatefrom = null;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"C6C3CAB0-A556-43CD-965B-2F936003B8B3",variableType:93}
 */
var selectedDateAt = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"BA29F8EF-4CFA-4D07-BCA5-D929CDF8E2E5",variableType:8}
 */
var selectedJobOrder = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F5E93C75-1AE1-440A-A72E-7B70E3BACD12",variableType:8}
 */
var selectedUser = null;

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number}   queryType
 * @properties={typeid:24,uuid:"A3A5822A-2D08-4715-BC74-4949D8966618"}
 */
function exportReport(event,queryType) {
	var _query_download = null;
	var nameExport = null;
	if (globals.isEmpty(selectedDatefrom) || globals.isEmpty(selectedDateAt)) {
		globals.DIALOGS.showWarningDialog("Errore", "Inserire la data di inizio e fine", "OK");
		return
	}
	var arguments = new Array();
//	if (globals.isEmpty(selectedDateAt)) {
//		globals.DIALOGS.showWarningDialog("Errore", "Inserire la data di fine", "OK");
//		return
//	}
	if (queryType == 1) {
		_query_download = 'call sp_getReportEventsLog ( ?, ?, ?, ?)';
		nameExport = 'Consuntivi_';
		arguments[0] = selectedDatefrom;
		arguments[1] = selectedDateAt;
		arguments[2] = (selectedUser != null) ? selectedUser : 0;
		arguments[3] = (selectedJobOrder != null) ? selectedJobOrder : 0;
	} 
	else if (queryType == 2) {
		
		_query_download = 'call sp_general_expenses_amount (  ?, ?, ?, ?, ?, ?, ?)';
		nameExport = 'NS_';
		arguments[0] = 0;
		arguments[1] = 0;
		arguments[2] = 0;
		arguments[3] = selectedDatefrom;
		arguments[4] = selectedDateAt;
		arguments[5] = (selectedUser != null) ? selectedUser : 0;
		arguments[6] = (selectedJobOrder != null) ? selectedJobOrder : 0;
		
	}
	application.output('Query ' + _query_download);
	application.output(arguments);
	var dataset = databaseManager.getDataSetByQuery('geco', _query_download, arguments, 10000);
	
	var htmlTable = '<html>'+dataset.getAsHTML(true, true, false, true, true)+'</html>';
	
	forms.personnel_report_list.tableReport = htmlTable;
	forms.personnel_report_list.datasetReport = dataset;
	forms.personnel_report_list.nameExport = nameExport;
	
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"726E07A4-CB8B-4856-98D4-21584B67B501"}
 */
function refresh(event) {
	selectedDatefrom = null;
	selectedDateAt = null;
	selectedJobOrder = null;
	selectedUser = null;
		
	forms.personnel_report_list.tableReport = '';
	forms.personnel_report_list.datasetReport = null;
	forms.personnel_report_list.nameExport = '';	
}


/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"7C356730-118D-4BC4-8113-41F108A47BEF"}
 */
function updateUI(event) {
	_super.updateUI(event);
	if (globals.hasRole('HR Expenses') && !globals.hasRole('HR Administrators')){
		elements.expConsuntivi.visible = false;
	}
}
