
/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"57DBB728-AD61-4999-96DA-4C576501D068"}
 */
function onLoad(event) {
	 isDirectApprover = null;
	 approver = null;
	 _super.onLoad(event);
}
