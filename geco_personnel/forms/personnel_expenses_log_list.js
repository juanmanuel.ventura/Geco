/**
 * @param event
 *
 * @properties={typeid:24,uuid:"C7FF7227-4190-410E-9D31-7A2F8E1E2A18"}
 */
function updateUI(event) {
	_super.updateUI(event);
	if(globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses') ){
		elements.buttonAdd.visible = false;
		elements.buttonDelete.visible = false;
		elements.buttonClose.visible = false;
		elements.buttonApprove.visible = false;
		elements.buttonReject.visible = false;
		elements.buttonShift.visible = false;
		elements.buttonCloseMonth.visible = false;
		elements.printUserExpenses.visible = false;
		
	}
}



