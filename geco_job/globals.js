/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"46047838-D2F5-485E-8529-71D723017EB0"}
 */
//function onAction(event) {
//	var objMail = getMailType(1);
//	var message = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubstitution);
//	// Render a template and return the evaluated (String) result:
//	application.output(message);
//}

/**
 * Generico reminder di compilazione timesheet per tutti gli utenti tranne Diadema
 * @properties={typeid:24,uuid:"0BC599B9-A382-442B-8D00-30105F24826A"}
 * @AllowToRunInFind
 */
function reminderTimesheet() {
	application.output('#### reminderTimesheet ####');
	var totalUser = 0;

	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	if (users.find()) {
		users.is_enabled = 1;
		users.user_id = '!197'; // esclude una persona
		totalUser = users.search();
		totalUser = databaseManager.getFoundSetCount(users)
		application.output('dimensione reale users ' + totalUser);
	}

	//mail template
	var objMail = globals.getMailType(1);
	var bcc = '';
	var count = 0;
	for (var index = 1; index <= totalUser; index++) {
		var user = users.getRecord(index);
		//		application.output(user.user_name);
		var recipient = user.users_to_contacts.default_email.channel_value;
		var subject = objMail.subject;
		globals.mailSubstitution.nome = ''//user.users_to_contacts.first_name;
		var message = plugins.VelocityReport.evaluateWithContext(objMail.text, globals.mailSubstitution);
		count++;
		if (recipient) {
			bcc = bcc + recipient + ',';
			application.output(index + '************SENDING MAIL to ' + user.user_name + ' indirizzo ' + recipient);
		}
		//scopes.globals.enqueueMailReminder(recipient, subject, message);

		if (count == 30 || index == totalUser) {
			application.output(count + '--------------------------------------------------SENDING MAIL to \n' + bcc);
			scopes.globals.enqueueMailReminderTimesheet('[GECO] <noreply@spindox.it>', subject, message, bcc);
			bcc = '';
			count = 0;
		}
	}
}

/**
 * Cerca le richieste inserite il giorno precedente ancora in stato aperto e invia la mail ai relativi approvatori
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"2A5F8013-C192-49B1-B2F1-106389E1E0EA"}
 * @AllowToRunInFind
 */
function checkNewEventRequest_old(event) {
	application.output('#### checkNewEventRequest ####');
	var today = new Date();
	var yesterday = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
	application.output('today ' + today + ' yesterday ' + yesterday);
	var mm = yesterday.getMonth() + 1;
	if (mm.toString().length == 1) mm = '0' + mm;
	var yyyy = yesterday.getFullYear();
	var dd = yesterday.getDate();

	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	if (events_log.find()) {
		events_log.created_at = '#' + yyyy + '-' + mm + '-' + dd + '|yyyy-MM-dd';
		events_log.events_log_to_events.is_requestable = 1;
		events_log.event_log_status = 1;
		events_log.event_id = '!' + 34;
		events_log.search();
		events_log.sort('user_approver_id asc, user_id asc, event_log_date asc');
	}
	var lastApprover = null;
	//mail template
	var objMail = globals.getMailType(2);

	for (var index = 1; index <= events_log.getSize(); index++) {
		var evento = events_log.getRecord(index);
		if (evento.user_approver_id != lastApprover) {
			// se approvatore abilitato
			if (evento.events_log_to_approver_assigned.is_enabled == 1) {
				//application.output('ricevente ' + evento.events_log_to_approver_assigned.user_name);
				var recipient = evento.events_log_to_approver_assigned.users_to_contacts.default_email.channel_value;
				var subject = objMail.subject;
				globals.mailSubstitution.nome = evento.events_log_to_approver_assigned.users_to_contacts.first_name;
				var message = plugins.VelocityReport.evaluateWithContext(objMail.text, globals.mailSubstitution);

				application.output('************SENDING MAIL to ' + evento.events_log_to_approver_assigned.user_name);
				scopes.globals.enqueueMailReminder(recipient, subject, message);
			}
			lastApprover = evento.user_approver_id;
		}
	}
}

/**
 * Reminder per gli approvatori, calcola il numero di eventi e note spese non ancora approvati  e invia reminder agli approvatori
 * @properties={typeid:24,uuid:"6F2010F5-3E87-4CE9-992A-6A953C0F2D87"}
 * @AllowToRunInFind
 */
function reminderApprover() {
	application.output('#### reminderApprover ####');
	var today = new Date();
	var firstOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
	var mm = firstOfMonth.getMonth();
	if (mm.toString().length == 1) mm = '0' + mm;
	var yyyy = firstOfMonth.getFullYear();
	application.output("firstOfMonth " + firstOfMonth);

	/** @type {JSFoundSet<db:/geco/view_approvers_aggregate>} */
	var view_approvers_aggregate = databaseManager.getFoundSet('geco', 'view_approvers_aggregate');
	view_approvers_aggregate.loadAllRecords();
	if (view_approvers_aggregate.find()) {
		view_approvers_aggregate.mese = '' + yyyy + '' + mm + '';
		view_approvers_aggregate.search();
	}

	//mail template
	var objMail = globals.getMailType(3);
	application.output('size ' + view_approvers_aggregate.getSize());
	for (var index = 1; index <= view_approvers_aggregate.getSize(); index++) {
		var approver = view_approvers_aggregate.getRecord(index);
		//		application.output(approver.approver_id + ' ' + approver.cognome);
		var recipient = approver.view_approvers_aggregate_to_users.users_to_contacts.default_email.channel_value;
		var subject = objMail.subject;
		globals.mailSubstitution.nome = approver.view_approvers_aggregate_to_users.users_to_contacts.first_name;
		globals.mailSubstitution.numEventi = approver.numero_eventi;
		globals.mailSubstitution.numSpese = approver.numero_spese;
		//JStaffa aggiunto numero eventi a conteggio rimanenti da inviare all'approvatore
		globals.mailSubstitution.numEventConteggi = approver.numero_eventi_conteggio;
		var message = plugins.VelocityReport.evaluateWithContext(objMail.text, globals.mailSubstitution);

		application.output('************SENDING MAIL to ' + approver.cognome + ' ' + approver.nome);
		scopes.globals.enqueueMailReminder(recipient, subject, message);
	}
}

//function killInactiveClients(){
//	//get page
//	var user = "admin"
//	var password = "password"
//	var address = "localhost:8080"
//	var x = application.executeProgram('');
//	//executeProgram('curl', '-u', user + ":" + password, address + '/servoy-admin/clients');
//	//var x = application.executeProgram('curl', '-u', user + ":" + password, address + '/servoy-admin/clients');
//}

/**
 * http://www.quartz-scheduler.org/documentation/quartz-1.x/tutorials/crontrigger
 *  *  *  *  *  *  *  command to be executed
 *  |  |  |  |  |  |
 *  |  |  |  |  |  +----- day of week (1-7 or SUN-SAT)
 *  |  |  |  |  +------- month (1-12 or JAN-DEC)
 *  |  |  |  +--------- day of month (1 - 31)
 *  |  |  +----------- hour (0 - 23)
 *  |  +------------- min (0 - 59)
 *  +--------------sec (0 - 59)
 *  es.
 *  0 0 12 * * ?	    – at 12pm (noon) every day
 *	0 0/5 14,18 * * ?	– every 5 minutes starting at 2pm and ending at 2:55pm, AND every 5 minutes starting at 6pm and ending at 6:55pm, every day
 * 	0 10,44 14 ? 3 WED	– at 2:10pm and at 2:44pm every Wednesday in the month of March.
 *	0 15 10 15 * ?	    – at 10:15am on the 15th day of every month
 *
 * @properties={typeid:24,uuid:"886773CA-7C64-4569-8276-41B3F7CF2F14"}
 */
function onStartUp() {
	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		application.putClientProperty(APP_WEB_PROPERTY.WEBCLIENT_TEMPLATES_DIR, 'custom');
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_DEFAULT_SCROLLABLE, true);
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_SCROLLABLE_KEEP_LOADED_ROWS, true);
	}
	plugins.UserManager.register("Spindox", "q9SA5eCyb085cvATVO8s9onGe3iBzJyCsUrf0J3NcZ7eiAqcRalsfw==");

	//reminder timesheet alle 7:01 il 27 del mese
	//plugins.scheduler.addCronJob('reminderTimesheet', '1 1 12 29 * ?', reminderTimesheet);
	plugins.scheduler.addCronJob('reminderTimesheet', '1 1 7 27 * ?', reminderTimesheet);

	//new request tutti i giorni alle 7:30
	//plugins.scheduler.addCronJob('checkNewEventRequest', '1 30 7 * * ?', checkNewEventRequest);
	plugins.scheduler.addCronJob('checkNewEventRequest', '1 30 7 * * ?', checkNewEventRequest);

	//reminder approver tutti i giorni alle 8:01
	plugins.scheduler.addCronJob('reminderApprover', '1 1 8 * * ?', reminderApprover);
	//plugins.scheduler.addCronJob('reminderApprover', '1 1 8 * * ?', reminderApprover);

	//reminderUserTimesheet tutti i giorni alle 8:30
	plugins.scheduler.addCronJob('reminderUserTimesheet', '1 30 8 * * ?', reminderUserTimesheet)
	//plugins.scheduler.addCronJob('reminderUserTimesheet','1 30 8 * * ?',reminderUserTimesheet)

	//disconnectIdleClients tutti i giorni ogni 30 minuti
	plugins.scheduler.addCronJob('disconnectIdleClients', '0 0/30 * * * ?', disconnectIdleClients);
	//	plugins.scheduler.addCronJob('disconnectIdleClients', '0 0/3 * * * ?', disconnectIdleClients);
}

/**
 * @properties={typeid:24,uuid:"CF1B1B53-15F3-462E-84EF-7925BF2BAECC"}
 * @AllowToRunInFind
 */
function testMail() {
	application.output('#### TestMail ####');
	var totalUser = 0;

	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	if (users.find()) {
		users.is_enabled = 1;
		users.user_id = '63';
		totalUser = users.search();
		totalUser = databaseManager.getFoundSetCount(users)
		application.output('dimensione reale users ' + totalUser);
	}
	//mail template
	var objMail = globals.getMailType(1);

	for (var index = 1; index <= totalUser; index++) {
		var user = users.getRecord(index);
		//		application.output(user.user_name);
		var recipient = user.users_to_contacts.default_email.channel_value;
		var subject = objMail.subject;
		globals.mailSubstitution.nome = user.users_to_contacts.first_name;
		var message = plugins.VelocityReport.evaluateWithContext(objMail.text, globals.mailSubstitution);

		application.output('************SENDING MAIL to ' + user.user_name);
		scopes.globals.enqueueMailReminder(recipient, subject, message);
	}
}

/**
 * Cerca le richieste inserite il giorno precedente ancora in stato aperto e invia la mail ai relativi approvatori
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"1D48567E-85B2-42AB-BC12-29CA890B63C7"}
 * @AllowToRunInFind
 */
function checkNewEventRequest(event) {
	application.output('#### checkNewEventRequest ####');
	var today = new Date();
	var yesterday = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
	application.output('today ' + today + ' yesterday ' + yesterday);
	var mm = yesterday.getMonth() + 1;
	if (mm.toString().length == 1) mm = '0' + mm;
	var yyyy = yesterday.getFullYear();
	var dd = yesterday.getDate();

	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	if (events_log.find()) {
		events_log.created_at = '#' + yyyy + '-' + mm + '-' + dd + '|yyyy-MM-dd';
		events_log.events_log_to_events.is_requestable = 1;
		events_log.event_log_status = 1;
		events_log.event_id = '!' + 34;
		events_log.search();
		events_log.sort('user_approver_id asc, user_id asc, event_id asc, event_log_date asc');
	}
	var currentApprover = null;
	var currentUser = null;
	var currentEvent = null;
	//verranno valorizzate a ogni giro del for
	var currentUserName, currentEventName = '';
	//verranno valorizzate solo quando cambia utente o evento
	var currentUserName2, currentEventName2 = '';
	var period = '';
	var mailText = '';
	//mail template
	var objMail = globals.getMailType(10);
	var subject = objMail.subject;

	application.output('---------------------------------------------------------');
	for (var i = 1; i <= events_log.getSize(); i++) {
		var evento1 = events_log.getRecord(i);
		application.output('approvatore ' + evento1.user_approver_id + ' utente ' + evento1.events_log_to_users.users_to_contacts.real_name + ' evento ' + evento1.events_log_to_events.event_name + ' data ' + evento1.event_log_date);
	}
	application.output('---------------------------------------------------------');

	for (var index = 1; index <= events_log.getSize(); index++) {
		var evento = events_log.getRecord(index);
		var eventoNext = (index + 1 <= events_log.getSize()) ? events_log.getRecord(index + 1) : null;
		var message = '';
		var recipient = '';
		currentApprover = evento.user_approver_id;
		currentUser = evento.user_id
		currentEvent = evento.event_id;

		currentUserName = evento.events_log_to_users.users_to_contacts.real_name;
		currentEventName = evento.events_log_to_events.event_name;
		period = period + evento.event_log_date.toLocaleDateString() + ' - ';

		if (mailText == '') mailText = currentUserName + ' ha richiesto\n\t' + currentEventName + ' nel periodo ' + period;
		else {
			if (currentUserName2 == '') mailText = mailText + '\n' + currentUserName + ' ha richiesto\n\t' + currentEventName + ' nel periodo ' + period;
			else if (currentEventName2 == '') mailText = mailText + ' \n\t' + currentEventName + ' nel periodo ' + period;
			else mailText = mailText + evento.event_log_date.toLocaleDateString() + ' - ';

		}
		currentUserName2 = (currentUserName2 != '') ? currentUserName2 : currentUserName;
		currentEventName2 = (currentEventName2 != '') ? currentEventName2 : currentEventName;
		if (eventoNext != null) {

			if (currentApprover != eventoNext.user_approver_id) {
				//application.output('1+++++++++++INVIO seconda mail\n' + mailText + '\n+++++++++++++++');
				//INVIO MAIL
				recipient = evento.events_log_to_approver_assigned.users_to_contacts.default_email.channel_value;
				globals.mailSubstitution.nome = evento.events_log_to_approver_assigned.users_to_contacts.first_name;
				//per evitare il <br> nel corpo della mail
				globals.mailSubstitution.mailText = '$mailText';
				message = plugins.VelocityReport.evaluateWithContext(objMail.text, globals.mailSubstitution);
				message = message.replace("$mailText", mailText);

				application.output('\n************SENDING MAIL to ' + globals.mailSubstitution.nome);
				application.output(message);
				application.output('************\n');
				scopes.globals.enqueueMailReminder(recipient, subject, message);
				period = '';
				currentEventName2 = '';
				currentUserName2 = '';
				mailText = '';
			} //stesso approvatore, diverso user
			else if (currentUser != eventoNext.user_id) {
				period = '';
				currentUserName2 = ''
				currentEventName2 = '';
			} //stesso approvatore, stesso utente, diverso evento
			else if (currentEvent != eventoNext.event_id) {
				period = '';
				currentEventName2 = '';
			} //stesso approvatore, stesso utente, stesso evento diverse date
			else {
				//				application.output('4 stesso approvatore stesso utente stesso evento giorno diverso');
			}

		} else {
			//application.output('FINE INVIO seconda mail\n' + mailText + '\n+++++++++++++++');
			recipient = evento.events_log_to_approver_assigned.users_to_contacts.default_email.channel_value;
			globals.mailSubstitution.nome = evento.events_log_to_approver_assigned.users_to_contacts.first_name;
			globals.mailSubstitution.mailText = '$mailText';
			message = plugins.VelocityReport.evaluateWithContext(objMail.text, globals.mailSubstitution);
			message = message.replace("$mailText", mailText);
			application.output('\n************SENDING MAIL to ' + globals.mailSubstitution.nome);
			application.output(message);
			application.output('************\n');
			scopes.globals.enqueueMailReminder(recipient, subject, message);
		}
	}
}

/**
 * Reminder per timesheet incompleto: calcola il numero di ore mancanti per il mese precedente e invia notifica ai relativi utenti
 * solo per dipendenti tranne chi non ha fep code
 * @properties={typeid:24,uuid:"885C31A1-C78A-4E22-AFB7-E376D6F9A9F7"}
 * @AllowToRunInFind
 */
function reminderUserTimesheet() {
	application.output('#### reminderUserTimesheet ####');

	var today = new Date();

	var previousMonthDate = new Date(today.getFullYear(), today.getMonth() - 1, 1);
	var year = previousMonthDate.getFullYear();
	var month = previousMonthDate.getMonth() + 1;

	application.output('anno ' + year + ' mese ' + month);

	/** @type {JSFoundSet<db:/geco/calendar_days>} */
	var calendar_days = databaseManager.getFoundSet('geco', 'calendar_days');
	var workableDay = 0;
	if (calendar_days.find()) {
		calendar_days.calendar_year = year;
		calendar_days.calendar_month = month;
		calendar_days.is_working_day = 1;
		calendar_days.is_holiday = 0;
		workableDay = calendar_days.search();
	}

	//mail template
	var objMail = globals.getMailType(4);

	var maxReturnedRows = 10000; //useful to limit number of rows
	//	var server_name = databaseManager.getDataSourceServerName(controller.getDataSource());
	var sql_query = 'call sp_getHoursMonth(?,?)';
	var arguments = new Array();
	arguments[0] = year;
	arguments[1] = month;
	var dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, maxReturnedRows);

	for (var index = 1; index <= dataset.getMaxRowIndex(); index++) {

		var riga = dataset.getRowAsArray(index);
		var userID = riga[0];
		//var userName = riga[1];
		var dataAss = riga[3];
		var dataDim = riga[4]
		var oreInserite = riga[6];
		var oreRespinte = riga[7];
		//application.output('-----------------------------------------------------\nUtente ' + userID + ' inserite ' + oreInserite);
		var oreLavorabili = 0;
		var numGg = 0;
		//application.output('Assunzione ' + dataAss + ' Dimissioni ' + dataDim + ' Data ' + dateStart);
		for (var i = 1; i <= calendar_days.getSize(); i++) {
			var dateStart = calendar_days.getRecord(i).calendar_date;
			//application.output(dataAss + ' - ' +dateStart + ' Assunzione minore data =  ' + (dataAss <= dateStart) + ' - Dimissioni maggiore di data = ' + (dataDim == null || dataDim >= dateStart) );
			if (dataAss <= dateStart && (dataDim == null || dataDim >= dateStart)) {
				if (!globals.isWeekEnd(dateStart) && !globals.isHoliday(dateStart) && !globals.isUserHoliday(dateStart.getMonth() + 1, dateStart.getDate(), userID)) {
					numGg++;
					var objTimeUser = globals.getUserTimeWorkByDay(userID, dateStart);
					oreLavorabili = oreLavorabili + objTimeUser.workingHours;

				}
			}
		}

		var oreMancanti = oreLavorabili - oreInserite;
		application.output('-----------------------\nUtente ' + userID + ' ore lavorabili =  ' + oreLavorabili + ' ore Inserite = ' + oreInserite + '  ore mancanti =  ' + oreMancanti + ' ore respinte ' + oreRespinte);

		if (oreMancanti > 0 || oreRespinte > 0) {
			/** @type {JSFoundSet<db:/geco/users>} */
			var users = databaseManager.getFoundSet('geco', 'users');
			if (users.find()) {
				users.user_id = userID;
				users.search();
			}
			//nessun evento inserito o meno delle ore lavorabili
			var recipient = users.users_to_contacts.default_email.channel_value;
			var subject = objMail.subject;
			globals.mailSubstitution.nome = users.users_to_contacts.first_name;
			if (oreMancanti > 0)
				globals.mailSubstitution.oreMancanti = oreMancanti;
			else
				globals.mailSubstitution.oreMancanti = 0;
			if (oreRespinte > 0) {
				globals.mailSubstitution.oreRespinte = 'Sono presenti ' + oreRespinte + ' ore respinte.';
			} else globals.mailSubstitution.oreRespinte = '';
			var message = plugins.VelocityReport.evaluateWithContext(objMail.text, globals.mailSubstitution);
			//application.output(message);
			if (recipient != null) {
				application.output('************SENDING MAIL to ' + users.user_name + ' totale ore ' + oreInserite + '  ore mancanti ' + oreMancanti);
				scopes.globals.enqueueMailReminder(recipient, subject, message);
			} else
				application.output('Indirizzo mail non trovato');
		} else if (oreInserite >= oreLavorabili) {
			application.output('************ore raggiunte no mail per user ' + userID);
		}
	}
	application.output('#### STOPreminderUserTimesheet ####');
}

/**
 * @properties={typeid:24,uuid:"D794F69A-E88D-415D-9AFC-029FDFEA2872"}
 */
function disconnectIdleClients() {
	application.output(globals.messageLog + 'START geco_job.disconnectIdleClients() disconnectIdleClients', LOGGINGLEVEL.INFO);
	try {

		plugins.UserManager.getClients()
		//		var validPassword = plugins.servoyguy_robot.checkAccessPassword('password');
		//        if(!validPassword)
		//            return;
		//        application.output('PASSWORD OK');
		var clients = plugins.UserManager.getClients();
		application.output(globals.messageLog + 'geco_job.disconnectIdleClients() clients: ' + clients, LOGGINGLEVEL.INFO);
		for (var i = 0; i < clients.length; i++) {
			var cl = clients[i];
			application.output('-------------------------------------------------');
			application.output(globals.messageLog + 'geco_job.disconnectIdleClients() getApplicationType: ' + cl.applicationType, LOGGINGLEVEL.INFO);
			application.output(globals.messageLog + 'geco_job.disconnectIdleClients() getClientId ' + cl.clientId, LOGGINGLEVEL.INFO);
			application.output(globals.messageLog + 'geco_job.disconnectIdleClients() getIdleTimestamp ' + cl.idle, LOGGINGLEVEL.INFO);
			application.output(globals.messageLog + 'geco_job.disconnectIdleClients() getLoginTimestamp ' + cl.login, LOGGINGLEVEL.INFO);
			application.output(globals.messageLog + 'geco_job.disconnectIdleClients() getSolutionNAME ' + cl.solutionName, LOGGINGLEVEL.INFO);
			application.output(globals.messageLog + 'geco_job.disconnectIdleClients() getUserName ' + cl.userName, LOGGINGLEVEL.INFO);
			application.output(globals.messageLog + 'geco_job.disconnectIdleClients() getUserUid ' + cl.userUid, LOGGINGLEVEL.INFO);
			application.output(globals.messageLog + 'geco_job.disconnectIdleClients() isAlive ' + cl.alive, LOGGINGLEVEL.INFO);
			application.output(globals.messageLog + 'geco_job.disconnectIdleClients() solutionOpened ' + cl.solutionOpened, LOGGINGLEVEL.INFO);

			var idleTS = cl.idle;
			var now = new Date();
			var diff = now.getTime() - idleTS.getTime();
			var diffSeconds = Math.abs(diff / 1000);
			var diffMinuts = Math.round(diffSeconds / 60);
			application.output(globals.messageLog + 'geco_job.disconnectIdleClients() INACTIVE FROM ' + diffSeconds + ' SECONDS --> ' + diffMinuts + ' MINUTS', LOGGINGLEVEL.INFO);

			if (cl.solutionName != 'geco_job') {
				//				if (diffMinuts > 3) {
				if (diffMinuts > 30) {
					/** @type {String[]} */
					var groups = ['Controllers', 'HR Administrators', 'HR Expenses', 'Admin Readers', 'Administrators', 'Delivery Manager', 'Delivery Admin', 'Planner', 'Steering Board', 'Approvers'];
					var isRoleInList = hasRoleGecoJob(cl.userUid, groups);
					//var isController = hasRoleGecoJob(cl.userUid, 'Controllers');
					//					var isPersonnel = hasRoleGecoJob(cl.userUid,'HR Administrators');
					//					var isAdminReader = hasRoleGecoJob(cl.userUid, 'Admin Readers');
					//					var isAdmin = hasRoleGecoJob(cl.userUid, 'Administrators');
					//					var isDeliveryManager = hasRoleGecoJob(cl.userUid, 'Delivery Manager');
					//					var isDeliveryAdmin = hasRoleGecoJob(cl.userUid, 'Delivery Admin');
					//					var isPlanner = hasRoleGecoJob(cl.userUid, 'Planner');
					//					var isSteeringBoard = hasRoleGecoJob(cl.userUid, 'Steering Board');
					//					var isApprover = hasRoleGecoJob(cl.userUid, 'Approvers');
					//					var isHRExpenses = hasRoleGecoJob(cl.userUid, 'HR Expenses');
					if (!globals.isEmpty(cl.userUid)) {
						//						if (!isController && !isPersonnel) {
						if (!isRoleInList) {
							application.output(globals.messageLog + 'geco_job.disconnectIdleClients() !! FORCE SHUTDOWN !!', LOGGINGLEVEL.INFO)
							cl.shutdown();
						} else application.output(globals.messageLog + 'geco_job.disconnectIdleClients() LASCIO IN VITA I CLIENT PERCHE\' RUOLO UTENTE E\' UNO TRA QUESTI: ' + groups, LOGGINGLEVEL.INFO)
					} else {
						application.output(globals.messageLog + 'geco_job.disconnectIdleClients() !! FORCE SHUTDOWN !!', LOGGINGLEVEL.INFO)
						cl.shutdown();
					}
				}
			}
		}
	} catch (e) {
		application.output(e)
	} finally {
		application.output(globals.messageLog + 'STOP geco_job.disconnectIdleClients()', LOGGINGLEVEL.INFO);
	}
}

/**
 * Return true if user is in group
 * @param {String[]} group
 * @param {String} userId
 * @return {Boolean}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"E0EAB87A-8880-4CAE-94A0-13EB4B60227D"}
 */
function hasRoleGecoJob(userId, group) {
	application.output(globals.messageLog + 'START geco_jobs.hasRoleGecoJob() userId: ' + userId + '; group: ' + group, LOGGINGLEVEL.INFO);
	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var usersRoles = databaseManager.getFoundSet('geco', 'user_groups');
	/** @type {JSFoundSet<db:/geco/groups>} */
	var roles = databaseManager.getFoundSet('geco', 'groups');
	var returnVal = false;
	if (users.find()) {
		users.user_id = userId;
		var tot = users.search();
		if (tot > 0) {
			var recUser = users.getRecord(1);
			if (usersRoles.find()) {
				usersRoles.user_id = recUser.user_id;
				var totUserRoles = usersRoles.search();
				if (totUserRoles > 0) {
					for (var index = 1; index <= usersRoles.getSize(); index++) {
						var recUserRole = usersRoles.getRecord(index);

						if (roles.find()) {
							roles.group_id = recUserRole.group_id;
							var totRoles = roles.search();

							if (totRoles > 0) {
								var recRole = roles.getRecord(1);
								application.output('recRole.name: ' + recRole.name);
								if (group.indexOf(recRole.name) > -1) {
									returnVal = true;
									break;
								}
							}
						}
					}
				}
			}
		}
	}
	application.output(globals.messageLog + 'STOP geco_jobs.hasRoleGecoJob() utente -' + userId + '-  ha quel ruolo? ' + returnVal, LOGGINGLEVEL.INFO);
	return returnVal;
}
