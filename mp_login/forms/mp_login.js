/**
 * @type {String}
 * @properties={typeid:35,uuid:"339688EF-4ACF-4F4C-857D-E02217239373"}
 */
var password = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"C3439C54-4F47-45CD-B5B9-4070A8ACFD24"}
 */
var userName = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A413E41D-9797-452E-8F59-AF212D6AB7A5"}
 */
var resultMessage = null;

/**
 * @properties={typeid:35,uuid:"C658DF31-8854-451C-B3CE-CC7C0ACF9867",variableType:-4}
 */
var client = null;

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C6D53350-3375-493A-A31D-50C03CC1A4CB"}
 */
function login(event) {
	application.output('START mp_login.login() ' + userName, LOGGINGLEVEL.INFO);
//	switch (userName) {
//	case 'fs':
//		userName = 'francesco.sigolotto';
//		password = "spindox";
//		break;
//	case 'js':
//		userName = 'jonathan.staffa';
//		password = 'spindox';
//		break;
//	case 'js1':
//		userName = 'jonathan.staffa.1';
//		password = 'spindox';
//		break;
//	case 'lp':
//		userName = 'laura.pirovano';
//		password = 'spindox';
//		break;
//	case 'fb':
//		userName = 'fabrizio.bindi';
//		password = 'spindox';
//		break;
//	case 'mm':
//		userName = 'mauro.marengo';
//		password = 'spindox';
//		break;
//	}
	var result = security.authenticate("mp_authenticator", "mpLoginUser", [userName, password]);
	if (result) {
		if (result['logged']) {
			application.output('STOP mp_login.login() OK ' + userName, LOGGINGLEVEL.INFO);
			globals.isFromMedicalPractice = true;
			return true;
		} else {
			resultMessage = result['error'];
			application.output('STOP mp_login.login() KO ' + userName + ' ' + resultMessage, LOGGINGLEVEL.INFO);
			return false;
		}
	} else {
		resultMessage = 'ERRORE APPLICATIVO'
		application.output('STOP mp_login.login() KO APPLICATION ERROR' + +userName, LOGGINGLEVEL.ERROR);
		return false;
	}

}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {String} user the user
 * @private\
 * @properties={typeid:24,uuid:"CE8C6C0D-4344-4120-A0C1-0517F84A42D7"}
 */
function logMeIn(event, user) {
	userName = user;
	login(event);
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"975362C7-CE8B-4176-B09F-910CECCF664D"}
 */
function onLoad(event) {
	if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
		elements.image.visible = true;
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"C8593B5C-7DC5-441E-8E91-84138A1EC03F"}
 * @AllowToRunInFind
 */
function callLostPassword2(event) {
	application.output('START mp_login.callLostPassword2() ', LOGGINGLEVEL.INFO);
	if (userName != null) {
		application.output('mp_login.callLostPassword2() ' + userName + ' solution caller ' + application.getSolutionName(), LOGGINGLEVEL.INFO);
		/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
		var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
		if (users.find()) {
			users.user_name = userName;
			var tot = users.search();

			if (tot >= 1) {
				var rec = users.getRecord(1);
				if (rec.company_name == 'SPINDOX' && rec.geco_user_id != null) {
					resultMessage = 'Sei di Spindox, effettua il reset password da GeCo.';
					application.output('START mp_login.callLostPassword2() ERROR: ' + resultMessage, LOGGINGLEVEL.INFO);
					return;
				}
				if (rec.user_email == null) {
					resultMessage = 'Nessuna e-mail associata all\'utente. Reset fallito.';
					application.output('START mp_login.callLostPassword2() ERROR: ' + resultMessage, LOGGINGLEVEL.INFO);
					return;
				}
			}
		}
		client = plugins.headlessclient.createClient("mp_queue", null, null, null);
		if (client != null && client.isValid()) {
			application.output('START mp_login.callLostPassword2() Cient OK ' + client.getClientID(), LOGGINGLEVEL.INFO);
			client.queueMethod(null, "mpSendMailResetPassword", [userName, application.getServerURL(), application.getSolutionName()], enqueueMailLog);
			//resultMessage = 'Inviata mail';
		} else {
			application.output('START mp_login.callLostPassword2() Client is not valid ', LOGGINGLEVEL.INFO);
		}
	} else {
		resultMessage = 'Lo username non può essere vuoto';
		application.output('START mp_login.callLostPassword2() ERROR: ' + resultMessage, LOGGINGLEVEL.INFO);
	}
	application.output('STOP mp_login.callLostPassword2 ', LOGGINGLEVEL.INFO);
}

/**
 * @param {JSEvent} headlessCallback
 * @properties={typeid:24,uuid:"1090079D-E43E-4178-8831-5A9510CDA82C"}
 */
function enqueueMailLog(headlessCallback) {
	application.output('START mp_login.enqueueMailLog() ', LOGGINGLEVEL.INFO);
	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		var outMessege = '-------------SENDING MAIL----------------------' + '\ntimestamp: ' + headlessCallback.getTimestamp() + '\nrecipient: ' + headlessCallback.data.recipient + '\nsubject: ' + headlessCallback.data.subject + '\nmessage: ' + headlessCallback.data.message + '\nsuccess: ' + headlessCallback.data.success.toString().toUpperCase() + '\n-----------------------------------------------\n'
		application.output('mp_login.callLostPassword2 result: ' + outMessege, LOGGINGLEVEL.INFO);
		if (headlessCallback.data.success == false)
			resultMessage = headlessCallback.data.message;
		else
			resultMessage = 'Inviata mail per reset password';
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output('mp_login.callLostPassword2 exception callback, name: ' + headlessCallback.data + outMessege, LOGGINGLEVEL.ERROR);
		resultMessage = 'Errore invio mail'
	}
	application.output('mp_login.callLostPassword2 message: ' + resultMessage, LOGGINGLEVEL.INFO);
	if (client != null && client.isValid()) {
		application.output('mp_login.callLostPassword2 close client ' + client.getClientID(), LOGGINGLEVEL.INFO);
		client.shutdown();
	}
	application.output('STOP mp_login.enqueueMailLog() ', LOGGINGLEVEL.INFO);
}

/**
 * @param {String} formName
 * @properties={typeid:24,uuid:"57BF4298-6D40-4CD5-9298-E8EF79314EDF"}
 */
function prepareWindow(formName) {
	var form = forms[formName];

	// hide menu and tool bars
	plugins.window.getMenuBar().setVisible(false);
	plugins.window.setToolBarAreaVisible(false);
	plugins.window.setStatusBarVisible(false);

	// set size and center the window
	var window = application.getWindow();
	window.show(form);
	if (application.getOSName() != 'Mac OS X') plugins.window.maximize();
}
