/**
 * @type {String}
 * @properties={typeid:35,uuid:"C34ECF75-87B9-480C-9C35-67B0279319EF"}
 */
var selectedEntity = null;

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"E22CC2F6-A98C-45F3-BBA1-F5A3DF26D818"}
 */
function goUp(event) {
	var recordAbove = foundset.getRecord(foundset.getSelectedIndex() - 1);
	if (recordAbove) {
		recordAbove.execution_order += 1;
		foundset.getSelectedRecord().execution_order -= 1;
		foundset.sort('execution_order');
		databaseManager.saveData(foundset);
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"5ED01162-9770-4C8A-9646-1D648D313475"}
 */
function goDown(event) {
	var recordBelow = foundset.getRecord(foundset.getSelectedIndex() + 1);
	// for a workaround of what seems to be a bug
	var wasFirstRecord = foundset.getSelectedIndex() == 1 ? true : false;
	if (recordBelow) {
		recordBelow.execution_order -= 1;
		foundset.getSelectedRecord().execution_order += 1;
		// the workaround
		if (wasFirstRecord) foundset.setSelectedIndex(2);
		foundset.sort('execution_order');
		databaseManager.saveData(foundset);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"061AC449-5908-4174-B632-73273AA3D12E"}
 */
function onActionScanForNewRules(event) {
	scopes.globals.scanForNewMethods();
	orderRules(event);
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"8257DFA5-9DF0-4295-9AE6-7A9AB4845013"}
 */
function orderRules(event) {
	//	foundset.loadAllRecords();
	databaseManager.setAutoSave(false);
	for (var index = 1; index <= foundset.getSize(); index++) {
		foundset.getRecord(index).execution_order = index;
	}
	databaseManager.setAutoSave(true);
	controller.sort('execution_order');
	_super.updateUI(event);
}

/**
 * @private
 *
 * @properties={typeid:24,uuid:"5145F89D-D698-4DF1-AD81-A6C69AF5ADA1"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	if (foundset.find()) {
		foundset.working_entity = selectedEntity;
		foundset.search();
	}
}

/**
 * Handle changed data.
 *
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"38B05AB3-DC0D-4A37-B2D4-D33226F9E8F6"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	orderRules(event);
	return true
}
