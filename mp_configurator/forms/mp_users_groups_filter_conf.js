/**
 * @type {Number}
 * @properties={typeid:35,uuid:"0594E703-E293-4FC2-A31D-30C7900FFB59",variableType:8}
 */
var usersGroup = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"EAF027B3-3572-42A5-BE69-C933812F9B81",variableType:8}
 */
var userId = null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"7507D4B9-C0A9-4E42-B816-2731C275E968"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"824AB1B9-8B6E-41ED-B845-71F0AAB02FDD"}
 */
function resetFilter(event, goTop) {
	usersGroup = null;
	userId = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"D89C22FB-6A06-4C69-A22F-6562DB7EDE9E"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.mp_users_to_mp_users_roles.role_id = usersGroup;
		foundset.user_id = userId;
		foundset.search();
	}
}
