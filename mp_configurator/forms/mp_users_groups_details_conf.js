/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A91662E3-1B0F-4C00-BD4D-4ED227E6EE41"}
 */
var surname = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DA8154AE-9388-4D28-A3A8-F4C933FF487B"}
 */
var name = '';

/**
 * 
 *
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"425F58ED-A844-4794-B3B2-EBEFCDA18685"}
 * @AllowToRunInFind
 */
function saveEdits(event) {

	var rec = foundset.getSelectedRecord();
	application.output('rec: ' + rec);
	if (rec && rec.isNew()){
		//Cognome o nome mancanti
		if(scopes.globals.isEmpty(surname) || scopes.globals.isEmpty(name)){
			scopes.globals.DIALOGS.showErrorDialog('Error','- Inserire sia nome che cognome','OK');
			return;
		}//Cognome e nome ci sono, verifico se l'username da mettere c'è già
		else{
			var username;
			var realName = surname + ' ' + name;
			var first = name.toLowerCase();
			var last = surname.toLowerCase();
			first = globals.mpTrimStringNew(first);
			last = globals.mpTrimStringNew(last);
			username = first + "." + last;
			application.output('username nuovo utente: ' + username);

			/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
			var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
			if(users.find()){
				users.user_name = username;
				
				var totUsers = users.search();
				if (totUsers >= 1){
					scopes.globals.DIALOGS.showErrorDialog('Error','- L\'utente che si vuole creare esiste già, modificare i dati','OK');
					return;
				}else{
					foundset.user_name = username;
					foundset.real_name = realName;
				}
			}
		}
		
		//verifico che abbia inserito il nome dell'azienda
		if (scopes.globals.isEmpty(foundset.company_name)) {
			scopes.globals.DIALOGS.showErrorDialog('Error','- Inserire il nome dell\'azienda','OK');
			return;
		}//controllo che, se ha messo SPINDOX, allora metta anche il FEP CODE
		else{
			if (company_name == 'SPINDOX' && scopes.globals.isEmpty(foundset.fep_code)){
				scopes.globals.DIALOGS.showErrorDialog('Error','- Inserire il codice fep per l\'utente SPINDOX','OK');
				return;
			} else if (company_name == 'SPINDOX' && !scopes.globals.isEmpty(foundset.fep_code)){
				/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
				var users2 = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
				
				if(users2.find()){
					users2.fep_code = foundset.fep_code;
					var totUsers2 = users2.search();
					
					if (totUsers2 >= 1){
						scopes.globals.DIALOGS.showErrorDialog('Error','- Il codice fep inserito per l\'utente SPINDOX è già presente; inserirne un altro','OK');
						return;
					}
				}
			}
		}
		
		foundset.user_password = scopes.globals.sha1('generali');
		application.output('password post sha1: ' + foundset.user_password);
	}
	
	forms.mp_groups_for_user_list_conf.deleteRecordsSelected(event);
	if (globals.deleteExceptionMessage != null && globals.deleteExceptionMessage != '') {
		application.output('groups_to_users ' + globals.deleteExceptionMessage);
		globals.DIALOGS.showErrorDialog('Error', globals.deleteExceptionMessage, 'OK');
	}
	
	_super.saveEdits(event);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"A96C224A-5205-4C44-89FD-9BF60EC42D94"}
 */
function stopEditing(event) {
	_super.stopEditing(event);
	
	//RENDO VISIBILE REAL_NAME, NASCONDO NOME E COGNOME
	elements.lRealName.visible = true;
	elements.fRealName.visible = true;
	elements.lSurname.visible = false;
	elements.fSurname.visible = false;
	elements.lName.visible = false;
	elements.fName.visible = false;
	
	//NON RENDO EDITABILI CAMPI AZIENDA E FEP
	elements.fFepCode.enabled = false;
	elements.fCompanyName.enabled = false;
	
	//NASCONDO UNA TENDINA E NE MOSTRO UN'ALTRA
	forms.mp_groups_for_user_list_conf.elements.user_approver_id.enabled = true;
	forms.mp_groups_for_user_list_conf.elements.user_approver_id.visible = true;
	forms.mp_groups_for_user_list_conf.elements.user_approver_id_copy.enabled = false;
	forms.mp_groups_for_user_list_conf.elements.user_approver_id_copy.visible = false;
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"888D421B-5BCD-4456-9401-1F86647FEC4B"}
 */
function onShow(firstShow, event) {
//	_super.onShow(firstShow, event);

	//RENDO VISIBILE REAL_NAME, NASCONDO NOME E COGNOME
	elements.lRealName.visible = true;
	elements.fRealName.visible = true;
	elements.lSurname.visible = false;
	elements.fSurname.visible = false;
	elements.lName.visible = false;
	elements.fName.visible = false;
	
	//NON RENDO EDITABILI CAMPI AZIENDA E FEP
	elements.fFepCode.enabled = false;
	elements.fCompanyName.enabled = false;
	
	//NASCONDO UNA TENDINA E NE MOSTRO UN'ALTRA
	forms.mp_groups_for_user_list_conf.elements.user_approver_id.enabled = true;
	forms.mp_groups_for_user_list_conf.elements.user_approver_id.visible = true;
	forms.mp_groups_for_user_list_conf.elements.user_approver_id_copy.enabled = false;
	forms.mp_groups_for_user_list_conf.elements.user_approver_id_copy.visible = false;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"3A4E0440-2AD8-4745-9D17-2DEDC2947674"}
 */
function startEditing(event) {
	_super.startEditing(event);
	
	//SE RECORD NON E' NUOVO, NON RENDO EDITABILE L'AZIENDA
	var rec = foundset.getSelectedRecord();
	if (rec && !rec.isNew()) elements.fCompanyName.enabled = false;
	
	//NASCONDO UNA TENDINA E NE MOSTRO UN'ALTRA
	forms.mp_groups_for_user_list_conf.elements.user_approver_id.enabled = false;
	forms.mp_groups_for_user_list_conf.elements.user_approver_id.visible = false;
	forms.mp_groups_for_user_list_conf.elements.user_approver_id_copy.enabled = true;
	forms.mp_groups_for_user_list_conf.elements.user_approver_id_copy.visible = true;
}
