/**
 * @type {String}
 * @properties={typeid:35,uuid:"C873B163-978A-4EE1-82DC-7DEB234EE113"}
 */
var selectedEntity = null;

/**
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"3E97C100-2793-4E54-A7E5-262AD3B4A1BC"}
 */
function goUp(event) {
	var recordAbove = foundset.getRecord(foundset.getSelectedIndex() - 1);
	if (recordAbove) {
		recordAbove.execution_order += 1;
		foundset.getSelectedRecord().execution_order -= 1;
		foundset.sort('execution_order');
		saveRulesOrder(event);
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"7F7C5023-62EE-4C43-987A-AECB91F51A76"}
 */
function goDown(event) {
	var recordBelow = foundset.getRecord(foundset.getSelectedIndex() + 1);
	// for a workaround to what seems to be a bug
	var wasFirstRecord = foundset.getSelectedIndex() == 1 ? true : false;
	if (recordBelow) {
		recordBelow.execution_order -= 1;
		foundset.getSelectedRecord().execution_order += 1;
		// the workaround
		if (wasFirstRecord) foundset.setSelectedIndex(2);
		foundset.sort('execution_order');
		saveRulesOrder(event);
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"16041848-39E1-4906-A7AF-62F5265DF9E3"}
 */
function onActionScanForNewRules(event) {
	scopes.globals.scanForNewMethods();
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"07142704-6118-4548-9F3D-7D8D59269488"}
 */
function saveRulesOrder(event) {
	databaseManager.setAutoSave(false);
	for (var index = 1; index <= foundset.getSize(); index++) {
		var record = foundset.getRecord(index);
		record.execution_order = index;
	}
	databaseManager.setAutoSave(true);
	controller.sort('execution_order');
	_super.updateUI(event);
}
