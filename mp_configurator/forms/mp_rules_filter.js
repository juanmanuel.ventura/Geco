/**
 * @type {String}
 * @properties={typeid:35,uuid:"22F1FC4D-6C35-4B73-88DD-F44719AE1541"}
 */
var selectedEntity = 'mp_insured_users';

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"C6466FFB-6B9A-46D1-975B-21C6E9069023",variableType:4}
 */
var selectedKind = 0;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"B2F69B41-53F0-4B5B-9FA1-EFAB37D646A2"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"C95D85E2-8DA8-4E03-A033-2AF2BBB822E4"}
 */
function resetFilter(event, goTop) {
	selectedEntity = 'mp_insured_users';
	selectedKind = 0;
	_super.resetFilter(event, goTop)
	applyFilter(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"BAC2E545-1D59-49E6-9935-968734897659"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.working_entity = selectedEntity;
		foundset.rule_kind = selectedKind;
		foundset.search();
		foundset.sort('execution_order asc');
	}
}
