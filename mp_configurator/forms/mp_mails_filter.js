/**
 * @type {Number}
 * @properties={typeid:35,uuid:"C03138EA-3F2D-4540-A225-3C43EF5240FC",variableType:8}
 */
var mailType = null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"2B0FBD3F-E51F-4072-81F6-B7142D37638D"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"296FFF62-E572-457A-BDF2-9EB78B1B431A"}
 */
function resetFilter(event, goTop) {
	mailType = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"618A1B6B-A65E-471B-87EB-A028C196D30C"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.mail_type_id = mailType;
		foundset.search();
	}
}
