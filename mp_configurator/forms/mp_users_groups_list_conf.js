/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"6EEE49E8-2359-4B8D-B35E-B5794261700C"}
 */
function newRecord(event) {
	_super.newRecord(event);
	
	//SVUOTO I CAMPI nome e cognome
	forms.mp_users_groups_details_conf.surname = '';
	forms.mp_users_groups_details_conf.name = '';
	
	//FACCIO VEDERE I CAMPI NOME E COGNOME, NASCONDO REAL_NAME
	forms.mp_users_groups_details_conf.elements.lRealName.visible = false;
	forms.mp_users_groups_details_conf.elements.fRealName.visible = false;
	forms.mp_users_groups_details_conf.elements.lSurname.visible = true;
	forms.mp_users_groups_details_conf.elements.fSurname.visible = true;
	forms.mp_users_groups_details_conf.elements.lName.visible = true;
	forms.mp_users_groups_details_conf.elements.fName.visible = true;
	
	//RENDO ABILITATI I CAMPI FEP_CODEO E COMPANY_NAME
	forms.mp_users_groups_details_conf.elements.fFepCode.enabled = true;
	forms.mp_users_groups_details_conf.elements.fCompanyName.enabled = true;
	
	//NASCONDO UNA TENDINA E NE MOSTRO UN'ALTRA
	forms.mp_groups_for_user_list_conf.elements.user_approver_id.enabled = false;
	forms.mp_groups_for_user_list_conf.elements.user_approver_id.visible = false;
	forms.mp_groups_for_user_list_conf.elements.user_approver_id_copy.enabled = true;
	forms.mp_groups_for_user_list_conf.elements.user_approver_id_copy.visible = true;
}

/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"AF99624E-C16D-4D72-A85E-F6D5CC184A4A"}
 */
function updateUI(event){
	var rec = foundset.getSelectedRecord();
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle && bundle.elements['split']) {
		bundle.elements['split'].visible = !isEditing();
		if (rec && rec.isNew()) bundle['focus'] = 'NUOVO UTENTE';
		else if (rec && !rec.isNew())  bundle['focus'] = rec.real_name;
		bundle.elements['focus'].visible = isEditing();
	}
}
