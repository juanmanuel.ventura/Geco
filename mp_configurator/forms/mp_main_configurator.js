/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"F9E105A6-A716-40D4-AFEA-B520E7BB90D0"}
 */
function initialize(event) {
	if (!globals.grantAccess(['Admin'])) {
		logOut(event);
	}
	_super.initialize(event);
	loadPanel(event,'mp_rules_bundle');
}