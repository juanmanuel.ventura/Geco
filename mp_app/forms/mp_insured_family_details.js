/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"ADBE8F42-9743-436C-BA7D-A21DBC44B287"}
 */
function updateUI(event) {
	_super.updateUI(event);
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"B3745802-8DDE-435B-AC91-DA5D89D9AD86"}
 */
function onDataChange(oldValue, newValue, event) {
//	if (oldValue != newValue) {
//		forms.mp_insured_relative_list.updateUI(event);
//		if (newValue == 0) elements.tabRelatives.enabled = false;
//		else elements.tabRelatives.enabled = true;
//	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"798F27F6-6CBC-4A53-BD1A-1C28EEA53D84"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
	application.output(globals.mpMessageLog + 'START mp_insured_family_details.saveEdits()', LOGGINGLEVEL.INFO);
//	/** @type {Boolean} */
//	var hasPassed = false;
	/** @type {String} [] */
	var arrayRealNames = [];
//	var sumDataChanged = 0;
	var editedRecords = databaseManager.getEditedRecords(foundset.mp_insured_users_to_mp_insured_relatives);
	for (var index = 0; index < editedRecords.length; index++) {
		var ds = editedRecords[index].getChangedData();
		/** @type {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} */
		var recEditedRecord = editedRecords[index];
		// Get a dataset with outstanding changes on a record
		for (var i = 1; i <= ds.getMaxRowIndex(); i++) {
			if (ds.getValue(i, 1) == 'is_relative_enabled') {
				//se vecchio è diverso da nuovo e (vecchio è 0 e nuovo è 1)
//				if (ds.getValue(i, 2) != ds.getValue(i, 3) && (ds.getValue(i, 2) == 0 && ds.getValue(i, 3) == 1)) {
//					hasPassed = true;
//					sumDataChanged = sumDataChanged + 1;
//				}
				//se vecchio è diverso da nuovo e (vecchio è 1 e nuovo è 0)
				if (ds.getValue(i, 2) != ds.getValue(i, 3) && (ds.getValue(i, 2) == 1 && ds.getValue(i, 3) == 0)) {
//					sumDataChanged = sumDataChanged -1;
					/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_medical_dossiers>} */
					var medicalDossiers = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_medical_dossiers');
					if (medicalDossiers.find()) {
						medicalDossiers.user_insured_id = recEditedRecord.user_insured_id;
						medicalDossiers.relative_id = recEditedRecord.relative_id;
						medicalDossiers.dossier_status_id = [1, 2, 3, 4, 5, 6];
						medicalDossiers.is_closed = 0;
						var totMD = medicalDossiers.search();

						if (totMD > 0) arrayRealNames.push(recEditedRecord.real_name);
					}
				}
			}
		}
	}
	if (arrayRealNames.length != 0) {
		var realNames = '';
		for (var v = 0; v < arrayRealNames.length; v++) {
			if (v == 0) realNames = arrayRealNames[v];
			else realNames = realNames + ', ' + arrayRealNames[v];
		}
		application.output(globals.mpMessageLog + 'mp_insured_family_details.saveEdits() KO: Impossibile disabilitare i familiari:\n' + realNames + '\nCi sono pratiche in corso.', LOGGINGLEVEL.DEBUG);
		globals.DIALOGS.showErrorDialog('Error', 'Impossibile disabilitare i familiari:\n' + realNames + '\nCi sono pratiche in corso.', 'OK');
		return;
	}
//	if (foundset.mp_insured_users_to_mp_insured_relatives.getSize() > 0 && foundset.has_relative == 0) {
//		if (hasPassed == false && (foundset.mp_insured_users_to_mp_insured_relatives.relEnabled + sumDataChanged) > 0) {
//			application.output(globals.mpMessageLog + 'mp_insured_family_details.saveEdits() KO: La copertura è estesa a uno o più familiari.\nDisabilitare prima i familiari dall\'elenco, togliere il flag dalla copertura e quindi salvare.', LOGGINGLEVEL.DEBUG);
//			globals.DIALOGS.showErrorDialog('IMPOSSIBILE PROCEDERE COL SALVATAGGIO', 'La copertura è estesa a uno o più familiari.\nDisabilitare prima i familiari dall\'elenco, togliere il flag dalla copertura e quindi salvare.', 'OK');
//			return;
//		} else if (hasPassed) { //&& sumIsRelativeEnabled > 0){
//			application.output(globals.mpMessageLog + 'mp_insured_family_details.saveEdits() KO: La copertura è estesa a uno o più familiari.\nDisabilitare prima i familiari dall\'elenco, togliere il flag dalla copertura e quindi salvare.', LOGGINGLEVEL.DEBUG);
//			globals.DIALOGS.showErrorDialog('IMPOSSIBILE PROCEDERE COL SALVATAGGIO', 'La copertura è estesa a uno o più familiari.\nDisabilitare prima i familiari dall\'elenco, togliere il flag dalla copertura e quindi salvare.', 'OK');
//			return;
//		}
//	}
//	var insuredAddress = forms.mp_insured_details.address;
//	if (!globals.isEmpty(insuredAddress)) {
//		var result = insuredAddress.match(',');
//		if (result != -1) {
//			var comma = insuredAddress.indexOf(result[0]);
//			var _address = insuredAddress.slice(0, comma);
//			var civicNumber = insuredAddress.match(/\d+/);
//			if (civicNumber != undefined) {
//				var newAddress = _address + ',' + civicNumber[0];
//				forms.mp_insured_details.address = newAddress;
//				application.output("Indirizzo senza civico " + forms.mp_insured_details.address);
//			}
//		}
//	}
//	if (foundset.has_relative == 1 && (foundset.mp_insured_users_to_mp_insured_relatives.getSize() == 0 || (foundset.mp_insured_users_to_mp_insured_relatives.relEnabled + sumDataChanged) <= 0)) {
//		globals.DIALOGS.showErrorDialog('Error','Hai esteso la copertura assicurativa. Aggiungi almeno un familiare.', 'OK');
//		return;
//	}
	application.output(globals.mpMessageLog + 'STOP mp_insured_family_details.saveEdits()', LOGGINGLEVEL.INFO);
	_super.saveEdits(event);
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"05F59DE5-8E22-4988-B677-875D41B609A6"}
 * @AllowToRunInFind
 */
function onShow(firstShow, event) {
	//setto la variabile a 0 --> 0 = foundset di insured_details
	scopes.globals.foundsetForm = 0;
	elements.buttonEdit.enabled = foundset.getSize() == 0 ? false : true;
	//elements.buttonEdit.visible = foundset.getSize() == 0 ? false : true;
	_super.onShow(firstShow, event);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"894105C3-CA07-4540-955A-79E1F16621AF"}
 */
function startEditing(event) {
	_super.startEditing(event);
	forms.mp_insured_relative_list.updateUI(event);
	//elements.tabRelatives.enabled = foundset.has_relative == 0 ? false : true;
}
