dataSource:"db:/pratiche_sanitarie/mp_medical_dossiers",
extendsID:"7CAA06F6-FD01-473C-BE66-F0371B9EF4E0",
initialSort:null,
items:[
{
anchors:13,
dataProviderID:"mp_medical_dossiers_to_mp_media_has_note.media",
formIndex:22,
location:"1023,48",
name:"hasNote",
showClick:false,
showFocus:false,
size:"16,16",
typeid:7,
uuid:"0178BB22-940C-4990-B0F2-A354F02C5523"
},
{
anchors:13,
formIndex:13,
horizontalAlignment:2,
location:"517,7",
name:"label_stato",
onActionMethodID:"2D2BE307-70E0-4984-9DE4-8647192EBC98",
showClick:false,
showFocus:false,
size:"134,20",
text:"Stato",
transparent:true,
typeid:7,
uuid:"113E33C4-21DE-4AA8-8FB8-2371B51A9678"
},
{
anchors:13,
formIndex:15,
horizontalAlignment:2,
location:"657,7",
name:"label_datacreazione",
onActionMethodID:"2D2BE307-70E0-4984-9DE4-8647192EBC98",
showClick:false,
showFocus:false,
size:"159,20",
text:"Data creazione",
transparent:true,
typeid:7,
uuid:"23CAB43B-1B04-4161-A301-334ABA1E04FB"
},
{
fontType:"Calibri,1,15",
formIndex:23,
location:"87,73",
name:"lCount",
size:"113,20",
text:"Numero Pratiche: ",
transparent:true,
typeid:7,
uuid:"27DD5F6C-654C-4C4F-8D30-F1E673BF894B"
},
{
anchors:13,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"dossier_beneficiary_name",
formIndex:12,
horizontalAlignment:2,
location:"350,44",
name:"insured_user_relative",
size:"157,20",
transparent:true,
typeid:7,
uuid:"2852DDB1-5CEA-4183-BC5D-953AB75925EB"
},
{
anchors:13,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"created_at",
displayType:5,
editable:false,
enabled:false,
formIndex:16,
format:"dd-MM-yyyy",
horizontalAlignment:2,
location:"657,44",
size:"159,20",
transparent:true,
typeid:4,
uuid:"425D8A17-327C-405D-A858-C5AA6FBAD8DB"
},
{
anchors:13,
formIndex:17,
horizontalAlignment:2,
location:"830,7",
name:"label_ultimoaggiornamento",
onActionMethodID:"2D2BE307-70E0-4984-9DE4-8647192EBC98",
showClick:false,
showFocus:false,
size:"152,20",
text:"Ultimo aggiornamento",
transparent:true,
typeid:7,
uuid:"4FCF9BC6-5668-440C-94E0-6F76D5586C39"
},
{
anchors:13,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"dossier_status_id",
editable:false,
formIndex:14,
horizontalAlignment:2,
location:"517,44",
name:"dossier_status",
size:"134,20",
transparent:true,
typeid:4,
uuid:"55D8455C-4DF4-4B05-8938-E98DDAA67A4D",
valuelistID:"ED1765C8-B87C-4E2E-9738-E72F4ACAA87A"
},
{
anchors:13,
formIndex:19,
horizontalAlignment:2,
location:"168,7",
name:"label_assicurato",
onActionMethodID:"2D2BE307-70E0-4984-9DE4-8647192EBC98",
showClick:false,
showFocus:false,
size:"172,20",
text:"Assicurato",
transparent:true,
typeid:7,
uuid:"59E601F6-13BF-4AC1-9325-DA28C42F72E7"
},
{
anchors:13,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"mp_medical_dossiers_to_mp_insured_users.real_name",
editable:false,
formIndex:20,
horizontalAlignment:2,
location:"168,44",
name:"insured_user",
size:"172,20",
transparent:true,
typeid:4,
uuid:"5A410AC0-D5C8-488C-82A3-24B4CFC98EF8"
},
{
anchors:13,
dataProviderID:"mp_medical_dossier_to_mp_media_has_documents.media",
formIndex:21,
location:"998,48",
name:"hasAttachment",
showClick:false,
showFocus:false,
size:"16,16",
typeid:7,
uuid:"5E6DEC6C-E879-45A0-BEDD-9C2CE6350158"
},
{
anchors:13,
formIndex:11,
horizontalAlignment:2,
location:"350,7",
name:"label_assistito",
onActionMethodID:"2D2BE307-70E0-4984-9DE4-8647192EBC98",
showClick:false,
showFocus:false,
size:"157,20",
text:"Beneficiario rimborso",
transparent:true,
typeid:7,
uuid:"960EC00C-1995-4D25-8A9C-215027904354"
},
{
enabled:false,
extendsID:"19DC9D86-1F2B-45D6-B6CC-E6EC75E0DCDF",
imageMediaID:"C4635651-169C-4282-8A83-537109BBB4A1",
rolloverImageMediaID:"236D7DC9-DA14-49D2-ACFF-ABDBED8319B5",
typeid:7,
uuid:"97775C75-D767-4B1B-9317-142C629CBA60",
visible:false
},
{
enabled:false,
extendsID:"3A451108-5E01-4404-864F-DB21E6D1FB17",
imageMediaID:"A7414DF4-7BC9-40C8-8800-F8366871E048",
rolloverImageMediaID:"C390B8BC-0DB1-41CF-B762-539AA36DC768",
typeid:7,
uuid:"A1C2E4EF-0AC7-4B60-9004-DD296019A94F",
visible:false
},
{
fontType:"Calibri,1,15",
formIndex:23,
labelFor:"",
location:"200,73",
name:"lCountPractices",
size:"37,20",
transparent:true,
typeid:7,
uuid:"A31C8496-8502-417E-A7BD-9504DB891E10"
},
{
extendsID:"70EFC6DA-3A9C-471E-A628-F59E1702F4D1",
height:95,
typeid:19,
uuid:"BCDDF1B3-3030-4396-827B-9F41F997A3B1"
},
{
anchors:13,
extendsID:"5FCDACC5-18DB-492C-ABE7-B223C63E0E03",
location:"11,10",
size:"26,20",
typeid:7,
uuid:"C3F64CBF-5604-42CF-ABF7-B9D7127D0350",
visible:false
},
{
anchors:12,
customProperties:"",
formIndex:2,
imageMediaID:"45BB0F9B-02DF-43E2-BE8C-E2E6011569C0",
location:"15,43",
name:"button_show",
onActionMethodID:"D0B9BD54-1E44-41E9-95EB-3D1AFEBE8418",
rolloverCursor:12,
rolloverImageMediaID:"2E8FDED5-3D5C-4770-B544-185BB752B24E",
showClick:false,
size:"22,22",
toolTipText:"vai a",
transparent:true,
typeid:7,
uuid:"C712A750-71BC-48CB-91F7-4B360400DF28"
},
{
anchors:13,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"mp_medical_dossiers_to_mp_insured_users.mp_insured_users_to_mp_insurance_policies.contractor",
editable:false,
formIndex:10,
horizontalAlignment:2,
location:"47,44",
name:"contractor",
size:"95,20",
transparent:true,
typeid:4,
uuid:"D9EB55FE-7819-4837-A1DD-908A41F08CEA"
},
{
anchors:13,
formIndex:9,
horizontalAlignment:2,
location:"47,7",
name:"label_contraente",
onActionMethodID:"2D2BE307-70E0-4984-9DE4-8647192EBC98",
showClick:false,
showFocus:false,
size:"95,20",
text:"Contraente",
transparent:true,
typeid:7,
uuid:"DDCEB5FF-C59D-4E9C-8FE3-1CBC29936122"
},
{
anchors:13,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"modified_at",
displayType:5,
editable:false,
enabled:false,
formIndex:18,
format:"dd-MM-yyyy",
horizontalAlignment:2,
location:"831,44",
size:"152,20",
transparent:true,
typeid:4,
uuid:"F8381FFC-0597-41D4-815A-02EC0EF2F8ED"
}
],
name:"mp_dossier_list",
onLoadMethodID:"-1",
onShowMethodID:"6F19F61B-D623-4789-8F3E-280CBB59789D",
scrollbars:0,
size:"1049,100",
typeid:3,
uuid:"35663D6A-488B-4BC9-9A7B-96B1626CA538"