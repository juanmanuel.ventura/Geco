/**
 * @type {plugins.file.JSFile}
 *
 * @properties={typeid:35,uuid:"F1AFA962-5717-4285-BB0B-40E40ECFF0E4",variableType:-4}
 */
var mpVJsFile = null;
/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9CFBFB6E-397B-43AB-8D00-7AAD4FC6A964"}
 */
var documentPath = "";
/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"4FA0C450-7B28-4D5B-A661-9D34E0CAEFCA"}
 */
var docTitle = "";
/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"969F4B67-2243-4749-BA04-B74BE549E2CA",variableType:8}
 */
var idx = null;
/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"DBA5322E-803A-48CB-9D4C-9A1A1736FDBB"}
 */
function mpDownloadFile(event) {
	application.output(globals.mpMessageLog + 'START mp_informative_documents.mpDownloadFile() ', LOGGINGLEVEL.INFO);
	if (!foundset.getSelectedRecord().isNew()) {
		var index = foundset.getSelectedIndex();
		var filePath = globals.mpServerDir + "/" + foundset.getRecord(index).document_name;
		//application.output("Percorso del file sul filesystem : "+filePath);
		var file = plugins.file.convertToJSFile(filePath);
		if (file.exists()) {
			var fileToDownload = new Array();
			fileToDownload = file.getBytes();
			if (!plugins.file.writeFile(foundset.document_name, fileToDownload)) {
				application.output('Errore scrittura file' + filePath + 'STOP mp_informative_documents.mpDownloadFile() ', LOGGINGLEVEL.INFO);
			}
		} else {
			globals.DIALOGS.showErrorDialog('Errore download', 'Il file non esiste più. Contattare l\'Amministratore.', 'chiudi');
		}
	}
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"A74B7D3E-816D-4984-B44C-D53D537FE57C"}
 * )
 */
function uploadDocument(event) {

	application.output(globals.mpMessageLog + 'START mp_informative_documents.mpDownloadFile() ', LOGGINGLEVEL.INFO);
	var filesFilter = new Array('pdf', 'doc', 'docx', 'xls', 'jpeg', 'jpg', 'txt');
	/** @type {plugins.file.JSFile} **/
	mpVJsFile = plugins.file.showFileOpenDialog(1, null, false, filesFilter, uploadCallBack, 'Carica documenti informativi');

	if (mpVJsFile != null) {
		application.output('file ' + mpVJsFile.getAbsolutePath());
		documentPath = mpVJsFile.getAbsolutePath();
		application.output(mpVJsFile.getName());
	} else {
		documentPath = null;
	}
	application.output(globals.mpMessageLog + 'STOP mp_informative_documents.mpDownloadFile() ', LOGGINGLEVEL.INFO);
}
/**
 * @param {plugins.file.JSFile[]} fileToUpload
 *
 * @properties={typeid:24,uuid:"1F873EEC-14F7-4661-A14E-59501943DFCC"}
 */
function uploadCallBack(fileToUpload) {

	application.output(globals.mpMessageLog + 'START mp_informative_documents.uploadCallBack() ', LOGGINGLEVEL.INFO);
	globals.mpServerDir = plugins.file.convertToJSFile("GeCo_pratiche_sanitarie_documenti_utili");
	/** @type {plugins.file.JSFile} **/
	var myUploadedFile = fileToUpload[0];
	var uploadedFileContentType = myUploadedFile.getContentType();
	var isIllegalContentType = true;
	//10 MB in Bytes
	var fileWeightLimit = 10485760;
	
	if (myUploadedFile.size() > fileWeightLimit) {
		globals.DIALOGS.showWarningDialog('Avviso Caricamento file', 'Puoi caricare file di dimensioni massime di 10 MB', 'chiudi');
		return;
	}
	var filesFilter = new Array('application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'text/plain', 'application/vnd.ms-excel', 'image/jpeg', 'image/jpg', 'image/png');
	for (var index = 0; index < filesFilter.length; index++) {
		var ext = filesFilter[index];
		if (uploadedFileContentType == ext) {
			isIllegalContentType = false;
		}
	}
	if (isIllegalContentType) {
		globals.DIALOGS.showWarningDialog('Estensione file non valida', 'Puoi caricare solo file con estensione pdf,doc,docx,xls,jpeg,jpg,png,txt', 'chiudi');
		return;
	}
	var vJexistingfile = plugins.file.convertToJSFile(globals.mpServerDir + '/' + foundset.getSelectedRecord().document_name);
	//application.output("Esiste il file : "+vJexistingfile.exists());
	if (vJexistingfile.exists() && foundset.getSelectedRecord().document_name != null) {
		vJexistingfile.deleteFile();
	}
	application.output(globals.mpMessageLog + 'dir path ' + globals.mpServerDir.getAbsolutePath(), LOGGINGLEVEL.DEBUG);
	var resultDir = plugins.file.createFolder(globals.mpServerDir);
	docTitle = myUploadedFile.getName();
	//application.output(globals.mpServerDir+'/'+ docTitle);
	application.output(globals.mpMessageLog + 'creata dir ' + resultDir, LOGGINGLEVEL.DEBUG);
	//application.output(globals.mpMessageLog + 'dir globale ' + globals.mpServerDir, LOGGINGLEVEL.DEBUG);
	globals.mpServerfile = plugins.file.createFile(globals.mpServerDir + '/' + docTitle);
	if (!globals.mpServerfile.exists()) {
		application.output(globals.mpMessageLog + 'file server ' + globals.mpServerfile, LOGGINGLEVEL.DEBUG);
		//verifico che non esista il file sul server
		var result = plugins.file.writeFile(globals.mpServerfile, myUploadedFile.getBytes());
		application.output(globals.mpMessageLog + 'file scritto in locale ' + result, LOGGINGLEVEL.DEBUG);
		if (result) {
			application.output(globals.mpServerDir.getAbsolutePath());
			documentPath = globals.mpServerDir.getAbsolutePath();
			//application.output(globals.mpMessageLog + 'dir path ' + documentPath, LOGGINGLEVEL.DEBUG)
			//scrivo il percorso del file sul DB
			foundset.document_name = docTitle;
		}
	} else {
		globals.DIALOGS.showInfoDialog('Avviso file', 'Il file è già caricato sul server', 'chiudi');
	}
	application.output(globals.mpMessageLog + 'STOP mp_informative_documents.uploadCallBack() ', LOGGINGLEVEL.INFO);
}
/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"D39B7079-6BF9-4F0B-ACF9-F0BBE10F1F03"}
 */
function createNewRecord(event) {

	application.output(globals.mpMessageLog + 'START mp_informative_documents.createNewRecord() ', LOGGINGLEVEL.INFO);
	idx = _super.newRecord(event);
	updateUI(event);
	application.output(globals.mpMessageLog + 'STOP mp_informative_documents.createNewRecord() ', LOGGINGLEVEL.INFO);
}
/**
 * @param{Number} index
 *
 * @properties={typeid:24,uuid:"CCC5011C-5DA3-4AB0-9F68-6EE0DBD6A707"}
 */
function deleteRelatedFile(index) {
	var fileName = globals.mpServerDir + '/' + foundset.getRecord(index).document_name;
	application.output("percorso del file da cancellare " + fileName);
	application.output(globals.mpMessageLog + 'START mp_informative_documents.deleteRelatedFile() ', LOGGINGLEVEL.INFO);
	if (plugins.file.deleteFile(fileName)) {
		application.output("file cancellato correttamente ");
	}
	application.output(globals.mpMessageLog + 'STOP mp_informative_documents.deleteRelatedFile() ', LOGGINGLEVEL.INFO);
}
/**
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"17B7099E-B36C-41FF-85A3-F3A71B4193C9"}
 */
function onShow(firstShow, event) {
	updateUI(event);
}
/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"F60C7DBA-9F45-40A6-B951-A4CC9906E0F2"}
 */
function onLoad(event) {
	//	//inizializzo la cartella dove sono presenti i documenti da scaricare
	globals.mpServerDir = plugins.file.convertToJSFile("GeCo_pratiche_sanitarie_documenti_utili");
}
/**@param {JSEvent} event
 * @private
 * @properties={typeid:24,uuid:"3E45A14C-5292-459D-A085-72E72CC41264"}
 */
function updateUI(event) {

	if (globals.isInsured) {
		elements.buttonDelete.visible = false;
		elements.buttonAdd.visible = false;
		elements.buttonEdit.visible = false;
		elements.buttonSave.visible = false;
		elements.buttonCancel.visible = false;
		elements.buttonUpload.visible = false;
		elements.documentLabel.enabled = false;
		elements.buttonDownload.visible = true;
		elements.buttonDownload.enabled = true;

	}

	if (globals.isHrAdmin || globals.isInsurer) {
		elements.buttonDelete.visible = false;
		elements.buttonAdd.visible = true;
		elements.buttonEdit.visible = true;
		elements.buttonUpload.visible = true;
		elements.buttonDownload.visible = true;
		elements.buttonDownload.enabled = true;

		if (isEditing()) {
			elements.documentLabel.enabled = true;
			elements.buttonUpload.enabled = true;
			elements.buttonAdd.enabled = true;
			elements.buttonEdit.visible = false;
			elements.buttonSave.visible = true;
			elements.buttonCancel.visible = true;
		} else {
			elements.documentLabel.enabled = false;
			elements.buttonUpload.enabled = false;
			elements.buttonAdd.enabled = false;
			elements.buttonEdit.visible = true;
			elements.buttonSave.visible = false;
			elements.buttonCancel.visible = false;
		}
	}
	if (globals.isAdmin) {
		elements.buttonDelete.visible = true;
		elements.buttonAdd.visible = true;
		elements.buttonUpload.visible = true;
		elements.buttonDownload.visible = true;
		elements.buttonDownload.enabled = true;

		if (isEditing()) {
			elements.documentLabel.enabled = true;
			elements.buttonUpload.enabled = true;
			elements.buttonAdd.enabled = true;
			elements.buttonDelete.enabled = false;
			elements.buttonEdit.visible = false;
			elements.buttonSave.visible = true;
			elements.buttonCancel.visible = true;
		} else {
			elements.documentLabel.enabled = false;
			elements.buttonUpload.enabled = false;
			elements.buttonAdd.enabled = false;
			elements.buttonEdit.visible = true;
			elements.buttonSave.visible = false;
			elements.buttonCancel.visible = false;
			if (foundset.getSize() > 0) {
				elements.buttonDelete.enabled = true;
			} else {
				elements.buttonDelete.enabled = false;
			}
		}
	}
	elements.line.visible = foundset.getSize()>0?true:false;
	elements.documentLabel.transparent = !isEditing();
	elements.lDocName.enabled = !isEditing();
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"CE4846E2-8B7E-4C0B-A518-364C3795BC79"}
 */
function deleteRecord(event) {
	//Se c'è  almeno un record cancello
	if (foundset.getSize() > 0) {
		var index = foundset.getSelectedIndex();
		deleteRelatedFile(index);
		var answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai per eliminare un record.\nQuesta operazione non può essere annullata", "Elimina", "Annulla");
		if (answer == "Elimina") {
			foundset.deleteRecord(index);
		}
	}
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"62C0DEE8-B680-4C34-B101-F0A485A203C2"}
 */
function saveEdits(event) {
	_super.saveEdits(event);
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"A2C1A258-F95B-41C3-9B22-B4F22FBFD0F0"}
 */
function stopEditing(event) {
	if (foundset.getSelectedRecord().isNew()) {
		var index = foundset.getSelectedIndex();
		if (foundset.getRecord(index).document_name != null) {
			deleteRelatedFile(index);
		}
	}
	_super.stopEditing(event);
}
