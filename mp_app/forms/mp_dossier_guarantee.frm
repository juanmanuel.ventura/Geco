dataSource:"db:/pratiche_sanitarie/mp_medical_dossiers",
extendsID:"66EC01AD-29A3-44BB-91B3-948D80DD8A6B",
items:[
{
anchors:3,
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.lens",
displayType:4,
groupID:"guarantee",
location:"878,173",
size:"335,20",
text:"Lenti",
transparent:true,
typeid:4,
uuid:"1410BFBF-804C-45FE-A822-9E30517C1D4C"
},
{
anchors:3,
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.benefit_replacement",
displayType:4,
groupID:"guarantee",
location:"878,214",
size:"335,20",
text:"Indennità sostitutiva",
transparent:true,
typeid:4,
uuid:"1FD5B35B-A0FE-4BDC-A749-0222634B5170"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.health_service_sight",
displayType:4,
groupID:"guarantee",
location:"528,91",
size:"335,20",
text:"Intervento per eliminazione/correzione difetti vista",
transparent:true,
typeid:4,
uuid:"2F19E6CB-4FC5-4C60-B34B-5E8CACAC0B1C"
},
{
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.helper_transport_body",
displayType:4,
groupID:"guarantee",
location:"528,255",
size:"335,20",
text:"Rimpatrio salma",
transparent:true,
typeid:4,
uuid:"337F0273-804D-40BC-A6E5-75060C016539"
},
{
anchors:3,
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.advance",
displayType:4,
groupID:"guarantee",
location:"878,255",
size:"335,20",
text:"Anticipo",
transparent:true,
typeid:4,
uuid:"37FB2608-C170-4CB1-A7C6-11724CA3EDA2"
},
{
anchors:3,
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.extra_hospital_terapy",
displayType:4,
groupID:"guarantee",
location:"878,9",
selectOnEnter:true,
size:"335,20",
text:"Prestazione diagnostica e terapeudica extra-ospedaliera",
transparent:true,
typeid:4,
uuid:"5AD6F50F-C7D3-4AD0-B5D3-F714420AF6B7"
},
{
anchors:3,
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.extra_hospital_prosthesis",
displayType:4,
groupID:"guarantee",
location:"878,50",
size:"335,20",
text:"Protesi ortopediche o apparecchi acustici",
transparent:true,
typeid:4,
uuid:"6E458FC3-D29A-4069-8A57-06130FB7F066"
},
{
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.health_service_no_operation",
displayType:4,
groupID:"guarantee",
location:"528,50",
size:"335,20",
text:"Ospedaliea con intervento chirurgico",
transparent:true,
typeid:4,
uuid:"82B91792-7CA3-400F-9A4D-6C3765DC05F3"
},
{
height:443,
partType:5,
typeid:19,
uuid:"880F1575-6443-4B8F-BF78-642A53BB1503"
},
{
anchors:3,
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.medical_exams",
displayType:4,
groupID:"guarantee",
location:"878,91",
size:"335,20",
text:"Visite specialistiche,analisi ed sami diagnostici e di laboratorio",
transparent:true,
typeid:4,
uuid:"915CBBBF-0456-4D58-BD15-1FF70B312EE3"
},
{
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.health_service_with_operation",
displayType:4,
groupID:"guarantee",
location:"528,9",
name:"health_service_no_operation",
onActionMethodID:"-1",
size:"335,20",
text:"Ospedaliera senza intervento chirurgico",
transparent:true,
typeid:4,
uuid:"93D433AE-5726-4511-BEC7-4641D0037670"
},
{
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.helper_transport_board",
displayType:4,
groupID:"guarantee",
location:"528,132",
size:"335,20",
text:"Accompagnatore/Trasporto: vitto e pernottamento",
transparent:true,
typeid:4,
uuid:"A14B1DC5-88D2-4911-BAC8-3C20477B083E"
},
{
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.helper_transport_abroad",
displayType:4,
groupID:"guarantee",
location:"528,214",
size:"335,20",
text:"Accompagnatore/Trasporto in aereo di linea",
transparent:true,
typeid:4,
uuid:"A2B5D1A1-69EE-4A50-A6B0-57A9957C1C22"
},
{
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.helper_transport_ambulance",
displayType:4,
groupID:"guarantee",
location:"528,173",
size:"335,20",
text:"Accompagnatore/Trasporto in ambulanza(in Italia)",
transparent:true,
typeid:4,
uuid:"AC0889D2-04E1-4E04-9749-32F74F465191"
},
{
anchors:3,
dataProviderID:"mp_medical_dossiers_to_mp_guarantees_required.dental_care",
displayType:4,
groupID:"guarantee",
location:"878,132",
size:"335,20",
text:"Cure Dentarie",
transparent:true,
typeid:4,
uuid:"C6DCD3BD-8397-4BC5-8E15-0BE92DA2CFBA"
},
{
items:[
{
containsFormID:"C75A3483-8BBE-4567-B0C7-DDF5B92DB4FE",
location:"147,81",
text:"mp_dossier_insured_relatives",
typeid:15,
uuid:"51D04DF7-7211-40AB-9741-C3922D084A1F"
}
],
location:"5,5",
name:"tabless",
printable:false,
size:"498,393",
tabOrientation:-1,
transparent:true,
typeid:16,
uuid:"E338FFF8-4A7C-4E67-8113-894ACEB1AD8F"
}
],
name:"mp_dossier_guarantee",
scrollbars:4,
showInMenu:true,
size:"1219,480",
styleName:"GeCo",
typeid:3,
uuid:"E8CA70B3-5F7E-4DFD-A5E8-BC03554C65D0"