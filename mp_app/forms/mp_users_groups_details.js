/**
 * @properties={typeid:35,uuid:"99F9D376-D447-43ED-82A7-791ECEC871B4",variableType:-4}
 */
var pwdChanged = false;

/**@type {String}
 * @properties={typeid:35,uuid:"DF3330B6-23EF-4B0F-BF11-650F595D5DD8"}
 */
var actualInsurerEmail = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"E432059A-AFF0-4210-80ED-A8F54B89C0E2"}
 */
var actualInsurerName = null;

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"CAB0E11A-A3F0-49E1-A7DA-85DB19F988AC",variableType:-4}
 */
var actualInsurer = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"BC099AC8-9BE0-420E-A00A-7D69989B5D36",variableType:8}
 */
var startEdit = 0;

/**
 * @properties={typeid:35,uuid:"F6BD7782-2B70-4763-9E2C-618730FD8F1E",variableType:-4}
 */
var wasInsured = false;

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"91818E07-FD5F-48EE-A1EF-1792A35A081E"}
 */
function updateUI(event) {
	_super.updateUI(event);
	elements.fUsername.enabled = (isEditing() && foundset.company_name != 'SPINDOX') || (isEditing() && foundset.company_name == 'SPINDOX' && foundset.getSelectedRecord().isNew());
	elements.fCompanyName.enabled = isEditing() && foundset.getSelectedRecord().isNew();
	elements.fEmail.enabled = (isEditing() && foundset.company_name != 'SPINDOX') || (isEditing() && foundset.company_name == 'SPINDOX' && foundset.getSelectedRecord().isNew()); // && foundset.user_email == null;
	elements.fFepCode.enabled = isEditing() && foundset.company_name == 'SPINDOX' && foundset.getSelectedRecord().isNew();
	elements.fRealName.enabled = (isEditing() && foundset.company_name != 'SPINDOX') || (isEditing() && foundset.company_name == 'SPINDOX' && foundset.getSelectedRecord().isNew());
	elements.lDescRealName.visible = isEditing();
	elements.lDescUsername.visible = isEditing();
	elements.fPwd.enabled = isEditing() && !foundset.getSelectedRecord().isNew() && foundset.company_name != 'SPINDOX';
	elements.lDescPwd.visible = isEditing() && foundset.getSelectedRecord().isNew();
	elements.lDescPwd2.visible = isEditing() && !foundset.getSelectedRecord().isNew() && foundset.company_name == 'SPINDOX';
	if (isEditing()) {
		elements.fPwd.bgcolor = !foundset.getSelectedRecord().isNew() && foundset.company_name == 'GENERALI' ? '#ffffff' : '#dbdbdb';
		elements.fFepCode.bgcolor = foundset.company_name == 'SPINDOX' ? '#ffffff' : '#dbdbdb';
	} else {
		elements.fPwd.bgcolor = '#ffffff';
		elements.fFepCode.bgcolor = '#ffffff';
	}
	//LOGICA TENDINA RUOLI
//	forms.mp_groups_for_user_list.elements.role.enabled = !isEditing();
//	forms.mp_groups_for_user_list.elements.role.visible = !isEditing();
//	forms.mp_groups_for_user_list.elements.roleExceptInsured.enabled = isEditing();
//	forms.mp_groups_for_user_list.elements.roleExceptInsured.visible = isEditing();
	forms.mp_groups_for_user_list.elements.role.enabled = true;
	forms.mp_groups_for_user_list.elements.role.visible = true;
	forms.mp_groups_for_user_list.elements.roleExceptInsured.enabled = false
	forms.mp_groups_for_user_list.elements.roleExceptInsured.visible = false;
}

/**
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"40B6498A-0679-440A-A9C5-CD4164E96BDB"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
	/** @type {Boolean} */
	var foundInsuredAfterEdit = false;
	var rec = foundset.getSelectedRecord();
	var totUsers = 0;
	application.output(globals.mpMessageLog + 'START mp_users_groups_details.saveEdits() rec: ' + rec, LOGGINGLEVEL.INFO);
	//tutti i campi sono obbligatori
	if (scopes.globals.mpIsEmpty(rec.user_name) || scopes.globals.mpIsEmpty(rec.user_email) || scopes.globals.mpIsEmpty(rec.real_name) || scopes.globals.mpIsEmpty(rec.company_name)) {
		scopes.globals.DIALOGS.showErrorDialog('Error', '- Inserire tutti i campi obbligatori', 'OK');
		return;
	}
	//controllo esistenza username
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
	var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
	if (users.find()) {
		users.user_id = '!' + rec.user_id;
		users.user_name = rec.user_name;
		totUsers = users.search();
		if (totUsers >= 1) {
			application.output(globals.mpMessageLog + 'mp_users_groups_details.saveEdits() KO: - L\'utente che si vuole creare esiste già, modificare i dati', LOGGINGLEVEL.DEBUG);
			scopes.globals.DIALOGS.showErrorDialog('Error', '- L\'utente che si vuole creare esiste già, modificare i dati', 'OK');
			return;
		}
	}
	//controllo se c'era ruolo Assicurato PRIMA e DOPO la modifica
	if (wasInsured == true) {
		application.output('rec.mp_users_to_mp_users_roles.getSize() ' + rec.mp_users_to_mp_users_roles.getSize());
		for (var v = 1; v <= rec.mp_users_to_mp_users_roles.getSize(); v++) {
			var recV = rec.mp_users_to_mp_users_roles.getRecord(v);
			application.output('iterazione ' + v + '; recV: ' + recV.mp_users_roles_to_mp_roles.description);
			if (recV.role_id == 3) {
				foundInsuredAfterEdit = true;
				break;
			}
		}
		if (foundInsuredAfterEdit) {
			globals.DIALOGS.showErrorDialog('ERRORE','- Non puoi togliere il ruolo "Assicurato" all\'utente ' + rec.real_name,'OK');
			return;
		}
	}
	//controllo che non venga inserito ruolo ASSICURATO (3)
	if (wasInsured == false) {
		for (var z = 1; z <= rec.mp_users_to_mp_users_roles.getSize(); z++) {
			var recZ = rec.mp_users_to_mp_users_roles.getRecord(z);
			if (recZ.role_id == 3) {
				globals.DIALOGS.showErrorDialog('ERRORE', '- Non puoi assegnare il ruolo Assicurato da Pratiche Sanitarie', 'OK');
				return;
			}
		}
	}
	//controllo se azienda è SPINDOX
	if (rec.company_name == 'SPINDOX') {
		if (scopes.globals.mpIsEmpty(rec.fep_code)) {
			application.output(globals.mpMessageLog + 'mp_users_groups_details.saveEdits() KO: - Inserire il codice fep per l\'utente SPINDOX', LOGGINGLEVEL.DEBUG);
			scopes.globals.DIALOGS.showErrorDialog('Error', '- Inserire il codice fep per l\'utente dell\'azienda SPINDOX', 'OK');
			return;
		}//azienda c'è e c'è fep code, controllo che sia univoco
		else {
			users.loadAllRecords();
			if (users.find()) {
				users.user_id = '!' + rec.user_id;
				users.fep_code = rec.fep_code;
				totUsers = users.search();

				if (totUsers >= 1) {
					application.output(globals.mpMessageLog + 'mp_users_groups_details.saveEdits() KO: - Il codice fep inserito per l\'utente SPINDOX è già presente; inserirne un altro', LOGGINGLEVEL.DEBUG);
					scopes.globals.DIALOGS.showErrorDialog('Error', '- Il codice fep inserito per l\'utente SPINDOX è già presente; inserirne un altro', 'OK');
					return;
				} else {
					if (rec.isNew()) foundset.user_password = globals.mpSha1('spindox');
					if (pwdChanged) foundset.user_password = globals.mpSha1(rec.user_password);
				}
			}
		}
		//CONTROLLO CHE NON SIA STATO MESSO RUOLO INSURER (2)
		application.output('rec.mp_users_to_mp_users_roles.getSize() ' + rec.mp_users_to_mp_users_roles.getSize());
		for (var i = 1; i <= rec.mp_users_to_mp_users_roles.getSize(); i++) {
			var recI = rec.mp_users_to_mp_users_roles.getRecord(i);
			application.output('iterazione ' + i + '; recI: ' + recI.mp_users_roles_to_mp_roles.description);
			if (recI.role_id == 2) {
				globals.DIALOGS.showErrorDialog('ERRORE', '- L\'utente dell\'azienda SPINDOX non può essere un Assicuratore.', 'OK');
				return;
			}
		}
	} else if (rec.company_name == 'GENERALI') {
		if (rec.isNew()) foundset.user_password = globals.mpSha1('generali');
		if (pwdChanged) foundset.user_password = globals.mpSha1(rec.user_password);
	}
	foundset.real_name = rec.real_name.toUpperCase();
	foundset.user_name = rec.user_name.toLowerCase();
	foundset.user_email = rec.user_email.toLowerCase();
	//Se un utente non è più assicuratore elimino la sua email dalla lista delle email degli assicuratori
	if (actualInsurer == true && foundset.user_email != null) {
		application.output("Cambio ruoli all\'utente : " + rec.real_name + "era assicuratore ? " + actualInsurer);
		//controllo i record editati che l'utente sia ancora assicuratore
		var editedRoles = databaseManager.getEditedRecords(foundset.mp_users_to_mp_users_roles);
		if (editedRoles.length > 0) {
			for (var index = 0; index < editedRoles.length; index++) {
				var ds = editedRoles[index].getChangedData();
				for (var n = 1; n <= ds.getMaxRowIndex(); n++) {
					//se era assicuratore e adesso non più
					if (ds.getValue(n, 1) == 'role_id' && ds.getValue(n, 2) == 2 && ds.getValue(n, 3) != 2) {
						forms.mp_groups_for_user_list.deleteInsurerEmail(rec.user_email);
					}
				}
			}
		}
	}
	//Utente a cui un ruolo è cambiato ad assicuratore
	editedRoles = databaseManager.getEditedRecords(foundset.mp_users_to_mp_users_roles);
	if (editedRoles.length > 0) {
		for (index = 0; index < editedRoles.length; index++) {
			ds = editedRoles[index].getChangedData();
			for (i = 1; i <= ds.getMaxRowIndex(); i++) {
				if (ds.getValue(i, 1) == 'role_id' && ds.getValue(i, 2) != 2 && ds.getValue(i, 3) == 2) {
					application.output("ora l\'utente " + rec.user_name + " è diventato assicuratore");
					setInsurerEmail(rec.user_email);
				}
			}
		}
	}
	//Sostuisco la email di un assicuratore
	if (actualInsurer == true && actualInsurerEmail != rec.user_email && rec.user_email != null && !foundset.mp_users_to_mp_users_roles.getSelectedRecord().isNew()) {
		var choise = globals.DIALOGS.showInfoDialog('Cambio mail assicuratore ', 'Stai per sostituire l\'email dell\'assicuratore', 'prosegui', 'annulla');
		if (choise == 'prosegui') {
			application.output("AGGIORNO EMAIL DI " + rec.user_name);
			//aggiorno la mail
			changeInsurerEmail(actualInsurerEmail, rec.user_email);
		}
		if (choise == 'annulla') {
			databaseManager.revertEditedRecords(foundset);
		}
	}
	//Se aggiungo ad un utente il ruolo assicuratore
	if (foundset.mp_users_to_mp_users_roles.getSelectedRecord() && foundset.mp_users_to_mp_users_roles.getSelectedRecord().isNew() && foundset.mp_users_to_mp_users_roles.getSelectedRecord().role_id == 2) {
		application.output("Ho aggiunto il ruolo assicuratore all\'utente " + rec.real_name + " e aggiungo la sua email alla lista");
		setInsurerEmail(rec.user_email);
	}
	pwdChanged = false;
	_super.saveEdits(event);
	application.output(globals.mpMessageLog + 'STOP mp_users_groups_details.saveEdits()', LOGGINGLEVEL.INFO);
}

/**
 * Handle changed data.
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"843C72D4-7610-4F87-BBC6-CDD8BBE4A425"}
 */
function onDataChange(oldValue, newValue, event) {
	if (oldValue != newValue && newValue == 'GENERALI') fep_code = null;
	updateUI(event);
}

/**
 * Handle changed data.
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C7C19817-F359-4062-83E6-8126E0363806"}
 */
function onDataChangePwd(oldValue, newValue, event) {
	if (oldValue != newValue && newValue != null) pwdChanged = true;
}
/**
 * Callback method for when form is shown.
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"44EA02D4-4497-4122-AE4D-6E5CC3C4C773"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
	pwdChanged = false;
	startEdit = 0;
}
/**
 * Inserisce se non c'è la email dell'assicuratore nella lista delle email degli assicuratori
 * (tabella : mp_mail_types, campo : mail_distribution_list)
 * @param {String} insurerEmailToAdd
 * @properties={typeid:24,uuid:"E946A5DD-68F7-430D-B256-D5C45B43B8D9"}
 * @AllowToRunInFind
 */
function setInsurerEmail(insurerEmailToAdd) {

	application.output(globals.mpMessageLog + 'START mp_users_groups_details.setInsurerEmail()', LOGGINGLEVEL.INFO);
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_mail_types>} */
	var mp_mail_types = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_mail_types');
	if (mp_mail_types.find()) {
		//cerco il record dove ci sono gli indirizzi mail degli assicuratori che ha id=11
		mp_mail_types.mail_type_id = 11;
		var searchResult = mp_mail_types.search();
		if (searchResult > 0) {
			if(!(globals.isEmpty(mp_mail_types.mail_distribution_list))){
			application.output("Lista email assicuratori " + mp_mail_types.mail_distribution_list);			
			var mailFoundedIndex = mp_mail_types.mail_distribution_list.indexOf(insurerEmailToAdd);
			//se non c'è la mail nella lista
			if (mailFoundedIndex == -1) {
				application.output("Aggiungo la mail alla lista");
				if (mp_mail_types.mail_distribution_list.length == 0) {
					application.output("Lista vuota delle mail assicuratori");
					mp_mail_types.mail_distribution_list = insurerEmailToAdd;
				} else {
					mp_mail_types.mail_distribution_list = mp_mail_types.mail_distribution_list + ',' + insurerEmailToAdd;
				}
				databaseManager.recalculate(mp_mail_types);
				application.output("Lista email assicuratori aggiornata " + mp_mail_types.mail_distribution_list);
			}
		}
		else{
			mp_mail_types.mail_distribution_list = insurerEmailToAdd;
		}
		}
	}
	application.output(globals.mpMessageLog + 'STOP mp_users_groups_details.setInsurerEmail()', LOGGINGLEVEL.INFO);
}
/**
 * Modifica email assicuratore nella lista delle email
 * @param{String} oldEmail
 * @param{String} newEmail
 * @properties={typeid:24,uuid:"73388D6D-74F6-4A9D-972C-D8817D4B3DC7"}
 * @AllowToRunInFind
 */
function changeInsurerEmail(oldEmail, newEmail) {
	application.output("-----------------------------------------------------------------------------------")
	application.output(globals.mpMessageLog + 'START mp_users_groups_details.changeInsurerEmail()', LOGGINGLEVEL.INFO);
	application.output("Vecchia mail : " + oldEmail);
	application.output("Nuova mail : " + newEmail);
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_mail_types>} */
	var mp_mail_types = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_mail_types');
	/** @type {String} */
	var insurerEmailList = null;
	if (mp_mail_types.find()) {
		mp_mail_types.mail_type_id = 11;
		var searchResult = mp_mail_types.search();
		insurerEmailList = mp_mail_types.mail_distribution_list;
		if (searchResult > 0) {
			application.output("Lista email assicuratori prima sostituzione : " + insurerEmailList);
			mp_mail_types.mail_distribution_list = insurerEmailList.replace(oldEmail, newEmail);

		}
		databaseManager.recalculate(mp_mail_types);
		application.output("Lista email assicuratori dopo sostituzione : " + mp_mail_types.mail_distribution_list);
	}
	//		forms.mp_groups_for_user_list.deleteInsurerEmail(oldEmail);
	//		setInsurerEmail(newEmail);
	application.output("-----------------------------------------------------------------------------------")
	application.output(globals.mpMessageLog + 'STOP mp_users_groups_details.changeInsurerEmail()', LOGGINGLEVEL.INFO);

}
/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"2A83ACDB-55DF-4CE3-A476-F78E2B72383B"}
 */
function startEditing(event) {
	application.output(globals.mpMessageLog + 'START mp_users_groups_details.startEditing()', LOGGINGLEVEL.INFO);
	var rec = foundset.getSelectedRecord();
	wasInsured = false;
	for (var i = 1; i <= rec.mp_users_to_mp_users_roles.getSize(); i++) {
		var recInsured = rec.mp_users_to_mp_users_roles.getRecord(i);
		if (recInsured.role_id == 3) {
			wasInsured = true;
			break;
		}
	}
	//Controllo se sono assicuratore
	if (isCurrentUserInsurer()) {
		actualInsurerEmail = rec.user_email;
		actualInsurerName = rec.user_name;
		application.output("Mail assicuratore attuale  " + actualInsurerEmail + " nome assicuratore selezionato " + actualInsurerName + " è assicuratore ? " + actualInsurer);
	}
	_super.startEditing(event);
	application.output(globals.mpMessageLog + 'STOP mp_users_groups_details.startEditing()', LOGGINGLEVEL.INFO);
}
/**
 *@return {Boolean}
 * @properties={typeid:24,uuid:"A8A4DB67-FD5F-4254-98FC-AC61AB7EC2DB"}
 * @AllowToRunInFind
 */
function isCurrentUserInsurer() {
	application.output(globals.mpMessageLog + 'START mp_users_groups_details.isCurrentUserInsurer()', LOGGINGLEVEL.INFO);
	application.output("-----------------------------------------------------------------------------------")
	actualInsurer = false;
	var userFoundSet = foundset.getSelectedRecord().mp_users_to_mp_users_roles;
	for (var index = 1; index <= userFoundSet.getSize(); index++) {
		var rec = userFoundSet.getRecord(index);
		if (rec.role_id == 2) {
			actualInsurer = true;
			break;
		}
	}
	return actualInsurer;
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"44B1D61A-47B2-4C2C-B608-719993287847"}
 */
function stopEditing(event) {
	_super.stopEditing(event);
	startEdit = 0;
}
