dataSource:"db:/pratiche_sanitarie/mp_users_roles",
extendsID:"DBAB17E3-EFD6-401A-824A-F0446C339D31",
items:[
{
displaysTags:true,
formIndex:3,
horizontalAlignment:2,
location:"10,4",
size:"375,28",
text:"Ruoli",
transparent:true,
typeid:7,
uuid:"1EED9BDA-3448-469D-A73E-D92D092FB4A8"
},
{
dataProviderID:"role_id",
displayType:10,
enabled:false,
formIndex:3,
location:"10,40",
name:"roleExceptInsured",
size:"375,28",
typeid:4,
uuid:"43D47588-1B58-4ADF-96CE-DBACD6FF222A",
valuelistID:"DC80BECD-5799-4E29-A90F-0B859709819B",
visible:false
},
{
extendsID:"58BD9147-5AE4-4853-927C-0495957B2C6F",
onActionMethodID:"99769947-A64A-443F-A76B-0BF0D1559ED5",
typeid:7,
uuid:"4C3EF4E0-42CA-43CD-A7FE-8E10F2ACC8CB"
},
{
dataProviderID:"is_selected",
extendsID:"C221B10B-530E-4609-8EBF-CBDBC4F28A5A",
typeid:4,
uuid:"56398C79-E2FE-4254-AD5C-5E26B41D9BBD",
visible:false
},
{
customProperties:"",
extendsID:"1346538E-0AF7-476B-90F4-3040508C5C39",
onActionMethodID:"301C187E-E371-4724-8A04-3DF82E82AC8E",
typeid:7,
uuid:"724B7669-19E2-4613-850C-352AD9249A3C"
},
{
extendsID:"F95AD149-E872-4B52-921E-838116060658",
typeid:7,
uuid:"8541A868-105B-43F9-AD63-60FA0C2FF3E8",
visible:false
},
{
extendsID:"8B019565-1F86-46DF-902D-0D74E978DCF0",
typeid:4,
uuid:"BEE98147-3A62-4CAE-84C3-7539A0321660",
visible:false
},
{
dataProviderID:"role_id",
displayType:10,
formIndex:3,
location:"10,40",
name:"role",
size:"375,28",
typeid:4,
uuid:"D6A90584-4A47-44E1-8B16-5FCA90DCD0A0",
valuelistID:"AC119B78-1781-4ADB-9D4B-73A0CF3277F5"
}
],
name:"mp_groups_for_user_list",
scrollbars:0,
styleName:"GeCo",
typeid:3,
uuid:"11520F16-51B9-4FDD-8277-5F1C3F070F1F",
view:1