dataSource:"db:/pratiche_sanitarie/mp_users",
extendsID:"1EA6575E-5ED3-4FB8-85CD-05DA39570746",
items:[
{
formIndex:3,
horizontalAlignment:2,
location:"25,53",
size:"59,20",
text:"Gruppo",
transparent:true,
typeid:7,
uuid:"0C25E889-9C8A-4945-94CB-8B4BCE32E31E"
},
{
extendsID:"97C8E3BE-3DCD-4394-AB5A-7F5D55D09D37",
height:119,
typeid:19,
uuid:"177954E6-B381-43E2-8482-B95AC338DB3A"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"isEnabled",
displayType:4,
enabled:false,
formIndex:2,
horizontalAlignment:2,
location:"5,79",
onActionMethodID:"F7947181-3E04-41D5-9F66-47D28265842A",
onDataChangeMethodID:"-1",
size:"153,20",
text:"Attivi",
toolTipText:"Mostra solo attivi",
transparent:true,
typeid:4,
uuid:"1B7E2422-D0CB-4BCF-8A04-B4D253F6972A",
visible:false
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"usersGroup",
displayType:2,
editable:false,
formIndex:1,
horizontalAlignment:2,
location:"96,53",
onActionMethodID:"-1",
onDataChangeMethodID:"1FDAEC23-94C5-41CD-B9A8-CD714D777CF8",
size:"245,20",
text:"User Id",
toolTipText:"Filtra per gruppo",
typeid:4,
uuid:"CC61F6C0-E902-4E0A-9CD6-C3460B256D9B",
valuelistID:"AC119B78-1781-4ADB-9D4B-73A0CF3277F5"
},
{
anchors:11,
borderType:"SpecialMatteBorder,1.0,0.0,0.0,0.0,#cccccc,#000000,#000000,#000000,0.0,",
enabled:false,
formIndex:10,
horizontalAlignment:0,
location:"0,112",
mediaOptions:6,
printSliding:16,
size:"400,1",
text:"",
transparent:true,
typeid:7,
uuid:"CF396469-5D6D-4E00-B000-32AB1F79C9B3",
visible:false
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"userId",
displayType:2,
editable:false,
formIndex:1,
horizontalAlignment:2,
location:"96,24",
onActionMethodID:"-1",
onDataChangeMethodID:"1FDAEC23-94C5-41CD-B9A8-CD714D777CF8",
size:"245,20",
text:"User Id",
toolTipText:"Filtra per gruppo",
typeid:4,
uuid:"D05E137F-F16E-4B84-96D5-ED5B6924B740",
valuelistID:"BCCF4721-1F38-432E-9887-331FDA4ACD42"
},
{
anchors:3,
customProperties:"",
formIndex:5,
imageMediaID:"B432FC13-6422-4A74-956D-4474A7CD5FF0",
location:"366,82",
onActionMethodID:"F35A3FB1-30AC-4A53-B47F-A69AEFA590D1",
rolloverImageMediaID:"B2C0D6C5-8456-4477-BFD5-825161DD2756",
showClick:false,
showFocus:false,
size:"16,17",
transparent:true,
typeid:7,
uuid:"D169CFD9-1FBD-402F-8068-D08744CD6263"
},
{
formIndex:3,
horizontalAlignment:2,
location:"25,24",
size:"59,20",
text:"Utente",
transparent:true,
typeid:7,
uuid:"FCED4B73-47C1-424A-A100-A78EEAEA5F88"
}
],
name:"mp_users_groups_filter",
onLoadMethodID:"F7947181-3E04-41D5-9F66-47D28265842A",
onShowMethodID:"-1",
typeid:3,
uuid:"D153D06A-8394-4ED8-A163-707D210D196D"