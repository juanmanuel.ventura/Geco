
/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formFilterName
 * @param {String} formListName
 *
 * @private
 *
 * @properties={typeid:24,uuid:"E0AFA68B-D737-41C1-BFBA-D3A2C365FB28"}
 */
function onLoad(event, formFilterName, formListName) {
	_super.onLoad(event, formFilterName, formListName);
}
