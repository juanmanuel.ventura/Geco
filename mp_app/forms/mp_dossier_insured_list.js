/**
 *
 * @param {JSEvent} event   		Event that triggered the action
 * @param {String} 	formName 		The name of the form in which to display details
 * @param {Boolean} toDetails 		Boolean to show details or to go back to the list
 *
 * @properties={typeid:24,uuid:"2B4367BA-B632-44BF-BF69-9A7465E1EBA4"}
 */
function mpShowDetailsList(event, formName, toDetails) {

	if (toDetails == false && forms.__mp_base.isEditing()) {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!", "OK");
		return;
	}
	application.output(scopes.globals.mpMessageLog + ' mp_dossier_insured_list.showDetailsList ' + formName + ' ' + toDetails);

	if (toDetails) {

		forms.mp_dossier_bundle.initialize(event, "mp_dossier_details", null);
	} else {
	}
}
/*		//forms[formName].foundset.loadAllRecords();
 // force to apply filter (seems not to work on form's onLoad event)
 var filterForm = forms[formName].elements.split.getLeftForm();
 //application.output('filterForm: ' + filterForm);

 //.getLeftForm()

 if (filterForm && filterForm.applyFilter) {
 //application.output('ritorno al filtro ' + intermediate)
 filterForm.applyFilter();
 }
 }
 }*/

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F85DCD44-B38C-4DDD-A900-8C850AEF75D7"}
 */
function goToDetails(event) {
	application.output("DOSSIER ID : " + dossier_id);

	globals.mpShowDetailsList(event, "mp_dossier_insured_bundle", "mp_dossier_details", true);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"19359847-063F-43AF-B4CF-5ED7AFFBB9BA"}
 */
function createNewRecord(event) {
	var mobileNumber=scopes.globals.mp_currentuserid_to_mp_insured_users.mobile_number;
	//var ibanCode=scopes.globals.mp_currentuserid_to_mp_insured_users.iban_code;
	var birthPlace=globals.mp_currentuserid_to_mp_insured_users.birth_date;
	var birthDate=globals.mp_currentuserid_to_mp_insured_users.birth_place;
	var fiscalCode=globals.mp_currentuserid_to_mp_insured_users.fiscal_code;
	var districtBirth=globals.mp_currentuserid_to_mp_insured_users.birth_district;
	var district=globals.mp_currentuserid_to_mp_insured_users.district;
	//controllo che i campi obbligatori in pratiche sanitarie abbiano un valore != null,altrimenti compare il messaggio all'utente 
	if (mobileNumber == null || birthDate == null || birthPlace ==null || fiscalCode ==null || district==null || districtBirth==null) {
		var str = globals.DIALOGS.showInfoDialog('Avviso utente','Per creare una pratica di rimborso completa prima la tua Anagrafica', 'OK','ANNULLA');
		if (str.equals('OK')) {
			forms.mp_main.loadPanel(event, 'mp_insured_bundle');
			forms.mp_main.elements.selector.setLocation(forms.mp_main.elements.button2c.getLocationX(), forms.mp_main.elements.button2c.getLocationY());
			forms.mp_main.elements.selector.setSize(forms.mp_main.elements.button2c.getWidth(), forms.mp_main.elements.button2c.getHeight());
			return;
		}
		else{
			_super.stopEditing(event);
			return;
		}
	}
	//se c'è un familiare ed è abilitato apro la popup altrimenti no

	if (databaseManager.hasRecords(mp_currentuserid_to_mp_insured_users.mp_insured_users_to_mp_insured_relatives_enabled)) {
		//popup per scegliere come assistito se stesso o un parente
		showRelativeChooserPopup();
	} else {
		//		//Avviso l'utente che la pratica sarà creata per lui poiché non ci sono familiari
		//		var dialogText = "Non ci sono familiari abilitati associati,controlla la tua sezione Anagrafica";
		//		globals.DIALOGS.showInfoDialog("Avviso", dialogText, "chiudi");
	}
	var idx = _super.newRecord(event);
	// stato pratica  = 1,corrisponde allo stato 'Nuova'
	dossier_status_id = 1;
	application.output("-------------------------------------------------------------------")
	application.output("nuovo record pratica " + idx + " e lo stato è " + dossier_status_id);
	//passo alla visualizzazione lista delle pratiche
	globals.mpShowDetailsList(event, 'mp_dossier_insured_bundle', 'mp_dossier_details', true);

}
/**
 * Popup per la scelta del familiare di cui si chiede il rimborso per le spese sanitarie
 * @author Francesco Sigolotto
 * @private
 * @properties={typeid:24,uuid:"3E213F71-3C96-4155-8A3B-EC9D1ECA517B"}
 */
function showRelativeChooserPopup() {

	var relativePopup = application.createWindow("Popup parente ", JSWindow.MODAL_DIALOG);
	relativePopup.setInitialBounds(1000, 1000, 350, 100);
	relativePopup.title = 'La pratica è per un familiare?';
	relativePopup.show(forms.mp_dossier_insured_popup_relative);
}
/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"EA08A232-2D57-4FB7-95E9-9C5C7B5A65A0"}
 * @AllowToRunInFind
 */
function onShow(firstShow, event) {
	updateUI(event);
}
/**
 * @author Francesco Sigolotto
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"B0449482-0C62-49E6-88EF-31785EB16702"}
 */
function updateUI(event) {

	if (!scopes.globals.mp_currentuserid_to_mp_insured_users.user_insured_id) {
		elements.buttonAdd.enabled = false;
		return;
	} else {
		elements.buttonAdd.enabled = true;
	}
	//disabilito pulsante creazione pratiche per i ruoli Admin e HR Admin
	if (globals.isAdmin && globals.isInsured) {
		elements.buttonAdd.visible = true;

		return;
	}
	if (globals.isAdmin) {
		elements.buttonAdd.visible = false;
	}
	//se sono HR admin e assicurato
	if (globals.isHrAdmin && globals.isInsured) {
		elements.buttonAdd.visible = true;
		return;
	}
	//aggiornamento del beneficiario della pratica
//	for (var index = 1; index <= foundset.getSize(); index++) {
//		var record = foundset.getRecord(index);		
//		if(record.is_for_relative==1){
//			record.dossier_beneficiary_name=record.mp_medical_dossiers_to_mp_insured_relatives.real_name;
//		}
//		if(record.is_for_relative==0){
//			record.dossier_beneficiary_name=record.mp_medical_dossiers_to_mp_insured_users.real_name;
//		}
//	}
}
/**
 * Handle record selected.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"A5E5E679-F7D8-4090-A974-FCFCE21D3469"}
 */
function onRecordSelection(event) {
	_super.onRecordSelection(event);
}

