/**
 * @type {String}
 * @properties={typeid:35,uuid:"5F549433-B4B7-46A7-BE84-ADE3E319C4EA"}
 */
var insuredInsuranceCompanyName = null;
/**
 * @type {Number}
 * @properties={typeid:35,uuid:"5981F91B-84ED-4F1B-B85A-1D0170FE9011",variableType:8}
 */
var insuredRelativeName = null;

/**@type {String}
 * @properties={typeid:35,uuid:"CD1CD1C8-BBCE-4823-8D6E-17E1C5F74360"}
 */
var insuredDossierClosureDate = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"754709B7-0352-4BF8-9E3A-04C1BD66FDDA"}
 */
var insuredDossierCreationDate = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"16C1C168-EC1E-402F-8850-62526E0F65B2"}
 */
var insuredDossierModificationDate = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"A234F9AB-4E36-4850-9291-475B2DAE732A"}
 */
var insuredDossierCreationYear = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"5F21CF52-8889-469C-B8DD-C47BDC39F178"}
 */
var insuredInsuranceNumber = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"8E1AB1A3-1536-43A0-8369-813D6A3B15AE",variableType:8}
 */
var insuredDossierStatus = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"CBEA38DA-83B8-4C16-A671-B156A162A813"}
 */
var dateAsString = null;

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"4753E242-C5C4-4825-AE0F-34597A5ACD06"}
 */
function resetFilter(event) {
	//resetto i campi dei filtri
	insuredDossierStatus = null;
	dossierClosed = null;
	insuredInsuranceCompanyName = null
	insuredRelativeName = null;
	insuredDossierCreationDate = null;
	insuredDossierClosureDate = null;
	insuredDossierModificationDate = null;
	insuredDossierCreationYear = null;
	relativeEnabled = null;
	applyFilter(event);
}
/**
 * @author Francesco Sigolotto
 * @param {JSEvent} event
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"630A83A8-F737-4C3F-8E61-7246918BE9C0"}
 */
//function loadInsuredDossier(event) {
//	application.output(globals.mpMessageLog + 'START dossier_insured_filter.loadInsuredDossier() FIND METHOD', LOGGINGLEVEL.INFO);
//	//carico le pratiche corrette per l'assicurato loggato
//	if (foundset.find()) {
//		if (scopes.globals.mp_currentuserid_to_mp_insured_users.user_insured_id) {
//			application.output(globals.mpMessageLog + 'dossier_insured_filter.loadInsuredDossier() ID utente loggato ' + scopes.globals.mp_currentuserid_to_mp_insured_users.user_insured_id, LOGGINGLEVEL.INFO);
//			user_insured_id = scopes.globals.mp_currentuserid_to_mp_insured_users.user_insured_id;
//			application.output(globals.mpMessageLog + 'dossier_insured_filter.loadInsuredDossier() ID assicurato nella pratica medica ' + user_insured_id, LOGGINGLEVEL.INFO);
//			var result = foundset.search();
//			application.output(globals.mpMessageLog + 'dossier_insured_filter.loadInsuredDossier() Trovate: ' + result + ' pratiche sanitarie', LOGGINGLEVEL.INFO);
//		}
//	}
//	application.output(globals.mpMessageLog + 'STOP dossier_insured_filter.loadInsuredDossier() FIND METHOD STOP', LOGGINGLEVEL.INFO);
//}
/**
 * Apply searching filters
 *
 * @private
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"2297977E-DDE5-4788-BB26-890673B661CD"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	application.output(globals.mpMessageLog + 'START dossier_insured_filter.applyFilter()' + relativeInsured, LOGGINGLEVEL.INFO);
	//filtri del foundset
	if (foundset.find()) {
		//assicurato
//		if (!scopes.globals.mp_currentuserid_to_mp_insured_users.user_insured_id) {
//			foundset.user_insured_id=-1;
//		}
//		//Controllo se l'utente che è loggato come assicurato abbia la sua anagrafica
		user_insured_id = scopes.globals.mp_currentuserid_to_mp_insured_users.user_insured_id != null ?scopes.globals.mp_currentuserid_to_mp_insured_users.user_insured_id :-1 ;
		//verificare che il familiare sia abilitato o meno

		//Solo familiari abilitati
		if (databaseManager.hasRecords(mp_medical_dossiers_to_mp_insured_relatives) && relativeEnabled == 1) {
			foundset.mp_medical_dossiers_to_mp_insured_relatives.is_relative_enabled = relativeEnabled;
		}
		//tutte le pratiche con familiari disabilitati
		else if (databaseManager.hasRecords(mp_medical_dossiers_to_mp_insured_relatives) && relativeEnabled == 0) {
			foundset.mp_medical_dossiers_to_mp_insured_relatives.is_relative_enabled = 0;
		} else if (databaseManager.hasRecords(mp_medical_dossiers_to_mp_insured_relatives) && (relativeEnabled == null)) {
			foundset.mp_medical_dossiers_to_mp_insured_relatives.is_relative_enabled = null;
		}
		//familiare assicurato
		if (insuredRelativeName != null) {
			foundset.mp_medical_dossiers_to_mp_insured_relatives.relative_id = insuredRelativeName;
		}
		//stato del dossier
		if (insuredDossierStatus != null) {
			foundset.dossier_status_id = insuredDossierStatus;
		}
		//Pratica chiusa
		if (dossierClosed != null) {
			foundset.is_closed = dossierClosed;
		}
		//anno creazione
		if (insuredDossierCreationYear != null) {
			
			created_at = forms.mp_dossier_filter.searchByYear(insuredDossierCreationYear);
		}
		//data creazione pratica
		if (insuredDossierCreationDate != null) {
			created_at = forms.mp_dossier_filter.searchByDate(insuredDossierCreationDate);
		}
		//data ultimo aggiornamento pratica
		if (insuredDossierModificationDate != null) {
			modified_at = forms.mp_dossier_filter.searchByDate(insuredDossierModificationDate);
		}
		//data chiusura pratica
		if (insuredDossierClosureDate != null) {
			closed_at = forms.mp_dossier_filter.searchByDate(insuredDossierClosureDate);
		}
		//familiare (se c'è)
		if (insuredRelativeName != null) {
			foundset.relative_id = insuredRelativeName;
		}
		//Numero polizza
		if (insuredInsuranceNumber != null) {
			//application.output(globals.mpMessageLog + 'dossier_insured_filter.applyFilter() numero polizza: ' + insuredInsuranceNumber, LOGGINGLEVEL.INFO);
			mp_currentuserid_to_mp_insured_users.insured_users_to_insurance_policies.insurance_number = insuredInsuranceNumber;
		}
		//Nome assicurazione
		//currentuserid_to_insured_users.insured_users_to_insurance_policies.insurance_company = insuredInsuranceCompanyName;
		//Azienda contraente
		if (contractor != null) {
			application.output(globals.mpMessageLog + 'dossier_insured_filter.applyFilter() contraente : ' + contractor, LOGGINGLEVEL.INFO);
			//medical_dossiers_to_insured_users.insured_users_to_companies.company_name = contractor;
		}
		var tot = foundset.search();
		forms.mp_dossier_insured_list.elements.lCountPractices.text = tot;
	}
	application.output(globals.mpMessageLog + 'STOP dossier_insured_filter.applyFilter() ', LOGGINGLEVEL.INFO);
}
/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C88D1F5B-7F8A-4C4B-8850-E2487841CD77"}
 */
function onShow(firstShow, event) {
	reloadFilterValuelist();
	applyFilter(event);
}
/**
 * @properties={typeid:24,uuid:"9F998466-FAB0-459F-ABDD-39D03E857C48"}
 */
function reloadFilterValuelist() {
	//application.output(globals.mpMessageLog + 'START dossier_insured_filter.reloadFilterValuelist() flushAllClientsCache',LOGGINGLEVEL.INFO);
	//Ricarico i dati delle valuelist dei filtri
	plugins.rawSQL.flushAllClientsCache('pratiche_sanitarie', 'mp_insured_dossier_creation_date');
	plugins.rawSQL.flushAllClientsCache('pratiche_sanitarie', 'mp_insured_dossier_modification_date');
	plugins.rawSQL.flushAllClientsCache('pratiche_sanitarie', 'mp_insured_dossier_closure_date');
	plugins.rawSQL.flushAllClientsCache('pratiche_sanitarie', 'mp_insured_dossier_creation_year');
	//application.output(globals.mpMessageLog + 'STOP dossier_insured_filter.reloadFilterValuelist()',LOGGINGLEVEL.INFO);
}

