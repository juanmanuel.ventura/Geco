/**
 * @type {String}
 * @properties={typeid:35,uuid:"77ECEE6F-22AB-46DC-A7D7-FBD1D636C9E0"}
 */
var resultMessage = null;

/**
 * @properties={typeid:35,uuid:"FC2197E5-3F97-40AE-B206-B98CAC13FC48",variableType:-4}
 */
var client = null;

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"24B6CC9B-6A15-4B9F-880B-0FEB1C449319",variableType:-4}
 */
var successMPUser = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"333072CD-3CA3-4952-AFC9-D2E0A8DD7223"}
 */
var message = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2E885788-A775-4DB3-A152-CC1182CA1E8A"}
 */
var startDate = '2014-09-01';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F22519C5-E585-48C9-B057-818318D8F271"}
 */
var endDate = '2014-12-31';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B5D71982-BB62-40B3-A5A2-472503D96CD4"}
 */
var username = 'luca.luca';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"87CA2BE6-C4D3-48F3-8CE8-520723EF0D3C"}
 */
var realname = 'LUCA LUCA';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A80418CB-6FD2-497E-84D3-895F52B55E36"}
 */
var companyName = 'SPINDOX';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"37908010-A786-4A5C-8768-77408999B167"}
 */
var pwd = 'generali';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"357A0158-0A4B-4CE7-9A72-1720149D3552"}
 */
var fepcode = '10000';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DCB2ED52-DEB9-4E4E-A872-75276988BE8E"}
 */
var email = 'luca.luca@spindox.it';

/**
 * @param {JSEvent} event *
 * @properties={typeid:24,uuid:"231BBDA6-F5EA-4EDF-928E-7170C3ED5621"}
 */
function closeForm(event) {
	clearField();
	controller.getWindow().destroy();
}

/**
 * Handle hide window.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"AAA748A1-C6B9-49EF-9DDA-7A1F90FFE3A9"}
 */
function onHide(event) {
	application.output(globals.mpMessageLog + 'START mp_insert_form.onHide() ', LOGGINGLEVEL.INFO);
	var cleared = clearField();
	application.output(globals.mpMessageLog + 'START mp_insert_form.onHide() Field cleared? ' + cleared, LOGGINGLEVEL.DEBUG);
	var currentWindow = controller.getWindow();

	if (currentWindow != null) {
		currentWindow.destroy();
	}
	application.output(globals.mpMessageLog + 'STOP mp_insert_form.onHide() currentWindow destroyed', LOGGINGLEVEL.INFO);
}

/**
 * @return {Boolean}
 * @properties={typeid:24,uuid:"761D5E17-9A8C-4000-94BA-D4DEB4F477BE"}
 */
function clearField() {
	try {
		//		startDate = null;
		//		endDate = null;
		//		username = null;
		//		realname = null;
		//		fepcode = null;
		//		pwd = null;
		//		companyName = null;
		//		email = null;
		return true;
	} catch (e) {
		application.output('error: ' + e);
		return false;
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} action
 * @properties={typeid:24,uuid:"7E2AC331-675E-4E06-BD77-4A6B52175EDF"}
 */
function execute(event, action) {
	application.output(globals.mpMessageLog + 'START mp_insert_form.execute()', LOGGINGLEVEL.INFO);
	if (startDate == null || endDate == null || username == null || email == null || realname == null || companyName == null || pwd == null || fepcode == null) {
		globals.DIALOGS.showErrorDialog('Error', 'Inserisci tutti i campi.', 'OK');
		return;
	}

	if (action == 1) {
		var details = {
			user_name: username,
			start_date: startDate,
			end_date: endDate,
			real_name: realname,
			first_name: 'luca',
			last_name: 'luca',
			company_name: companyName,
			user_password: '72dd47252f9f1236a149f6033b7c94ab2e4c43e6',
			fep_code: fepcode,
			e_mail: email
		};

		client = plugins.headlessclient.createClient("mp_queue", null, null, null);
		if (client != null && client.isValid()) {
			application.output(globals.mpMessageLog + 'mp_insert_form.execute() Cient OK ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
			client.queueMethod(null, "createMPUserFromGecoPersonnel", [details, application.getServerURL()], enqueueMPUserFromGecoPersonnel);
		} else {
			application.output(globals.mpMessageLog + 'mp_insert_form.execute() Client is not valid ', LOGGINGLEVEL.DEBUG);
			globals.DIALOGS.showErrorDialog('Error', 'Client is not valid. Please contact the support at: geco_helpdesk@spindox.it', 'OK');
			return;
		}
	} else {
		var detailsToModify = {
			user_name: username,
			end_date: action == 2 ? '2014-08-15' : endDate
		};

		client = plugins.headlessclient.createClient("mp_queue", null, null, null);
		if (client != null && client.isValid()) {
			application.output(globals.mpMessageLog + 'mp_users_details.saveEdits() Cient OK ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
			client.queueMethod(null, "modifyMPUserFromGecoPersonnel", [detailsToModify, application.getServerURL()], enqueueModifiedMPUserFromGecoPersonnel);
		} else {
			application.output(globals.mpMessageLog + 'mp_users_details.saveEdits() Client is not valid ', LOGGINGLEVEL.DEBUG);
			globals.DIALOGS.showErrorDialog('Error', 'Client is not valid. Please contact the support at: geco_helpdesk@spindox.it', 'OK');
			return;
		}
	}
}

/**
 * @param {JSEvent} headlessCallback
 * @properties={typeid:24,uuid:"00CAD9A1-32A3-430C-B4D5-92D3F037BCC1"}
 */
function enqueueMPUserFromGecoPersonnel(headlessCallback) {
	application.output(globals.mpMessageLog + 'START mp_users_details.enqueueMPUserFromGecoPersonnel() ', LOGGINGLEVEL.INFO);
	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		if (headlessCallback.data.success == false) {
			resultMessage = 'Errore nella creazione dell\'utente:\n' + headlessCallback.data.message;
			application.output(globals.mpMessageLog + 'mp_users_details.enqueueMPUserFromGecoPersonnel() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			globals.DIALOGS.showErrorDialog('ERROR', resultMessage, 'OK');
			return;
		} else {
			application.output(globals.mpMessageLog + 'mp_users_details.enqueueMPUserFromGecoPersonnel() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			if (headlessCallback.data.message == 'Utente creato con successo.') {
				var objMail = globals.mpGetMailType(1);
				var mailSubs = {
					nome: '',
					hrName: '',
					link: ''
				};
				var recipient = email;
				//var recipient = objMail.distrList;
				var subject = objMail.subject;
				var hostname = application.getServerURL();
				var link = hostname + '/servoy-webclient/application/solution/mp_app';
				mailSubs.nome = 'luca';
				mailSubs.hrName = 'LUCIANO LIGABUE';
				mailSubs.link = (link != null) ? link : '';
				var messageMail = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubs);
				application.output(globals.mpMessageLog + 'globals.sendMailForNewInsuredUser() SENDING MAIL to distribution list  subject: ' + subject + 'messageMail: ' + messageMail, LOGGINGLEVEL.INFO);

				client.queueMethod(null, "sendMail", [recipient, subject, messageMail], scopes.globals.mpEnqueueMailLogForInsuredUsers);
				//Wait 5 seconds before shutting down the client
				application.sleep(5000);
				if (client != null && client.isValid()) {
					application.output('chiudo client ' + client.getClientID());
							client.shutdown();
				}
			}
			globals.DIALOGS.showInfoDialog('INFO', headlessCallback.data.message, 'OK');
			onHide(headlessCallback);
		}
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output(globals.mpMessageLog + 'mp_users_details.saveEdits() exception callback, name: ' + headlessCallback.data + headlessCallback.data.message, LOGGINGLEVEL.DEBUG);
		resultMessage = 'Errore nella creazione dell\'utente:\n' + headlessCallback.data + headlessCallback.data.message;
		globals.DIALOGS.showErrorDialog('ERROR', resultMessage, 'OK');
		return;

	}
	application.output(globals.mpMessageLog + 'mp_users_details.saveEdits() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
	if (client != null && client.isValid()) {
		application.output(globals.mpMessageLog + 'mp_users_details.saveEdits() close client ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
		client.shutdown();
	}
	application.output(globals.mpMessageLog + 'STOP mp_users_details.enqueueMPUserFromGecoPersonnel() ', LOGGINGLEVEL.INFO);
}

/**
 * @param {JSEvent} headlessCallback
 * @properties={typeid:24,uuid:"D22B8766-80DA-4393-A680-CAAD954A96EE"}
 */
function enqueueModifiedMPUserFromGecoPersonnel(headlessCallback) {
	application.output(globals.mpMessageLog + 'START mp_users_details.enqueueModifiedMPUserFromGecoPersonnel() ', LOGGINGLEVEL.INFO);
	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		if (headlessCallback.data.success == false) {
			//c'è stato un errore
			resultMessage = 'Errore nella modifica della data di Fine Copertura Assicurativa:\n' + headlessCallback.data.message;
			application.output(globals.mpMessageLog + 'mp_users_details.enqueueModifiedMPUserFromGecoPersonnel() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			globals.DIALOGS.showErrorDialog('ERROR', resultMessage, 'OK');
			return;
		} else {
			application.output(globals.mpMessageLog + 'mp_users_details.enqueueModifiedMPUserFromGecoPersonnel() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			if (headlessCallback.data.message == 'Data di Fine Copertura Assicurativa modificata con successo.') {
				var objMail = globals.mpGetMailType(2);
				var mailSubs = {
					nome: '',
					hrName: ''
				};
				var recipient = email;
				//var recipient = objMail.distrList;
				var subject = objMail.subject;
				mailSubs.nome = 'luca';
				mailSubs.hrName = 'LUCIANO LIGABUE';
				var messageMail = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubs);
				application.output(globals.mpMessageLog + 'globals.sendMailForNewInsuredUser() SENDING MAIL to distribution list  subject: ' + subject + 'messageMail: ' + messageMail, LOGGINGLEVEL.INFO);

				client.queueMethod(null, "sendMail", [recipient, subject, messageMail], scopes.globals.mpEnqueueMailLogForInsuredUsers);
				//Wait 5 seconds before shutting down the client
				application.sleep(5000);
				if (client != null && client.isValid()) {
					application.output('chiudo client ' + client.getClientID());
							client.shutdown();
				}
			}
			globals.DIALOGS.showInfoDialog('INFO', headlessCallback.data.message, 'OK');
			onHide(headlessCallback);
		}
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output(globals.mpMessageLog + 'mp_users_details.saveEdits() exception callback, name: ' + headlessCallback.data + headlessCallback.data.message, LOGGINGLEVEL.DEBUG);
		//c'è stato un errore
		resultMessage = 'Errore nella modifica della data di Fine Copertura Assicurativa:\n' + headlessCallback.data + headlessCallback.data.message;
		globals.DIALOGS.showErrorDialog('ERROR', resultMessage, 'OK');
		return;
	}
	application.output(globals.mpMessageLog + 'mp_users_details.saveEdits() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
	if (client != null && client.isValid()) {
		application.output(globals.mpMessageLog + 'mp_users_details.saveEdits() close client ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
		client.shutdown();
	}
	application.output(globals.mpMessageLog + 'STOP mp_users_details.enqueueModifiedMPUserFromGecoPersonnel() ', LOGGINGLEVEL.INFO);
}
