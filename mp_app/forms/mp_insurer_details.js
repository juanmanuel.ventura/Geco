/**
 * Handle record selected.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"F6D1DEF7-2931-4D94-89A4-B7A7E149C12D"}
 */
function onRecordSelection(event) {
	globals.mpSelectedUser = foundset.user_id;
	_super.onRecordSelection(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"83010BB9-AEBF-46A6-997E-D86E4AE84E9F"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
	application.output(globals.mpMessageLog + 'START mp_insurer_details.saveEdits() ', LOGGINGLEVEL.INFO);
	var isRecordNew = false;
	scopes.globals.userDontExistInInsuredUsers = false;
//	/** @type {Boolean} */
//	var hasPassed = false;
	/** @type {String} [] */
	var arrayRealNames = [];
//	var sumDataChanged = 0;
	var editedRecords = databaseManager.getEditedRecords(foundset.mp_insured_users_to_mp_insured_relatives);
	for (var index = 0; index < editedRecords.length; index++) {
		var ds = editedRecords[index].getChangedData();
		/** @type {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} */
		var recEditedRecord = editedRecords[index];
		// Get a dataset with outstanding changes on a record
		for (var i = 1; i <= ds.getMaxRowIndex(); i++) {
			if (ds.getValue(i, 1) == 'is_relative_enabled') {
				//se vecchio è diverso da nuovo e (vecchio è 0 e nuovo è 1)
				if (ds.getValue(i, 2) != ds.getValue(i, 3) && (ds.getValue(i, 2) == 0 && ds.getValue(i, 3) == 1)) {
					//hasPassed = true;
					//sumDataChanged = sumDataChanged + 1;
				}
				//se vecchio è diverso da nuovo e (vecchio è 1 e nuovo è 0)
				if (ds.getValue(i, 2) != ds.getValue(i, 3) && (ds.getValue(i, 2) == 1 && ds.getValue(i, 3) == 0)) {
					//sumDataChanged = sumDataChanged -1;
					/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_medical_dossiers>} */
					var medicalDossiers = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_medical_dossiers');
					if (medicalDossiers.find()) {
						medicalDossiers.user_insured_id = recEditedRecord.user_insured_id;
						medicalDossiers.relative_id = recEditedRecord.relative_id;
						medicalDossiers.dossier_status_id = [1, 2, 3, 4, 5, 6];
						medicalDossiers.is_closed = 0;
						var totMD = medicalDossiers.search();

						if (totMD > 0) arrayRealNames.push(recEditedRecord.real_name);
					}
				}
			}
		}
	}
	if (arrayRealNames.length != 0) {
		var realNames = '';
		for (var v = 0; v < arrayRealNames.length; v++) {
			if (v == 0) realNames = arrayRealNames[v];
			else realNames = realNames + ', ' + arrayRealNames[v];
		}
		globals.DIALOGS.showErrorDialog('Error', 'Impossibile disabilitare i familiari:\n' + realNames + '\nCi sono pratiche in corso.', 'OK');
		return;
	}
//	if (foundset.mp_insured_users_to_mp_insured_relatives.getSize() > 0 && foundset.has_relative == 0) {
//		if (hasPassed == false && (foundset.mp_insured_users_to_mp_insured_relatives.relEnabled + sumDataChanged) > 0) {
//			application.output(globals.mpMessageLog + 'mp_insured_family_details.saveEdits() KO: La copertura è estesa a uno o più familiari.\nDisabilitare prima i familiari dall\'elenco, togliere il flag dalla copertura e quindi salvare.', LOGGINGLEVEL.DEBUG);
//			globals.DIALOGS.showErrorDialog('IMPOSSIBILE PROCEDERE COL SALVATAGGIO', 'La copertura è estesa a uno o più familiari.\nDisabilitare prima i familiari dall\'elenco, togliere il flag dalla copertura e quindi salvare.', 'OK');
//			return;
//		} else if (hasPassed) { //&& sumIsRelativeEnabled > 0){
//			application.output(globals.mpMessageLog + 'mp_insured_family_details.saveEdits() KO: La copertura è estesa a uno o più familiari.\nDisabilitare prima i familiari dall\'elenco, togliere il flag dalla copertura e quindi salvare.', LOGGINGLEVEL.DEBUG);
//			globals.DIALOGS.showErrorDialog('IMPOSSIBILE PROCEDERE COL SALVATAGGIO', 'La copertura è estesa a uno o più familiari.\nDisabilitare prima i familiari dall\'elenco, togliere il flag dalla copertura e quindi salvare.', 'OK');
//			return;
//		}
//	}
//	var insuredAddress = forms.mp_insured_details.address;
//	if (!globals.isEmpty(insuredAddress)) {
//		var result = insuredAddress.match(',');
//		if (result != -1) {
//			var comma = insuredAddress.indexOf(result[0]);
//			var _address = insuredAddress.slice(0, comma);
//			var civicNumber = insuredAddress.match(/\d+/);
//			if (civicNumber != undefined) {
//				var newAddress = _address + ',' + civicNumber[0];
//				forms.mp_insured_details.address = newAddress;
//				application.output("Indirizzo senza civico " + forms.mp_insured_details.address);
//			}
//		}
//	}
//	if (foundset.has_relative == 1 && (foundset.mp_insured_users_to_mp_insured_relatives.getSize() == 0 || (foundset.mp_insured_users_to_mp_insured_relatives.relEnabled + sumDataChanged) <= 0)) {
//		globals.DIALOGS.showErrorDialog('Error','Hai esteso la copertura assicurativa. Aggiungi almeno un familiare.', 'OK');
//		return;
//	}
	
	//TODO far vedere generato username e chiedere associazione se utente già c'è e non ha scheda assicurato
	var insured = foundset.getSelectedRecord();
	if (insured.isNew() && (!scopes.globals.mpIsEmpty(forms.mp_insurer_user_details.first_name) && !scopes.globals.mpIsEmpty(forms.mp_insurer_user_details.last_name))) {
		isRecordNew = true;
		var username;
		//insert username is null
		var first = forms.mp_insurer_user_details.first_name.toLowerCase();
		first = globals.mpTrimString(first);

		var last = forms.mp_insurer_user_details.last_name.toLowerCase();
		last = globals.mpTrimString(last);

		username = first + "." + last;

		//cerco se già esiste lo stesso username per un utente diverso
		/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
		var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
		/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_insured_users>} */
		var insuredUsers = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_insured_users')
		var insuredUsersFound = null;
		if (users.find()) {
			users.user_name = username;
			var recordCount = users.search();
		}
		if (recordCount > 0) {
			var recUser = users.getRecord(1);
			var info = 'L\'utente ' + '\'' + username + '\'' + ' esiste già';

			if (insuredUsers.find()) {
				insuredUsers.user_id = recUser.user_id;
				insuredUsersFound = insuredUsers.search();
			}

			if (insuredUsersFound > 0) {
				scopes.globals.DIALOGS.showErrorDialog('ERROR', info + ' come Assicurato, modificare l\'anagrafica.', 'OK');
				return;
			} else {
				var answer = scopes.globals.DIALOGS.showQuestionDialog('INFO', info + ' ma è sprovvisto di anagrafica.\nAssociare l\'utente esistente al nuovo assicurato?', 'SI', 'NO');
				if (answer == 'NO') {
					scopes.globals.DIALOGS.showWarningDialog('WARNING', 'Cambiare allora il nome e/o cognome dell\'assicurato, quindi riprovare.', 'OK');
					return;
				} else if (answer == 'SI') {
					scopes.globals.userDontExistInInsuredUsers = true;
					insured.user_id = users.user_id;
				}
			}
		}
		//				throw new Error('- Esiste già un utente con lo stesso username, modifica il campo');
	}
	var result = _super.saveEdits(event);

	if (result && isRecordNew) {
		scopes.globals.mpSendMailForNewInsuredUser(1,foundset.first_name,foundset.email,scopes.globals.currentUserDisplayName);
		//scopes.globals.mpSendMailForNewInsuredUser(1, foundset.first_name, scopes.globals.mpCurrentUserDisplayName);
		isRecordNew = false;
	}
	application.output(globals.mpMessageLog + 'STOP mp_insurer_details.saveEdits() ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"3794466A-56FA-4AB8-B2E6-F886CB2A959A"}
 */
function stopEditing(event) {
	_super.stopEditing(event);
	if (forms.mp_insurer_user_details.elements.fNumberCode.enabled == true) forms.mp_insurer_user_details.elements.fNumberCode.enabled = false;
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"51E385CC-8706-4440-9CA5-5A6AEB874117"}
 */
function updateUI(event) {
	_super.updateUI(event);
	elements.buttonExportExcel.enabled = isEditing() ? false : true;
	elements.username.enabled = (isEditing() && globals.hasRole('Admin')) ? true : false;
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		bundle.elements['split'].visible = !isEditing();
		if (foundset.real_name != null)
			bundle['focus'] = foundset.real_name;
		else bundle['focus'] = "NUOVO ASSICURATO";
		bundle.elements['focus'].visible = isEditing();
	}
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"6426771E-910A-4DC7-BE67-187DD8D4E395"}
 */
function onDataChange(oldValue, newValue, event) {
//	if (oldValue != newValue) forms.mp_insured_relative_list_for_hr.updateUI(event);
	return true;
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"360E766A-32C6-4624-976F-52B7024C039C"}
 */
function onShow(firstShow, event) {
	//setto la variabile a 1 --> 1 = foundset di insurer_user_details
	scopes.globals.foundsetForm = 1;
	if (!globals.isAdmin) {
		elements.is_enabled.visible = false;
		elements.lActive.visible = false;
		elements.buttonEdit.enabled = false;
		//forms.mp_insurer_list.elements.buttonAdd.enabled = false;
	}
	if (globals.isHrAdmin || globals.isAdmin) {
		elements.buttonExportExcel.visible = true;
	} else {
		elements.buttonExportExcel.visible = false;
	}
	_super.onShow(firstShow, event);
}

/**
 * Generate csv report
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"C1ED3BC6-8B69-4514-A63A-2D3F1F86594C"}
 */
function generateExcelReport(event) {
	var success = false;
	var line = '';
	var date = new Date();
	var year = date.getFullYear();
	var day = date.getDate();
	if (day < 10) {
		day = '0' + day;
	}
	application.output(globals.mpMessageLog + 'START mp_insurer_details.generateExcelReport()', LOGGINGLEVEL.INFO);
	//Nome dell'export
	var exportFileName = 'Export_anagrafica_assicurati' + '_' + creationExcelExportDate + '.xls';
	var month = date.getMonth() + 1;
	var creationExcelExportDate = year + '_' + month + '_' + day;
	var excelReportStoredProcedureName = '{call mp_exportExcelData()}';
	var exportDataset = plugins.rawSQL.executeStoredProcedure(databaseManager.getDataSourceServerName(controller.getDataSource()), excelReportStoredProcedureName, null, null, 1000);
	application.output("Dataset export excel " + exportDataset);
	application.output("Nome report : " + exportFileName);
	//	var templateDir=plugins.file.convertToJSFile("GeCo_Pratiche_Sanitarie_template");
	//	var excelTemplateName='templateExcel.xlt';
	//	var excelTemplate=plugins.file.readFile(templateDir+'\\' +excelTemplateName);
	//	var excelExport=null;
	//
	//	if(excelTemplate){
	//		var startRow=3;
	//		application.output("Foundsset tabella assicurati grande : "+foundset.getSize());
	//		excelExport=plugins.excelxport.excelExport(foundset,['first_name','last_name'],excelTemplate,'Anagrafica_assicurati',startRow);
	//		//TODO ciclare sul foundset per recuperare i dati corretti sia per assicurato che per i suoi familiari se ci son
	//		if(excelExport){
	//			plugins.file.writeFile('provaExport.xls',excelExport);
	//			//scrivo il file xls compilato
	//		}
	//		else{
	//			application.output(globals.mpMessageLog + 'mp_insurer_details.generateExcelReport() KO funzione export',LOGGINGLEVEL.DEBUG);
	//		}
	//	}
	//	else{
	//		application.output(globals.mpMessageLog + 'mp_insurer_details.generateExcelReport() KO non c\'è il template',LOGGINGLEVEL.DEBUG);
	//	}

	if (exportDataset != null) {

		var rec = exportDataset.getValue(1, 1);
		if (rec == null) {
			globals.DIALOGS.showWarningDialog('Attenzione', 'Export vuoto', 'OK');
			return
		}
		var colArray = new Array()
		for (var i = 1; i <= exportDataset.getMaxColumnIndex(); i++) {
			colArray[i - 1] = exportDataset.getColumnName(i)
			//note the -1, because an array is zero based and dataset is 1 based.
		}
		//application.output(colArray);
		line = colArray.join('\t') + '\r\n';
		for (var index = 1; index <= exportDataset.getMaxRowIndex(); index++) {
			var row = exportDataset.getRowAsArray(index);
			var rowstring = row.join('\t');
			line = line + rowstring + '\n';
		}
		success = plugins.file.writeTXTFile(exportFileName, line, 'UTF-8', 'application/vnd.ms-excel');
		if (!success) {
			application.output(globals.mpMessageLog + 'Errore creazione report', LOGGINGLEVEL.INFO);
		}
		application.output(globals.mpMessageLog + 'STOP mp_insurer_details.generateExcelReport()', LOGGINGLEVEL.INFO);
	}
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"34C79427-589F-4F6F-B716-3DCB7DCFD07A"}
 */
function startEditing(event) {
	_super.startEditing(event);
	forms.mp_insured_relative_list_for_hr.updateUI(event);

}
