/**@type {Number}
 *
 * @properties={typeid:35,uuid:"AE564DA2-DF9B-4807-A17B-C91261F73A51",variableType:8}
 */
var dossierStatus = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"0F0C633D-CD9A-4AF4-A8F0-C34B24D0BA7A",variableType:8}
 */
var insuredId = null;
/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"BD175E01-4466-46A4-8C40-E3DDA452D953"}
 */
var dossierCreationDate = null;
/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"7845A267-0972-41F0-B2CC-A8362F9AE2AF"}
 */
var dossierModificationDate = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F28BECA9-C446-4CAD-8CBD-E6A614FBFB01",variableType:8}
 */
var relativeEnabled = null;
/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A83A3C54-4D4C-4895-A10A-2297045FD0DD"}
 */
var dossierClosureDate = null;
/**@type {Number}
 * @properties={typeid:35,uuid:"DC31A068-3BE7-4B58-A9BD-423DAD46319B",variableType:8}
 */
var relativeInsured = null;
/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"48A4B12C-E435-4B9D-A6D1-430C75620F47"}
 */
var userCompanyName = null;
/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3D468DCF-4CCC-4D8A-B511-BC2CF4964CCA"}
 */
var insuranceNumber = null;
/**
 * @type {String}
 * @properties={typeid:35,uuid:"55585BAB-334A-4B79-9913-C8CDF647B434"}
 */
var dossierCreationYear = null;
/**
 * @type {String}
 * @properties={typeid:35,uuid:"72C9090D-DD30-42D0-9D6B-927C9074D443"}
 */
var insuranceCompanyName = null;
/**
 * @type {String}
 * @properties={typeid:35,uuid:"58A9A7B8-7EAC-46B6-867C-18DFCB112008"}
 */
var contractor = null;
/**
 * @type {Number}
 * @properties={typeid:35,uuid:"A2377D79-A580-4291-AA5E-9CBBC79D1F5F",variableType:8}
 */
var dossierClosed = null;
/**
 *
 * @param {String} year
 * @return {String}
 * @properties={typeid:24,uuid:"576B0966-22A3-4373-A2B9-2B6A26FCEBA9"}
 */
function searchByYear(year) {
	//formato corretto dell'anno da cercare
	var formatDB = '|yyyy-MM-dd';
	var yearToSearch = '' + year + '-01-01-' + '...' + year + '-12-31' + formatDB;
	return yearToSearch;
}
/**
 *
 * @param {String} dateToSearch
 *@return {String}
 * @properties={typeid:24,uuid:"76AF3F52-1378-472A-9948-B86AB19A3DCC"}
 */
function searchByDate(dateToSearch) {
	/** @type {String} */
	//Formato data nel DB è dd-MM-yyyy
	var formatDateDB = '|dd-MM-yyyy';
	var correctFormatDate = '#' + dateToSearch + formatDateDB;
	return correctFormatDate;
}
/**
 * Applica i filtri alla ricerca
 *
 * @author Francesco Sigolotto
 * @protected
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	if (foundset.find()) {
		//nome utente della pratica
		if (insuredId != null) {
			foundset.user_insured_id = insuredId;
			globals.mpInsuredIdSelected = insuredId;
			elements.insuredFamily.enabled = true;
		} else {
			elements.insuredFamily.enabled = false;
		}
		//verificare che il familiare sia abilitato o meno

		//familiari abilitati
		if (databaseManager.hasRecords(mp_medical_dossiers_to_mp_insured_relatives) && relativeEnabled == 1) {
			foundset.mp_medical_dossiers_to_mp_insured_relatives.is_relative_enabled = relativeEnabled;
		}
		//tutte le pratiche con familiari disabilitati
		else if (databaseManager.hasRecords(mp_medical_dossiers_to_mp_insured_relatives) && relativeEnabled == 0) {
			foundset.mp_medical_dossiers_to_mp_insured_relatives.is_relative_enabled = 0;
		} else if (databaseManager.hasRecords(mp_medical_dossiers_to_mp_insured_relatives) && (relativeEnabled == null)) {
			foundset.mp_medical_dossiers_to_mp_insured_relatives.is_relative_enabled = null;
		}

		//stato pratica
		if (dossierStatus != null) {
			foundset.dossier_status_id = dossierStatus;
		}
		//Pratica chiusa
		if (dossierClosed != null) {
			foundset.is_closed = dossierClosed;
		}
		if (dossierStatus != null) {
			foundset.dossier_status_id = dossierStatus;
		}
		//anno creazione
		if (dossierCreationYear != null) {
			created_at = searchByYear(dossierCreationYear);
		}
		//data creazione pratica
		if (dossierCreationDate != null) {

			created_at = searchByDate(dossierCreationDate);
		}
		//data ultimo aggiornamento pratica
		if (dossierModificationDate != null) {
			modified_at = searchByDate(dossierModificationDate);
		}
		//data chiusura pratica
		if (dossierClosureDate != null) {
			closed_at = searchByDate(dossierClosureDate);
		}
		//familiare
		if (relativeInsured != null) {

			foundset.relative_id = relativeInsured;
		}
		//Numero polizza
		if (insuranceNumber != null) {
			foundset.mp_medical_dossiers_to_mp_insured_users.mp_insured_users_to_mp_insurance_policies.insurance_number = insuranceNumber;
		}
		//Nome assicurazione
		if (insuranceCompanyName != null) {
			foundset.mp_medical_dossiers_to_mp_insured_users.mp_insured_users_to_mp_insurance_policies.insurance_company = insuranceCompanyName;
		}
		//Contraente
		if (contractor != null) {
			foundset.mp_medical_dossiers_to_mp_insured_users.mp_insured_users_to_mp_insurance_policies.contractor = contractor;
		}
		var result = foundset.search();
		forms.mp_dossier_list.elements.lCountPractices.text = result;
		application.output("Trovati " + result + " records");
	}
}
/**
 * Callback method for when form is shown.
 * @protected
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"215F1C18-6EFE-454B-B09C-13A995C7A4EB"}
 */
function onShow(firstShow, event) {
	if (firstShow) {
		elements.insuredFamily.enabled = false;
	}
	reloadFilterValuelist();
	applyFilter(event);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"5951C89D-F482-4226-BFCD-206321114A66"}
 * @AllowToRunInFind
 */
function resetFilter(event) {
	//resetto i campi dei filtri
	insuredId = null;
	relativeEnabled = null;
	relativeInsured = null;
	insuranceCompanyName = null
	insuranceNumber = null;
	contractor = null;
	dossierClosureDate = null;
	dossierCreationDate = null;
	dossierModificationDate = null;
	dossierStatus = null;
	dossierClosed = null;
	dossierCreationYear = null;
	applyFilter(event);
}
/**
 * Ricarica le valuelist dei filtri
 * @properties={typeid:24,uuid:"E61A0B83-8D7C-4239-9B93-9C5C2DEB5A29"}
 */
function reloadFilterValuelist() {
	plugins.rawSQL.flushAllClientsCache('pratiche_sanitarie', 'mp_dossier_creation_date');
	plugins.rawSQL.flushAllClientsCache('pratiche_sanitarie', 'mp_dossier_modification_date');
	plugins.rawSQL.flushAllClientsCache('pratiche_sanitarie', 'mp_dossier_closure_date');
	plugins.rawSQL.flushAllClientsCache('pratiche_sanitarie', 'mp_dossier_creation_year');
}
