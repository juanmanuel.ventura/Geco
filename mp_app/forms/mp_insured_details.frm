borderType:"EmptyBorder,0,0,0,0",
dataSource:"db:/pratiche_sanitarie/mp_insured_users",
extendsID:"66EC01AD-29A3-44BB-91B3-948D80DD8A6B",
items:[
{
formIndex:36,
location:"294,336",
name:"lMobileNumber",
size:"83,20",
text:"Cellulare *",
transparent:true,
typeid:7,
uuid:"0616BB37-05DE-4E62-AE16-3AF1C3A17E57"
},
{
dataProviderID:"city",
formIndex:33,
format:"|U",
location:"392,267",
name:"fCity",
size:"150,20",
typeid:4,
uuid:"079A8CC9-C882-450F-AE4B-5E5F9FCBCDA7"
},
{
formIndex:9,
location:"15,98",
name:"lCompanyName",
size:"100,20",
text:"Azienda *",
transparent:true,
typeid:7,
uuid:"1835ADD4-CD7D-4A78-93C9-817F1CD9E277"
},
{
dataProviderID:"fiscal_code",
formIndex:23,
format:"|U",
location:"392,171",
name:"fFiscalCode",
size:"150,20",
toolTipText:"16 caratteri",
typeid:4,
uuid:"19D361F0-809F-461A-BAB1-FFE3AF91D708"
},
{
formIndex:37,
location:"294,363",
name:"lInsuranceId",
size:"83,20",
text:"N. Polizza",
transparent:true,
typeid:7,
uuid:"1CABFF27-9D4F-4A15-8C40-CE285A883AE6",
visible:false
},
{
formIndex:5,
location:"294,10",
name:"lLastName",
size:"83,20",
text:"Cognome *",
transparent:true,
typeid:7,
uuid:"2A831D88-BCF6-4B43-8215-3D7D3C2B5BC1"
},
{
dataProviderID:"country",
formIndex:35,
format:"|U",
location:"392,294",
name:"fCountry",
size:"150,20",
typeid:4,
uuid:"2E17B2EA-F97E-4771-AE70-04E1B3F88D02"
},
{
formIndex:12,
location:"294,125",
name:"lEndDate",
size:"83,20",
text:"Fine *",
transparent:true,
typeid:7,
uuid:"2EB23B03-F0DE-4850-BF45-2933034CFFA5"
},
{
formIndex:22,
location:"15,171",
name:"lBirthDate",
size:"100,20",
text:"Data di nascita *",
transparent:true,
typeid:7,
uuid:"30E5B458-3561-4977-BD4E-5A8E67DC31A8"
},
{
formIndex:30,
location:"15,294",
name:"lDistrict",
size:"100,20",
text:"Provincia *",
transparent:true,
typeid:7,
uuid:"392577D9-C28D-4E56-95E6-B12BA7394FEA"
},
{
dataProviderID:"first_name",
formIndex:6,
format:"|U",
location:"125,10",
name:"fFirstName",
size:"150,20",
typeid:4,
uuid:"3CD747CF-69C7-4841-AFDB-E2867E41F350"
},
{
formIndex:37,
location:"15,363",
name:"lOfficeNumber",
size:"100,20",
text:"Tel. ufficio",
transparent:true,
typeid:7,
uuid:"3DA63759-390F-4861-A26E-DC70A3933288"
},
{
dataProviderID:"insurance_id",
displayType:2,
editable:false,
enabled:false,
formIndex:38,
location:"392,363",
name:"fInsuranceId",
size:"151,20",
typeid:4,
uuid:"430F19C0-03C8-493F-A362-562AE28C39E3",
valuelistID:"9993907E-7E6D-43C1-A4F8-609C8ED68984",
visible:false
},
{
formIndex:8,
location:"15,37",
name:"lEmail",
size:"100,20",
text:"Email *",
transparent:true,
typeid:7,
uuid:"44134235-9637-4581-956E-9DE632C4B414"
},
{
dataProviderID:"office_number",
formIndex:38,
format:"|#",
location:"125,363",
name:"fOfficeNumber",
size:"150,20",
typeid:4,
uuid:"449384EC-98C5-429B-8B11-D8E1A684B15C"
},
{
formIndex:4,
location:"15,10",
name:"lFirstName",
size:"100,20",
text:"Nome *",
transparent:true,
typeid:7,
uuid:"45FA95FB-3B99-4340-8406-87F157061417"
},
{
formIndex:32,
location:"294,267",
name:"lCity",
size:"83,20",
text:"Città",
transparent:true,
typeid:7,
uuid:"48E1C2CE-3DE6-4334-8C49-F522021562BD"
},
{
dataProviderID:"last_name",
formIndex:7,
format:"|U",
location:"392,10",
name:"fLastName",
size:"150,20",
typeid:4,
uuid:"4BA9C38C-2C25-4E97-AD54-EBB6CC00B5FF"
},
{
formIndex:41,
location:"392,240",
size:"64,20",
text:"Civico",
transparent:true,
typeid:7,
uuid:"51B30C9D-E4EB-4DB4-9EBB-2A94B6F5EA69"
},
{
dataProviderID:"address",
formIndex:27,
format:"|U",
location:"125,240",
name:"fAddress",
size:"218,20",
typeid:4,
uuid:"5E707DFE-EBED-426F-80B6-57DFEB626F45"
},
{
formIndex:20,
location:"294,170",
name:"lFiscalCode",
size:"83,20",
text:"Cod. Fiscale *",
toolTipText:"Massimo 16 caratteri",
transparent:true,
typeid:7,
uuid:"68132874-B09E-47A8-8AC5-5C4199A76E2F"
},
{
dataProviderID:"iban_code",
formIndex:38,
format:"|U",
location:"125,405",
name:"fIbanCode",
size:"218,20",
typeid:4,
uuid:"6D3BFF6A-9B3A-4984-A263-EB6D6C63A45E"
},
{
formIndex:34,
labelFor:"",
location:"294,294",
name:"lCountry",
size:"83,20",
text:"Stato",
transparent:true,
typeid:7,
uuid:"6E14F21E-8243-42B6-90E4-4069D89BD2D0"
},
{
formIndex:18,
location:"15,201",
name:"lBirthPlace",
size:"100,20",
text:"Luogo di nascita *",
transparent:true,
typeid:7,
uuid:"70CDA9A0-D693-44A2-A45F-D09CDF4BE218"
},
{
dataProviderID:"birth_date",
displayType:5,
editable:false,
formIndex:21,
format:"dd-MM-yyyy",
location:"125,170",
name:"fBirthDate",
size:"150,20",
typeid:4,
uuid:"7490AD10-67F9-4E81-AD8F-2CA604D36BAE"
},
{
formIndex:10,
location:"15,125",
name:"lStartDate",
size:"100,20",
text:"Inizio *",
transparent:true,
typeid:7,
uuid:"7A7511E9-2E2A-434B-ACE1-12888E0A27F1"
},
{
dataProviderID:"end_date",
displayType:5,
editable:false,
enabled:false,
formIndex:15,
format:"dd-MM-yyyy",
location:"392,125",
name:"fEndDate",
size:"150,20",
typeid:4,
uuid:"7B579250-AE9A-45B4-9779-405D8664519F"
},
{
formIndex:28,
location:"15,267",
name:"lZipCode",
size:"100,20",
text:"CAP",
transparent:true,
typeid:7,
uuid:"7E7E41AD-9347-4CD4-AF02-6B178F83E7C7"
},
{
formIndex:36,
location:"15,336",
name:"lHomeNumber",
size:"100,20",
text:"Tel. abitazione",
transparent:true,
typeid:7,
uuid:"85521F95-A062-4F66-B88D-FFAD49D0964E"
},
{
dataProviderID:"start_date",
displayType:5,
editable:false,
enabled:false,
formIndex:11,
format:"dd-MM-yyyy",
location:"125,125",
name:"fStartDate",
size:"150,20",
typeid:4,
uuid:"8E30948B-45D1-4563-9D96-55256DB5F77D"
},
{
dataProviderID:"district",
displayType:10,
formIndex:31,
location:"125,294",
name:"fDistrict",
size:"35,20",
typeid:4,
uuid:"9C077C54-58B1-4EA1-A6AF-B6796D267C9B",
valuelistID:"65A1562B-0591-45C3-B728-25782EDC018C"
},
{
formIndex:30,
location:"294,201",
name:"lBirthDistrict",
size:"83,20",
text:"Prov. Nascita *",
transparent:true,
typeid:7,
uuid:"9F562DF8-0852-4D55-8D2D-83A9D9B67051"
},
{
dataProviderID:"street_number",
formIndex:40,
location:"481,240",
name:"street_number",
size:"60,20",
typeid:4,
uuid:"B3428F07-2F74-4FCA-B8A0-2DD6D42878F8"
},
{
dataProviderID:"email",
formIndex:13,
location:"125,36",
name:"fEmail",
size:"218,20",
typeid:4,
uuid:"CB4F9A8B-AF0A-42E7-863D-ACCB45DAB75D"
},
{
dataProviderID:"zip_code",
formIndex:29,
format:"|#",
location:"125,267",
name:"fZipCode",
size:"57,20",
typeid:4,
uuid:"D518BE1A-E923-45BB-8F5D-6BB773EC51B9"
},
{
formIndex:26,
location:"15,240",
name:"lAddress",
size:"100,20",
text:"Indirizzo",
transparent:true,
typeid:7,
uuid:"D87669F9-19B3-44D6-9750-CF82F45151E9"
},
{
dataProviderID:"mobile_number",
formIndex:38,
format:"|#",
location:"392,336",
name:"fMobileNumber",
size:"151,20",
typeid:4,
uuid:"D9894FA1-4E6B-4DC6-BB36-C0D0B5EC20BC"
},
{
formIndex:37,
location:"15,405",
name:"lIbanCode",
size:"100,20",
text:"IBAN",
transparent:true,
typeid:7,
uuid:"DAA2DFF6-DE9C-4C10-BC28-335DE2F183FC"
},
{
dataProviderID:"company_id",
displayType:2,
editable:false,
enabled:false,
formIndex:15,
location:"125,98",
name:"fCompanyName",
size:"150,20",
typeid:4,
uuid:"DECD7357-AFDA-4C17-B854-32C37FFE5E68",
valuelistID:"F462C14D-1FFD-4A72-A14F-F69DC624E74F"
},
{
dataProviderID:"birth_district",
displayType:10,
formIndex:19,
location:"392,201",
name:"fBirthDistrict",
size:"35,20",
typeid:4,
uuid:"E42FEBC4-9EE6-4A3C-B839-FDDE26A83A7A",
valuelistID:"65A1562B-0591-45C3-B728-25782EDC018C"
},
{
height:437,
partType:5,
styleClass:"data",
typeid:19,
uuid:"EA7F1E63-268E-489D-9F6B-C9D8B1DC05DB"
},
{
dataProviderID:"home_number",
formIndex:38,
format:"|#",
location:"125,336",
name:"fHomeNumber",
size:"150,20",
typeid:4,
uuid:"EE42D89E-ADDA-4E1E-A584-6647881C93C9"
},
{
dataProviderID:"gender",
displayType:2,
editable:false,
formIndex:25,
format:"|U",
location:"500,405",
name:"fGender",
size:"42,20",
toolTipText:"Massimo 1 carattere",
typeid:4,
uuid:"EF3BF8DA-82BE-4ED3-93E3-9D75FBEB490D",
valuelistID:"FA7BB4E6-D1E9-4BD8-B36E-6A4151CF1E9B"
},
{
dataProviderID:"birth_place",
formIndex:31,
format:"|U",
location:"125,201",
name:"fBirthPlace",
size:"150,20",
typeid:4,
uuid:"EFA1B9AD-9798-4EEC-873C-D190BA4BFA66"
},
{
formIndex:39,
location:"15,71",
size:"156,20",
styleClass:"bold",
text:"Copertura Assicurativa",
transparent:true,
typeid:7,
uuid:"F4A39846-40CB-4048-9518-6EE4DA1F9882"
},
{
formIndex:24,
location:"422,405",
name:"lGender",
size:"77,20",
text:"Sesso*",
toolTipText:"Massimo 1 carattere",
transparent:true,
typeid:7,
uuid:"F898461F-33C6-483E-BE66-BF5EAB0F2497"
}
],
name:"mp_insured_details",
navigatorID:"-1",
onRecordSelectionMethodID:"-1",
size:"546,437",
styleName:"GeCo",
transparent:false,
typeid:3,
uuid:"5362BE25-C244-4A05-B809-46973C743F86"