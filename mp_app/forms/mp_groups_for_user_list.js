/**
 * @properties={typeid:35,uuid:"E66CCFEE-BD60-4DF3-BFEF-F995D0C65407",variableType:-4}
 */
var rec = null;

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"301C187E-E371-4724-8A04-3DF82E82AC8E"}
 */
function deleteRecord(event) {
	if (foundset.getSize() > 0) {

		var index = foundset.getSelectedIndex();
		rec = foundset.getRecord(index);
		if (rec.role_id == 3) {
			globals.DIALOGS.showErrorDialog('ERRORE', '- Non puoi togliere il ruolo "Assicurato" all\'utente ' + rec.mp_users_roles_to_mp_users.real_name, 'OK');
			return;
		}

		var answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai per eliminare un record.\nQuesta operazione non può essere annullata", "Elimina", "Annulla");
		if (answer == "Elimina") {
			if (rec.role_id == 2) {
				deleteInsurerEmail(rec.mp_users_roles_to_mp_users.user_email);
			}
			foundset.deleteRecord(index);
			//close in-memory transaction
			_super.stopEditing(event);
			application.output(globals.messageLog + 'STOP mp_groups_for_user_list.deleteRecord() ', LOGGINGLEVEL.DEBUG);
		}
	}
}
/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"4C4DEB50-1484-46E2-8174-6E15C33C1925"}
 */
function updateUI(event) {
	elements.buttonAdd.visible = true;
	elements.buttonAdd.enabled = isEditing() && forms.mp_users_groups_details.startEdit == 0;
	elements.buttonDelete.visible = true;
	elements.buttonDelete.enabled = !isEditing() && foundset.getSize() > 0;
}
/**
 * @param {String} emailToRemove
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"ECDEE2EC-9236-4F50-A125-3DD9279BC4F1"}
 */
function deleteInsurerEmail(emailToRemove) {
	var startPositionEmailToDelete = null;
	var endPositionMailToDelete = null;
	var emailBefore = null;
	var emailAfter = null;
	var emailToDelete = null;
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_mail_types>} */
	var mp_mail_types = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_mail_types');
	/** @type {String} */
	var insurerEmailList = null;
	if (mp_mail_types.find()) {
		//cerco il record dove ci sono gli indirizzi mail degli assicuratori che ha id=11
		mp_mail_types.mail_type_id = 11;
		var searchResult = mp_mail_types.search();
		if (searchResult > 0) {
			application.output("Risultato search : " + searchResult);
			insurerEmailList = mp_mail_types.mail_distribution_list;
			if (!globals.isEmpty(insurerEmailList)) {
				application.output("\nMail da eliminare :  " + emailToRemove);
				application.output("Lista email assicuratori " + insurerEmailList);
				startPositionEmailToDelete = insurerEmailList.search(emailToRemove);
				application.output("Posizione inizio email da cancellare : " + startPositionEmailToDelete);
				endPositionMailToDelete = insurerEmailList.indexOf(',', startPositionEmailToDelete);
				application.output("Posizione finale email da cancellare " + endPositionMailToDelete);
				//se la mail non c'è nella lista
				if (startPositionEmailToDelete == -1) {
					application.output("Nessuna mail trovata ");
					return;
				}
				//se è l'unica email nella lista
				if (startPositionEmailToDelete == 0 && endPositionMailToDelete == -1) {
					mp_mail_types.mail_distribution_list = null;
					databaseManager.recalculate(mp_mail_types);
					application.output("\nemail prima " + emailBefore + " email da cancellare " + emailToDelete);
				}
				// la mail è in testa
				if (startPositionEmailToDelete == 0) {
					application.output("Mail in testa");
					emailBefore = '';
					emailToDelete = insurerEmailList.substring(startPositionEmailToDelete, endPositionMailToDelete + 1);
					emailAfter = insurerEmailList.substring(endPositionMailToDelete + 1, insurerEmailList.length);
					application.output("\nemail prima " + emailBefore + " email da cancellare " + emailToDelete);

				}
				//la mail è in coda
				if (endPositionMailToDelete == -1) {
					application.output("Mail in coda");
					emailBefore = insurerEmailList.substring(0, startPositionEmailToDelete - 1);
					emailToDelete = insurerEmailList.substring(startPositionEmailToDelete, insurerEmailList.length);
					emailAfter = '';
					application.output("\nemail prima " + emailBefore + " email da cancellare " + emailToDelete);
				}
				//la mail è nel mezzo della lista
				if (startPositionEmailToDelete > 0 && endPositionMailToDelete != -1) {
					application.output("Mail in mezzo");
					emailBefore = emailBefore = insurerEmailList.substring(0, startPositionEmailToDelete - 1);
					emailToDelete = insurerEmailList.substring(startPositionEmailToDelete, endPositionMailToDelete);
					emailAfter = insurerEmailList.substring(endPositionMailToDelete, insurerEmailList.length);
					application.output("\nemail prima " + emailBefore + " email da cancellare " + emailToDelete);
				}
				application.output("lista : " + emailBefore + emailAfter);
				mp_mail_types.mail_distribution_list = emailBefore + emailAfter;
				databaseManager.recalculate(mp_mail_types);
				application.output("lista email aggiornata : " + mp_mail_types.mail_distribution_list);
			}
		}
	}
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"99769947-A64A-443F-A76B-0BF0D1559ED5"}
 */
function newRecord(event) {
	_super.newRecord(event);
	forms.mp_users_groups_details.startEdit = 1;
	updateUI(event);
}
