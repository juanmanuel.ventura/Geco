/**
 * @type {plugins.file.JSFile}
 * @properties={typeid:35,uuid:"9B39C390-1CA3-4D27-BBD5-2E54F8E5DF49",variableType:-4}
 */
var vJsFile = null;
/**
 * @type {String}
 * @properties={typeid:35,uuid:"E7DCD7B3-2910-4B43-94DE-2E96FD5F3FFB"}
 */
var documentPath = "";
/**
 * @type {String}
 * @properties={typeid:35,uuid:"122FCFC2-BED2-43D6-BCCA-8321E6B0ACA8"}
 */
var customTitle = "";
/**
 * Function fired onclick button.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"A72C0A0D-4A90-4E01-B054-772F3B4C436C"}
 */
function uploadDocument(event) {

	application.output(globals.mpMessageLog + 'START dossier_bills.uploadDocument() ', LOGGINGLEVEL.INFO);
	//Inutile da web client(vedi documentazione)
	var filesFilter = new Array('pdf', 'doc', 'docx', 'xls', 'jpeg', 'jpg', 'txt');
	/** @type {plugins.file.JSFile} **/
	vJsFile = plugins.file.showFileOpenDialog(1, null, false, filesFilter, uploadCallBack, 'Carica fattura');
	if (vJsFile != null) {
		documentPath = vJsFile.getAbsolutePath();
	} else {
		documentPath = null;
	}
	application.output(globals.mpMessageLog + 'STOP dossier_bills.uploadDocument() ', LOGGINGLEVEL.INFO);
}
/**Callback function
 * @param {plugins.file.JSFile[]} fileToUpload
 * @properties={typeid:24,uuid:"6FCEB897-D6F9-4326-95BF-9BD574977780"}
 */
function uploadCallBack(fileToUpload) {
	application.output(globals.mpMessageLog + 'START dossier_bills.uploadCallBack() ', LOGGINGLEVEL.INFO);
	//array contenente estensioni files validi.
	var filesFilter = new Array('application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'text/plain', 'application/vnd.ms-excel', 'image/jpeg', 'image/jpg', 'image/png');
	/** @type {plugins.file.JSFile} **/
	//fileToUpload[0] è il nome del file
	var myUploadedFile = fileToUpload[0];
	//fileToUpload[1] è il contentType del file
	var uploadedFileContentType = myUploadedFile.getContentType();
	var isIllegalContentType = true;
	//10 MB in Bytes,dimensione massima file caricabile
	var fileWeightLimit = 10485760;
	if (myUploadedFile.size() > fileWeightLimit) {
		globals.DIALOGS.showWarningDialog('Avviso Caricamento file', 'Puoi caricare file di dimensioni massime di 10 MB', 'chiudi');
		return;
	}
	for (var index = 0; index < filesFilter.length; index++) {
		var ext = filesFilter[index];
		if (uploadedFileContentType == ext) {
			isIllegalContentType = false;
		}
	}
	if (isIllegalContentType) {
		globals.DIALOGS.showWarningDialog('Estensione file non valida', 'Puoi caricare solo file con estensione pdf,doc,docx,xls,jpeg,jpg,png,txt', 'chiudi');
		return;
	}
	//cancello il file dal filesystem se voglio sostituirlo
	var vJexistingfile = plugins.file.convertToJSFile(globals.mpServerDir + '/' + foundset.getSelectedRecord().file_url);
	//application.output("Esiste il file : "+vJexistingfile.exists());
	if (vJexistingfile.exists() && foundset.getSelectedRecord().file_url != null) {
		vJexistingfile.deleteFile();
		//application.output("Ho cancellato il file da sostituire : " + isDeleted);
	}
	globals.mpServerDir = plugins.file.convertToJSFile("GeCo_pratiche_sanitarie_fatture");
	//application.output(globals.mpMessageLog + 'dir path ' + globals.mpServerDir.getAbsolutePath(), LOGGINGLEVEL.DEBUG);
	var resultDir = plugins.file.createFolder(globals.mpServerDir);
	application.output(globals.mpMessageLog + 'creata dir ' + resultDir, LOGGINGLEVEL.DEBUG);
	application.output(globals.mpMessageLog + 'dir globale ' + globals.mpServerDir, LOGGINGLEVEL.DEBUG);
	//file rinominato come nomeUtenteID_+dossierID_nome file
	customTitle = globals.mp_currentuserid_to_mp_insured_users.user_insured_id + '_' + mp_documents_to_mp_medical_dossiers.dossier_id + '_' + myUploadedFile.getName();
	//customTitle = globals.mp_currentuserid_to_mp_insured_users.user_insured_id+'_'+myUploadedFile.getName();
	application.output(globals.mpServerDir + '/' + customTitle);
	globals.mpServerfile = plugins.file.createFile(globals.mpServerDir + '/' + customTitle);
	if (!globals.mpServerfile.exists()) {
		//application.output(globals.mpMessageLog + 'file server ' + globals.mpServerfile, LOGGINGLEVEL.DEBUG);
		var result = plugins.file.writeFile(globals.mpServerfile, myUploadedFile.getBytes());
		application.output(globals.mpMessageLog + 'file scritto in locale ' + result, LOGGINGLEVEL.DEBUG);
		if (result) {
			application.output(globals.mpServerDir.getAbsolutePath());
			documentPath = globals.mpServerDir.getAbsolutePath();
			application.output(globals.mpMessageLog + 'dir path ' + documentPath, LOGGINGLEVEL.DEBUG);
			file_url = customTitle;
		}
	} else {
		globals.DIALOGS.showInfoDialog('Avviso file', 'Il file è già caricato sul server', 'chiudi');
	}
	application.output(globals.mpMessageLog + 'STOP dossier_bills.uploadCallBack() ', LOGGINGLEVEL.INFO);
}
/**
 * Perform create new record.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"F204FE55-A7B2-4998-946A-891BF07C41FD"}
 */
function createNewRecord(event) {
	//numero massimo fatture che verranno stampate
	var maxNumBills = 5;
	if (foundset.getSize() >= maxNumBills) {
		var strResult = globals.DIALOGS.showInfoDialog('Avviso limite fatture', 'ATTENZIONE Il modulo di rimborso può contenere massimo 5 fatture. Per inviare più fatture creare una nuova pratica.', 'continua', 'annulla');
		if (strResult == 'annulla') {
			return;
		}
	}
	application.output(globals.mpMessageLog + 'START dossier_bills.createNewRecord() ', LOGGINGLEVEL.INFO);
	_super.newRecord(event);
	application.output(globals.mpMessageLog + 'STOP dossier_bills.createNewRecord() ', LOGGINGLEVEL.INFO);
}
/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"E9A56634-E065-4D65-8644-44052FC936DB"}
 */
function deleteRecord(event) {
	//se ci sono record posso cancellare
	if (foundset.getSize() > 0) {
		var index = foundset.getSelectedIndex();
		var fileName = globals.mpServerDir + '/' + foundset.getRecord(index).file_url;
		var isDeleted = _super.deleteRecord(event, index);
		if (isDeleted) {
			deleteRelatedFile(fileName);
		}
	}
}
/**
 * @param{String} fileName
 *
 * @properties={typeid:24,uuid:"75F09936-6198-4797-92EC-ADD4AC6535F7"}
 */
function deleteRelatedFile(fileName) {
	var file = plugins.file.convertToJSFile(fileName);
	if (file.exists() && file != null) {
		application.output("percorso del file da cancellare " + fileName);
		application.output(globals.mpMessageLog + 'START dossier_documents.deleteRelatedFile() ', LOGGINGLEVEL.INFO);
		if (plugins.file.deleteFile(fileName)) {
			application.output("file cancellato correttamente ");
		}
	} else {
		return;
	}
	application.output(globals.mpMessageLog + 'STOP dossier_documents.deleteRelatedFile() ', LOGGINGLEVEL.INFO);

}
/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"AF9E816E-8B2E-4A20-8740-3F6BBC80E028"}
 */
function downloadDocument(event) {
	//setto in memoria la directory delle fatture
	globals.mpServerDir = plugins.file.convertToJSFile("GeCo_pratiche_sanitarie_fatture");
	var index = foundset.getSelectedIndex();
	var filePath = globals.mpServerDir + "/" + foundset.getRecord(index).file_url;
	application.output("Percorso file : " + filePath);
	var file = plugins.file.convertToJSFile(filePath);
	var fileToDownload = new Array();
	fileToDownload = file.getBytes();
	if (file.exists()) {
		//		application.output("esiste il file ? " + file.exists());
		//		application.output("il file è grande " + file.size());
		if (!plugins.file.writeFile(foundset.file_url, fileToDownload)) {
			application.output("-----------ERRORE SCRITTURA FILE SUL SERVER-----------");
		}
	} else {
		//messaggio di errore nel caso in cui non ci sia il file sul server
		globals.DIALOGS.showErrorDialog('Errore download file', 'File non trovato,ci sono problemi sul server', 'chiudi');

	}
}
/**
 * Callback method for when form is shown.
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"374FDA50-0F2B-4EF1-961F-6715C2409D73"}
 */
function onShow(firstShow, event) {
	updateUI(event);
}
/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"BB3BFA16-8779-41C1-9FA7-A22EAFFF46E9"}
 */
function updateUI(event) {
	application.output(globals.mpMessageLog + 'START dossier_bills.updateUI() ', LOGGINGLEVEL.INFO);
	//se non ho inserito fatture disabilito il pulsante elimina
	if (!foundset.getSize() > 0 && !isEditing()) {
		elements.buttonAdd.enabled = false;
		elements.buttonDelete.enabled = false;
	}
	if (globals.isInsured) {
		//Se lo stato della pratica non mi permette più di aggiungere o modificare le fatture o la pratica è chiusa
		//nascondo i pulsanti aggiungi/elimina
		if (globals.canChangeInfo == false || globals.isDossierClosed == true) {
			elements.buttonAdd.visible = false;
			elements.buttonDelete.visible = false;
			elements.buttonUpload.enabled = false;
			elements.bills_record.enabled = false;
		}
		//I pulsanti per creare/eliminare record documenti abilitati se sono assicurato mentre disabilitati se sono assicuratore
		//if (globals.isInsured) {
		elements.bills_record.transparent = !isEditing() ? true : false;
		elements.bills_record.enabled = true;
		elements.buttonAdd.visible = true;
		elements.buttonDelete.visible = true;
		elements.buttonUpload.visible = true;
		//}
		//Pratica aperta e sono in editing pulsante aggiungi record abilitato
		if (globals.isDossierClosed == false && isEditing()) {
			elements.buttonAdd.enabled = true;
			elements.buttonDelete.enabled = false;
			elements.buttonUpload.enabled = true;

		}
		//Pratica aperta e non sono in editing pulsante aggiungi record disabilitato
		if (globals.isDossierClosed == false && !isEditing()) {
			elements.buttonAdd.enabled = false;
			if (foundset.getSize() > 0) {
				elements.buttonDelete.enabled = true;
			} else {
				elements.buttonDelete.enabled = false;
			}
			elements.buttonUpload.enabled = false;
		}
	}
	if (globals.isInsurer || globals.isHrAdmin || globals.isAdmin) {
		elements.buttonAdd.visible = false;
		elements.buttonDelete.visible = false;
		elements.buttonUpload.enabled = false;
		elements.bills_record.enabled = false;
		elements.bills_record.transparent = true;
	}
	if (globals.isHrAdmin) {
		elements.buttonDownload.enabled = false;
	} else {
		elements.buttonDownload.enabled = !isEditing() ? true : false;
	}
	application.output(globals.mpMessageLog + 'STOP dossier_bills.updateUI() ', LOGGINGLEVEL.INFO);
}
/**
 * Revert all unsaved records
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"A9149689-FF9D-4569-9C76-CAD86AE56062"}
 */
function revertRecord(event) {
	var index = foundset.getSelectedIndex();
	var fileName = globals.mpServerDir + '/' + foundset.getRecord(index).file_url;
	deleteRelatedFile(fileName);
}
