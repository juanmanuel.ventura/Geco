/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"34F8A2FE-9692-4752-ADB6-A92C468F2BC4"}
 * @AllowToRunInFind
 */
 function onShow(firstShow, event) {
 	updateUI(event);
}
/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"0CE4593C-368F-4801-A1A6-94BE6E28B34A"}
 */
function updateUI(event) {
	application.output(globals.mpMessageLog + 'START dossier_guarantee.updateUI() ',LOGGINGLEVEL.INFO);
	if(globals.isInsurer || globals.isAdmin || globals.isHrAdmin){		
		elements.guarantee.enabled = false;
	}	
	//Pratica chiusa
	if(globals.isDossierClosed){
		elements.guarantee.enabled=false;	
	}	
	//La pratica è in uno stato dove le modifiche non sono più possibili
	else if(globals.isInsured){		
		if(globals.canChangeInfo && isEditing()){
			application.output('Abilito il gruppo scelta rimborso');
			elements.guarantee.enabled=true;
			application.output('Gruppo guarantee è abilitato : '+elements.guarantee.enabled);
			return; 
		}
		else{
			application.output('Sono nell\'else : '+elements.guarantee.enabled);
			elements.guarantee.enabled=false;
			application.output('Gruppo guarantee è abilitato : '+elements.guarantee.enabled);			
		}			
	}
	application.output(globals.mpMessageLog + 'STOP dossier_guarantee.updateUI() ',LOGGINGLEVEL.INFO);
}
