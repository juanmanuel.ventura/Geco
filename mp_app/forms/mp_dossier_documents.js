/**
 * @type {plugins.file.JSFile}
 * @properties={typeid:35,uuid:"5BB0CAA8-1305-44B9-B6FB-AD29197AE8AA",variableType:-4}
 */
var vJsFile = null;
/**
 * @type {String}
 * @properties={typeid:35,uuid:"AB09BA6C-4303-4498-AEA6-A2EAA33156FD"}
 */
var documentPath = "";
/**
 * @type {String}
 * @properties={typeid:35,uuid:"506C72DF-F96B-4C03-8338-4C21B799A4E0"}
 */
var customTitle = "";
/**
 * @type {String}
 * @properties={typeid:35,uuid:"6BEE25E0-C5C5-4923-A67B-87723CA377E7"}
 */
var originalTitle = "";
/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F05F9D16-4B89-422D-8BEB-61585B51AD8E",variableType:8}
 */
var idx = null;
/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"B043D681-5A5E-43DD-B3D1-9A46D13262E3"}
 * )
 */
function uploadDocument(event) {

	application.output(globals.mpMessageLog + 'START dossier_documents.uploadDocument ', LOGGINGLEVEL.INFO);
	var filesFilter = new Array('pdf', 'doc', 'docx', 'xls', 'jpeg', 'jpg', 'txt');
	/** @type {plugins.file.JSFile} **/
	vJsFile = plugins.file.showFileOpenDialog(1, null, false, filesFilter, uploadCallBack, 'Carica documenti');
	if (vJsFile != null) {
		application.output('file ' + vJsFile.getAbsolutePath());
		documentPath = vJsFile.getAbsolutePath();
		application.output(vJsFile.getName());
	} else {
		documentPath = null;
	}
	application.output(globals.mpMessageLog + 'STOP dossier_documents.uploadDocument ', LOGGINGLEVEL.INFO);
}
/**
 * @param {plugins.file.JSFile[]} fileToUpload
 * @properties={typeid:24,uuid:"C28D93F4-820A-4DA8-BBDA-172BC7AB520F"}
 */
function uploadCallBack(fileToUpload) {

	application.output(globals.mpMessageLog + 'START dossier_documents.uploadCallBack ', LOGGINGLEVEL.INFO);
	globals.mpServerDir = plugins.file.convertToJSFile("GeCo_pratiche_sanitarie_allegati");
	/** @type {plugins.file.JSFile} **/
	var myUploadedFile = fileToUpload[0];
	var isIllegalContentType = true;
	//10 MB in Bytes
	var fileWeightLimit = 10485760;
	application.output("Peso file caricato " + myUploadedFile.size());
	if (myUploadedFile.size() > fileWeightLimit) {
		globals.DIALOGS.showWarningDialog('Avviso Caricamento file', 'Puoi caricare file di dimensioni massime di 10 MB', 'chiudi');
		return;
	}
	var uploadedFileContentType = fileToUpload[0].getContentType();
	var filesFilter = new Array('application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'text/plain', 'application/vnd.ms-excel', 'image/jpeg', 'image/jpg', 'image/png');
	for (var index = 0; index < filesFilter.length; index++) {
		var ext = filesFilter[index];
		if (uploadedFileContentType == ext || uploadedFileContentType == 'text/plain') {
			isIllegalContentType = false;
		}
	}
	if (isIllegalContentType) {
		globals.DIALOGS.showWarningDialog('Estensione file non valida', 'Puoi caricare solo file con estensione pdf,doc,docx,xls,jpeg,jpg,png,txt', 'chiudi');
		return;
	}
	//cancello il file dal filesystem se voglio sostituirlo
	var vJexistingfile = plugins.file.convertToJSFile(globals.mpServerDir + '/' + foundset.getSelectedRecord().doc_url);
	//application.output("Esiste il file : "+vJexistingfile.exists());
	if (vJexistingfile.exists() && foundset.getSelectedRecord().doc_url != null) {
		var isDeleted = vJexistingfile.deleteFile();
		application.output("Ho cancellato il file da sostituire : " + isDeleted);
	}
	application.output(globals.mpMessageLog + 'dir path ' + globals.mpServerDir.getAbsolutePath(), LOGGINGLEVEL.DEBUG);
	var resultDir = plugins.file.createFolder(globals.mpServerDir);
	//nome originale file uplodato
	originalTitle = myUploadedFile.getName();
	//application.output("nome file originale "+originalTitle);
	//file rinominato come nomeUtenteID_+dossierID_nome file
	customTitle = globals.mp_currentuserid_to_mp_insured_users.user_insured_id + '_' + mp_documents_to_mp_medical_dossiers.dossier_id + '_' + myUploadedFile.getName();
	//application.output("nome file rinominato "+customTitle);
	application.output(globals.mpMessageLog + 'Percorso file ' + globals.mpServerDir + '/' + customTitle, LOGGINGLEVEL.DEBUG);
	//application.output(globals.mpServerDir+'/'+ customTitle);
	application.output(globals.mpMessageLog + 'creata dir ' + resultDir, LOGGINGLEVEL.DEBUG);
	//application.output(globals.mpMessageLog + 'dir globale ' + globals.mpServerDir, LOGGINGLEVEL.DEBUG);
	globals.mpServerfile = plugins.file.createFile(globals.mpServerDir + '/' + customTitle);
	if (!globals.mpServerfile.exists()) {
		//application.output(globals.mpMessageLog + 'file server ' + globals.mpServerfile, LOGGINGLEVEL.DEBUG);
		//verifico che non esista il file sul server
		var result = plugins.file.writeFile(globals.mpServerfile, myUploadedFile.getBytes());
		application.output(globals.mpMessageLog + 'file scritto in locale ' + result, LOGGINGLEVEL.DEBUG);
		if (result) {
			application.output(globals.mpServerDir.getAbsolutePath());
			documentPath = globals.mpServerDir.getAbsolutePath();
			application.output(globals.mpMessageLog + 'dir path ' + documentPath, LOGGINGLEVEL.DEBUG)
			//scrivo i campi sul DB
			//doc_title è il nome originale del file caricato
			doc_title = originalTitle;
			//contiene il percorso dove è salvato il file
			doc_url = customTitle;
		}
	} else {
		globals.DIALOGS.showInfoDialog('Avviso file', 'Il file è già caricato sul server', 'chiudi');
	}
	application.output(globals.mpMessageLog + 'STOP dossier_documents.uploadCallBack ', LOGGINGLEVEL.INFO);
}
/**
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"5393CBB4-F270-4498-A07C-DF3CFF5CB74F"}
 */
function createNewRecord(event) {
	application.output(globals.mpMessageLog + 'START dossier_documents.createNewRecord() ', LOGGINGLEVEL.INFO);
	idx = _super.newRecord(event);
	application.output("nuovo record dossier_documents " + idx);
	application.output(isEditing());
	updateUI(event);
	application.output(globals.mpMessageLog + 'STOP dossier_documents.createNewRecord() ', LOGGINGLEVEL.INFO);
}
/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"89AE9AE4-4BF1-4BCA-B5D5-F60422F2A6C7"}
 */
function updateUI(event) {
	application.output(globals.mpMessageLog + 'START dossier_documents.updateUI() ', LOGGINGLEVEL.INFO);
	//se non ho inserito fatture disabilito il pulsante elimina
	if (!foundset.getSize() > 0 && !isEditing()) {
		elements.buttonAdd.enabled = false;
		elements.buttonDelete.enabled = false;
	}
	if (globals.isInsurer || globals.isHrAdmin || globals.isAdmin) {
		elements.buttonAdd.visible = false;
		elements.buttonDelete.visible = false;
		elements.buttonUpload.enabled = false;
		elements.documents_record.enabled = false;
		elements.documents_record.transparent = true;
	}
	//Se lo stato della pratica non mi permette più di aggiungere o modificare le fatture o la pratica è chiusa
	//nascondo i pulsanti aggiungi/elimina
	if (globals.canChangeInfo == false || globals.isDossierClosed == true) {
		elements.buttonAdd.visible = false;
		elements.buttonDelete.visible = false;
		elements.buttonUpload.enabled = false;
		elements.documents_record.enabled = false;

	}

	//I pulsanti per creare/eliminare record documenti abilitati se sono assicurato mentre disabilitati se sono assicuratore
	if (globals.isInsured) {
		elements.documents_record.enabled = true;
		elements.buttonAdd.visible = true;
		elements.buttonDelete.visible = true;
		elements.buttonUpload.visible = true;
		elements.created_at.transparent = !isEditing() ? true : false;
		elements.documents_record.transparent = !isEditing() ? true : false;

		//Pratica aperta e sono in editing pulsante aggiungi record abilitato
		if (globals.isDossierClosed == false && isEditing()) {
			elements.buttonAdd.enabled = true;
			elements.buttonDelete.enabled = false;
			elements.buttonUpload.enabled = true;
		}
		//Pratica aperta e non sono in editing pulsante aggiungi record disabilitato
		if (globals.isDossierClosed == false && !isEditing()) {
			elements.buttonAdd.enabled = false;
			if (foundset.getSize() > 0) {
				elements.buttonDelete.enabled = true;
			} else {
				elements.buttonDelete.enabled = false;
			}
			elements.buttonUpload.enabled = false;
		}
	}
	if (globals.isHrAdmin) {
		elements.buttonDownload.enabled = false;
	} else {
		elements.buttonDownload.enabled = !isEditing() ? true : false;
	}
	application.output(globals.mpMessageLog + 'STOP dossier_documents.updateUI() ', LOGGINGLEVEL.INFO);
}
/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"A3706969-3E0C-49C1-87B8-90FA2C22ADC8"}
 * @AllowToRunInFind
 */
function onShow(firstShow, event) {

	//Carico in memoria la directory dove ci sono gli allegati
	globals.mpServerDir = plugins.file.convertToJSFile("GeCo_pratiche_sanitarie_allegati");
	application.output("Nome Bundle : " + globals.formBundleName);
	//updateUI(event);
	_super.onShow(firstShow, event);
}
/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"022C501A-AA08-4CC7-9DC8-68D4CDA9CF5D"}
 */
function deleteRecord(event) {
	//Se c'è  almeno un record cancello il record selezionato
	if (foundset.getSize() > 0) {
		var index = foundset.getSelectedIndex();
		var fileName = globals.mpServerDir + '/' + foundset.getRecord(index).doc_url;
		var isDeleted = _super.deleteRecord(event, index);
		if (isDeleted) {
			deleteRelatedFile(fileName);
		}
	}
}
/**
 * @private
 * @param{String} fileName
 * @properties={typeid:24,uuid:"A69016B8-F41E-43EE-99A7-138DC7E86EE9"}
 */
function deleteRelatedFile(fileName) {
	application.output(globals.mpMessageLog + 'START dossier_documents.deleteRelatedFile() ', LOGGINGLEVEL.INFO);
	if (plugins.file.deleteFile(fileName)) {
		application.output("file cancellato correttamente ");
	}
	application.output(globals.mpMessageLog + 'STOP dossier_documents.deleteRelatedFile() ', LOGGINGLEVEL.INFO);
}
/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"3DB4B927-B287-4062-B81B-76F0EA392FBB"}
 */
function downloadDocument(event) {
	application.output('-------------------------------------------------------------------------------------');
	application.output(globals.mpMessageLog + 'START dossier_documents.downloadDocument() ', LOGGINGLEVEL.INFO);
	var index = foundset.getSelectedIndex();
	var filePath = globals.mpServerDir + "/" + foundset.getRecord(index).doc_url;
	var file = plugins.file.convertToJSFile(filePath);
	var fileToDownload = new Array();
	fileToDownload = file.getBytes();
	if (file.exists()) {
		if (!plugins.file.writeFile(foundset.doc_title, fileToDownload)) {
			application.output("ERRORE CREAZIONE FILE DA SCARICARE");
		}
	} else {
		globals.DIALOGS.showErrorDialog('Errore download file', 'Errore nel download del file', 'chiudi');
	}
	application.output('-------------------------------------------------------------------------------------');
	application.output(globals.mpMessageLog + 'STOP dossier_documents.downloadDocument() ', LOGGINGLEVEL.INFO);
}
/**
 * Revert all unsaved records
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"111A6DDB-ECC6-47B2-8F18-5736B1E37E1B"}
 */
function revertRecord(event) {
	var index = foundset.getSelectedIndex();
	var fileName = globals.mpServerDir + '/' + foundset.getRecord(index).doc_url;
	deleteRelatedFile(fileName);
}
