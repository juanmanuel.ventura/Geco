/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"29D887CB-F8B6-48B4-9AC3-7311BFBACE8D"}
 */
function onShow(firstShow, event) {
	updateUI(event);
	return _super.onShow(firstShow, event);
}

/**
 * Handle changed data.
 *
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"EB37388B-A10B-4A64-9AF7-DAEB41C88C61"}
 */
function onDataChange(oldValue, newValue, event) {
	
	if(newValue != oldValue){		
		application.output("Ho modificato un campo dell'anagrafica "+ controller.getName());
		//foundset.mp_insured_users_to_medical_dossiers.from_insured_user = 0;
		foundset.mp_insured_users_to_mp_medical_dossiers.from_insured_user=0;
	}
	return true
}
/**
 *@param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"7EC6CC25-497E-4ED9-8F9C-37FEDCC41B4A"}
 */
function updateUI(event) {
	//campi in grigio non modificabili #dbdbdb
	//campi modificabili in bianco #f8f8f8
	
	application.output(globals.mpMessageLog + 'START dossier_insured_details.updateUI() ',LOGGINGLEVEL.INFO);
	application.output('La pratica è chiusa ? '+globals.isDossierClosed);
	application.output('Posso modificare i dati ?  '+globals.canChangeInfo);
	//Solo l'utente assicurato modifica i dati
	if(globals.isInsurer || globals.isHrAdmin){		
		elements.insuredDetails.enabled = false;
		elements.insuredDetails.bgcolor='#dbdbdb';		
	}
	if(globals.isInsured){
//		if(globals.isDossierClosed){
//			elements.insuredDetails.enabled=false;
//			
//		}
//		else if(globals.canChangeInfo== false){
//			application.output("Disabilito modifiche anagrafica familiare ");
//			elements.insuredDetails.enabled=false;
//		}
//		else{
//			elements.insuredDetails.enabled=true;
//			
//		}
		if(globals.isDossierClosed){
		elements.insuredDetails.enabled=!globals.isDossierClosed;	
		elements.insuredDetails.bgcolor='#dbdbdb';
		}
		else{
			elements.insuredDetails.bgcolor='#f8f8f8';
			elements.insuredDetails.enabled=globals.canChangeInfo;
		}
		application.output("Gruppo anagrafica dettagli assicurato abilitato?  " +elements.insuredDetails.enabled);
		application.output(globals.mpMessageLog + 'STOP dossier_insured_details.updateUI() ',LOGGINGLEVEL.INFO);	
	}
}	
