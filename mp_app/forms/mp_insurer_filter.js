/**
 * @type {Number}
 * @properties={typeid:35,uuid:"EBDBCD69-9226-4D69-9B8C-9EC9BBE066B1",variableType:8}
 */
var insuredUserId = null;

/**
 * @type {Number}
 * @protected
 * @properties={typeid:35,uuid:"5D05305C-A649-4810-B3B6-67345A95D914",variableType:4}
 */
var isLocked = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"89EF27F3-1C51-4D73-B131-4FB9011428E4",variableType:4}
 */
var insuranceId = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"905538AC-1F0D-48E4-8CA1-2721C2B1AC7B",variableType:4}
 */
var companyId = null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"3C35C494-F43F-4D26-9EFD-19134D997FA4"}
 */
function onDataChange(oldValue, newValue, event) {
	if (newValue != oldValue) {
		applyFilter(event);
	}
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"FF483109-6091-4D65-B1E4-78282A868A03"}
 */
function resetFilter(event, goTop) {
	insuredUserId = null;
	insuranceId = null;
	companyId = null;
//	isLocked = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"CE93F2C0-0C7A-4578-BD86-4354816C6322"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.user_insured_id = insuredUserId;
//		foundset.user_type_id = userType;
//		foundset.profit_center_id = profitCenter;
//		if(isLocked == 0){
//			foundset.is_locked = null;
//		}else
//		foundset.is_locked = isLocked;
		if(insuranceId != null) foundset.insurance_id = insuranceId;
		if(companyId != null) foundset.company_id = companyId;
		foundset.search();
		}
		foundset.sort("users.real_name asc");
}

/**
 * Handle changed data.
 *
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"CC9A4D89-4E64-4E6F-8CDE-3E29127F6970"}
 */
function onDataChangeEnabled(oldValue, newValue, event) {
//	isEnabled = newValue;
//	if(isEnabled == 0){
//		elements.fUser.visible = true;
//		elements.fUser.enabled = true;
//		userId = null;
//		
//	}
//	else {
//		elements.fUser.visible = false;
//		elements.fUser.enabled = false;
//		userId = null;
//	}
	return true
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"FE120B02-87C9-4302-B360-D5AC42D80E02"}
 */
function onLoad(event) {
	_super.onLoad(event);
}

/**
 * @properties={typeid:24,uuid:"9677E4F2-2FC9-4803-A55E-B2021CAC49DA"}
 */
function filterEnabledRecords() {
	_super.filterEnabledRecords();
}
