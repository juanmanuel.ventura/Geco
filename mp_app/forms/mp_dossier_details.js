/**@type {String}
 * @properties={typeid:35,uuid:"38647E1A-F027-4D15-A5DC-1D988C4CA4CA"}
 */
var addresseeName = null;
/**@type {Number}
 * @properties={typeid:35,uuid:"784FD8BD-126C-4BDE-A8DF-E70D33D46C4C",variableType:8}
 */
var mailTypeID = null;
/**@type {String}
 * @properties={typeid:35,uuid:"290CC752-D206-4210-81FF-8867EFD3E145"}
 */
var mailRecipient = null;
/**
 * @type {String}
 * @properties={typeid:35,uuid:"FA6F7A5F-17B1-4277-A4F7-A0B230AB74D4"}
 */
var DossierNextStatus = null;
/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"4C7CD2DD-E088-476D-85B2-285DCB61A23E",variableType:-4}
 */
var canDownloadDossierPDF = null;
/**
 * @type {String}
 * @properties={typeid:35,uuid:"D3CB711C-17C3-4AFC-8DE4-8387949A2E01"}
 */
var dossierStatusErrorMessage = null;
/**
 * @type {String}
 * @properties={typeid:35,uuid:"CF125426-CD5A-450B-9E0D-9A01948AF52B"}
 */
var dossierNoteForMail = null;

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"4B1DE8A7-B8FE-473C-8080-1361C545995D",variableType:-4}
 */
var isIBANinsert = true;
/**
 * Callback method for when form is shown.
 * @param {Boolean} firstShow  form is shown first time after load
 * @param {JSEvent} event 	  the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"0C35A37F-1662-4893-93EC-2D20051BE19E"}
 * @AllowToRunInFind
 */
function onShow(firstShow, event) {

	DossierNextStatus = null;
	globals.mpValidationExceptionMessage = "Salvataggio errato della Pratica ";
	if (foundset.getSelectedRecord() && foundset.getSelectedRecord().isNew()) {
		elements.insuredDossierStatus.enabled = false;
		elements.insurerDossierStatus.enabled = false;
		elements.insuredDossierStatus.bgcolor = '#dbdbdb';
		elements.insurerDossierStatus.bgcolor = '#dbdbdb';
		elements.buttonPDF.visible = false;
		//Setto il primo tab se la pratica è appena stata creata
		elements.tabs.tabIndex = 1;
		//foundset.mobile_number = scopes.globals.mp_currentuserid_to_mp_insured_users.mobile_number;
		foundset.user_insured_id = scopes.globals.mp_currentuserid_to_mp_insured_users.user_insured_id;
		//foundset.address = scopes.globals.mp_currentuserid_to_mp_insured_users.address;
		//creo il record dove selezionare le voci del rimborso
		foundset.mp_medical_dossiers_to_mp_guarantees_required.newRecord(false);
	}
	updateUI(event);
}
/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"4A0341DE-11D8-4550-BB78-6B3F3892E1D9"}
 */
function stopEditingDossiers(event) {

	isIBANinsert = true;
	application.output(globals.mpMessageLog + 'START dossier_details.stopEditingDossiers() ', LOGGINGLEVEL.INFO);
	if (foundset.getSelectedRecord().isNew()) {
		//revert dei documenti e fatture caricate se la pratica è nuova e voglio eliminarla
		if (databaseManager.hasNewRecords(mp_medical_dossiers_to_mp_documents)) {
			str = globals.DIALOGS.showInfoDialog('Avviso cancellazione documento', 'Il documento caricato in precedenza non verrà salvato continuare ?', 'Sì', 'No');
			if (str == 'Sì') {
				forms.mp_dossier_documents.revertRecord(event);
			} else {
				return;
			}
		}
		if (databaseManager.hasNewRecords(mp_medical_dossiers_to_mp_bills)) {
			var str = globals.DIALOGS.showInfoDialog('Avviso cancellazione fattura', 'Il documento caricato in precedenza non verrà salvato continuare ?', 'Sì', 'No');
			if (str == 'Sì') {
				forms.mp_dossier_bills.revertRecord(event);
			} else {
				return;
			}
		}
		_super.stopEditing(event);
		globals.mpShowDetailsList(event, 'mp_dossier_insured_bundle', 'mp_dossier_insured_filter_list', false);
	}
	//revert records documenti e fatture
	var billsRecord = forms.mp_dossier_bills.foundset.getSelectedRecord();
	var documentsRecord = forms.mp_dossier_documents.foundset.getSelectedRecord();
	//revert records fatture e documenti se non ho caricato files
	if (databaseManager.hasRecords(foundset.mp_medical_dossiers_to_mp_bills)) {
		if (billsRecord.file_url != null && billsRecord.isNew()) {
			//application.output('BILL URL :'+billsRecord);
			str = globals.DIALOGS.showInfoDialog('Avviso cancellazione fattura', 'Il documento caricato in precedenza non verrà salvato continuare ?', 'Sì', 'No');
			if (str == 'Sì') {
				forms.mp_dossier_bills.revertRecord(event);
			} else {
				return;
			}
		}
	}
	if (databaseManager.hasNewRecords(forms.mp_dossier_documents.foundset)) {
		if (documentsRecord.doc_url != null && documentsRecord.isNew()) {
			//application.output('Document URL :'+documentsRecord.doc_url);
			str = globals.DIALOGS.showInfoDialog('Avviso cancellazione documento', 'Il documento caricato in precedenza non verrà salvato continuare ?', 'Sì', 'No');
			if (str == 'Sì') {
				forms.mp_dossier_documents.revertRecord(event);
			} else {
				return;
			}
		}
	}
	_super.stopEditing(event);
	application.output(globals.mpMessageLog + 'STOP dossier_details.stopEditingDossiers() ', LOGGINGLEVEL.INFO);
}
/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"561B4582-CB5C-4687-9465-DFDC1BB18B81"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
	application.output(globals.mpMessageLog + 'START dossier_details.saveEdits() ', LOGGINGLEVEL.INFO);
	//Se il record è nuovo setto alcuni campi obbligatori
	if (foundset.getSelectedRecord().isNew()) {
		foundset.configuration_id = 1;
		foundset.dossier_type_id = 1;
		foundset.dossier_status_id = 1;
		foundset.mobile_number = mp_currentuserid_to_mp_insured_users.mobile_number;
		foundset.is_closed = 0;
		//controllo che nell'anagrafica dell'assicurato ci sia l'IBAN e se c'è setto il campo iban nella tabella medical_dossiers
		if (foundset.mp_medical_dossiers_to_mp_insured_users.iban_code != null && globals.isInsured) {
			foundset.iban = scopes.globals.mp_currentuserid_to_mp_insured_users.iban_code;
		}
		//se non ho inserito l'IBAN durante la creazione dell'anagrafica devo settarlo adesso.
		else {

			if (globals.mpIsEmpty(foundset.mp_medical_dossiers_to_mp_insured_users.iban_code) && globals.isInsured) {
				isIBANinsert = false;
				updateUI(event);
				globals.DIALOGS.showErrorDialog('IBAN', '- Codice IBAN non inserito', 'chiudi');
				return;
			}
		}
		//Salvo correttamente il beneficiario del rimborso
		if (foundset.is_for_relative == 0) {
			//Il beneficiario è l'assicurato stesso
			foundset.dossier_beneficiary_name = foundset.mp_medical_dossiers_to_mp_insured_users.real_name;
		}
		if (foundset.is_for_relative == 1) {
			//Il beneficiario è il familiare selezionato
			foundset.dossier_beneficiary_name = foundset.mp_medical_dossiers_to_mp_insured_relatives.real_name;
		}
		globals.canChangeInfo = true;
	}
	//Se la pratica non è nuova
	else {
		if (globals.mpIsEmpty(foundset.mp_medical_dossiers_to_mp_insured_users.iban_code)) {

			//if (globals.mpIsEmpty(scopes.globals.mp_currentuserid_to_mp_insured_users.iban_code)) {
			isIBANinsert = false;
			updateUI(event);
			globals.DIALOGS.showErrorDialog('IBAN', '- Codice IBAN non inserito', 'chiudi');
			return;
		}

		//Setto i valori prevStatus e actualStatus della pratica solo se impostato un nuovo stato valido per la pratica
		if (globals.isDossierStatusUpdated == true) {
			application.output("stato della pratica prima dell'aggiornamento : " + dossier_status_id);
			//salvo lo stato precedente
			foundset.mp_medical_dossiers_to_mp_status_notes.prev_status_id = globals.prevStatusNote;
			//aggiorno lo stato attuale
			foundset.dossier_status_id = globals.nextStatusNote;
			application.output("stato della pratica dopo l'aggiornamento : " + dossier_status_id);
			//invio la mail
			sendMailAfterChangingDossierStatus();
			globals.isDossierStatusUpdated = false;
		}
		//se modifico i dati del dossier setto il campo a 0
		from_insured_user = 0;
		//Se la pratica è in uno stato finale la chiudo,ovvero è in solo lettura come storico
		//(Cancellata = 9,Respinta = 8,Liquidata = 7)
		if ( (dossier_status_id == 7) || (dossier_status_id == 8) || (dossier_status_id == 9)) {
			application.output("Sto per chiudere la pratica");
			foundset.is_closed = 1;
			globals.isDossierClosed = true;
			globals.canChangeInfo = false;
			//Data chiusura pratica
			foundset.closed_at = new Date();
			//ID Utente che ha chiuso la pratica (assicurato o assicuratore)
			foundset.closed_by = scopes.globals.mp_currentuserid_to_mp_users.user_id;
			application.output("Pratica chiusa : " + foundset.is_closed + ' globals.isDossierClosed : ' + globals.isDossierClosed);
		}
		//Se la pratica è negli stati Validata(3),Inviata(5),In Lavorazione(6) disabilito l'inserimento di fatture e documenti
		//e non è più possibile modificare dati anagrafici o la form dossier_guarantee
		if ( (dossier_status_id == 3) || (dossier_status_id == 5) || (dossier_status_id == 6)) {
			globals.canChangeInfo = false;
		} else {
			globals.canChangeInfo = true;
		}
	}
	isIBANinsert = true;
	_super.saveEdits(event);

	application.output(globals.mpMessageLog + 'STOP dossier_details.saveEdits() ', LOGGINGLEVEL.INFO);
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"AA98D93A-893D-4052-B827-38D8A595765A"}
 */
function goToDossierList(event) {
	var formName = "";
	application.output(scopes.globals.mpMessageLog + ' dossier_details.goToDossierList bundle ' + scopes.globals.formBundleName);
	application.output("form bundle : " + scopes.globals.formBundleName);
	if (scopes.globals.formBundleName.equals("mp_dossier_bundle")) {
		scopes.globals.cleanData = false;
		formName = 'mp_dossier_filter_list';
	}
	if (scopes.globals.formBundleName.equals("mp_dossier_insured_bundle")) {
		formName = 'mp_dossier_insured_filter_list';
	}
	scopes.globals.mpShowDetailsList(event, scopes.globals.formBundleName, formName, false);
}
/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"F1DB2209-011F-443C-92A5-6528997578A0"}
 */
function startEditing(event) {
	_super.startEditing(event);
}
/**
 * @param event
 * @properties={typeid:24,uuid:"E03E41E7-9CE1-498D-8DD1-6C1D65CA1618"}
 */
function updateUI(event) {
	DossierNextStatus = null;
	application.output(globals.mpMessageLog + 'START dossier_details.updateUI() ', LOGGINGLEVEL.INFO);
	//Se la pratica è chiusa disabilito tutti i pulsanti di modifica pratica ed eliminazione dei documenti o fatture
	if (is_closed == 1) {
		globals.isDossierClosed = true;
		globals.canChangeInfo = false;
	}
	if (is_closed == 0) {
		globals.isDossierClosed = false;
		globals.canChangeInfo = true;
		elements.buttonEdit.visible = true;
	}
	if (dossier_status_id == 1 || dossier_status_id == 2 || dossier_status_id == 4) {
		globals.canChangeInfo = true;
	} else {
		globals.canChangeInfo = false;
	}
	if (globals.isInsured) {

		application.output("Sono un assicurato");
		application.output("Pratica modificabile ? " + globals.canChangeInfo + " chiusa ? " + globals.isDossierClosed);
		elements.buttonPDF.visible = true;
		application.output("Download pdf abilitato ? " + elements.buttonPDF.enabled);
		if (!foundset.getSelectedRecord().isNew()) {
			elements.buttonPDF.visible = true;
			elements.insuredDossierStatus.bgcolor = '#ffffff';
			elements.insurerDossierStatus.bgcolor = '#ffffff';

		} else {
			elements.insuredDossierStatus.bgcolor = '#dbdbdb';
			elements.insurerDossierStatus.bgcolor = '#dbdbdb';
			elements.buttonPDF.visible = false;
			elements.insuredDossierStatus.enabled = false;
			
		}
		if (isIBANinsert && !isEditing()) {
			elements.buttonPDF.enabled = true;
		} else {
			elements.buttonPDF.enabled = false;
		}
		if (!globals.isDossierClosed) {
			elements.insurerDossierStatus.visible = false;
			elements.insuredDossierStatus.visible = true;
			elements.dossierNextStatusLabel.visible = true;
			//Se sono in editing
			if (isEditing() && globals.canChangeInfo) {
				elements.insuredDossierStatus.enabled = true;
				elements.insuredDossierStatus.bgcolor = '#ffffff';				

			}
			else{
				elements.insuredDossierStatus.bgcolor = '#dbdbdb';
				elements.insuredDossierStatus.enabled = false;
			}
			
			_super.updateUI(event);
		}
		//Pratica chiusa
		else {
			elements.buttonPDF.visible = false;
			elements.buttonEdit.visible = false;
			elements.buttonCancel.visible = false;
			elements.buttonSave.visible = false;
			elements.insurerDossierStatus.visible = false;
			elements.insuredDossierStatus.visible = false;
			elements.dossierNextStatusLabel.visible = false;
		}
		if (foundset.getSelectedRecord().isNew()) {
			elements.buttonPDF.visible = false;
			elements.insuredDossierStatus.bgcolor = '#dbdbdb';
			elements.insuredDossierStatus.enabled = false;
		} 			
	}
	//Se sono assicuratore posso solo cambiare lo stato della pratica
	if (globals.isInsurer) {

		application.output("Sono un assicuratore");
		if (!globals.isDossierClosed) {
			elements.insurerDossierStatus.visible = true;
			elements.dossierNextStatusLabel.visible = true;
			elements.insuredDossierStatus.visible = false;
			//Se sono in editing
			if (isEditing()) {
				elements.buttonCancel.visible = true;
				elements.buttonSave.visible = true;
				elements.buttonEdit.visible = false;
				elements.insurerDossierStatus.enabled = true;

			}
			_super.updateUI(event);
		} else {
			elements.buttonEdit.visible = false;
			elements.buttonCancel.visible = false;
			elements.buttonSave.visible = false;
			elements.insurerDossierStatus.visible = false;
			elements.insuredDossierStatus.visible = false;
			elements.dossierNextStatusLabel.visible = false;
		}
	}
	//Se sono HR Admin
	if (globals.isHrAdmin) {
		application.output("Sono un HR admin!!!");
		elements.buttonCancel.visible = false;
		elements.buttonSave.visible = false;
		elements.buttonEdit.visible = false;
		elements.insurerDossierStatus.visible = false;
		elements.insuredDossierStatus.visible = false;
		elements.dossierNextStatusLabel.visible = false;
	}
	if (globals.isHrAdmin && globals.isInsured) {
		application.output("Sono un assicurato");
		if (!foundset.getSelectedRecord().isNew()) {
			elements.buttonPDF.visible = true;
			elements.insuredDossierStatus.bgcolor = '#ffffff';
			elements.insurerDossierStatus.bgcolor = '#ffffff';

		} else {
			elements.insuredDossierStatus.bgcolor = '#dbdbdb';
			elements.insurerDossierStatus.bgcolor = '#dbdbdb';
			elements.buttonPDF.visible = false;
		}
		//Pratica chiusa
		if (!globals.isDossierClosed) {
			elements.insurerDossierStatus.visible = false;
			elements.insuredDossierStatus.visible = true;
			elements.dossierNextStatusLabel.visible = true;
			//Se sono in editing
			if (isEditing() && globals.canChangeInfo) {
				elements.buttonCancel.visible = true;
				elements.buttonSave.visible = true;
				elements.buttonEdit.visible = false;
				elements.insuredDossierStatus.enabled = true;
			}
			_super.updateUI(event);
		} else {
			elements.buttonEdit.visible = false;
			elements.buttonCancel.visible = false;
			elements.buttonSave.visible = false;
			elements.insurerDossierStatus.visible = false;
			elements.insuredDossierStatus.visible = false;
			elements.dossierNextStatusLabel.visible = false;
		}
//		if (!foundset.getSelectedRecord().isNew()) {
//			elements.buttonPDF.visible = true;
//		} else {
//			elements.buttonPDF.visible = false;
//		}
		
		
	}
	//	//Se sono Admin
	if (globals.isAdmin) {
		application.output("Sono un Admin!!!");
		elements.buttonCancel.visible = false;
		elements.buttonSave.visible = false;
		elements.buttonEdit.visible = false;
		elements.insurerDossierStatus.visible = false;
		elements.insuredDossierStatus.visible = false;
		elements.dossierNextStatusLabel.visible = false;
	}
	application.output("Pratica chiusa : " + globals.isDossierClosed);
	application.output("Pratica modificabile : " + globals.canChangeInfo);
	application.output(globals.mpMessageLog + 'STOP dossier_details.updateUI() ', LOGGINGLEVEL.INFO);
}
/**
 * Funzione popup per la scelta della nota da inserire
 * @author Francesco Sigolotto
 * @param oldValue old value
 * @param newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"501B52D1-629C-4F74-8446-AFF69F5988D3"}
 * @AllowToRunInFind
 */
function showNotesChooserPopup(oldValue, newValue, event) {
	dossierStatusErrorMessage = 'Impossibile richiedere l\'avanzamento della pratica da ' + globals.convertDossierStatusIdToString(dossier_status_id) + ' a ' + globals.convertDossierStatusIdToString(newValue);
	if ( (newValue != oldValue) && (DossierNextStatus != null)) {
		application.output(globals.mpMessageLog + 'STARt dossier_details.validateNextStatusDossier()', LOGGINGLEVEL.INFO);
		//stato attuale è in dossier_id quello futuro l'ho scelto dalla combobox
		var status = validateNextStatusDossier(dossier_status_id, newValue);
		application.output(globals.mpMessageLog + 'STOP dossier_details.validateNextStatusDossier()', LOGGINGLEVEL.INFO);
		application.output("valore ritornato della funzione validateNextStatusDossier() : " + status);
		if (status) {
			globals.isDossierStatusUpdated = true;
			//salvo la stato precedente della pratica
			globals.prevStatusNote = dossier_status_id;
			//salvo lo stato aggiornato della pratica
			globals.nextStatusNote = newValue;
			//application.output("Stato pratica precedente : " + globals.prevStatusNote);
			//application.output("Stato pratica aggiornato : " + globals.nextStatusNote);
			//Popup per la creazione delle note
			var notePopup = application.createWindow("Popup Nota ", JSWindow.MODAL_DIALOG);
			notePopup.setInitialBounds(1000, 1000, 350, 100);
			notePopup.title = 'Creo la nota?';
			notePopup.show(forms.mp_dossier_notes_popup);
			forms.mp_dossier_notes_popup.isToCreateNote = 0;
			//reset stato avanzamento pratica
			DossierNextStatus = null;
		}
		//stato futuro non valido
		if (!status) {
			//Popup in cui avviso l'utente che ha tentato di impostare uno stato della pratica non valido
			globals.DIALOGS.showErrorDialog("Errore", dossierStatusErrorMessage, "chiudi");
		}
	}
	updateUI(event);
	return true;
}
/**
 * Controlla che lo stato futuro della pratica sia raggiungibile dallo stato attuale della pratica
 * @param {Number} currentDossierStatus stato corrente pratica
 * @param {Number} nextDossierStatus stato desiderato della pratica
 * @return {Boolean} validationResult
 * @properties={typeid:24,uuid:"5F46263D-CB45-4D34-A1D3-AAB67662036A"}
 */
function validateNextStatusDossier(currentDossierStatus, nextDossierStatus) {
	application.output(globals.mpMessageLog + 'START dossier_details.validateNextStatusDossier()', LOGGINGLEVEL.INFO);
	application.output("Stato corrente pratica :" + currentDossierStatus);
	application.output("Stato futuro pratica :" + nextDossierStatus);
	switch (currentDossierStatus) {
	//lo stato pratica attuale è nuova e l'assicurato può scegliere tutti i suoi valori;
	case 1:
		switch (nextDossierStatus) {
		// se lo stato futuro è Da Validare(solo assicurato può assengnarlo)
		case 2:
			return true;
			break;
		// se lo stato futuro è Inviata (solo assicurato può assengnarlo)
		case 5:
			return true;
			break;
		// se lo stato futuro è Cancellata (solo assicurato può assengnarlo)
		case 9:
			return true;
			break;
		// altri casi(quelli dell'assicuratore) la validazione non passa
		default:
			return false;
			break;
		}
		//validationResult=validateNextStatus(nextDossierStatus);
		break;
	// lo stato attuale è Da validare
	case 2:
		switch (nextDossierStatus) {
		//stato futuro è Not OK(solo assicuratore può)
		case 4:
			return true;
			break;
		//stato futuro è Validata(solo assicuratore può)
		case 3:
			return true;
			break;
		//stato futuro è Cancellata(solo assicurato può)
		case 9:
			return true;
			break;
		default:
			//altri stati non raggiungibili
			return false;
			break;
		}
	//lo stato attuale è Validata
	case 3:
		switch (nextDossierStatus) {
		//se lo stato futuro è Da Validare
		case 2:
			return true;
		// se lo stato futuro è Inviata (solo assicurato può assengnarlo)
		case 5:
			return true;

		// se lo stato futuro è Cancellata (solo assicurato può assengnarlo)
		case 9:
			return true;

		default:
			//altri stati non raggiungibili
			return false;

		}
	//stato attuale Not OK
	case 4:
		switch (nextDossierStatus) {
		// se lo stato futuro è Da validare (solo assicurato può assengnarlo)
		case 2:
			return true;
			break;
		//		//se lo stato futuro è Inviata (solo assicurato può assegnarlo)
		//		case 5:
		//			return true;
		//			break;
		// se lo stato futuro è Cancellata (solo assicurato può assengnarlo)
		case 9:
			return true;
			break;
		default:
			//altri stati non raggiungibili
			return false;
			break;
		}
	//stato attuale Inviata
	case 5:
		switch (nextDossierStatus) {
		//stato futuro in lavorazione(solo assicuratore può)
		case 6:
			return true;
			break;
		default:
			//altri stati non raggiungibili
			return false;
			break;
		}
	//stato attuale in Lavorazione
	case 6:
		switch (nextDossierStatus) {
		//se stato futuro è Liquidata (solo assicuratore può)
		case 7:
			return true;
			break;
		//se stato futuro è Respinta (solo assicuratore può)
		case 8:
			return true;
			break;
		default:
			//altri stati non raggiungibili
			return false;
			break;
		}
	default:
		//lo stato attuale della pratica è in una degli stati finali (Respinta,cancellata o liquidata)
		return false;
		break;
	}
}
/**
 * Funzione che viene lanciata per inoltre mail di aggiornamento STATO PRATICA
 * @private
 * @properties={typeid:24,uuid:"071644DB-D618-44E8-8F53-A7DA9CFE1833"}
 */
function sendMailAfterChangingDossierStatus() {
	//recupero la stringa associato all'ID dello stato della pratica per lo stato attuale e per quello precedente
	var prevStatusAsString = globals.convertDossierStatusIdToString(globals.prevStatusNote);
	var actualStatusAsString = globals.convertDossierStatusIdToString(globals.nextStatusNote);
	//mando la mail
	application.output(globals.mpMessageLog + 'START mp_dossier_details.sendMailAfterChangingDossierStatus()', LOGGINGLEVEL.INFO);
	//se sono l'assicuratore mando la mail al "proprietario" della pratica
	if (globals.isInsurer) {
		mailTypeID = 10;
		
		//nome destinatario
		addresseeName = foundset.mp_medical_dossiers_to_mp_insured_users.first_name;
		//email destinatario
		mailRecipient = foundset.mp_medical_dossiers_to_mp_insured_users.email;
	}
	//se sono assicurato mando la mail all'assicuratore
	if (globals.isInsured) {
		mailTypeID = 11;		
		//email destinatario
		//le mail degli assicuratori la recupero dalla tabella mp_mail_types nella funzione sendMailForChangedDossierStatus
		mailRecipient = '';
	}
	application.output("Mail type " + mailTypeID + " destinario " + addresseeName + " stato prev pratica " + prevStatusAsString + " stato attuale pratica " + actualStatusAsString);
	try {
		
		//var isMailSent = globals.sendMailForChangedDossierStatus(mailTypeID,addresseeName, mailRecipient, prevStatusAsString, actualStatusAsString);
		var isMailSent = globals.sendMailForChangedDossierStatus(mailTypeID, addresseeName,mailRecipient,prevStatusAsString, actualStatusAsString);
		
	} catch (e) {
		
		e.message = "invio mail " + isMailSent;
	}
	application.output(globals.mpMessageLog + 'STOP mp_dossier_details.sendMailAfterChangingDossierStatus()', LOGGINGLEVEL.INFO);
}
/**
 * Perform the element default action.
 *Genera il pdf precompilato
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"A6D46027-1C01-4093-8207-D061C172A139"}
 */
function generatePDF(event) {
	application.output(globals.mpMessageLog + 'START dossier_details.generatePDF()', LOGGINGLEVEL.INFO);
	var templateDir = plugins.file.convertToJSFile("GeCo_Pratiche_Sanitarie_template");
	var pdfTemplateName = 'dossierTemplate.pdf';
	var pdf_form = plugins.file.readFile(templateDir + '//' + pdfTemplateName);
	if (pdf_form) {
		var field_values = fillingDataIntoPDF();
		var result = plugins.pdf_output.convertPDFFormToPDFDocument(pdf_form, field_values);
		if (result == null) {
			application.output('Errore nella creazione pdf');
		} else {
			application.output("OK creazione pdf");
			plugins.file.writeFile('modulo_rimborso.pdf', result);
		}
	} else {
		application.output(globals.mpMessageLog + 'mp_dossier_details.generatePDF() KO non c\'è il template', LOGGINGLEVEL.DEBUG);
	}
	application.output(globals.mpMessageLog + 'STOP dossier_details.generatePDF()', LOGGINGLEVEL.INFO);
}
/**
 * Scrivo sul pdf i campi con i dati corretti
 * @return {Array} field_values
 * @properties={typeid:24,uuid:"E4058DB1-4C02-4BC4-B53B-921A2DABE834"}
 * @AllowToRunInFind
 */
function fillingDataIntoPDF() {
	var field_values = new Array();
	var insuredGenderField = null;
	var relativeGenderField = null;
	field_values.push('CognomeAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.last_name);
	field_values.push('NomeAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.first_name);
	//per mette la spunta su un campo del pdf creato 'campodaSettare'=Sì;
	if (foundset.mp_medical_dossiers_to_mp_insured_users.gender && foundset.mp_medical_dossiers_to_mp_insured_users.gender.equals('M')) {
		insuredGenderField = 'SessoMAssicurato';
	} else {
		insuredGenderField = 'SessoFAssicurato';
	}
	field_values.push('LuogoNascitaAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.birth_place);
	var formattedInsuredBirthDate = plugins.it2be_tools.dateFormat(foundset.mp_medical_dossiers_to_mp_insured_users.birth_date, 'dd-MM-yyyy');
	field_values.push('DataNascitaAssicurato=' + formattedInsuredBirthDate);
	field_values.push(insuredGenderField + '=Sì');
	field_values.push('CodiceFiscaleAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.fiscal_code);
	if (foundset.mp_medical_dossiers_to_mp_insured_users.address != null) {
		field_values.push('IndirizzoAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.address);
	}
	//Scrivo il numero civico se c'è
	if (foundset.mp_medical_dossiers_to_mp_insured_users.street_number != null) {
		field_values.push('CivicoAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.street_number);
	}
	field_values.push('CAPAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.zip_code);
	field_values.push('ProvinciaAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.district);
	//Scrivo il numero tel casa se c'è
	if (foundset.mp_medical_dossiers_to_mp_insured_users.office_number != null) {
		field_values.push('TelCasaAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.home_number);
	}
	field_values.push('TelCellAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.mobile_number);
	//Scrivo il numero tel ufficio se c'è
	if (foundset.mp_medical_dossiers_to_mp_insured_users.office_number) {
		field_values.push('TelUfficioAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.office_number);
	}
	field_values.push('EMailAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.email);
	field_values.push('AziendaAppartenenzaAssicurato=' + foundset.mp_medical_dossiers_to_mp_insured_users.mp_insured_users_to_mp_companies.company_name);
	//Se la pratica è per un familiare compilo la sezione persona per la quale si chiede il rimborso con i dati del familiare
	if (foundset.is_for_relative == 1) {
		field_values.push('CognomeFamiliare=' + foundset.mp_medical_dossiers_to_mp_insured_relatives.last_name);
		field_values.push('NomeFamiliare=' + foundset.mp_medical_dossiers_to_mp_insured_relatives.first_name);
		field_values.push('LuogoNascitaFamiliare=' + foundset.mp_medical_dossiers_to_mp_insured_relatives.birth_place);
		var formattedRelativeBirthDate = plugins.it2be_tools.dateFormat(foundset.mp_medical_dossiers_to_mp_insured_relatives.birth_date, 'dd-MM-yyyy');
		field_values.push('DataNascitaFamiliare=' + formattedRelativeBirthDate);
		if (foundset.mp_medical_dossiers_to_mp_insured_relatives.gender.equals('M')) {
			relativeGenderField = 'SessoMfamiliare';
		} else {
			relativeGenderField = 'SessoFfamiliare';
		}
		field_values.push(relativeGenderField + '=Sì');
		field_values.push('CodiceFiscaleFamiliare=' + foundset.mp_medical_dossiers_to_mp_insured_relatives.fiscal_code);
		if (foundset.mp_medical_dossiers_to_mp_insured_relatives.relative_type_id == 1) {
			field_values.push('CompNucleoFam=Sì');
		}
		if (foundset.mp_medical_dossiers_to_mp_insured_relatives.relative_type_id == 2) {
			field_values.push('NoCompNucleoFam=Sì');
		}
	}
	//Se pratica è per assistito,compilo la sezione persona per la quale si chiede il rimborso con i dati dell'assistito stesso
	if (foundset.is_for_relative == 0) {
		field_values.push('Dipendente=Sì');
		field_values.push('CognomeFamiliare=' + foundset.mp_medical_dossiers_to_mp_insured_users.last_name);
		field_values.push('NomeFamiliare=' + foundset.mp_medical_dossiers_to_mp_insured_users.first_name);
		field_values.push('LuogoNascitaFamiliare=' + foundset.mp_medical_dossiers_to_mp_insured_users.birth_place);
		//formatto la data di nascita del familiare
		formattedRelativeBirthDate = plugins.it2be_tools.dateFormat(foundset.mp_medical_dossiers_to_mp_insured_users.birth_date, 'dd-MM-yyyy');
		field_values.push('DataNascitaFamiliare=' + formattedRelativeBirthDate);
		if (foundset.mp_medical_dossiers_to_mp_insured_users.gender.equals('M')) {
			relativeGenderField = 'SessoMfamiliare';
		} else {
			relativeGenderField = 'SessoFfamiliare';
		}
		field_values.push(relativeGenderField + '=Sì');
		field_values.push('CodiceFiscaleFamiliare=' + foundset.mp_medical_dossiers_to_mp_insured_users.fiscal_code);
	}
	//Compilo la sezione coordinate bancarie
	var ibanCompositionObj = extractIBANfileds(foundset.mp_medical_dossiers_to_mp_insured_users.iban_code);
	field_values.push('TitolareIBAN=Sì');
	field_values.push('PaeseIBAN1=' + ibanCompositionObj.internationalCode[0]);
	field_values.push('PaeseIBAN2=' + ibanCompositionObj.internationalCode[1]);
	field_values.push('CIN_Euro1=' + ibanCompositionObj.checkNumbers[0]);
	field_values.push('CIN_Euro2=' + ibanCompositionObj.checkNumbers[1]);
	field_values.push('CIN_Ita=' + ibanCompositionObj.cin);
	//scrivo carattere per carattere ABI,CAB e CC italiani
	if (ibanCompositionObj.abi.length <= 5) {
		for (var index = 0; index < ibanCompositionObj.abi.length; index++) {
			field_values.push('ABI' + (index + 1) + '=' + ibanCompositionObj.abi[index]);
		}
	}
	if (ibanCompositionObj.cab.length <= 5) {
		for (var idx = 0; idx < ibanCompositionObj.abi.length; idx++) {
			field_values.push('CAB' + (idx + 1) + '=' + ibanCompositionObj.cab[idx]);
		}
	}
	application.output("Lunghezza cc :" + ibanCompositionObj.cab.length);
	if (ibanCompositionObj.currentAccount.length <= 12) {
		for (var j = 0; j < ibanCompositionObj.currentAccount.length; j++) {
			field_values.push('CC' + (j + 1) + '=' + ibanCompositionObj.currentAccount[j]);
		}
	}
	//Compilazione garanzia di rimborso
	var guaranteeChecked = checkingGuaranteeRequired(foundset.mp_medical_dossiers_to_mp_guarantees_required);
	for (var i = 0; i < guaranteeChecked.length; i++) {
		switch (guaranteeChecked[i]) {
		case 'health_service_with_operation':
			field_values.push('interventoChirurgico=Sì');
			break;
		case 'health_service_no_operation':
			field_values.push('noInterventoChirurgico=Sì');
			break;
		case 'health_service_sight':
			field_values.push('correzioneVista=Sì');
			break;
		case 'helper_transport_board':
			field_values.push('vitto=Sì');
			break;
		case 'helper_transport_ambulance':
			field_values.push('trasportoAmbulanza=Sì');
			break;
		case 'helper_transport_abroad':
			field_values.push('trasportoEstero=Sì');
			break;
		case 'helper_transport_body':
			field_values.push('rimpatrioSalma=Sì');
			break;
		case 'advance':
			field_values.push('anticipo=Sì');
			break;
		case 'benefit_replacement':
			field_values.push('indennità=Sì');
			break;
		case 'extra_hospital_terapy':
			field_values.push('prestazioniDiagnostiche=Sì');
			break;
		case 'extra_hospital_prosthesis':
			field_values.push('protesi=Sì');
			break;
		case 'medical_exams':
			field_values.push('visitaSpecialistica=Sì');
			break;
		case 'dental_care':
			field_values.push('cureDentarie=Sì');
			break;
		case 'lens':
			field_values.push('lenti=Sì');
			break;
		default:
			break;
		}
	}
	if (foundset.mp_medical_dossiers_to_mp_bills.getSize() > 0) {
		var billsToWriteInPDF = [];
		billsToWriteInPDF = fillBillsPDFSection();
		var hospitalizationDate = null;
		application.output("Grandezz array da scrivere : " + billsToWriteInPDF.length);
		for (var ix = 0; ix < billsToWriteInPDF.length; ix++) {
			field_values.push('NumFat' + (ix + 1) + '=' + billsToWriteInPDF[ix].index);
			field_values.push('DataFattura' + (ix + 1) + '=' + billsToWriteInPDF[ix].creationDate);
			if (billsToWriteInPDF[ix].earner != null) {
				application.output("Percettore da stampare : " + billsToWriteInPDF[ix].earner);
				field_values.push('Percettore' + (ix + 1) + '=' + billsToWriteInPDF[ix].earner);
			}
			field_values.push('Importo' + (ix + 1) + '=' + billsToWriteInPDF[ix].amount);
			if (hospitalizationDate == null) {
				hospitalizationDate = billsToWriteInPDF[ix].hospitalizationDate;
			} else {
				hospitalizationDate = hospitalizationDate + ';  ' + billsToWriteInPDF[ix].hospitalizationDate;
			}
			field_values.push('RicoveroDel=' + hospitalizationDate);
		}
	}
	field_values.push('DataCreazionePDF=' + generatePDFCreationDate());
	return field_values;
}
/**Funzione per inserire la data di compilazione del PDF
 * @private
 * @return {String}
 * @properties={typeid:24,uuid:"6A130196-D521-45AA-97BC-5FE1A27F184B"}
 */
function generatePDFCreationDate() {
	var creationPDFDate = new Date();
	var year = creationPDFDate.getFullYear();
	var month = creationPDFDate.getMonth() + 1;
	var day = creationPDFDate.getDate();
	if (day < 10) {
		day = '0' + day;
	}
	return (day + '-' + month + '-' + year);
}
/**
 * Estrae i campi che compongono l'IBAN
 *@param{String} ibanCode
 *@return {Object} ibanCompositionObjects
 * @properties={typeid:24,uuid:"3289FCF7-5E36-4A17-8148-051066A5FF33"}
 */
function extractIBANfileds(ibanCode) {
	//l'iban italiano è composto da 27 caratteri (es : IT 02 L 12345 12345 123456789012)così formato:
	//info http://it.wikipedia.org/wiki/Coordinate_bancarie
	//sigla internazionale(2 lettere maiuscole es: IT),
	//numeri di controllo (2 numeri  es: 02),
	//CIN (1 carattere maiuscolo es : L),
	//ABI (5 cifre es: 12345),
	//CAB (5 cifre es :12345);
	//Numero di conto corrente (12 caratteri alfanumerici eventualmente preceduti da zeri nel caso il numero di caratteri sia inferiore a 12 es :123456789012)
	var ibanCompositionObject = {
		internationalCode: null,
		checkNumbers: null,
		cin: null,
		abi: null,
		cab: null,
		currentAccount: null
	}
	ibanCompositionObject.internationalCode = ibanCode.slice(0, 2);
	ibanCompositionObject.checkNumbers = ibanCode.slice(2, 4);
	ibanCompositionObject.cin = ibanCode[4];
	ibanCompositionObject.abi = ibanCode.slice(5, 10);
	ibanCompositionObject.cab = ibanCode.slice(10, 15);
	ibanCompositionObject.currentAccount = ibanCode.slice(15, 27);
	//	application.output("IBAN : " + ibanCode + ' grande :' + iban.length);
	//	application.output('Codice internazionale : ' + ibanCompositionObject.internationalCode + ' grande : ');
	//	application.output('Codice di controllo : ' + ibanCompositionObject.checkNumbers);
	//	application.output('CIN : ' + ibanCompositionObject.cin);
	//	application.output('Codice ABI : ' + ibanCompositionObject.abi);
	//	application.output('Codice CAB : ' + ibanCompositionObject.cab);
	//	application.output('Conto corrente : ' + ibanCompositionObject.currentAccount);
	return ibanCompositionObject;
}
/**
 *Controllo quali voci di rimborso sono state segnate
 * @param {JSFoundSet} _foundset
 *@return {Array} fieldsChecked
 * @properties={typeid:24,uuid:"21BD79E1-F0CC-4AEE-8B3F-26E5B76384F3"}
 */
function checkingGuaranteeRequired(_foundset) {
	var fieldsChecked = new Array();
	for (var index = 0; index < _foundset.alldataproviders.length; index++) {
		if (_foundset.getDataProviderValue(_foundset.alldataproviders[index]) == 1 && _foundset.alldataproviders[index] != 'dossier_id') {
			fieldsChecked.push(_foundset.alldataproviders[index].toString());
		}
	}
	return fieldsChecked;
}
/**
 *@return {Array<Object>} billsObj
 * @properties={typeid:24,uuid:"36CCF63F-62BC-4B5A-94D7-325F3F9661FA"}
 */
function fillBillsPDFSection() {
	var length = null;
	var billsRecords = [];
	var billObj = {
		index: null,
		billDate: null,
		earner: null,
		amount: null,
		hospitalizationDate: null
	};
	if (foundset.mp_medical_dossiers_to_mp_bills.getSize() > 5) {
		length = 5;
	} else {
		length = foundset.mp_medical_dossiers_to_mp_bills.getSize();
	}
	for (var index = 1; index <= length; index++) {
		var record = foundset.mp_medical_dossiers_to_mp_bills.getRecord(index);
		application.output('Record ' + record);
		billObj.index = index;
		billObj.billDate = plugins.it2be_tools.dateFormat(record.bill_date, 'dd-MM-yyyy');
		billObj.amount = record.amount;
		billObj.earner = record.earner;
		billObj.hospitalizationDate = plugins.it2be_tools.dateFormat(record.hospitalization_date, 'dd-MM-yyyy');
		billsRecords.push({ index: billObj.index, creationDate: billObj.billDate, amount: billObj.amount, earner: billObj.earner, hospitalizationDate: billObj.hospitalizationDate });
		application.output(billsRecords[index - 1]);
	}
	return billsRecords;
}
/**
 * @return {Boolean}
 * @properties={typeid:24,uuid:"EF1DD427-967B-49C4-B9B7-6B6DB539996B"}
 */
function checkIBAN() {

	if (scopes.globals.mpIsEmpty(iban)) {
		isIBANinsert = false;
		var errorMessage = '- Codice IBAN non inserito';
		throw new Error(errorMessage);
	}
	isIBANinsert = true;
}