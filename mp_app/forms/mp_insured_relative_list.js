/**
 * @param event
 * @properties={typeid:24,uuid:"AC7DE2BE-DF20-4A02-BEA8-286D602F5F05"}
 */
function updateUI(event) {
	_super.updateUI(event);
	application.output(globals.mpMessageLog + 'START mp_insured_relative_list.updateUI()', LOGGINGLEVEL.INFO);
	//visibilità bottone Add
	if (foundset.mp_insured_relatives_to_mp_insured_users == null) {
		var rec = null;
		if (scopes.globals.foundsetForm == 0) {
			rec = forms.mp_insured_details.foundset.getSelectedRecord();
		} else if (scopes.globals.foundsetForm == 1) {
			rec = forms.mp_insurer_user_details.foundset.getSelectedRecord();
		}
		elements.buttonAdd.enabled = isEditing() && (rec.has_relative == 1);
	} else elements.buttonAdd.enabled = isEditing() && foundset.mp_insured_relatives_to_mp_insured_users.has_relative == 1;

	application.output(globals.mpMessageLog + 'mp_insured_relative_list.updateUI() scopes.globals.foundsetForm: ' + scopes.globals.foundsetForm, LOGGINGLEVEL.INFO);
	application.output(globals.mpMessageLog + 'mp_insured_relative_list.updateUI() INSURED_RELATIVE_LIST buttonAdd.enabled: ' + elements.buttonAdd.enabled, LOGGINGLEVEL.INFO);
	elements.relative_group.transparent = !isEditing() ? true : false;
	application.output(globals.mpMessageLog + 'STOP mp_insured_relative_list.updateUI()', LOGGINGLEVEL.INFO);
	
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C5CCDED1-F295-4637-B070-BCDF6D8FC4E5"}
 * @AllowToRunInFind
 */
function newRecord(event) {
	_super.newRecord(event);
	application.output(globals.mpMessageLog + 'START mp_insured_relative_list.newRecord()', LOGGINGLEVEL.INFO);
	var newRecToCreate = foundset.getSelectedRecord();
	if (newRecToCreate.isNew()) {
		newRecToCreate.relative_number = foundset.relNumber + 1;
		newRecToCreate.is_relative_enabled = 1;
	}
	application.output(globals.mpMessageLog + 'STOP mp_insured_relative_list.newRecord() relative_number del nuovo record in creazione: ' + newRecToCreate.relative_number, LOGGINGLEVEL.INFO);
}
///**
// * Disable Relative.
// *
// * @param {JSEvent} event the event that triggered the action
// * @param {Number} index
// *
// * @private
// *
// * @properties={typeid:24,uuid:"A955B1AE-FBF2-447F-AF1A-715F6417577A"}
// */
//function disableRelative(event, index) {
//	application.output(globals.messageLog + 'START mp_insured_relative_list.disableRelative() ', LOGGINGLEVEL.INFO);
//	var answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai per disabilitare un familiare.\nL\'utente disabilitato non sarà più visualizzabile nell\'elenco dei familiari.\nQuesta operazione non può essere annullata", "Disabilita", "Annulla");
////	var answer = 'Disabilita';
//	if (answer == "Disabilita") {
//		_super.startEditing(event);
//		var recSelected = foundset.getSelectedRecord();
//		recSelected.is_relative_enabled = 0;
//		application.output('mp_recSelected.is_relative_enabled: ' + recSelected.is_relative_enabled);
//		_super.saveEdits(event);
//		_super.stopEditing(event);
//	}
//	application.output(globals.messageLog + 'STOP mp_insured_relative_list.deleteRecord() ', LOGGINGLEVEL.INFO);
//}
///**
// * Perform the element default action.
// *
// * @param {JSEvent} event the event that triggered the action
// * @param {Number} index
// *
// * @private
// *
// * @properties={typeid:24,uuid:"92812FEA-A24F-4AD8-9F93-73C67E71673F"}
// */
//function deleteRecord(event, index) {
//	_super.deleteRecord(event, index);
//}
