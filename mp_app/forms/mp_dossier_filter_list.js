
/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formFilterName
 * @param {String} formListName
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"72E3BA1E-9535-4E9F-BBC6-C73F8C1E1243"}
 */
function onLoad(event, formFilterName, formListName) {
	return _super.onLoad(event, formFilterName, formListName);
}
