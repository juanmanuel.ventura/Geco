/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"AF5F11DA-330C-4ED1-8E1C-26FD3389FA34"}
 */
function updateUI(event){
	var rec = foundset.getSelectedRecord();
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle && bundle.elements['split']) {
		bundle.elements['split'].visible = !isEditing();
		if (rec && rec.isNew()) bundle['focus'] = 'NUOVO UTENTE';
		else if (rec && !rec.isNew())  bundle['focus'] = rec.real_name;
		bundle.elements['focus'].visible = isEditing();
	}
}
