/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"AE6CCD93-5E9D-4134-9333-EA6F970BC355"}
 */
var user_name = "";
/**
 * @type {RuntimeLabel}
 * @properties={typeid:35,uuid:"90D0B09F-5239-4C06-B1EB-3206CA3B75FF",variableType:-4}
 */
var label;
/**
 * @type {String}
 * @properties={typeid:35,uuid:"661A5AF3-D791-4ED4-9FBA-D01DCD3C8AD0"}
 */
var sortDirection = 'asc';

/**
 * Perform the element default action.
 * 
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"D0B9BD54-1E44-41E9-95EB-3D1AFEBE8418"}
 */
function goToDetails(event) {
	
	globals.mpShowDetailsList(event,"mp_dossier_bundle","mp_dossier_details",true);
}
 /** Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
**
 * @properties={typeid:24,uuid:"982D8529-DF45-4C42-9A25-385E39347227"}
 */
function createNewRecord(event) {	
		var idx = _super.newRecord(event);
		application.output("nuovo record pratica " + idx);
		globals.mpShowDetailsList(event,'mp_dossier_bundle','mp_dossier_details',true);
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"6F19F61B-D623-4789-8F3E-280CBB59789D"}
 */
function onShow(firstShow, event) {
	updateUI(event);
//	foundset.loadAllRecords();
	_super.onShow(firstShow, event);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"2D2BE307-70E0-4984-9DE4-8647192EBC98"}
 */
function sort(event) {
	// change color back to the original one
	if (label) label.fgcolor = '#727784';
	// (re) set the triggered label
	label = event.getSource();
	// (re) change color to indicate selection
	label.fgcolor = '#990000';
	var sortKey = label.text;

	switch (sortKey) {
	case 'Contraente':
		controller.sort('medical_dossiers_to_insured_users.insured_users_to_companies.company_name' + ' ' + sortDirection);
		break;
	case 'Assicurato':
		controller.sort('medical_dossiers_to_insured_users.insured_users_to_users.real_name' + ' ' + sortDirection);
		break;
	case 'Assistito':
		controller.sort('medical_dossiers_to_insured_relatives.real_name' + ' ' + sortDirection);
		break;
	case 'Stato':
		controller.sort('dossier_status_id' + ' ' + sortDirection);
		break;
	case 'Data creazione':
		controller.sort('created_at' + ' ' + sortDirection);
		break;
	case 'Ultimo aggiornamento':
		controller.sort('modified_at' + ' ' + sortDirection);
		break;
	}

	// toggle the sort order
	sortDirection = (sortDirection == 'asc') ? 'desc' : 'asc';
}
/**
 * @param {JSEvent} event
 *@protected 
 * @properties={typeid:24,uuid:"F41CFDAA-DD22-4C29-AF61-55BEB27EE83B"}
 */
function updateUI(event) {
	//aggiornamento del beneficiario della pratica
//	for (var index = 1; index <= foundset.getSize(); index++) {
//		var record = foundset.getRecord(index);		
//		if(record.is_for_relative==1){
//			record.dossier_beneficiary_name=record.mp_medical_dossiers_to_mp_insured_relatives.real_name;
//		}
//		if(record.is_for_relative==0){
//			record.dossier_beneficiary_name=record.mp_medical_dossiers_to_mp_insured_users.real_name;
//		}
//		//application.output("record "+index + 'nome beneficiario '+record.dossier_beneficiary_name);
//	}
}