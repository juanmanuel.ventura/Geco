/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"66C7757B-FF9C-4862-BE22-D7951BB939E8"}
 * @AllowToRunInFind
 */
function updateUI(event) {
	application.output(globals.mpMessageLog + 'START dossier_insured_relatives.updateUI() ',LOGGINGLEVEL.INFO);
	
	//la pratica è per un familiare
	if(is_for_relative){
		application.output('La pratica è per un familiare');
		elements.insuredName.visible = false;
		elements.insuredRelative.visible = true;
		elements.relativeInfo.visible = true;
	}
	//Se la pratica non è per un familiare e non sono in editing ,nascondo i dati anagrafici del familiare
//	if((!is_for_relative)&&(!isEditing())){
//		application.output('La pratica NON è per un familiare');
//		elements.group_relative.visible = false;
//		elements.insuredName.visible = true;
//		elements.insuredRelative.visible =false;		
//	}
	//se la pratica non è per un familiare
	if(!is_for_relative){
		elements.relativeInfo.visible = false;
		elements.insuredName.visible = true;
		elements.insuredRelative.visible =false;		
	}
	//Solo l'utente assicurato modifica i dati
	if(globals.isInsurer || globals.isAdmin || globals.isHrAdmin){		
		elements.relativeInfo.enabled = false;
		elements.relativeInfo.bgcolor='#dbdbdb';
		elements.insuredName.bgcolor='#dbdbdb';
		elements.insuredRelative.bgcolor='#dbdbdb';
		return;
	}
	if(globals.isDossierClosed){
		elements.relativeInfo.enabled=false;
		elements.relativeInfo.bgcolor='#dbdbdb';
		elements.insuredName.bgcolor='#dbdbdb';
		elements.insuredRelative.bgcolor='#dbdbdb';
		return;
	}
	application.output("Posso modificare i dati ?(canChangeInfo) : "+globals.canChangeInfo);
	//La pratica è in uno stato dove le modifiche non sono più possibili
	if(globals.canChangeInfo == false && globals.isInsured){
		elements.relativeInfo.enabled=false;
		elements.relativeInfo.bgcolor='#dbdbdb';
		elements.insuredName.bgcolor='#dbdbdb';
		elements.insuredRelative.bgcolor='#dbdbdb';
	}
	else{
		elements.relativeInfo.enabled=true;	
		elements.relativeInfo.bgcolor='#ffffff';
		elements.insuredName.bgcolor='#ffffff';
		elements.insuredRelative.bgcolor='#ffffff';

	}	
	application.output("STOP dossier_insured_relatives.updateUI()");
}
/**
 * Callback method for when form is shown.
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"D2732C0A-C48D-461A-A99E-958E9837A916"}
 * @AllowToRunInFind
 */
function onShow(firstShow, event) {
		
	if(foundset.is_for_relative == 1){
		//carico i dati corretti del familiare
		foundset.mp_medical_dossiers_to_mp_insured_relatives.selectRecord(relative_id);
	}
	_super.onShow(firstShow, event)
}
