borderType:"SpecialMatteBorder,0.0,0.0,1.0,0.0,#000000,#000000,#cacaca,#000000,0.0,",
dataSource:"db:/pratiche_sanitarie/mp_medical_dossiers",
extendsID:"1EA6575E-5ED3-4FB8-85CD-05DA39570746",
items:[
{
location:"692,43",
size:"142,20",
text:"Data apertura",
transparent:true,
typeid:7,
uuid:"26CD1A7F-75B5-47A4-ABC5-73807A8B020A"
},
{
location:"350,76",
size:"120,20",
text:"Stato pratica",
transparent:true,
typeid:7,
uuid:"3CBC4BA2-9773-49BA-A698-53B066E3C3C6"
},
{
dataProviderID:"contractor",
displayType:2,
editable:false,
formIndex:12,
format:"|U",
location:"145,76",
onDataChangeMethodID:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081",
size:"157,20",
typeid:4,
uuid:"474A8BF9-9527-40BC-9F27-BC0F00FB900C",
valuelistID:"C67C59B3-153B-4CBA-A8B3-D02E942AD689"
},
{
dataProviderID:"relativeEnabled",
displayType:2,
editable:false,
formIndex:21,
location:"145,108",
onDataChangeMethodID:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081",
size:"157,20",
typeid:4,
uuid:"4E0CACD1-CA7A-4AA4-98EA-2E43E0899070",
valuelistID:"1CAF458A-0EC3-46A4-82E2-5AC19F0D2426"
},
{
dataProviderID:"dossierClosureDate",
displayType:2,
editable:false,
location:"842,108",
name:"dossierClosureDate",
onDataChangeMethodID:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081",
size:"157,20",
typeid:4,
uuid:"55FBF4E7-80FB-4768-A255-1E051134DB8C",
valuelistID:"18878DE0-9439-4022-9DF8-16D97675511D"
},
{
extendsID:"97C8E3BE-3DCD-4394-AB5A-7F5D55D09D37",
height:138,
typeid:19,
uuid:"5603FFE3-8985-4DB5-872F-2418EE03B734"
},
{
dataProviderID:"insuranceNumber",
displayType:2,
editable:false,
formIndex:16,
location:"500,43",
onActionMethodID:"-1",
onDataChangeMethodID:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081",
size:"157,20",
typeid:4,
uuid:"677E633A-BBA5-45D0-B472-F81DE05A3FF8",
valuelistID:"B32CC4C4-6664-4C3D-B255-650B8A32B607"
},
{
formIndex:20,
location:"5,108",
size:"117,20",
text:"Familiari abilitati",
transparent:true,
typeid:7,
uuid:"6DE53823-1961-41AF-8FF8-8BFB3D13161B"
},
{
dataProviderID:"insuredId",
displayType:10,
location:"145,9",
name:"insuredId",
onActionMethodID:"-1",
onDataChangeMethodID:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081",
onFocusGainedMethodID:"-1",
size:"157,20",
typeid:4,
uuid:"81FF1323-047E-4F3A-9EBA-90F5DBF3D2CF",
valuelistID:"98E987F7-E9AC-433F-B76D-E3DE51F8B32A"
},
{
dataProviderID:"dossierStatus",
displayType:2,
editable:false,
location:"500,76",
name:"dossierStatus",
onDataChangeMethodID:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081",
size:"157,20",
typeid:4,
uuid:"87FDFC8C-F2DD-4DC7-930E-5829448C8705",
valuelistID:"ED1765C8-B87C-4E2E-9738-E72F4ACAA87A"
},
{
location:"692,108",
size:"142,20",
text:"Data Chiusura",
transparent:true,
typeid:7,
uuid:"8C8A0670-0FAC-42D5-92B4-B09B49149E39"
},
{
anchors:3,
customProperties:"methods:{
onActionMethodID:{
arguments:[
null,
\"true\"
]
}
}",
formIndex:19,
imageMediaID:"B432FC13-6422-4A74-956D-4474A7CD5FF0",
location:"1158,108",
name:"resetFilter",
onActionMethodID:"5951C89D-F482-4226-BFCD-206321114A66",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
rolloverImageMediaID:"B2C0D6C5-8456-4477-BFD5-825161DD2756",
showClick:false,
showFocus:false,
size:"20,20",
typeid:7,
uuid:"91E1C44C-603B-4C47-AB83-527C22A48664"
},
{
anchors:3,
borderType:"LineBorder,1,#000000",
formIndex:28,
items:[
{
containsFormID:"19E05192-C922-446B-8811-4C1A5F8CC158",
location:"1038,42",
text:"legendArea",
typeid:15,
uuid:"44BFDDD2-060A-4C50-875A-9E8A55725CE9"
}
],
location:"1158,9",
name:"legendTabless",
printable:false,
size:"150,86",
tabOrientation:-1,
transparent:true,
typeid:16,
uuid:"96685DB4-63B6-4B25-802A-0CED0B9BAEBF"
},
{
dataProviderID:"relativeInsured",
displayType:2,
editable:false,
location:"500,9",
name:"insuredFamily",
onDataChangeMethodID:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081",
size:"157,20",
typeid:4,
uuid:"9C290130-1AA3-4E6E-AE99-0974F82459B3",
valuelistID:"C6517AB6-8D06-4B6A-BEF7-66620A02E24E"
},
{
dataProviderID:"dossierClosed",
displayType:2,
editable:false,
formIndex:10,
location:"500,108",
onDataChangeMethodID:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081",
size:"157,20",
text:"Chiusa",
typeid:4,
uuid:"A439458E-FB26-4912-AAC2-3B5F546CA3EF",
valuelistID:"C0B10AED-A684-44A3-BA75-F8DDE20526FC"
},
{
location:"692,76",
size:"142,20",
text:"Ultimo aggiornamento",
transparent:true,
typeid:7,
uuid:"B67AC5AE-4F87-42D3-947A-4323B2372790"
},
{
location:"350,9",
size:"120,20",
text:"Familiare",
transparent:true,
typeid:7,
uuid:"B70BED0B-525F-4D58-919A-940E98A9BB1B"
},
{
dataProviderID:"dossierCreationYear",
displayType:2,
editable:false,
formIndex:17,
location:"842,9",
onDataChangeMethodID:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081",
size:"157,20",
typeid:4,
uuid:"BA6025C6-3B40-43DE-B267-D0E027E1C9C2",
valuelistID:"3D613407-E396-4023-891E-4575566E2034"
},
{
location:"5,9",
size:"117,20",
text:"Assicurato",
transparent:true,
typeid:7,
uuid:"CC52C3BC-DF40-408E-95B6-DAF8858517E9"
},
{
dataProviderID:"dossierCreationDate",
displayType:2,
editable:false,
location:"842,43",
name:"dossierCreationDate",
onDataChangeMethodID:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081",
size:"157,20",
typeid:4,
uuid:"DC0071AF-5266-49EC-A60E-C9387B0A34E2",
valuelistID:"A4236CB0-411E-4E39-A0C7-B3EE33D70994"
},
{
formIndex:22,
location:"350,108",
size:"120,20",
text:"Aperta/Chiusa",
transparent:true,
typeid:7,
uuid:"E661B248-E7FD-4910-B0B7-6B3799F766B5"
},
{
formIndex:18,
location:"692,9",
size:"142,20",
text:"Anno creazione",
transparent:true,
typeid:7,
uuid:"ED1856A3-D9FC-489D-A613-A0084A3027DF"
},
{
formIndex:11,
location:"5,76",
size:"117,20",
text:"Azienda contraente",
transparent:true,
typeid:7,
uuid:"EDD412D6-70C3-44D9-A75C-EBE2C9AD5FB2"
},
{
dataProviderID:"dossierModificationDate",
displayType:2,
editable:false,
location:"842,76",
name:"dossierModificationDate",
onDataChangeMethodID:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081",
size:"157,20",
typeid:4,
uuid:"EECFA9A0-0438-4EA4-95D8-DF7DBF459143",
valuelistID:"E75298BE-448B-4DE3-9792-1891325DE3B2"
},
{
formIndex:13,
location:"5,42",
size:"117,20",
text:"Assicurazione",
transparent:true,
typeid:7,
uuid:"F3B61715-6493-4855-897C-B4714D711E34"
},
{
formIndex:15,
location:"350,43",
size:"120,20",
text:"N. Polizza",
transparent:true,
typeid:7,
uuid:"F6C0BC78-D4A2-4CD5-BB19-281655ADB8F4"
},
{
dataProviderID:"insuranceCompanyName",
displayType:2,
editable:false,
formIndex:14,
location:"145,43",
onDataChangeMethodID:"4BC895CB-A390-4EDD-A7EB-7162FD0FB081",
size:"157,20",
transparent:true,
typeid:4,
uuid:"F9A5666D-EFAA-4C52-B7EA-D643D6570764",
valuelistID:"8088F73B-38B5-4FBD-9BFD-3ED878D3066F"
}
],
name:"mp_dossier_filter",
onDeleteRecordCmdMethodID:"-1",
onFindCmdMethodID:"-1",
onLoadMethodID:"-1",
onNewRecordCmdMethodID:"-1",
onRecordEditStopMethodID:"-1",
onRecordSelectionMethodID:"-1",
onSearchCmdMethodID:"-1",
onShowAllRecordsCmdMethodID:"-1",
onShowMethodID:"215F1C18-6EFE-454B-B09C-13A995C7A4EB",
size:"1440,138",
typeid:3,
uuid:"124E3389-1072-40AE-A069-C13A2FCEEE2B"