/**
 * @type {String}
 * @properties={typeid:35,uuid:"ABF17D1F-84AD-41CA-AAD0-CDEC9A2BE310"}
 */
var note= null;
/**
 * Save the note into DB field.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"D6236EC5-ED16-433E-9BD9-2B42C85816FB"}
 */
function saveNote(event) {	
		//salvo la nota scritta dall'utente
		if(note!=null){
		status_note = note;	
		globals.dossierNote=note;
		application.closeAllWindows();
		}
		else{
			globals.DIALOGS.showWarningDialog('Nota non inserita','Inserisci una nota, oppure annulla','chiudi');
		}		
}
/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"C252EB95-DD9F-423C-82FE-48B1FE8D3EAF"}
 * @AllowToRunInFind
 */
function onShow(firstShow, event) {
	//reset testo nota
	note = null;
	application.output(globals.mpMessageLog + 'START dossier_addNote_popup.onShow() ',LOGGINGLEVEL.INFO);
	//creo la nuova nota
	_super.newRecord(event);
	//salvo i nuovi stati della pratica
	prev_status_id=globals.prevStatusNote;
	actual_status_id=globals.nextStatusNote;
//	application.output("foundset grande : "+foundset.getSize());
//	application.output("ID nota : "+status_notes_id);
//	application.output("Dossier ID : "+dossier_id);
//	application.output("prevStatus : "+prev_status_id);
//	application.output("nextStatus : "+actual_status_id);	
	application.output(globals.mpMessageLog + 'STOP dossier_addNote_popup.onShow() ',LOGGINGLEVEL.INFO);
}
/**
 * Revert the record
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"31E9C7DB-B4F2-4F6E-935A-6B81BB92C369"}
 */
function revertRecord(event) {
	application.output(globals.mpMessageLog + 'START dossier_addNote_popup.revertRecord() ',LOGGINGLEVEL.INFO);
	globals.isDossierStatusUpdated=false;
	if(mp_status_notes_to_mp_medical_dossiers.getSelectedRecord().isNew()){
		application.output("Reimposto lo stato della pratica");		
		foundset.mp_status_notes_to_mp_medical_dossiers.dossier_status_id=prev_status_id;
	}
	foundset.mp_status_notes_to_mp_medical_dossiers.dossier_status_id=prev_status_id;
	application.output("Stato precedente :"+prev_status_id);
	application.output("Stato successivo :"+actual_status_id);
	databaseManager.revertEditedRecords(foundset);
	application.output(globals.mpMessageLog + 'STOP dossier_addNote_popup.revertRecord() ',LOGGINGLEVEL.INFO);
	application.closeAllWindows();
}
