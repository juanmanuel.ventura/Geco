/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formDetailsName
 * @param {String} [relation]
 * @properties={typeid:24,uuid:"3D5C12A5-20AF-478F-A291-767CACCE7524"}
 */
function initialize(event, formDetailsName, relation) {
		if (elements.tabless.removeAllTabs()) {
			if (forms[formDetailsName]) {
//				elements.tabless.addTab(forms[formDetailsName], null, null, null, null, null, null, null, -1);
				elements.tabless.addTab(forms[formDetailsName], null, null, null, null, null, null, relation || null, null);
			} else application.output('errore sul form ' + formDetailsName);
		}
}
