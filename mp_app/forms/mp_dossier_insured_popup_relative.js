/***
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"CB6765DC-F9A1-4464-AFB3-0ECB8DACD35C",variableType:8}
 */
var isForRelative = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"32677C47-2EA0-42B8-99D1-83E0443ED8E8"}
 */
var relativeName = null;

/**
 * @author Francesco Sigolotto
 * Show relative List
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"8A2E85BE-3716-4150-A76C-174E95C5741A"}
 */
function showRelativeList(event) {

	application.output("Pratica per un familiare  " + isForRelative);
	if ((isForRelative)) {
		elements.relativeList.visible = true;
		elements.relativeList.enabled = true;
	} else {
		elements.relativeList.visible = false;
		elements.relativeList.enabled = false;
	}
}
/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"47C0E62A-E1C5-4FE3-B661-CD8884D8E770"}
 */
function onShow(firstShow, event) {
	relativeName = null;
	isForRelative = 0;
	showRelativeList(event);
	_super.onShow(firstShow, event);
}
/**
 * @author Francesco Sigolotto
 * Close popup
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"D2CE17C6-ABF4-41B2-91BE-2578FE177037"}
 */
function closePopup(event) {
	//se ho selezionato il parente setto nella tabella medical_dossier is_relative a 1,
	//altrimenti 0 cioè la pratica è per l'assicurato stesso.
	if ((isForRelative == 1) && (relativeName != null)) {
		is_for_relative = 1;
	}else if ((isForRelative == 1 && relativeName == null)){
		scopes.globals.DIALOGS.showErrorDialog('Error', 'Hai selezionato "Parente", seleziona un parente dalla lista', 'OK');
		return;
	}
	if (isForRelative == 0) {
		is_for_relative = 0;
	}
	application.output("C'è un parente(1 se c'è) " + is_for_relative);
	application.closeAllWindows();
	//globals.showDetailsList(event,'dossier_insured_bundle','dossier_details',true);
}
/**
 * @author Francesco Sigolotto
 * Handle changed data.
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action *
 * @returns {Boolean}
 * @properties={typeid:24,uuid:"1F504F83-46C9-4527-822B-30C6259AD73F"}
 */
function selectRelative(oldValue, newValue, event) {

	if (newValue != oldValue) {
		relative_id = newValue;
		application.output("Relative ID di medical dossier : " + relative_id);
	}
	return true
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"E4CA5A30-2911-4AB1-829C-0C549590CA57"}
 */
function revertRecord(event) {
	if (foundset.getSelectedRecord().isNew()) {
		_super.stopEditing(event);
		globals.mpShowDetailsList(event, 'mp_dossier_insured_bundle', 'mp_dossier_insured_filter_list', false);
	}
	application.closeAllWindows();
}
