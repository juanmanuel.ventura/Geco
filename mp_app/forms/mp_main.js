/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"D5F640E1-951B-4600-90A5-550A3D88AC81"}
 * @AllowToRunInFind
 */
function initialize(event) {
	application.output(globals.mpMessageLog + 'START mp_main.initialize() ', LOGGINGLEVEL.INFO);
	_super.initialize(event);
	if (!globals.mpGrantAccess(['HR Admin', 'Insurer', 'Insured', 'Admin', 'Users'])) {
		logOut(event);
	}
	//Variabili - ruolo
	globals.isInsurer = (globals.isFromMedicalPractice == false) ? scopes.globals.mpHasRoleFromGeco('Insurer') : scopes.globals.mpHasRole('Insurer'); //Assicuratore
	globals.isInsured = (globals.isFromMedicalPractice == false) ? scopes.globals.mpHasRoleFromGeco('Insured') : scopes.globals.mpHasRole('Insured'); //Assicurato
	globals.isHrAdmin = (globals.isFromMedicalPractice == false) ? scopes.globals.mpHasRoleFromGeco('HR Admin') : scopes.globals.mpHasRole('HR Admin'); //HR Admin
	globals.isAdmin = (globals.isFromMedicalPractice == false) ? scopes.globals.mpHasRoleFromGeco('Admin') : scopes.globals.mpHasRole('Admin'); //Admin
	var isInsurer = globals.isInsurer;
	var isInsured = globals.isInsured;
	var isHrAdmin = globals.isHrAdmin;
	var isAdmin = globals.isAdmin;
	var x = null;
	var y = null;
	//	elements.button6c.enabled = scopes.globals.mpCurrentUserDisplayName == 'STAFFA JONATHAN'?true:false;
	//	elements.button6c.visible = scopes.globals.mpCurrentUserDisplayName == 'STAFFA JONATHAN'?true:false;

	//	Assicurato
	//	if ( (isInsured || isUser) && !isInsurer && !isHrAdmin && !isAdmin) {
	if (isInsured && !isInsurer && !isHrAdmin && !isAdmin) {
		elements.button7.setLocation(elements.button3.getLocationX(), elements.button3.getLocationY());
		loadPanel(event, 'mp_dossier_insured_bundle');
	}//Assicuratore
	else if (isInsurer && !isInsured && !isHrAdmin && !isAdmin) {
		x = elements.button2c.getLocationX();
		y = elements.button2c.getLocationY();
		elements.button1.visible = false;
		elements.button1.enabled = false;
		elements.button2c.visible = false;
		elements.button2c.enabled = false;
		elements.button3.visible = true;
		elements.button3.enabled = true;
		//		elements.button4.visible = true;
		//		elements.button4.enabled = true;
		elements.button3.setLocation(elements.button1.getLocationX(), elements.button1.getLocationY());
		//		elements.button4.setLocation(elements.button2c.getLocationX(), elements.button2c.getLocationY());
		elements.button7.setLocation(x, y);
		loadPanel(event, 'mp_dossier_bundle');
	}//HR Admin
	else if (isHrAdmin && !isInsured && !isInsurer && !isAdmin) {
		x = elements.button3.getLocationX();
		y = elements.button3.getLocationY();
		elements.button1.visible = false;
		elements.button1.enabled = false;
		elements.button2c.visible = false;
		elements.button2c.enabled = false;
		elements.button3.visible = true;
		elements.button3.enabled = true;
		elements.button4.visible = true;
		elements.button4.enabled = true;
		elements.button3.setLocation(elements.button1.getLocationX(), elements.button1.getLocationY());
		elements.button4.setLocation(elements.button2c.getLocationX(), elements.button2c.getLocationY());
		elements.button7.setLocation(x, y);
		loadPanel(event, 'mp_dossier_bundle');
	}//Admin
	else if (isAdmin && !isHrAdmin && !isInsured && !isInsurer) {
		x = elements.button3.getLocationX();
		y = elements.button3.getLocationY();
		var x1 = elements.button4.getLocationX();
		var y1 = elements.button4.getLocationY();
		elements.button1.visible = false;
		elements.button1.enabled = false;
		elements.button2c.visible = false;
		elements.button2c.enabled = false;
		elements.button3.visible = true;
		elements.button3.enabled = true;
		elements.button4.visible = true;
		elements.button4.enabled = true;
		elements.button6.enabled = true;
		elements.button6.visible = true;
		elements.button3.setLocation(elements.button1.getLocationX(), elements.button1.getLocationY());
		elements.button4.setLocation(elements.button2c.getLocationX(), elements.button2c.getLocationY());
		elements.button6.setLocation(x, y);
		elements.button7.setLocation(x1, y1);
		loadPanel(event, 'mp_dossier_bundle');
	}//Assicurato e Assicuratore
	else if (isInsured && isInsurer && !isHrAdmin && !isAdmin) {
		elements.button3.visible = true;
		elements.button3.enabled = true;
		elements.button4.visible = true;
		elements.button4.enabled = true;
		elements.button7.setLocation(elements.button6.getLocationX(), elements.button6.getLocationY());
		loadPanel(event, 'mp_dossier_insured_bundle');
	}//Assicurato e HR Admin
	else if (isInsured && isHrAdmin && !isInsurer && !isAdmin) {
		elements.button3.visible = true;
		elements.button3.enabled = true;
		elements.button4.visible = true;
		elements.button4.enabled = true;
		elements.button7.setLocation(elements.button6.getLocationX(), elements.button6.getLocationY());
		loadPanel(event, 'mp_dossier_insured_bundle');
	}//Admin e Assicurato
	else if (isAdmin && isInsured && !isInsurer && !isHrAdmin) {
		//	else {
		elements.button3.visible = true;
		elements.button3.enabled = true;
		elements.button4.visible = true;
		elements.button4.enabled = true;
		elements.button6.enabled = true;
		elements.button6.visible = true;
		loadPanel(event, 'mp_dossier_insured_bundle');
	}
	//	var now = new Date()
	//	globals.workingYear = now.getFullYear();
	//	globals.workingMonth = now.getMonth() + 1; // in JavaScript it is a 0 based array
	globals.mpLoadRunnableRules();
	elements.buttonToLogger.visible = globals.isFromMedicalPractice == false ? true : false; //&& globals.mpHasMedicalPractice(utils.stringToNumber(security.getUserUID()));
//	JStaffa scommentare quando si trova il modo per girare tra mp_app e mp_configurator senza sloggare l'utente ogni volta
//	elements.buttonToConfigurator.visible = isAdmin;
	application.output(globals.mpMessageLog + 'STOP mp_main.initialize() ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element double-click action.
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"AD48D4FE-8060-4C88-B718-DAB2BCFA0439"}
 */
function openForm(event) {
	application.output(globals.mpMessageLog + 'START mp_main.openForm()', LOGGINGLEVEL.INFO);
	var win = application.createWindow("Carica Utente", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 530, 250);
	win.title = 'Carica Utente'
	win.show(forms.mp_insert_form);
	application.output(globals.mpMessageLog + 'STOP mp_main.openForm()', LOGGINGLEVEL.INFO);
}