borderType:"EmptyBorder,0,0,0,0",
dataSource:"db:/pratiche_sanitarie/mp_medical_dossiers",
extendsID:"35663D6A-488B-4BC9-9A7B-96B1626CA538",
items:[
{
extendsID:"D338D5F5-A343-4790-A440-B30F05D057EB",
height:72,
typeid:19,
uuid:"0E050FD4-3C13-45EB-A05E-BEBCB250A494"
},
{
dataProviderID:"mp_medical_dossiers_to_mp_insured_users.mp_insured_users_to_mp_insurance_policies.contractor",
extendsID:"D9EB55FE-7819-4837-A1DD-908A41F08CEA",
horizontalAlignment:2,
transparent:true,
typeid:4,
uuid:"0FA99678-B34B-4B72-AA02-8098B776D6FD"
},
{
anchors:9,
extendsID:"55D8455C-4DF4-4B05-8938-E98DDAA67A4D",
horizontalAlignment:2,
location:"517,44",
transparent:true,
typeid:4,
uuid:"14BBC0F8-2554-4E88-BEB5-22E1B5C8D317"
},
{
extendsID:"960EC00C-1995-4D25-8A9C-215027904354",
horizontalAlignment:2,
location:"350,7",
size:"157,20",
typeid:7,
uuid:"2112471B-ABC3-41AA-A928-F4AD1CAC8E76"
},
{
anchors:9,
extendsID:"4FCF9BC6-5668-440C-94E0-6F76D5586C39",
horizontalAlignment:2,
typeid:7,
uuid:"29E76B88-3E97-49D3-A079-2DA274D096D6"
},
{
anchors:9,
extendsID:"23CAB43B-1B04-4161-A301-334ABA1E04FB",
horizontalAlignment:2,
typeid:7,
uuid:"3D5C7415-65B4-41C0-A036-53BA042FE5F2"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"dossier_beneficiary_name",
extendsID:"2852DDB1-5CEA-4183-BC5D-953AB75925EB",
location:"350,44",
showFocus:true,
size:"157,20",
typeid:7,
uuid:"4E0CE60A-89AB-4D6C-95F3-5390D6F0B95A",
visible:true
},
{
extendsID:"5E6DEC6C-E879-45A0-BEDD-9C2CE6350158",
location:"998,48",
typeid:7,
uuid:"525FAC56-D2F1-4BEF-AF6C-2E9D5C732892"
},
{
extendsID:"59E601F6-13BF-4AC1-9325-DA28C42F72E7",
horizontalAlignment:2,
typeid:7,
uuid:"66037A15-8EB9-45BA-B0D7-F9761EDC3776"
},
{
extendsID:"70EFC6DA-3A9C-471E-A628-F59E1702F4D1",
height:95,
typeid:19,
uuid:"6AE165CF-E6FF-4247-A38F-38EA4FDAE966"
},
{
anchors:9,
editable:false,
enabled:false,
extendsID:"425D8A17-327C-405D-A858-C5AA6FBAD8DB",
format:"dd-MM-yyyy",
horizontalAlignment:2,
location:"657,44",
transparent:true,
typeid:4,
uuid:"72C05BCF-773C-48FB-A5CB-4AC9FFFE9590"
},
{
enabled:true,
extendsID:"19DC9D86-1F2B-45D6-B6CC-E6EC75E0DCDF",
location:"0,72",
onActionMethodID:"19359847-063F-43AF-B4CF-5ED7AFFBB9BA",
size:"32,23",
typeid:7,
uuid:"8AB6E34C-803B-45A6-9597-E160D6FAE486",
visible:true
},
{
extendsID:"0178BB22-940C-4990-B0F2-A354F02C5523",
location:"1023,48",
typeid:7,
uuid:"99210D66-04C6-43A5-B537-17A98EC3B11C"
},
{
anchors:9,
extendsID:"113E33C4-21DE-4AA8-8FB8-2371B51A9678",
horizontalAlignment:2,
typeid:7,
uuid:"9A366DD1-EB34-4729-B100-D3929EDBE1DF"
},
{
extendsID:"5FCDACC5-18DB-492C-ABE7-B223C63E0E03",
location:"11,10",
typeid:7,
uuid:"A3F3C5B5-798F-46D4-B0E4-A0649661E177",
visible:false
},
{
enabled:false,
extendsID:"3A451108-5E01-4404-864F-DB21E6D1FB17",
location:"32,72",
onActionMethodID:"-1",
typeid:7,
uuid:"A89C1671-5CC9-40C0-8447-DFD8F107704B",
visible:false
},
{
dataProviderID:"mp_medical_dossiers_to_mp_insured_users.real_name",
extendsID:"5A410AC0-D5C8-488C-82A3-24B4CFC98EF8",
horizontalAlignment:2,
size:"172,20",
transparent:true,
typeid:4,
uuid:"CC96AED4-8373-446D-ACD1-FAC352AB8E36",
visible:true
},
{
customProperties:"",
extendsID:"C712A750-71BC-48CB-91F7-4B360400DF28",
onActionMethodID:"F85DCD44-B38C-4DDD-A900-8C850AEF75D7",
transparent:false,
typeid:7,
uuid:"D62CB61E-B94F-4CFC-94F2-D146FE3F466C"
},
{
extendsID:"DDCEB5FF-C59D-4E9C-8FE3-1CBC29936122",
horizontalAlignment:2,
typeid:7,
uuid:"DE94C0C6-6717-4278-BF03-37034334C8D5"
},
{
anchors:9,
editable:false,
enabled:false,
extendsID:"F8381FFC-0597-41D4-815A-02EC0EF2F8ED",
horizontalAlignment:2,
location:"831,44",
transparent:true,
typeid:4,
uuid:"E88B86B8-4ACA-45B3-9A8D-26F6534B8E00"
}
],
name:"mp_dossier_insured_list",
onRecordSelectionMethodID:"-1",
onShowAllRecordsCmdMethodID:"00CBD9B3-8C9C-4039-AE42-8B8436EC0019",
onShowMethodID:"EA08A232-2D57-4FB7-95E9-9C5C7B5A65A0",
styleName:"GeCo",
typeid:3,
uuid:"BE6F7A3C-5025-4C94-A827-80E614F18F96"