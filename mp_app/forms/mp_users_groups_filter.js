/**
 * @type {Number}
 * @properties={typeid:35,uuid:"E8A92452-C17C-483C-BA47-6880D79DE57B",variableType:8}
 */
var usersGroup = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"CCFB46B7-18AF-4496-A054-8FB151A7FA61",variableType:8}
 */
var userId = null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"1FDAEC23-94C5-41CD-B9A8-CD714D777CF8"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"F35A3FB1-30AC-4A53-B47F-A69AEFA590D1"}
 */
function resetFilter(event, goTop) {
	usersGroup = null;
	userId = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"F7947181-3E04-41D5-9F66-47D28265842A"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.mp_users_to_mp_users_roles.role_id = usersGroup;
		foundset.user_id = userId;
		foundset.search();
	}
}
