/**
 * @type {Number}
 * @properties={typeid:35,uuid:"E0551201-FB90-4021-AB72-C31292EE102F",variableType:8}
 */
var isToCreateNote = 0;
/**
 *Handle changed data.
 *
 * @param oldValue old value
 * @param newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"E1705843-DBA9-438B-A36F-43CA25E505D9"}
 */
function showTextAreaNotes(oldValue, newValue, event) {
	
	if(oldValue != newValue){
		
		application.output(globals.mpMessageLog + 'START mp_dossier_notes_popup.showTextAreaNotes() ',LOGGINGLEVEL.INFO);
		application.output("Scelta fatta della creazione della nota : "+isToCreateNote);	
		//l'utente ha scelto di creare la nota	
		if(isToCreateNote == 1){	
//			//creo il record della nota
//			var recNumber=foundset.medical_dossiers_to_status_notes.newRecord(false);
//			foundset.medical_dossiers_to_status_notes.dossier_id = dossier_id;
//			var notaID = foundset.medical_dossiers_to_status_notes.status_notes_id;
//			application.output("ID nota creata :"+notaID);
//			foundset.medical_dossiers_to_status_notes.prev_status_id = globals.prevStatusNote;
//			foundset.medical_dossiers_to_status_notes.actual_status_id= globals.nextStatusNote;
//			//application.output("Record creato in note : "+recNumber);
//			application.output("ID Dossier nella tabella note : "+foundset.medical_dossiers_to_status_notes.dossier_id);
//			application.output("Prev status in note : "+foundset.medical_dossiers_to_status_notes.prev_status_id);
//			application.output("Actual status in note : "+foundset.medical_dossiers_to_status_notes.actual_status_id);
			application.output(globals.mpMessageLog + 'START mp_dossier_notes_popup.showTextAreaNotes() apro il popup ',LOGGINGLEVEL.INFO);
			//Apro il popup dove inserire la nota
			var notePopup = application.createWindow("Nota", JSWindow.MODAL_DIALOG);
			notePopup.setInitialBounds(1000, 1000,500,150);		
			notePopup.title = 'Inserisci la nota';
			notePopup.show(forms.mp_dossier_addNote_bundle);
			application.output(globals.mpMessageLog + 'STOP mp_dossier_notes_popup.showTextAreaNotes() chiudo il popup ',LOGGINGLEVEL.INFO);
		}
		application.output(globals.mpMessageLog + 'STOP mp_dossier_notes_popup.showTextAreaNotes() ',LOGGINGLEVEL.INFO);
	}
	return true;
}
/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"3BBE43C5-1B97-4F67-8218-5456F3EDFD00"}
 */
function onShow(firstShow, event) {
	application.output("istoCreate è "+isToCreateNote);
	//valore default radio button
	isToCreateNote = 0;
	//return _super.onShow(firstShow, event);
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"1363FFAD-CBAE-4B45-B497-2FDB2FFC7F55"}
 */
function closePopup(event) {	
	//chiudo il popup	 
	isToCreateNote = 0;
	if(isToCreateNote == 0){
		application.closeAllWindows();		
	}
}


