/**
 * @param {JSEvent} event The event that triggered the action. 
 * @properties={typeid:24,uuid:"0860DEDF-B11B-4809-AF73-A3B314EA81D3"}
 */
function updateUI(event) {
	if(!globals.isHrAdmin && !globals.isAdmin){
			elements.buttonAdd.visible = false;
	   }
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"20964B86-5FB0-44B9-A737-F50F8F94A637"}
 */
function newRecord(event) {
	_super.newRecord(event);
	forms.mp_insurer_user_details.elements.fNumberCode.enabled = true;
	forms.mp_insurer_details.elements.username.enabled = true;
}
