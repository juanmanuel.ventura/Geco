/**@type {Number}
 *
 * @properties={typeid:35,uuid:"54346025-9352-46B5-89B1-F5024332F762",variableType:8}
 */
var prevStatusNote= null;

/**@type {Number}
* @properties={typeid:35,uuid:"D6F35CB2-0242-42ED-A043-D24224490ED8",variableType:8} */
var nextStatusNote= null;

/**
 * @type {Object}
 * @properties={typeid:35,uuid:"5D374B6C-B928-4E55-983F-B651D1EF2FD1",variableType:-4}
 */
var dossierNoteObjForMail={
	dossierID : '',
	noteID : '',
	note : ''
};
/**@type {String}
 * @properties={typeid:35,uuid:"40CB0D26-0FFF-46CC-BEAD-3911C618487B"}
 */
var dossierNote=null;
/**
 * @type {Number}
 * @properties={typeid:35,uuid:"9B3F4C50-EE45-42D7-B576-F78CFA8B8F9C",variableType:8}
 */
var dossierNoteIDForEmail=null;
/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"CCCBDC63-CEEF-42DA-9642-DE2584219BDA",variableType:-4}
 */
var isDossierStatusUpdated =false;
/**
 * @type {Boolean}
 *
 * @properties={typeid:35,uuid:"D5D27B06-75FF-4764-BB88-6FE48EF31B33",variableType:-4}
 */
var canChangeInfo = true;
/**
 * @type {Boolean}
 *
 * @properties={typeid:35,uuid:"107D55CB-63E7-4D04-A609-2C0569F514CC",variableType:-4}
 */
var isDossierClosed = false;


/**
 * @properties={typeid:24,uuid:"BAA686AB-269A-4458-8E26-6CDD6E8CD49E"}
 */
function mpOnSolutionOpen() {
	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		application.putClientProperty(APP_WEB_PROPERTY.WEBCLIENT_TEMPLATES_DIR, 'custom');
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_DEFAULT_SCROLLABLE, true);
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_SCROLLABLE_KEEP_LOADED_ROWS, true);
		
	}
}

/**@type {Boolean}
 * @properties={typeid:35,uuid:"3104FBA4-50E9-48D0-B733-D2F147521478",variableType:-4}
 */
var cleanData= true;


/**@type {Boolean}
 * @properties={typeid:35,uuid:"4380EF11-972C-448B-826A-5B9929DC6769",variableType:-4}
 */
var newRecord = false;
/**
 * @type {String}
 * @properties={typeid:35,uuid:"83750101-542C-41F3-A530-C7ECC53CF55B"}
 */
var formBundleName ="";
/**@type{Boolean}
 * @properties={typeid:35,uuid:"1476E977-DA93-432D-AA82-D28572819534",variableType:-4}
 */
var abortCreatingNewRecord  = false;

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formBundle
 * @param {String} formName
 * @param {Boolean} toDetails
 *
 * @properties={typeid:24,uuid:"F7DA4CD3-2012-4087-8BFE-100F4E1B0606"}
 */
function mpShowDetailsList(event,formBundle,formName, toDetails) {
	
	application.output("showDetailsList bundle :  "+formBundle+" formName "+formName + ' toDetails ' + toDetails);
	if (toDetails == false && forms.__mp_base.isEditing()) {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!", "OK");
		return;
	}
	
	//carica il bundle corretto
	formBundleName=formBundle;
	application.output("BUNDLE è "+formBundleName+" METODO LANCIATO DA "+formName);

	forms[formBundle].initialize(event,formName);
	if (!toDetails) {
		forms[formBundle].initialize(event,formName);
		// force to apply filter (seems not to work on form's onLoad event)
		var filterForm = forms[formName].elements.split.getLeftForm();
		application.output(' filterForm: ' + filterForm);			
	}
}

/**
 * Invia un email al cambio dello stato della pratica se inviata correttamente ritorna true 
 * @param {Number} mailTypeId
 * @param {String} addresseeName  //destinario email
 * @param {String} mailRecipient  (indirizzo destinario email)
 * @param {String} dossierPrevStatus
 * @param {String} dossierActualStatus
 * @return {Boolean} isMailsent
 * @properties={typeid:24,uuid:"0295D501-511C-4645-8272-A9EB54274527"}
 */
function sendMailForChangedDossierStatus(mailTypeId,addresseeName,mailRecipient,dossierPrevStatus,dossierActualStatus) {
	application.output(globals.mpMessageLog + 'START globals.sendMailForChangedDossierStatus() ', LOGGINGLEVEL.INFO);
	var objMail = globals.mpGetMailType(mailTypeId);		
	var	mailSubs = {
			nome : '',
			prevStatus :'',
			actualStatus :'',
			nota:''		
		};
	
	application.output('Nome assicurato : ' + addresseeName + '; stato precedente ' + dossierPrevStatus+ '; stato attuale ' + dossierActualStatus + '; nota inserita '+dossierNote);
	var recipient = mailRecipient;
	//email da spedire all'assicuratore
	if(mailTypeId==11){
	recipient=objMail.distrList;
	}
	var subject = objMail.subject;
	mailSubs.nome = (addresseeName!=null)? addresseeName:'';
	mailSubs.prevStatus = (dossierPrevStatus!=null)?dossierPrevStatus:'';
	mailSubs.actualStatus = (dossierActualStatus!=null)?dossierActualStatus:'';
	application.output('globals.dossierNoteObjForMail.note  è '+globals.dossierNoteObjForMail.note)
	mailSubs.nota = (dossierNote!=null)?('Nota inserita : '+globals.dossierNote):'';
	var message = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubs);
	application.output(globals.mpMessageLog + 'globals.sendMailForChangedDossierStatus() SENDING MAIL to distribution list  subject: ' + subject + ' message: ' + message, LOGGINGLEVEL.INFO);
	var isMailsent=mpEnqueueMailReminderForDossierStatus(recipient, subject, message);
	application.output("Mail spedita ? : "+isMailsent);
	application.output(globals.mpMessageLog + 'STOP globals.sendMailForChangedDossierStatus() ', LOGGINGLEVEL.INFO);
	return isMailsent;
}
/**
 *Ritorna la stringa associata al valore dell'ID dello stato della pratica
 * @param {Number} statusID
 * @return {String} dossierStatusAsString
 * @properties={typeid:24,uuid:"C8CD2722-CD4B-4D34-9C34-27064297E2D4"}
 */
function convertDossierStatusIdToString(statusID) {
	switch (statusID) {
	case 1:
		return "Nuova";
		break;
	case 2:
		return "Da Validare";
	case 3:
		return "Validata";
	case 4:
		return "Not OK";
	case 5:
		return "Inviata";
	case 6:
		return "In Lavorazione";
	case 7:
		return "Liquidata";
	case 8:
		return "Respinta";
	case 9:
		return "Cancellata";
	default:
		return null;
		break;
	}	
}
