/**
 * @param {Object} user
 * @param {Object} password
 *
 * @properties={typeid:24,uuid:"AD608B69-594D-403C-AB2E-2E5578F8A219"}
 * @SuppressWarnings(wrongparameters)
 * @AllowToRunInFind
 */
function loginUser(user, password) {
	application.output(globals.messageLog + 'START authenticator.loginUser() '+user,LOGGINGLEVEL.INFO);
	var result = {
		logged: false,
		error: null
	};

	if (!user) {
		result.error = 'Lo username non può essere vuoto';
		application.output(globals.messageLog + 'authenticator.loginUser() message error  '+result.error,LOGGINGLEVEL.DEBUG);
		return result;
	}

	if (!password) {
		result.error = 'La password non può essere vuota';
		application.output(globals.messageLog + 'authenticator.loginUser() message error '+result.error,LOGGINGLEVEL.DEBUG);
		return result;
	}
	
	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');

	// look up user
	if (users.find()) {
		users.user_name = user;

		// search!
		if (users.search() != 0) {

			// kick out not enabled users
			if (users.is_enabled != 1) {
				result.error = "Utente non abilitato";
				application.output(globals.messageLog + 'authenticator.loginUser() message error '+result.error,LOGGINGLEVEL.DEBUG);
				return result;
			}
			// errore per utenti che sono bloccati
			if(users.is_locked != 0 && users.is_locked != null){
				result.error = "Utente bloccato";
				application.output(globals.messageLog + 'authenticator.loginUser() message error '+result.error,LOGGINGLEVEL.DEBUG);
				return result;
			}

			// successfully logged!
			if (users.user_password == globals.sha1(password)) {
				// get the user groups foundset
				/** @type {JSFoundSet<db:/geco/user_groups>} */
				var userGroups = users.users_to_user_groups;

				// set groups array
				var groupsArray = [];

				// populates the array with the groups name
				for (var index = 1; index <= userGroups.getSize(); index++) {
					groupsArray.push(userGroups.getRecord(index).user_groups_to_groups.name);
				}

				if (security.login(user, users.user_id, groupsArray)) {
					users.failed_login_attempts = 0;
					result.logged = true;
					application.output(globals.messageLog + 'STOP authenticator.loginUser() user logged OK ',LOGGINGLEVEL.INFO);
					return result;
				}
			} else {
				
				// login attempt failed
				users.failed_login_attempts++;
				application.output(globals.messageLog + 'authenticator.loginUser() password KO login errate: '+ users.failed_login_attempts,LOGGINGLEVEL.DEBUG);
				if (users.failed_login_attempts >= 3) {
					users.is_locked = 1;
				}
			}

		}

		result.error = 'Utente o password sbagliata';
		if (users.failed_login_attempts > 0) result.error = 'Utente o password sbagliata. Login errate ' + users.failed_login_attempts;
		application.output(globals.messageLog + 'authenticator.loginUser() message error: '+ result.error,LOGGINGLEVEL.DEBUG);

	}
	application.output(globals.messageLog + 'STOP authenticator.loginUser() ',LOGGINGLEVEL.DEBUG);
	return result;
}
