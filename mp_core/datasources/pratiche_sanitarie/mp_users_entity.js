/**
 * DISABILITATO; TOLTO CAMPO MP_USER_ID DALLA TABELLA users DI GECO
 * @param {JSRecord<db:/pratiche_sanitarie/mp_users>} record
 * @properties={typeid:24,uuid:"A4A29FD9-6143-4825-B44E-C7B6FD683398"}
 * @AllowToRunInFind
 */
function afterRecordInsert(record) {
//	if (record.geco_user_id != null) {
//		application.output('------------------START AFTER RECORD INSERT MP USERS ENTITY;\nRECORD.GECO_USER_ID: ' +record.geco_user_id);
//		/** @type {JSFoundSet<db:/geco/users>} */
//		var users = databaseManager.getFoundSet('geco', 'users');
//
//		if (users.find()) {
//			users.user_id = record.geco_user_id;
//			var tot = users.search();
//			application.output('------------------TOT RECORD: ' +tot + '; RECORD.USER_ID = ' + record.user_id);
//			if (tot > 0) {
//				var rec = users.getRecord(1);
//				if (rec.mp_user_id == null) {
//					databaseManager.setAutoSave(false);
//					rec.mp_user_id = record.user_id;
//					databaseManager.saveData(rec);
//				}
//				application.output('-------------------rec.mp_user_id: ' + rec.mp_user_id);
//			}
//		}
//		application.output('------------------STOP AFTER RECORD INSERT MP USERS ENTITY');
//	}
}

/**
 * DISABILITATO; TOLTO CAMPO MP_USER_ID DALLA TABELLA users DI GECO
 * @param {JSRecord<db:/pratiche_sanitarie/mp_users>} record
 * @properties={typeid:24,uuid:"D536B00C-D4D3-448F-AF38-E20AC32DA5AC"}
 * @AllowToRunInFind
 */
function afterRecordUpdate(record) {
//	if (record.geco_user_id != null) {
//		application.output('------------------START AFTER RECORD UPDATE MP USERS ENTITY;\nRECORD.GECO_USER_ID: ' +record.geco_user_id);
//		/** @type {JSFoundSet<db:/geco/users>} */
//		var users = databaseManager.getFoundSet('geco', 'users');
//
//		if (users.find()) {
//			users.user_id = record.geco_user_id;
//			var tot = users.search();
//			application.output('------------------TOT RECORD: ' +tot + '; RECORD.USER_ID = ' + record.user_id);
//			if (tot > 0) {
//				var rec = users.getRecord(1);
//				if (rec.mp_user_id == null) {
//					databaseManager.setAutoSave(false);
//					rec.mp_user_id = record.user_id;
//					databaseManager.saveData(rec);
//				}
//				application.output('-------------------rec.mp_user_id: ' + rec.mp_user_id);
//			}
//		}
//		application.output('------------------STOP AFTER RECORD UPDATE MP USERS ENTITY');
//	}
}

/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_users>} record record that will be inserted
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"9FB10A25-D6B2-4711-B01A-44965B601BB0"}
 */
function onRecordInsert(record) {
	return globals.mpApplyAllRules(record, 'onInsert');
}

/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_users>} record record that will be updated
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"43A5F190-BEAB-486E-81FF-C8DA6E4E6D57"}
 */
function onRecordUpdate(record) {
	return globals.mpApplyAllRules(record, 'onUpdate');
}
