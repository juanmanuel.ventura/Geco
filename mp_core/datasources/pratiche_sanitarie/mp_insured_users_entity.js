/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record record that will be inserted
 * @properties={typeid:24,uuid:"F52B83DD-D27F-412E-BEA2-FCFFC98EB22B"}
 * @AllowToRunInFind
 */
function afterRecordUpdate(record) {
	//		/** @type {JSDataSet} */
	//		var dataset=record.getChangedData();
	//		for (var index = 1; index <= dataset.getMaxRowIndex(); index++) {
	//			if(dataset.getValue(index,1)=='real_name'){
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_medical_dossiers>} */
	var medical_dossiers = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_medical_dossiers');
	if (medical_dossiers.find()) {
		medical_dossiers.user_insured_id = record.user_insured_id;
		medical_dossiers.is_for_relative = 0;
		var tot = medical_dossiers.search();
		if (tot > 0) {
			for (var i = 1; i <= medical_dossiers.getSize(); i++) {
				var rec = medical_dossiers.getRecord(i);
				if (rec.dossier_beneficiary_name != record.real_name) {
					databaseManager.setAutoSave(false);
					rec.dossier_beneficiary_name = record.real_name;
					var success = databaseManager.saveData(rec);
					application.output('indice : ' + i + ' ;dossier_id : ' + rec.dossier_id + ' success: ?' + success);
				}
			}
		}
	}
	//			}
	//		}

}

/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record record that will be inserted
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"7ECF69FF-B186-44AE-97F6-B66DCF2BCB36"}
 */
function onRecordInsert(record) {
	return globals.mpApplyAllRules(record, 'onInsert');
}

/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record record that will be updated
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"3E65733C-37D7-4D53-B3FB-FED441EC6A06"}
 */
function onRecordUpdate(record) {
	return globals.mpApplyAllRules(record, 'onUpdate');
}
