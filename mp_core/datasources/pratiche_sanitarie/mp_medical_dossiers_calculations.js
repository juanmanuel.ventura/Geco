/**
 * @properties={type:4,typeid:36,uuid:"492B9284-714A-464A-945D-63D626B23802"}
 */
function has_note(){
	if (mp_medical_dossiers_to_mp_status_notes&& mp_medical_dossiers_to_mp_status_notes.getSize()>0){
			return 1;
		}
		else return 0;
}

/**
 * @properties={type:4,typeid:36,uuid:"8A143D86-17A1-4A24-9953-96B4F17C37CB"}
 */
function has_document_or_has_bill(){
	if ((mp_medical_dossiers_to_mp_bills&& mp_medical_dossiers_to_mp_bills.getSize()>0)
		||
		(mp_medical_dossiers_to_mp_documents && mp_medical_dossiers_to_mp_documents.getSize()>0)){
		return 1;
	}
	else return 0;
}
