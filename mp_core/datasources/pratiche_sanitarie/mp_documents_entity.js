/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_documents>} record record that will be inserted
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"4401E87F-71A7-4423-8A82-2A2638AFF7A2"}
 */
function onRecordInsert(record) {
	return globals.mpApplyAllRules(record, 'onInsert');
}
/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_documents>} record record that will be updated
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"F9829525-32BD-4263-A363-C94886A7E7F3"}
 */
function onRecordUpdate(record) {
	return globals.mpApplyAllRules(record, 'onUpdate');
}