/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_informative_documents>} record record that will be inserted
 * @returns {Boolean}
 *
 *
 * @properties={typeid:24,uuid:"F85F0AB1-587F-411D-A584-E61B8BA2BB99"}
 */
function onRecordInsert(record) {
	return globals.mpApplyAllRules(record, 'onInsert');
}

/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_informative_documents>} record record that will be updated
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"4153518F-6C19-47B1-837B-C387DA2E78A2"}
 */
function onRecordUpdate(record) {
	return globals.mpApplyAllRules(record, 'onUpdate');
}