/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} record record that will be inserted
 * @properties={typeid:24,uuid:"6B5CFC1C-5DE9-4807-AB98-193B380598BE"}
 * @AllowToRunInFind
 */
function afterRecordUpdate(record) {
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_medical_dossiers>} */
	var medical_dossiers = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_medical_dossiers');
	if (medical_dossiers.find()) {
		medical_dossiers.relative_id = record.relative_id;
		medical_dossiers.is_for_relative = 1;
		var tot = medical_dossiers.search();
		if (tot > 0) {
			for (var index = 1; index <= medical_dossiers.getSize(); index++) {
				var rec = medical_dossiers.getRecord(index);
				if (rec.dossier_beneficiary_name != record.real_name) {
					databaseManager.setAutoSave(false);
					rec.dossier_beneficiary_name = record.real_name;
					var success = databaseManager.saveData(rec);
					application.output('indice : ' + index + ' ;dossier_id : ' + rec.dossier_id + ' success: ?' + success);
				}
			}
		}
	}
}
/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} record record that will be inserted
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"3273EA6E-3B9E-46E9-8A7C-EBA7223432DF"}
 */
function onRecordInsert(record) {
	return globals.mpApplyAllRules(record, 'onInsert');
}

/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} record record that will be updated
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"885C7ABD-9586-47FA-8BE3-515D18BEEB44"}
 */
function onRecordUpdate(record) {
	return globals.mpApplyAllRules(record, 'onUpdate');
}
