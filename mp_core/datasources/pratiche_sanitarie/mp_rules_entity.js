
/**
 * Record after-insert trigger.
 * @param {JSRecord<db:/pratiche_sanitarie/mp_rules>} record record that is inserted
 *
 * @properties={typeid:24,uuid:"7B7E69A7-2EF4-44E0-8137-6B6D3320FBF0"}
 */
function afterRecordInsert(record) {
	scopes.globals.mpLoadRunnableRules();
}

/**
 * Record after-update trigger.
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_rules>} record record that is updated
 *
 * @properties={typeid:24,uuid:"22F56258-6DD7-48E5-9977-9C3A795A77A9"}
 */
function afterRecordUpdate(record) {
	scopes.globals.mpLoadRunnableRules();
}

/**
 * Record pre-delete trigger.
 * Validate the record to be deleted.
 * When false is returned the record will not be deleted in the database.
 * When an exception is thrown the record will also not be deleted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_rules>} record record that will be deleted
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"EE04DCA5-645E-4DC4-8336-BCDC5F06CFC7"}
 */
function onRecordDelete(record) {
	if (record.working_entity) {
		globals.DIALOGS.showErrorDialog("Impossibile eliminare", "Questa regola è associata all'entity [" + record.working_entity + "] quindi non può essere eliminata", 'OK')
		return false;
	}
	return true;
}