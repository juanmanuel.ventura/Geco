/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_users_roles>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"50598F96-1596-4D17-8AB3-B34F18589CC1"}
 */
function validateUserGroups(record) {
	if (globals.isEmpty(record.role_id)) {
		throw new Error('- Il ruolo non può essere vuoto.');
	} else {
		/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users_roles>} */
		var users_roles = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users_roles');
		if (users_roles.find()) {
			users_roles.user_id = record.user_id;
			users_roles.role_id = record.role_id;
			var tot = users_roles.search();
			if (tot >= 1) throw new Error('- La coppia utente-ruolo esiste già, cambiala');
		}
	}
}

