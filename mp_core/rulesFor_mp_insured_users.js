/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"FA813C2D-A027-4A17-8EB3-F9DC9F477DC8"}
 */
function validateFirstName(record) {
	var regex = new RegExp('^[\\\'a-zA-Z][a-zA-Z\\s\\\']+$');
	if (globals.mpIsEmpty(record.first_name)) throw new Error('- Il campo Nome è obbligatorio');
	if (!globals.mpIsEmpty(record.first_name) && !regex.test(record.first_name)) throw new Error('- Inserire solo lettere nel campo Nome');
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"8C41D853-D020-4253-95D0-B06F9B30B5F7"}
 */
function validateLastName(record) {
	var regex = new RegExp('^[\\\'a-zA-Z][a-zA-Z\\s\\\']+$');
	if (globals.mpIsEmpty(record.last_name)) throw new Error('- Il campo Cognome è obbligatorio');
	if (!globals.mpIsEmpty(record.last_name) && !regex.test(record.last_name)) throw new Error('- Inserire solo lettere nel campo Cognome');
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"0DFF319D-174E-44F0-8ADC-566AFF7731AE"}
 */
function validateGender(record) {
	if (globals.mpIsEmpty(record.gender)) throw new Error('- Il campo Sesso è obbligatorio');
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"5D5E5373-F4E5-48AC-894D-FF533835A9FA"}
 */
function validateFiscalCode(record) {
	var regex = new RegExp('^[A-Z]{6}[A-Z0-9]{2}[A-Z][A-Z0-9]{2}[A-Z][A-Z0-9]{3}[A-Z]$');
	if (globals.mpIsEmpty(record.fiscal_code)) throw new Error('- Il campo Cod. Fiscale è obbligatorio');
	if (!globals.mpIsEmpty(record.fiscal_code)) {
		record.fiscal_code = globals.mpTrimString(record.fiscal_code);
		if (!regex.test(record.fiscal_code)) throw new Error(' - Il campo Cod. Fiscale non è nel formato corretto');
	}
	record.fiscal_code = globals.mpTrimString(record.fiscal_code);
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"3EF9E6B6-9E97-4B6B-9882-E9C1AC6DEF28"}
 */
function validateBirthPlace(record) {
	if (globals.mpIsEmpty(record.birth_place)) {
		throw new Error('- Il campo Luogo di nascita è obbligatorio');
	} else if (!globals.mpIsEmpty(record.birth_place)) {
		var regex = new RegExp('^[a-zA-Z][a-zA-Z\\s\\\']+$');
		if (!regex.test(record.birth_place)) throw new Error('- Inserire solo lettere nel Luogo di nascita');
	}
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"F4D18126-201E-4424-924C-ABCE8BA4C031"}
 */
function validateBirthDate(record) {
	if (globals.mpIsEmpty(record.birth_date)){
		throw new Error('- Il campo Data di nascita è obbligatorio');
	}
	else{		
		if(record.birth_date.getFullYear()<1900){
			throw new Error('- Inserire un anno maggiore di 1900');
		}		
	}
}

/**
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"8BB916CA-0F71-4C25-B847-D4BC49311EFA"}
 */
function validateDate(record) {
	var errorMessage = '';
	if (globals.mpIsEmpty(record.start_date)) errorMessage = errorMessage + '- Il campo Inizio cop. assicur. è obbligatorio.\n'; //throw new Error('- Il campo inizio cop. assicur. è obbligatorio.');
	if (globals.mpIsEmpty(record.end_date)) errorMessage = errorMessage + '- Il campo Fine cop. assicur. è obbligatorio.\n'; //throw new Error('- Il campo fine cop. assicur. è obbligatorio.');
	if (record.start_date > record.end_date) errorMessage = errorMessage + '- La cop. assicur. non può finire prima dell\'Inizio della copertura stessa.' //throw new Error('- La cop. assicur. non può finire prima dell\'inizio della copertura stessa.');

	if (!scopes.globals.mpIsEmpty(errorMessage)) throw new Error(errorMessage);
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"121E8D53-0D2A-4885-AE54-A7C24CCE5AC3"}
 */
function validateCompanyId(record) {
	if (globals.mpIsEmpty(record.company_id)) throw new Error('- Il campo Azienda è obbligatorio');
}
/**
 * Verifica numero di cellulare
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"B436B5FC-2978-41CC-9354-94CE0A0505D2"}
 */
function validateMobileNumber(record) {
	var regex=new RegExp(/^[0-9]{10,14}$/);
	if (globals.mpIsEmpty(record.mobile_number)) {
		throw new Error('- Il campo Cellulare è obbligatorio');
	}	
	else{
		if(!regex.test(record.mobile_number)){
			throw new Error('- Inserisci un numero di cellulare nel formato corretto,solo numeri(minimo 10)');
		}
	}
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"1FB4C7A8-8ECF-4A3E-BBD3-FDF25CB3A252"}
 */
function validateHomeNumber(record) {
	if (!globals.mpIsEmpty(record.home_number)) {
		var regex=new RegExp(/^[0-9]+$/);
		if(!regex.test(record.home_number)){
			throw new Error('- Il campo Tel.abitazione non può contenere lettere');
		}
	}
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"7A1DCBA0-3EDA-4375-9013-5F468E554582"}
 */
function validateOfficeNumber(record) {
	if (!globals.mpIsEmpty(record.office_number)) {
		var regex=new RegExp(/^[0-9]+$/);
		if(!regex.test(record.office_number)){
			throw new Error('- Il campo Tel.ufficio non può contenere lettere');
		}
	}
}
/**
 * Verifica provincia
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"2A15E3FD-96E1-4873-B189-3D66C501EBF9"}
 */
function validateDistrict(record) {
	if (globals.mpIsEmpty(record.district)) throw new Error('- Il campo Provincia è obbligatorio');
}
/**
 * Verifica provincia di nascita
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"6BD3B0B7-9C31-465D-A203-88015DE8515D"}
 */
function validateBirthDistrict(record) {
	if (globals.mpIsEmpty(record.birth_district)) throw new Error('- Il campo Prov. Nascita è obbligatorio');
}
/**Verifico (se c'è) che l'IBAN sia nel formato corretto
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"E464E0DE-EF0D-49AB-B8FF-58668B2FA43B"}
 */
function validateIbanCode(record) {
	if (!scopes.globals.mpIsEmpty(record.iban_code)) {
		var errorMessage = '';
		//esempio IBAN italiano "IT12L1234512345123456789012";		
		var ibanCode = record.iban_code;
		var regex = null;
		var countryCode = ibanCode.slice(0, 2);
		if (!countryCode.equals("IT")) errorMessage = '- Codice IBAN errato: Non inizia con "IT".\n'; //throw new Error('- Codice paese IBAN errato: Non inizia con "IT".');
		regex = new RegExp('[A-Z]');
		//Verifico che il CIN sia una lettera maiuscola
		if (!regex.test(ibanCode[4])) errorMessage = errorMessage + '- Codice IBAN errato: CIN errato.\n' //throw new Error('- Codice paese IBAN errato: CIN errato.');
		if (ibanCode.length != 27) errorMessage = errorMessage + '- Codice IBAN errato: Lunghezza inferiore a quella standard (27).\n' //throw new Error('- Codice paese IBAN errato: Lunghezza inferiore a quella standard (27).');

		//controllo che dal sesto carattere sino all'ultimo siano solo numeri
		var iban = ibanCode.slice(5, ibanCode.length);
		application.output('iban.length: ' + iban.length);
		regex = new RegExp('^[0-9]*$');
		if (!regex.test(iban)) errorMessage = errorMessage + '- Codice IBAN errato: sono ammessi solo numeri dal sesto carattere in poi.' //throw new Error('- Codice paese IBAN errato.');

		if (!scopes.globals.mpIsEmpty(errorMessage)) throw new Error(errorMessage);
	}
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"A0B99583-A14D-42C9-922A-277399938EDE"}
 */
function validateCountry(record) {
	if (!scopes.globals.mpIsEmpty(record.country)) {
		var regex = new RegExp('^[a-zA-Z][a-zA-Z\\s\\\']+$');
		if (!regex.test(record.country)) throw new Error('- Inserire solo lettere nel campo Stato');
	}
}

/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"1BB63213-8EBC-4269-9001-FE24D0F4F799"}
 */
function validateCity(record) {
	if (!scopes.globals.mpIsEmpty(record.city)) {
		var regex = new RegExp('^[a-zA-Z][a-zA-Z\\s\\\']+$');
		if (!regex.test(record.city)) throw new Error('- Inserire solo lettere nel campo Città');
	}
}

/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"1C86A7DF-3F9E-4D1B-BE5A-8A83BE2900A5"}
 * @AllowToRunInFind
 */
function validateNumberCode(record) {
	if (scopes.globals.mpIsEmpty(record.number_code)) {
		throw new Error('- Inserire un Codice Matricola.');
	} else {
		/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
		var mp_users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
		var totUsers = 0;
		var recUser = null;
		/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_insured_users>} */
		var mp_insured_users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_insured_users');
		var totInsuredUsers = 0;
		var progressiveNumber = '';
		var maxLength = 7;
		/** @type {String} */
		var numToContact = '0';
		var difference = 0;
		var numbCode = 0;

		//	utente esiste in mp_users ma NON in mp_insured_users, controllo in mp_users se ha il FEP code!
		if (scopes.globals.userDontExistInInsuredUsers == true) {
			if (mp_users.find()) {
				mp_users.user_id = record.user_id;
				totUsers = mp_users.search();
			}

			if (totUsers == 1) {
				recUser = mp_users.getRecord(1);
				//NON C'E' IL FEP CODE
				if (scopes.globals.mpIsEmpty(recUser.fep_code)) {
					progressiveNumber = record.number_code.toString();
					maxLength = 7;
					/** @type {String} */
					numToContact = '0';
					if (progressiveNumber.length < maxLength) {
						difference = maxLength - progressiveNumber.length;

						for (var k = 0; k < difference; k++) {
							progressiveNumber = numToContact.concat(progressiveNumber);
						}
					}
					numbCode = progressiveNumber;

					/*ricerca in mp_insured_users:
					 * cerco tutti i record tranne quello appena inserito
					 * dove la polizza è quella del record che arriva (340etc... oppure l'altra)
					 * dove il number_code è uguale a quello che arriva dal nuovo record
					 */
					if (mp_insured_users.find()) {
						mp_insured_users.user_insured_id = '!' + record.user_insured_id;
						mp_insured_users.insurance_id = record.insurance_id;
						mp_insured_users.number_code = numbCode;
						totInsuredUsers = mp_insured_users.search();
					}
					//trovato.. non va bene allora
					if (totInsuredUsers > 0) throw new Error('- Il numero di matricola inserito esiste già, inseriscine uno diverso');

				}//C'E' IL FEP CODE
				else {
					//Non devo fare nessun controllo

					//					progressiveNumber = recUser.fep_code.toString();
					//
					//					if (progressiveNumber.length < maxLength) {
					//						difference = maxLength - progressiveNumber.length;
					//
					//						for (var i = 0; i < difference; i++) {
					//							progressiveNumber = numToContact.concat(progressiveNumber);
					//						}
					//					}
					//					numbCode = progressiveNumber;
				}
			} else {
				throw new Error('- L\'utente sembra esserci in mp_users ma non riesco a recuperarlo; contattare l\'Amministratore.');
			}
		}// NUOVA UTENZA SIA IN USERS CHE IN INSURED_USERS
		else {
			progressiveNumber = record.number_code.toString();
			maxLength = 7;
			/** @type {String} */
			numToContact = '0';
			if (progressiveNumber.length < maxLength) {
				difference = maxLength - progressiveNumber.length;

				for (var j = 0; j < difference; j++) {
					progressiveNumber = numToContact.concat(progressiveNumber);
				}
			}
			numbCode = progressiveNumber;

			/*ricerca in mp_insured_users:
			 * cerco tutti i record tranne quello appena inserito
			 * dove la polizza è quella del record che arriva (340etc... oppure l'altra)
			 * dove il number_code è uguale a quello che arriva dal nuovo record
			 */
			if (mp_insured_users.find()) {
				mp_insured_users.user_insured_id = '!' + record.user_insured_id;
				mp_insured_users.insurance_id = record.insurance_id;
				mp_insured_users.number_code = numbCode;
				totInsuredUsers = mp_insured_users.search();
			}

			//trovato.. non va bene allora
			if (totInsuredUsers > 0) throw new Error('- Il numero di matricola inserito esiste già, inseriscine uno diverso');
		}
	}
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"1E5CEB4F-3E49-4A92-9CAA-4E956BB2E0CB"}
 */
function validateZipCode(record) {
	var regex=new RegExp(/^[0-9]+$/);	
	if (!scopes.globals.mpIsEmpty(record.zip_code)) {
		if (record.zip_code.length != 5) {
			throw new Error('- Il campo CAP deve essere di 5 cifre');
		}
		else{
			if(!regex.test(record.zip_code)){
				throw new Error('- Il campo CAP è composto da soli numeri');
			}
		}	
	}
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"84045763-57C8-413C-B70E-FFEFFD413FF1"}
 */
function validateEmail(record) {
	if (scopes.globals.mpIsEmpty(record.email)){ 		
		throw new Error('- Il campo Email è obbligatorio');
	}
	else{
		//  /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
		var regex=new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
		if (!regex.test(record.email)) throw new Error('- Formato email inserito non valido');
	}
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"90FF44DE-611D-4C96-919B-12FF044B5915"}
 * @AllowToRunInFind
 */
function validateAddress(record) {
	if (!scopes.globals.mpIsEmpty(record.address)) {
		var regex=new RegExp(/^[a-zA-Z\\'\s]*$/);
		if (!regex.test(record.address)) throw new Error('- Inserire solo lettere,spazi o apici nel campo Indirizzo');
	}
}
/**
* Valida il numero civico solo numeri e lettere consentiti (es: 4a)
* @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
* @throws {Error}
*
*@properties={typeid:24,uuid:"4F1A202B-F860-4E02-96D1-4A0A6B60D25E"}
*/
function validateStreetNumber(record) {
	if (!scopes.globals.mpIsEmpty(record.street_number)) {
		var regex=new RegExp(/^[1-9]{1}[0-9]{0,3}[a-zA-Z]{0,1}$/);
		if (!regex.test(record.street_number)) throw new Error('- Inserire solo numeri(massimo 4) oppure numeri seguiti da una lettera nel campo Civico');
	}
}
/**
 * TODO capire bene come impostare
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"328CB726-B240-4AAF-A239-16F591745385"}
 * @AllowToRunInFind
 */
function processNumberCode(record) {
	//	var progressiveNumber = record.number_code.toString();
	//	var maxLength = 7;
	//	/** @type {String} */
	//	var numToContact = '0';
	//	if (progressiveNumber.length < maxLength) {
	//		var difference = maxLength - progressiveNumber.length;
	//
	//		for (var j = 0; j < difference; j++) {
	//			progressiveNumber = numToContact.concat(progressiveNumber);
	//		}
	//	}
	//	record.number_code = progressiveNumber;
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
	var mp_users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
	var totUsers = 0;
	var recUser = null;
	var progressiveNumber = '';
	var maxLength = 7;
	/** @type {String} */
	var numToContact = '0';
	var difference = 0;

	//	utente esiste in mp_users ma NON in mp_insured_users, controllo in mp_users se ha il FEP code!
	if (scopes.globals.userDontExistInInsuredUsers == true) {
		if (mp_users.find()) {
			mp_users.user_id = record.user_id;
			totUsers = mp_users.search();
		}

		if (totUsers == 1) {
			recUser = mp_users.getRecord(1);
			//NON C'E' IL FEP CODE
			if (scopes.globals.mpIsEmpty(recUser.fep_code)) {
				progressiveNumber = record.number_code.toString();
				maxLength = 7;
				/** @type {String} */
				numToContact = '0';
				if (progressiveNumber.length < maxLength) {
					difference = maxLength - progressiveNumber.length;

					for (var k = 0; k < difference; k++) {
						progressiveNumber = numToContact.concat(progressiveNumber);
					}
				}
				record.number_code = progressiveNumber;
			}//C'E' IL FEP CODE
			else {
				progressiveNumber = recUser.fep_code.toString();

				if (progressiveNumber.length < maxLength) {
					difference = maxLength - progressiveNumber.length;

					for (var i = 0; i < difference; i++) {
						progressiveNumber = numToContact.concat(progressiveNumber);
					}
				}
				record.number_code = progressiveNumber;
			}
		}
	}// NUOVA UTENZA SIA IN USERS CHE IN INSURED_USERS
	else {
		progressiveNumber = record.number_code.toString();
		maxLength = 7;
		/** @type {String} */
		numToContact = '0';
		if (progressiveNumber.length < maxLength) {
			difference = maxLength - progressiveNumber.length;

			for (var j = 0; j < difference; j++) {
				progressiveNumber = numToContact.concat(progressiveNumber);
			}
		}
		record.number_code = progressiveNumber;
	}
}

/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"8BFF4C5F-F7E2-4BD0-8C2F-550AC7ED7530"}
 * @AllowToRunInFind
 */
function processBenefitPackage(record) {
	if (record.isNew()) {
		record.benefit_package = 'SPIND';
	}
}

/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"16DD0EB7-9AB9-4791-9E2A-5294B64E8819"}
 * @AllowToRunInFind
 */
function processUniqueUsername(record) {
	var username;
	var first = record.first_name.toLowerCase();
	first = globals.mpTrimStringNew(first);
	var last = record.last_name.toLowerCase();
	last = globals.mpTrimStringNew(last);
	username = first + "." + last;
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
	var mp_users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
	if (record.user_id == null) {
		application.output('username nuovo utente: ' + username);
		mp_users.newRecord();
		record.user_id = mp_users.user_id;
		if (record.company_id == 1) {
			mp_users.user_password = globals.mpSha1('spindox');
			mp_users.company_name = 'SPINDOX';
		} else {
			mp_users.user_password = globals.mpSha1('generali');
			mp_users.company_name = 'GENERALI';
		}
		mp_users.user_name = username;
		application.output(mp_users.user_name);
		record.real_name = record.last_name + ' ' + record.first_name;
		mp_users.real_name = record.real_name;
		application.output(mp_users.real_name);
	} else {
		application.output('username utente esistente: ' + username);
		//cerco e modifico real_name in mp_users
		if (mp_users.find()) {
			mp_users.user_id = record.user_id;
			var totUsers = mp_users.search();

			if (totUsers > 0) {
				var recUsers = mp_users.getRecord(1);
				//se sono io quello che sto modificando, risetto la variabile globale
				if (globals.mpCurrentUserId == recUsers.user_id) globals.mpCurrentUserDisplayName = record.last_name + ' ' + record.first_name;
				recUsers.real_name = record.last_name + ' ' + record.first_name;
			}
		}
		//modifico real_name in mp_insured_users
		record.real_name = record.last_name + ' ' + record.first_name;
	}
}

/**
 * Associo il ruolo "Assicurato" durante la creazione di una nuova anagrafica
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"9F664ADA-004C-4D6B-9D7A-A47F4D3CC464"}
 * @AllowToRunInFind
 */
function processInsuredUserRole(record) {
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users_roles>} */
	var user_roles = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users_roles');
	if (user_roles.find()) {
		user_roles.user_id = record.user_id;
		user_roles.role_id = 3;
		user_roles.search();
	}
	if (user_roles.getSize() == 0) {
		user_roles.newRecord();
		user_roles.user_id = record.user_id;
		user_roles.role_id = 3;
	}
}

/**
 * Assegno automaticamente il numero di polizza 340028012 a ogni assicurato se non ci pensa l'utente
 *
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_users>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"81561DF5-0867-4AE6-9B3B-D3A052F53B53"}
 */
function processInsurancePolicy(record) {
	if (scopes.globals.mpIsEmpty(record.insurance_id)) {
		record.insurance_id = 1;
		//		/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_insured_users>} */
		//		var insuredUsers = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_insured_users');
		//		var totSearch = 0;
		//		if (insuredUsers.find()) {
		//			insuredUsers.user_id = record.user_id;
		//			totSearch = insuredUsers.search();
		//		}
		//
		//		if (totSearch == 1) {
		//			var recInsuredUsers = insuredUsers.getRecord(1);
		//			recInsuredUsers.insurance_id = 1;
		//		}
	}
}
