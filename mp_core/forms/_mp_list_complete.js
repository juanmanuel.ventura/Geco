/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"1631F987-CB8F-4C5C-AD8A-C17BDEBB4A83"}
 */
function updateUI(event) {
	elements.buttonAdd.visible = isEditing();
	elements.buttonDelete.visible = isEditing();
}
