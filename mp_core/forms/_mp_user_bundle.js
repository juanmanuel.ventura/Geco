/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} [formFilterName]
 * @param {String} formListName
 * @param {String} formDetailsName
 * @param {String} [relation]
 * @protected
 *
 * @properties={typeid:24,uuid:"D52729C1-FDB5-4CB8-824B-4870C12C4B74"}
 */
function initialize(event, formFilterName, formListName, formDetailsName, relation) {
	try {
		// set navigator
		if (elements.split.dividerLocation) {
			elements.split.dividerSize = 0;
			elements.split.dividerLocation = 48;
		}
	} catch (e) {
//		application.output(e);
//		application.output('errore initialize');
	}

	// load the filter
	if (formFilterName && forms[formFilterName]) {
		elements.split.setLeftForm(forms[formFilterName]);
		// force to apply filter (seems not to work on form's onLoad event)
		if (forms[formFilterName].applyFilter) forms[formFilterName].applyFilter();
	}

	// load the list
	elements.split.setRightForm(forms[formListName]);
	if (elements.tabless.removeAllTabs()) {
		elements.tabless.addTab(forms[formDetailsName], null, null, null, null, null, null, relation || null, null);
	}

}
