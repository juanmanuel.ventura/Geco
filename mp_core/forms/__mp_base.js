/**
 * @properties={typeid:35,uuid:"E9510872-4E97-454C-B999-3B28E7A03956",variableType:-4}
 */
var saveResult = true;

/**
 * Base method to begin an in-memory transaction
 *
 * @param {JSEvent} event The event that triggered the action.
 * @return {Boolean} true if started the transaction, false if not
 * @properties={typeid:24,uuid:"67F4F5E4-7D65-420F-A216-76C8827E92CE"}
 */
function startEditing(event) {
	application.output(globals.mpMessageLog + 'START __mp_base.startEditing() ',LOGGINGLEVEL.DEBUG);
	// begin in-memory transaction
	if (databaseManager.setAutoSave(false)) {
		updateUI(event);
		application.output(globals.mpMessageLog + 'STOP __mp_base.startEditing() OK',LOGGINGLEVEL.DEBUG);
		return true;
	}
	application.output(globals.mpMessageLog + 'STOP __mp_base.startEditing() KO',LOGGINGLEVEL.DEBUG);
	return false;
}

/**
 * Base method to save outstanding edits. This action does NOT close the in-memory transaction.
 * A form's validate() method is called Prior to saving data. Data is not saved when validation fails.
 *
 * @param {JSEvent} event The event that triggered the action.
 * @returns {Boolean} true if form was validated and edits were saved.
 * @properties={typeid:24,uuid:"3D9F34EE-50EC-43E3-8EAA-DE90AE2B2AFC"}
 */
function saveEdits(event) {
	application.output(globals.mpMessageLog + 'START __mp_base.saveEdits',LOGGINGLEVEL.DEBUG);
	// validation occurs at entity level
	if (databaseManager.saveData()) {
		// force to recalculate all calculated fields. IMPORTANT!
		databaseManager.recalculate(foundset);
		// close transactions
		stopEditing(event);
		// record was saved successfully
		application.output(globals.mpMessageLog + 'STOP __mp_base.saveEdits',LOGGINGLEVEL.DEBUG);
		return true;
	} else {
		application.output(globals.mpMessageLog + 'STOP __mp_base.saveEdits ERROR saveEdits failed ' + globals.mpValidationExceptionMessage,LOGGINGLEVEL.DEBUG);
		globals.DIALOGS.showErrorDialog('Error', globals.mpValidationExceptionMessage, 'OK');
		updateUI(event);
		// Failed to save data
		return false;
	}
}

/**
 * @param event
 * @properties={typeid:24,uuid:"57FDE4A9-7A04-4F74-B4A7-915FECEDAFBD"}
 */
function processSave(event) {
	application.output(globals.mpMessageLog + 'START __mp_base.processSave()',LOGGINGLEVEL.DEBUG);
	// dismiss any remaining busy dialog
	application.updateUI();

	try {
		// validation occurs at entity level
		if (databaseManager.saveData()) {
			// force to recalculate all calculated fields. IMPORTANT!
			databaseManager.recalculate(foundset);
			// close transactions
			stopEditing(event);
			// record was saved successfully
			saveResult = true;
		} else {
			application.output(globals.mpMessageLog + ' __mp_base.processSave() ERROR',LOGGINGLEVEL.DEBUG);
			throw new Error
		}
	} catch (e) {
		application.output(globals.mpMessageLog + ' __mp_base.processSave() ERROR ' +e.message,LOGGINGLEVEL.DEBUG);
		application.output(globals.mpMessageLog + ' __mp_base.processSave() ERROR ' +e,LOGGINGLEVEL.DEBUG);
		updateUI(event);
		saveResult = false; // Failed to save data
	} finally {
		//plugins.busy.unblock();
	}
	application.output(globals.mpMessageLog + 'STOP __mp_base.processSave()',LOGGINGLEVEL.DEBUG);
}

/**
 * Base method to close an in-memory transaction.
 * All edits are rolled back, so a save should have already been called
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"10A95259-A5E9-46F7-8A32-FA9BF9710AD8"}
 */
function stopEditing(event) {
	application.output(globals.mpMessageLog + 'START __mp_base.stopEditing()',LOGGINGLEVEL.DEBUG);
	// revert edits only if in editing
	if (isEditing()) databaseManager.revertEditedRecords();
	// close in-memory transaction
	databaseManager.setAutoSave(true);
	// MVC: update the view based on the model
	updateUI(event);
	application.output(globals.mpMessageLog + 'STOP __mp_base.stopEditing()',LOGGINGLEVEL.DEBUG);
}

/**
 * Handle record selected. Invokes the updateUI() method
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"FAAAFD70-58C2-432C-8480-57FE67B4830D"}
 */
function onRecordSelection(event) {
	updateUI(event);
}

/**
 * Callback method for when form is shown. Invokes the updateUI() method
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"6A6FBBA0-57FD-433D-AC04-D1F5047313C6"}
 */
function onShow(firstShow, event) {
//	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
//		plugins.busy.prepare();
//	}
	updateUI(event);
}

/**
 * Default handler for record edit stop event. (record save)
 * Invokes the updateUI() method.
 * @param {JSRecord} record that record was being edited
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"26517656-E143-4593-988F-E49CAF88F385"}
 */
function onRecordEditStop(record, event) {
	updateUI(event);
}

/**
 * Deletes a record and optionally closes an in-memory transaction.
 * Implementations should probably confirm the delete in the UI.
 * Default action is to close the transaction
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} [index] The index of the record to delete. Default is selected index.
 * @returns {Boolean} true when record was deleted from database
 *
 * @properties={typeid:24,uuid:"E1F70F1C-2F64-4286-9FAF-21312B05E224"}
 */
function deleteRecord(event, index) {
	application.output(globals.mpMessageLog + 'START __mp_base.deleteRecord() ',LOGGINGLEVEL.DEBUG);
	index = (index) ? index : foundset.getSelectedIndex();
	try{
		foundset.deleteRecord(index);
		 //close in-memory transaction
		stopEditing(event);
		application.output(globals.mpMessageLog + 'STOP __mp_base.deleteRecord() ',LOGGINGLEVEL.DEBUG);
		return true;
	}
	catch(e){
		application.output(globals.mpMessageLog + 'STOP __mp_base.deleteRecords() ERROR ' + e.message,LOGGINGLEVEL.DEBUG);
		globals.DIALOGS.showErrorDialog('Error', globals.mpValidationExceptionMessage, 'OK');
		return false;
	
	}
//	if (foundset.deleteRecord(index)) {
//		// close in-memory transaction
//		stopEditing(event);
//		application.output(globals.messageLog + 'STOP _base.deleteRecord() ',LOGGINGLEVEL.DEBUG);
//		return true;
//	} else {
//	application.output(globals.messageLog + 'STOP _base.deleteRecord() Delete failed ',LOGGINGLEVEL.DEBUG);
//	return false;
//	}
}

/**
 * Delete record
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Array<JSRecord>} records and array containing the records to delete
 * @returns {Boolean} true when record was deleted from database
 *
 * @properties={typeid:24,uuid:"6F2978C3-A23D-4DC6-9374-BBE0ABC705DA"}
 */
function deleteRecordsAtOnce(event, records) {
	application.output(globals.mpMessageLog + 'START __mp_base.deleteRecordsAtOnce()',LOGGINGLEVEL.DEBUG);
	if (records && records.length > 0) {
		databaseManager.setAutoSave(false);
		for (var index = 0; index < records.length; index++) {
			//workaround to avoid exception when deleting
			try {
				foundset.deleteRecord(records[index]);
			} catch (e) {
				globals.mpDeleteExceptionMessage +='\n'+globals.mpValidationExceptionMessage;
				application.output(globals.mpMessageLog + 'STOP __mp_base.deleteRecordsAtOnce() ERROR ' + e.message,LOGGINGLEVEL.DEBUG);
				application.output(globals.mpMessageLog + 'STOP __mp_base.deleteRecordsAtOnce() ERROR ' + e,LOGGINGLEVEL.DEBUG);
			}

		}
		databaseManager.setAutoSave(true);
		application.output(globals.mpMessageLog + 'STOP __mp_base.deleteRecordsAtOnce() OK',LOGGINGLEVEL.DEBUG);
		return true
	}
	application.output(globals.mpMessageLog + 'STOP __mp_base.deleteRecordsAtOnce() KO',LOGGINGLEVEL.DEBUG);
	return false;
}

/**
 * Will not enter find if already editing or record is invalid
 *
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean} true when the form entered find mode
 * @properties={typeid:24,uuid:"8EB26D82-67B0-4896-BF2F-723EDA38F57C"}
 */
function startFind(event) {
	if (!isEditing() && foundset.find()) {
		return updateUI(event); // updateUI always returns true
	}
	application.output("Find failed");
	return false;
}

/**
 * Adds a new record. It opens an in-memory transaction.
 * New records cannot be added if an invalid state exists outside of a transaction
 *
 * @param {JSEvent} event the event that triggered the action
 * @returns {Number} The index of the record that was added
 *
 * @properties={typeid:24,uuid:"01281748-BACE-4DBD-BEED-524BF49DD981"}
 */
function newRecord(event) {
	// start in-memory transaction
	if (startEditing(event)) {
		// create a new record
		return foundset.newRecord(false);
	}
	return null;
}

/**
 * Base method to perform search.
 *
 * @param {Boolean} [clear] clear last results
 * @param {Boolean} [reduce] reduce search
 * @param {JSEvent} event the event that triggered the action
 * @returns {Number} The number of records cached from the result.
 *
 * @properties={typeid:24,uuid:"2959F79E-7B95-457B-BB26-3C8E4238D623"}
 * @AllowToRunInFind
 */
function search(clear, reduce, event) {
	var results = 0;
	if (foundset.isInFind()) {
		results = controller.search(clear, reduce);
		updateUI(event);
	}
	return results;
}

/**
 * Shows all records.
 * Action is disallowed when inside an in-memory transaction or when form is not valid.
 *
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean} True when the show-all was successful
 *
 * @properties={typeid:24,uuid:"00CBD9B3-8C9C-4039-AE42-8B8436EC0019"}
 */
function showAllRecords(event) {
	if (!isEditing()) {
		return foundset.loadAllRecords();
	}
	application.output("Show all records failed")
	return false;
}

/**
 * Sorts the foundset based on the dataprovider and sort string
 * Action is disallowed when inside an in-mem transaction or when form is not valid
 *
 * @param {String} dataProviderID element data provider
 * @param {Boolean} asc sort acscending [true] or descending [false]
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"DCEE0EC0-F32C-479D-8F2E-3BE0C492D6B2"}
 */
function onSort(dataProviderID, asc, event) {
	if (!isEditing()) {
		foundset.sort(dataProviderID + (asc ? ' asc' : ' desc'), false);
	}
}

/**
 * Base method to determine if a form is editing.
 * This is a convenience method for checking auto-save.
 *
 * @returns {Boolean} True if the form is editing
 * @properties={typeid:24,uuid:"CC21C788-249B-454D-A3C9-697CECCE5E4C"}
 */
function isEditing() {
	// Check Auto-Save state
	return !databaseManager.getAutoSave();
}

/**
 * This method updates the form appearance based on the state of the model.
 * Called any time the state of the model may have changed.
 * Including: onRecordSelection, onShow, onRecordEditStop
 * Implementation forms should override this method to update the user interface.
 * Implementation forms should call this method when the data model is programmatically changed.
 *
 * Updates visible forms in tabpanels and splitpanes
 *
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"71296E59-DB1A-42EF-9377-62BCD3AC014F"}
 */
function updateUI(event) {
	var form = null;

	// Iterate over elements
	for (var i = 0; i < elements.length; i++) {
		var e = elements[i];
		// get the element type
		var type = (e.getElementType) ? e.getElementType() : null;
		
		switch (type) {
		// tab panels
		case ELEMENT_TYPES.TABPANEL:
			// get the selected tab form
			form = forms[elements[i].getTabFormNameAt(elements[i].tabIndex)];
			// update its UI
			if (form && form['updateUI']) form['updateUI']();
			break;

		case ELEMENT_TYPES.SPLITPANE:
			// update left form UI
			form = elements[i].getLeftForm();
			if (form && form['updateUI']) form['updateUI']();
			// update right form UI
			form = elements[i].getRightForm();
			if (form && form['updateUI']) form['updateUI']();
			break;
		}
	}
}