/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"EC4D4B67-8137-45DC-AB59-D9F7E4F1B94A"}
 */
function updateUI(event) {
	_super.updateUI(event);
	elements.buttonAdd.visible = !isEditing();
	elements.buttonDelete.visible = !isEditing();
	elements.footer.visible = !isEditing();
}
