/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @properties={typeid:24,uuid:"47ABCE43-B248-4523-A0C0-16BAF909E914"}
 */
function deleteRecord(event, index) {
	
	var answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai per eliminare un record.\nQuesta operazione non può essere annullata", "Elimina", "Annulla");
	if (answer == "Elimina") {
		
		return _super.deleteRecord(event,index);
	}
	return false;
}
