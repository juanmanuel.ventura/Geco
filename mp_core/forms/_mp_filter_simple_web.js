/**
 * @type {Number}
 * @protected
 * @properties={typeid:35,uuid:"58CE4C3D-8416-43D5-97CE-669DD70F7CD3",variableType:4}
 */
var isEnabled = 1;

/**
 * @properties={typeid:24,uuid:"A1F549F3-3196-4A9C-913B-233E50504280"}
 * @AllowToRunInFind
 */
function filterEnabledRecords() {
	// filter for entities that implement the field "is_enabled"
	if (foundset.alldataproviders.indexOf('is_enabled') != -1) {
		foundset.removeFoundSetFilterParam('enabled');
		foundset.addFoundSetFilterParam('is_enabled', '=', isEnabled, 'enabled');
		foundset.loadAllRecords();
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} [goTop] true if the cursor must be placed at the first index
 * @properties={typeid:24,uuid:"D9BCE0BD-9E63-4885-997E-EA90FA419179"}
 */
function resetFilter(event, goTop) {
	foundset.loadAllRecords();
	// move to the first record (otherwise the cursor remains at the last selected record)
	if (goTop) foundset.setSelectedIndex(1);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"6CE876D5-7A77-418F-9689-F276BAF5F2A7"}
 */
function onLoad(event) {
	filterEnabledRecords();
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"5375C1A7-20D7-4677-A1E4-CC95E5F3F358"}
 */
function updateUI(event) {
	_super.updateUI(event);
	controller.enabled = !isEditing();
	//elements.imageHeader.visible = !isEditing();
}
