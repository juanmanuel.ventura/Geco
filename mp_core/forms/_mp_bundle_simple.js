/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formDetailsName
 * @param {String} [relation]
 * @properties={typeid:24,uuid:"1992110B-DCEA-4999-A6D7-3027AC114342"}
 */
function initialize(event, formDetailsName, relation) {
	application.output(globals.mpMessageLog + 'START _mp_bundle_simple.initialize() insured_bundle formDetailsName: ' + formDetailsName, LOGGINGLEVEL.INFO);
		if (elements.tabless.removeAllTabs()) {
			if (forms[formDetailsName]) {
				elements.tabless.addTab(forms[formDetailsName], null, null, null, null, null, null, relation || null, null);
			} else application.output(globals.mpMessageLog + '_mp_bundle_simple.initialize() errore sul form ' + formDetailsName, LOGGINGLEVEL.ERROR);
		}
		application.output(globals.mpMessageLog + 'STOP _mp_bundle_simple.initialize()', LOGGINGLEVEL.INFO);
}
