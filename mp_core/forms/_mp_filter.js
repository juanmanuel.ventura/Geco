/**
 * @type {Boolean}
 * @private
 * @properties={typeid:35,uuid:"9075AFFB-97EA-4B0F-8772-34C245993EF9",variableType:-4}
 */
var isExpanded = false;

/**
 * @type {Number}
 * @protected
 * @properties={typeid:35,uuid:"651362F5-D708-401B-A06A-950170828DD3",variableType:4}
 */
var isEnabled = 1;

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} reset true to reset the form
 * @protected
 * @properties={typeid:24,uuid:"6FD85282-1F3A-45CB-AE5F-DC725066F61B"}
 */
function toggleHeader(event, reset) {
	
	// get the bundle
	var mainForm = forms[controller.getFormContext().getValue(2, 2)] || null;

	// get the split pane
	/** @type {RuntimeSplitPane} */
	var split = mainForm.elements['split'];

	// reset and quit
	if (reset && split) {
		split.dividerLocation = 48;
		elements.buttonToggle.imageURL = "media:///filter/arrow_down.png";
		application.updateUI();
		return;
	}

	if (split && split.getElementType() == ELEMENT_TYPES.SPLITPANE) {
		if (isExpanded) {
			split.dividerLocation = 48;
			elements.buttonToggle.imageURL = "media:///filter/arrow_down.png";
		} else {
			split.dividerLocation = controller.getPartHeight(JSPart.BODY);
			elements.buttonToggle.imageURL = "media:///filter/arrow_up.png";
		}
		isExpanded = !isExpanded;
	}
}

/**
 * @properties={typeid:24,uuid:"C0BE3302-6F9D-4E2A-82A3-B67845FC8558"}
 * @AllowToRunInFind
 */
function filterEnabledRecords() {
	// filter for entities that implement the field "is_enabled"
	if (foundset.alldataproviders.indexOf('is_enabled') != -1) {
		foundset.removeFoundSetFilterParam('enabled');
		foundset.addFoundSetFilterParam('is_enabled', '=', isEnabled, 'enabled');
		foundset.loadAllRecords();
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} [goTop] true if the cursor must be placed at the first index
 * @properties={typeid:24,uuid:"8AF46D2A-74DF-4B05-B7FB-196348B2966E"}
 */
function resetFilter(event, goTop) {
	foundset.loadAllRecords();
	// move to the first record (otherwise the cursor remains at the last selected record)
	if (goTop) foundset.setSelectedIndex(1);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"EA7E3ACB-9341-4F48-9A5C-2864FCD801CF"}
 */
function onLoad(event) {
	filterEnabledRecords();
}

/**
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"9910BBD8-9268-47DD-B9E5-99099F4FAC33"}
 */
function onShow(firstShow, event) {
	toggleHeader(event, true);
	_super.onShow(firstShow, event);
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"9BF20345-EF0D-4A7E-921B-762D14E3CB1D"}
 */
function updateUI(event) {
	_super.updateUI(event);
	controller.enabled = !isEditing();
	elements.imageBody.visible = !isEditing();
	elements.imageHeader.visible = !isEditing();
}
