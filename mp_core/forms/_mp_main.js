/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"D6784877-76A8-4521-AC80-AD7D88415825",variableType:-4}
 */
var showMenu = false;

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"E7E1A6DA-9414-4A04-A4D8-34218BCA5A31"}
 * @AllowToRunInFind
 */
function initialize(event) {
	application.output(globals.mpMessageLog + 'START _mp_main.initialize() ', LOGGINGLEVEL.INFO);
	globals.mpLoadRunnableRules();
	// prepare windows
	globals.mpPrepareWindow(event.getFormName());
	// set current user info
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
	var fs = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
	if (fs.find()) {
		application.output('----------------------globals.isFromMedicalPractice: ' + globals.isFromMedicalPractice);
		if (globals.isFromMedicalPractice) fs.user_id = security.getUserUID();
		else fs.geco_user_id = security.getUserUID();
		application.output('----------fs.user_id: ' + fs.user_id + ', fs.geco_user_id: ' + fs.geco_user_id);
		var doSearch = fs.search();
		application.output('---------sql: ' + databaseManager.getSQL(fs));
		if (doSearch != 0) {
			application.output(globals.mpMessageLog + '_mp_main.initialize() Utente trovato tramite user_id: ' + security.getUserUID(), LOGGINGLEVEL.INFO);
			globals.mpCurrentUserDisplayName = fs.real_name;
			globals.mpCurrentUserId = fs.user_id;
		} else {
			application.output(globals.mpMessageLog + '_mp_main.initialize() KO; UTENTE NON TROVATO: ' + security.getUserUID(), LOGGINGLEVEL.INFO);
			var strDialog = globals.DIALOGS.showErrorDialog('ERROR', 'Non hai i permessi necessari per accedere a questa applicazione', 'OK');
			if (strDialog == 'OK') logOut(event);
		}
	}
	application.output(globals.mpMessageLog + 'STOP _mp_main.initialize() ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"C8311118-5BFC-40AC-9F49-20F8E673573E"}
 */
function toggleMenu(event) {
	plugins.window.getMenuBar().setVisible(showMenu);
	showMenu = !showMenu;
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"268E0167-1F21-469E-9987-517D776F3F3E"}
 * @protected
 */
function setSelector(event) {
	if (event.getType() == JSEvent.ACTION) {
		// move the selector
		/** @type {RuntimeLabel} */
		var label = event.getSource();
		elements.selector.setLocation(label.getLocationX(), label.getLocationY());
		elements.selector.setSize(label.getWidth(), label.getHeight());
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"070BBA9E-BCCB-4472-B12E-3BF53B1E2C6E"}
 */
function logOut(event) {
	application.output(globals.mpMessageLog + 'START _mp_main.logOut() from ' + application.getSolutionName(), LOGGINGLEVEL.INFO);
	var serverUrl = application.getServerURL();
	application.output(globals.mpMessageLog + '_mp_main.logOut() serverUrl ' + serverUrl, LOGGINGLEVEL.INFO);
	var solutionName = application.getSolutionName();
	if (!isEditing()) {
		application.closeAllWindows();
		if (serverUrl != 'http://localhost:8080') application.showURL(serverUrl + '/servoy-webclient/application/solution/' + solutionName, '_self');
		security.logout(solutionName);
		application.output(globals.mpMessageLog + 'STOP _mp_main.logOut()', LOGGINGLEVEL.INFO);
	} else {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!","OK");
		application.output(globals.messageLog + 'STOP _mp_main.logOut() tentato logout in editing: annullato', LOGGINGLEVEL.INFO);
		return;
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formName the event that triggered the action
 * @properties={typeid:24,uuid:"A5A36816-E6E3-4D05-9ED1-B03951BA3FD6"}
 */
function loadPanel(event, formName) {
	application.output(globals.mpMessageLog + 'START _mp_main.loadPanel() ' + formName, LOGGINGLEVEL.INFO);
	// if in editing mode DO NOT move away!
	if (isEditing()) {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!", "OK");
		return;
	}

	// move selector
	setSelector(event);

	if (elements.tabless.removeAllTabs()) {
		if (forms[formName]) {
			elements.tabless.addTab(forms[formName], null, null, null, null, null, null, null, -1);
		} else application.output(globals.mpMessageLog + '_mp_main.loadPanel() KO: errore sul form ' + formName, LOGGINGLEVEL.INFO);
	}
	application.output(globals.mpMessageLog + 'STOP _mp_main.loadPanel() ' + formName, LOGGINGLEVEL.INFO);

}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"D99831C7-AB12-4896-8555-AF85FB36A58A"}
 * @AllowToRunInFind
 */
function openChangePassword(event) {
	application.output(globals.mpMessageLog + 'START _mp_main.openChangePassword() ', LOGGINGLEVEL.INFO);
	if (isEditing()) {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!", "OK");
		return;
	}

	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
	var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');

	if (users.find()) {
		users.user_id = scopes.globals.mpCurrentUserId;
		var tot = users.search();

		if (tot >= 1) {
			var rec = users.getRecord(1);
			if (rec.company_name == 'SPINDOX' && rec.geco_user_id != null) {
				scopes.globals.DIALOGS.showErrorDialog('Error', 'Sei di Spindox, effettua il cambio password da GeCo.', 'OK');
				return;
			}
		}
	}
	var win = application.createWindow("selezione", JSWindow.MODAL_DIALOG);
	globals.mpCallerForm = forms[event.getFormName()];
	win.title = 'Cambio Password';
	win.show(forms.mp_change_password);
	application.output(globals.mpMessageLog + 'STOP _mp_main.openChangePassword() ', LOGGINGLEVEL.INFO);
}
/**
 * @param {JSEvent} event
 * @param {String} solutionName
 *
 * @properties={typeid:24,uuid:"0DB5BDBB-A185-475A-B99F-7F159C6F36AA"}
 */
function goToSolution(event, solutionName) {
	application.output(globals.mpMessageLog + 'START _mp_main.goToSolution() ', LOGGINGLEVEL.INFO);
	if (isEditing()) {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!", "OK");
		return;
	}
	application.output(globals.mpMessageLog + '_mp_main.goToSolution() apri Solution ' + solutionName, LOGGINGLEVEL.INFO);
	var serverUrl = application.getServerURL();
	application.output(globals.mpMessageLog + '_mp_main.goToSolution() serverUrl ' + serverUrl, LOGGINGLEVEL.INFO);
	if (solutionName.indexOf('mp') > -1) {
		serverUrl = globals.mpGetServerUrlMedicalPractices(serverUrl);
		serverUrl = serverUrl + '/servoy-webclient/application/solution/' + solutionName;
	} else {
		serverUrl = globals.getServerUrlGeco(serverUrl);
		serverUrl = serverUrl + '/servoy-webclient/application/solution/geco_' + solutionName;
	}
	application.showURL(serverUrl, '_self');
	application.output(globals.mpMessageLog + '_mp_main.goToSolution() serverUrl2 ' + serverUrl, LOGGINGLEVEL.INFO);
	try {
		application.closeSolution();
	} catch (e) {
		application.output(globals.mpMessageLog + '_mp_main.goToSolution() ERROR: ' + e.message, LOGGINGLEVEL.ERROR);
		application.output(globals.mpMessageLog + '_mp_main.goToSolution() ' + e, LOGGINGLEVEL.ERROR);
	}
	application.output(globals.mpMessageLog + 'STOP _mp_main.goToSolution() ', LOGGINGLEVEL.INFO);
}
