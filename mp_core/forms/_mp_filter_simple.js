/**
 * @type {Number}
 * @protected
 * @properties={typeid:35,uuid:"2D56741F-CCA0-4CAC-8C5F-9CBB10E1D3DB",variableType:4}
 */
var isEnabled = 1;

/**
 * @properties={typeid:24,uuid:"2F5BDD2D-741B-45C4-BA62-4F5C9E0DD463"}
 * @AllowToRunInFind
 */
function filterEnabledRecords() {
	// filter for entities that implement the field "is_enabled"
	if (foundset.alldataproviders.indexOf('is_enabled') != -1) {
		foundset.removeFoundSetFilterParam('enabled');
		foundset.addFoundSetFilterParam('is_enabled', '=', isEnabled, 'enabled');
		foundset.loadAllRecords();
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} [goTop] true if the cursor must be placed at the first index
 * @properties={typeid:24,uuid:"FF830543-C82C-439B-9D15-6F2F6EF98C8F"}
 */
function resetFilter(event, goTop) {
	foundset.loadAllRecords();
	// move to the first record (otherwise the cursor remains at the last selected record)
	if (goTop) foundset.setSelectedIndex(1);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"A3E99148-0D6E-4F19-A101-40120CF61496"}
 */
function onLoad(event) {
	filterEnabledRecords();
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"C2BAC2D6-D671-4ACB-901C-A5357A5A9B6C"}
 */
function updateUI(event) {
	_super.updateUI(event);
	controller.enabled = !isEditing();
	//elements.imageHeader.visible = !isEditing();
}
