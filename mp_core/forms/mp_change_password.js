/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B3B67C25-3168-485E-A016-34CBDD8EB15F"}
 */
var username = globals.mp_currentuserid_to_mp_users.user_name;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3D5A90CB-F884-4C13-8440-45B7269D823F"}
 */
var oldPassword = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9754CB79-0533-4CE6-9006-F555E7571D56"}
 */
var newPassword = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9AFA0E0B-60ED-452E-99E1-E9616E0A0778"}
 */
var repeatNewPassword = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C23E112B-1C4A-4CF2-89F8-5871BC67E282"}
 */
var result = null;

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"7EBB768C-CC30-40D8-9BC3-E7EA6F0D4E1E"}
 */
function closePopUp(event) {
	if (isEditing()) _super.stopEditing(event);
	clearFields();
	var currentWindow = controller.getWindow();
	if (currentWindow != null) {	
		currentWindow.destroy();
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @protected 
 *
 * @properties={typeid:24,uuid:"A7CA448B-EF34-4531-B2C4-9C8C64F731AF"}
 * @AllowToRunInFind
 */
function saveNewPassword(event) {
	application.output(globals.mpMessageLog + 'START mp_change_password.saveNewPassword() '+username,LOGGINGLEVEL.INFO);
	result = null;
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
//	var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
//	if (users.find()){
//		users.user_name = username;
//		var tot = users.search();
//		
//		if (tot >= 1){
//			var rec = users.getRecord(1);
//			if (rec.company_name == 'SPINDOX' && rec.geco_user_id != null){
//				result = 'Sei di Spindox, effettua il reset password da GeCo.';
//				return;
//			}
//		}
//	}
	if (!oldPassword) {
		result = 'Errore: La password non può essere vuota';
	} else if (!newPassword) {
		result = 'Errore: La nuova password non può essere vuota';
	} else if (!repeatNewPassword || repeatNewPassword != newPassword) {
		result = 'Errore: Le due password non corrispondono';
	}
	if (result != null) {
		application.output(globals.mpMessageLog + 'STOP mp_change_password.saveNewPassword() '+result,LOGGINGLEVEL.INFO);
		return;
	}
	
	try {
		scopes.globals.mpChangePassword(username, oldPassword, newPassword);
		close(event);
		var serverUrl = application.getServerURL();
		var solutionName = application.getSolutionName();
		application.output(globals.mpMessageLog + 'mp_change_password.saveNewPassword() serverUrl ' + application.getServerURL(),LOGGINGLEVEL.DEBUG);
		application.showURL(serverUrl + '/servoy-webclient/application/solution/'+solutionName, '_self');
		security.logout(application.getSolutionName());
	} catch (e) {
		result = e['message'];
		application.output(globals.mpMessageLog + 'mp_change_password.saveNewPassword() KO ERROR: ' + e.message,LOGGINGLEVEL.DEBUG);
	}
	application.output(globals.mpMessageLog + 'STOP mp_change_password.saveNewPassword() '+username,LOGGINGLEVEL.INFO);
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A5C7ABC5-3DD7-4069-8018-C22BB23FDB62"}
 */
function onShow(firstShow, event) {
	clearFields()
	_super.startEditing(event);
}

/**
 * Handle hide window.
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"B70723A6-CDCB-4E36-B4F4-2E0708F18859"}
 */
function onHide(event) {
	closePopUp(event);
	return true
}

/**
 *
 * @properties={typeid:24,uuid:"40F2A7B0-A282-463F-BB96-A05686030A6D"}
 */
function clearFields(){
	result = null;
	repeatNewPassword = null;
	newPassword = null;
	oldPassword = null;
}

/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"BB653250-6CBC-4AA4-824F-3558EEC10932"}
 */
function updateUI(event){
	_super.updateUI(event);
	elements.buttonEdit.visible = false;
}