/**
 * @type {Number}
 * @properties={typeid:35,uuid:"CE5EDCBE-4CC8-4728-A25C-6600F5AB644A",variableType:4}
 */
var areAllSelected = 0;

/**
 * Select all records
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"054706D7-39AD-4781-8DEC-F524F35573A7"}
 */
function selectAllRecords(event) {
	for (var index = 1; index <= foundset.getSize(); index++) {
		var rec = foundset.getRecord(index);
		rec['is_selected'] = areAllSelected;
	}
}

/**
 * Perform the single selection.
 *
 * @param {JSEvent} event the event that triggered the action
 * @protected
 *
 * @properties={typeid:24,uuid:"126F5D6F-FE41-46FE-964A-02346798EB58"}
 */
function deselectOthers(event) {
	var me = foundset.getSelectedIndex();
	for (var index = 1; index <= foundset.getSize(); index++) {
		if (index != me && foundset.getRecord(index)['is_selected']) foundset.getRecord(index)['is_selected'] = 0;
	}
	return true;
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"08BF1F29-5B93-4ABA-BC82-D65A6108BC5B"}
 */
function updateUI(event) {
	_super.updateUI(event);
	
	// buttons
	elements.buttonDelete.visible = false;
//	elements.buttonAdd.visible= isEditing();
	if (!isEditing()) {
		areAllSelected = 0;
		selectAllRecords(event);
	}
	
	return true;
}

/**
 * Check if one record is selected
 * @return {Boolean}
 * @properties={typeid:24,uuid:"DDF9C5E2-9CFB-4445-8CC9-0255D578CE2F"}
 */
function hasChanged() {
	if (foundset && foundset.getSize() != 0) {
		for (var index = 1; index <= foundset.getSize(); index++) {
			if (foundset['is_selected']) return true;
		}
	}
	return false;
}

/**
 * Delete records selected
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"4226CF75-25ED-4FD7-B641-E00AC891FF01"}
 */
function deleteRecordsSelected(event) {
	application.output(globals.mpMessageLog + 'START _mp_list_complete_selector.deleteRecordsSelected() ', LOGGINGLEVEL.INFO);
	if (hasChanged()) {
		/** @type {Array<JSRecord>} */
		var recordsToDelete = []
		if (foundset) {
			for (var index = 1; index <= foundset.getSize(); index++) {
				var rec = foundset.getRecord(index);
				if (rec['is_selected']){
					recordsToDelete.push(rec);
				}
			}
			deleteRecordsAtOnce(event, recordsToDelete)
			//application.output('list_complete delete  '+globals.validationExceptionMessage);
		}
	}
	application.output(globals.mpMessageLog + 'STOP _mp_list_complete_selector.deleteRecordsSelected() ', LOGGINGLEVEL.INFO);
}
