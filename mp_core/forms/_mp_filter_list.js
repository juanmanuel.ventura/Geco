/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formFilterName
 * @param {String} formListName
 * @properties={typeid:24,uuid:"374E83FD-7888-4C2F-87B4-460E1648D4EA"}
 */
function onLoad(event, formFilterName, formListName) {
	application.output(globals.mpMessageLog + 'START _mp_dossier bundle.onLoad() form filtro: ' + formFilterName + ', form lista: ' + formListName, LOGGINGLEVEL.INFO);
	var h_filter = 134;

	if (formFilterName && forms[formFilterName]) {

		var form = solutionModel.getForm(formFilterName);
		h_filter = form.getPart(JSPart.BODY).height
	}
	try {
		// set navigator
		if (elements.split.dividerLocation) {
			elements.split.dividerSize = 0;
			elements.split.dividerLocation = h_filter;
		}
	} catch (e) {
		application.output(globals.mpMessageLog + '_mp_dossier bundle.onLoad() KO, errore initialize: ' + e, LOGGINGLEVEL.DEBUG);
	}
	if (formFilterName && forms[formFilterName]) {
		elements.split.setLeftForm(forms[formFilterName]);
		// force to apply filter (seems not to work on form's onLoad event)
		if (forms[formFilterName].applyFilter) forms[formFilterName].applyFilter();
	}
	elements.split.setRightForm(forms[formListName]);
	plugins.WebClientUtils.executeClientSideJS('window.location.reload()');
	application.output(globals.mpMessageLog + 'STOP _mp_dossier bundle.onLoad() ', LOGGINGLEVEL.INFO);
}
