/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"BADD1E83-DDC4-46C7-8248-69D1951B4611"}
 */
function updateUI(event) {
	_super.updateUI(event);
	elements.buttonAdd.visible = !isEditing();
	elements.buttonDelete.visible = !isEditing();
	elements.footer.visible = !isEditing();
	controller.enabled = !isEditing();
}
