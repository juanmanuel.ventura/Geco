/**
 * @param {JSEvent} event the event that triggered the action
 * @protected 
 * @properties={typeid:24,uuid:"647DDE2D-F914-494B-A2BA-DD066762A813"}
 */
function close(event) {
	controller.getWindow().destroy();
}
