/**
 * @type {RuntimeForm}
 *
 * @properties={typeid:35,uuid:"3DA2FCA8-6521-4B6A-85D3-8CEEAEC69201",variableType:-4}
 */
var mpCallerForm = null;

/**
 * @type {Number}
 *
 * Serve per dirmi da quale form sono entrato in editing e da quale foundset prendere il record.
 * foundsetForm = 0 --> foundset di insured_details;
 * foundsetForm = 1 --> foundset di insurer_user_details;
 *
 * @properties={typeid:35,uuid:"92CB9AB2-884E-4FB4-A890-62E1637FEA05",variableType:4}
 */
var foundsetForm = null;

/**
 * @properties={typeid:35,uuid:"DE006E3E-34D4-40DF-993C-605314E42799",variableType:-4}
 */
var mpClient = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F5508CD2-4DD8-462A-BF4B-05D6F21FD143"}
 */
var praticheSanitarieDB = 'pratiche_sanitarie';

/**
 * @type {String} *
 *
 * @properties={typeid:35,uuid:"254D4506-1303-41D6-B213-C79BFB8DCEB8",variableType:-4}
 */
var mpMessageLog = security.getUserUID() ? 'UID: ' + security.getUserUID() + ' - ' : '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"676DFFCC-8F7F-439E-8C95-F6184F87264D"}
 */
var mpValidationExceptionMessage = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3432A282-AAB4-46ED-9ECB-A5D13CE511D1"}
 */
var mpDeleteExceptionMessage = '';

/**
 * @type {Object}
 *
 * @properties={typeid:35,uuid:"53AEE128-46F2-44EB-A6A3-FE72DEA65CA0",variableType:-4}
 */
var mpMailOptions = {
	username: 'noreply@spindox.it',
	password: '$p1nd0xG3c0'
}

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"710F4902-F2BD-4346-BA4E-DFEDDD28EB41",variableType:8}
 */
var mpSelectedUser = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"E2C5D724-FD63-4BE0-83AF-B82C8FE234AA",variableType:8}
 */
var mpCurrentUserId = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0BC15AD0-30A6-4889-86E1-085A7D5EF9AA"}
 */
var mpCurrentUserDisplayName = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"BFBB8E30-49B5-49F8-9E69-2DBCB1F0A75F",variableType:8}
 */
var mpInsuredIdSelected= null;

/**
 *
 * @properties={typeid:35,uuid:"FAD5315C-D9FD-4C20-828F-7EC7D4397386",variableType:-4}
 */
var mpValidationRules = { };

/**
 *
 * @properties={typeid:35,uuid:"9885AD8C-CD1E-428C-8373-B68691B29575",variableType:-4}
 */
var mpProcessRules = { };

/**
 * @type {plugins.file.JSFile}
 *
 * @properties={typeid:35,uuid:"70A23E40-EF6B-4599-9EEB-FEBED81EF9D1",variableType:-4}
 */
var mpServerDir = null;

/**
 * @type {plugins.file.JSFile}
 *
 * @properties={typeid:35,uuid:"ACA70E70-3995-41EA-B714-A28E7A2ED6FB",variableType:-4}
 */
var mpServerfile = null;

/**
 * @type {Boolean}
 *
 * @properties={typeid:35,uuid:"C9653048-2EF6-413B-BA45-1FEFF01CFCBD",variableType:-4}
 */
var userDontExistInInsuredUsers = null;
/**
 * @type {Boolean}
 *
 * @properties={typeid:35,uuid:"F91B32CD-029F-4D00-8819-5BF25A85CCF2",variableType:-4}
 */
var successMPUser = null;

/**
 * @type Boolean
 * @properties={typeid:35,uuid:"506966CD-ABC7-4CB9-B37D-4F5E3879853B",variableType:-4}
 */
var isFromMedicalPractice = false;

/**
 * @type Boolean
 * @properties={typeid:35,uuid:"26E20ED1-E2D2-4C98-8917-C842BBB2EE0B",variableType:-4}
 */
var isInsurer = null;

/**
 * @type Boolean
 * @properties={typeid:35,uuid:"1283B4F6-1FE0-4250-8C87-7BB2B21A2D60",variableType:-4}
 */
var isInsured = null;

/**
 * @type Boolean
 * @properties={typeid:35,uuid:"62D9BC65-3D03-48FE-BD0C-312737920059",variableType:-4}
 */
var isHrAdmin = null;

/**
 * @type Boolean
 * @properties={typeid:35,uuid:"093831D0-F00E-437F-8C5F-013941771BF6",variableType:-4}
 */
var isAdmin = null;

/**
 * @param {String} formName
 * @properties={typeid:24,uuid:"01A123A5-5805-4A65-A135-47271BFF3539"}
 */
function mpPrepareWindow(formName) {
	var form = forms[formName];

	// hide menu and tool bars
	plugins.window.getMenuBar().setVisible(false);
	plugins.window.setToolBarAreaVisible(false);
	plugins.window.setStatusBarVisible(false);

	// set size and center the window
	var window = application.getWindow();
	window.show(form);
	if (application.getOSName() != 'Mac OS X') plugins.window.maximize();
}

/**
 * @properties={typeid:24,uuid:"3BC01B10-2754-4A02-BEA0-907A0CA64D25"}
 * @AllowToRunInFind
 */
function mpLoadRunnableRules() {
	mpBuildRulesByKind(0);
	mpBuildRulesByKind(1);
}
/**
 *  This method loads into an global object all the defined and enabled rules, the object has this form:
 *
 *  container = {
 *		entity1: {
 *			onInsert: [rule1, rule2, ruleN],
 *			onUpdate: [rule1, rule2, ruleN],
 *			onDelete: [rule1, rule2, ruleN],
 *		}
 *		entity2: {
 *			onInsert: [rule1, rule2, ruleN],
 *			onUpdate: [rule1, rule2, ruleN],
 *			onDelete: [rule1, rule2, ruleN],
 *		}
 *  }
 *
 * Once this is done the applyRules method can iterate and apply to the passed record the
 * proper rules based on its entity and the triggered event (onInsert, onUpdate, onDelete)
 *
 * @param {Number} kind The rule kind: 0 = validation, 1 = process
 * @properties={typeid:24,uuid:"6C10DA11-0B98-49DC-894D-218A357C7554"}
 * @AllowToRunInFind
 */
function mpBuildRulesByKind(kind) {

	var container = null;
	if (kind == 0) container = 'mpValidationRules';
	if (kind == 1) container = 'mpProcessRules';

	// reinitialize the container
	globals[container] = { };

	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_rules>} */
	var rules = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_rules');

	if (rules && rules.find()) {
		rules.is_enabled = 1;
		rules.rule_kind = kind;
		rules.search();
		rules.sort('working_entity, execution_order');

		// loop thru the entities
		for (var i = 1; i <= rules.getSize(); i++) {
			var record = rules.getRecord(i);
			var triggers = { };

			if (record.on_insert) triggers.onInsert = true;
			if (record.on_update) triggers.onUpdate = true;
			if (record.on_delete) triggers.onDelete = true;

			/** @type {{onInsert: String[], onUpdate: String[], onDelete: String[]}} */
			var entityRules = globals[container][record.working_entity] || new Object;

			// loop thru the events for this entity
			for (var trigger in triggers) {
				/** @type {String[]} */
				var eventRules = entityRules[trigger] || new Array;
				eventRules.push(record.invoked_method);
				entityRules[trigger] = eventRules;
			}
			// fill the container
			globals[container][record.working_entity] = entityRules;
		}
	}
}

/**
 * This method applies all the rules in sequence for the passed record, it should be called directly
 * from the entity responding to it's events: onRecordInsert, onRecordUpdate and onRecordDelete.
 *
 * @param {JSRecord} record The record to be validated/processed
 * @param {String} eventType The triggered event type (onInsert, onUpdate, onDelete)
 * @return {Boolean}
 * @throws {Error}
 * @properties={typeid:24,uuid:"B0AD452F-2F5C-4F11-9431-DF6AD9BAC8E9"}
 */
function mpApplyAllRules(record, eventType) {
	mpValidationExceptionMessage = '';

	// WEB only
	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		try {
			if (mpApplyRulesByKind(record, eventType, 0)) {
				// only run process rules if validation has passed
				return mpApplyRulesByKind(record, eventType, 1);
			}
		} catch (e) {
			mpValidationExceptionMessage = e.message;
			application.output('1 web ' + e)
			return false;
		}
	}
	if (application.getApplicationType() == APPLICATION_TYPES.HEADLESS_CLIENT) {
		try {
			if (mpApplyRulesByKind(record, eventType, 0)) {
				// only run process rules if validation has passed
				return mpApplyRulesByKind(record, eventType, 1);
			}
		} catch (e) {
			application.output('2 web ' + e);
			mpValidationExceptionMessage = e.message;
			return false;
		}
	}

	return true;
}
/**
 * Applies the validation rules in sequence for the passed record.
 *
 * @param {JSRecord} record The record to be validated/processed
 * @param {String} eventType The triggered event type (onInsert, onUpdate, onDelete)
 * @param {Number} kind The rules kind: 0 = validation, 1 = process
 *
 * @return {Boolean}
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"C4D7E5AD-DC09-40A9-836D-4FF6D0492393"}
 */
function mpApplyRulesByKind(record, eventType, kind) {
	var entity = databaseManager.getDataSourceTableName(record.getDataSource());
	var entityRules = null;
	if (kind == 0) entityRules = mpValidationRules[entity];
	if (kind == 1) entityRules = mpProcessRules[entity];

	if (entityRules) {
		/** @type {String[]} */
		var eventRules = entityRules[eventType];
		if (eventRules && eventRules.length > 0) {
			/** @type {String[]} */
			var errors = [];
			var totaltime = null;

			application.output( ( (kind == 0) ? 'VALIDATING ' : 'PROCESSING ') + entity.toUpperCase())
			application.output(mpMessageLog + "\n----------------------------------------------------------------------------------------");

			for (var rule in eventRules) {
				var start = Date.now();
				try {

					// check rule existence
					if (!scopes['rulesFor_' + entity][eventRules[rule]]) {
						application.output('undefined rule: ' + eventRules[rule]);
					}

					// run the rule
					var stepTime = Date.now() - start;
					application.output(parseInt(rule) + 1 + ") running " + eventRules[rule]);
					scopes['rulesFor_' + entity][eventRules[rule]].call(this, record);

					// print some info
					totaltime += stepTime;
					application.output("\tOK - rule executed in " + stepTime + " ms ");
					application.output("----------------------------------------------------------------------------------------");

				} catch (e) {
					application.output('4 ' + e)
					// fill errors
					errors.push(e.message);

					// print errors
					application.output("\n\t********************************  FAILED  **************************************");
					application.output("\toperation\t: " + eventType);
					application.output("\tentity\t\t: " + entity);
					application.output("\trecord id\t: " + record.getPKs()[0]);
					application.output("\terror\t\t: " + e.message);
					application.output("\t********************************************************************************\n");
					application.output("----------------------------------------------------------------------------------------");
				}
			}

			application.output("\nTOTAL EXECUTION TIME: " + totaltime + " ms\n\n\n");
			if (errors.length > 0) {
				var stringed = errors.join('\n');
				throw new Error(stringed);
			}
		}
	}

	return true;
}

/**
 * Establish if the value is null OR an empty string
 *
 * @param {Object} value
 * @return {Boolean}
 * @properties={typeid:24,uuid:"B2F24C58-C01C-4FC8-9CA3-0F3B632C4E89"}
 */
function mpIsEmpty(value) {
	if (value == null) return true;
	if (value.toString().replace(/^\s+|\s+$/g, '').length == 0) return true;
	return false;
}

/**
 * The SHA1 cipher Method
 * @param str The string to encode
 *
 * @properties={typeid:24,uuid:"1D48A831-1AA1-49B0-A982-9C23709C090E"}
 */
function mpSha1(str) {
	var rotate_left = function(n, s) {
		var t4 = (n << s) | (n >>> (32 - s));
		return t4;
	};

	var cvt_hex = function(val) {
		str = "";
		var k;
		var v;

		for (k = 7; k >= 0; k--) {
			v = (val >>> (k * 4)) & 0x0f;
			str += v.toString(16);
		}
		return str;
	};

	var blockstart;
	var i, j;
	var W = new Array(80);
	var H0 = 0x67452301;
	var H1 = 0xEFCDAB89;
	var H2 = 0x98BADCFE;
	var H3 = 0x10325476;
	var H4 = 0xC3D2E1F0;
	var A, B, C, D, E;
	var temp;

	var str_len = str.length;

	var word_array = [];
	for (i = 0; i < str_len - 3; i += 4) {
		j = str.charCodeAt(i) << 24 | str.charCodeAt(i + 1) << 16 | str.charCodeAt(i + 2) << 8 | str.charCodeAt(i + 3);
		word_array.push(j);
	}

	switch (str_len % 4) {
	case 0:
		i = 0x080000000;
		break;
	case 1:
		i = str.charCodeAt(str_len - 1) << 24 | 0x0800000;
		break;
	case 2:
		i = str.charCodeAt(str_len - 2) << 24 | str.charCodeAt(str_len - 1) << 16 | 0x08000;
		break;
	case 3:
		i = str.charCodeAt(str_len - 3) << 24 | str.charCodeAt(str_len - 2) << 16 | str.charCodeAt(str_len - 1) << 8 | 0x80;
		break;
	}

	word_array.push(i);

	while ( (word_array.length % 16) != 14) {
		word_array.push(0);
	}

	word_array.push(str_len >>> 29);
	word_array.push( (str_len << 3) & 0x0ffffffff);

	for (blockstart = 0; blockstart < word_array.length; blockstart += 16) {
		for (i = 0; i < 16; i++) {
			W[i] = word_array[blockstart + i];
		}
		for (i = 16; i <= 79; i++) {
			W[i] = rotate_left(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);
		}

		A = H0;
		B = H1;
		C = H2;
		D = H3;
		E = H4;

		for (i = 0; i <= 19; i++) {
			temp = (rotate_left(A, 5) + ( (B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
			E = D;
			D = C;
			C = rotate_left(B, 30);
			B = A;
			A = temp;
		}

		for (i = 20; i <= 39; i++) {
			temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
			E = D;
			D = C;
			C = rotate_left(B, 30);
			B = A;
			A = temp;
		}

		for (i = 40; i <= 59; i++) {
			temp = (rotate_left(A, 5) + ( (B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
			E = D;
			D = C;
			C = rotate_left(B, 30);
			B = A;
			A = temp;
		}

		for (i = 60; i <= 79; i++) {
			temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
			E = D;
			D = C;
			C = rotate_left(B, 30);
			B = A;
			A = temp;
		}

		H0 = (H0 + A) & 0x0ffffffff;
		H1 = (H1 + B) & 0x0ffffffff;
		H2 = (H2 + C) & 0x0ffffffff;
		H3 = (H3 + D) & 0x0ffffffff;
		H4 = (H4 + E) & 0x0ffffffff;
	}

	temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
	return temp.toLowerCase();
}

/**
 * Return true if user is in group
 * @param {String} group
 * @return {Boolean}
 * @properties={typeid:24,uuid:"1DC7C2D2-2183-4268-BACF-314F298F9B5E"}
 */
function mpHasRole(group) {
	var groups = security.getUserGroups();

	for (var index = 1; index <= groups.getMaxRowIndex(); index++) {
		if (groups.getValue(index, 2) == group) {
			return true;
		}
	}
	return false;
}

/**
 * Return true if user is in group
 * @param {String} group
 * @return {Boolean}
 * @properties={typeid:24,uuid:"8201441A-0878-45A1-A9B2-9695CD4A3B09"}
 * @AllowToRunInFind
 */
function mpHasRoleFromGeco(group) {
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
	var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users_roles>} */
	var usersRoles = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users_roles');
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_roles>} */
	var roles = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_roles');
	/** @type {Boolean} */
	var returnVal = false;

	if (users.find()) {
		users.geco_user_id = security.getUserUID();
//		users.geco_user_id = mp_currentuserid_to_mp_users.geco_user_id;
		var tot = users.search();
		//trovo utente proveniente da geco, mi devo salvare l'id che avrebbe in pratiche sanitarie
		if (tot > 0) {
			var recUser = users.getRecord(1);

			if (usersRoles.find()) {
				usersRoles.user_id = recUser.user_id;
				var totUserRoles = usersRoles.search();

				if (totUserRoles > 0) {
					for (var index = 1; index <= usersRoles.getSize(); index++) {
						var recUserRole = usersRoles.getRecord(index);

						if (roles.find()) {
							roles.role_id = recUserRole.role_id;
							var totRoles = roles.search();

							if (totRoles > 0) {
								var recRole = roles.getRecord(1);
								if (recRole.description == group) {
									returnVal = true;
									break;
								}
							}
						}
					}
				}
			}
		}
	}
	return returnVal;
}

/**
 * Return true if user has geco_user_id set (i.e. he is on GECO)
 * @param {Number} userId
 * @return {Boolean}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"308A135F-EA59-466D-8CE6-BBA9F0CECA24"}
 */
function mpHasMedicalPractice(userId) {
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
	var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
	if (users.find()) {
		users.user_id = userId;
		var tot = users.search();
	}
	if (tot > 0) {
		var recUser = users.getRecord(1);
		if (recUser.geco_user_id != null) return true;
		else return false;
	} else {
		return false;
	}
}

///**
// * @AllowToRunInFind
// * @param {Number} userId
// * @return {Boolean}
// * @properties={typeid:24,uuid:"6611074F-DA05-4F06-99AA-59F70E4B5D7D"}
// */
//function isProfitCenterManager(userId) {
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/profit_centers>} */
//	var profit_centers = databaseManager.getFoundSet('pratiche_sanitarie', 'profit_centers');
//	if (profit_centers.find()) {
//		profit_centers.user_owner_id = userId;
//		if (profit_centers.search() > 0) return true;
//	}
//	return false;
//}

///**
// * @AllowToRunInFind
// * @param {Number} userId
// * @return {Boolean}
// * @properties={typeid:24,uuid:"B7395FBC-D408-486A-A746-C02F665C52E2"}
// */
//function isJobOrderManager(userId) {
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/job_orders>} */
//	var job_orders = databaseManager.getFoundSet('pratiche_sanitarie', 'job_orders');
//	if (job_orders.find()) {
//		job_orders.user_owner_id = userId;
//		if (job_orders.search() > 0) return true;
//	}
//	return false;
//}

///**
// * SUPPORTO COMMERCIALE: Restituisce true se il cliente scelto è presente nel mercato scelto.
// *
// * @AllowToRunInFind
// * @param {Number} clientId
// * @param {Number} pcId
// * @return {Boolean}
// *
// * @properties={typeid:24,uuid:"8BD03044-4B9C-48CA-BA94-7AD1DD40605C"}
// */
//function isClientPCSuppComm(clientId, pcId) {
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/comm_supp_profit_centers>} */
//	var commSuppPc = databaseManager.getFoundSet('pratiche_sanitarie', 'comm_supp_profit_centers');
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/comm_supp_customers>} */
//	var commSuppCustomers = databaseManager.getFoundSet('pratiche_sanitarie', 'comm_supp_customers');
//
//	var totPC = 0;
//	var totCustomers = 0;
//
//	//andare in find su tab dei pc per utente e mercato;
//	if(commSuppPc.find()){
//		commSuppPc.user_id = scopes.globals.currentUserId;
//		commSuppPc.profit_center_id = pcId;
//		totPC = commSuppPc.search();
//	}
//
//	if (totPC > 0){
//		var recTot = commSuppPc.getRecord(1);
//
//		//andare in find su quella dei clienti dove l'id di quella prima è uguale a quello della seconda tabella
//		//e dove il cliente è quello che mi arriva
//		if (commSuppCustomers.find()){
//			commSuppCustomers.comm_supp_pc_id = recTot.comm_supp_profit_center_id;
//			commSuppCustomers.customer_id = clientId;
//			totCustomers = commSuppCustomers.search();
//		}
//
//		if (totCustomers > 0) return true;
//	}
//
//	return false;
//}

/**
 * Return true if user can access to solution
 * @param {String[]} groupsList groups enabled
 * @return {Boolean}
 * @properties={typeid:24,uuid:"AE295C0C-207E-41B5-B9E6-C386D1345EFF"}
 */
function mpGrantAccess(groupsList) {
	application.output(globals.mpMessageLog + 'START globals.mpGrantAccess() ' + groupsList, LOGGINGLEVEL.INFO);
	groupsList.push("Administrators");
	var groups = security.getUserGroups();

	for (var index = 1; index <= groups.getMaxRowIndex(); index++) {
		for (var i = 0; i < groupsList.length; i++) {
			if (groups.getValue(index, 2) == groupsList[i]) {
				//trovato uno dei gruppi abilitati
				application.output(globals.mpMessageLog + 'STOP mpGrantAccess() abilitato', LOGGINGLEVEL.INFO);
				return true;
			}
		}
	}
	globals.DIALOGS.showErrorDialog("Errore", "Non hai i permessi necessari per accedere a questa applicazione", 'OK');
	application.output(globals.mpMessageLog + 'STOP globals.mpGrantAccess() NON abilitato', LOGGINGLEVEL.INFO);
	return false;
}

/**
 * @param {JSEvent} headlessCallback
 * @properties={typeid:24,uuid:"CAF3A9DC-BD70-40B3-859A-2F384AD53DD3"}
 */
function mpEnqueueMailLog(headlessCallback) {
	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		var messageMail = '-------------SENDING MAIL----------------------' + '\ntimestamp: ' + headlessCallback.getTimestamp() + '\nrecipient: ' + headlessCallback.data.recipient + '\nbcc: ' + headlessCallback.data.bcc + '\nsubject: ' + headlessCallback.data.subject + '\nmessage: ' + headlessCallback.data.message + '\nsuccess: ' + headlessCallback.data.success.toString().toUpperCase() + '\n-----------------------------------------------\n';
		application.output(globals.mpMessageLog + 'globals.enqueueMailLog() ' + messageMail, LOGGINGLEVEL.DEBUG);
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output(globals.mpMessageLog + 'globals.enqueueMailLog() exception callback, name: ' + headlessCallback.data, LOGGINGLEVEL.DEBUG);
	}
	if (mpClient != null && mpClient.isValid()) {
		//application.output('chiudo client ' + client.getClientID());
		//		client.shutdown();
	}
}

/**
 * @param {JSEvent} headlessCallback
 * @properties={typeid:24,uuid:"54AC5C33-657F-41A9-9EAD-A0A3FA3E6C6F"}
 */
function mpEnqueueMailLogForInsuredUsers(headlessCallback) {
	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		var messageMail = '-------------SENDING MAIL----------------------' + '\ntimestamp: ' + headlessCallback.getTimestamp() + '\nrecipient: ' + headlessCallback.data.recipient + '\nbcc: ' + headlessCallback.data.bcc + '\nsubject: ' + headlessCallback.data.subject + '\nmessage: ' + headlessCallback.data.message + '\nsuccess: ' + headlessCallback.data.success.toString().toUpperCase() + '\n-----------------------------------------------\n';
		application.output(globals.mpMessageLog + 'globals.enqueueMailLog() ' + messageMail, LOGGINGLEVEL.DEBUG);
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output(globals.mpMessageLog + 'globals.enqueueMailLog() exception callback, name: ' + headlessCallback.data, LOGGINGLEVEL.DEBUG);
	}
	if (mpClient != null && mpClient.isValid()) {
		application.output('chiudo client ' + mpClient.getClientID());
		mpClient.shutdown();
	}
}

/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"6C6395B7-1BDB-43AF-910C-103756509FD0"}
 */
function mpEnqueueMailReminder(recipient, subject, message) {
	application.output(globals.mpMessageLog + 'START globals.enqueueMailReminder() ', LOGGINGLEVEL.INFO);
	if (mpClient == null || !mpClient.isValid()) {
		application.output(globals.mpMessageLog + 'globals.enqueueMailReminder() client create', LOGGINGLEVEL.INFO);
		mpClient = plugins.headlessclient.createClient("mp_queue", null, null, null);
	}

	if (mpClient != null && mpClient.isValid()) {

		mpClient.queueMethod(null, "sendMail", [recipient, subject, message], mpEnqueueMailLog);
		application.output(globals.mpMessageLog + 'STOP globals.enqueueMailReminder() Client OK ' + mpClient.getClientID(), LOGGINGLEVEL.INFO);
		return true
	} else {
		application.output('Client is not valid');
		application.output(globals.mpMessageLog + 'STOP globals.enqueueMailReminder() Client is not valid ', LOGGINGLEVEL.INFO);
		return false
	}
}

/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
 * @return {Boolean}
 * @properties={typeid:24,uuid:"850B7F42-6174-4C95-B02B-AD6A161C2C16"}
 */
function mpEnqueueMailReminderForInsuredUsers(recipient, subject, message) {
	application.output(globals.mpMessageLog + 'START globals.enqueueMailReminder() ', LOGGINGLEVEL.INFO);
	if (mpClient == null || !mpClient.isValid()) {
		application.output(globals.mpMessageLog + 'globals.enqueueMailReminder() client create', LOGGINGLEVEL.INFO);
		mpClient = plugins.headlessclient.createClient("mp_queue", null, null, null);
	}
	if (mpClient != null && mpClient.isValid()) {

		mpClient.queueMethod(null, "sendMail", [recipient, subject, message], mpEnqueueMailLogForInsuredUsers);
		application.output(globals.mpMessageLog + 'STOP globals.enqueueMailReminder() Client OK ' + mpClient.getClientID(), LOGGINGLEVEL.INFO);
		return true
	} else {
		application.output('Client is not valid');
		application.output(globals.mpMessageLog + 'STOP globals.enqueueMailReminder() Client is not valid ', LOGGINGLEVEL.INFO);
		return false
	}
}
/**
 * @properties={typeid:35,uuid:"525A6337-32BD-4232-BB76-D5A4AB757D33",variableType:-4}
 */
var mpMailSubstitution = {
	nome: '',
	numEventi: 0,
	numSpese: 0,
	oreMancanti: 0,
	periodo: '',
	approvatore: '',
	tipoEvento: '',
	link: '',
	newPwd: '',
	mailText: '',
	note: '',
	oreRespinte: ''
};

/**
 * @param {String} url
 * @return {String}
 * @properties={typeid:24,uuid:"D1513DD3-1FA1-4F14-91A8-9340ECE70ECE"}
 */
function mpGetServerUrlMedicalPractices(url) {
	//http://localhost:8080
	//	application.output('primo ' + url.indexOf(':'));
	//	application.output('secondo ' + url.lastIndexOf(':'));
	//	application.output('ultimo index https: ' + url.indexOf('https'));
	if (url.indexOf('https') == -1 || url.indexOf(':') != url.lastIndexOf(':')) {
		//		application.output('index last : ' + url.lastIndexOf(':'));
		//		application.output('lunghezza ' + url.length);
		if (url != 'https://213.254.15.84:8443') url = url.substr(0, (url.lastIndexOf(':')));
		application.output('url finale ' + url);
	}
	return url;
}

///**
// *  @param {JSEvent} event the event that triggered the action
// *  @AllowToRunInFind
// *  @properties={typeid:24,uuid:"FDDB900A-6A1D-4BAA-858B-F76BD1D54701"}
// */
//function reportUsers(event) {
//	var success = null;
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/users>} */
//	var user = databaseManager.getFoundSet('pratiche_sanitarie', 'users');
//
//	user.loadAllRecords();
//	//application.output(user);
//	/** @type {String} */
//	var totalLines = '';
//	var columnLine = '';
//
//	columnLine = 'Nominativo\t' + 'Codice_Navision\t' + 'Codice_FEP\t' + 'Centro_profitto\t' + 'Data_assunzione\t' + 'Data_dimissione\t' + 'Abilitato\n';
//	/** @type {Number} */
//	var count = databaseManager.getFoundSetCount(user);
//	for (var index = 1; index <= count; index++) {
//		user.setSelectedIndex(index);
//		/** @type {JSRecord<db:/pratiche_sanitarie/users>} */
//		var rec = user.getRecord(index);
//		var nome = rec.users_to_contacts.real_name;
//		var profCent = rec.users_to_profit_centers.profit_center_name;
//		var startDate = '' + rec.enrollment_start_date.getDate() + '/' + (rec.enrollment_start_date.getMonth() + 1) + '/' + rec.enrollment_start_date.getFullYear();
//		if (rec.enrollment_end_date && rec.enrollment_end_date != null) {
//			var endDate = '' + rec.enrollment_end_date.getDate() + '/' + (rec.enrollment_end_date.getMonth() + 1) + '/' + rec.enrollment_end_date.getFullYear();
//		} else endDate = '';
//		var enabled = '';
//		if (rec.is_enabled == 1) {
//			enabled = 'SI';
//		} else enabled = 'NO';
//		var line = nome + '\t' + rec.enrollment_number + '\t' + rec.fep_code + '\t' + profCent + '\t' + startDate + '\t' + endDate + '\t' + enabled;
//		totalLines = line + '\n';
//		columnLine = columnLine + totalLines;
//
//	}
//
//	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
//		var fileName = 'reportUtenti.xls';
//		success = plugins.file.writeTXTFile(fileName, columnLine, 'UTF-8', 'application/vnd.ms-excel');
//	}
//	if (!success) application.output('Scrittura non riuscita');
//}

///**
// *  @param {JSEvent} event the event that triggered the action
// *  @AllowToRunInFind
// *  @properties={typeid:24,uuid:"5A82E33B-B16C-4F2F-95F7-BAB553D3A3EB"}
// */
//function reportPCManagers(event) {
//	var success = null;
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/profit_centers>} */
//	var profitCenters = databaseManager.getFoundSet('pratiche_sanitarie', 'profit_centers');
//	profitCenters.sort('profit_center_name asc')
//	profitCenters.loadAllRecords();
//	//application.output(profitCenters);
//	/** @type {String} */
//	var totalLines = '';
//	var columnLine = '';
//
//	columnLine = 'Centro_profitto\t' + 'Responsabile\n';
//	/** @type {Number} */
//	var count = profitCenters.getSize();
//	for (var index = 1; index <= count; index++) {
//
//		/** @type {JSRecord<db:/pratiche_sanitarie/profit_centers>} */
//		var rec = profitCenters.getRecord(index);
//		var nome = rec.profit_centers_to_owner_users.users_to_contacts.real_name;
//
//		var line = rec.profit_center_name + '\t' + nome;
//		totalLines = line + '\n';
//		columnLine = columnLine + totalLines;
//	}
//
//	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
//		var fileName = 'reportCentriProfitto.xls';
//		success = plugins.file.writeTXTFile(fileName, columnLine, 'UTF-8', 'application/vnd.ms-excel');
//	}
//	if (!success) application.output('Scrittura non riuscita');
//}

///**
// *  @param {JSEvent} event the event that triggered the action
// *  @AllowToRunInFind
// *  @properties={typeid:24,uuid:"6C0195AC-FADB-4A9F-A290-14A4964639BB"}
// */
//function reportEvents(event) {
//	var success = null;
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/events>} */
//	var events = databaseManager.getFoundSet('pratiche_sanitarie', 'events');
//	events.sort('event_name asc')
//	events.loadAllRecords();
//	//application.output(events);
//	/** @type {String} */
//	var totalLines = '';
//	var columnLine = '';
//
//	columnLine = 'Nome_evento\t' + 'Conteggio\t' + 'Su_commessa\t' + 'Figura_approvatore\t' + 'Su_richiesta\t' + 'Abilitato\t' + 'Centro_profitto_approvatore\t' + 'Codice_Navision\t' + 'Codice_Fep\n';
//	/** @type {Number} */
//	var count = events.getSize();
//	for (var index = 1; index <= count; index++) {
//		events.setSelectedIndex(index);
//		/** @type {JSRecord<db:/pratiche_sanitarie/events>} */
//		var rec = events.getRecord(index);
//		var nome = rec.event_name;
//		var conteggio = '';
//		if (rec.unit_measure == 1) {
//			conteggio = 'Ad unita\'';
//		} else if (rec.unit_measure == 2) conteggio = 'Ad ore';
//		var hasJob = '';
//		if (rec.has_job_order == 1) {
//			hasJob = 'SI';
//		} else hasJob = 'NO';
//		var approvFig = '';
//		switch (rec.approver_figure) {
//		case 1:
//			approvFig = 'Centro profitto'
//			break
//		case 2:
//			approvFig = 'Responsabile dipendente'
//			break
//		case 3:
//			approvFig = 'Responsabile commessa'
//			break
//		}
//		var hasRequest = '';
//		if (rec.is_requestable == 1) {
//			hasRequest = 'SI';
//		} else hasRequest = 'NO';
//
//		var enabled = '';
//		if (rec.is_enabled == 1) {
//			enabled = 'SI';
//		} else enabled = 'NO';
//		if (rec.approver_profit_center_id && rec.approver_profit_center_id != null) {
//			var profCentName = rec.events_to_profit_centers_approver.profit_center_name;
//		} else profCentName = '';
//		if (rec.external_code_navision && rec.external_code_navision != null) {
//			var extCode = rec.external_code_navision;
//		} else extCode = '';
//		var codeFep = rec.external_code_fep;
//		var line = nome + '\t' + conteggio + '\t' + hasJob + '\t' + approvFig + '\t' + hasRequest + '\t' + enabled + '\t' + profCentName + '\t' + extCode + '\t' + codeFep;
//		totalLines = line + '\n';
//		columnLine = columnLine + totalLines;
//
//	}
//
//	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
//		var fileName = 'reportEventi.xls';
//		success = plugins.file.writeTXTFile(fileName, columnLine, 'UTF-8', 'application/vnd.ms-excel');
//	}
//	if (!success) application.output('Scrittura non riuscita');
//}

///**
// *  @param {JSEvent} event the event that triggered the action
// *  @AllowToRunInFind
// *  @properties={typeid:24,uuid:"79435F9F-40E2-4B6C-824A-A1DD7179BA40"}
// */
//function reportJobOrders(event) {
//	var success = null;
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/job_orders>} */
//	var job_orders = databaseManager.getFoundSet('pratiche_sanitarie', 'job_orders');
//	job_orders.loadAllRecords();
//	//application.output(job_orders);
//	/** @type {String} */
//	var totalLines = '';
//	var columnLine = '';
//
//	columnLine = 'Codice\t' + 'Titolo\t' + 'Responsabile\t' + 'Centro_profitto\t' + 'Abilitata\t' + 'Da_chiudere\t' + 'Utilizzabile\n';
//	/** @type {Number} */
//	var count = databaseManager.getFoundSetCount(job_orders);
//	for (var index = 1; index <= count; index++) {
//		job_orders.setSelectedIndex(index);
//		/** @type {JSRecord<db:/pratiche_sanitarie/job_orders>} */
//		var rec = job_orders.getRecord(index);
//		var code = '"' + rec.external_code_navision + '"';
//		var extCode = code.replace(/\n/g, '');
//		var title = '"' + rec.job_order_title + '"';
//		var titolo = title.replace(/\n/g, '');
//		var owner = rec.job_orders_owner_to_users.users_to_contacts.real_name;
//		var profCent = rec.job_orders_to_profit_centers.profit_center_name;
//
//		var enabled = '';
//		if (rec.is_enabled == 1) {
//			enabled = 'SI';
//		} else enabled = 'NO';
//		var toClose = '';
//		if (rec.is_to_close == 1) {
//			toClose = 'SI';
//		} else toClose = 'NO';
//		var loggable = '';
//		if (rec.is_loggable == 1) {
//			loggable = 'SI';
//		} else loggable = 'NO';
//		var line = extCode + '\t' + titolo + '\t' + owner + '\t' + profCent + '\t' + enabled + '\t' + toClose + '\t' + loggable;
//		totalLines = line + '\r\n';
//		columnLine = columnLine + totalLines;
//
//	}
//
//	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
//		var fileName = 'reportCommesse.xls';
//		success = plugins.file.writeTXTFile(fileName, columnLine, 'UTF-8', 'application/vnd.ms-excel');
//	}
//	if (!success) application.output('Scrittura non riuscita');
//}

///**
// *  @param {JSEvent} event the event that triggered the action
// *  @AllowToRunInFind
// *  @properties={typeid:24,uuid:"5F5A02DA-95B3-4A7D-8AF2-C2A25DBEFD3D"}
// */
//function reportCompanies(event) {
//	var success = null;
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/job_orders>} */
//	var companies = databaseManager.getFoundSet('pratiche_sanitarie', 'companies');
//	companies.loadAllRecords();
//	//application.output(job_orders);
//	/** @type {String} */
//	var totalLines = '';
//	var columnLine = '';
//
//	columnLine = 'Nome\t' + 'Codice\t' + 'Città\t' + 'Abilitato\t' + 'Tipo\n';
//	/** @type {Number} */
//	var count = databaseManager.getFoundSetCount(companies);
//	for (var index = 1; index <= count; index++) {
//		companies.setSelectedIndex(index);
//		/** @type {JSRecord<db:/pratiche_sanitarie/companies>} */
//		var rec = companies.getRecord(index);
//		var name = rec.company_name;
//		var code = '';
//		if (rec.external_code == null) {
//			code = '';
//		} else
//			code = rec.external_code;
//		var city = '';
//		if (rec.city == null) {
//			city = '';
//		} else
//			city = rec.city;
//
//		var enabled = '';
//		if (rec.is_enabled == 1) {
//			enabled = 'SI';
//		} else enabled = 'NO';
//		var type = '';
//		switch (rec.company_type_id) {
//		case 1:
//			type = 'Fornitore'
//			break
//		case 2:
//			type = 'Cliente'
//			break
//		case 3:
//			type = 'Cliente - Fornitore'
//			break
//		}
//
//		var line = name + '\t' + code + '\t' + city + '\t' + enabled + '\t' + type;
//		totalLines = line + '\r\n';
//		columnLine = columnLine + totalLines;
//
//	}
//
//	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
//		var fileName = 'reportClienti.xls';
//		success = plugins.file.writeTXTFile(fileName, columnLine, 'UTF-8', 'application/vnd.ms-excel');
//	}
//	if (!success) application.output('Scrittura non riuscita');
//}

///**
// * @param {Number} user_id
// * @param {Date} day
// *
// * @properties={typeid:24,uuid:"11ED2E41-9C37-4D4E-B941-B6C05695A5A3"}
// * @AllowToRunInFind
// */
//function getUserTimeWorkByDay(user_id, day) {
//	//application.output('globals.getUserTimeWorkByDay() FINE');
//	//application.output(day  +' '+day.getDay());
//	var totDays = 0;
//	var totHistory = 0;
//	var totalUser = 0;
//	var giorno = day.getDay();
//	var objTimeUser = {
//		startTime: 0,
//		stopTime: 0,
//		breakStart: 0,
//		breakStop: 0,
//		workingHours: 0,
//		contractId: 0,
//		isShiftWorker: 0,
//		shiftStartTime: 0,
//		shiftStopTime: 0
//	}
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/working_hours_week>} */
//	var workHoursWeek = null;
//
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/users>} */
//	var users = databaseManager.getFoundSet('pratiche_sanitarie', 'users');
//	if (users.find()) {
//		users.user_id = user_id;
//		totalUser = users.search();
//	}
//
//	if (totalUser == 1) {
//		//application.output('globals.getUserTimeWorkByDay() trovato user');
//		objTimeUser.breakStop = users.break_stop;
//		objTimeUser.breakStart = users.break_start;
//		objTimeUser.startTime = users.shift_start;
//		objTimeUser.stopTime = users.shift_stop;
//		objTimeUser.workingHours = users.working_hours;
//		objTimeUser.contractId = users.contract_type_id;
//		//application.output('globals.getUserTimeWorkByDay() attuale contratto ' + objTimeUser.contractId);
//		if (users.users_to_contract_types && users.users_to_contract_types.is_weekly == 1) {
//			//application.output('globals.getUserTimeWorkByDay() attuale contratto verticale cerco giorno ' + giorno);
//			//cerco il giorno della settimana e i corrispettivi orari
//			workHoursWeek = users.users_to_contract_types.contract_types_to_working_hours_week;
//			if (workHoursWeek.find()) {
//				workHoursWeek.calendar_weekday = giorno;
//				totDays = workHoursWeek.search();
//			}
//			if (totDays > 0) {
//				//application.output('globals.getUserTimeWorkByDay() trovato giorno ' +giorno);
//				objTimeUser.breakStop = workHoursWeek.break_stop;
//				objTimeUser.breakStart = workHoursWeek.break_start;
//				objTimeUser.startTime = workHoursWeek.time_start;
//				objTimeUser.stopTime = workHoursWeek.time_stop;
//				objTimeUser.workingHours = workHoursWeek.duration_work;
//			} else {
//				//se per quel gg non è previsto lavoro da contratto
//				//application.output('globals.getUserTimeWorkByDay() NON trovato giorno ' +giorno);
//				objTimeUser.breakStop = 0;
//				objTimeUser.breakStart = 0;
//				objTimeUser.startTime = 0;
//				objTimeUser.stopTime = 0;
//				objTimeUser.workingHours = 0;
//			}
//		}
//		//cerco se c'è un history di cambio orario lavorativo
//		/** @type {JSFoundSet<db:/pratiche_sanitarie/users_working_hours_history>} */
//		var users_working_hours_history = databaseManager.getFoundSet('pratiche_sanitarie', 'users_working_hours_history');
//		//application.output('globals.getUserTimeWorkByDay() cerco history ');
//		if (users_working_hours_history.find()) {
//			users_working_hours_history.user_id = user_id;
//			totHistory = users_working_hours_history.search();
//		}
//		//se ha trovato history cerco se il girono è prima del cambio
//		for (var i = 1; i <= totHistory; i++) {
//			users_working_hours_history.sort('start_change_date desc');
//			//application.output('globals.getUserTimeWorkByDay() trovata history ');
//			var rec_whh = users_working_hours_history.getRecord(i);
//			if (day < rec_whh.start_change_date) {
//				//application.output('globals.getUserTimeWorkByDay() giorno minore del cambio data ' + rec_whh.start_change_date);
//				//se il giorno è prima del cambio orario lavorativo
//				//controllo il tipo di contratto se part time erticale o no
//				if (rec_whh.users_working_hours_history_to_previous_contract_types.is_weekly) {
//					//application.output('globals.getUserTimeWorkByDay() contratto precedente parti time vert');
//					//se part time verticale
//					//cerco l'orario giornaliero del part time verticale
//					workHoursWeek = rec_whh.users_working_hours_history_to_previous_contract_types.contract_types_to_working_hours_week;
//					totDays = 0;
//					if (workHoursWeek.find()) {
//						workHoursWeek.calendar_weekday = giorno;
//						totDays = workHoursWeek.search();
//					}
//					if (totDays > 0) {
//						objTimeUser.breakStop = workHoursWeek.break_stop;
//						objTimeUser.breakStart = workHoursWeek.break_start;
//						objTimeUser.startTime = workHoursWeek.time_start;
//						objTimeUser.stopTime = workHoursWeek.time_stop;
//						objTimeUser.workingHours = workHoursWeek.duration_work;
//
//					} else {
//						//se per quel gg non è previsto lavoro da contratto
//						objTimeUser.breakStop = 0;
//						objTimeUser.breakStart = 0;
//						objTimeUser.startTime = 0;
//						objTimeUser.stopTime = 0;
//						objTimeUser.workingHours = 0;
//					}
//					objTimeUser.contractId = rec_whh.previous_contract_type;
//				} else {
//					//application.output('globals.getUserTimeWorkByDay() contratto precedente NO parti time vert');
//					// il contratto precedente non è part time verticale
//					objTimeUser.breakStop = rec_whh.previous_stop_break;
//					objTimeUser.breakStart = rec_whh.previous_start_break;
//					objTimeUser.startTime = rec_whh.previous_start_time;
//					objTimeUser.stopTime = rec_whh.previous_stop_time;
//					objTimeUser.workingHours = rec_whh.previous_working_hours;
//					objTimeUser.contractId = rec_whh.previous_contract_type;
//				}
//			} else {
//				//application.output('globals.getUserTimeWorkByDay() giorno maggiore del cambio contratto');
//			}
//		}
//		if (users.is_shift_worker == 1) {
//			objTimeUser.isShiftWorker = users.is_shift_worker;
//			objTimeUser.shiftStartTime = users.users_to_shift_work_type.shift_start;
//			objTimeUser.shiftStopTime = users.users_to_shift_work_type.shift_stop;
//		}
//
//	} else {
//		application.output('globals.getUserTimeWorkByDay() utente non trovato o più utenti con lo stesso ID');
//	}
//	//application.output('globals.getUserTimeWorkByDay() trovato contratto ' + objTimeUser.contractId + ' ' + objTimeUser.startTime + ' ' + objTimeUser.stopTime + ' ' + objTimeUser.breakStart + ' ' + objTimeUser.breakStop + ' ' + objTimeUser.workingHours);
//	//application.output('globals.getUserTimeWorkByDay() FINE');
//	return objTimeUser;
//}

///**
// * @AllowToRunInFind
// * @param {Number} userId
// * @return {Number[]}
// * @properties={typeid:24,uuid:"760DD92C-8AD0-43F9-9BC4-8EB371DC4DF8"}
// */
//function getProfitCenterByUserManager(userId) {
//	var listPC = [];
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/profit_centers>} */
//	var profit_centers = databaseManager.getFoundSet('pratiche_sanitarie', 'profit_centers');
//	//	if (profit_centers.find()) {
//	//		profit_centers.user_owner_id = userId;
//	//		if (profit_centers.search() > 0) {
//	//			for (var index = 1; index <= profit_centers.getSize(); index++) {
//	//				var rec = profit_centers.getRecord(index);
//	//				listPC.push(rec.profit_center_id);
//	//			}
//	//		}
//	//	}
//	if (profit_centers.find()) {
//		if (scopes.globals.hasRole('Supp. Commerciale')) {
//			profit_centers.profit_centers_to_comm_support.user_id = userId;
//		} else {
//			profit_centers.profit_centers_to_owners_markets.user_id = userId;
//		}
//		if (profit_centers.search() > 0) {
//			profit_centers.sort('profit_center_type_id desc, profit_center_name asc')
//			for (var index = 1; index <= profit_centers.getSize(); index++) {
//				var rec = profit_centers.getRecord(index);
//				listPC.push(rec.profit_center_id);
//			}
//		}
//	}
//	return listPC;
//}
///**
// * @AllowToRunInFind
// * @param {Number} userId
// * @return {Number[]}
// * @properties={typeid:24,uuid:"3C362152-0391-4723-89EE-1D539ECE4E8B"}
// */
//function getProfitCenterByUserManagerForMarket(userId) {
//	var listPC = [];
//	/** @type {JSFoundSet<db:/pratiche_sanitarie/profit_centers>} */
//	var profit_centers = databaseManager.getFoundSet('pratiche_sanitarie', 'profit_centers');
//	//	if (profit_centers.find()) {
//	//		profit_centers.user_owner_id = userId;
//	//		if (profit_centers.search() > 0) {
//	//			for (var index = 1; index <= profit_centers.getSize(); index++) {
//	//				var rec = profit_centers.getRecord(index);
//	//				listPC.push(rec.profit_center_id);
//	//			}
//	//		}
//	//	}
//	if (profit_centers.find()) {
//		profit_centers.profit_centers_to_owners_markets.user_id = userId;
//		profit_centers.profit_centers_to_profit_center_types.profit_center_types_id = 5;
//		if (profit_centers.search() > 0) {
//			profit_centers.sort('profit_center_type_id desc, profit_center_name asc')
//			for (var index = 1; index <= profit_centers.getSize(); index++) {
//				var rec = profit_centers.getRecord(index);
//				listPC.push(rec.profit_center_id);
//			}
//		}
//	}
//	return listPC;
//}
/**
 * @param{String} str
 * @return {String}
 *
 * @properties={typeid:24,uuid:"0A334361-5EF0-49C0-AB70-B60F43CD46AE"}
 * @AllowToRunInFind
 */
function mpTrimString(str) {
	str = str.replace(/^\s+|\s+$/g, '');
	return str;
}

/**
 * Trimma anche: gli spazi in mezzo a nome e cognome e gli apici
 * @param{String} str
 * @return {String}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"D177B77D-AB3E-4D7D-85CB-8762D08AFEFA"}
 */
function mpTrimStringNew(str) {
	str = str.replace(/^\s+|\s|\W|\s+$/g, '');
	return str;
}

/**
 * Recupera mail subject e mail text dato il tipo di mail cercata
 * @param {Number}  mailTypeId
 * @return {Object}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"BF9A107E-C4E4-4A2A-929D-81AE394B53ED"}
 */
function mpGetMailType(mailTypeId) {
	var obj = {
		subject: null,
		text: null,
		distrList: null
	}
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_mail_types>} */
	var mail_types = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_mail_types');
	if (mail_types.find()) {
		mail_types.mail_type_id = mailTypeId;
		mail_types.search();
		if (mail_types.getSize() == 1) {
			obj.subject = mail_types.mail_type_subject;
			obj.text = mail_types.mail_type_text;
			obj.distrList = mail_types.mail_distribution_list;
		}
	}
	return obj;
}
/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"870D66E7-E1ED-4262-BD9B-A73948333A0E"}
 */
function mpEnqueueMailReminderForDossierStatus(recipient, subject, message) {
	application.output(globals.mpMessageLog + 'START globals.mpEnqueueMailReminderForDossierStatus() ', LOGGINGLEVEL.INFO);
	if (mpClient == null || !mpClient.isValid()) {
		application.output(globals.mpMessageLog + 'globals.mpEnqueueMailReminderForDossierStatus() client create', LOGGINGLEVEL.INFO);
		mpClient = plugins.headlessclient.createClient("mp_queue", null, null, null);
	}
	if (mpClient != null && mpClient.isValid()) {
		mpClient.queueMethod(null, "mpSendMail", [recipient, subject, message], mpEnqueueMailLogForDossierStatus);
		application.output(globals.mpMessageLog + 'STOP globals.mpEnqueueMailReminderForDossierStatus() Client OK ' + mpClient.getClientID(), LOGGINGLEVEL.INFO);
		//client.shutdown();
		return true
	} else {
		application.output('Client is not valid');
		application.output(globals.mpMessageLog + 'STOP globals.mpEnqueueMailReminderForDossierStatus() Client is not valid ', LOGGINGLEVEL.INFO);
		//client.shutdown();
		return false
	}
}
/**
 * @param {JSEvent} headlessCallback
 * @properties={typeid:24,uuid:"613BEF64-DC8B-44D7-8C51-544DEA4781EF"}
 */
function mpEnqueueMailLogForDossierStatus(headlessCallback) {
	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		var messageMail = '-------------SENDING MAIL----------------------' + '\ntimestamp: ' + headlessCallback.getTimestamp() + '\nrecipient: ' + headlessCallback.data.recipient + '\nbcc: ' + headlessCallback.data.bcc + '\nsubject: ' + headlessCallback.data.subject + '\nmessage: ' + headlessCallback.data.message + '\nsuccess: ' + headlessCallback.data.success.toString().toUpperCase() + '\n-----------------------------------------------\n';
		application.output(globals.mpMessageLog + 'globals.mpEnqueueMailLogForDossierStatus() ' + messageMail, LOGGINGLEVEL.DEBUG);
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output(globals.mpMessageLog + 'globals.mpEnqueueMailLogForDossierStatus() exception callback, name: ' + headlessCallback.data, LOGGINGLEVEL.DEBUG);
	}
	if (mpClient != null && mpClient.isValid()) {
		application.output('chiudo client ' + mpClient.getClientID());
		mpClient.shutdown();
	}
}
/**
 * @param {Number} mailTypeId
 * @param {String} name
 * @param {String} email
 * @param {String} hrName
 *
 * @properties={typeid:24,uuid:"3B95FB9D-854A-4CA7-AAC4-CF6CD2ABBD8B"}
 */
function mpSendMailForNewInsuredUser(mailTypeId, name,email,hrName) {
	application.output(globals.mpMessageLog + 'START globals.sendMailForNewInsuredUser() ', LOGGINGLEVEL.INFO);
	var objMail = globals.mpGetMailType(mailTypeId);

	var mailSubs = {
		nome: '',
		hrName: '',
		link: ''
	};
	var recipient = email;
	//var recipient = objMail.distrList;
	var subject = objMail.subject;
	var hostname = application.getServerURL();
	var link = hostname + '/servoy-webclient/application/solution/mp_app';
	application.output(link);
	mailSubs.nome = (name != null) ? name : '';
	mailSubs.hrName = (hrName != null) ? hrName : '';
	mailSubs.link = (link != null) ? link : '';
	var message = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubs);
	application.output(globals.mpMessageLog + 'globals.sendMailForNewInsuredUser() SENDING MAIL to distribution list  subject: ' + subject + ' message: ' + message, LOGGINGLEVEL.INFO);

	mpEnqueueMailReminderForInsuredUsers(recipient, subject, message);
	application.output(globals.mpMessageLog + 'STOP globals.sendMailForNewInsuredUser() ', LOGGINGLEVEL.INFO);
}

/**
 * @param {Number} mailTypeId
 * @param {String} name
 * @param {String} hrName
 * @properties={typeid:24,uuid:"D0E35966-FC80-4EE0-9A6C-AF942C9B4139"}
 */
function mpSendMailForInsuredUserWhenEndDateChanged(mailTypeId, name, hrName) {
	application.output(globals.mpMessageLog + 'START globals.sendMailForNewInsuredUser() ', LOGGINGLEVEL.INFO);
	var objMail = globals.mpGetMailType(mailTypeId);

	var mailSubs = {
		nome: '',
		hrName: ''
	};
	//	TODO SCOMMENTARE QUANDO SI ANDRA' IN PRO
	//	var recipient = email;
	var recipient = objMail.distrList;
	var subject = objMail.subject;
	mailSubs.nome = (name != null) ? name : '';
	mailSubs.hrName = (hrName != null) ? hrName : '';

	var message = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubs);
	application.output(globals.mpMessageLog + 'globals.sendMailForNewInsuredUser() SENDING MAIL to distribution list  subject: ' + subject + ' message: ' + message, LOGGINGLEVEL.INFO);

	mpEnqueueMailReminderForInsuredUsers(recipient, subject, message);
	application.output(globals.mpMessageLog + 'STOP globals.sendMailForNewInsuredUser() ', LOGGINGLEVEL.INFO);
}
/**
 * // change user's password
 * @param {String} username
 * @param {String} oldPassword
 * @param {String} newPassword
 *
 *
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"012DBEB2-A72A-4EBC-992A-D5CE97ADCDB5"}
 */
function mpChangePassword(username, oldPassword, newPassword) {
	application.output(globals.messageLog + 'START globals.changePassword() ' + username, LOGGINGLEVEL.INFO);
	//verifico nuova password
	if (newPassword != null && mpCheckPassword(newPassword)) {
		//controlla esistenza utente
		/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
		var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');
		// look up user
		if (users.find()) {
			users.user_name = username;
			if (oldPassword != null) users.user_password = globals.sha1(oldPassword);

			// search
			if (users.search() != 0) {

				// kick out not enabled users
				if (users.is_enabled != 1) {
					application.output(globals.messageLog + 'globals.mpChangePassword() Utente non abilitato ', LOGGINGLEVEL.INFO);
					throw new Error("Utente non abilitato");
				}
				databaseManager.setAutoSave(false);
				var newPwd = globals.mpSha1(newPassword);
				//				/** @type {JSFoundSet<db:/geco/users_password_expiration>} */
				//				var userspwdex = databaseManager.getFoundSet('pra', 'users_password_expiration');
				//				var insertResutl = userspwdex.insertNewPassword(users.user_id, newPwd, 90, false);
				//				if (insertResutl == false) {
				//					application.output(globals.messageLog + 'globals.changePassword() Password non valida. Non puoi utilizzare le ultime 3 password ', LOGGINGLEVEL.INFO);
				//					throw new Error("Password non valida. Non puoi utilizzare le ultime 3 password");
				//				}
				users.user_password = newPwd;
				application.output(globals.messageLog + 'globals.mpChangePassword() Password cambiata', LOGGINGLEVEL.INFO);
				databaseManager.setAutoSave(true);
			} else {
				//not found
				application.output(globals.messageLog + 'globals.mpChangePassword() ERROR: Utente o password sbagliata', LOGGINGLEVEL.INFO);
				throw new Error("Utente o password sbagliata");
			}
		}
	} else {
		application.output(globals.messageLog + 'globals.changePassword() ERROR: Nuova password non valida', LOGGINGLEVEL.INFO);
		throw new Error("Nuova password non valida");
	}
	application.output(globals.messageLog + 'STOP globals.changePassword() ' + username, LOGGINGLEVEL.INFO);
}
/**
 * check if the password contains at least one number and at least one uppercase letter
 * @param {String} password
 *
 *
 * @properties={typeid:24,uuid:"F7176295-DB1A-4381-8A11-B269969E0AF5"}
 */
function mpCheckPassword(password) {

	var newPassArray = password.split("");
	var hasNumber = false;
	var hasUppercase = false;

	if (password.length < 8) return false;

	newPassArray.forEach(function(character) {

		if (!isNaN(character)) {
			hasNumber = true;
		}

		if (isNaN(character) && character === character.toUpperCase()) {
			hasUppercase = true;
		}
		//application.output(character + " " + character.toUpperCase());

	});
	return (hasNumber && hasUppercase);
}
