/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_bills>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"6144779C-D90D-4EDC-AA65-96FB193DDE1B"}
 */
function validateUploadedBill(record){
	//verifico che al salvataggio del record sia stato caricato il documento
	if(scopes.globals.mpIsEmpty(record.file_url)){
		throw new Error('- Non hai caricato la fattura');		
	}
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_bills>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"7EF7A0EB-62EF-4FBA-BA45-B05917A5466C"}
 */
function validateBillEarner(record) {
	if(scopes.globals.mpIsEmpty(record.earner)){
		throw new Error('- Il percettore è obbligatorio');		
	}
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_bills>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"CDAB54DC-C8A2-49BC-A225-A4CD9A42BC01"}
 */
function validateAmountBill(record) {
	if(scopes.globals.mpIsEmpty(record.amount)){
		throw new Error('- Inserire l\'importo della fattura');		
	}
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_bills>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"13295139-6C32-4A50-B40B-22D703DBE460"}
 */
function validateBillDate(record) {
	if(scopes.globals.mpIsEmpty(record.bill_date)){
		throw new Error('- La data del documento è obbligatoria');		
	}
}
/**
 * Validazione Data ricovero 
 *  @param {JSRecord<db:/pratiche_sanitarie/mp_bills>} record
 * @throws {Error}
 *
 *
 * @properties={typeid:24,uuid:"8DCCDB33-F0DE-4354-B46A-21D06A8608C4"}
 */
function validateHospitalizationDate(record) {
	var firstDate=record.bill_date;
	var lastDate=record.hospitalization_date;
	if(lastDate!=null){
		var yearDifference=lastDate.getFullYear()-firstDate.getFullYear();
		var monthDifference=(lastDate.getMonth()+1)-(firstDate.getMonth()+1);
		var dayDifference=(lastDate.getDate())-(firstDate.getDate());
		if(yearDifference>0){			
			throw new Error('- La data del ricovero deve essere precedente della data attuale');	
		}
		else{
			if(yearDifference==0){
				if(monthDifference>0){
					throw new Error('- La data del ricovero deve essere precedente della data attuale');	
				}
				else{
					if(monthDifference==0){
						if(dayDifference>0){
							throw new Error('- La data del ricovero deve essere precedente della data attuale');	
						}							
					}
				}
			}
		}	
	}
}