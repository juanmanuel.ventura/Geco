/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"CF25D5DB-C08A-4819-A5D1-3266A6ABFCC2"}
 */
function validateFirstName(record) {
	var regex = new RegExp('^[\\\'a-zA-Z][a-zA-Z\\s\\\']+$');
	if (globals.mpIsEmpty(record.first_name)) throw new Error('- Il campo Nome del familiare è obbligatorio');
	if(!globals.mpIsEmpty(record.first_name) && !regex.test(record.first_name)) throw new Error('- Inserire solo lettere nel campo Nome del familiare');
}

/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"1B4E46D1-A680-4E8D-9477-77EF7B0DECE7"}
 */
function validateLastName(record) {
	var regex = new RegExp('^[\\\'a-zA-Z][a-zA-Z\\s\\\']+$');
	if (globals.mpIsEmpty(record.last_name)) throw new Error('- Il campo Cognome del familiare è obbligatorio');
	if(!globals.mpIsEmpty(record.last_name) && !regex.test(record.last_name)) throw new Error('- Inserire solo lettere nel campo Cognome del familiare');
}

/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"267D2B5A-9BD4-4759-B986-2AE3A88A90E5"}
 */
function validateGender(record) {
	if (globals.mpIsEmpty(record.gender)) {
		throw new Error('- Il campo Sesso del familiare è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"858A0A59-46EF-4FA0-BF4B-B5B090890D25"}
 */
function validateFiscalCode(record) {
	var regex = new RegExp('^[A-Z]{6}[A-Z0-9]{2}[A-Z][A-Z0-9]{2}[A-Z][A-Z0-9]{3}[A-Z]$');
	if (globals.mpIsEmpty(record.fiscal_code)) throw new Error('- Il campo Codice Fiscale del familiare è obbligatorio');
	if (!globals.mpIsEmpty(record.fiscal_code)) {
		record.fiscal_code = globals.mpTrimString(record.fiscal_code);
		if(!regex.test(record.fiscal_code)) throw new Error(' - Il campo Codice Fiscale del familiare non è nel formato corretto');
	}
	record.fiscal_code = globals.mpTrimString(record.fiscal_code);
}

/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"4820C794-3C5F-4ADA-B919-2EC3A5043B9A"}
 */
function validateBirthPlace(record) {
	if (globals.mpIsEmpty(record.birth_place)) {
		throw new Error('- Il campo Luogo di Nascita del familiare è obbligatorio');
	} else if (!globals.mpIsEmpty(record.birth_place)){
		var regex = new RegExp('^[a-zA-Z][a-zA-Z\\s\\\']+$');
		if (!regex.test(record.birth_place)) throw new Error('- Inserire solo lettere nel campo Luogo di Nascita del familiare');
	}
}

/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"AD570CAB-BEB4-4416-B6D4-536B1BBC4179"}
 */
function validateBirthDate(record) {
	if (globals.mpIsEmpty(record.birth_date)) throw new Error('- Il campo Data di Nascita del familiare è obbligatorio');
}

/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"2C16AFE7-2D47-4B74-BBCD-31C108524075"}
 */
function validateRelativeTypeId(record){
	if (globals.mpIsEmpty(record.relative_type_id)) throw new Error('- Il campo Tipo Famigliare è obbligatorio');
}

/**
 * Genero il real_name del parente
 * @param {JSRecord<db:/pratiche_sanitarie/mp_insured_relatives>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"8FA833C5-374F-45F0-8EFC-82D4B5707452"}
 */
function processRelativeRealName(record) {
	record.real_name = record.last_name + ' ' + record.first_name;
}