/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_informative_documents>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"2E3A0FAD-D202-471F-A351-989495690713"}
 */
function validateDocumentLabel(record){
	//Verifico che il campo document_label non sia vuoto
	if(scopes.globals.mpIsEmpty(record.document_label)){
		throw new Error('- Inserire la descrizione del documento caricato');		
	}
	
}
/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_informative_documents>} record
 * @throws {Error}

 *
 * @properties={typeid:24,uuid:"3142CF1E-B752-4EDF-BA75-71C722BF26A0"}
 */
function validateDocumentName(record){
	//Verifico che il campo document_name non sia vuoto
	if(scopes.globals.mpIsEmpty(record.document_name)){
		throw new Error('- Nessun documento caricato');		
	}
	
}
