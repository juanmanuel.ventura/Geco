/**
 * @param {JSRecord<db:/pratiche_sanitarie/mp_documents>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"70EE2F44-33EA-48E3-A3DF-3B8ADA2A65FE"}
 */
function validateUploadedDocument(record) {
	//verifico che al salvataggio del record sia stato caricato il documento
	if(scopes.globals.mpIsEmpty(record.doc_title)){
		throw new Error('- Nessun documento caricato');		
	}
}

/**
 * Validazione Data ricovero 
 *  @param {JSRecord<db:/pratiche_sanitarie/mp_documents>} record
 * @throws {Error}
 *
 *
 * @properties={typeid:24,uuid:"28BD66EF-6068-4D0C-808B-3829EC037BD8"}
 */
function validateHospitalizationDate(record) {
	var firstDate=record.created_at;
	var lastDate=record.hospitalization_date;
	if(lastDate!=null){
		var yearDifference=lastDate.getFullYear()-firstDate.getFullYear();
		var monthDifference=(lastDate.getMonth()+1)-(firstDate.getMonth()+1);
		var dayDifference=(lastDate.getDate())-(firstDate.getDate());
		if(yearDifference>0){			
			throw new Error('- La data del ricovero deve essere precedente della data attuale');	
		}
		else{
			if(yearDifference==0){
				if(monthDifference>0){
					throw new Error('- La data del ricovero deve essere precedente della data attuale');	
				}
				else{
					if(monthDifference==0){
						if(dayDifference>0){
							throw new Error('- La data del ricovero deve essere precedente della data attuale');	
						}							
					}
				}
			}
		}
//		if(yearDifference>0 || monthDifference>0 || dayDifference>0){
//			throw new Error('- La data del ricovero deve essere precedente della data attuale');	
//		}	
	}
}
	