/**
 * @param {Object} user
 * @param {Object} password
 * @SuppressWarnings(wrongparameters)
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"01D3E8CB-491C-46C3-970A-56CDC5345257"}
 */
function mpLoginUser(user, password) {
	application.output(globals.mpMessageLog + 'START authenticator.loginUser() '+user,LOGGINGLEVEL.INFO);
	var result = {
		logged: false,
		error: null
	};

	if (!user) {
		result.error = 'Lo username non può essere vuoto';
		application.output(globals.mpMessageLog + 'authenticator.loginUser() message error  '+result.error,LOGGINGLEVEL.DEBUG);
		return result;
	}

	if (!password) {
		result.error = 'La password non può essere vuota';
		application.output(globals.mpMessageLog + 'authenticator.loginUser() message error '+result.error,LOGGINGLEVEL.DEBUG);
		return result;
	}
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users>} */
	var users = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_users');

	if (users.find()) {
		users.user_name = user;
		if (users.search() != 0) {

			// kick out not enabled users
			if (users.is_enabled != 1) {
				result.error = "Utente non abilitato";
				application.output(globals.mpMessageLog + 'authenticator.loginUser() message error '+result.error,LOGGINGLEVEL.DEBUG);
				return result;
			}
			// successfully logged!
			if (users.user_password == globals.mpSha1(password)) {
				// get the user groups foundset
				/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_users_roles>} */
				var usersRoles = users.mp_users_to_mp_users_roles;

				// set groups array
				var rolesArray = [];

				// populates the array with the groups name
				for (var index = 1; index <= usersRoles.getSize(); index++) {
					rolesArray.push(usersRoles.getRecord(index).mp_users_roles_to_mp_roles.description);
				}

				if (security.login(user, users.user_id, rolesArray)) {
//					users.failed_login_attempts = 0;
					result.logged = true;
					application.output(globals.mpMessageLog + 'STOP authenticator.loginUser() user logged OK ',LOGGINGLEVEL.INFO);
					return result;
				}
			} else {
				// login attempt failed
				
//				users.failed_login_attempts++;
//				application.output(globals.messageLog + 'authenticator.loginUser() password KO login errate: '+ users.failed_login_attempts,LOGGINGLEVEL.DEBUG);
//				if (users.failed_login_attempts >= 3) {
//					users.is_locked = 1;
//				}
//				result.error = 'Utente o password sbagliata';
				result.error = 'Utente o password sbagliata. Login errate ';
			}
		}
		result.error = 'Utente o password sbagliata. Login errate ';
//
//		
//		if (users.failed_login_attempts > 0) 
//			result.error = 'Utente o password sbagliata. Login errate '; //+ users.failed_login_attempts;
//		application.output(globals.messageLog + 'authenticator.loginUser() message error: '+ result.error,LOGGINGLEVEL.DEBUG);
	}
	application.output(globals.mpMessageLog + 'STOP authenticator.loginUser() ',LOGGINGLEVEL.DEBUG);
	return result;
}
