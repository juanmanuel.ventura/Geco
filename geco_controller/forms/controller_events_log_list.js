
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"3F311D47-0874-40D9-B2D3-D4190CBE365C"}
 */
function openPopupPeriodEnable(event) {
	//application.createWindow("period enable", JSWindow.MODAL_DIALOG).show(forms.period_popup_events_log);
	var win = application.createWindow("period enable", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(400, 0, 430, 525);
	win.title = 'Disabilita mesi'
	win.resizable = true;
	databaseManager.setAutoSave(true);
	win.show(forms.period_popup_events_log);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"FFF9BEE8-6F42-4E5F-99CB-7138E044BA95"}
 * @AllowToRunInFind
 */
function openUserTimesheet(event) {
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) {
		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
		userId = bundle.elements['split']['getLeftForm']()['selectedUser'];
		jobOrder = bundle.elements['split']['getLeftForm']()['selectedJobOrder'];
		stato = bundle.elements['split']['getLeftForm']()['selectedStatus'];
	}
	if (month == null || year == null || userId == null){
		globals.DIALOGS.showErrorDialog('Error','Selezionare i campi Anno Mese e Utente','Chiudi');
		return;
	}
	
//	if (forms.print_user_events_log.foundset.find()) {
//		forms.print_user_events_log.foundset.user_id = userId;
//		forms.print_user_events_log.foundset.timesheet_month = month;
//		forms.print_user_events_log.foundset.timesheet_year = year;
//		if (jobOrder != null) 
//			forms.print_user_events_log.foundset.job_order_id = jobOrder;
//		forms.print_user_events_log.foundset.search();
//	}
	
	var maxReturnedRows = 10000; //useful to limit number of rows
//	var server_name = databaseManager.getDataSourceServerName(controller.getDataSource());
	var sql_query = 'call sp_user_timesheet(?,?,?,?)';
	var arguments = new Array();
	arguments[0] = userId;
	arguments[1] = year;
	arguments [2] = month;
	arguments [3] = (jobOrder == null)? 0: jobOrder;
	databaseManager.getDataSetByQuery('geco', sql_query, arguments, maxReturnedRows);
	if (forms.print_user_events_log_sp.foundset.find()) {
		forms.print_user_events_log_sp.foundset.search();
	}
	
	var win = application.createWindow('print', JSWindow.WINDOW);
	//var print_form = forms.print_user_events_log;
	var print_form = forms.print_user_events_log_sp;
	win.show(print_form);
}


/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"FA2E40EB-3005-45B2-9721-D5ACBB004883"}
 * @AllowToRunInFind
 */
function openMonthHours(event) {
	
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) {
		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
		stato = bundle.elements['split']['getLeftForm']()['selectedStatus'];
		userId = bundle.elements['split']['getLeftForm']()['selectedUser'];
	}
	if (month == null || year == null){
		plugins.dialogs.showErrorDialog('Error','Selezionare i campi Anno Mese');
		return;
	}
	
	var maxReturnedRows = 10000;
//	var server_name = databaseManager.getDataSourceServerName(controller.getDataSource());
	var sql_query = 'call sp_hoursMonthUsers(?,?,?,?)';
	var name = 'Riepilogo_Ore';
	var arguments = new Array();
	arguments[0] = year;
	arguments[1] = month;
	arguments [2] = (userId == null) ? 0 : userId;
	arguments [3] = (stato == null)? 0: stato;
	var dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, maxReturnedRows);
	
	var htmlTable = '<html>'+dataset.getAsHTML(true, true, false, true, true)+'</html>';
	
	
	
	forms.controller_month_hours.tableReport = htmlTable;
	forms.controller_month_hours.datasetReport = dataset;
	forms.controller_month_hours.month = month;
	forms.controller_month_hours.year = year;
	forms.controller_month_hours.nameExport = name;
	if(userId  != null){
		forms.controller_month_hours.user = userId;
	} 

	var win = application.createWindow('print', JSWindow.WINDOW);
	var print_form = forms.controller_month_hours;
	win.setInitialBounds(JSWindow.DEFAULT,JSWindow.DEFAULT,550,400);
	win.show(print_form);
	

}


/**
 * @param event
 * @properties={typeid:24,uuid:"AB9481C9-E77C-49FA-8AAB-FC0661FBFBAC"}
 */
function updateUI(event) {
	_super.updateUI(event);
	controller.enabled = !isEditing();
}
