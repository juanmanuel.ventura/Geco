/**
 * @type {Number}
 * @protected
 * @properties={typeid:35,uuid:"ADCF37E2-23E3-4D89-8B95-705E85556B8A",variableType:4}
 */
var isToClose = 0;

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C8E8BD48-792D-40F6-B2D6-A1690FB5D499"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	filterToCloseRecords()
	if (foundset.find()) {
		foundset.user_owner_id = userOwner;
		foundset.company_id = company;
		foundset.external_code_navision = codeNavision;
		foundset.profit_center_id = profitCenter;
		foundset.search();
		
		//application.output('controller ' + foundset.getSize());
		
	}
	//application.output('controller 2 ' + foundset.getSize());
}

/**
 * @properties={typeid:24,uuid:"49034D36-F648-4083-919B-DC4F6AF58FF1"}
 * @AllowToRunInFind
 */
function filterToCloseRecords() {
	// filter for entities that implement the field "is_to_close"
	if (foundset.alldataproviders.indexOf('is_to_close') != -1) {
		foundset.removeFoundSetFilterParam('toClose');
		foundset.addFoundSetFilterParam('is_to_close', '=', isToClose, 'toClose');
		foundset.loadAllRecords();
		//application.output('da chiudere ' + foundset.getSize());
	}
}
