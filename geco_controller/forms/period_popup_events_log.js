/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"CF3BBA5D-6801-4B73-8A06-AEB71C1A58A0"}
 */
function resetFilter(event) {
	_super.resetFilter(event)
	selectedYear = new Date().getFullYear();
	applyFilter(event);
}

/**
 * @param {Number} year
 * @param {Number} month
 * @AllowToRunInFind
 * @return {String}
 * @properties={typeid:24,uuid:"B9C7CE38-1263-4479-874F-D35AD2D20160"}
 */
function checkUserTimesheet(year, month) {
	var message = '';
	application.output(globals.messageLog + 'START period_popup_events.checkUserTimesheet() anno ' + year + ' mese ' + month, LOGGINGLEVEL.INFO);
	var startMonthDate = new Date(year, month - 1, 1);
	var endMonthDate = new Date(year, month, 0);
	var firstOfNextMonth = new Date(year, month, 1);
	var previousMonth = (month.toString().length == 1) ? '0' + month : month;
	var lastDay = endMonthDate.getDate();

	application.output(globals.messageLog + 'period_popup_events.checkUserTimesheet() start ' + startMonthDate + ' end ' + endMonthDate, LOGGINGLEVEL.INFO);
	/** @type {JSFoundSet<db:/geco/calendar_days>} */
	var calendar_days = databaseManager.getFoundSet('geco', 'calendar_days');
	var workableDay = 0;
	if (calendar_days.find()) {
		calendar_days.calendar_year = year;
		calendar_days.calendar_month = month;
		calendar_days.is_working_day = 1;
		calendar_days.is_holiday = 0;
		workableDay = calendar_days.search();
	}
	//application.output(globals.messageLog + 'period_popup_events.checkUserTimesheet() giorni lavorabili ' + workableDay, LOGGINGLEVEL.INFO);

	var totalUser = 0;
	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	if (users.find()) {
		users.is_enabled = 1;
		users.user_type_id = '7';
		users.fep_code = '!-1';
		totalUser = users.search();
		totalUser = databaseManager.getFoundSetCount(users)
		application.output(globals.messageLog + 'period_popup_events.checkUserTimesheet() dimensione reale users ' + databaseManager.getFoundSetCount(users));
	}

	var countUserNUmber = 0;
	var totalEvents = 0;

	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	for (var index = 1; index <= totalUser; index++) {
		var numGg = 0;
		startMonthDate = new Date(year, month - 1, 1);
		var user = users.getRecord(index);
		//var oreUser = user.working_hours * workableDay;
		/** @type {Date} */
		var start = new Date(year, month - 1, 1);
		/** @type {Date} */
		var end = firstOfNextMonth;
		var oreLavorabili = 0;
		//application.output('data assunzione di ' + user.user_name + ': ' + user.enrollment_start_date);
		//application.output('data dimissioni di ' + user.user_name + ': ' + user.enrollment_end_date);
		//assunto prima della fine del mese di riferimeto
		if (user.enrollment_start_date < firstOfNextMonth) {
			start = (user.enrollment_start_date > startMonthDate) ? user.enrollment_start_date : startMonthDate;
			end = (user.enrollment_end_date != null && user.enrollment_end_date < firstOfNextMonth) ? user.enrollment_end_date : endMonthDate;
			//end = data dimissioni o primo del mese successivo
			for (var dateStart = start; dateStart <= end; dateStart.setDate(dateStart.getDate() + 1)) {
				if (!globals.isWeekEnd(dateStart) && !globals.isHoliday(dateStart) && !globals.isUserHoliday(dateStart.getMonth() + 1, dateStart.getDate(), user.user_id)) {
					numGg++;
					var objTimeUser = globals.getUserTimeWorkByDay(user.user_id, dateStart);
					oreLavorabili = oreLavorabili + objTimeUser.workingHours;
				}
			}
			if (events_log.find()) {
				events_log.user_id = user.user_id;
				events_log.event_log_date = year + '-' + previousMonth + '-01...' + year + '-' + previousMonth + '-' + lastDay + '|yyyy-MM-dd';
				totalEvents = events_log.search();
			}
			var oreInserite = (totalEvents == 0) ? 0 : (events_log.total_duration_regular / 60);
			var oreMancanti = oreLavorabili - oreInserite;
			if (oreMancanti > 0) {
				message = message + user.user_name + ' totale ore ' + (events_log.total_duration_regular) / 60 + '  ore mancanti ' + oreMancanti + '\n';
			} else if ( (events_log.total_duration_regular) / 60 >= oreLavorabili) {
				//application.output('************ore raggiunte no mail');
			}
			countUserNUmber++
		} else {
			//application.output('*********assunto dopo il mese da disabilitare ');
		}
	}
	application.output(globals.messageLog + 'STOP period_popup_events.checkUserTimesheet() ', LOGGINGLEVEL.INFO);
	return message;

}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"41D8B614-14BE-4E64-8CAA-D9D742553093"}
 */
function saveEdits(event) {
	application.output(globals.messageLog + 'START period_popup_events.saveEdits() ', LOGGINGLEVEL.INFO);
	databaseManager.setAutoSave(false)
	var message = '';
	/** @type {JSDataSet} */
	var ds = null;
	for (var x = 1; x <= foundset.getSize(); x++) {
		var rec = foundset.getRecord(x);
		//application.output('rec ' +rec.month_number + ' ' + rec.month_name + '  ' + rec.month_year+ ' abilitato tm ' + rec.is_enabled_for_timesheet);
		/** @type {JSDataSet} */
		ds = rec.getChangedData();
		application.output(ds.getMaxRowIndex());
		for (var i = 1; i <= ds.getMaxRowIndex(); i++) {
			//application.output(ds.getValue(i, 1) + ' ' + ds.getValue(i, 2) + ' ' + ds.getValue(i, 3));
			if (ds.getValue(i, 1) == 'is_enabled_for_timesheet' && ds.getValue(i, 2) == 1 && ds.getValue(i, 3) == 0) {
				application.output(globals.messageLog + ' period_popup_events.saveEdits() chiama checkUserTimesheet ' + rec.month_name, LOGGINGLEVEL.INFO);
				//var result = checkUserTimesheet(rec.month_year, rec.month_number);
				var result = checkClosureMonth(rec.month_year, rec.month_number);
				if (result != '')
					message = message + 'mese ' + rec.month_name + '\n' + result + '\n';
			}
		}

	}

	application.output('messaggio ' +message);
	if (message != '') {
		globals.DIALOGS.showInfoDialog('Check Timesheet', message, 'OK');
	} else
		_super.saveEdits(event);
	application.output(globals.messageLog + 'STOP period_popup_events.saveEdits() ', LOGGINGLEVEL.INFO);

}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"8B9C7FF5-8D8C-4BE2-A105-F68FA5939A65"}
 */
function onShow(firstShow, event) {
	databaseManager.setAutoSave(false);
	return _super.onShow(firstShow, event)
}

/**
 * Handle hide window.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"D07C55D0-19D5-4B9E-8F0C-961EE38EC31B"}
 */
function onHide(event) {
	rollbackAndClose(event)
}

/**
 * @param {Number} year
 * @param {Number} month
 * @AllowToRunInFind
 * @return {String}
 * @properties={typeid:24,uuid:"3639D01A-2C24-4F39-986F-9BF2D8B72B23"}
 */
function checkClosureMonth(year, month) {
	var message = '';
	/** @type {JSFoundSet<db:/geco/calendar_days>} */
	var calendar_days = databaseManager.getFoundSet('geco', 'calendar_days');
	var workableDay = 0;
	if (calendar_days.find()) {
		calendar_days.calendar_year = year;
		calendar_days.calendar_month = month;
		calendar_days.is_working_day = 1;
		calendar_days.is_holiday = 0;
		workableDay = calendar_days.search();
	}

	var maxReturnedRows = 10000; //useful to limit number of rows
	//	var server_name = databaseManager.getDataSourceServerName(controller.getDataSource());
	var sql_query = 'call sp_getHoursMonth(?,?)';
	var arguments = new Array();
	arguments[0] = year;
	arguments [1] = month;
	var dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, maxReturnedRows);

	for (var index = 1; index <= dataset.getMaxRowIndex(); index++) {
		
		var riga = dataset.getRowAsArray(index);
		var userID = riga[0];
		var userName = riga[1];
		var dataAss = riga[3];
		var dataDim = riga[4]
		var oreInserite = riga[6];
		var oreRespinte = riga[7];
		application.output('-----------------------------------------------------\n' + userID + ' inserite ' + oreInserite);
		var oreLavorabili = 0;
		var numGg = 0;
		//application.output('Assunzione ' + dataAss + ' Dimissioni ' + dataDim + ' Data ' + dateStart);
		for (var i = 1; i <= calendar_days.getSize(); i++) {
			var dateStart = calendar_days.getRecord(i).calendar_date;
			//application.output(dataAss + ' - ' +dateStart + ' Assunzione minore data =  ' + (dataAss <= dateStart) + ' - Dimissioni maggiore di data = ' + (dataDim == null || dataDim >= dateStart) );
			if (dataAss <= dateStart && (dataDim == null || dataDim >= dateStart)) {
				if (!globals.isWeekEnd(dateStart) && !globals.isHoliday(dateStart) && !globals.isUserHoliday(dateStart.getMonth() + 1, dateStart.getDate(), userID)) {
					numGg++;
					var objTimeUser = globals.getUserTimeWorkByDay(userID, dateStart);
					oreLavorabili = oreLavorabili + objTimeUser.workingHours;
					
				}
			}
		}
		
		var oreMancanti = oreLavorabili - oreInserite;
		application.output('Utente '+userID + ' ore lavorabili =  ' + oreLavorabili + '  ore mancanti =  ' + oreMancanti);
		if (oreMancanti > 0) {
			message = message + userName + ' Ore inserite ' + oreInserite + ' mancanti ' + oreMancanti + ' respinte ' + oreRespinte +'\n';
		} 
	}
	return message;
}

