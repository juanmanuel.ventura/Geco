/**
 * @type {String}
 * @properties={typeid:35,uuid:"C0E6E753-44B6-4C49-8BD5-E292F74E0D93"}
 */
var tableReport = '';

/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"BE7F8B5B-D8DC-4DD1-B981-150247540795",variableType:-4}
 */
var datasetReport = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"63C52E92-3557-4CA8-9B13-0D4949901250"}
 */
var nameExport = '';

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"3F30E248-CFA0-483B-9C14-4B67C3390BA0"}
 */
function saveReport(event) {
	application.output(globals.messageLog + 'START controller_report_list.saveReport()' ,LOGGINGLEVEL.INFO);
	var success = false;
	var line = '';
	
	if (datasetReport != null) {
		
		var rec = datasetReport.getValue(1,1);
		application.output(rec);
		if(rec == null){
			globals.DIALOGS.showWarningDialog('Attenzione','Export vuoto','OK');
			return
		}

		var colArray = new Array()
		for (var i = 1; i <= datasetReport.getMaxColumnIndex(); i++)
		{
			colArray[i-1] = datasetReport.getColumnName(i)
			//note the -1, because an array is zero based and dataset is 1 based.
		}
		//application.output(colArray);
		line = colArray.join('\t')+ '\r\n';
		for (var index = 1; index <= datasetReport.getMaxRowIndex(); index++) {
			var row = datasetReport.getRowAsArray(index);
			var rowstring = row.join('\t') ;
			line = line + rowstring + '\n';
		}
		//application.output(line);
		if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
			/** @type {String} */
			var yearFrom = '' + forms.controller_report_filter.selectedDatefrom.getFullYear();
			/** @type {String} */
			var monthFrom = '' + (forms.controller_report_filter.selectedDatefrom.getMonth() + 1);
			/** @type {String} */
			var dayFrom = '' + forms.controller_report_filter.selectedDatefrom.getDate();
			/** @type {String} */
			var dateFrom = yearFrom + ((monthFrom.length > 1) ? monthFrom : '0' + monthFrom)+ ((dayFrom.length > 1) ? dayFrom : '0' + dayFrom);
			/** @type {String} */
			var yearTo = '' + forms.controller_report_filter.selectedDateAt.getFullYear();
			/** @type {String} */
			var monthTo = '' + (forms.controller_report_filter.selectedDateAt.getMonth() + 1);
			/** @type {String} */
			var dayTo = '' + forms.controller_report_filter.selectedDateAt.getDate();
			/** @type {String} */
			var dateTo = yearTo + (monthTo.length > 1 ? monthTo : '0' + monthTo) + (dayTo.length > 1 ? dayTo : '0' + dayTo);

			var fileName = nameExport + dateFrom + '_' + dateTo + '.xls';
			success = plugins.file.writeTXTFile(fileName, line, 'UTF-8', 'application/vnd.ms-excel');
			application.output(globals.messageLog + 'STOP controller_report_list.saveReport() : report OK' ,LOGGINGLEVEL.INFO);
		}
	} 
	if (!success) application.output(globals.messageLog + 'STOP controller_report_list.saveReport() : report fallito' ,LOGGINGLEVEL.INFO);
}


/**
 * 
 *@param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C1B2243B-7953-4527-8059-B9D21D331359"}
 */
function getUsers(event){
		globals.reportUsers(event);
}



/**
 * 
 *@param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"860024A3-6445-4D29-8CA1-C6C79C33A003"}
 */
function getProfitCenter(event){
		globals.reportPCManagers(event);
}





/**
 * 
 *@param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"F462FA38-313F-42B7-AC10-30485D34F083"}
 */
function getEvents(event){
		globals.reportEvents(event);
}


/**
 * 
 *@param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"68032777-F62B-4CC5-ADD8-D5B49D5A7906"}
 */
function getJobOrders(event){
		globals.reportJobOrders(event);
}


/**
 * 
 *@param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"788912B8-E580-4DA6-BFBA-432EF27217F8"}
 */
function getCompanies(event){
		globals.reportCompanies(event);
}

