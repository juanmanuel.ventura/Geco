/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"532A5F8E-350A-4498-987F-48212E5C6FBB"}
 */
function onLoad(event) {
	_super.onLoad(event);
	approver = null;
	isDirectApprover = null;
	
}


/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"01F5D6AB-5335-4FDE-84F8-0EB189714942"}
 */
function resetFilter(event) {
	selectedApprover = null;
	selectedProfitCenter = null;
	selectedUser = null;
	selectedJobOrder = null;
	selectedStatus = 1;
	userTypeForFilter = null;
	selectedMonth = new Date().getMonth() + 1;;
	selectedYear = new Date().getFullYear();;
	selectedFepCode = null;
	isDirectApprover = null;
	applyFilter();
	toggleButton(selectedStatus != null);
}

