/**
 * @type {Date}
 * @properties={typeid:35,uuid:"B487317F-028C-4DA5-9A03-7D493DC394AC",variableType:93}
 */
var selectedDatefrom = null;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"6DCD0357-5361-4E76-9E71-C628F6DC9432",variableType:93}
 */
var selectedDateAt = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"B3464809-3349-4685-96C1-677AC61DC9DF",variableType:8}
 */
var selectedJobOrder = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"C221FA25-A45E-4FFA-A986-E3C0EBCBFD21",variableType:8}
 */
var selectedUser = null;

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number}   queryType
 * @properties={typeid:24,uuid:"14CF4602-92A9-44EB-A141-D0A57D924E59"}
 */
function exportReport(event,queryType) {
	var _query_download = null;
	var nameExport = null;
	if (globals.isEmpty(selectedDatefrom) || globals.isEmpty(selectedDateAt)) {
		globals.DIALOGS.showWarningDialog("Errore", "Inserire la data di inizio e fine", "OK");
		return
	}
	var arguments = new Array();
//	if (globals.isEmpty(selectedDateAt)) {
//		globals.DIALOGS.showWarningDialog("Errore", "Inserire la data di fine", "OK");
//		return
//	}
	if (queryType == 1) {
		_query_download = 'call sp_getReportEventsLog ( ?, ?, ?, ?)';
		nameExport = 'Consuntivi_';
		arguments[0] = selectedDatefrom;
		arguments[1] = selectedDateAt;
		arguments[2] = (selectedUser != null) ? selectedUser : 0;
		arguments[3] = (selectedJobOrder != null) ? selectedJobOrder : 0;
	} 
	else if (queryType == 2) {
		
		_query_download = 'call sp_general_expenses_amount (  ?, ?, ?, ?, ?, ?, ?)';
		nameExport = 'NS_';
		arguments[0] = 0;
		arguments[1] = 0;
		arguments[2] = 0;
		arguments[3] = selectedDatefrom;
		arguments[4] = selectedDateAt;
		arguments[5] = (selectedUser != null) ? selectedUser : 0;
		arguments[6] = (selectedJobOrder != null) ? selectedJobOrder : 0;
		
	}
	
	//iFrom, iTo, iUserId, iJobOrderId, iUserOwnerId, iProfCenterUserId, iProfCenterJobId, iApproverId
	
	application.output(arguments);
	var dataset = databaseManager.getDataSetByQuery('geco', _query_download, arguments, 10000);
	
	var htmlTable = '<html>'+dataset.getAsHTML(true, true, false, true, true)+'</html>';
	
	forms.controller_report_list.tableReport = htmlTable;
	forms.controller_report_list.datasetReport = dataset;
	forms.controller_report_list.nameExport = nameExport;
	
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"753CF2E0-0629-4E4F-81F3-2C29184F82D0"}
 */
function refresh(event) {
	selectedDatefrom = null;
	selectedDateAt = null;
	selectedJobOrder = null;
	selectedUser = null;
		
	forms.controller_report_list.tableReport = '';
	forms.controller_report_list.datasetReport = null;
	forms.controller_report_list.nameExport = '';	
}

///**
// * @param event
// * @properties={typeid:24,uuid:"ED6A8E5F-236A-4343-AC6E-EAC20E55D05B"}
// */
//function updateUI(event) {
//	_super.updateUI(event);
//	if (globals.hasRole("Delivery Admin")) {
//		elements.slectReport.visible = true;
//	}
//	else elements.slectReport.visible = false;
//	
//	if (globals.isProfitCenterManager(scopes.globals.currentUserId) || globals.hasRole("Delivery Admin")) {
//		elements.buttonPresence.enabled = true;
//	}
//	else elements.buttonPresence.enabled = false;
//}
