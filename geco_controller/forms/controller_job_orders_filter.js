/**
 * @type {Number}
 * @protected
 * @properties={typeid:35,uuid:"27791D7D-1494-42A0-B303-6C677973AE30",variableType:4}
 */
var isToClose = 0;

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C4326F94-2A39-4598-BF20-03F4B8716B1B"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	filterToCloseRecords()
	if (foundset.find()) {
		foundset.user_owner_id = userOwner;
		foundset.company_id = company;
		foundset.external_code_navision = codeNavision;
		foundset.profit_center_id = profitCenter;
		foundset.search();
		
		//application.output('controller ' + foundset.getSize());
		
	}
	//application.output('controller 2 ' + foundset.getSize());
}

/**
 * @properties={typeid:24,uuid:"90BFFCAA-EEB7-4432-81A4-F62C1A7E4F8E"}
 * @AllowToRunInFind
 */
function filterToCloseRecords() {
	// filter for entities that implement the field "is_to_close"
	if (foundset.alldataproviders.indexOf('is_to_close') != -1) {
		foundset.removeFoundSetFilterParam('toClose');
		foundset.addFoundSetFilterParam('is_to_close', '=', isToClose, 'toClose');
		foundset.loadAllRecords();
		//application.output('da chiudere ' + foundset.getSize());
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"0AA5D924-77CE-408F-B7B1-A6B750B81EAA"}
 */
function filterRecords(event) {
	_super.filterEnabledRecords();
	applyFilter(event);
}
