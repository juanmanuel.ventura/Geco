///**@type {String}
// *
// * @properties={typeid:35,uuid:"5A0B8BDE-E9C6-46E0-955D-FE9486C996B1"}
// */
//var dynamicValuelist = null;
//
///**
// * @type {JSDataSet}
// *
// * @properties={typeid:35,uuid:"44DAE878-8D21-47C0-8EC9-3A38F89704A8",variableType:-4}
// */
//var dynamicDataset = null;
//
///**@type {Array<String>}
// *
// * @properties={typeid:35,uuid:"95094351-1D13-48C5-A7D9-4B9A9940AE3C",variableType:-4}
// */
//var solutionsList = [];

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C2C3A3DB-65D5-4AE4-B26E-988DB71A8D81"}
 */
function initialize(event) {
	if (!globals.grantAccess(['Controllers'])) {
		logOut(event);
	}
	_super.initialize(event);
//	dynamicDataset = databaseManager.createEmptyDataSet();
//	if (globals.hasRole('Approvers')) solutionsList.push('Approver');
//	//	if (globals.hasRole('Controllers')) solutionsList.push('Controller');
//	if (globals.hasRole('HR Administrators') || globals.hasRole('HR Expenses') || globals.hasRole('Admin Readers')) solutionsList.push('Personnel');
//	if (globals.hasRole('Delivery Admin') || globals.hasRole('Responsabili Commessa') || globals.hasRole('Controllers') || globals.hasRole('Resp. Mercato') || globals.hasRole('Orders Admin') || globals.hasRole('Planner') || globals.hasRole('Supp. Commerciale')) solutionsList.push('Management');
//	if (globals.currentUserDisplayName == 'STAFFA JONATHAN' || globals.currentUserDisplayName == 'FRAU DANIELA') solutionsList.push('Configurator');
//	solutionsList.push('Logger');
//	application.output('solutionsList.length: ' + solutionsList.length + '; solutionsList: ' + solutionsList);
//	if (solutionsList.length > 0) {
//		solutionsList.sort();
//		for (var index = 0; index < solutionsList.length; index++) {
//			dynamicDataset.addRow(new Array(solutionsList[index]));
//		}
//		elements.dynamicSolutionsChoice.setValueListItems(dynamicDataset);
//		elements.lSolutionName.enabled = true;
//		elements.lSolutionName.visible = true;
//		elements.dynamicSolutionsChoice.enabled = true;
//		elements.dynamicSolutionsChoice.visible = true;
//	}
	loadPanel(event, 'users_bundle');
	_super.checkPwdExpiration();
}

/**
 * Callback method for when form is shown.
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"7CBF9605-14EE-4B7D-BD24-3384DBA13CA5"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
//	dynamicValuelist = null;
}
