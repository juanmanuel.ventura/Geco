/**
 * @type {Number}
 * @properties={typeid:35,uuid:"B2EE15DA-6CAA-4116-B171-26FDACA536E5",variableType:8}
 */
var user = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"6F8BA9B3-7518-423E-9EA9-73A8F0B3EBB8"}
 */
var month = '';

/**
 * @type {String}
 * @properties={typeid:35,uuid:"F3FC25A0-F2C7-47A4-B8A7-23AFADC6854C"}
 */
var year = '';

/**
 * @type {String}
 * @properties={typeid:35,uuid:"73D5B869-3F78-4869-B183-655F85CB733C"}
 */
var tableReport = '';

/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"EF8C15BB-AE63-44D6-BC25-0C85116652B5",variableType:-4}
 */
var datasetReport = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"627EBDD3-D1C8-42EB-A8B1-4D7E92E30833"}
 */
var nameExport = '';

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"39913B05-F8CA-42DF-889D-DA6EC6888B80"}
 * @AllowToRunInFind
 */
function saveReport(event) {
	var success = false;
	var line = '';
	
	if (datasetReport != null) {
		
		var rec = datasetReport.getValue(1,1);
		application.output(rec);
		if(rec == null){
			globals.DIALOGS.showWarningDialog('Attenzione','Export vuoto','OK');
			return
		}

		var colArray = new Array()
		for (var i = 1; i <= datasetReport.getMaxColumnIndex(); i++)
		{
			colArray[i-1] = datasetReport.getColumnName(i)
			//note the -1, because an array is zero based and dataset is 1 based.
		}
		//application.output(colArray);
		line = colArray.join('\t')+ '\r\n';
		for (var index = 1; index <= datasetReport.getMaxRowIndex(); index++) {
			var row = datasetReport.getRowAsArray(index);
			var rowstring = row.join('\t') ;
			line = line + rowstring + '\n';
		}
		//application.output(line);
		if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
			
//			/** @type {String} */
//			var dateFrom = yearFrom + ((monthFrom.length > 1) ? monthFrom : '0' + monthFrom)+ ((dayFrom.length > 1) ? dayFrom : '0' + dayFrom);
			/** @type {String} */
			var mese = month.substr(0,1);
			var anno = year.substr(0,4);
			if(user != null){
			/** @type {JSFoundSet<db:/geco/users>} */
			var users = databaseManager.getFoundSet('geco', 'users');
			if(users.find()){
				users.user_id = user;
				var tot = users.search();
			}
			if(tot == 1) var nameUser = users.users_to_contacts.real_name;
			} else nameUser = '';
			var fileName = nameExport + '_' + ((mese.length > 1) ? mese : '0' + mese) + anno + '_' + nameUser + '.xls';
			success = plugins.file.writeTXTFile(fileName, line, 'UTF-8', 'application/vnd.ms-excel');
			user = null;
		}
	} 
	if (!success) application.output('Scrittura file non riuscita');
}


/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"150B8C8A-A8CB-4882-8301-AAFE02FB5C3E"}
 */
function close(event) {
	controller.getWindow().destroy();
}
