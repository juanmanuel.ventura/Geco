/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"44224E17-C441-4685-9B04-DF466E0C895F"}
 */
function onLoad(event) {
	_super.onLoad(event);

	approver = null;
	isDirectApprover = null;
}
