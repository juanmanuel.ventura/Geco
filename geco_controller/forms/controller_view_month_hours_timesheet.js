/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"6360338A-446A-4371-9C8E-60D094C4E0C1"}
 */
function onAction(event) {
	controller.print(false, true, plugins.pdf_output.getPDFPrinter());
}
