/**
 * @param {String} displayValue The value of a lookupfield that a user types
 * @param realValue The real value for a lookupfield where a display value should be get for
 * @param {JSRecord} record The current record for the valuelist.
 * @param {String} valueListName The valuelist name that triggers the method.
 * (This is the FindRecord in find mode, which is like JSRecord has all the columns/dataproviders,
 * but doesn't have its methods)
 * @param {Boolean} findMode True if foundset of this record is in find mode
 *
 * @returns {JSDataSet} A dataset with 1 or 2 columns display[,real]
 *
 * @properties={typeid:24,uuid:"39155850-BF53-49EE-AC81-3D49B2EBA06C"}
 */
function buildRulesValueList(displayValue, realValue, record, valueListName, findMode) {
	var valueList = databaseManager.createEmptyDataSet(0, ['displayValue']);

	switch (valueListName) {

	case 'entities':
		solutionModel.getScopeNames().forEach(function(scopeName) {
			if (scopeName.indexOf('rulesFor_') != -1) valueList.addRow(scopeName.slice(9));
		});

		break;
	}
	valueList.sort(1,true);
	return valueList;
}

/**
 * This method scans all the scopes looking for validation and process rules
 * to be imported into the database.
 *
 * For this to work these conventions must be observed:
 * 	- It must be one scope for every entity its wanted to define rules.
 * 	- The scope must be named "rulesFor_[entity name]" (no square brackets).
 * 	- The methods inside the scope must be prepended with "validate" for
 * 	  the validation rules and "process" for the business rules.
 * Only if these conditions are met the rules will be imported into the [rules]
 * table and they will be disabled by default.
 * 
 * Rules that are already present in the rules table will be ignored.
 *
 * @properties={typeid:24,uuid:"D29462ED-008F-43B6-89C4-65A1F8997949"}
 */
function scanForNewMethods() {

	// create an array with the rules scopes (if any)
	var rulesScopes = []; // [rulesFor_events_log, rulesFor_users, rulesFor_whatever...]
	solutionModel.getScopeNames().forEach(function(scopeName) {
		if (scopeName.indexOf('rulesFor_') != -1) rulesScopes.push(scopeName)
	});

	// create an object containing every scope and its defined rules (if any)
	var rulesForEntity = { };
	rulesScopes.forEach(function(scopeName) {
		// create an array containing all rules for the given entity
		var methods = [];
		var jsMethods = solutionModel.getGlobalMethods(scopeName);
		// load all methods for this scope
		jsMethods.forEach(function(jsMethod) {
			if ( (jsMethod.getName().indexOf('validate') != -1) || (jsMethod.getName().indexOf('process') != -1)) {
				methods.push(jsMethod.getName());
			} else {
				globals.DIALOGS.showErrorDialog("Errore", "Il metodo [" + jsMethod.getName() + "] nello scope [" + scopeName + "] è malformato\nLe regole devono essere prefissate con 'validate' o 'process'", 'OK');
			}
		});
		// take the "rulesFor_" part out the name
		rulesForEntity[scopeName.slice(9)] = methods;
	});

	// load all rules from the database
	/** @type {JSFoundSet<db:/geco/rules>} */
	var rules = databaseManager.getFoundSet('geco', 'rules');
	rules.loadAllRecords();
	var rulesInDB = [] // in the form of [scope.rule]
	for (var index = 1; index <= rules.getSize(); index++) {
		rulesInDB.push(rules.getRecord(index).working_entity + '.' + rules.getRecord(index).invoked_method);
	}

	// from the rules defined in scopes create the missing rules in the database (if any)
	for (var entity in rulesForEntity) {
		rulesForEntity[entity].forEach(function(rule) {
			if (rulesInDB.indexOf(entity + '.' + rule) == -1) {
				rules.newRecord(false);
				rules.rule_kind = (rule.indexOf('validate') != -1) ? 0 : 1;
				rules.invoked_method = rule;
				rules.working_entity = entity;
				rules.rule_name = "Nuova regola [" + rule + "] per [" + entity + "] importata e disabilitata di default"
				rules.loadAllRecords();
			}
		});
	}
	
	// reload filter
	forms.rules_filter.applyFilter(new JSEvent);
}

/**
 * Callback method for when solution is opened.
 *
 * @properties={typeid:24,uuid:"E1544F35-7888-413C-BE49-C09EE6224C0A"}
 */
function onSolutionOpenConfigurator() {
	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		application.putClientProperty(APP_WEB_PROPERTY.WEBCLIENT_TEMPLATES_DIR, 'custom');
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_DEFAULT_SCROLLABLE, true);
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_SCROLLABLE_KEEP_LOADED_ROWS, true);
	}
}
