/**
 * Handle changed data.
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @protected
 * @properties={typeid:24,uuid:"67AB3CDA-5DF8-4236-BAEF-CB00543F8F5C"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"27290FE6-05B7-404C-8347-8F28C926A47A"}
 */
function resetFilter(event, goTop) {
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"821DF376-4025-4FF5-92B6-64AB09DB7D08"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.search();
		foundset.sort('execution_order asc');
	}
}
