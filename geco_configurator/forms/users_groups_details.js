/**
 * 
 *
 * @param {JSEvent} event The event that triggered the action.
 * @returns {Boolean} true if form was validated and edits were saved.
 * @properties={typeid:24,uuid:"878C2FEB-57E3-4C39-92F1-3543777F771D"}
 */
function saveEdits(event) {

	forms.groups_for_user_list.deleteRecordsSelected(event);
	if (globals.deleteExceptionMessage != null && globals.deleteExceptionMessage != '') {
		application.output('groups_to_users ' + globals.deleteExceptionMessage);
		globals.DIALOGS.showErrorDialog('Error', globals.deleteExceptionMessage, 'OK');
	}
	
	_super.saveEdits(event);
	return true
}
