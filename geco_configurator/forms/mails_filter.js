/**
 * @type {Number}
 * @properties={typeid:35,uuid:"9314C61B-6BEA-44B5-B986-FC7850CB054A",variableType:8}
 */
var mailType= null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"0DA64C1E-1A52-4B08-8FC1-2271EF214612"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"D806A05A-190C-4BA6-BC96-C140CD356FA0"}
 */
function resetFilter(event, goTop) {
	mailType = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"1B270E0A-0FFA-4511-A838-2056F55FF562"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.mail_type_id = mailType;
		foundset.search();
	}
}
