/**
 * @type {String}
 * @properties={typeid:35,uuid:"623BC8BA-2D65-4417-8EEE-1E384FC47663"}
 */
var selectedEntity = null;

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"0782B0A7-4D89-4F7E-AD21-032500EFE575"}
 */
function goUp(event) {
	var recordAbove = foundset.getRecord(foundset.getSelectedIndex() - 1);
	if (recordAbove) {
		recordAbove.execution_order += 1;
		foundset.getSelectedRecord().execution_order -= 1;
		foundset.sort('execution_order');
		databaseManager.saveData(foundset);
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"DC688116-6B6A-4BBA-85FC-73B40965A5E7"}
 */
function goDown(event) {
	var recordBelow = foundset.getRecord(foundset.getSelectedIndex() + 1);
	// for a workaround of what seems to be a bug
	var wasFirstRecord = foundset.getSelectedIndex() == 1 ? true : false;
	if (recordBelow) {
		recordBelow.execution_order -= 1;
		foundset.getSelectedRecord().execution_order += 1;
		// the workaround
		if (wasFirstRecord) foundset.setSelectedIndex(2);
		foundset.sort('execution_order');
		databaseManager.saveData(foundset);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"7B3CCC17-C527-4020-B587-4B168251D9C2"}
 */
function onActionScanForNewRules(event) {
	scopes.globals.scanForNewMethods();
	orderRules(event);
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"011BD970-6CBC-42A6-ADEB-F36C1755FD10"}
 */
function orderRules(event) {
	//	foundset.loadAllRecords();
	databaseManager.setAutoSave(false);
	for (var index = 1; index <= foundset.getSize(); index++) {
		foundset.getRecord(index).execution_order = index;
	}
	databaseManager.setAutoSave(true);
	controller.sort('execution_order');
	_super.updateUI(event);
}

/**
 * @private
 *
 * @properties={typeid:24,uuid:"D4D46406-5395-432D-A063-CC7F63CEFF83"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	if (foundset.find()) {
		foundset.working_entity = selectedEntity;
		foundset.search();
	}
}

/**
 * Handle changed data.
 *
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"F50D00FC-81D4-4E7E-91CD-9FCB08030B8C"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	orderRules(event);
	return true
}
