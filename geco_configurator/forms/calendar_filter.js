/**
 * @type {Number}
 * @properties={typeid:35,uuid:"7A3C621E-BFC7-4D35-A492-7E0E2B379E92",variableType:8}
 */
var year = new Date().getFullYear();;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"DDC47218-5F8C-4418-B64E-CFA4BBE00564"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"0F3F8876-BEAB-4484-99A8-B0E418E97A5D"}
 */
function resetFilter(event, goTop) {
	year = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"8F8FDC93-E38E-4F89-AB6E-0FD0725DE1B6"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.month_year= year;
		foundset.search();
	}
}
