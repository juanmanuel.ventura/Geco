/**
 * @type {String}
 * @properties={typeid:35,uuid:"E841903D-E86D-4CC5-A259-39323C963B75"}
 */
var selectedEntity = null;

/**
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"5ED66A6D-0B9F-498D-9949-DC27C1808268"}
 */
function goUp(event) {
	var recordAbove = foundset.getRecord(foundset.getSelectedIndex() - 1);
	if (recordAbove) {
		recordAbove.execution_order += 1;
		foundset.getSelectedRecord().execution_order -= 1;
		foundset.sort('execution_order');
		saveRulesOrder(event);
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"983464B9-8761-410F-92EF-4C5C0A58FF96"}
 */
function goDown(event) {
	var recordBelow = foundset.getRecord(foundset.getSelectedIndex() + 1);
	// for a workaround to what seems to be a bug
	var wasFirstRecord = foundset.getSelectedIndex() == 1 ? true : false;
	if (recordBelow) {
		recordBelow.execution_order -= 1;
		foundset.getSelectedRecord().execution_order += 1;
		// the workaround
		if (wasFirstRecord) foundset.setSelectedIndex(2);
		foundset.sort('execution_order');
		saveRulesOrder(event);
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"DE580357-8132-432D-BF46-B9BF3211EABE"}
 */
function onActionScanForNewRules(event) {
	scopes.globals.scanForNewMethods();
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"1159A48E-943F-4AFD-9790-3D8BA10024FF"}
 */
function saveRulesOrder(event) {
	databaseManager.setAutoSave(false);
	for (var index = 1; index <= foundset.getSize(); index++) {
		var record = foundset.getRecord(index);
		record.execution_order = index;
	}
	databaseManager.setAutoSave(true);
	controller.sort('execution_order');
	_super.updateUI(event);
}
