///**@type {String}
// *
// * @properties={typeid:35,uuid:"5D1E2EBB-846E-49CA-9A62-7DC9650F7A8C"}
// */
//var dynamicValuelist = null;
//
///**
// * @type {JSDataSet}
// *
// * @properties={typeid:35,uuid:"C88BC97F-83FD-4520-AD74-9E62678B4A61",variableType:-4}
// */
//var dynamicDataset = null;
//
///**@type {Array<String>}
// *
// * @properties={typeid:35,uuid:"1D2C2CAB-2A48-49CB-B2B2-77D8A643C25B",variableType:-4}
// */
//var solutionsList = [];

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"3667A1B4-6DD2-40E8-B590-8E0F7B92E0F1"}
 * @AllowToRunInFind
 */
function initialize(event) {
	if (!globals.grantAccess(['Administrators'])) {
		logOut(event);
	}
	_super.initialize(event);
//	//setValueList(event);
//	dynamicDataset = databaseManager.createEmptyDataSet();
//	if (globals.hasRole('Approvers')) solutionsList.push('Approver');
//	if (globals.hasRole('Controllers')) solutionsList.push('Controller');
//	if (globals.hasRole('HR Administrators') || globals.hasRole('HR Expenses') || globals.hasRole('Admin Readers')) solutionsList.push('Personnel');
//	if (globals.hasRole('Delivery Admin') || globals.hasRole('Responsabili Commessa') || globals.hasRole('Controllers') || globals.hasRole('Resp. Mercato') || globals.hasRole('Orders Admin') || globals.hasRole('Planner') || globals.hasRole('Supp. Commerciale')) solutionsList.push('Management');
////	if (globals.currentUserDisplayName == 'STAFFA JONATHAN' || globals.currentUserDisplayName == 'FRAU DANIELA') solutionsList.push('Configurator');
//	solutionsList.push('Logger');
//	application.output('solutionsList.length: ' + solutionsList.length + '; solutionsList: ' + solutionsList);
//	if (solutionsList.length > 0) {
//		solutionsList.sort();
//		for (var index = 0; index < solutionsList.length; index++) {
//			dynamicDataset.addRow(new Array(solutionsList[index]));
//		}
//		elements.dynamicSolutionsChoice.setValueListItems(dynamicDataset);
//		elements.lSolutionName.enabled = true;
//		elements.lSolutionName.visible = true;
//		elements.dynamicSolutionsChoice.enabled = true;
//		elements.dynamicSolutionsChoice.visible = true;
//	}
	loadPanel(event,'groups_bundle');
}

/**
 * Callback method for when form is shown.
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"38BD8AA6-4B10-49CB-8F31-18DD0F5C5560"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
//	dynamicValuelist = null;
}
