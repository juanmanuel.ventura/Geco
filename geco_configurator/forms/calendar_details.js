/**
 * @type {Number}
 * @properties={typeid:35,uuid:"BA125653-1E65-4931-A1AB-2AA60A27C6D1",variableType:8}
 */
var newYear = null;


/**
 * Perform the element default action.
 * @param {JSEvent} event
 * @private
 *
 * @properties={typeid:24,uuid:"CDDB0BDC-9F25-4626-8BC1-E5913DB9E0E2"}
 * @AllowToRunInFind
 */
function addCalendar(event) {
	if (newYear == null || isNaN(newYear)|| newYear.toString().length < 4){
		globals.DIALOGS.showWarningDialog('Attenzione','Anno non valido','OK')
		application.output('anno non inserito o non valido')
		return
	}
	 
	else {
		/** @type {JSFoundSet<db:/geco/calendar_years>} */
		var calendar_years = databaseManager.getFoundSet('geco', 'calendar_years');
		if (calendar_years.find()) {
			calendar_years.year_number = newYear
			if (calendar_years.search() >= 1) {
				application.output('anno esistente');
			} else {
				calendar_years.newRecord();
				calendar_years.year_number = newYear;
				databaseManager.saveData(calendar_years)
			}
		}
		/** @type {JSFoundSet<db:/geco/calendar_months>} */
		var calendar_months = databaseManager.getFoundSet('geco', 'calendar_months');
		for (var i = 0; i < 12; i++) {
			if (calendar_months.find()) {
				calendar_months.month_year = newYear;
				calendar_months.month_number = i+1;
				if (calendar_months.search() >= 1) {
					application.output('esiste già il mese');
				} else {
					calendar_months.newRecord();
					calendar_months.month_year = newYear;
					calendar_months.month_number = i+1;
					calendar_months.month_name = scopes.globals.monthName[i];
					calendar_months.is_enabled_for_expenses = 1;
					calendar_months.is_enabled_for_timesheet = 1;
					databaseManager.saveData(calendar_months);
				}
			}
		}
		var start = new Date(newYear, 0, 1);
		var end = new Date(newYear, 11, 31);
		//start.setDate(start.getDate()+1);
		/** @type {JSFoundSet<db:/geco/calendar_days>} */
		var calendar_days = databaseManager.getFoundSet('geco', 'calendar_days');

		var giorno = start.getDate();
		var mese = start.getMonth() + 1
		var anno = start.getFullYear();

		application.output('data ' + anno + '-' + mese + '-' + giorno);
		application.output('giorno ' + giorno);
		application.output('mese ' + mese);
		application.output('anno ' + anno);
		application.output('giorno settimana ' + start.getDay());
		application.output('giorno lavorativo ' + ( (start.getDay() != 6 && start.getDay() != 0) ? 1 : 0));

		while (start <= end) {
			giorno = start.getDate();
			mese = start.getMonth() + 1
			anno = start.getFullYear();
			application.output('data ' + anno + '-' + mese + '-' + giorno);
			application.output('giorno settimana ' + start.getDay());
			application.output('giorno lavorativo ' + ( (start.getDay() != 6 && start.getDay() != 0) ? 1 : 0));
			application.output(' -----DATA ------ ' + start);
			var totDays = 0;
			calendar_days.loadAllRecords();
			if (calendar_days.find()) {
				calendar_days.calendar_date = start;
				totDays = calendar_days.search();
			}
			application.output(totDays);
			if (totDays >= 1) application.output('Giorno già creato')
			else {
				calendar_days.newRecord();
				calendar_days.calendar_date = start;
				calendar_days.calendar_day = giorno;
				calendar_days.calendar_month = mese;
				calendar_days.calendar_year = anno;
				calendar_days.calendar_weekday = start.getDay();
				calendar_days.is_working_day = (start.getDay() != 6 && start.getDay() != 0) ? 1 : 0;
				calendar_days.is_holiday = 0;

			}
			databaseManager.saveData(calendar_days);
			start.setDate(start.getDate() + 1);
		}
		newYear = null
	}
	stopEditing(event);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"9BB5B9ED-DB6B-40D2-A4CE-357667783371"}
 */
function setHoliday(event) {
	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	application.output('PRIMA ' + databaseManager.getTableCount(events_log));
	for (var index = 1; index <= foundset.calendar_months_to_calendar_days.getSize(); index++) {
		var record = foundset.calendar_months_to_calendar_days.getRecord(index);
		record.is_holiday = record.is_selected;				

		
		application.output("*** " + record.calendar_date + ' festa ' + record.is_holiday);
		var maxReturnedRows = 1000;
		var sql_query = 'call sp_events_holiday(?,?,?,?)';

		var dataset = null;
		//var line = '';
		var riga = null;
		var rigaStr = null;

		var arguments = new Array();

		var mm = record.calendar_month;
		if (mm.toString().length == 1) mm = '0' + mm;
		var yyyy = record.calendar_year;
		var dd = record.calendar_day;
		if (dd.toString().length == 1) dd = '0' + dd;
		arguments[0] = yyyy + '-' + mm + '-' + dd;
		arguments [1] = scopes.globals.currentUserId;
		//se festivo inserisco evento (1) altrimenti cancello evento (0)
		arguments[2] = record.is_holiday;
		arguments[3] = 0;
		if (record.calendar_days_to_head_office.head_office_id != null) {
			arguments[2] = 1;
			arguments[3] = record.calendar_days_to_head_office.head_office_id;
		}
		application.output('input ' + arguments);

		dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, maxReturnedRows)

		for (var insertIndex = 1; insertIndex <= dataset.getMaxRowIndex(); insertIndex++) {
			riga = dataset.getRowAsArray(insertIndex);
			//application.output('riga array' + riga);
			rigaStr = riga.join('|');
			//line = line + rigaStr + '\n';
			application.output('riga ' + rigaStr);
		}
		
		
	}
	_super.saveEdits(event);
	application.output('DOPO ' +databaseManager.getTableCount(events_log));
}

/**
 * 
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"DEEE6131-A7E8-44D1-94E6-BFD0CDFFC3A1"}
 */
function updateUI(event) {
	_super.updateUI(event);
	elements.buttonInsert.enabled = isEditing();
}

/**
 * 
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"85782D9F-9033-4C99-8A2D-558F51BFE550"}
 */
function stopEditing(event) {
	_super.stopEditing(event);
	newYear = null;
}
