dataSource:"db:/geco/groups",
extendsID:"22140138-4F6E-42E1-9DFD-BDB0BAF23C8E",
items:[
{
formIndex:3,
horizontalAlignment:2,
location:"5,77",
size:"96,20",
text:"Utente",
transparent:true,
typeid:7,
uuid:"115DA8EC-00C2-4E4E-991A-716BD39F412C"
},
{
extendsID:"5C441A2C-B69C-4578-BB2D-7EB8DAFF0A08",
height:119,
typeid:19,
uuid:"1FA1AFC1-0AF0-4FBC-AE72-65746F2A9433"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"isEnabled",
displayType:4,
formIndex:2,
horizontalAlignment:2,
location:"5,5",
onActionMethodID:"41B21BC2-C6EA-4B7A-AF63-3D1CC18DCA2A",
onDataChangeMethodID:"-1",
size:"153,20",
text:"Attivi",
toolTipText:"Mostra solo attivi",
transparent:true,
typeid:4,
uuid:"314EFC70-14B8-48E5-A78C-EAAF89AA11E1"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"userId",
displayType:2,
editable:false,
formIndex:1,
horizontalAlignment:2,
location:"106,77",
onActionMethodID:"-1",
onDataChangeMethodID:"7A26017B-904E-4B4D-9451-98CAB2719DF4",
size:"279,20",
text:"User Id",
toolTipText:"Filtra per gruppo",
typeid:4,
uuid:"4C6EAC6A-0D6A-4ABF-9A70-F3E7408E8423",
valuelistID:"3AB8DD5C-F02E-40DF-879B-9BFBCE22AF8D"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"usersGroup",
displayType:2,
editable:false,
formIndex:1,
horizontalAlignment:2,
location:"106,52",
onActionMethodID:"-1",
onDataChangeMethodID:"7A26017B-904E-4B4D-9451-98CAB2719DF4",
size:"279,20",
text:"User Id",
toolTipText:"Filtra per gruppo",
typeid:4,
uuid:"67A9D74D-3148-42A4-AA21-E5472DFEBAEE",
valuelistID:"EEBA6ADA-B659-4E79-80E9-0C49D5252B20"
},
{
anchors:11,
borderType:"SpecialMatteBorder,1.0,0.0,0.0,0.0,#cccccc,#000000,#000000,#000000,0.0,",
formIndex:10,
horizontalAlignment:0,
location:"0,38",
mediaOptions:6,
printSliding:16,
size:"400,1",
text:"",
transparent:true,
typeid:7,
uuid:"67BEF177-0E06-4348-ACD5-4EEDA9EE1849"
},
{
formIndex:3,
horizontalAlignment:2,
location:"5,52",
size:"96,20",
text:"Gruppo",
transparent:true,
typeid:7,
uuid:"703DA20B-59DC-4EDC-918E-1CD6C069B92B"
},
{
anchors:3,
customProperties:"",
formIndex:5,
imageMediaID:"B432FC13-6422-4A74-956D-4474A7CD5FF0",
location:"335,9",
onActionMethodID:"BB82D44D-9BA3-479D-BFBB-BBD4F6907F81",
rolloverImageMediaID:"B2C0D6C5-8456-4477-BFD5-825161DD2756",
showClick:false,
showFocus:false,
size:"16,17",
transparent:true,
typeid:7,
uuid:"AC7E44D6-73E1-4FC9-9FC0-967573B8639B"
}
],
name:"groups_filter",
onLoadMethodID:"41B21BC2-C6EA-4B7A-AF63-3D1CC18DCA2A",
typeid:3,
uuid:"64952A26-E8F7-48F5-A44C-F695EC9995FC"