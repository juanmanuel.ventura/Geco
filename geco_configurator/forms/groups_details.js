/**
 *
 *
 * @param {JSEvent} event The event that triggered the action.
 * @returns {Boolean} true if form was validated and edits were saved.
 * @properties={typeid:24,uuid:"89F07C33-97DC-409A-8F4C-D6A2CD3BEADB"}
 */
function saveEdits(event) {
	var rec = foundset.getSelectedRecord()
	if (rec.isNew()){
		security.createGroup(rec.name);
		application.output(security.getGroups())
	}
	
	forms.groups_to_users.deleteRecordsSelected(event);
	if (globals.deleteExceptionMessage != null && globals.deleteExceptionMessage != '') {
		application.output('groups_to_users ' + globals.deleteExceptionMessage);
		globals.DIALOGS.showErrorDialog('Error', globals.deleteExceptionMessage, 'OK');
	}
	_super.saveEdits(event);
	return true
}
