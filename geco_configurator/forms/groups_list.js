/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"C49269F3-B6C1-451B-B479-CCF87E682D58"}
 */
function newRecord(event) {
	return _super.newRecord(event)
}
