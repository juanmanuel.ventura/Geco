dataSource:"db:/geco/calendar_months",
extendsID:"22140138-4F6E-42E1-9DFD-BDB0BAF23C8E",
items:[
{
anchors:3,
customProperties:"",
formIndex:5,
imageMediaID:"B432FC13-6422-4A74-956D-4474A7CD5FF0",
location:"335,9",
onActionMethodID:"0F3F8876-BEAB-4484-99A8-B0E418E97A5D",
rolloverImageMediaID:"B2C0D6C5-8456-4477-BFD5-825161DD2756",
showClick:false,
showFocus:false,
size:"16,17",
transparent:true,
typeid:7,
uuid:"0DB80E4E-7450-425C-B954-634B78BC093A"
},
{
extendsID:"5C441A2C-B69C-4578-BB2D-7EB8DAFF0A08",
height:89,
typeid:19,
uuid:"7D249BF8-D9AB-4A82-954F-5C028A1D604C"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"isEnabled",
displayType:4,
formIndex:2,
horizontalAlignment:2,
location:"5,5",
onActionMethodID:"8F8FDC93-E38E-4F89-AB6E-0FD0725DE1B6",
onDataChangeMethodID:"-1",
size:"153,20",
text:"Attivi",
toolTipText:"Mostra solo attivi",
transparent:true,
typeid:4,
uuid:"D7937AF6-051A-41A7-B9D2-B0CF741DAE74",
visible:false
},
{
formIndex:3,
horizontalAlignment:2,
location:"5,52",
size:"96,20",
text:"Anno",
transparent:true,
typeid:7,
uuid:"F1CFFB72-B7FA-4FAA-9B79-30693C89BA8F"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"year",
displayType:2,
editable:false,
formIndex:1,
format:"####",
horizontalAlignment:2,
location:"106,52",
onActionMethodID:"-1",
onDataChangeMethodID:"DDC47218-5F8C-4418-B64E-CFA4BBE00564",
size:"140,20",
text:"User Id",
toolTipText:"Filtra per gruppo",
typeid:4,
uuid:"F995F284-5F31-476F-97D4-58B5DEC414DE",
valuelistID:"075E9257-460A-403C-B979-C6A3C3402907"
},
{
anchors:11,
borderType:"SpecialMatteBorder,1.0,0.0,0.0,0.0,#cccccc,#000000,#000000,#000000,0.0,",
formIndex:10,
horizontalAlignment:0,
location:"0,38",
mediaOptions:6,
printSliding:16,
size:"400,1",
text:"",
transparent:true,
typeid:7,
uuid:"FE4D5F16-32D9-4ACF-8418-567CD241BB9B"
}
],
name:"calendar_filter",
onLoadMethodID:"8F8FDC93-E38E-4F89-AB6E-0FD0725DE1B6",
typeid:3,
uuid:"232F83EF-4C5B-4CB9-A550-6380B688DDA9"