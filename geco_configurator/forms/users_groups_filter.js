/**
 * @type {Number}
 * @properties={typeid:35,uuid:"ACC72D63-A677-455E-9BA5-969AA0E6EDE9",variableType:8}
 */
var usersGroup = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"66144C7C-2AB6-4F9D-A665-13CCA97BAB04",variableType:8}
 */
var userId = null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"9437453E-E751-44E2-BFFE-CAE39FF0B372"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"D4E2B290-06D1-4411-9364-3BD4F71C84A5"}
 */
function resetFilter(event, goTop) {
	usersGroup = null;
	userId = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"F285CAF7-4373-4B52-A55D-FA4F36F6BB42"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.users_to_user_groups.group_id = usersGroup;
		foundset.user_id = userId;
		foundset.search();
	}
}
