/**
 * @type {Number}
 * @properties={typeid:35,uuid:"84C39365-3662-47B1-80EC-36B0FB5B29E4",variableType:8}
 */
var usersGroup = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"9ECD690C-F80D-4153-98ED-F16571AFF223",variableType:8}
 */
var userId = null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"7A26017B-904E-4B4D-9451-98CAB2719DF4"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"BB82D44D-9BA3-479D-BFBB-BBD4F6907F81"}
 */
function resetFilter(event, goTop) {
	usersGroup = null;
	userId = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"41B21BC2-C6EA-4B7A-AF63-3D1CC18DCA2A"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.group_id = usersGroup;
		foundset.groups_to_user_groups.user_id = userId;
		foundset.search();
	}
}
