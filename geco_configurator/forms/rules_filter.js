/**
 * @type {String}
 * @properties={typeid:35,uuid:"085F11EA-8848-4476-AF0F-EE452E6DAD86"}
 */
var selectedEntity = 'events_log';

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"75F2F966-45F0-4EB6-8710-628FDEADF39D",variableType:4}
 */
var selectedKind = 0;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"03413BED-8784-4235-A64B-3DB908A2A8FA"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"83303C5B-1487-4407-A813-A7F2D6D1E4B4"}
 */
function resetFilter(event, goTop) {
	selectedEntity = 'events_log';
	selectedKind = 0;
	_super.resetFilter(event, goTop)
	applyFilter(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"5A327618-7CCA-44E4-8371-73552B28E83B"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.working_entity = selectedEntity;
		foundset.rule_kind = selectedKind;
		foundset.search();
		foundset.sort('execution_order asc');
	}
}

