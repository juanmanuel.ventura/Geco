/**
 * @type {String}
 * @properties={typeid:35,uuid:"518C5D98-5A4A-44E5-80B1-B79E6E78C45D"}
 */
var selectedEntity = null;

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"659EFD8C-8A1D-49CE-8ABF-248115621285"}
 */
function goUp(event) {
	var recordAbove = foundset.getRecord(foundset.getSelectedIndex() - 1);
	if (recordAbove) {
		recordAbove.execution_order += 1;
		foundset.getSelectedRecord().execution_order -= 1;
		foundset.sort('execution_order');
		databaseManager.saveData(foundset);
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"274C35D5-7B8B-4CF2-8FD2-8C62771A0A8F"}
 */
function goDown(event) {
	var recordBelow = foundset.getRecord(foundset.getSelectedIndex() + 1);
	// for a workaround of what seems to be a bug
	var wasFirstRecord = foundset.getSelectedIndex() == 1 ? true : false;
	if (recordBelow) {
		recordBelow.execution_order -= 1;
		foundset.getSelectedRecord().execution_order += 1;
		// the workaround
		if (wasFirstRecord) foundset.setSelectedIndex(2);
		foundset.sort('execution_order');
		databaseManager.saveData(foundset);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"1B0629DB-BA11-4DE0-993B-00537F49141F"}
 */
function onActionScanForNewRules(event) {
	scopes.globals.scanForNewMethods();
	orderRules(event);
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"DF94F230-E6D2-4A5F-B505-1B64D551D181"}
 */
function orderRules(event) {
	//	foundset.loadAllRecords();
	databaseManager.setAutoSave(false);
	for (var index = 1; index <= foundset.getSize(); index++) {
		foundset.getRecord(index).execution_order = index;
	}
	databaseManager.setAutoSave(true);
	controller.sort('execution_order');
	_super.updateUI(event);
}

/**
 * @private
 *
 * @properties={typeid:24,uuid:"9C4C4785-7EC5-453A-AB80-2188EE193197"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	if (foundset.find()) {
		foundset.working_entity = selectedEntity;
		foundset.search();
	}
}

/**
 * Handle changed data.
 *
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"6EB67269-0507-4250-A039-6CFCB9446D03"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	orderRules(event);
	return true
}
