/**
 * @type {Number}
 * @properties={typeid:35,uuid:"A7E7A3EC-DDDB-4E27-A0EC-87F81497DDA1",variableType:8}
 */
var usersGroup = null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"A335D42F-8F3E-41FE-99C1-5685FBA915D9"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"C3DAE621-164E-41E9-9072-C2A4380C3EEA"}
 */
function resetFilter(event, goTop) {
	usersGroup = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"B834AFAF-9FCC-4038-9204-738A0E97E36B"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.users_to_user_groups.group_id = usersGroup;
		foundset.search();
	}
}
