dataSource:"db:/geco/media",
extendsID:"7D147E9E-8F99-4CAE-8E3B-6C45741464B5",
items:[
{
extendsID:"D2DC6F09-3211-46F8-BB43-D3D79D329349",
location:"444,378",
typeid:7,
uuid:"058982AE-56DA-43C9-8611-935C40927B84"
},
{
anchors:11,
dataProviderID:"media",
displayType:9,
formIndex:4,
horizontalAlignment:0,
location:"120,106",
size:"330,100",
typeid:4,
uuid:"1B13C738-2475-445A-A7F0-DBDB1E6C433E"
},
{
anchors:9,
formIndex:3,
horizontalAlignment:0,
labelFor:"",
location:"122,257",
name:"media_labelccc",
size:"48,20",
text:"24x24",
transparent:true,
typeid:7,
uuid:"24B07433-FAD4-4F14-926B-5580A059C527"
},
{
anchors:9,
dataProviderID:"media",
formIndex:2,
horizontalAlignment:0,
location:"231,234",
mediaOptions:14,
size:"16,16",
transparent:true,
typeid:7,
uuid:"24FA6504-1BAE-4861-83F6-3E52DEBE4E61"
},
{
anchors:11,
dataProviderID:"media_key",
location:"120,30",
size:"330,28",
typeid:4,
uuid:"2D212EFD-4DD4-4506-BF82-47531F0EB57E"
},
{
labelFor:"",
location:"30,106",
size:"80,28",
text:"Media",
transparent:true,
typeid:7,
uuid:"305D3C35-9034-4748-93B2-B7D4347551A5"
},
{
labelFor:"",
location:"29,297",
size:"80,28",
text:"Note",
transparent:true,
typeid:7,
uuid:"39903B6B-74EC-4441-A99E-36F483C1081B"
},
{
anchors:9,
dataProviderID:"media",
formIndex:4,
horizontalAlignment:0,
location:"421,242",
mediaOptions:14,
size:"8,8",
transparent:true,
typeid:7,
uuid:"54FB5403-39A7-4C0A-B0FF-0B417A191C4B"
},
{
anchors:11,
dataProviderID:"media_key_value",
location:"120,68",
size:"330,28",
typeid:4,
uuid:"879CE528-4BA2-4DB1-ABBB-4905FF724848"
},
{
labelFor:"",
location:"30,68",
size:"80,28",
text:"Value",
transparent:true,
typeid:7,
uuid:"9AFFACB2-E463-4C9A-BE51-D1893EEE33B6"
},
{
anchors:9,
formIndex:1,
horizontalAlignment:0,
labelFor:"",
location:"308,257",
name:"media_labelccccc",
size:"48,20",
text:"12x12",
transparent:true,
typeid:7,
uuid:"A2E0D49E-3BC4-43C5-8C27-D6DDF37D8E23"
},
{
extendsID:"A6A9C3D4-C72F-4B01-AE06-119D630A5887",
height:368,
typeid:19,
uuid:"A8FD427F-A701-461B-9BD8-679BCC7EDE2B"
},
{
extendsID:"E7451E69-4EF0-4E48-8702-0E01DD3AABF7",
location:"0,368",
size:"479,48",
typeid:7,
uuid:"ACDB94B0-0861-4875-8331-7C1608D228A6"
},
{
anchors:9,
horizontalAlignment:0,
labelFor:"",
location:"401,257",
name:"media_labelcccccc",
size:"48,20",
text:"8x8",
transparent:true,
typeid:7,
uuid:"AF0779AC-D752-462D-87CB-FBF8D2A9D436"
},
{
labelFor:"",
location:"30,30",
size:"80,28",
text:"Key",
transparent:true,
typeid:7,
uuid:"B69E1597-3186-47BE-8815-1A05BE816343"
},
{
extendsID:"584852A3-48F1-4566-90D3-FF43FE280208",
height:416,
typeid:19,
uuid:"C0E67A7B-7D74-4D0D-BC4E-A8F695DB8793"
},
{
anchors:9,
dataProviderID:"media",
formIndex:5,
horizontalAlignment:0,
location:"326,238",
mediaOptions:14,
size:"12,12",
transparent:true,
typeid:7,
uuid:"C1568B13-A28E-42A7-B0D0-FF8F5A2227F5"
},
{
anchors:9,
formIndex:6,
horizontalAlignment:0,
labelFor:"",
location:"215,257",
name:"media_labelcccc",
size:"48,20",
text:"16x16",
transparent:true,
typeid:7,
uuid:"CB3E68C7-2D8D-4321-A122-7D4ED9C9ED83"
},
{
anchors:9,
dataProviderID:"media",
formIndex:7,
horizontalAlignment:0,
location:"134,226",
mediaOptions:14,
size:"24,24",
transparent:true,
typeid:7,
uuid:"E650CB86-4A05-48F1-A73E-48D8EC70014A"
},
{
anchors:15,
dataProviderID:"note",
displayType:1,
location:"119,297",
scrollbars:34,
size:"330,41",
typeid:4,
uuid:"F30AA072-BB35-4CDA-A259-B0C19D176ABE"
},
{
extendsID:"B649AC0D-00D5-4039-9F29-473D6E55F601",
location:"444,378",
typeid:7,
uuid:"F7BA399C-BFCA-4ACC-8BCB-6CCA65320905"
},
{
extendsID:"7D3B8205-CC20-43C3-989D-1A127CF8CFD6",
location:"403,378",
typeid:7,
uuid:"F9D4C32B-E911-4446-8506-BE21B7081F3A"
}
],
name:"media_details",
showInMenu:true,
size:"479,416",
styleName:"GeCo",
typeid:3,
uuid:"45194339-CDF3-42EE-8E0D-422D58AC438A",
view:5