/**
 * @properties={typeid:24,uuid:"CD5117E6-C9D1-4047-9E89-1CBF269A5E03"}
 * @SuppressWarnings(deprecated)
 */
function onSolutionOpen() {
	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		application.putClientProperty(APP_WEB_PROPERTY.WEBCLIENT_TEMPLATES_DIR, 'custom');
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_DEFAULT_SCROLLABLE, true);
		application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_SCROLLABLE_KEEP_LOADED_ROWS, true);
	}
}