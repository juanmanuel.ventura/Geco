/**
 * @type {Number}
 * @properties={typeid:35,uuid:"E69CB873-6099-4E41-BE6D-E21C03BB54AB",variableType:8}
 */
var year = new Date().getFullYear();

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @protected
 * @properties={typeid:24,uuid:"AE1CE6B1-A1C3-4131-B669-A2E495F492BD"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"5CCEDD9A-C229-421C-8A91-8B9FE34C1F73"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	if (foundset.find()) {
		foundset.document_year = year;
		foundset.user_id = globals.currentUserId;
		foundset.search();
		foundset.sort('document_month asc');
	}
}
