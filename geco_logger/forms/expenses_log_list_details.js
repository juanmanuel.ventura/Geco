/**
 * @properties={typeid:35,uuid:"8AC9932F-D2AA-46DC-BF49-B4C24905C77A",variableType:-4}
 */
var isShifting = false;

/**
 * Handle changed data.
 * @param oldValue old value
 * @param newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @properties={typeid:24,uuid:"B87A0251-4AFC-41F9-A5A6-DF7E0D4E7E5A"}
 * @AllowToRunInFind
 */
function setExpenseType(oldValue, newValue, event) {
	switchPanels();
	return true;
}

/**
 * @return {Boolean}
 * @properties={typeid:24,uuid:"370BA714-B44E-45EF-ADB2-587EEF92CFF0"}
 */
function switchPanels() {
	// if new record unload panels and exit
	if (foundset.getSize() == 0) {
		elements.amount_g.visible = false;
		elements.km_g.visible = false;
		return false;
	}

	if (foundset.expense_type_id && foundset.expenses_log_to_expense_types.has_km == 0) {
		elements.amount_g.visible = true;
		elements.km_g.visible = false;
		//elements.km  = null;
		if (isEditing()) {
			foundset.km = null;
			foundset.total_amount = null;
			foundset.total_amount_km = null;
		}
	}
	if (foundset.expense_type_id && foundset.expenses_log_to_expense_types.has_km == 1) {
		elements.amount_g.visible = false;
		elements.km_g.visible = true;
		if (isEditing()) {
			foundset.amount = null;
			foundset.currency = 0;
			foundset.expense_change = 1;
			foundset.total_amount = null;
			foundset.total_amount_money = null;
		}
	}
	// default exit
	return true;
}

/**
 * Handle record selected.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"211F77A1-09C2-412D-8E7B-2AE285B32306"}
 */
function onRecordSelection(event) {
	if (isShifting == false) {
		_super.onRecordSelection(event);
		switchPanels();
		showButtonShiftMonth();
	}
}

/**
 * Handle changed data.
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"D42F5B5C-3D96-4D65-90AF-46E34915D684"}
 */
function setJobOrderExternalCode(oldValue, newValue, event) {
	if (oldValue != newValue) {
		foundset.external_code = foundset.expenses_log_to_job_orders.external_code_navision;
	}
	return true
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"87B3A40D-F982-4F81-8513-D6C2BFAF1F25"}
 */
function showNote(event) {
	globals.DIALOGS.showInfoDialog("Nota dall' approvatore", foundset.note,'OK');
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"BA57B7B8-C348-47DA-9146-F2E5153198A4"}
 */
function newRecord(event) {
	_super.newRecord(event);

	foundset.expense_year = globals.workingYear;
	foundset.expense_month = forms.calendar_months_list.month_number;
	foundset.user_id = globals.currentUserId;
	foundset.currency = 0;
	foundset.expense_change = 1;

	globals.selectedExpensesType = null;
	elements.expenseType.requestFocus();
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"1D23CC87-74BA-4434-9E3E-16183C740CAE"}
 */
function showProgressive(event) {
	var win = application.createWindow("selezione", JSWindow.MODAL_DIALOG);
	globals.callerForm = forms[event.getFormName()];
	win.show(forms.expenses_log_progress_picker);
}

/**
 * Opens a pop up to select date
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"EB1E9E06-55C0-44C6-A96F-A2471E09A6A1"}
 */
function openPopupCopy(event) {
	var win = application.createWindow("copia", JSWindow.MODAL_DIALOG);
	win.title = "Copia evento";
	win.show(forms.log_duplicator_expenses);
}

/**
 * @param {Date} start
 * @param {Date} end
 * @param {Number} skip
 * @param {Number} year
 * @param {Number} month
 * @properties={typeid:24,uuid:"44A7C0E3-A451-477B-A8DA-4E5D9F686D77"}
 */
function processDuplicateExpensesLog(start, end, skip, year, month) {

	var params = {
		processFunction: callDuplicateExpensesLog,
		message: 'Operazione in corso, attendere...',
		processArgs: [start, end, skip, year, month],
		opacity: 0.75,
		paneColor: '#000000',
		textColor: '#FFFFFF',
		showCancelButton: false,
		fontType: 'Calibri,0,48'
	};

	plugins.busy.block(params);
}

/**
 * @param {Date} start
 * @param {Date} end
 * @param {Number} skip
 * @param {Number} year
 * @param {Number} month
 * @properties={typeid:24,uuid:"2455FD4E-0255-433D-B50A-FAF2CBA3314E"}
 */
function callDuplicateExpensesLog(start, end, skip, year, month) {
	application.updateUI();
	try {
		duplicateExpensesLog(start, end, skip, year, month);
		// trap any exception here (or throw)
	} catch (e) {
		application.output(e);
	} finally {
		if (plugins.busy) plugins.busy.unblock();
	}
}

/**
 * @param {Date} start
 * @param {Date} end
 * @param {Number} skip
 * @param {Number} year
 * @param {Number} month
 * @properties={typeid:24,uuid:"C0F9E920-1DC7-45EA-8D27-BF542D6A67E7"}
 * @AllowToRunInFind
 */
function duplicateExpensesLog(start, end, skip, year, month) {

	// get the original record
	var record = foundset.getSelectedRecord();

	// skip if no record is selected
	if (!record) {
		application.output("No record selected");
		return;
	}

	// check for disbled month
	if (foundset.expenses_log_to_calendar_months.is_enabled_for_expenses == 0) {
		globals.DIALOGS.showErrorDialog("Errore", "Non è possibile inserire note spese su un mese disabilitato", "OK");
		return;
	}

	// sanitize dates
	start.setHours(0, 0, 0);
	end.setHours(0, 0, 0);

	var indexToDuplicate = foundset.getRecordIndex(record);
	/** @type {JSRecord<db:/geco/expenses_log>} */
	var originalRecord = foundset.getRecord(indexToDuplicate);

	// loop thru dates
	var count = 1;
	for (var date = start; start <= end; date.setDate(date.getDate() + 1)) {
		count++;
		// skip weekends
		if (skip == 1 && globals.isWeekEnd(date)) {
			continue;
		}
		//prendo il record originario
		indexToDuplicate = foundset.getRecordIndex(originalRecord);
		databaseManager.setAutoSave(false);
		indexToDuplicate = foundset.duplicateRecord(indexToDuplicate, true, true);
		var newrecord = foundset.getRecord(indexToDuplicate);
		newrecord.expense_date = date;
		newrecord.expense_log_status = 1;
		newrecord.expense_month = month;
		newrecord.expense_year = year;
		newrecord.actual_approver_id = null;;
		if (record.expenses_log_to_expense_types.has_km == 0) {
			newrecord.expense_reference = originalRecord.expense_reference + "_" + count;
		}
		if (!databaseManager.setAutoSave(true)) {
			application.output('reecord fallito '+databaseManager.getFailedRecords(foundset));
			databaseManager.revertEditedRecords();
			if (globals.validationExceptionMessage) {
				globals.DIALOGS.showErrorDialog('Error', globals.validationExceptionMessage, 'OK');
			}
			databaseManager.setAutoSave(true);
			
		}
	}

	updateUI(new JSEvent);

}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"F6C7534B-9711-438B-A867-101EE7ADDE8B"}
 * @SuppressWarnings(wrongparameters)
 * @AllowToRunInFind
 */
function shiftMonth(event) {
	var answer = globals.DIALOGS.showWarningDialog("Sposta", "Stai per spostare la nota spesa al mese prossimo.\nProcedere?", "Si", "No");

	if (answer == "No") return;
	isShifting = true;
	/** @type {JSRecord<db:/geco/expenses_log>} */
	var record = foundset.getSelectedRecord();

	/** @type {Number} */
	var month = record.expense_month;

	/** @type {Number} */
	var year = record.expense_year;

	if (month == 12) {
		year = year + 1;
		month = 1;
	} else {
		month = month + 1;
	}
	_super.startEditing(event);
	/** @type {JSFoundSet<db:/geco/expenses_log>} */
	var expenses_log = databaseManager.getFoundSet('geco', 'expenses_log');
	expenses_log.shiftMonth(record, month, year);
	if(!_super.saveEdits(event))
		_super.stopEditing(event);
	isShifting = false;
//	updateUI(event);
	onRecordSelection(event);

}

/**
 * @properties={typeid:24,uuid:"377BF027-6C88-4EA2-B340-15C75A25318A"}
 */
function showButtonShiftMonth() {
	if (foundset.getSize() > 0) {
		/** @type {JSRecord<db:/geco/expenses_log>} */
		var record = foundset.getSelectedRecord();
		//se il mese selezionato è chiuso per la consuntivazione
		if (record.expenses_log_to_calendar_months.is_enabled_for_expenses == 0) {
			// se il record è chiuso o approvato non è possibile spostare la nota spesa
			if (record.expense_log_status == 4 || record.expense_log_status == 2) {
				elements.buttonMove.visible = false;
			} else {
				elements.buttonMove.visible = !isEditing() ? true : false;
			}
		} else {
			//se il mese selezionato è aperto per la consuntivazione
			elements.buttonMove.visible = false;
		}
	} else {
		elements.buttonMove.visible = false;
	}
}

/**
 * @properties={typeid:24,uuid:"12AA414F-9D1D-417A-A42C-FA32D25AE662"}
 * @AllowToRunInFind
 */
function printExpenses() {

	if (forms.print_user_expenses_log.foundset.find()) {
		forms.print_user_expenses_log.foundset.user_id = security.getUserUID();
		forms.print_user_expenses_log.foundset.expense_month = globals.selectedMonth;
		forms.print_user_expenses_log.foundset.expense_year = globals.selectedYear;
		forms.print_user_expenses_log.foundset.search();
		forms.print_user_expenses_log.foundset.sort("expense_date asc, expense_reference asc");
	}

	var win = application.createWindow('print', JSWindow.WINDOW);
	var print_form = forms.print_user_expenses_log;
	win.setInitialBounds(200,100,1000,500)
	win.show(print_form);
}

/**
 * Handle changed data.
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"6A0C3CDE-0719-449E-BA06-8F9F4D36A36B"}
 */
function onDataChangeCurrency(oldValue, newValue, event) {
	if (newValue != oldValue && newValue == 0) {
		foundset.expense_change = 1;

	}

}

/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"79BD807E-7B70-4ADC-9896-ED73CCAE1D1B"}
 */
function updateUI(event) {
	// call parent first
	if (isShifting == false) {
		_super.updateUI(event);

		var record = null;
		var status = false;
		var notePresent = false;
		// only if there is a record
		if (foundset.getSelectedRecord()) {
			// get current record
			record = (foundset.getSelectedRecord()) ? foundset.getSelectedRecord() : null;

			// disallow editing and deleting if status = approved || closed
			status = (record.expense_log_status == 2 || record.expense_log_status == 4) ? false : true;

			// show note icon only if present
			notePresent = !globals.isEmpty(record.note);
		}

		// fields
		elements.expenseType.enabled = isEditing() && status;
		elements.expenseDate.enabled = isEditing() && status;
		elements.jobOrder.enabled = isEditing() && status;
		elements.description.enabled = isEditing() && status;
		elements.km.enabled = isEditing() && status;
		elements.km_g.enabled = isEditing() && status;
		elements.amount.enabled = isEditing() && status;
		elements.amount_g.enabled = isEditing() && status;
		elements.is_billable.enabled = isEditing() && status;

		// show note when present
		elements.buttonNote.visible = notePresent;

		// buttons
		elements.buttonCopy.visible = !isEditing() && (record) ? true : false;
		elements.buttonEdit.visible = !isEditing() && status;
		elements.buttonDelete.visible = !isEditing() && (status) ? status : false;

		elements.buttonProgress.enabled = isEditing() && status;
		// sort
		if (!isEditing()) {
			foundset.sort('expense_date  asc');
		}
		
		var bundle = forms[controller.getFormContext().getValue(2, 2)];
		if (bundle) {
			bundle.elements['split'].visible = !isEditing();
			bundle['focus'] = (record && isEditing()) ? record.expenses_log_to_calendar_months.month_name + ' ' + globals.workingYear : '';
			bundle.elements['focus'].visible = isEditing();
		}
	}
}
