/**
 * @type {Date}
 * @properties={typeid:35,uuid:"6E7C823D-1C96-4331-B6EE-EA675191AA24",variableType:93}
 */
var now = new Date();

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"32BAE2A7-B343-4350-9DD0-226AC3DBBD40",variableType:8}
 */
var workingYear = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"6F524D8F-E797-4632-98EE-4A812E196507",variableType:8}
 */
var workingMonth = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"8948541A-FE84-455B-9321-6914A9BBF147",variableType:8}
 */
var sumWorked = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"6D361DD4-3FDA-44E3-95DC-AAB0BC6F79C9",variableType:8}
 */
var sumRetrieved = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"59174806-2B10-4F9A-97E1-58CF238E9F2C",variableType:8}
 */
var sumRemained = null;

/**
 * @properties={typeid:35,uuid:"9F47B285-5A21-425E-A8C3-E7F8BC5B7234",variableType:-4}
 */
var currentWin = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F1E7FBF4-1DB7-492A-9316-F871624DB836",variableType:8}
 */
var sumRejectedWorked = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"EA452807-0080-452D-8531-96F6930FB2CC",variableType:8}
 */
var sumRejectedRetr = null;

/**
 * Filter records
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"D019BC00-C2B8-4AF0-911A-D1BFC72D1BD9"}
 */
function applyFilter(event) {
	application.output(globals.messageLog + 'START popupRecapHours applyFilter ', LOGGINGLEVEL.DEBUG);
	sumWorked = 0, sumRetrieved = 0, sumRemained = 0, sumRejectedRetr = 0, sumRejectedWorked = 0;
	//mi calcolo la data inserita sul record trovato in formato stringa
	var dateToConfront = new Date(workingYear, workingMonth, 0);
	var dayTC = '' + (dateToConfront.getDate());
	var monthTC = '' + (dateToConfront.getMonth() + 1);
	var dateTC = dateToConfront.getFullYear() + '-' + (monthTC.length > 1 ? monthTC : '0' + monthTC) + '-' + (dayTC.length > 1 ? dayTC : '0' + dayTC);
	//application.output('dateToConfront: ' + dateTC);

	//mi calcolo la data massima precedente su cui posso cercare; scopes.globals.elapsedMaxMonth mi dice di quanto posso tornare indietro.
	var elapsedMaxDate = new Date(workingYear, workingMonth - scopes.globals.elapsedMaxMonth, 0);
	var dayEMD = '' + (elapsedMaxDate.getDate());
	var monthEMD = '' + (elapsedMaxDate.getMonth() + 1);
	var dateEMD = elapsedMaxDate.getFullYear() + '-' + (monthEMD.length > 1 ? monthEMD : '0' + monthEMD) + '-' + (dayEMD.length > 1 ? dayEMD : '0' + dayEMD);
	//application.output('dateElapsedMaxDate: ' + dateEMD);

	if (foundset.find()) {
		//application.output('workingMonth: ' + workingMonth);
		if (workingMonth == null) {
			foundset.el_year = workingYear;
		} else {
			foundset.el_competence_month = ' ' + dateEMD + '...' + dateTC + ' |yyyy-MM-dd';
			foundset.is_expired = 0;
		}
		foundset.user_id = scopes.globals.currentUserId;
		foundset.search();
	}
	foundset.sort('el_year asc, el_month asc');

	for (var index = 1; index <= foundset.getSize(); index++) {
		var recIndex = foundset.getRecord(index);
//		sumWorked = sumWorked + recIndex.duration_worked - (sumRejectedWorked + recIndex.duration_worked_rejected);
		sumWorked = sumWorked + recIndex.duration_worked;
//		sumRetrieved = sumRetrieved + recIndex.duration_retrieved - (sumRejectedRetr + recIndex.duration_retrieved_rejected);
		sumRetrieved = sumRetrieved + recIndex.duration_retrieved;
//		sumRemained = sumRemained + recIndex.duration_remained - sumWorked;
		sumRemained = sumRemained + recIndex.duration_remained;
		sumRejectedWorked = sumRejectedWorked + recIndex.duration_worked_rejected;
		sumRejectedRetr = sumRejectedRetr + recIndex.duration_retrieved_rejected;
	}
	sumWorked = scopes.globals.roundNumberWithDecimal( (sumWorked / 60), 2);
	sumRetrieved = scopes.globals.roundNumberWithDecimal( (sumRetrieved / 60), 2);
	sumRemained = scopes.globals.roundNumberWithDecimal( (sumRemained / 60), 2);
	sumRejectedWorked = scopes.globals.roundNumberWithDecimal( (sumRejectedWorked / 60), 2);
	sumRejectedRetr = scopes.globals.roundNumberWithDecimal( (sumRejectedRetr / 60), 2);
	application.output(globals.messageLog + 'popupRecapHours.applyFilter() sumWorked: ' + sumWorked + '; sumRetrieved: ' + sumRetrieved + '; sumRemained: ' + sumRemained + '; sumRejectedWorked: ' + sumRejectedWorked + '; sumRejectedRetr: ' + sumRejectedRetr, LOGGINGLEVEL.DEBUG);
	application.output(globals.messageLog + 'STOP popupRecapHours applyFilter ', LOGGINGLEVEL.INFO);
}

///**
// * Clear selection, revert db changes and close popup
// * @param {JSEvent} event the event that triggered the action
// * @properties={typeid:24,uuid:"A103D346-95CE-4FE3-ADCB-5AB5FB3643D1"}
// */
//function rollbackAndClose(event) {
//	application.output(globals.messageLog + 'START popupRecapHours.rollbackAndClose() ', LOGGINGLEVEL.DEBUG);
//	currentWin = controller.getWindow();
//	if (currentWin != null) currentWin.destroy();
//	application.output(globals.messageLog + 'STOP popupRecapHours.rollbackAndClose() ', LOGGINGLEVEL.DEBUG);
//}

/**
 * Handle hide window.
 * @param event
 * @properties={typeid:24,uuid:"2D63A00E-4353-4BE4-AE1E-76CF4E1895A0"}
 */
function onHide(event) {
	application.output(globals.messageLog + 'START popupRecapHours.onHide() ', LOGGINGLEVEL.DEBUG);
	currentWin = controller.getWindow();
	if (currentWin != null) currentWin.destroy();
	application.output(globals.messageLog + 'STOP popupRecapHours.onHide() ', LOGGINGLEVEL.DEBUG);
}

/**
 * Callback method for when form is shown.
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"5039853C-D2F0-4341-AC38-093C7EB2851F"}
 */
function onShow(firstShow, event) {
	if (firstShow) {
		workingYear = now.getFullYear();
		workingMonth = now.getMonth() + 1;
	}
	applyFilter(event);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"595CAD96-EA5D-4A08-BFD8-85046B4CB802"}
 */
function applyHistory(event) {
	application.output(globals.messageLog + 'START popupRecapHours applyHistory ', LOGGINGLEVEL.DEBUG);
	elements.labelMonthc.visible = false;
	elements.monthc.visible = false;
	workingMonth = null;
	workingYear = workingYear ? workingYear : now.getFullYear();
	applyFilter(event);
	elements.bApplyHistory.enabled = false;
	application.output(globals.messageLog + 'STOP popupRecapHours applyHistory ', LOGGINGLEVEL.DEBUG);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"DECA0F69-A45C-41C2-BB82-D69D424D134D"}
 */
function resetFilter(event) {
	application.output(globals.messageLog + 'START popupRecapHours resetFilter ', LOGGINGLEVEL.DEBUG);
	elements.labelMonthc.visible = true;
	elements.monthc.visible = true;
	workingYear = now.getFullYear();
	workingMonth = now.getMonth() + 1;
	applyFilter(event);
	elements.bApplyHistory.enabled = true;
	application.output(globals.messageLog + 'STOP popupRecapHours resetFilter ', LOGGINGLEVEL.DEBUG);
}

/**
 * @param event
 * @properties={typeid:24,uuid:"E8DFC3D9-B467-4511-8819-CDB321DA6D73"}
 */
function updateUI(event){
	elements.fSumRejectedWorked.fgcolor = ((foundset&&foundset.getSelectedRecord()&&foundset.getSelectedRecord().hoursRejectedWork)/60) > 0?'Red':'DEFAULT';
	elements.fSumRejectedRetr.fgcolor = ((foundset&&foundset.getSelectedRecord()&&foundset.getSelectedRecord().hoursRejectedRetr)/60) > 0?'Red':'DEFAULT';
	elements.fSumRemained.fgcolor = ((foundset&&foundset.getSelectedRecord()&&foundset.getSelectedRecord().hoursRemained)/60) > 0?'#008040':'DEFAULT';
}