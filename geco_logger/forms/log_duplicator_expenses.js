/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @param {String} entity
 * @properties={typeid:24,uuid:"E04D256A-F3C7-4055-951D-58B3F65BAA29"}
 */
function clearAndClose(event, entity) {

	if (globals.isEmpty(year)) {
		globals.DIALOGS.showErrorDialog("Errore", "Il mese di consuntivazione non può essere vuoto", 'OK');
		return;
	}

	if (globals.isEmpty(month)) {
		globals.DIALOGS.showErrorDialog("Errore", "L'anno di consuntivazione non può essere vuoto", 'OK');
		return;
	}

	_super.clearAndClose(event, entity);
}
