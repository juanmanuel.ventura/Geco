/**
 * @type {Number}
 * @properties={typeid:35,uuid:"0B377320-D001-48CE-9994-6F5D041B36A6",variableType:8}
 */
var year = new Date().getFullYear();

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"5B5E031E-AABA-4705-8A55-6F2E8E1E5145",variableType:8}
 */
var month = new Date().getMonth() + 1;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"550FCE8C-33FB-48DB-8DDF-408BB0F2FC10",variableType:93}
 */
var startDate = new Date();

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"33431F1F-615F-41DC-B08B-B52E097A612E",variableType:93}
 */
var endDate = new Date();

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"4FD5F081-3CD0-4105-AF1B-7C02A9B165A1",variableType:8}
 */
var skipWeekend = 1;

/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"04FB4633-9A1E-4608-9F10-032DB4FE6EF0"}
 */
function updateUI(event) {
	controller.readOnly = false;
	elements.buttonCancel.enabled = true;
	elements.buttonSave.enabled = true;
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @param {String} entity
 * @properties={typeid:24,uuid:"20F715A1-BBCC-4B8A-B84D-816936B3E5FD"}
 */
function clearAndClose(event, entity) {

	if (globals.isEmpty(startDate)) {
		globals.DIALOGS.showErrorDialog("Errore", "La data di inizio non può essere vuota", 'OK');
		return;
	}

	if (globals.isEmpty(endDate)) {
		globals.DIALOGS.showErrorDialog("Errore", "La data di fine non può essere vuota", 'OK');
		return;
	}

	if (startDate.setHours(0, 0, 0) > endDate.setHours(0, 0, 0)) {
		globals.DIALOGS.showErrorDialog("Errore", "La data di inizio non può essere maggiore alla data di fine", 'OK');
		return;
	}

	// close the form
	controller.getWindow().destroy();

	// call the proper function
	if (entity == 'events_log') {
		// WEB
		forms.events_log_list_details.duplicateEventLog(startDate, endDate, skipWeekend);
	}

	if (entity == 'expenses_log') {
		// WEB
		forms.expenses_log_list_details.duplicateExpensesLog(startDate, endDate, skipWeekend, year, month);
	}

}

/**
 * @param {Date} oldValue old value
 * @param {Date} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"63C05ADE-CB30-4B9A-9296-25C15F29E899"}
 */
function onDataChange(oldValue, newValue, event) {
	endDate = newValue;
}
