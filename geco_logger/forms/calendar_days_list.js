/**
 * Create an even_log record from the selected date and the current user
 * It uses a relation
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"7E84502F-2E0C-4DBC-BE1C-540D9E6560DC"}
 */
function createEventLog(event) {
	if (startEditing(event)) {
		if (foundset && foundset.calendar_days_to_events_log) {
			foundset.calendar_days_to_events_log.newRecord(false);
		} else {
			globals.DIALOGS.showErrorDialog('Error', 'Per inserire un evento, creare prima il calendario per il giorno e l\'anno selezionato', 'OK');
			stopEditing(event);
		}
	}
}

/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"2A4CF471-18A2-44EC-A019-F2DB065FA22D"}
 */
function updateUI(event) {
	_super.updateUI(event);
	forms.timesheet_bundle.elements.split.enabled = !isEditing();
}

/**
 * Handle record selected.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"ABFD1401-A45C-4D3C-9463-AFE5937DF577"}
 */
function onRecordSelection(event) {

	// empty global variable if there is no related record
	if (foundset && foundset.calendar_days_to_events_log && foundset.calendar_days_to_events_log.getSize() == 0) {
		globals.selectedEventType = null;
	}

	// trigger updateUI on related form
	forms.events_log_list_details.updateUI(event);
	//TODO da eliminare - risolve il problema del JSRenderEvent
	var enableMonth = (foundset && foundset.calendar_days_to_calendar_months && foundset.calendar_days_to_calendar_months.is_enabled_for_timesheet == 1) ? true : false;
	forms.events_log_list_details.elements.defaultLabel.visible = !enableMonth;
	forms.events_log_list_details.elements.buttonAdd.enabled = enableMonth;

	_super.onRecordSelection(event);
}

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"CC02416F-70A1-4326-B32B-DC552F5E4222"}
 */
function onLoad(event) {
	var today = new Date().getDate();
	foundset.setSelectedIndex(today);
}

/**
 * Called before the form component is rendered.
 * @param {JSRenderEvent} event the render event
 * @private
 * @properties={typeid:24,uuid:"0B4BE7F0-242F-4884-BD82-202377181431"}
 */
function onRenderRecordSelector(event) {
	/** @type {JSRecord<db:/geco/calendar_days>} */
	var day = event.getRecord();

	if (day.calendar_days_to_calendar_months.is_enabled_for_timesheet == 0) {
		event.getRenderable().bgcolor = globals.COLORS.DISABLED;
		return
	}
	// sabato e domenica o patrone non selezionati
	if ( (day.is_working_day == 0 || day.is_holiday == 1 || globals.isCurrentUserHoliday(day.calendar_month, day.calendar_day)) && !event.isRecordSelected()) {
		event.getRenderable().bgcolor = globals.COLORS.NON_WORKABLE;
		return;
	}
}
