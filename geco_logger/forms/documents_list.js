/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"D8F85BD9-C247-48CE-93C1-EFC1D25FF902"}
 */
function downloadFile(event) {
	if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
		var jsFile = plugins.file.showFileSaveDialog(foundset.users_documents_to_documents.document_original_name);
		if (jsFile) plugins.file.writeFile(jsFile.getAbsolutePath(), foundset.users_documents_to_documents.document, "application/pdf")
	} else {
		plugins.file.writeFile(foundset.users_documents_to_documents.document_original_name, foundset.users_documents_to_documents.document, "application/pdf")
	}
}