dataSource:"db:/geco/calendar_days",
extendsID:"09E8A54F-0DD2-4CF5-951A-510F863E1E26",
items:[
{
dataProviderID:"globals.workingMonth",
displayType:2,
editable:false,
formIndex:4,
horizontalAlignment:2,
location:"81,15",
onActionMethodID:"-1",
onDataChangeMethodID:"85B7EB12-E240-4AB2-8379-F1DBAD4E31B6",
size:"115,20",
text:"User Id",
typeid:4,
uuid:"11A0B0BB-B657-46D2-A848-DE3EC2E703EB",
valuelistID:"3656A91E-9B4A-4E32-956B-C29D30CAFEBC"
},
{
dataProviderID:"scopes.globals.workingYear",
displayType:2,
editable:false,
formIndex:5,
format:"####",
horizontalAlignment:2,
location:"216,14",
onActionMethodID:"-1",
onDataChangeMethodID:"85B7EB12-E240-4AB2-8379-F1DBAD4E31B6",
size:"114,20",
text:"User Id",
typeid:4,
uuid:"9705E270-A267-4A58-B13F-F4316B3A2338",
valuelistID:"075E9257-460A-403C-B979-C6A3C3402907"
},
{
extendsID:"057636AF-CCE5-46A9-BD20-9CC3A7796EE6",
formIndex:3,
typeid:7,
uuid:"BE309B29-613F-43C6-A325-4A46955469E4"
},
{
formIndex:6,
location:"10,15",
size:"61,20",
text:"Periodo",
transparent:true,
typeid:7,
uuid:"CC8F8647-EA66-4C59-800B-81D12503C524"
}
],
name:"calendar_days_filter",
onLoadMethodID:"1493D865-235E-40BE-9CA5-528EACCC59E3",
styleName:"GeCo",
typeid:3,
uuid:"D42FD307-C03D-4D5D-A84F-B3DDF63647E9"