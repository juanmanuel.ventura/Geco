///**@type {String}
// * @properties={typeid:35,uuid:"CC8F3496-2D26-4915-88A2-F660853E34EF"}
// */
//var dynamicValuelist = null;
//
///**
// * @type {JSDataSet}
// * @properties={typeid:35,uuid:"61462385-F8EE-4A4D-8E1B-B3FCB4481012",variableType:-4}
// */
//var dynamicDataset = null;
//
///**@type {Array<String>}
// * @properties={typeid:35,uuid:"F123A785-B4B2-486B-873E-92C0DD609736",variableType:-4}
// */
//var solutionsList = [];

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"5F238AEA-A67F-446D-9B00-873D8D6E4FBA"}
 * @AllowToRunInFind
 */
function initialize(event) {
	if (!globals.grantAccess(['Users', 'Employees', 'Consultants', 'Intern', 'Contratto a progetto'])) {
		logOut(event);
	}
	_super.initialize(event);
	databaseManager.addTableFilterParam('geco', 'event_types', 'event_type_id', 'not in', [6], 'holidayEventsTypeFilter');
	var now = new Date();
	globals.workingYear = now.getFullYear();
	globals.workingMonth = now.getMonth() + 1; // in JavaScript it is a 0 based array
	globals.loadRunnableRules();
	//se  è Consultant non vede il tab nota spese
	if (globals.hasRole('Consultants') && !globals.hasRole('Consultants Expenses')) {
		//if (globals.isConsultant() && !globals.isConsultantExpenses()) {
		elements.button2.visible = false;
		elements.button2.enabled = false;
		// move the documents tab to take button2's place
		elements.button3.setLocation(elements.button2.getLocationX(), elements.button2.getLocationY());
	}
	//	/** @type {JSFoundSet<db:/geco/users>} */
	//	var users = databaseManager.getFoundSet('geco', 'users');
	//	if (users.find()) {
	//		users.user_id = scopes.globals.currentUserId;
	//		if (users.search() > 0) {
	//			var rec = users.getRecord(1);
	//			if (rec.has_medical_practice == 1) solutionsListMain.push('Pratiche Sanitarie');
	//		}
	//	}

	//	dynamicDataset = databaseManager.createEmptyDataSet();
	//	if (globals.hasRole('Approvers')) solutionsList.push('Approver');
	//	if (globals.hasRole('Controllers')) solutionsList.push('Controller');
	//	if (globals.hasRole('HR Administrators') || globals.hasRole('HR Expenses') || globals.hasRole('Admin Readers')) solutionsList.push('Personnel');
	//	if (globals.hasRole('Delivery Admin') || globals.hasRole('Responsabili Commessa') || globals.hasRole('Controllers') || globals.hasRole('Resp. Mercato') || globals.hasRole('Orders Admin') || globals.hasRole('Planner') || globals.hasRole('Supp. Commerciale')) solutionsList.push('Management');
	//	if (globals.currentUserDisplayName == 'STAFFA JONATHAN' || globals.currentUserDisplayName == 'FRAU DANIELA') solutionsList.push('Configurator');
	////	solutionsList.push('Logger');
	//	application.output('solutionsList.length: ' + solutionsList.length + '; solutionsList: ' + solutionsList);
	//	if (solutionsList.length > 0) {
	//		solutionsList.sort();
	//		for (var index = 0; index < solutionsList.length; index++) {
	//			dynamicDataset.addRow(new Array(solutionsList[index]));
	//		}
	//		elements.dynamicSolutionsChoice.setValueListItems(dynamicDataset);
	//		elements.lSolutionName.enabled = true;
	//		elements.lSolutionName.visible = true;
	//		elements.dynamicSolutionsChoice.enabled = true;
	//		elements.dynamicSolutionsChoice.visible = true;
	//	}
	loadPanel(event, 'timesheet_bundle');
	_super.checkPwdExpiration();
}

//function setSelector(event) {
//	if (event.getType() == JSEvent.ACTION) {
//		// move the selector
//		/** @type {RuntimeLabel} */
//		var label = event.getSource();
//		if(label.getName()=='openLogger') {
//		elements.selector.setLocation(elements.button1.getLocationX(), elements.button1.getLocationY());
//		elements.selector.setSize(elements.button1.getWidth(), elements.button1.getHeight());
//		}
//		else{
//			elements.selector.setLocation(label.getLocationX(), label.getLocationY());
//			elements.selector.setSize(label.getWidth(), label.getHeight());
//		}
//	}
//}

/**
 * Callback method for when form is shown.
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"D4E5333D-7280-4A84-8CF0-CB7A1588052F"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
	//	dynamicValuelist = null;
}
