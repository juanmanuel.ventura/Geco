/**
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"AB2E480E-85A9-498A-ABEE-3180734F50D4"}
 */
function onLoad(event) {
	foundset.setSelectedIndex(new Date().getMonth() + 1);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"C65C4898-C48C-497E-9676-21A6877BFE01"}
 */
function onRecordSelection(event) {
	
	// triggerupdateUI on related form
	forms.expenses_log_list_details.updateUI(event);
	globals.selectedYear = foundset.getSelectedRecord().month_year;
	globals.selectedMonth = foundset.getSelectedRecord().month_number;
	
	//TODO da eliminare - risolve il problema del JSRenderEvent 
	var enableMonth = (foundset.is_enabled_for_expenses == 1) ? true:false;
	forms.expenses_log_list_details.elements.defaultLabel.visible = !enableMonth;
	forms.expenses_log_list_details.elements.buttonAdd.enabled = enableMonth;	
	_super.onRecordSelection(event);
}

/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"697AAC1D-89F4-420D-85EC-1F88BFF5FE74"}
 */
function updateUI(event) {
	_super.updateUI(event);
	forms.expenses_bundle.elements.split.enabled = !isEditing();
	//controller.enabled = !isEditing();
}

///**
// * @param {JSRenderEvent} event the render event
// * @private
// *
// * @properties={typeid:24,uuid:"369F3A21-9463-49CD-A325-D5E8D897D7D6"}
// */
//function onRenderRecordSelector(event) {
//	
//	/** @type {JSRecord<db:/geco/calendar_months>} */
//	var month = event.getRecord();
//
//	if (month.is_enabled_for_expenses == 0) {
//		if (event.getRenderable()) {
//			event.getRenderable().bgcolor = '#fdfcdc';
//			if (event.isRecordSelected()) {
//				event.getRenderable().bgcolor = '#e4e9ed';
//			}
//			return
//		}
//	}
//	//_super.onRenderRecordSelector(event);
//}


