/**
 * @type {Number}
 * @properties={typeid:35,uuid:"FB7D89A3-AAFF-415B-8331-7177C49E3E73",variableType:8}
 */
var progress = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"E13F7272-3872-4DA7-85D1-9B79331DEB85",variableType:8}
 */
var selectedYear = new Date().getFullYear();

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"C9E6FE18-E4D1-42BD-BB29-5B75E2D847EE",variableType:93}
 */
var result = null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"58111A8B-027A-45CE-AB5D-18AA284E2BDA"}
 */
function onDataChange(oldValue, newValue, event) {
	result = getDateFromDayNumber(newValue, globals.selectedYear);
}

/**
 * @param {Number} number
 * @param {Number} year
 * @return {Date}
 * Set the date from the number of days of the given year
 * @properties={typeid:24,uuid:"4B6A26B3-918F-403A-AC6B-07211C3E403A"}
 */
function getDateFromDayNumber(number, year) {

	//se entrambi i campi sono valorizzati
	if (number != null && year != null) {
		var millisecondsForYear = new Date(year, 0, 0).valueOf();
		var millisecondsForDay = 60 * 60 * 24 * 1000 * number;
		var date = new Date(millisecondsForYear + millisecondsForDay);

		return new Date(date.setHours(0, 0, 0, 0));
	}
	return null;
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"47D5A3E9-6AC0-4EAF-A21C-7E01111737F8"}
 */
function ok(event) {
	globals.callerForm.foundset['expense_date'] = result;
	//plugins.window.closeFormPopup(result);
	controller.getWindow().destroy();
	//application.closeAllWindows();
}
