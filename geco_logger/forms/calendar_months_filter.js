/**
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"36ED3770-5914-4951-9B28-BB37E7BBFA04"}
 * @AllowToRunInFind
 */
function filterByYear() {

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		preloadInCache();
	}

	if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {

	var params = {
		processFunction: preloadInCache,
		message: 'Carico dati del periodo, attendere...',
		opacity: 0.75,
		paneColor: '#000000',
		textColor: '#FFFFFF',
		showCancelButton: false,
		fontType: 'Calibri,0,48'
	};

		plugins.busy.block(params);
	}

	return true;
}

/**
 * @properties={typeid:24,uuid:"26BDC445-F8B5-488C-AFD1-CD6EA9C5743D"}
 * @private
 * @AllowToRunInFind
 */
function preloadInCache() {
	// dismiss any remaining busy dialog
	application.updateUI();

	try {

		if (foundset.find()) {
			foundset.month_year = globals.workingYear;
			foundset.search();
		}

		for (var index = 1; index <= foundset.getSize(); index++) {
			foundset.getRecord(index).calendar_months_to_expenses_log.loadAllRecords();
		}
	} catch (e) {
		application.output(e);
	} finally {
		if (plugins.busy) plugins.busy.unblock();
	}
}

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"9AEF8747-B9C6-475C-A81C-C36EE6B24F9C"}
 */
function setWorkingPeriod(event) {
	filterByYear();
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"78912080-1830-4383-A84A-7D8EB1A85229"}
 */
function onDataChange(oldValue, newValue, event) {
	filterByYear();
}

