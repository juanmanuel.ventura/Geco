/**
 * @properties={typeid:24,uuid:"1493D865-235E-40BE-9CA5-528EACCC59E3"}
 * @AllowToRunInFind
 */
function filterByMonth() {

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		preloadInCache();
	}

	if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
		var params = {
			processFunction: preloadInCache,
			message: 'Carico dati del periodo, attendere...',
			opacity: 0.75,
			paneColor: '#000000',
			textColor: '#FFFFFF',
			showCancelButton: false,
			fontType: 'Calibri,0,48'
		};

		plugins.busy.block(params);
	}

	application.updateUI();
	
}

/**
 * @properties={typeid:24,uuid:"5852BF84-DCF6-4E46-A19C-5A8678DBCD32"}
 * @private
 * @AllowToRunInFind
 */
function preloadInCache() {
	// dismiss any remaining busy dialog
	application.updateUI();

	try {
		if (foundset.find()) {
			foundset.calendar_month = globals.workingMonth;
			foundset.calendar_year = globals.workingYear;
			foundset.search();

			for (var index = 1; index <= foundset.getSize(); index++) {
				foundset.getRecord(index).calendar_days_to_events_log.loadAllRecords();
			}
		
		}
	} catch (e) {
		application.output(e);
	} finally {
		if (plugins.busy) plugins.busy.unblock();
	}
}

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"FC6843BB-456A-4AC0-B412-733DE9F002DA"}
 */
function setWorkingPeriod(event) {
	globals.workingMonth = new Date().getMonth() + 1;
	globals.workingYear = new Date().getFullYear();
	filterByMonth();
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"85B7EB12-E240-4AB2-8379-F1DBAD4E31B6"}
 */
function onDataChange(oldValue, newValue, event) {
	filterByMonth();
}
