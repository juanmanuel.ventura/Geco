/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"DAB63D9E-4ABA-419B-9CD6-411C59EC89DA",variableType:-4}
 */
var dt = null;

/**
 * @type {Number} serve per sapere qual è l'indice del record selezionato nel momento in cui entro in editing
 * @properties={typeid:35,uuid:"F90E1149-1A7C-4526-A8F7-76CEEC891642",variableType:-4}
 */
var recIndexOnStartEdit = null;

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"AEA9DAF8-7E46-4CC4-A8D0-2E97DE57B8A2"}
 */
function onDataChangeEventType(oldValue, newValue, event) {
	if (oldValue != newValue) {
		elements.event.requestFocus();
	}
	return true
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @properties={typeid:24,uuid:"764C0DDA-1620-432E-9BCA-45A34CFD70C7"}
 */
function onDataChangeEvent(event) {
	// enable job order selector
	if (foundset.events_log_to_events.has_job_order == 1) {
		elements.jobOrder.enabled = true;
		elements.jobOrder.requestFocus();
	} else {
		elements.jobOrder.enabled = false;
		foundset.job_order_id = null;
	}

	// enable time slots selectors
	if (foundset.events_log_to_events.unit_measure == 2) {
		elements.stopSlot.enabled = true;
		elements.startSlot.enabled = true;
	} else {
		elements.stopSlot.enabled = false;
		elements.startSlot.enabled = false;
		foundset.time_start = null;
		foundset.time_stop = null;
	}

	return true
}

/**
 * Handle changed data.
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"84E5516F-74D3-4932-B6FD-C5D9A7ADB9F4"}
 */
function onDataChangeJobOrder(oldValue, newValue, event) {
	if (events_log_to_events.unit_measure == 2) {
		elements.startSlot.requestFocus();
	} else {
		time_start = null;
		time_stop = null;
	}
	return true
}

/**
 * Handle record selected.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"D6BA6751-C132-4BB6-A0A8-28CBBB9B4750"}
 */
function onRecordSelection(event) {
	//	if (isEditing()) {
	//	globals.DIALOGS.showInfoDialog("Attenzione","Stai editando un record!");
	//		_super.stopEditing(event);
	//		//return;
	//	}

	if (foundset.events_log_to_events) {
		globals.selectedEventType = foundset.events_log_to_events.event_type_id;
	}

	globals.selectedStartTime = foundset.time_start;
	_super.onRecordSelection(event);

	//verifico che si possa cambiare record selezionato quando si è in editing, bloccando così la modifica
	//per gli eventi che verrebbero bypassati
	if (recIndexOnStartEdit != null) {
		if (recIndexOnStartEdit != foundset.getSelectedIndex()) {
			var recToSelect = foundset.getSelectedRecord(); //record nuovo che ha cliccato
			if (recToSelect.event_id == 27 || recToSelect.event_id == 28) {
				globals.DIALOGS.showErrorDialog('Error', 'Non è possibile modificare gli straordinari.\nCancellare e inserire nuovamente.', 'OK');
				foundset.setSelectedIndex(recIndexOnStartEdit);
				return;
			} else if (recToSelect.event_id == 25) {
				globals.DIALOGS.showErrorDialog('Error', 'Non è possibile modificare l\'evento riposo compensativo straordinario.\nCancellare e inserire nuovamente.', 'OK');
				foundset.setSelectedIndex(recIndexOnStartEdit);
				return;
			} else {
				recIndexOnStartEdit = foundset.getSelectedIndex();
			}
		}
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"A21335EB-977B-44CE-A6E0-B6E018BFAAE1"}
 */
function showNote(event) {
	globals.DIALOGS.showInfoDialog("Nota dall' approvatore", foundset.note, 'OK');
}

/**
 * Handle changed data.
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @properties={typeid:24,uuid:"23CBFE11-CB57-4413-8857-5D6D96A98F26"}
 */
function onDataChangeStartTime(oldValue, newValue, event) {
	if (oldValue != newValue) {
		globals.selectedStartTime = foundset.time_start;
		elements.stopSlot.requestFocus();
		return true;
	}
	return false;
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"E2825BC2-E43C-4669-A966-ACC76FD03069"}
 * @AllowToRunInFind
 */
function newRecord(event) {
	forms.calendar_days_list.createEventLog(event);
	globals.selectedEventType = null;
	elements.eventType.requestFocus();
	//PRESET SU LAVORO SU COMMESSA E LAVORO ORDINARIO PER GG LAVORATIVO FERIALE
	if (foundset && foundset.event_log_date && foundset.event_log_date.getDay() != 0 && foundset.event_log_date.getDay() != 6 && foundset.events_log_to_calendar_days.is_working_day != 0 && foundset.events_log_to_calendar_days.is_holiday != 1 && !globals.isCurrentUserHoliday(foundset.events_log_to_calendar_days.calendar_month, foundset.events_log_to_calendar_days.calendar_day)) {
		if (currentownerid_to_user_events && currentownerid_to_user_events.users_events_to_events && currentownerid_to_user_events.users_events_to_events.events_to_event_types) {
			if (currentuserid_to_users_events && currentuserid_to_users_events.selected_event_type_to_allowed_events) {
				if (currentuserid_to_users_job_orders && currentuserid_to_users_job_orders.users_job_orders_to_enabled_job_orders) {
					if (foundset.getSize() == 1) {
						globals.selectedEventType = 0;
						foundset.event_id = 0;
						elements.jobOrder.enabled = true;
						//SETTAGGIO ORA INIZIO = 9 E ORA FINE = 18 (COME ATTUALMENTE IN PRO -03/11/2014-)
						foundset.time_start = 540;
						globals.selectedStartTime = foundset.time_start;
						foundset.time_stop = 1080;
					} else {
						//autoinserimento ora di inizio nuovo record all'ora di fine del record più recente
						var record = foundset.getSelectedRecord();
						/** @type {JSFoundSet<db:/geco/events_log>} */
						var events_log = databaseManager.getFoundSet('geco', 'events_log');
						if (events_log.find()) {
							events_log.event_log_id = '!' + record.event_log_id;
							events_log.user_id = record.user_id;
							events_log.event_log_date = record.event_log_date;
							var tot = events_log.search();
							
							if (tot > 0) { 
								events_log.sort('time_stop desc');
								foundset.time_start = events_log.getRecord(1).time_stop;
								globals.selectedStartTime = foundset.time_start;
							}
						}
					}
				}
			}
		}
	}
	//mi salvo l'indice del record in quel momento appena entrato in editing
	recIndexOnStartEdit = foundset.getSelectedIndex();
}

/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"125C7FE1-68E3-4C65-B1A7-DABF5A32A839"}
 */
function updateUI(event) {

	// call parent first
	_super.updateUI(event);

	var record = null;
	var status = false;
	var hasJobOrder = false;
	var notePresent = false;

	// only if there is a record
	if (foundset.getSelectedRecord()) {
		// get current record
		record = (foundset.getSelectedRecord()) ? foundset.getSelectedRecord() : null;

		// disallow editing and deleting if status = approved || closed
		status = (record.event_log_status == 2 || record.event_log_status == 4) ? false : true;

		// show note icon only if present
		notePresent = !globals.isEmpty(record.note);
	}

	if (foundset.events_log_to_events) hasJobOrder = (foundset.events_log_to_events.has_job_order == 0);

	// fields
	elements.eventType.enabled = isEditing() && status;
	elements.event.enabled = isEditing() && status;
	//elements.jobOrder.enabled = (isEditing() && status && hasJobOrder);
	elements.jobOrder.enabled = (isEditing() && status && hasJobOrder) || (globals.allowedJobOrderSelection);
	elements.startSlot.enabled = isEditing() && status;
	elements.stopSlot.enabled = isEditing() && status;

	// show note when present
	elements.buttonNote.visible = notePresent;

	// buttons
	elements.buttonCopy.visible = !isEditing() && (record) ? true : false;
	elements.buttonEdit.visible = !isEditing() && status;
	elements.buttonDelete.visible = !isEditing() && (status) ? status : false;
	elements.historyHours.visible = !isEditing() && (scopes.globals.isUserAllowedToEvent(25) || scopes.globals.isUserAllowedToEvent(28));

	// sort
	if (!isEditing()) foundset.sort('time_start asc');

	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		bundle.elements['split'].visible = !isEditing();
		bundle['focus'] = foundset.event_log_date;
		bundle.elements['focus'].visible = isEditing();
	}
	//DF Workaround for get the calendar_days_list form disabled
	//	forms.timesheet_bundle.elements.split.visible = !isEditing();
	//	forms.timesheet_bundle.focus_date = foundset.event_log_date;
	//	forms.timesheet_bundle.elements.focus_date.visible = isEditing();
	
}

/**
 * TODO metodo temporaneo, da eliminare quando verrà implementata la logica di modifica degli straordinari
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"2FBD381F-3EBF-4645-BBEF-7DE0F8B1FCA7"}
 */
function tempOnAction(event) {
	if (foundset.getSelectedRecord() && (foundset.getSelectedRecord().event_id == 27 || foundset.getSelectedRecord().event_id == 28)) {
		globals.DIALOGS.showErrorDialog('Error', 'Non è possibile modificare gli straordinari.\nCancellare e inserire nuovamente.', 'OK');
	} else if (foundset.getSelectedRecord() && foundset.getSelectedRecord().event_id == 25) {
		globals.DIALOGS.showErrorDialog('Error', 'Non è possibile modificare l\'evento riposo compensativo straordinario.\nCancellare e inserire nuovamente.', 'OK');
	} else {
		startEditing(event);
		onDataChangeEvent(event);
		//mi salvo l'indice del record in quel momento appena entrato in editing
		recIndexOnStartEdit = foundset.getSelectedIndex();
	}
}

/**
 * Opens a pop up to select date
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"594F6726-8355-4156-8246-24DC78B9C3D9"}
 */
function openPopupCopy(event) {
	var win = application.createWindow("copia", JSWindow.MODAL_DIALOG);
	win.title = "Copia evento";
	win.show(forms.log_duplicator);
}

/**
 * @param {Date} start
 * @param {Date} end
 * @param {Number} skip
 * @properties={typeid:24,uuid:"12B80378-5703-4347-AEAA-BF5CD4FBEAA1"}
 */
function callDuplicateEventLog(start, end, skip) {
	application.updateUI();
	try {
		duplicateEventLog(start, end, skip);
	} catch (e) {
		globals.DIALOGS.showErrorDialog("Error", e.message, 'OK');
		throw e
	} finally {
		if (plugins.busy) plugins.busy.unblock();
	}
}

/**
 * @param {Date} start
 * @param {Date} end
 * @param {Number} skip
 * @properties={typeid:24,uuid:"98224509-4211-42A1-AD0D-6C46F5D5B630"}
 * @AllowToRunInFind
 */

function duplicateEventLog(start, end, skip) {

	// get the original record
	var record = foundset.getSelectedRecord();

	// skip if no record is selected
	if (!record) {
		application.output("No record selected");
		return;
	}

	// sanitize dates
	start.setHours(0, 0, 0);
	end.setHours(0, 0, 0);
	//var indexToDuplicate = foundset.getRecordIndex(record);
	var indexToDuplicate = forms.calendar_days_list.foundset.calendar_days_to_events_log.getRecordIndex(record);
	/** @type {JSRecord<db:/geco/events_log>} */
	//var originalRecord = foundset.getRecord(indexToDuplicate);
	var originalRecord = forms.calendar_days_list.foundset.calendar_days_to_events_log.getRecord(indexToDuplicate);

	for (var date = start; start <= end; date.setDate(date.getDate() + 1)) {

		// skip weekends
		if (skip == 1 && globals.isWeekEnd(date)) {
			continue;
		}
		//prendo il record originario
		//indexToDuplicate = foundset.getRecordIndex(originalRecord);
		indexToDuplicate = forms.calendar_days_list.foundset.calendar_days_to_events_log.getRecordIndex(originalRecord);
		databaseManager.setAutoSave(false);
		//indexToDuplicate = foundset.duplicateRecord(indexToDuplicate, true, true);
		indexToDuplicate = forms.calendar_days_list.foundset.calendar_days_to_events_log.duplicateRecord(indexToDuplicate, true, true);
		//var newrecord = foundset.getRecord(indexToDuplicate);
		var newrecord = forms.calendar_days_list.foundset.calendar_days_to_events_log.getRecord(indexToDuplicate);
		newrecord.event_log_date = date;
		newrecord.event_log_status = 1;
		newrecord.actual_approver_id = null;
		if (!databaseManager.setAutoSave(true)) {
			application.output(databaseManager.getFailedRecords(foundset));
			databaseManager.revertEditedRecords();
			databaseManager.setAutoSave(true);
			if (globals.validationExceptionMessage) {
				globals.DIALOGS.showErrorDialog('Error', globals.validationExceptionMessage, 'OK');
			}
		}
	}

	// force loading the new records
	updateUI(new JSEvent);
	forms.calendar_days_list.updateUI(new JSEvent);
	application.updateUI();
	forms.calendar_days_filter.filterByMonth();
}

/**
 * Stampa del timesheet utilizzando la SP
 * @param event
 *
 * @properties={typeid:24,uuid:"4B43AFFD-5BFF-471F-86EB-AABC378F26D4"}
 * @AllowToRunInFind
 */
function printTimesheet2(event) {
	var formName = 'TS_' + globals.workingYear + globals.workingMonth;
	var _query_download = 'call sp_print_user_ts (?, ? , ?)';

	var arguments = new Array();
	arguments[0] = security.getUserUID();
	arguments[1] = globals.workingYear;
	arguments[2] = globals.workingMonth;

	//var _query_download = 'SELECT interface_session_nr FROM interface_session'
	var _ds_download = databaseManager.getDataSetByQuery('geco', _query_download, arguments, 1000);
	//variabili per i totali
	var totalORD = 0;
	var totalSSS = 0;
	var totalHVI = 0;
	var totalRFES = 0;
	var totalRORD = 0;
	for (var index = 1; index <= _ds_download.getMaxRowIndex(); index++) {
		var riga = _ds_download.getRowAsArray(index);
		totalORD = totalORD + riga[7];
		totalSSS = totalSSS + riga[8];
		totalHVI = totalHVI + riga[9];
		totalRFES = totalRFES + riga[10];
		totalRORD = totalRORD + riga[11];
	}
	//application.output('ORD ' + totalORD + ' SSS '+ totalSSS + ' HVI ' + totalHVI +' RFES ' + totalRFES +' RORD ' + totalRORD);

	var _data_source = _ds_download.createDataSource('ts', [JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT]);
	var success = history.removeForm(formName);
	if (success) {
		solutionModel.removeForm(formName);
	}
	success = solutionModel.removeForm(formName);
	//application.output('solutionModel ' + success);
	if (forms[formName]) {

		application.output('esiste ancora');
		success = solutionModel.removeForm(formName);
		application.closeAllWindows();
	}
	var _form_download = solutionModel.newForm(formName, _data_source, null, 'GeCo', false, 852, 500);
	//var superForm = solutionModel.getForm('print_user_events_log');
	_form_download.extendsForm = 'print_user_events_log_copy';
	_form_download.dataSource = _data_source;
	//var _field_year = _form_download.newField('timesheet_year', JSField.TEXT_FIELD, 661, 83, 71, 20);
	//var _field_month = _form_download.newField('timesheet_month', JSField.TEXT_FIELD, 584, 83, 77, 20);
	var _field_day = _form_download.newField('timesheet_day', JSField.TEXT_FIELD, 22, 180, 54, 20);
	var _field_code = _form_download.newField('external_code_navision', JSField.TEXT_FIELD, 76, 180, 54, 20);
	var _field_title = _form_download.newField('job_order_title', JSField.TEXT_FIELD, 146, 180, 367, 20);
	var _field_ord = _form_download.newField('ord', JSField.TEXT_FIELD, 513, 180, 56, 20);
	var _field_sss = _form_download.newField('sss', JSField.TEXT_FIELD, 569, 180, 56, 20);
	var _field_hvi = _form_download.newField('hvi', JSField.TEXT_FIELD, 625, 180, 56, 20);
	var _field_rfes = _form_download.newField('rfes', JSField.TEXT_FIELD, 681, 180, 57, 20);
	var _field_rord = _form_download.newField('rodr', JSField.TEXT_FIELD, 737, 180, 58, 20);

	//	   _form_download.view = JSForm.LOCKED_LIST_VIEW
	//	   _form_download.navigator = SM_DEFAULTS.NONE
	//	   _form_download.scrollbars = SM_SCROLLBAR.HORIZONTAL_SCROLLBAR_NEVER | SM_SCROLLBAR.VERTICAL_SCROLLBAR_AS_NEEDED

	//	_field_year.editable = false;
	//	_field_year.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	//	_field_year.dataProviderID = 'timesheet_year';
	//	_field_year.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;

	//	_field_month.editable = false;
	//	_field_month.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	//	_field_month.dataProviderID = 'timesheet_month';
	//	_field_month.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;

	_field_day.editable = false;
	_field_day.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_day.styleClass = 'tiny';
	_field_day.dataProviderID = 'timesheet_day';
	_field_day.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;

	_field_code.editable = false;
	_field_code.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_code.styleClass = 'tiny';
	_field_code.dataProviderID = 'external_code_navision';
	_field_code.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;

	_field_title.editable = false;
	_field_title.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_title.styleClass = 'tiny';
	_field_title.dataProviderID = 'job_order_title';
	_field_title.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;

	_field_ord.editable = false;
	_field_ord.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_ord.styleClass = 'tiny';
	_field_ord.dataProviderID = 'ord';
	_field_ord.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	_field_ord.margin = '0,5,0,5';
	_field_ord.horizontalAlignment = SM_ALIGNMENT.RIGHT;

	_field_sss.editable = false;
	_field_sss.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_sss.styleClass = 'tiny';
	_field_sss.dataProviderID = 'sss';
	_field_sss.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	_field_sss.margin = '0,5,0,5';
	_field_sss.horizontalAlignment = SM_ALIGNMENT.RIGHT;

	_field_hvi.editable = false;
	_field_hvi.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_hvi.styleClass = 'tiny';
	_field_hvi.dataProviderID = 'hvi';
	_field_hvi.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	_field_hvi.margin = '0,5,0,5';
	_field_hvi.horizontalAlignment = SM_ALIGNMENT.RIGHT;

	_field_rfes.editable = false;
	_field_rfes.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_rfes.styleClass = 'tiny';
	_field_rfes.dataProviderID = 'rfes';
	_field_rfes.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	_field_rfes.margin = '0,5,0,5';
	_field_rfes.horizontalAlignment = SM_ALIGNMENT.RIGHT;

	_field_rord.editable = false;
	_field_rord.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_rord.styleClass = 'tiny';
	_field_rord.dataProviderID = 'rord';
	_field_rord.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	_field_rord.margin = '0,5,0,5';
	_field_rord.horizontalAlignment = SM_ALIGNMENT.RIGHT;

	forms[formName]['totalORD'] = totalORD;
	forms[formName]['totalSSS'] = totalSSS;
	forms[formName]['totalHVI'] = totalHVI;
	forms[formName]['totalRFES'] = totalRFES;
	forms[formName]['totalRORD'] = totalRORD;
	forms[formName]['monthStr'] = working_month_calendar_months.month_name;

	var win = application.createWindow('print', JSWindow.WINDOW);
	win.setInitialBounds(200, 100, 810, 500)
	win.undecorated = true;
	win.show(_form_download);
}

/**
 * Stampa del timesheet utilizzando la SP
 * @param event
 *
 * @properties={typeid:24,uuid:"64DB36B1-A1FD-4F9C-9BA0-82B824F734D5"}
 * @AllowToRunInFind
 */
function printTimesheetExcell(event) {
	var formName = 'TS_' + globals.workingYear + globals.workingMonth;
	var _query_download = 'call sp_print_user_timesheet (?, ? , ?)';

	var arguments = new Array();
	arguments[0] = security.getUserUID();
	arguments[1] = globals.workingYear;
	arguments[2] = globals.workingMonth;

	//var _query_download = 'SELECT interface_session_nr FROM interface_session'
	var _ds_download = databaseManager.getDataSetByQuery('geco', _query_download, arguments, 1000);
	//variabili per i totali
	var totalORD = 0;
	var totalSSS = 0;
	var totalHVI = 0;
	var totalRFES = 0;
	var totalRORD = 0;
	for (var index = 1; index <= _ds_download.getMaxRowIndex(); index++) {
		var riga = _ds_download.getRowAsArray(index);
		totalORD = totalORD + riga[7];
		totalSSS = totalSSS + riga[8];
		totalHVI = totalHVI + riga[9];
		totalRFES = totalRFES + riga[10];
		totalRORD = totalRORD + riga[11];
	}
	//application.output('ORD ' + totalORD + ' SSS '+ totalSSS + ' HVI ' + totalHVI +' RFES ' + totalRFES +' RORD ' + totalRORD);

	var _data_source = _ds_download.createDataSource('ts', [JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT, JSColumn.TEXT]);
	var success = history.removeForm(formName);
	if (success) {
		solutionModel.removeForm(formName);
	}
	success = solutionModel.removeForm(formName);
	//application.output('solutionModel ' + success);
	if (forms[formName]) {

		application.output('esiste ancora');
		success = solutionModel.removeForm(formName);
		application.closeAllWindows();
	}
	var _form_download = solutionModel.newForm(formName, _data_source, null, 'GeCo', false, 852, 500);
	//var superForm = solutionModel.getForm('print_user_events_log');
	_form_download.extendsForm = 'print_user_events_log_copy';
	_form_download.dataSource = _data_source;
	//var _field_year = _form_download.newField('timesheet_year', JSField.TEXT_FIELD, 661, 83, 71, 20);
	//var _field_month = _form_download.newField('timesheet_month', JSField.TEXT_FIELD, 584, 83, 77, 20);
	var _field_day = _form_download.newField('timesheet_day', JSField.TEXT_FIELD, 22, 180, 54, 20);
	var _field_code = _form_download.newField('external_code_navision', JSField.TEXT_FIELD, 76, 180, 54, 20);
	var _field_title = _form_download.newField('job_order_title', JSField.TEXT_FIELD, 146, 180, 367, 20);
	var _field_ord = _form_download.newField('ord', JSField.TEXT_FIELD, 513, 180, 56, 20);
	var _field_sss = _form_download.newField('sss', JSField.TEXT_FIELD, 569, 180, 56, 20);
	var _field_hvi = _form_download.newField('hvi', JSField.TEXT_FIELD, 625, 180, 56, 20);
	var _field_rfes = _form_download.newField('rfes', JSField.TEXT_FIELD, 681, 180, 57, 20);
	var _field_rord = _form_download.newField('rodr', JSField.TEXT_FIELD, 737, 180, 58, 20);

	//	   _form_download.view = JSForm.LOCKED_LIST_VIEW
	//	   _form_download.navigator = SM_DEFAULTS.NONE
	//	   _form_download.scrollbars = SM_SCROLLBAR.HORIZONTAL_SCROLLBAR_NEVER | SM_SCROLLBAR.VERTICAL_SCROLLBAR_AS_NEEDED

	//	_field_year.editable = false;
	//	_field_year.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	//	_field_year.dataProviderID = 'timesheet_year';
	//	_field_year.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;

	//	_field_month.editable = false;
	//	_field_month.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	//	_field_month.dataProviderID = 'timesheet_month';
	//	_field_month.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;

	_field_day.editable = false;
	_field_day.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_day.styleClass = 'tiny';
	_field_day.dataProviderID = 'timesheet_day';
	_field_day.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;

	_field_code.editable = false;
	_field_code.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_code.styleClass = 'tiny';
	_field_code.dataProviderID = 'external_code_navision';
	_field_code.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;

	_field_title.editable = false;
	_field_title.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_title.styleClass = 'tiny';
	_field_title.dataProviderID = 'job_order_title';
	_field_title.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;

	_field_ord.editable = false;
	_field_ord.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_ord.styleClass = 'tiny';
	_field_ord.dataProviderID = 'ord';
	_field_ord.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	_field_ord.margin = '0,5,0,5';
	_field_ord.horizontalAlignment = SM_ALIGNMENT.RIGHT;

	_field_sss.editable = false;
	_field_sss.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_sss.styleClass = 'tiny';
	_field_sss.dataProviderID = 'sss';
	_field_sss.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	_field_sss.margin = '0,5,0,5';
	_field_sss.horizontalAlignment = SM_ALIGNMENT.RIGHT;

	_field_hvi.editable = false;
	_field_hvi.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_hvi.styleClass = 'tiny';
	_field_hvi.dataProviderID = 'hvi';
	_field_hvi.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	_field_hvi.margin = '0,5,0,5';
	_field_hvi.horizontalAlignment = SM_ALIGNMENT.RIGHT;

	_field_rfes.editable = false;
	_field_rfes.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_rfes.styleClass = 'tiny';
	_field_rfes.dataProviderID = 'rfes';
	_field_rfes.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	_field_rfes.margin = '0,5,0,5';
	_field_rfes.horizontalAlignment = SM_ALIGNMENT.RIGHT;

	_field_rord.editable = false;
	_field_rord.borderType = solutionModel.createEmptyBorder(0, 0, 0, 0);
	_field_rord.styleClass = 'tiny';
	_field_rord.dataProviderID = 'rord';
	_field_rord.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH;
	_field_rord.margin = '0,5,0,5';
	_field_rord.horizontalAlignment = SM_ALIGNMENT.RIGHT;

	forms[formName]['totalORD'] = totalORD;
	forms[formName]['totalSSS'] = totalSSS;
	forms[formName]['totalHVI'] = totalHVI;
	forms[formName]['totalRFES'] = totalRFES;
	forms[formName]['totalRORD'] = totalRORD;
	forms[formName]['monthStr'] = working_month_calendar_months.month_name;

	var win = application.createWindow('print', JSWindow.WINDOW);
	win.setInitialBounds(200, 100, 810, 500)
	win.undecorated = true;
	win.show(_form_download);
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"9CE41CD5-2B64-4713-A459-7BF5A1AE98CC"}
 */
function saveEdits(event) {
	var record = foundset.getSelectedRecord();
	var objUserTime = globals.getUserTimeWorkByDay(globals.currentUserId, record.event_log_date);
	var breakStartDisplay = globals.formatMinutes(objUserTime.breakStart);
	var breakStopDisplay = globals.formatMinutes(objUserTime.breakStop);
	if ( (record.event_id == 25 || record.event_id == 27 || record.event_id == 28 || record.event_id == 38) && ( (record.time_start <= objUserTime.breakStart && record.time_stop >= objUserTime.breakStop) || (record.time_start >= objUserTime.breakStart && record.time_start < objUserTime.breakStop) || (record.time_stop <= objUserTime.breakStop && record.time_stop > objUserTime.breakStart))) {
		var answer = globals.DIALOGS.showWarningDialog("Conferma", "L'evento selezionato si sovrappone alla pausa pranzo (" + breakStartDisplay + '-' + breakStopDisplay + "), la durata della pausa verrà sottratta dalla durata totale", "Prosegui", "Annulla");
		if (answer == "Annulla") {
			return false;
		}
	}
	return _super.saveEdits(event);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"3E637C1F-28AE-4CF4-BE56-93EFD3BD1974"}
 */
function openRecapHours(event) {
	var win = application.createWindow("Riepilogo Ore", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(1000, 1000, 800, 250);
	win.title = 'Riepilogo Ore'
	win.show(forms.popupRecapHours);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"827787D9-E9CD-4053-B8DA-9894A2F50CCB"}
 */
function stopEditing(event) {
	recIndexOnStartEdit = null;
	//FS
	if(foundset && foundset.getSelectedRecord() && foundset.getSelectedRecord().isNew()){
	globals.selectedEventType=null;
	}
	_super.stopEditing(event);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @private
 *
 * @properties={typeid:24,uuid:"9BCE753B-7015-4F04-9449-564040CB4F9B"}
 */
function deleteRecord(event, index) {
	
	//se la cancellazione è andata a buon fine e c'era un unico record setto a null il campo Tipo Evento
	if(( _super.deleteRecord(event,index)==true) && foundset.getSize()==0){
	
		globals.selectedEventType=null;
	}
}
