/**
 * @param {JSRecord<db:/geco/vice_owner_profit_center>} record
 * @properties={typeid:24,uuid:"CD61A246-77EE-4638-B269-1DB21621F030"}
 */
function validateVicePC(record) {
	if (globals.isEmpty(record.user_id)) {
		throw new Error('- Il campo vice-responsabile è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/vice_owner_profit_center>} record
 * @properties={typeid:24,uuid:"621E0FC8-2272-4F3A-87D5-BA6E75417D58"}
 * @AllowToRunInFind
 */
function validateViceNotOwnerPC(record) {
	// cercare nella tabella owner che non sia responsabile dello stesso centro profitto
	/** @type {JSFoundSet<db:/geco/owners_profit_centers>} */
	var ownerPC = databaseManager.getFoundSet('geco', 'owners_profit_centers');
	ownerPC.loadRecords();
	
	
	var ownerEdited = databaseManager.getEditedRecords(record.vice_owner_to_profit_centers.profit_centers_to_owners_markets);
	for (var i = 0; i< ownerEdited.length; i++){
		/** @type {JSRecord<db:/geco/owners_profit_centers>} */
		var recOwner = ownerEdited[i];
		if (recOwner.user_id == record.user_id && recOwner.profit_center_id == record.profit_center_id) {
			throw new Error('- La persona selezionata è Responsabile del centro profitto');
		}
	}	
	
	if (ownerPC.find()) {
		ownerPC.user_id = record.user_id;
		ownerPC.profit_center_id = record.profit_center_id;
		if (ownerPC.search() > 0) throw new Error('- La persona selezionata è Responsabile del centro profitto');
	}
	
}

/**
 * @param {JSRecord<db:/geco/vice_owner_profit_center>} record
 * @properties={typeid:24,uuid:"E2BB5795-0B98-40AB-A12D-96490459A6BD"}
 * @AllowToRunInFind
 */
function validateViceNotSupportPC(record) {
	//cercare nella tabella supporto che non sia supporto per lo stesso centro profitto
	/** @type {JSFoundSet<db:/geco/comm_supp_profit_centers>} */
	var suppPC = databaseManager.getFoundSet('geco', 'comm_supp_profit_centers');
	suppPC.loadRecords();
	
	var suppEdited = databaseManager.getEditedRecords(record.vice_owner_to_profit_centers.profit_centers_to_comm_support);
	for (var i = 0; i< suppEdited.length; i++){
		/** @type {JSRecord<db:/geco/comm_supp_profit_centers>} */
		var recSupp = suppEdited[i];
		if (recSupp.user_id == record.user_id && recSupp.profit_center_id == record.profit_center_id) {
			throw new Error('- La persona selezionata è Supporto Commerciale del centro profitto');
		}
	}
	
	if (suppPC.find()) {
		suppPC.user_id = record.user_id;
		suppPC.profit_center_id = record.profit_center_id;
		if (suppPC.search() > 0) throw new Error('- La persona selezionata è Supporto Commerciale del centro profitto');
	}
	
	
}

/**
 * Inserisce user nel gruppo mrket manager
 * @param {JSRecord<db:/geco/vice_owner_profit_center>} record
 * @properties={typeid:24,uuid:"3BDDBFC7-D548-4454-92A7-080AAFE70CBC"}
 * @AllowToRunInFind
 */
function processAddGroups(record) {
	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
	try {
		//FS aggiunta statement per impedire errore del DB se la coppia user_id, group_id è già inserita in precedenza
		//DF aggiungo il rolo Viceresponsabile
		if (user_groups.searchUserGroup(record.user_id, 26) == 0) {
			user_groups.addUserGroup(record.user_id, 26);
		}
		//se non è nel gruppo approvatori lo aggiungo
		//FS aggiunta statement per impedire errore del DB se la coppia user_id, group_id è già inserita in precedenza
		if (user_groups.searchUserGroup(record.user_id, 11) == 0 && globals.isApproversAddtoUserGroupTable == false) {
			user_groups.addUserGroup(record.user_id, 11);
		}
		if (!record.isNew()) {
			var ds = record.getChangedData();
			application.output(ds.getValue(1, 1) + ' ' + ds.getValue(1, 2) + ' ' + ds.getValue(1, 3));

			var totOPC = 0
			/** @type {JSFoundSet<db:/geco/vice_owner_profit_center>} */
			var vice_pc = databaseManager.getFoundSet('geco', 'vice_owner_profit_center');
			if (vice_pc.find()) {
				vice_pc.user_id = ds.getValue(1, 2);
				vice_pc.profit_center_id = '!' + record.profit_center_id;
				totOPC = vice_pc.search();

			}
			//se non è responsabile di altri pc gli tolgo il ruolo di vice responsabile mercato
			if (totOPC == 0) {
				user_groups.deleteUserGroup(ds.getValue(1, 2), [26]);
			}
			/** @type {JSFoundSet<db:/geco/approvers>} */
			var approver = databaseManager.getFoundSet('geco', 'approvers');
			if (approver.find()) {
				approver.user_approver_id = ds.getValue(1, 2);
				if (approver.search() == 0) user_groups.deleteUserGroup(ds.getValue(1, 2), [11]);
			}
		}
	} catch (e) {
		throw new Error('- Impossibile assegnare il ruolo di Vice e/o Approvatore alla persona\n' + e['message']);
	} finally {
		globals.isApproversAddtoUserGroupTable == false;
	}
}

/**
 * Cancella il user dal gruppo market manager
 * @param {JSRecord<db:/geco/vice_owner_profit_center>} record
 * @properties={typeid:24,uuid:"5576F08E-2191-4EFF-AC6E-AA59935926AA"}
 * @AllowToRunInFind
 */
function processDeleteUserFromGroup(record) {
	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
	application.output('sto per cancellare solo dal gruppo 26');
	//user_groups.deleteUserGroup(record.user_id, [21]);
	var totOPC = 0
	/** @type {JSFoundSet<db:/geco/vice_owner_profit_center>} */
	var vice_pc = databaseManager.getFoundSet('geco', 'vice_owner_profit_center');
	if (vice_pc.find()) {
		vice_pc.user_id = record.user_id;
		vice_pc.profit_center_id = '!' + record.profit_center_id;
		totOPC = vice_pc.search();

	}
	//se non è responsabile di altri pc gli tolgo il ruolo di vice responsabile mercato
	if (totOPC == 0) {
		user_groups.deleteUserGroup(record.user_id, [26]);
	}
	//	else {
	//		if (vice_pc.find()) {
	//			vice_pc.user_id = record.user_id;
	//			vice_pc.profit_center_id = '!' + record.profit_center_id;
	//			vice_pc.owners_markets_to_profit_centers.profit_center_type_id = 5;
	//			totOPC = vice_pc.search();
	//		}
	//		//non è responsabile di altri mercati dipende se al vice si da anche il ruolo di responsabile mercato
	//		if (totOPC == 0) {
	//			user_groups.deleteUserGroup(record.user_id, [21]);
	//		}
	//	}
	/** @type {JSFoundSet<db:/geco/approvers>} */
	var approver = databaseManager.getFoundSet('geco', 'approvers');
	if (approver.find()) {
		approver.user_approver_id = record.user_id;
		if (approver.search() == 0) user_groups.deleteUserGroup(record.user_id, [11]);
	}
}
