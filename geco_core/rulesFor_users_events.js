/**
 * @param {JSRecord<db:/geco/users_events>} record
 *
 * @properties={typeid:24,uuid:"F2D094A7-64E4-4A02-B813-9ABB159F59B2"}
 * @AllowToRunInFind
 */
function validateUserEventKey(record) {
	if (globals.isEmpty(record.user_id)) {
		application.output(record + ' manca user_id');
	}
	if (globals.isEmpty(record.event_id)) {
		application.output(record + ' manca event_id');
	}
	/** @type {JSFoundSet<db:/geco/users_events>} */
	var users_events = databaseManager.getFoundSet('geco', 'users_events');
	if (users_events.find()){
		users_events.user_id = record.user_id;
		users_events.event_id = record.event_id
		if (users_events.search()>0){
			application.output('esiste già un record per primary key ' +record.user_id+'-'+record.event_id);
		}
		else application.output('NON esiste un record per primary key ' +record.user_id+'-'+record.event_id);
	}
}