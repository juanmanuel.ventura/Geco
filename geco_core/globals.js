/**
 * @type {String}
 * @properties={typeid:35,uuid:"5BEA71BC-D732-4713-9ABD-AB5E52B5BB1A"}
 */
var gecoDb = 'geco';

/**
 * @type {String} *
 * @properties={typeid:35,uuid:"3C107D2F-2703-4897-8B7E-11A93FB0370F",variableType:-4}
 */
var messageLog = security.getUserUID() ? 'UID: ' + security.getUserUID() + ' - ' : '';

/**
 * @type {String}
 * @properties={typeid:35,uuid:"01FB0B88-8E39-413A-940A-7FCEDBF634E1"}
 */
var validationExceptionMessage = '';

/**
 * @type {String}
 * @properties={typeid:35,uuid:"04EBE5FB-2473-46D6-AC54-C2CDB884FA02"}
 */
var deleteExceptionMessage = '';

/**
 * @properties={typeid:35,uuid:"55D6051A-59F4-4E35-8627-5E0F4D00BCB3",variableType:-4}
 * @type {Object}
 */
var COLORS = {
	SELECTION: '#DBE3EB',
	DISABLED: '#cacaca',
	NON_WORKABLE: '#f8f8f8',
	READ_ONLY: '#eaeaea'
};

/**
 * @properties={typeid:35,uuid:"D9651658-5E5A-42D2-AC5F-AB28564424E9",variableType:-4}
 * @type {Object}
 */
var mailOptions = {
	username: 'noreply@spindox.it',
	password: '$p1nd0xG3c0'
};

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"47AE1F22-AEA4-4CCC-B0D3-8BFF92C25A54",variableType:8}
 */
var selectedJobOrder = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"3A020DA5-38B9-4A25-B780-ED0BDAC03573",variableType:8}
 */
var job_order_selected = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"00D28E25-556E-4B73-9DD4-0404853A6909",variableType:8}
 */
var selectedEventType = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"A535D73E-CAD2-4A9C-8D07-5B9D38278FFF",variableType:8}
 */
var selectedEvent = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"E234C1BA-145C-47A9-8EBA-3829B965A71E",variableType:8}
 */
var selectedUser = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"C841117B-BD2D-4D27-AD1E-814AEC262414",variableType:8}
 */
var selectedYear = new Date().getFullYear();

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"1548BEC7-EC04-4FCD-A5E1-9B82C231ACC2",variableType:8}
 */
var selectedMonth = new Date().getMonth() + 1;

/**
 * @type {JSDataSet}
 * @properties={typeid:35,uuid:"3E02DCF4-4209-4AB3-A436-346F2FFFC5F5",variableType:-4}
 */
var valueLists = { };

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"536BC401-0AB3-4929-9A46-1497E8E58D8A",variableType:8}
 */
var currentUserId = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"10B26713-5853-4602-A587-6CFE65995046"}
 */
var currentUserDisplayName = null;

/**
 * @properties={typeid:35,uuid:"6FA074C8-4344-4A35-9097-100B6F6210B7",variableType:-4}
 */
var valueListCache = { };

/**
 * @properties={typeid:35,uuid:"709CF577-964E-4884-860C-5A4906F9F7B9",variableType:-4}
 */
var validationRules = { };

/**
 * @properties={typeid:35,uuid:"B015D3F2-3C3B-4F27-9172-7DB38CD2AAD9",variableType:-4}
 */
var processRules = { };

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"CBBD96B9-F34B-4C31-A52A-267422198DEB",variableType:8}
 */
var workingMonth = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"F7AD5F0C-7ACF-41EA-A79C-8A8DA2576B9F",variableType:8}
 */
var workingYear = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"4CF917A4-3DA6-4C88-B27E-F6F117EFE7E2",variableType:8}
 */
var selectedStartTime = null;

/**
 * @type {RuntimeForm}
 * @properties={typeid:35,uuid:"BA54B08A-D049-4232-AC96-B546EA3150E6",variableType:-4}
 */
var callerForm = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"5BACE01F-2420-4E4C-8B9E-737BF61C4CB2",variableType:8}
 */
var selectedExpensesType = null;

/**
 * @type {Array<plugins.file.JSFile>}
 * @properties={typeid:35,uuid:"BAEDB310-54F1-4F8F-9802-1EDF4AAFA43E",variableType:-4}
 */
var files = null;

/**
 * @type {plugins.file.JSFile}
 * @properties={typeid:35,uuid:"9A61EF7A-947A-49D9-96E4-C5AF04D8332E",variableType:-4}
 */
var tempFolder = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"FDA75E79-EEE2-49F9-A09D-4DACF039E63B"}
 */
var resultImportDocuments = '';

/**
 * @type {String}
 * @properties={typeid:35,uuid:"98A1B5DC-AE4E-4057-958C-8C3C5A6BF954"}
 */
var tot_duration = '';
/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3E4CB2B1-2E3C-4587-B65C-F9A4CD15899C"}
 */
var tot_duration_regular = '';
/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2A59F8FC-BDED-4C15-ACB6-B9FEE5C69AE1"}
 */
var tot_duration_overtime = '';

/**
 * @type {plugins.file.JSFile}
 * @properties={typeid:35,uuid:"BC27EFFF-EAAF-4DC1-ABB9-1B864889BC80",variableType:-4}
 */
var serverDir = null;

/**
 * @type {plugins.file.JSFile}
 * @properties={typeid:35,uuid:"CF5E77CB-DF5C-436F-AEC3-0380CD5D31EE",variableType:-4}
 */
var serverfile = null;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"E8B8E2A5-6149-4BB7-A187-321611E44F60",variableType:93}
 */
var actualDate = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"3EFA0CA3-5646-4539-9AF9-EDD5E46EFE2E",variableType:8}
 */
var actualMonth = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"972ABCA0-6FBC-4931-9836-E94062ED3DA0"}
 */
var actualMonthStrIT = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"6F6C4723-DC11-486F-B4A1-317A1B26CD46",variableType:8}
 */
var actualYear = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"365425CB-A1A8-4680-8AF9-D675879DEE11",variableType:4}
 */
var isOpenToActual = 0;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"FF14048B-01F1-45EF-A57C-D63D861E02D3",variableType:4}
 */
var statusActual = 0;
/**
 * @type {Number}
 * @properties={typeid:35,uuid:"33C7D51B-3D4B-4100-98CD-BFE4E6A796AA",variableType:8}
 */
var selected_client = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"244F6E9F-B45B-4F06-A756-A62C9649D1E5",variableType:8}
 */
var selectedPC = null;

/**
 *
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"BCCA6E5A-BD03-4AE5-A4FB-18A6A2C79B4D",variableType:8}
 */
var selectedPCCommSupp = null;

/**
 *
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"C6CBC93A-D2E3-483A-8D6E-CFAFF013B10E",variableType:8}
 */
var selectedPCCommSuppBoDetails = null;

/**
 * @properties={typeid:35,uuid:"6CFEE7C6-DDDD-4654-8B63-E9883C5FE63B",variableType:-4}
 */
var monthName = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"2203A216-F76D-4CFD-A50C-A87D569D9735",variableType:8}
 */
var actualYearFilterController = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"CF4DC33A-683E-44B7-8199-5583A015A88A",variableType:8}
 */
var actualMonthFilterController = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"FBA6BDE8-14D8-4F86-BEC7-403DD961D7E2",variableType:4}
 */
var totalMonthReturnActual = 0;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"B435C5C0-767E-4ACD-AAC4-CCF8D0898893",variableType:4}
 */
var elapsedMaxMonth = 2;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"7245357C-ACDC-44BD-B95A-DAECEC8249E6",variableType:4}
 */
var mpUserId = null;

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"DB2FFD26-13C3-4402-819A-9D1A64FCD315",variableType:-4}
 */
var hasMedicalPractice = false;

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"DBA7DCE5-EFA0-42AD-A6C1-0479A33F970D",variableType:-4}
 */
var isFromGeco = false;

/**
 * Serve per sapere se sono in jo_details o no e gestire l'updateUI sul bottone rifiuta actual
 * @type {String}
 * @properties={typeid:35,uuid:"F3959CA6-BC07-4445-BBC4-78335EA77495"}
 */
var frmSelectedManagement = '';

/**@type {Array}
 * @properties={typeid:35,uuid:"C8DB1B28-43E3-4F66-B0B4-70DE5A2F73AD",variableType:-4}
 */
var userListAddedToJO = [];

/** @type {JSDataSet} *
 * @properties={typeid:35,uuid:"D022194A-B74F-4CDD-BA8E-0B1129B6B040",variableType:-4}
 */
var currentUserRoleList = security.getUserGroups();

/**
 * Called when the valuelist needs data, it has 3 modes.
 * real and display params both null: return the whole list
 * only display is specified, called by a typeahead, return a filtered list
 * only real value is specified, called when the list doesnt contain the real value for the give record value,
 * this will insert this value into the existing list
 *
 * @param {String} displayValue The value of a lookupfield that a user types
 * @param realValue The real value for a lookupfield where a display value should be get for
 * @param {JSRecord} record The current record for the valuelist.
 * @param {String} valueListName The valuelist name that triggers the method. (This is the FindRecord in find mode,
 * which is like JSRecord has all the columns/dataproviders, but doesn't have its methods)
 * @param {Boolean} findMode True if foundset of this record is in find mode
 *
 * @returns {JSDataSet} A dataset with 1 or 2 columns display[,real]
 *
 * @properties={typeid:24,uuid:"CEE2A340-B861-413A-BF0F-61BB073E341E"}
 */
function loadValueLists(displayValue, realValue, record, valueListName, findMode) {
	/** @type {JSDataSet} */
	var valueList = null;
	if (valueListName) {
		valueList = globals.valueListCache[valueListName];
		if (!valueList) {
			var sql = '';
			var params = null;

			switch (valueListName) {
			case 'currentUserAllowedEventTypes':

				sql = 'SELECT \
				    et.event_type, et.event_type_id \
				FROM \
				    event_types et, \
				    users_events ue, \
				    events e \
				WHERE \
				    ue.user_id = ? \
				        and et.event_type_id = e.event_type_id \
				        and e.event_id = ue.event_id \
				GROUP BY et.event_type \
				ORDER BY et.event_type ';

				params = [globals.currentUserId];

				valueList = databaseManager.getDataSetByQuery('geco', sql, params, -1);
				break;

			case 'yearMonths':

				valueList = databaseManager.createEmptyDataSet(0, 2);
				var months = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];
				for (var index = 0; index < months.length; index++) {
					valueList.addRow(new Array(months[index], index + 1));
				}

				break;
			//			case 'currentUserAllowedEvents':
			//				sql = 'SELECT e.event_name, e.event_id from events e \
			//				INNER JOIN users_events ue ON e.event_id = ue.event_id \
			//				WHERE ue.user_id = ? AND e.event_type_id = ?\
			//				ORDER BY e.event_name asc;';
			//				params = [globals.currentUserId, globals.selectedEventType];
			//				break;
			//			case 'currentUserAllowedJobOrders':
			//				sql = 'SELECT jo.job_order_title, jo.job_order_id FROM job_orders jo \
			//				INNER JOIN users_job_orders ujo ON jo.job_order_id = ujo.job_order_id \
			//				WHERE ujo.user_id = ? AND jo.is_enabled = 1 \
			//				ORDER BY jo.job_order_title';
			//				params = [globals.currentUserId];
			//				break;
			//			case 'timeStart':
			//				sql = 'SELECT display, minutes FROM calendar_hours WHERE `minutes` < ?;';
			//				params = [1440];
			//				break;
			//			case 'users':
			//				sql = 'SELECT c.real_name, u.user_id FROM contacts c, users u \
			//				WHERE u.contact_id = c.contact_id \
			//				ORDER BY c.real_name;'
			//				break;
			}

			globals.valueListCache[valueListName] = valueList;
		}
	}
	return valueList;
}

/**
 * @param {String} valueListName
 *
 * @properties={typeid:24,uuid:"F3FAC546-9353-41FB-A3A5-521E7028845B"}
 */
function clearValueList(valueListName) {
	globals.valueListCache[valueListName] = null;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formName
 * @properties={typeid:24,uuid:"C849B114-03C1-4EA3-80ED-E60CEDF5A531"}
 */
function showFormInDialog(event, formName) {
	var win = application.createWindow(formName, JSWindow.MODAL_DIALOG)
	win.setSize(1280, 657);
	win.setLocation(win.getX() + 100, win.getY() + 100);
	win.show(forms[formName]);
}

/**
 * @properties={typeid:24,uuid:"4C3C81F1-531C-4A59-86FC-C4208A188DE9"}
 * @param {RuntimeTabPanel} tabPanel
 * @param {RuntimeForm} form
 */
function loadFormInTabPanel(tabPanel, form) {
	if (tabPanel.removeAllTabs()) tabPanel.addTab(form);
}

/**
 * @param {String} formName
 * @properties={typeid:24,uuid:"CBA0FF68-7CD7-4AE3-8CF2-FAAB3D6CF646"}
 */
function prepareWindow(formName) {
	var form = forms[formName];

	// hide menu and tool bars
	plugins.window.getMenuBar().setVisible(false);
	plugins.window.setToolBarAreaVisible(false);
	plugins.window.setStatusBarVisible(false);

	// set size and center the window
	var window = application.getWindow();
	window.show(form);
	if (application.getOSName() != 'Mac OS X') plugins.window.maximize();
}

/**
 * @param {Number} minutes
 * @return {String}
 * @properties={typeid:24,uuid:"123BAE71-BF1B-4BB6-9A16-0F75E871D16D"}
 */
function formatMinutes(minutes) {
	var hour, min = null;

	hour = Math.floor(minutes / 60);
	min = minutes % 60;
	if (min == 0) min = '00';

	return hour + ':' + min;
}

/**
 * @properties={typeid:24,uuid:"CCFF64A6-DE36-455C-A7F2-82275D8A323A"}
 * @AllowToRunInFind
 */
function loadRunnableRules() {
	buildRulesByKind(0);
	buildRulesByKind(1);
}

/**
 *  This method loads into an global object all the defined and enabled rules, the object has this form:
 *
 *  container = {
 *		entity1: {
 *			onInsert: [rule1, rule2, ruleN],
 *			onUpdate: [rule1, rule2, ruleN],
 *			onDelete: [rule1, rule2, ruleN],
 *		}
 *		entity2: {
 *			onInsert: [rule1, rule2, ruleN],
 *			onUpdate: [rule1, rule2, ruleN],
 *			onDelete: [rule1, rule2, ruleN],
 *		}
 *  }
 *
 * Once this is done the applyRules method can iterate and apply to the passed record the
 * proper rules based on its entity and the triggered event (onInsert, onUpdate, onDelete)
 *
 * @param {Number} kind The rule kind: 0 = validation, 1 = process
 * @properties={typeid:24,uuid:"2683ADF3-B1CF-4540-A8B9-6D32A1498084"}
 * @AllowToRunInFind
 */
function buildRulesByKind(kind) {

	var container = null;
	if (kind == 0) container = 'validationRules';
	if (kind == 1) container = 'processRules';

	// reinitialize the container
	globals[container] = { };

	/** @type {JSFoundSet<db:/geco/rules>} */
	var rules = databaseManager.getFoundSet('geco', 'rules');

	if (rules && rules.find()) {
		rules.is_enabled = 1;
		rules.rule_kind = kind;
		rules.search();
		rules.sort('workig_entity, execution_order');

		// loop thru the entities
		for (var i = 1; i <= rules.getSize(); i++) {
			var record = rules.getRecord(i);
			var triggers = { };

			if (record.on_insert) triggers.onInsert = true;
			if (record.on_update) triggers.onUpdate = true;
			if (record.on_delete) triggers.onDelete = true;

			/** @type {{onInsert: String[], onUpdate: String[], onDelete: String[]}} */
			var entityRules = globals[container][record.working_entity] || new Object;

			// loop thru the events for this entity
			for (var trigger in triggers) {
				/** @type {String[]} */
				var eventRules = entityRules[trigger] || new Array;
				eventRules.push(record.invoked_method);
				entityRules[trigger] = eventRules;
			}
			// fill the container
			globals[container][record.working_entity] = entityRules;
		}
	}
}

/**
 * @properties={typeid:35,uuid:"517C28F1-8B60-430C-A183-DE97FA542097",variableType:-4}
 * @type {Boolean}
 */
var preocessResult = false;

/**
 * This method applies all the rules in sequence for the passed record, it should be called directly
 * from the entity responding to it's events: onRecordInsert, onRecordUpdate and onRecordDelete.
 *
 * @param {JSRecord} record The record to be validated/processed
 * @param {String} eventType The triggered event type (onInsert, onUpdate, onDelete)
 * @return {Boolean}
 * @throws {Error}
 * @properties={typeid:24,uuid:"119EC788-456F-411C-99D6-142C0285D653"}
 */
function applyAllRules(record, eventType) {
	validationExceptionMessage = '';

	// WEB only
	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		try {
			if (applyRulesByKind(record, eventType, 0)) {
				// only run process rules if validation has passed
				return applyRulesByKind(record, eventType, 1);
			}
		} catch (e) {
			validationExceptionMessage = e.message;
			application.output('1 web ' + e)
			return false;
		}
	}
	if (application.getApplicationType() == APPLICATION_TYPES.HEADLESS_CLIENT) {
		try {
			if (applyRulesByKind(record, eventType, 0)) {
				// only run process rules if validation has passed
				return applyRulesByKind(record, eventType, 1);
			}
		} catch (e) {
			application.output('2 web ' + e);
			validationExceptionMessage = e.message;
			return false;
		}
	}

	if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
		// SMART only
		var params = {
			processFunction: processApplyAllRules,
			processArgs: [record, eventType],
			message: 'Processando, attendere...',
			opacity: 0.75,
			paneColor: '#000000',
			textColor: '#FFFFFF',
			showCancelButton: false,
			fontType: 'Calibri,0,48'
		};

		plugins.busy.block(params);
		// when finished return the result
		return preocessResult;
	}
	return true;
}

/**
 * This method applies all the rules in sequence for the passed record, it should be called directly
 * from the entity responding to it's events: onRecordInsert, onRecordUpdate and onRecordDelete.
 *
 * @param {JSRecord} record The record to be validated/processed
 * @param {String} eventType The triggered event type (onInsert, onUpdate, onDelete)
 * @throws {Error}
 * @properties={typeid:24,uuid:"FBEBE5EA-E6EE-4615-AA78-67C05D25F9AD"}
 */
function processApplyAllRules(record, eventType) {

	// dismiss any remaining busy dialog
	application.updateUI();

	try {
		if (applyRulesByKind(record, eventType, 0)) {
			// only run process rules if validation has passed
			preocessResult = applyRulesByKind(record, eventType, 1);
		}
	} catch (e) {
		//DIALOGS.showErrorDialog("Error", e.message, 'OK');
		preocessResult = false;
		validationExceptionMessage = e.message;
		application.output('3 ' + e)
		//return false;
	} finally {
		if (plugins.busy) plugins.busy.unblock();
	}
}

/**
 * Applies the validation rules in sequence for the passed record.
 *
 * @param {JSRecord} record The record to be validated/processed
 * @param {String} eventType The triggered event type (onInsert, onUpdate, onDelete)
 * @param {Number} kind The rules kind: 0 = validation, 1 = process
 *
 * @return {Boolean}
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"A34CC177-B641-4E16-9864-19C9CF83150A"}
 */
function applyRulesByKind(record, eventType, kind) {
	var entity = databaseManager.getDataSourceTableName(record.getDataSource());
	var entityRules = null;
	if (kind == 0) entityRules = validationRules[entity];
	if (kind == 1) entityRules = processRules[entity];

	if (entityRules) {
		/** @type {String[]} */
		var eventRules = entityRules[eventType];
		if (eventRules && eventRules.length > 0) {
			/** @type {String[]} */
			var errors = [];
			var totaltime = null;

			application.output( ( (kind == 0) ? 'VALIDATING ' : 'PROCESSING ') + entity.toUpperCase())
			application.output(messageLog + "\n----------------------------------------------------------------------------------------");

			for (var rule in eventRules) {
				var start = Date.now();
				try {

					// check rule existence
					if (!scopes['rulesFor_' + entity][eventRules[rule]]) {
						application.output('undefined rule: ' + eventRules[rule]);
					}

					// run the rule
					var stepTime = Date.now() - start;
					application.output(parseInt(rule) + 1 + ") running " + eventRules[rule]);
					scopes['rulesFor_' + entity][eventRules[rule]].call(this, record);

					// print some info
					totaltime += stepTime;
					application.output("\tOK - rule executed in " + stepTime + " ms ");
					application.output("----------------------------------------------------------------------------------------");

				} catch (e) {
					application.output('4 ' + e)
					// fill errors
					errors.push(e.message);

					// print errors
					application.output("\n\t********************************  FAILED  **************************************");
					application.output("\toperation\t: " + eventType);
					application.output("\tentity\t\t: " + entity);
					application.output("\trecord id\t: " + record.getPKs()[0]);
					application.output("\terror\t\t: " + e.message);
					application.output("\t********************************************************************************\n");
					application.output("----------------------------------------------------------------------------------------");
				}
			}

			application.output("\nTOTAL EXECUTION TIME: " + totaltime + " ms\n\n\n");
			if (errors.length > 0) {
				var stringed = errors.join('\n');
				throw new Error(stringed);
			}
		}
	}

	return true;
}

/**
 * Establish if the value is null OR an empty string
 *
 * @param {Object} value
 * @return {Boolean}
 * @properties={typeid:24,uuid:"CB805201-3D09-4566-9319-244D33EBC0EF"}
 */
function isEmpty(value) {
	if (value == null) return true;
	if (value.toString().replace(/^\s+|\s+$/g, '').length == 0) return true;
	return false;
}

/**
 * @param {Date} date
 * @properties={typeid:24,uuid:"0B591B32-4D6A-4B87-B52C-3C6CAFF01232"}
 */
function isWeekEnd(date) {
	return (date.getDay() == 0 || date.getDay() == 6);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} year
 * @param {Number} month
 * @param {Number} status
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"255D8F40-7371-43D3-9BA6-739E51B498AB"}
 * @SuppressWarnings(deprecated)
 * @SuppressWarnings(wrongparameters)
 */
function exportExpenses(event, year, month, status) {
	var success = true;
	//	var response = globals.DIALOGS.showQuestionDialog('Conferma', 'Export per il periodo ' + month + ' ' + year + '. \n Proseguire?', 'Si', 'No');
	//	if (response == 'No') return;

	//if (month.toString().length == 1 ) month = '0'+ month;

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		success = processSpExportExpenses(null, 'NS_', year, month, status);
	}
	//showFileSaveDialog non è compatibile con webclient
	if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
		var file = plugins.file.showFileSaveDialog()
		if (file != null && file.exists()) {
			globals.DIALOGS.showErrorDialog("Errore", "Non puoi sovrascrivere!", 'OK');
			return;
		}

		if (file && file.createNewFile()) {
			success = processSpExportExpenses(file, 'NS_', year, month, status);
			if (!success) {
				globals.DIALOGS.showErrorDialog("Error", "Export fallito", 'OK');
				return;
			}
		}
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} year
 * @param {Number} month
 * @param {Number} status
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"73E7AB9D-39E6-4CC7-8FBE-1DDC164F75E9"}
 * @SuppressWarnings(deprecated)
 * @SuppressWarnings(wrongparameters)
 */
function exportGeneralExpenses(event, year, month, status) {
	var success = true;

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		success = processSpExportGeneralExpenses(null, 'NS_Generale_', year, month, status);
	}
	//showFileSaveDialog non è compatibile con webclient
	if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
		var file = plugins.file.showFileSaveDialog()
		if (file != null && file.exists()) {
			globals.DIALOGS.showErrorDialog("Errore", "Non puoi sovrascrivere!", 'OK');
			return;
		}

		if (file && file.createNewFile()) {
			success = processSpExportGeneralExpenses(file, 'NS_Generale_', year, month, status);
			if (!success) {
				globals.DIALOGS.showErrorDialog("Error", "Export fallito", 'OK');
				return;
			}
		}

	}
}

/**
 * @AllowToRunInFind
 * @param {plugins.file.JSFile} file
 * @param {plugins.file.JSFile} fileName
 * @param {Number} year
 * @param {Number} month
 * @param {Number} status
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"6773493E-052A-44D1-92F3-C11BDB1AF1AE"}
 */

function processSpExportGeneralExpenses(file, fileName, year, month, status) {

	application.updateUI();
	var success = false;
	try {
		var sql_query = 'call sp_general_expenses_amount(?,?,?,?,?,?,?)';
		var arguments = new Array();
		arguments[0] = year;
		arguments [1] = month;
		arguments[2] = (status == null) ? 0 : status;
		//TODO nel prossimo rilascio mettere  stringhe vuote nella dichaira le var delle date di tipo varchar
		arguments[3] = 0;
		arguments[4] = 0;
		arguments[5] = 0;
		arguments[6] = 0;
		//		var dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, 1000)
		var dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, 10000);
		var line = '';
		var columnsArray = new Array()
		for (var i = 1; i <= dataset.getMaxColumnIndex(); i++) {
			columnsArray[i - 1] = dataset.getColumnName(i)
		}

		line = columnsArray.join('\t') + '\r\n';
		for (var index = 1; index <= dataset.getMaxRowIndex(); index++) {
			var row = dataset.getRowAsArray(index);
			var rowstring = row.join('\t');
			line = line + rowstring + '\n';
		}
		//application.output(line);

		if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
			/** @type String */
			var month1 = month.toLocaleString();
			fileName = fileName + year + ( (month1.length > 1) ? month1 : '0' + month1) + '.xls';
			success = plugins.file.writeTXTFile(fileName, line, 'UTF-8', 'application/vnd.ms-excel');
		} else if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
			success = plugins.file.writeTXTFile(file, line, 'UTF-8');
		}

		if (!success) application.output('Could not write file.');

	} catch (e) {
		application.output(e);
		application.output("Export failed");
	} finally {
		databaseManager.setAutoSave(true);
		if (plugins.busy) plugins.busy.unblock();
		return success;
	}
}

/**
 * @AllowToRunInFind
 * @param {plugins.file.JSFile} file
 * @param {plugins.file.JSFile} fileName
 * @param {Number} year
 * @param {Number} month
 * @param {Number} status
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"9E6A0B94-05E8-4960-8C1D-5D4E9E4142E5"}
 */

function processSpExportExpenses(file, fileName, year, month, status) {
	// dismiss any remaining busy dialog
	application.updateUI();
	var success = false;
	try {
		var maxReturnedRows = 10000; //useful to limit number of rows
		//		var server_name = databaseManager.getDataSourceServerName(controller.getDataSource());
		var sql_query = 'call sp_expenses_amount(?,?,?)';
		var arguments = new Array();
		arguments[0] = year;
		arguments [1] = month;
		arguments[2] = (status == null) ? 0 : status;
		var dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, maxReturnedRows)
		var line = '';
		//linea titoli colonne
		line += 'azienda\t' + 'matricola\t' + 'Nominativo\t' + 'anno_mese\t' + 'spese_di_rappresentanza\t' + 'rimborso_chilometrico_fuori_dal_comune\t' + 'rimborso_chilometrico_nel_comune\t' + 'rimborso_spese_c_fattura\t' + 'rimborso_spese_varie_fuori_dal_comune\t' + 'rimborso_spese_varie_nel_comune\t' + 'rimborso_carburante_esente\t' + 'rimborso_spese_di_trasporto_nel_comune\t' + 'totale\n';
		//var riga2 = new Array();
		for (var index = 1; index <= dataset.getMaxRowIndex(); index++) {
			var riga = dataset.getRowAsArray(index);
			//riga2.concat(riga);
			var rigaStr = riga.join('\t');
			line = line + rigaStr + '\r\n';
		}

		if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
			/** @type String */
			var month1 = month.toLocaleString();
			fileName = fileName + year + (month1.length > 1 ? month1 : '0' + month1) + '.xls';
			success = plugins.file.writeTXTFile(fileName, line, 'UTF-8', 'application/vnd.ms-excel');
		} else if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
			success = plugins.file.writeTXTFile(file, line, 'UTF-8');
		}

		if (!success) application.output('Could not write file.');

	} catch (e) {
		application.output(e);
		application.output("Exporty failed");
	} finally {
		databaseManager.setAutoSave(true);
		if (plugins.busy) plugins.busy.unblock();
		return success;
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} year
 * @param {Number} month
 * @param {Number} status
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"F2697E89-B139-4463-A99B-D1D47BB18906"}
 * @SuppressWarnings(wrongparameters)
 * @SuppressWarnings(deprecated)
 */
function exportBsCodi(event, year, month, status) {
	var success = true;
	//	var response = globals.DIALOGS.showQuestionDialog('Conferma', 'Export per il periodo ' + month + ' ' + year + '. \n Proseguire?', 'Si', 'No');
	//	if (response == 'No') return;

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		success = processExportSpBsCodi(null, 'bs_codi_', year, month, status);
	}
	//showFileSaveDialog non è compatibile con webclient
	if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {

		var file = plugins.file.showFileSaveDialog()
		if (file != null && file.exists()) {
			globals.DIALOGS.showErrorDialog("Errore", "Non puoi sovrascrivere!", 'OK');
			return;
		}
		if (file && file.createNewFile()) {
			success = processExportSpBsCodi(file, null, year, month, status);
			if (!success) {
				globals.DIALOGS.showErrorDialog("Error", "Export fallito", 'OK');
				return;
			}
		}

		// Create the file on disk.
		//processFunction: processExportBsCodi, sostituita per chiamare function con SP
		//		if (file && file.createNewFile()) {
		//
		//			var params = {
		//				processFunction: processExportSpBsCodi,
		//				message: 'Operazione in corso, attendere...',
		//				opacity: 0.75,
		//				paneColor: '#000000',
		//				textColor: '#FFFFFF',
		//				showCancelButton: true,
		//				cancelButtonText: 'Stop!',
		//				fontType: 'Calibri,0,48',
		//				processArgs: [file, year, month, status]
		//
		//			};
		//
		//			plugins.busy.block(params);
		//		} else {
		//			application.output("Export file not created.");
		//
		//		}

	}
}

/**
 * @param {plugins.file.JSFile} file
 * @param {String} fileName
 * @param {Number} year
 * @param {Number} month
 * @param {Number} status
 *
 * @properties={typeid:24,uuid:"20A04715-B20F-4E1D-9476-EC4AEFBE798F"}
 */
function processExportSpBsCodi(file, fileName, year, month, status) {
	// dismiss any remaining busy dialog
	application.updateUI();
	var success = true;
	try {
		var maxReturnedRows = 10000; //useful to limit number of rows
		var sql_query = 'call sp_bs_codi(?,?,?)';
		var arguments = new Array();
		arguments[0] = year;
		arguments [1] = month;
		arguments[2] = (status == null) ? 0 : status;
		var dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, maxReturnedRows)
		var line = '';
		for (var index = 1; index <= dataset.getMaxRowIndex(); index++) {
			var riga = dataset.getRowAsArray(index);
			//application.output('riga array' + riga);
			var rigaStr = riga.join('\t');
			line = line + rigaStr + '\r\n';
			//application.output('riga ' + line);
		}

		if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
			fileName = fileName + year + month;
			// mimeType per excel   'application/vnd.ms-excel'
			success = plugins.file.writeTXTFile(fileName, line, 'UTF-8', 'application/vnd.ms-excel');
		} else if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
			success = plugins.file.writeTXTFile(file, line, 'UTF-8');
		}

		//var success = plugins.file.writeTXTFile(file, line, 'UTF-8');

		if (!success) application.output('Could not write file.');

	} catch (e) {
		application.output(e);
		application.output("Exporty failed");
	} finally {
		databaseManager.setAutoSave(true);
		//		if (plugins.busy) plugins.busy.unblock();
	}
}

/**
 * The SHA1 cipher Method
 * @param str The string to encode
 *
 * @properties={typeid:24,uuid:"C7912411-A666-4153-9383-3A517E5062BD"}
 */
function sha1(str) {
	var rotate_left = function(n, s) {
		var t4 = (n << s) | (n >>> (32 - s));
		return t4;
	};

	var cvt_hex = function(val) {
		str = "";
		var k;
		var v;

		for (k = 7; k >= 0; k--) {
			v = (val >>> (k * 4)) & 0x0f;
			str += v.toString(16);
		}
		return str;
	};

	var blockstart;
	var i, j;
	var W = new Array(80);
	var H0 = 0x67452301;
	var H1 = 0xEFCDAB89;
	var H2 = 0x98BADCFE;
	var H3 = 0x10325476;
	var H4 = 0xC3D2E1F0;
	var A, B, C, D, E;
	var temp;

	var str_len = str.length;

	var word_array = [];
	for (i = 0; i < str_len - 3; i += 4) {
		j = str.charCodeAt(i) << 24 | str.charCodeAt(i + 1) << 16 | str.charCodeAt(i + 2) << 8 | str.charCodeAt(i + 3);
		word_array.push(j);
	}

	switch (str_len % 4) {
	case 0:
		i = 0x080000000;
		break;
	case 1:
		i = str.charCodeAt(str_len - 1) << 24 | 0x0800000;
		break;
	case 2:
		i = str.charCodeAt(str_len - 2) << 24 | str.charCodeAt(str_len - 1) << 16 | 0x08000;
		break;
	case 3:
		i = str.charCodeAt(str_len - 3) << 24 | str.charCodeAt(str_len - 2) << 16 | str.charCodeAt(str_len - 1) << 8 | 0x80;
		break;
	}

	word_array.push(i);

	while ( (word_array.length % 16) != 14) {
		word_array.push(0);
	}

	word_array.push(str_len >>> 29);
	word_array.push( (str_len << 3) & 0x0ffffffff);

	for (blockstart = 0; blockstart < word_array.length; blockstart += 16) {
		for (i = 0; i < 16; i++) {
			W[i] = word_array[blockstart + i];
		}
		for (i = 16; i <= 79; i++) {
			W[i] = rotate_left(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);
		}

		A = H0;
		B = H1;
		C = H2;
		D = H3;
		E = H4;

		for (i = 0; i <= 19; i++) {
			temp = (rotate_left(A, 5) + ( (B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
			E = D;
			D = C;
			C = rotate_left(B, 30);
			B = A;
			A = temp;
		}

		for (i = 20; i <= 39; i++) {
			temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
			E = D;
			D = C;
			C = rotate_left(B, 30);
			B = A;
			A = temp;
		}

		for (i = 40; i <= 59; i++) {
			temp = (rotate_left(A, 5) + ( (B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
			E = D;
			D = C;
			C = rotate_left(B, 30);
			B = A;
			A = temp;
		}

		for (i = 60; i <= 79; i++) {
			temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
			E = D;
			D = C;
			C = rotate_left(B, 30);
			B = A;
			A = temp;
		}

		H0 = (H0 + A) & 0x0ffffffff;
		H1 = (H1 + B) & 0x0ffffffff;
		H2 = (H2 + C) & 0x0ffffffff;
		H3 = (H3 + D) & 0x0ffffffff;
		H4 = (H4 + E) & 0x0ffffffff;
	}

	temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
	return temp.toLowerCase();
}

/**
 * Return true if user is in group
 * @param {String} group
 * @return {Boolean}
 * @properties={typeid:24,uuid:"07E251F0-77CB-4993-896E-B7A5B7D27916"}
 */
function hasRole(group) {
	var groups = currentUserRoleList; //security.getUserGroups();

	for (var index = 1; index <= groups.getMaxRowIndex(); index++) {
		if (groups.getValue(index, 2) == group) {
			return true;
		}
	}
	return false;
}

/**
 * @AllowToRunInFind
 * @param {Number} userId
 * @return {Boolean}
 * @properties={typeid:24,uuid:"6611074F-DA05-4F06-99AA-59F70E4B5D7D"}
 */
function isProfitCenterManager(userId) {
	/** @type {JSFoundSet<db:/geco/profit_centers>} */
	var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');
	if (profit_centers.find()) {
		profit_centers.user_owner_id = userId;
		if (profit_centers.search() > 0) return true;
	}
	return false;
}

/**
 * @AllowToRunInFind
 * @param {Number} userId
 * @return {Boolean}
 * @properties={typeid:24,uuid:"B7395FBC-D408-486A-A746-C02F665C52E2"}
 */
function isJobOrderManager(userId) {
	/** @type {JSFoundSet<db:/geco/job_orders>} */
	var job_orders = databaseManager.getFoundSet('geco', 'job_orders');
	if (job_orders.find()) {
		job_orders.user_owner_id = userId;
		if (job_orders.search() > 0) return true;
	}
	return false;
}

/**
 * SUPPORTO COMMERCIALE: Restituisce true se il cliente scelto è presente nel mercato scelto.
 *
 * @AllowToRunInFind
 * @param {Number} clientId
 * @param {Number} pcId
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"8BD03044-4B9C-48CA-BA94-7AD1DD40605C"}
 */
function isClientPCSuppComm(clientId, pcId) {
	/** @type {JSFoundSet<db:/geco/comm_supp_profit_centers>} */
	var commSuppPc = databaseManager.getFoundSet('geco', 'comm_supp_profit_centers');
	/** @type {JSFoundSet<db:/geco/comm_supp_customers>} */
	var commSuppCustomers = databaseManager.getFoundSet('geco', 'comm_supp_customers');

	var totPC = 0;
	var totCustomers = 0;

	//andare in find su tab dei pc per utente e mercato;
	if (commSuppPc.find()) {
		commSuppPc.user_id = scopes.globals.currentUserId;
		commSuppPc.profit_center_id = pcId;
		totPC = commSuppPc.search();
	}

	if (totPC > 0) {
		var recTot = commSuppPc.getRecord(1);

		//andare in find su quella dei clienti dove l'id di quella prima è uguale a quello della seconda tabella
		//e dove il cliente è quello che mi arriva
		if (commSuppCustomers.find()) {
			commSuppCustomers.comm_supp_pc_id = recTot.comm_supp_profit_center_id;
			commSuppCustomers.customer_id = clientId;
			totCustomers = commSuppCustomers.search();
		}

		if (totCustomers > 0) return true;
	}

	return false;
}

/**
 * Return true if user can access to solution
 * @param {String[]} groupsList groups enabled
 * @return {Boolean}
 * @properties={typeid:24,uuid:"59DAECBE-7289-4786-A1B0-3A0E4B3D80DD"}
 */
function grantAccess(groupsList) {
	application.output(globals.messageLog + 'START globals.grantAccess() ' + groupsList, LOGGINGLEVEL.INFO);
	groupsList.push("Administrators");
	var groups = security.getUserGroups();

	for (var index = 1; index <= groups.getMaxRowIndex(); index++) {
		for (var i = 0; i < groupsList.length; i++) {
			if (groups.getValue(index, 2) == groupsList[i]) {
				//trovato uno dei gruppi abilitati
				application.output(globals.messageLog + 'STOP grantAccess() abilitato', LOGGINGLEVEL.INFO);
				return true;
			}
		}
	}
	globals.DIALOGS.showErrorDialog("Errore", "Non hai i permessi necessari per accedere a questa applicazione", 'OK');
	application.output(globals.messageLog + 'STOP globals.grantAccess() NON abilitato', LOGGINGLEVEL.INFO);
	return false;
}

/**
 * @param {Object} options the event that triggered the action
 * @properties={typeid:24,uuid:"D39A3411-2896-4DB4-B3E6-D2B4F1AEE9F9"}
 */
function importDocuments(options) {

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		tempFolder = serverDir.getAbsolutePath();
		var fileList = plugins.file.getRemoteFolderContents(tempFolder, '.zip');
		application.output(fileList);
		var reusultUnzip = plugins.it2be_tools.unZip(serverfile.getAbsolutePath(), true);
		application.output('resultUnzip ' + reusultUnzip);
		files = plugins.file.getFolderContents(serverDir);
		application.output(files);
		if (files) processDocuments(options);
	} else {
		if (!options['zip']) return;
		// create a temp folder
		tempFolder = plugins.file.convertToJSFile(plugins.file.getHomeFolder() + "/temp");
		if (!plugins.file.createFolder(tempFolder)) application.output("Temp folder could not be created.");

		// copy the zip to the temp folder
		if (!plugins.file.copyFile(options['zip'], tempFolder.getAbsolutePath() + '/zip.zip')) application.output("Temp file could not be created.");

		// unzip
		plugins.it2be_tools.unZip(tempFolder.getAbsolutePath() + '/zip.zip', true);

		// get the unziped files
		files = plugins.file.getFolderContents(tempFolder.getPath());

		// process files
		if (files) {

			var params = {
				processFunction: processDocuments,
				message: 'Download dei files, attendere...',
				opacity: 0.75,
				paneColor: '#000000',
				textColor: '#FFFFFF',
				showCancelButton: false,
				fontType: 'Calibri,0,48',
				processArgs: [options]
			};

			plugins.busy.block(params);
		}
	}
}

/**
 * @properties={typeid:24,uuid:"985A4AB2-E798-4187-850A-DCC6F598DA43"}
 * @AllowToRunInFind
 */
function processDocuments(options) {
	application.output(globals.messageLog + 'START processDocuments() ', LOGGINGLEVEL.INFO);
	// dismiss any remaining busy dialog
	application.updateUI();

	try {
		databaseManager.setAutoSave(false);

		// initalize a counter
		var count = 0;
		var summary = "File importati:\n\n"
		summary += "#\tfile\t\t\t";
		summary += "user\t\t\t";
		summary += "document type\n";
		summary += "--------------------------------------------------------------------\n";

		var countKo = 0;
		var summaryKo = "File NON importati:\n\n";
		summaryKo += "#\tfile\t\t\t";
		summaryKo += "document type\n";
		summaryKo += "--------------------------------------------------------------------\n";

		// loop through files array
		files.forEach(function(item) {
			/** @type {plugins.file.JSFile} */
			var file = item;

			// only work with PDFs
			if (file.isFile() && file.getName().split('.')[1] == 'pdf') {

				// get the employee enrollment number from the filem name
				var fileName = file.getName().split('.')[0].slice(-6);
				var fepCode = parseInt(fileName, 10);

				// get the user_id from the enrollment number
				/** @type {JSFoundSet<db:/geco/users>} */
				var users = databaseManager.getFoundSet('geco', 'users');
				if (users.find()) {
					users.fep_code = fepCode;

					if (users.search() != 0) {

						// write the blob to database
						/** @type {JSFoundSet<db:/geco/documents>} */
						var documents = databaseManager.getFoundSet('geco', 'documents');
						documents.newRecord(false);
						documents.document = file.getBytes();
						documents.document_size = file.size();
						documents.document_original_name = file.getName();

						// assign the file to the user
						/** @type {JSFoundSet<db:/geco/users_documents>} */
						var users_documents = databaseManager.getFoundSet('geco', 'users_documents');
						users_documents.newRecord(false);
						users_documents.user_id = users.user_id;
						users_documents.document_id = documents.document_id;
						users_documents.document_type_id = options['documentType'];
						users_documents.document_details = options['documentDetails'];
						users_documents.document_year = options['year'];
						users_documents.document_month = options['month'];

						// update the counter
						count++;

						// update the summary
						summary += count + '\t'
						summary += file.getName() + '\t'
						summary += users.users_to_contacts.real_name + '\t\t'
						summary += users_documents.users_documents_to_document_types.document_type + '\n';
					} else {
						application.output('user non trovato  ' + fepCode);
						countKo++;
						summaryKo += countKo + '\t';
						summaryKo += file.getName() + options['documentType'] + '\n';
					}
				}

			}
		});
	} catch (e) {
		application.output(globals.messageLog + 'ERROR processDocuments() Importing files failed ', LOGGINGLEVEL.INFO);
	} finally {
		databaseManager.setAutoSave(true);
		var resultCheck = checkDocImported(options);
		resultImportDocuments = count + " Files caricati con successo \n" + summary + "\n\n" + countKo + " " + summaryKo + "\n\n" + resultCheck;
		if (plugins.busy) plugins.busy.unblock();
		application.output(globals.messageLog + ' processDocuments() SUCCESS\n' + summary, LOGGINGLEVEL.INFO);
		application.output(globals.messageLog + ' processDocuments() FAIL\n' + summaryKo, LOGGINGLEVEL.INFO);
		plugins.file.deleteFolder(tempFolder, false);
	}
}

/**
 * // change user's password
 * @param {String} username
 * @param {String} oldPassword
 * @param {String} newPassword
 *
 * @properties={typeid:24,uuid:"3C46728A-9DE1-4231-B9BD-4009D9193DD9"}
 * @AllowToRunInFind
 */
function changePassword(username, oldPassword, newPassword) {
	application.output(globals.messageLog + 'START globals.changePassword() ' + username, LOGGINGLEVEL.INFO);
	//verifico nuova password
	if (newPassword != null && checkPassword(newPassword)) {
		//controlla esistenza utente
		/** @type {JSFoundSet<db:/geco/users>} */
		var users = databaseManager.getFoundSet('geco', 'users');
		// look up user
		if (users.find()) {
			users.user_name = username;
			if (oldPassword != null) users.user_password = globals.sha1(oldPassword);

			// search
			if (users.search() != 0) {

				// kick out not enabled users
				if (users.is_enabled != 1) {
					application.output(globals.messageLog + 'globals.changePassword() Utente non abilitato ', LOGGINGLEVEL.INFO);
					throw new Error("Utente non abilitato");
				}
				databaseManager.setAutoSave(false);
				var newPwd = globals.sha1(newPassword);
				/** @type {JSFoundSet<db:/geco/users_password_expiration>} */
				var userspwdex = databaseManager.getFoundSet('geco', 'users_password_expiration');
				var insertResutl = userspwdex.insertNewPassword(users.user_id, newPwd, 90, false);
				if (insertResutl == false) {
					application.output(globals.messageLog + 'globals.changePassword() Password non valida. Non puoi utilizzare le ultime 3 password ', LOGGINGLEVEL.INFO);
					throw new Error("Password non valida. Non puoi utilizzare le ultime 3 password");
				}
				users.user_password = newPwd;
				application.output(globals.messageLog + 'globals.changePassword() Password cambiata', LOGGINGLEVEL.INFO);
				databaseManager.setAutoSave(true);

				//JS CONTROLLI MP: cambiare pwd ANCHE in medical practice
				if (globals.hasMedicalPractice == true) {
					var details = {
						geco_user_id: globals.currentUserId,
						user_password: newPwd
					};

					client = plugins.headlessclient.createClient("mp_queue", null, null, null);
					if (client != null && client.isValid()) {
						application.output(globals.messageLog + 'globals.changePassword() Cient OK ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
						client.queueMethod(null, "modifyMPUserPasswordFromGeco", [details, application.getServerURL()], enqueueMPUserPasswordFromGeco);
					} else {
						application.output(globals.messageLog + 'globals.changePassword() Client is not valid ', LOGGINGLEVEL.DEBUG);
						globals.DIALOGS.showErrorDialog('Error', 'Client is not valid. Please contact the support at: geco_helpdesk@spindox.it', 'OK');
						return;
					}
				}
			} else {
				//not found
				application.output(globals.messageLog + 'globals.changePassword() ERROR: Utente o password sbagliata', LOGGINGLEVEL.INFO);
				throw new Error("Utente o password sbagliata");
			}
		}
	} else {
		application.output(globals.messageLog + 'globals.changePassword() ERROR: Nuova password non valida', LOGGINGLEVEL.INFO);
		throw new Error("Nuova password non valida");
	}
	application.output(globals.messageLog + 'STOP globals.changePassword() ' + username, LOGGINGLEVEL.INFO);
}

/**
 * check if the password contains at least one number and at least one uppercase letter
 * @param {String} password
 *
 * @properties={typeid:24,uuid:"6EAD9119-2F6A-44FF-A909-3CE09BE4C75A"}
 */
function checkPassword(password) {

	var newPassArray = password.split("");
	var hasNumber = false;
	var hasUppercase = false;

	if (password.length < 8) return false;

	newPassArray.forEach(function(character) {

		if (!isNaN(character)) {
			hasNumber = true;
		}

		if (isNaN(character) && character === character.toUpperCase()) {
			hasUppercase = true;
		}
		//application.output(character + " " + character.toUpperCase());

	});
	return (hasNumber && hasUppercase);
}

/**
 * @param {JSEvent} headlessCallback
 *
 * @properties={typeid:24,uuid:"2887A7D1-2713-4827-8311-7FE46A41365B"}
 */
function enqueueMPUserPasswordFromGeco(headlessCallback) {
	application.output(globals.messageLog + 'START globals.enqueueMPUserPasswordFromGeco() ', LOGGINGLEVEL.INFO);
	var resultMessage = '';
	var okString = '';
	var serverUrl = null;
	var solutionName = null;

	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		if (headlessCallback.data.success == false) {
			//c'è stato un errore, riporto l'errore
			resultMessage = 'Errore nella modifica della password per l\'utente ' + globals.currentUserDisplayName + ' in Pratiche Sanitarie:\n' + headlessCallback.data.message;
			application.output(globals.messageLog + 'globals.enqueueMPUserPasswordFromGeco() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			okString = globals.DIALOGS.showErrorDialog('ERROR', resultMessage, 'OK');
			//			return;
		} else {
			application.output(globals.messageLog + 'globals.enqueueMPUserPasswordFromGeco() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			okString = globals.DIALOGS.showInfoDialog('INFO', headlessCallback.data.message, 'OK');
			//			return;
		}
		if (okString == 'OK') {
			forms.change_password.close(headlessCallback);
			serverUrl = application.getServerURL();
			solutionName = application.getSolutionName();
			application.output(globals.messageLog + 'change_password.saveNewPassword() serverUrl ' + application.getServerURL(), LOGGINGLEVEL.DEBUG);
			application.showURL(serverUrl + '/servoy-webclient/application/solution/' + solutionName, '_self');
			security.logout(application.getSolutionName());
		}
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output(globals.messageLog + 'globals.enqueueMPUserPasswordFromGeco() exception callback, name: ' + headlessCallback.data + headlessCallback.data.message, LOGGINGLEVEL.DEBUG);
		//c'è stato un errore, riporto l'errore
		resultMessage = 'Errore nella modifica della password per l\'utente ' + globals.currentUserDisplayName + ' in Pratiche Sanitarie:\n' + headlessCallback.data.message;
		okString = globals.DIALOGS.showErrorDialog('ERROR', resultMessage, 'OK');
		if (okString == 'OK') {
			forms.change_password.close(headlessCallback);
			serverUrl = application.getServerURL();
			solutionName = application.getSolutionName();
			application.output(globals.messageLog + 'change_password.saveNewPassword() serverUrl ' + application.getServerURL(), LOGGINGLEVEL.DEBUG);
			application.showURL(serverUrl + '/servoy-webclient/application/solution/' + solutionName, '_self');
			security.logout(application.getSolutionName());
		}
	}
	application.output(globals.messageLog + 'users_details.saveEdits() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
	if (client != null && client.isValid()) {
		application.output(globals.messageLog + 'globals.enqueueMPUserPasswordFromGeco() close client ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
		client.shutdown();
	}
	application.output(globals.messageLog + 'STOP globals.enqueueMPUserPasswordFromGeco() ', LOGGINGLEVEL.INFO);
}

/**
 * // change user's password
 * @param {String} username
 * @param {String} oldPassword
 * @param {String} newPassword
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"10A22657-8BE0-403A-97A5-E5BE16106641"}
 */
function changeExpiredPassword(username, oldPassword, newPassword) {
	application.output(globals.messageLog + 'START globals.changeExpiredPassword() ' + username, LOGGINGLEVEL.INFO);
	//verifico nuova password
	if (newPassword != null && checkPassword(newPassword)) {
		//controlla esistenza utente
		/** @type {JSFoundSet<db:/geco/users>} */
		var users = databaseManager.getFoundSet('geco', 'users');
		// look up user
		if (users.find()) {
			users.user_name = username;
			if (oldPassword != null) users.user_password = globals.sha1(oldPassword);

			// search
			if (users.search() != 0) {

				// kick out not enabled users
				if (users.is_enabled != 1) {
					application.output(globals.messageLog + 'globals.changeExpiredPassword() Utente non abilitato ', LOGGINGLEVEL.INFO);
					throw new Error("Utente non abilitato");
				}
				databaseManager.setAutoSave(false);
				var newPwd = globals.sha1(newPassword);
				/** @type {JSFoundSet<db:/geco/users_password_expiration>} */
				var userspwdex = databaseManager.getFoundSet('geco', 'users_password_expiration');
				var insertResutl = userspwdex.insertNewPassword(users.user_id, newPwd, 90, false);
				if (insertResutl == false) {
					application.output(globals.messageLog + 'globals.changeExpiredPassword() Password non valida. Non puoi utilizzare le ultime 3 password ', LOGGINGLEVEL.INFO);
					throw new Error("Password non valida. Non puoi utilizzare le ultime 3 password");
				}
				users.user_password = newPwd;
				application.output(globals.messageLog + 'globals.changeExpiredPassword() Password cambiata', LOGGINGLEVEL.INFO);
				databaseManager.setAutoSave(true);

				//JS CONTROLLI MP: cambiare pwd ANCHE in medical practice
				if (globals.hasMedicalPractice == true) {
					var details = {
						geco_user_id: globals.currentUserId,
						user_password: newPwd
					};

					client = plugins.headlessclient.createClient("mp_queue", null, null, null);
					if (client != null && client.isValid()) {
						application.output(globals.messageLog + 'globals.changeExpiredPassword() Cient OK ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
						client.queueMethod(null, "modifyMPUserPasswordFromGeco", [details, application.getServerURL()], enqueueMPUserExpiredPasswordFromGeco);
					} else {
						application.output(globals.messageLog + 'globals.changeExpiredPassword() Client is not valid ', LOGGINGLEVEL.DEBUG);
						globals.DIALOGS.showErrorDialog('Error', 'Client is not valid. Please contact the support at: geco_helpdesk@spindox.it', 'OK');
						return;
					}
				}
			} else {
				//not found
				application.output(globals.messageLog + 'globals.changeExpiredPassword() ERROR: Utente o password sbagliata', LOGGINGLEVEL.INFO);
				throw new Error("Utente o password sbagliata");
			}
		}
	} else {
		application.output(globals.messageLog + 'globals.changeExpiredPassword() ERROR: Nuova password non valida', LOGGINGLEVEL.INFO);
		throw new Error("Nuova password non valida");
	}
	application.output(globals.messageLog + 'STOP globals.changeExpiredPassword() ' + username, LOGGINGLEVEL.INFO);
}

/**
 * @param {JSEvent} headlessCallback
 *
 * @properties={typeid:24,uuid:"C17784AA-629C-4C03-AA2D-246DC7F3C97F"}
 */
function enqueueMPUserExpiredPasswordFromGeco(headlessCallback) {
	application.output(globals.messageLog + 'START globals.enqueueMPUserPasswordFromGeco() ', LOGGINGLEVEL.INFO);
	var resultMessage = '';
	//	var serverUrl = null;
	//	var solutionName = null;
	var okStr = '';

	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		if (headlessCallback.data.success == false) {
			//c'è stato un errore, riporto l'errore
			okStr = resultMessage = 'Errore nella modifica della password per l\'utente ' + globals.currentUserDisplayName + ' in Pratiche Sanitarie:\n' + headlessCallback.data.message;
			application.output(globals.messageLog + 'globals.enqueueMPUserPasswordFromGeco() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			globals.DIALOGS.showErrorDialog('ERROR', resultMessage, 'OK');
			//			if (okStr == 'OK') {
			//				serverUrl = application.getServerURL();
			//				application.output(globals.messageLog + 'change_password.savePasswordExpired() serverUrl ' + application.getServerURL(), LOGGINGLEVEL.DEBUG);
			//				application.output(globals.messageLog + 'change_password.savePasswordExpired() solutionName ' + solutionName, LOGGINGLEVEL.DEBUG);
			//				application.showURL(serverUrl + '/servoy-webclient/application/solution/' + solutionName, '_self');
			//				security.logout(globals.solutionNameRedirectResetPwd);
			//			}
		} else {
			application.output(globals.messageLog + 'globals.enqueueMPUserPasswordFromGeco() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			//			serverUrl = application.getServerURL();
			//			application.output(globals.messageLog + 'change_password.savePasswordExpired() serverUrl ' + application.getServerURL(), LOGGINGLEVEL.DEBUG);
			//			application.output(globals.messageLog + 'change_password.savePasswordExpired() solutionName ' + solutionName, LOGGINGLEVEL.DEBUG);
			//			application.showURL(serverUrl + '/servoy-webclient/application/solution/' + solutionName, '_self');
			//			security.logout(globals.solutionNameRedirectResetPwd);
		}
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output(globals.messageLog + 'globals.enqueueMPUserPasswordFromGeco() exception callback, name: ' + headlessCallback.data + headlessCallback.data.message, LOGGINGLEVEL.DEBUG);
		//c'è stato un errore, riporto l'errore
		resultMessage = 'Errore nella modifica della password per l\'utente ' + globals.currentUserDisplayName + ' in Pratiche Sanitarie:\n' + headlessCallback.data.message;
		okStr = globals.DIALOGS.showErrorDialog('ERROR', resultMessage, 'OK');
		if (okStr == 'OK') {
			//			application.output(globals.messageLog + 'globals.enqueueMPUserPasswordFromGeco() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			//			serverUrl = application.getServerURL();
			//			application.output(globals.messageLog + 'change_password.savePasswordExpired() serverUrl ' + application.getServerURL(), LOGGINGLEVEL.DEBUG);
			//			application.output(globals.messageLog + 'change_password.savePasswordExpired() solutionName ' + solutionName, LOGGINGLEVEL.DEBUG);
			//			application.showURL(serverUrl + '/servoy-webclient/application/solution/' + solutionName, '_self');
			//			security.logout(globals.solutionNameRedirectResetPwd);
		}
	}
	application.output(globals.messageLog + 'users_details.saveEdits() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
	if (client != null && client.isValid()) {
		application.output(globals.messageLog + 'globals.enqueueMPUserPasswordFromGeco() close client ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
		client.shutdown();
	}
	application.output(globals.messageLog + 'STOP globals.enqueueMPUserPasswordFromGeco() ', LOGGINGLEVEL.INFO);
}

/**
 * @param {Number} month
 * @param {Number} day
 *
 * @properties={typeid:24,uuid:"9908185D-E039-4D42-A6C0-9AD1715E4BDF"}
 * @AllowToRunInFind
 */
function isCurrentUserHoliday(month, day) {
	/** @type {JSFoundSet<db:/geco/head_office>} */
	var head_office = databaseManager.getFoundSet('geco', 'head_office');

	head_office.loadAllRecords();
	if (currentuserid_to_users.head_office_id != null) {
		if (head_office.find()) {
			//user_id logged head office
			head_office.head_office_id = currentuserid_to_users.head_office_id;
			head_office.head_office_month_holiday = month;
			head_office.head_office_day_holiday = day;
			if (head_office.search() == 1) return true;
		}
	}
	return false;
}

/**
 * @param {Number} month
 * @param {Number} day
 * @param {Number} user_id
 *
 * @properties={typeid:24,uuid:"6EBB24F4-CA2F-4305-B26A-594628A39EAF"}
 * @AllowToRunInFind
 */
function isUserHoliday(month, day, user_id) {
	/** @type {JSFoundSet<db:/geco/head_office>} */
	var head_office = databaseManager.getFoundSet('geco', 'head_office');

	head_office.loadAllRecords();

	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	if (users.find()) {
		users.user_id = user_id;
		if (users.search() != 1) {
			application.output('Utente non trovato ' + user_id);
			return false;
		}
	}
	if (users.head_office_id != null) {
		if (head_office.find()) {
			//user_id logged head office
			head_office.head_office_id = users.head_office_id;
			head_office.head_office_month_holiday = month;
			head_office.head_office_day_holiday = day;
			if (head_office.search() == 1) return true;
		}
	}
	return false;
}

/**
 * @param {Date} dateToSearch
 *
 * @properties={typeid:24,uuid:"B8224378-F8AE-49FE-85B3-4681B22CC56B"}
 * @AllowToRunInFind
 */
function isHoliday(dateToSearch) {
	/** @type {JSFoundSet<db:/geco/calendar_days>} */
	var calendar_days = databaseManager.getFoundSet('geco', 'calendar_days');

	var data = new Date(dateToSearch.getFullYear(), dateToSearch.getMonth(), dateToSearch.getDate());
	if (calendar_days.find()) {
		//'#'+dateToSearch+'|yyyy-MM-dd';
		calendar_days.calendar_date = data;
		if (calendar_days.search() != 1) {
			application.output('data non trovata ' + data);
			return false;
		}
	}
	if (calendar_days.is_holiday != null && calendar_days.is_holiday == 1) {
		return true;
	}
	return false;
}

/**
 * @properties={typeid:35,uuid:"1E2CF2FD-B3A2-41FC-ACFC-F0494AF61C6E",variableType:-4}
 */
var client = null;

///**
// * TODO chiamato da login da eliminare quando sarà OK il metodo di login
// * @param {String} username
// * @param {String} recipient
// * @param {String} subject
// * @param {String} message
// * @return {Boolean}
// * @properties={typeid:24,uuid:"F408FC40-478C-4F53-9E83-1726FC755C67"}
// * @AllowToRunInFind
// */
//function enqueueMailPassword(username, recipient, subject, message) {
//	application.output('globals username ' + username);
//
//	/** @type {JSFoundSet<db:/geco/users>} */
//	var users = databaseManager.getFoundSet('geco', 'users');
//
//	var user = null;
//	if (users.find()) {
//		users.user_name = username
//		if (users.search() == 1) {
//			user = users.getSelectedRecord();
//
//			var text = "";
//			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
//
//			for (var i = 0; i < 8; i++) {
//				text += possible.charAt(Math.floor(Math.random() * possible.length));
//			}
//			application.output(text);
//			recipient = user.users_to_contacts.default_email.channel_value;
//			subject = 'Reset password Geco';
//			message = "Ciao " + user.users_to_contacts.first_name + "\nla tua nuova password è: " + text + "\n\nHelpdesk Geco";
//			application.output('sending mail to ' + recipient);
//		} else {
//			application.output('Utente non trovato ' + username);
//		}
//	}
//	if (client == null || !client.isValid()) {
//		application.output('client==null || !client.isValid() ricreo client');
//		client = plugins.headlessclient.createClient("geco_queue", null, null, null);
//	}
//	if (client != null && client.isValid()) {
//		application.output('Cient OK ' + client.getClientID());
//		client.queueMethod(null, "sendMailPassword", [recipient, subject, message], enqueueMailLog);
//		databaseManager.setAutoSave(false);
//		user.user_password = globals.sha1(text);
//		user.has_requested_reset = 1;
//		databaseManager.setAutoSave(true);
//		return true
//	} else {
//		application.output('Client is not valid');
//		return false
//	}
//
//}

/**
 * @param {JSEvent} headlessCallback
 * @properties={typeid:24,uuid:"D5D4B510-F1A5-4A9C-8070-9D702FB08480"}
 */
function enqueueMailLog(headlessCallback) {
	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		var messageMail = '-------------SENDING MAIL----------------------' + '\ntimestamp: ' + headlessCallback.getTimestamp() + '\nrecipient: ' + headlessCallback.data.recipient + '\nbcc: ' + headlessCallback.data.bcc + '\nsubject: ' + headlessCallback.data.subject + '\nmessage: ' + headlessCallback.data.message + '\nsuccess: ' + headlessCallback.data.success.toString().toUpperCase() + '\n-----------------------------------------------\n';
		application.output(globals.messageLog + 'globals.enqueueMailLog() ' + messageMail, LOGGINGLEVEL.DEBUG);
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output(globals.messageLog + 'globals.enqueueMailLog() exception callback, name: ' + headlessCallback.data, LOGGINGLEVEL.DEBUG);
	}
	if (client != null && client.isValid()) {
		//application.output('chiudo client ' + client.getClientID());
		//		client.shutdown();
	}
}

/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"376B2EB7-DA32-41D5-8E14-DEFDC4367C43"}
 */
function enqueueMailReminder(recipient, subject, message) {
	application.output(globals.messageLog + 'START globals.enqueueMailReminder() ', LOGGINGLEVEL.INFO);
	if (client == null || !client.isValid()) {
		application.output(globals.messageLog + 'globals.enqueueMailReminder() client create', LOGGINGLEVEL.INFO);
		client = plugins.headlessclient.createClient("geco_queue", null, null, null);
	}

	if (client != null && client.isValid()) {

		client.queueMethod(null, "sendMail", [recipient, subject, message], enqueueMailLog);
		application.output(globals.messageLog + 'STOP globals.enqueueMailReminder() Client OK ' + client.getClientID(), LOGGINGLEVEL.INFO);
		return true
	} else {
		application.output('Client is not valid');
		application.output(globals.messageLog + 'STOP globals.enqueueMailReminder() Client is not valid ', LOGGINGLEVEL.INFO);
		return false
	}
}

/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
 * @param {String} bcc
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"888FB735-F5EE-4C9E-BEF7-94956A05BA90"}
 */
function enqueueMailReminderTimesheet(recipient, subject, message, bcc) {
	application.output(globals.messageLog + 'START globals.enqueueMailReminder() ', LOGGINGLEVEL.INFO);
	if (client == null || !client.isValid()) {
		application.output(globals.messageLog + 'globals.enqueueMailReminder() client create', LOGGINGLEVEL.INFO);
		client = plugins.headlessclient.createClient("geco_queue", null, null, null);
	}

	if (client != null && client.isValid()) {

		client.queueMethod(null, "sendMailBcc", [recipient, subject, message, bcc], enqueueMailLog);
		application.output(globals.messageLog + 'STOP globals.enqueueMailReminder() Client OK ' + client.getClientID(), LOGGINGLEVEL.INFO);
		return true
	} else {
		application.output('Client is not valid');
		application.output(globals.messageLog + 'STOP globals.enqueueMailReminder() Client is not valid ', LOGGINGLEVEL.INFO);
		return false
	}
}

/**
 * @properties={typeid:35,uuid:"FE4765C0-C010-465C-BC97-DE089741D2A7",variableType:-4}
 */
var mailSubstitution = {
	nome: '',
	numEventi: 0,
	numSpese: 0,
	oreMancanti: 0,
	periodo: '',
	approvatore: '',
	tipoEvento: '',
	link: '',
	newPwd: '',
	mailText: '',
	note: '',
	oreRespinte: '',
	numEventConteggi: 0
};

/**
 * Recupera mail subject e mail text dato il tipo di mail cercata
 * @param {Number}  mailTypeId
 * @return {Object}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"9CF6CBF5-5485-4AE2-9DB0-984422A41D37"}
 */
function getMailType(mailTypeId) {
	var obj = {
		subject: null,
		text: null,
		distrList: null
	}
	/** @type {JSFoundSet<db:/geco/mail_types>} */
	var mail_types = databaseManager.getFoundSet('geco', 'mail_types');
	if (mail_types.find()) {
		mail_types.mail_type_id = mailTypeId;
		mail_types.search();
		if (mail_types.getSize() == 1) {
			obj.subject = mail_types.mail_type_subject;
			obj.text = mail_types.mail_type_text;
			obj.distrList = mail_types.mail_distribution_list;

		}
	}
	return obj;
}

/**
 * Recupera mail subject e mail text dato il tipo di mail cercata
 * @param {Number}  mailTypeId
 * @return {Object}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"92109842-BF0D-4B7D-957B-7A2FAA297F17"}
 */
function mpGetMailTypeFromGecoPersonnel(mailTypeId) {
	var obj = {
		subject: null,
		text: null,
		distrList: null
	}
	/** @type {JSFoundSet<db:/pratiche_sanitarie/mp_mail_types>} */
	var mail_types = databaseManager.getFoundSet('pratiche_sanitarie', 'mp_mail_types');
	if (mail_types.find()) {
		mail_types.mail_type_id = mailTypeId;
		mail_types.search();
		if (mail_types.getSize() == 1) {
			obj.subject = mail_types.mail_type_subject;
			obj.text = mail_types.mail_type_text;
			obj.distrList = mail_types.mail_distribution_list;

		}
	}
	return obj;
}

/**
 * @param {Number} mailTypeId
 * @param {JSFoundset<db:/geco/users>} user
 * @param {String} event
 * @param {String} period
 * @param {String} note
 *
 * @properties={typeid:24,uuid:"051A8E4D-3007-4745-A44D-3522A911986A"}
 */
function approverSendMail(mailTypeId, user, event, period, note) {
	application.output(globals.messageLog + 'START globals.approverSendMail() ', LOGGINGLEVEL.INFO);
	var objMail = globals.getMailType(mailTypeId);

	var recipient = user.users_to_contacts.default_email.channel_value;
	var subject = objMail.subject;
	mailSubstitution.nome = user.users_to_contacts.first_name;
	mailSubstitution.periodo = period;
	mailSubstitution.tipoEvento = event;
	mailSubstitution.note = note?note:'';
	mailSubstitution.approvatore = currentuserid_to_users.users_to_contacts.first_name + ' ' + globals.currentuserid_to_users.users_to_contacts.last_name;
	var message = plugins.VelocityReport.evaluateWithContext(objMail.text, globals.mailSubstitution);
	application.output(globals.messageLog + 'globals.approverSendMail() SENDING MAIL to ' + user.user_name + ', recipient: ' + recipient + ', subject: ' + subject + ', message: ' + message, LOGGINGLEVEL.INFO);

	enqueueMailReminder(recipient, subject, message);
	application.output(globals.messageLog + 'STOP globals.approverSendMail() ', LOGGINGLEVEL.INFO);
}

/**
 * @param {Number} mailTypeId
 * @param {String} jo_code
 *
 * @properties={typeid:24,uuid:"E9913454-0CBF-457B-AA19-EC4241905E90"}
 */
function sendJobOrdersIsLoggableMail(mailTypeId, jo_code) {
	application.output(globals.messageLog + 'START sendJobOrdersIsLoggableMail() ', LOGGINGLEVEL.INFO);
	var objMail = globals.getMailType(mailTypeId);

	var mailSubs = {
		commessa: ''
	};

	var recipient = objMail.distrList;
	var subject = objMail.subject;
	mailSubs.commessa = (jo_code != null) ? jo_code : '';
	var message = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubs);
	application.output(globals.messageLog + 'globals.sendNotifyMail() SENDING MAIL to distribution list  subject: ' + subject + ' message: ' + message, LOGGINGLEVEL.INFO);

	enqueueMailReminder(recipient, subject, message);
	application.output(globals.messageLog + 'STOP sendJobOrdersIsLoggableMail.sendNotifyMail() ', LOGGINGLEVEL.INFO);
}

/**
 * @AllowToRunInFind
 * @param {Object} options
 * @return {String}
 * @properties={typeid:24,uuid:"1507C40D-1CDD-47FE-95E6-EF050640759D"}
 */
function checkDocImported(options) {
	var resultCheck = "Files non trovati per :\n\n"
	resultCheck += "#\tFEP\t\t\t";
	resultCheck += "user\n";
	resultCheck += "--------------------------------------------------------------------\n";
	var fileFound = false;
	/** @type {JSFoundSet<db:/geco/users>} */
	var usersDoc = databaseManager.getFoundSet('geco', 'users');

	/** @type {JSFoundSet<db:/geco/users_documents>} */
	var users_docs = databaseManager.getFoundSet('geco', 'users_documents');

	if (usersDoc.find()) {
		usersDoc.user_type_id = '[6,7,10]';
		usersDoc.fep_code = '!-1';
		usersDoc.search();
		var totalUser = databaseManager.getFoundSetCount(usersDoc);
		application.output(totalUser);
		for (var index = 1; index <= totalUser; index++) {
			var record = usersDoc.getRecord(index);
			if (users_docs.find()) {
				users_docs.document_type_id = options['documentType'];
				users_docs.document_details = options['documentDetails'];
				users_docs.document_year = options['year'];
				users_docs.document_month = options['month'];
				users_docs.user_id = record.user_id;
				if (users_docs.search() == 0) {
					fileFound = true;
					resultCheck += record.fep_code + '\t\t\t' + record.users_to_contacts.real_name + '\n';
					application.output('file non caricato per ' + record.users_to_contacts.real_name);
				}
			}
		}
	}

	return (fileFound ? resultCheck : '');
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} year
 * @param {Number} month
 * @properties={typeid:24,uuid:"2E541906-2CA6-4EDF-8B0D-A3378C7D7C4E"}
 * @AllowToRunInFind
 * @SuppressWarnings(wrongparameters)
 */
function exportSpPresence(event, year, month) {

	var success = true;
	//	var response = globals.DIALOGS.showQuestionDialog('Conferma', 'Export per il periodo ' + month + ' ' + year + '. \n Proseguire?', 'Si', 'No');
	//	if (response == 'No') return;

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		success = processExportSpPresence(null, 'exportCedolini_', year, month);
	}
	//showFileSaveDialog non è compatibile con webclient
	if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {

		//showFileSaveDialog non è compatibile con webclient
		var file = plugins.file.showFileSaveDialog()

		if (file != null && file.exists()) {
			globals.DIALOGS.showErrorDialog("Errore", "Non puoi sovrascrivere!", 'OK');
			return;
		}

		if (file && file.createNewFile()) {
			success = processExportSpPresence(file, null, year, month);
			if (!success) {
				globals.DIALOGS.showErrorDialog("Error", "Export fallito", 'OK');
				return;
			}
		}

	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} year
 * @param {Number} month
 * @param {Number} status
 * @AllowToRunInFind
 * @SuppressWarnings(wrongparameters)
 * @properties={typeid:24,uuid:"BC7D0B55-0F1D-41B8-A3AD-6A881C87D01C"}
 */
function exportSpPresenceCheck(event, year, month, status) {
	application.output(globals.messageLog + 'START globals.exportSpPresenceCheck() ', LOGGINGLEVEL.INFO);
	var success = true;
	//	var response = globals.DIALOGS.showQuestionDialog('Conferma', 'Export per il periodo ' + month + ' ' + year + '. \n Proseguire?', 'Si', 'No');
	//	if (response == 'No') return;

	//showFileSaveDialog non è compatibile con webclient

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		success = processExportSpPresenceCheck(null, 'presenze_', year, month, status);
	}
	//showFileSaveDialog non è compatibile con webclient
	if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {

		var file = plugins.file.showFileSaveDialog()
		if (file != null && file.exists()) {
			globals.DIALOGS.showErrorDialog("Errore", "Non puoi sovrascrivere!", 'OK');
			return;
		}

		if (file && file.createNewFile()) {
			success = processExportSpPresenceCheck(file, null, year, month, status);
			if (!success) {
				globals.DIALOGS.showErrorDialog("Error", "Export fallito", 'OK');
				return;
			}
		}
		/*
		 // Create the file on disk.
		 if (file && file.createNewFile()) {
		 var params = {
		 processFunction: processExportSpPresenceCheck,
		 message: 'Operazione in corso, attendere...',
		 opacity: 0.75,
		 paneColor: '#000000',
		 textColor: '#FFFFFF',
		 showCancelButton: true,
		 cancelButtonText: 'Stop!',
		 fontType: 'Calibri,0,48',
		 processArgs: [file, year, month, status]
		 };

		 plugins.busy.block(params);

		 } else {
		 application.output("Export file not created.");
		 }
		 */
	}
	application.output(globals.messageLog + 'STOP globals.exportSpPresenceCheck() ', LOGGINGLEVEL.INFO);
}

/**
 * @AllowToRunInFind
 * @param {plugins.file.JSFile} file
 * @param {String} fileName
 * @param {Number} year
 * @param {Number} month
 * @return {Boolean}
 * @properties={typeid:24,uuid:"F978EB63-A6F8-4B42-AF91-0814EB0617D8"}
 */
function processExportSpPresence(file, fileName, year, month) {
	// dismiss any remaining busy dialog
	application.updateUI();
	var success = true;
	try {
		var maxReturnedRows = 10000; //useful to limit number of rows
		var sql_query = 'call sp_presence(?,?)';
		var arguments = new Array();
		arguments[0] = year;
		arguments [1] = month;
		var dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, maxReturnedRows)
		var line = '';
		for (var index = 1; index <= dataset.getMaxRowIndex(); index++) {
			var riga = dataset.getRowAsArray(index);
			//			application.output('riga array' + riga);
			var rigaStr = riga.join('\t');
			line = line + rigaStr + '\r\n';
		}
		//application.output(line);

		if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
			fileName = fileName + year + month;
			success = plugins.file.writeTXTFile(fileName, line, 'UTF-8');
		} else if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
			success = plugins.file.writeTXTFile(file, line, 'UTF-8');
		}

		if (!success) application.output(globals.messageLog + 'globals.processExportSpPresence() Could not write file.', LOGGINGLEVEL.INFO);
	} catch (e) {
		application.output(e);
		application.output(globals.messageLog + 'globals.processExportSpPresence() Export failed.' + e, LOGGINGLEVEL.INFO);
		success = false;
	} finally {
		databaseManager.setAutoSave(true);
		if (plugins.busy) plugins.busy.unblock();
		application.output(globals.messageLog + 'STOP globals.processExportSpPresence()', LOGGINGLEVEL.INFO);
		return success;
	}
}

/**
 * @AllowToRunInFind
 * @param {plugins.file.JSFile} file
 * @param {String} fileName
 * @param {Number} year
 * @param {Number} month
 * @param {Number} status
 * @return {Boolean}
 * @properties={typeid:24,uuid:"0C520F51-CF4A-4961-BE5D-BC57E47DAF55"}
 */
function processExportSpPresenceCheck(file, fileName, year, month, status) {
	// dismiss any remaining busy dialog
	application.updateUI();
	application.output(globals.messageLog + 'START globals.processExportSpPresenceCheck() ', LOGGINGLEVEL.INFO);
	var success = true;
	try {
		var maxReturnedRows = 10000; //useful to limit number of rows
		var sql_query = 'call sp_presence_check(?,?,?)';
		var arguments = new Array();
		arguments[0] = year;
		arguments [1] = month;
		arguments[2] = (status == null) ? 0 : status;
		var dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, maxReturnedRows)
		var line = '';
		for (var index = 1; index <= dataset.getMaxRowIndex(); index++) {
			var riga = dataset.getRowAsArray(index);
			var rigaStr = riga.join('\t');
			line = line + rigaStr + '\n';
		}

		if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
			fileName = fileName + year + month + '.xls';
			success = plugins.file.writeTXTFile(fileName, line, 'UTF-8', 'application/vnd.ms-excel');
			application.output(globals.messageLog + 'STOP globals.processExportSpPresenceCheck() : export OK', LOGGINGLEVEL.INFO);
		} else if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
			success = plugins.file.writeTXTFile(file, line, 'UTF-8');
		}

		if (!success)application.output('Could not write file.');
	} catch (e) {
		application.output(e);
		application.output(globals.messageLog + 'STOP globals.processExportSpPresenceCheck() : export fallito', LOGGINGLEVEL.INFO);
		success = false;
	} finally {
		databaseManager.setAutoSave(true);
		if (plugins.busy) plugins.busy.unblock();
		return success;
	}

}

/**
 * @param {Number} user_id
 * @param {Date} day
 * @param {Number} actual_hours
 * @return {Number}
 * @properties={typeid:24,uuid:"0036958E-4A43-406E-B02A-A0AA4C922B00"}
 * @AllowToRunInFind
 */
function getUserWorkingHoursByDay(user_id, day, actual_hours) {
	var totDays = 0;
	var total = 0;
	var tot = 0;
	var totSearch = 0;
	var working_hours = 0;
	var giorno = day.getDay();
	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	if (users.find()) {
		users.user_id = user_id;
		total = users.search();
	}
	//se trova l'utente ed il contratto non è "settimanale"
	if (total == 1 && users.users_to_contract_types.is_weekly != 1) {
		working_hours = actual_hours;
		/** @type {JSFoundSet<db:/geco/users_working_hours_history>} */
		var users_working_hours_history = databaseManager.getFoundSet('geco', 'users_working_hours_history');
		if (users_working_hours_history.find()) {
			users_working_hours_history.user_id = user_id;
			tot = users_working_hours_history.search();
		}
		// se trova dei record di history

		if (tot > 0) {
			users_working_hours_history.sort('start_change_date desc');
			//ciclo sui record trovati
			for (var i = 1; i <= tot; i++) {
				var rec_whh = users_working_hours_history.getRecord(i);
				// se la data evento è minore della data di cambio ore lavorative
				if (day < rec_whh.start_change_date) {
					if (rec_whh.previous_working_hours != 0) {
						working_hours = rec_whh.previous_working_hours;
					} else {
						/** @type {JSFoundSet<db:/geco/working_hours_week>} */
						var workHoursWeek = rec_whh.users_working_hours_history_to_previous_contract_types.contract_types_to_working_hours_week
						if (workHoursWeek.find()) {
							workHoursWeek.calendar_weekday = giorno;
							totDays = workHoursWeek.search();
						}
						if (totDays > 0) {
							working_hours = workHoursWeek.duration_work;
						} else {
							working_hours = 0;
						}
					}

				}
				// altrimenti esco dal for ho già trovato il valore
				else break;
			}
		}
		application.output(globals.messageLog + ' globals.getUserWorkingHoursByDay :ore lavorative ' + working_hours, LOGGINGLEVEL.INFO);
		return working_hours;
	} else {
		// se l'utente ha il contratto "settimanale"

		/** @type {JSFoundSet<db:/geco/working_hours_week>} */
		var working_hours_week = users.users_to_contract_types.contract_types_to_working_hours_week;
		if (working_hours_week.find()) {
			working_hours_week.calendar_weekday = giorno;
			totSearch = working_hours_week.search();
		}
		if (totSearch > 0) {
			working_hours = working_hours_week.duration_work;
		} else {
			working_hours = 0;
		}

		/** @type {JSFoundSet<db:/geco/users_working_hours_history>} */
		var working_hours_history = databaseManager.getFoundSet('geco', 'users_working_hours_history');
		if (working_hours_history.find()) {
			working_hours_history.user_id = user_id;
			tot = working_hours_history.search();
		}
		if (tot > 0) {
			working_hours_history.sort('start_change_date desc');
			//ciclo sui record trovati
			for (var index = 1; index <= tot; index++) {
				var rec_wh = working_hours_history.getRecord(index);
				// se la data evento è minore della data di cambio ore lavorative
				if (day < rec_wh.start_change_date) {
					if (rec_wh.previous_working_hours != 0) {
						working_hours = rec_wh.previous_working_hours;
					} else {
						/** @type {JSFoundSet<db:/geco/working_hours_week>} */
						var whw = rec_wh.users_working_hours_history_to_previous_contract_types.contract_types_to_working_hours_week
						if (whw.find()) {
							whw.calendar_weekday = giorno;
							totDays = whw.search();
						}
						if (totDays > 0) {
							working_hours = whw.duration_work;
						} else {
							working_hours = 0;
						}
					}
				}
				// altrimenti esco dal for ho già trovato il valore
				else break;
			}
		}
		application.output(globals.messageLog + ' globals.getUserWorkingHoursByDay :ore lavorative ' + working_hours, LOGGINGLEVEL.INFO);
		return working_hours;
	}
	//	//cerco le ore lavorative prima del cambio
	//	var tot = 0;
	//	var working_hours = actual_hours;
	//	/** @type {JSFoundSet<db:/geco/users_working_hours_history>} */
	//	var users_working_hours_history = databaseManager.getFoundSet('geco', 'users_working_hours_history');
	//	if (users_working_hours_history.find()) {
	//		users_working_hours_history.user_id = user_id;
	//		tot = users_working_hours_history.search();
	//	}
	//	// se trova dei record di history
	//
	//	if (tot > 0) {
	//		users_working_hours_history.sort('start_change_date desc');
	//		//ciclo sui record trovati
	//		for (var i = 1; i <= tot; i++) {
	//			var rec_whh = users_working_hours_history.getRecord(i);
	//			// se la data evento è minore della data di cambio ore lavorative
	//			if (day < rec_whh.start_change_date) {
	//				working_hours = rec_whh.previous_working_hours;
	//			}
	//			// altrimenti esco dal for ho già trovato il valore
	//			else break;
	//		}
	//	}
	//	return working_hours;
	//

}

/**
 * @param {String} username
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"82057AE5-E1BD-4AA1-9340-F089AF99A138"}
 */
function enqueueMailResetPassword(username) {
	if (client == null || !client.isValid()) {
		application.output('client==null || !client.isValid() ricreo client');
		client = plugins.headlessclient.createClient("geco_queue", null, null, null);
	}

	if (client != null && client.isValid()) {
		application.output('Cient OK ' + client.getClientID());
		client.queueMethod(null, "sendMailResetPassword", [username], enqueueMailLog);
		return true
	} else {
		application.output('Client is not valid');
		return false
	}
}

/**
 * @param {String} url
 * @return {String}
 * @properties={typeid:24,uuid:"5E458139-5854-4D11-9C9C-690C600FD4D6"}
 */
function getServerUrlGeco(url) {
	//http://localhost:8080
	//	application.output('primo ' + url.indexOf(':'));
	//	application.output('secondo ' + url.lastIndexOf(':'));
	//	application.output('ultimo index https: ' + url.indexOf('https'));
	if (url.indexOf('https') == -1 || url.indexOf(':') != url.lastIndexOf(':')) {
		//		application.output('index last : ' + url.lastIndexOf(':'));
		//		application.output('lunghezza ' + url.length);
		if (url != 'https://213.254.15.84:8443') url = url.substr(0, (url.lastIndexOf(':')));
		application.output('url finale ' + url);
	}
	return url;
}

/**
 *  @param {JSEvent} event the event that triggered the action
 *  @AllowToRunInFind
 *  @properties={typeid:24,uuid:"FDDB900A-6A1D-4BAA-858B-F76BD1D54701"}
 */
function reportUsers(event) {
	var success = null;
	/** @type {JSFoundSet<db:/geco/users>} */
	var user = databaseManager.getFoundSet('geco', 'users');

	user.loadAllRecords();
	//application.output(user);
	/** @type {String} */
	var totalLines = '';
	var columnLine = '';

	columnLine = 'Nominativo\t' + 'Codice_Navision\t' + 'Codice_FEP\t' + 'Centro_profitto\t' + 'Data_assunzione\t' + 'Data_dimissione\t' + 'Abilitato\n';
	/** @type {Number} */
	var count = databaseManager.getFoundSetCount(user);
	for (var index = 1; index <= count; index++) {
		user.setSelectedIndex(index);
		/** @type {JSRecord<db:/geco/users>} */
		var rec = user.getRecord(index);
		var nome = rec.users_to_contacts.real_name;
		var profCent = rec.users_to_profit_centers.profit_center_name;
		var startDate = '' + rec.enrollment_start_date.getDate() + '/' + (rec.enrollment_start_date.getMonth() + 1) + '/' + rec.enrollment_start_date.getFullYear();
		if (rec.enrollment_end_date && rec.enrollment_end_date != null) {
			var endDate = '' + rec.enrollment_end_date.getDate() + '/' + (rec.enrollment_end_date.getMonth() + 1) + '/' + rec.enrollment_end_date.getFullYear();
		} else endDate = '';
		var enabled = '';
		if (rec.is_enabled == 1) {
			enabled = 'SI';
		} else enabled = 'NO';
		var line = nome + '\t' + rec.enrollment_number + '\t' + rec.fep_code + '\t' + profCent + '\t' + startDate + '\t' + endDate + '\t' + enabled;
		totalLines = line + '\n';
		columnLine = columnLine + totalLines;

	}

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		var fileName = 'reportUtenti.xls';
		success = plugins.file.writeTXTFile(fileName, columnLine, 'UTF-8', 'application/vnd.ms-excel');
	}
	if (!success) application.output('Scrittura non riuscita');
}

/**
 *  @param {JSEvent} event the event that triggered the action
 *  @AllowToRunInFind
 *  @properties={typeid:24,uuid:"5A82E33B-B16C-4F2F-95F7-BAB553D3A3EB"}
 */
function reportPCManagers(event) {
	var success = null;
	/** @type {JSFoundSet<db:/geco/profit_centers>} */
	var profitCenters = databaseManager.getFoundSet('geco', 'profit_centers');
	profitCenters.sort('profit_center_name asc')
	profitCenters.loadAllRecords();
	//application.output(profitCenters);
	/** @type {String} */
	var totalLines = '';
	var columnLine = '';

	columnLine = 'Centro_profitto\t' + 'Responsabile\n';
	/** @type {Number} */
	var count = profitCenters.getSize();
	for (var index = 1; index <= count; index++) {

		/** @type {JSRecord<db:/geco/profit_centers>} */
		var rec = profitCenters.getRecord(index);
		var nome = rec.profit_centers_to_owner_users.users_to_contacts.real_name;

		var line = rec.profit_center_name + '\t' + nome;
		totalLines = line + '\n';
		columnLine = columnLine + totalLines;
	}

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		var fileName = 'reportCentriProfitto.xls';
		success = plugins.file.writeTXTFile(fileName, columnLine, 'UTF-8', 'application/vnd.ms-excel');
	}
	if (!success) application.output('Scrittura non riuscita');
}

/**
 *  @param {JSEvent} event the event that triggered the action
 *  @AllowToRunInFind
 *  @properties={typeid:24,uuid:"6C0195AC-FADB-4A9F-A290-14A4964639BB"}
 */
function reportEvents(event) {
	var success = null;
	/** @type {JSFoundSet<db:/geco/events>} */
	var events = databaseManager.getFoundSet('geco', 'events');
	events.sort('event_name asc')
	events.loadAllRecords();
	//application.output(events);
	/** @type {String} */
	var totalLines = '';
	var columnLine = '';

	columnLine = 'Nome_evento\t' + 'Conteggio\t' + 'Su_commessa\t' + 'Figura_approvatore\t' + 'Su_richiesta\t' + 'Abilitato\t' + 'Centro_profitto_approvatore\t' + 'Codice_Navision\t' + 'Codice_Fep\n';
	/** @type {Number} */
	var count = events.getSize();
	for (var index = 1; index <= count; index++) {
		events.setSelectedIndex(index);
		/** @type {JSRecord<db:/geco/events>} */
		var rec = events.getRecord(index);
		var nome = rec.event_name;
		var conteggio = '';
		if (rec.unit_measure == 1) {
			conteggio = 'Ad unita\'';
		} else if (rec.unit_measure == 2) conteggio = 'Ad ore';
		var hasJob = '';
		if (rec.has_job_order == 1) {
			hasJob = 'SI';
		} else hasJob = 'NO';
		var approvFig = '';
		switch (rec.approver_figure) {
		case 1:
			approvFig = 'Centro profitto'
			break
		case 2:
			approvFig = 'Responsabile dipendente'
			break
		case 3:
			approvFig = 'Responsabile commessa'
			break
		}
		var hasRequest = '';
		if (rec.is_requestable == 1) {
			hasRequest = 'SI';
		} else hasRequest = 'NO';

		var enabled = '';
		if (rec.is_enabled == 1) {
			enabled = 'SI';
		} else enabled = 'NO';
		if (rec.approver_profit_center_id && rec.approver_profit_center_id != null) {
			var profCentName = rec.events_to_profit_centers_approver.profit_center_name;
		} else profCentName = '';
		if (rec.external_code_navision && rec.external_code_navision != null) {
			var extCode = rec.external_code_navision;
		} else extCode = '';
		var codeFep = rec.external_code_fep;
		var line = nome + '\t' + conteggio + '\t' + hasJob + '\t' + approvFig + '\t' + hasRequest + '\t' + enabled + '\t' + profCentName + '\t' + extCode + '\t' + codeFep;
		totalLines = line + '\n';
		columnLine = columnLine + totalLines;

	}

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		var fileName = 'reportEventi.xls';
		success = plugins.file.writeTXTFile(fileName, columnLine, 'UTF-8', 'application/vnd.ms-excel');
	}
	if (!success) application.output('Scrittura non riuscita');
}

/**
 *  @param {JSEvent} event the event that triggered the action
 *  @AllowToRunInFind
 *  @properties={typeid:24,uuid:"79435F9F-40E2-4B6C-824A-A1DD7179BA40"}
 */
function reportJobOrders(event) {
	var success = null;
	/** @type {JSFoundSet<db:/geco/job_orders>} */
	var job_orders = databaseManager.getFoundSet('geco', 'job_orders');
	job_orders.loadAllRecords();
	//application.output(job_orders);
	/** @type {String} */
	var totalLines = '';
	var columnLine = '';

	columnLine = 'Codice\t' + 'Titolo\t' + 'Responsabile\t' + 'Centro_profitto\t' + 'Abilitata\t' + 'Da_chiudere\t' + 'Utilizzabile\n';
	/** @type {Number} */
	var count = databaseManager.getFoundSetCount(job_orders);
	for (var index = 1; index <= count; index++) {
		job_orders.setSelectedIndex(index);
		/** @type {JSRecord<db:/geco/job_orders>} */
		var rec = job_orders.getRecord(index);
		var code = '"' + rec.external_code_navision + '"';
		var extCode = code.replace(/\n/g, '');
		var title = '"' + rec.job_order_title + '"';
		var titolo = title.replace(/\n/g, '');
		var owner = rec.job_orders_owner_to_users.users_to_contacts.real_name;
		var profCent = rec.job_orders_to_profit_centers.profit_center_name;

		var enabled = '';
		if (rec.is_enabled == 1) {
			enabled = 'SI';
		} else enabled = 'NO';
		var toClose = '';
		if (rec.is_to_close == 1) {
			toClose = 'SI';
		} else toClose = 'NO';
		var loggable = '';
		if (rec.is_loggable == 1) {
			loggable = 'SI';
		} else loggable = 'NO';
		var line = extCode + '\t' + titolo + '\t' + owner + '\t' + profCent + '\t' + enabled + '\t' + toClose + '\t' + loggable;
		totalLines = line + '\r\n';
		columnLine = columnLine + totalLines;

	}

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		var fileName = 'reportCommesse.xls';
		success = plugins.file.writeTXTFile(fileName, columnLine, 'UTF-8', 'application/vnd.ms-excel');
	}
	if (!success) application.output('Scrittura non riuscita');
}

/**
 *  @param {JSEvent} event the event that triggered the action
 *  @AllowToRunInFind
 *  @properties={typeid:24,uuid:"5F5A02DA-95B3-4A7D-8AF2-C2A25DBEFD3D"}
 */
function reportCompanies(event) {
	var success = null;
	/** @type {JSFoundSet<db:/geco/job_orders>} */
	var companies = databaseManager.getFoundSet('geco', 'companies');
	companies.loadAllRecords();
	//application.output(job_orders);
	/** @type {String} */
	var totalLines = '';
	var columnLine = '';

	columnLine = 'Nome\t' + 'Codice\t' + 'Città\t' + 'Abilitato\t' + 'Tipo\n';
	/** @type {Number} */
	var count = databaseManager.getFoundSetCount(companies);
	for (var index = 1; index <= count; index++) {
		companies.setSelectedIndex(index);
		/** @type {JSRecord<db:/geco/companies>} */
		var rec = companies.getRecord(index);
		var name = rec.company_name;
		var code = '';
		if (rec.external_code == null) {
			code = '';
		} else
			code = rec.external_code;
		var city = '';
		if (rec.city == null) {
			city = '';
		} else
			city = rec.city;

		var enabled = '';
		if (rec.is_enabled == 1) {
			enabled = 'SI';
		} else enabled = 'NO';
		var type = '';
		switch (rec.company_type_id) {
		case 1:
			type = 'Fornitore'
			break
		case 2:
			type = 'Cliente'
			break
		case 3:
			type = 'Cliente - Fornitore'
			break
		}

		var line = name + '\t' + code + '\t' + city + '\t' + enabled + '\t' + type;
		totalLines = line + '\r\n';
		columnLine = columnLine + totalLines;

	}

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		var fileName = 'reportClienti.xls';
		success = plugins.file.writeTXTFile(fileName, columnLine, 'UTF-8', 'application/vnd.ms-excel');
	}
	if (!success) application.output('Scrittura non riuscita');
}

/**
 * @param {Number} user_id
 * @param {Date} day
 *
 * @properties={typeid:24,uuid:"11ED2E41-9C37-4D4E-B941-B6C05695A5A3"}
 * @AllowToRunInFind
 */
function getUserTimeWorkByDay(user_id, day) {
	//application.output('globals.getUserTimeWorkByDay() FINE');
	//application.output(day  +' '+day.getDay());
	var totDays = 0;
	var totHistory = 0;
	var totalUser = 0;
	var giorno = day.getDay();
	var objTimeUser = {
		startTime: 0,
		stopTime: 0,
		breakStart: 0,
		breakStop: 0,
		workingHours: 0,
		contractId: 0,
		isShiftWorker: 0,
		shiftStartTime: 0,
		shiftStopTime: 0
	}
	/** @type {JSFoundSet<db:/geco/working_hours_week>} */
	var workHoursWeek = null;

	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	if (users.find()) {
		users.user_id = user_id;
		totalUser = users.search();
	}

	if (totalUser == 1) {
		//application.output('globals.getUserTimeWorkByDay() trovato user');
		objTimeUser.breakStop = users.break_stop;
		objTimeUser.breakStart = users.break_start;
		objTimeUser.startTime = users.shift_start;
		objTimeUser.stopTime = users.shift_stop;
		objTimeUser.workingHours = users.working_hours;
		objTimeUser.contractId = users.contract_type_id;
		//application.output('globals.getUserTimeWorkByDay() attuale contratto ' + objTimeUser.contractId);
		if (users.users_to_contract_types && users.users_to_contract_types.is_weekly == 1) {
			//application.output('globals.getUserTimeWorkByDay() attuale contratto verticale cerco giorno ' + giorno);
			//cerco il giorno della settimana e i corrispettivi orari
			workHoursWeek = users.users_to_contract_types.contract_types_to_working_hours_week;
			if (workHoursWeek.find()) {
				workHoursWeek.calendar_weekday = giorno;
				totDays = workHoursWeek.search();
			}
			if (totDays > 0) {
				//application.output('globals.getUserTimeWorkByDay() trovato giorno ' +giorno);
				objTimeUser.breakStop = workHoursWeek.break_stop;
				objTimeUser.breakStart = workHoursWeek.break_start;
				objTimeUser.startTime = workHoursWeek.time_start;
				objTimeUser.stopTime = workHoursWeek.time_stop;
				objTimeUser.workingHours = workHoursWeek.duration_work;
			} else {
				//se per quel gg non è previsto lavoro da contratto
				//application.output('globals.getUserTimeWorkByDay() NON trovato giorno ' +giorno);
				objTimeUser.breakStop = 0;
				objTimeUser.breakStart = 0;
				objTimeUser.startTime = 0;
				objTimeUser.stopTime = 0;
				objTimeUser.workingHours = 0;
			}
		}
		//cerco se c'è un history di cambio orario lavorativo
		/** @type {JSFoundSet<db:/geco/users_working_hours_history>} */
		var users_working_hours_history = databaseManager.getFoundSet('geco', 'users_working_hours_history');
		//application.output('globals.getUserTimeWorkByDay() cerco history ');
		if (users_working_hours_history.find()) {
			users_working_hours_history.user_id = user_id;
			totHistory = users_working_hours_history.search();
		}
		//se ha trovato history cerco se il girono è prima del cambio
		for (var i = 1; i <= totHistory; i++) {
			users_working_hours_history.sort('start_change_date desc');
			//application.output('globals.getUserTimeWorkByDay() trovata history ');
			var rec_whh = users_working_hours_history.getRecord(i);
			if (day < rec_whh.start_change_date) {
				//application.output('globals.getUserTimeWorkByDay() giorno minore del cambio data ' + rec_whh.start_change_date);
				//se il giorno è prima del cambio orario lavorativo
				//controllo il tipo di contratto se part time erticale o no
				if (rec_whh.users_working_hours_history_to_previous_contract_types.is_weekly) {
					//application.output('globals.getUserTimeWorkByDay() contratto precedente parti time vert');
					//se part time verticale
					//cerco l'orario giornaliero del part time verticale
					workHoursWeek = rec_whh.users_working_hours_history_to_previous_contract_types.contract_types_to_working_hours_week;
					totDays = 0;
					if (workHoursWeek.find()) {
						workHoursWeek.calendar_weekday = giorno;
						totDays = workHoursWeek.search();
					}
					if (totDays > 0) {
						objTimeUser.breakStop = workHoursWeek.break_stop;
						objTimeUser.breakStart = workHoursWeek.break_start;
						objTimeUser.startTime = workHoursWeek.time_start;
						objTimeUser.stopTime = workHoursWeek.time_stop;
						objTimeUser.workingHours = workHoursWeek.duration_work;

					} else {
						//se per quel gg non è previsto lavoro da contratto
						objTimeUser.breakStop = 0;
						objTimeUser.breakStart = 0;
						objTimeUser.startTime = 0;
						objTimeUser.stopTime = 0;
						objTimeUser.workingHours = 0;
					}
					objTimeUser.contractId = rec_whh.previous_contract_type;
				} else {
					//application.output('globals.getUserTimeWorkByDay() contratto precedente NO parti time vert');
					// il contratto precedente non è part time verticale
					objTimeUser.breakStop = rec_whh.previous_stop_break;
					objTimeUser.breakStart = rec_whh.previous_start_break;
					objTimeUser.startTime = rec_whh.previous_start_time;
					objTimeUser.stopTime = rec_whh.previous_stop_time;
					objTimeUser.workingHours = rec_whh.previous_working_hours;
					objTimeUser.contractId = rec_whh.previous_contract_type;
				}
			} else {
				//application.output('globals.getUserTimeWorkByDay() giorno maggiore del cambio contratto');
			}
		}
		if (users.is_shift_worker == 1) {
			objTimeUser.isShiftWorker = users.is_shift_worker;
			objTimeUser.shiftStartTime = users.users_to_shift_work_type.shift_start;
			objTimeUser.shiftStopTime = users.users_to_shift_work_type.shift_stop;
		}

	} else {
		application.output('globals.getUserTimeWorkByDay() utente non trovato o più utenti con lo stesso ID');
	}
	//application.output('globals.getUserTimeWorkByDay() trovato contratto ' + objTimeUser.contractId + ' ' + objTimeUser.startTime + ' ' + objTimeUser.stopTime + ' ' + objTimeUser.breakStart + ' ' + objTimeUser.breakStop + ' ' + objTimeUser.workingHours);
	//application.output('globals.getUserTimeWorkByDay() FINE');
	return objTimeUser;
}

/**
 * Ritorna la lista di tutti i PC di cui l'utente è responsabile o vice e se ha solo il ruolo di supporto commerciale solo quelli di cui è supporto
 * @AllowToRunInFind
 * @param {Number} userId
 * @return {Number[]}
 * @properties={typeid:24,uuid:"760DD92C-8AD0-43F9-9BC4-8EB371DC4DF8"}
 */
function getProfitCenterByUserManager(userId) {
	var listPC = [];
	/** @type {JSFoundSet<db:/geco/profit_centers>} */
	var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');
	//	if (profit_centers.find()) {
	//		profit_centers.user_owner_id = userId;
	//		if (profit_centers.search() > 0) {
	//			for (var index = 1; index <= profit_centers.getSize(); index++) {
	//				var rec = profit_centers.getRecord(index);
	//				listPC.push(rec.profit_center_id);
	//			}
	//		}
	//	}
	if (profit_centers.find()) {
		if (scopes.globals.hasRole('Supp. Commerciale') && !globals.hasRole('Resp. Mercato') && !globals.hasRole('Vice Resp. Mercato')) {
			profit_centers.profit_centers_to_comm_support.user_id = userId;
		} else {
			//responsabile o vice responsabile
			profit_centers.profit_centers_to_owners_markets.user_id = userId;
			profit_centers.newRecord();
			profit_centers.profit_centers_to_vice_owner.user_id = userId;
		}
		if (profit_centers.search() > 0) {
			profit_centers.sort('profit_center_type_id desc, profit_center_name asc');
			for (var index = 1; index <= profit_centers.getSize(); index++) {
				var rec = profit_centers.getRecord(index);
				listPC.push(rec.profit_center_id);
			}
		}
	}
	application.output(listPC)
	return listPC;
}

/**
 * Ritorna la lista dei mercati di cui l'utente è responsabile
 * @AllowToRunInFind
 * @param {Number} userId
 * @return {Number[]}
 * @properties={typeid:24,uuid:"3C362152-0391-4723-89EE-1D539ECE4E8B"}
 */
function getProfitCenterByUserManagerForMarket(userId) {
	var listPC = [];
	/** @type {JSFoundSet<db:/geco/profit_centers>} */
	var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');
	//	if (profit_centers.find()) {
	//		profit_centers.user_owner_id = userId;
	//		if (profit_centers.search() > 0) {
	//			for (var index = 1; index <= profit_centers.getSize(); index++) {
	//				var rec = profit_centers.getRecord(index);
	//				listPC.push(rec.profit_center_id);
	//			}
	//		}
	//	}
	if (profit_centers.find()) {
		profit_centers.profit_centers_to_owners_markets.user_id = userId;
		profit_centers.profit_centers_to_profit_center_types.profit_center_types_id = 5;
		if (profit_centers.search() > 0) {
			profit_centers.sort('profit_center_type_id desc, profit_center_name asc')
			for (var index = 1; index <= profit_centers.getSize(); index++) {
				var rec = profit_centers.getRecord(index);
				listPC.push(rec.profit_center_id);
			}
		}
	}
	return listPC;
}

/**
 * Ritorna la lista dei mercati di cui l'utente è responsabile o vice
 * @AllowToRunInFind
 * @param {Number} userId
 * @return {Number[]}
 * @properties={typeid:24,uuid:"93C86963-AE2F-45E1-B387-A028D69D7EAF"}
 */
function getPCOwnerViceForMarket(userId) {
	var listPC = [];
	/** @type {JSFoundSet<db:/geco/profit_centers>} */
	var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');
	if (profit_centers.find()) {
		profit_centers.profit_centers_to_owners_markets.user_id = userId;
		profit_centers.profit_centers_to_profit_center_types.profit_center_types_id = 5;
		profit_centers.is_to_check_market_competence = 1;
		profit_centers.newRecord();
		profit_centers.profit_centers_to_vice_owner.user_id = userId;
		profit_centers.profit_centers_to_profit_center_types.profit_center_types_id = 5;
		profit_centers.is_to_check_market_competence = 1;
		if (profit_centers.search() > 0) {
			profit_centers.sort('profit_center_type_id desc, profit_center_name asc')
			for (var index = 1; index <= profit_centers.getSize(); index++) {
				var rec = profit_centers.getRecord(index);
				listPC.push(rec.profit_center_id);
			}
		}
	}
	return listPC;
}

/**
 * @param{String} str
 * @return {String}
 *
 * @properties={typeid:24,uuid:"A98E7869-94AF-4449-B221-6E028995C41D"}
 * @AllowToRunInFind
 */
function trimString(str) {
	str = str.replace(/^\s+|\s+$/g, '');
	return str;
}
/**
 * @AllowToRunInFind
 * @param {Number} userId
 * @return {Number[]}
 * @properties={typeid:24,uuid:"B449E720-F9B5-4607-B2E4-D388051FA9A6"}
 */
function getJobOrderByUserManager(userId) {
	var listJO = [];
	/** @type {JSFoundSet<db:/geco/job_orders>} */
	var job_orders = databaseManager.getFoundSet('geco', 'job_orders');
	if (job_orders.find()) {
		job_orders.user_owner_id = userId;
		if (job_orders.search() > 0) {
			for (var index = 1; index <= job_orders.getSize(); index++) {
				var rec = job_orders.getRecord(index);
				listJO.push(rec.job_order_id);
			}
		}
	}
	return listJO;
}

/**
 * @param {Number} boId
 * @return {Boolean}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"E055A8C3-4469-4C21-A5D6-17596D405150"}
 */
function hasBusinessCase(boId) {
	/** @type {JSFoundSet<db:/geco/business_opportunities>} */
	var business_opportunities = databaseManager.getFoundSet('geco', 'business_opportunities');
	var totBo = 0;

	if (business_opportunities.find()) {
		business_opportunities.bo_id = boId;
		totBo = business_opportunities.search()
	}
	if (totBo > 0 && business_opportunities.bo_to_bo_details != null) {
		if (business_opportunities.bo_to_bo_details.find()) {
			business_opportunities.bo_to_bo_details.bo_details_to_profit_cost_types.profit_cost_acr = 'R';
			if (business_opportunities.bo_to_bo_details.search() > 0) {
				business_opportunities.bo_to_bo_details.loadRecords();
				if (business_opportunities.bo_to_bo_details.find()) {
					business_opportunities.bo_to_bo_details.bo_details_to_profit_cost_types.profit_cost_acr = 'C';
					if (business_opportunities.bo_to_bo_details.search() > 0) return true
				}
			}
		}
	}
	return false;
}

/**
 * @param {Number} boId
 * @return {Boolean}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"5667726C-A3B1-442C-815D-DE38CE1E1F43"}
 */
function hasBusinessCaseProfit(boId) {
	/** @type {JSFoundSet<db:/geco/business_opportunities>} */
	var business_opportunities = databaseManager.getFoundSet('geco', 'business_opportunities');
	var totBo = 0;
	if (business_opportunities.find()) {
		business_opportunities.bo_id = boId;
		totBo = business_opportunities.search()
	}
	if (totBo > 0 && business_opportunities.bo_to_bo_details != null) {
		if (business_opportunities.bo_to_bo_details.find()) {
			business_opportunities.bo_to_bo_details.bo_details_to_profit_cost_types.profit_cost_acr = 'R';
			if (business_opportunities.bo_to_bo_details.search() > 0) return true
		}
	}
	return false;
}

/**
 * @param {JSRecord<db:/geco/jo_details>} joDetail
 * @return {Number}
 * @properties={typeid:24,uuid:"A94302FE-525C-41C3-97F5-E20933BCE3E7"}
 * @AllowToRunInFind
 */
function getBoForJoDetails(joDetail) {
	/** @type {JSFoundSet<db:/geco/business_opportunities>} */
	var business_opportunities = databaseManager.getFoundSet('geco', 'business_opportunities');
	if (business_opportunities.find()) {
		business_opportunities.job_order_id = joDetail.jo_id;
		business_opportunities.search();
	}
	if (business_opportunities.getSize() == 1) {
		return business_opportunities.bo_id;
	}
	business_opportunities.sort('bo_id desc');
	var boId = null;
	for (var index = 1; index <= business_opportunities.getSize(); index++) {
		var recBO = business_opportunities.getRecord(index);
		if (recBO.end_date != null && recBO.end_date >= globals.actualDate) {
			if (recBO.start_date != null && recBO.start_date <= globals.actualDate) {
				boId = recBO.bo_id;
			}
		}
	}
	return boId;
}

/**
 * arrotonda il valore in input col num di decimali in input
 * @param {Number} value
 * @param {Number} num
 * @return {Number}
 * @properties={typeid:24,uuid:"55BCCBA1-4EA5-40BC-A6D6-28F19B145BB2"}
 */
function roundNumberWithDecimal(value, num) {
	return Math.round( (value) * Math.pow(10, num)) / Math.pow(10, num);
}

/**
 * @param {Number} monthNumber
 *
 * @properties={typeid:24,uuid:"695C20CF-F197-444E-B7BD-2ECF0314A38F"}
 * @AllowToRunInFind
 */
function getMonthName(monthNumber) {
	/** @type {JSFoundSet<db:/geco/calendar_months>} */
	var calendar_months = databaseManager.getFoundSet('geco', 'calendar_months');
	if (calendar_months.find()) {
		calendar_months.month_number = monthNumber;
		calendar_months.search();
	}
	return calendar_months.month_name;
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} boId
 * @return {String}
 *
 * @properties={typeid:24,uuid:"72EAB079-9B44-4DB0-98D2-A93634F8C1D7"}
 * @AllowToRunInFind
 */
function getBusinessCase(event, boId) {
	application.output(globals.messageLog + 'START globals.getBusinessCase() codice ' + boId, LOGGINGLEVEL.DEBUG);
	//application.output('getBusinessCase codice ' + foundset.bo_code);

	// prepare the report the way you usually do (putting any Servoy object in a context):
	var context = new Object();
	var totDetails = 0;
	/** @type {JSFoundSet<db:/geco/business_opportunities>} */
	var foundset = databaseManager.getFoundSet('geco', 'business_opportunities');
	if (foundset.find()) {
		foundset.bo_id = boId;
		foundset.search();
	}
	context.foundsetSintesi = foundset;
	/** @type {JSFoundSet<db:/geco/bo_details>} */
	var bo_details = databaseManager.getFoundSet('geco', 'bo_details');
	if (bo_details.find()) {
		bo_details.bo_id = foundset.bo_id;
		totDetails = bo_details.search();
	}
	//	application.output('getBusinessCase dettagli trovati ' + totDetails)
	//	application.output('getBusinessCase ' + bo_details)
	if (totDetails > 0) {
		bo_details.bo_details_to_bo_planning.loadAllRecords();
	}
	bo_details.sort('bo_details_to_profit_cost_types.profit_cost_acr desc ');
	//	for (var index = 1; index <= bo_details.getSize(); index++) {
	//		var recoDet = bo_details.getRecord(index);
	//		//application.output(recoDet.bo_details_id);
	//		recoDet.bo_details_to_bo_planning.loadAllRecords();
	////		for (var i = 1; i <= recoDet.bo_details_to_bo_planning.getSize(); i++) {
	////			var reco = recoDet.bo_details_to_bo_planning.getRecord(i);
	////			application.output('plID ' + reco.bo_planning_id + ' ' + reco.bo_year + '-' + reco.bo_month + ' total ' + reco.total_amount + ' ' + reco.bo_planning_to_bo_details.bo_details_to_profit_cost_types.profit_cost_description);
	////		}
	//
	//	}
	context.foundsetDati = bo_details.getSize() > 0 ? bo_details : null;
	//application.output('getBusinessCase ****************\n' + context.foundsetDati)

	context.ricavoTotale = bo_details.total_return;
	//application.output('getBusinessCase ----------------------' + context.ricavoTotale)
	context.costoTotale = bo_details.total_cost;
	context.mol = bo_details.mol_dispaly ? bo_details.mol_dispaly : 'NA';
	context.molPer = bo_details.mol_per_display ? bo_details.mol_per_display : 'NA';
	context.myFormat = '#,00';
	plugins.VelocityReport.installFonts();

	// we use a template that should be located in the default velocity reports folder:
	//excelTemplateBO.xml
	var template = 'excelTemplateBO.xml';

	// now let's render the report into a String:
	var excel = plugins.VelocityReport.renderTemplate(template, context);

	//	application.output(excel);

	application.output(globals.messageLog + 'STOP globals.getBusinessCase() ', LOGGINGLEVEL.DEBUG);
	return excel;

}

/**
 * @param {JSEvent} event
 * @param {Number} boId
 * @param {plugins.file.JSFile} fileExcel
 *
 * @properties={typeid:24,uuid:"9C9EDCF4-5FB3-479F-BEDC-BE8018987005"}
 */
function insertBCtoBO(event, boId, fileExcel) {
	application.output(messageLog + ' START globals.InsertBCtoBO()', LOGGINGLEVEL.INFO);

	var fileName = null;
	/**@type {Array<byte>}*/
	var bytes = new Array();
	application.output(fileExcel.getBytes());
	bytes = fileExcel.getBytes();
	application.output(fileExcel.size());
	application.output(fileExcel.getContentType());

	if (!plugins.file.writeFile(fileExcel, bytes, 'application/vnd.ms-excel'))
		application.output('Failed to write the file.');
	application.output(fileExcel.getAbsolutePath());
	fileName = fileExcel.getName();
	/** @type {JSFoundSet<db:/geco/bo_documents>} */
	var documents = databaseManager.getFoundSet('geco', 'bo_documents');
	documents.newRecord(false);
	documents.bo_document = bytes;
	documents.bo_document_size = fileExcel.size();
	documents.bo_document_original_name = fileName;
	documents.bo_document_mimetype = 'xls';
	//documents.bo_document_mimetype = fileSaved.getContentType();
	documents.bo_document_type = 1;
	var resultDoc = databaseManager.saveData(documents);

	if (resultDoc) {
		/** @type {JSFoundSet<db:/geco/bo_bo_documents>} */
		var bo_bo_documents = databaseManager.getFoundSet('geco', 'bo_bo_documents');
		bo_bo_documents.newRecord(false);
		bo_bo_documents.bo_id = boId;
		bo_bo_documents.bo_document_id = documents.bo_document_id;
		bo_bo_documents.bo_bo_documents_details = 'Richiesta commessa';
		bo_bo_documents.is_eraseable = 0;
		databaseManager.saveData(bo_bo_documents);
	}
	application.output(messageLog + ' STOP globals.InsertBCtoBO()', LOGGINGLEVEL.INFO)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} boId
 * @return {String}
 *
 * @properties={typeid:24,uuid:"CFEE4741-AB42-4883-AE63-ED0BC9FF6BE7"}
 * @AllowToRunInFind
 */
function getBusinessCaseBOLink(event, boId) {
	application.output(globals.messageLog + 'START globals.getBusinessCaseBOLink() codice ' + boId, LOGGINGLEVEL.DEBUG);
	//application.output('getBusinessCase codice ' + foundset.bo_code);

	// prepare the report the way you usually do (putting any Servoy object in a context):
	var context = new Object();

	/** @type {JSFoundSet<db:/geco/business_opportunities>} */
	var foundset = databaseManager.getFoundSet('geco', 'business_opportunities');
	if (foundset.find()) {
		foundset.bo_id = boId;
		foundset.search();
	}
	//context.foundsetSintesi = foundset;
	/** @type {JSFoundSet<db:/geco/job_orders>} */
	var job_orders = databaseManager.getFoundSet('geco', 'job_orders');
	if (job_orders.find()) {
		job_orders.job_order_id = foundset.job_order_id;
		job_orders.search();
	}

	context.foundsetDati = null;
	//application.output('getBusinessCase ****************\n' + context.foundsetDati)

	context.ricavoTotale = foundset.profit + job_orders.profit;
	//application.output('getBusinessCase ----------------------' + context.ricavoTotale)
	context.costoTotale = foundset.cost + job_orders.cost;
	context.mol = context.ricavoTotale - context.costoTotale;
	context.molPer = (context.ricavoTotale - context.costoTotale) / context.ricavoTotale * 100;
	context.myFormat = '#,00';
	plugins.VelocityReport.installFonts();

	// we use a template that should be located in the default velocity reports folder:
	//excelTemplateBO.xml
	var template = 'excelTemplateJOLink.xml';

	// now let's render the report into a String:
	var excel = plugins.VelocityReport.renderTemplate(template, context);

	//	application.output(excel);

	application.output(globals.messageLog + 'STOP globals.getBusinessCaseBOLink() ', LOGGINGLEVEL.DEBUG);
	return excel;

}

/**
 * @param {Number} eventId
 *
 * @return {Boolean}
 * @properties={typeid:24,uuid:"DEEAF631-D77D-4A70-97D2-272C1353D962"}
 * @AllowToRunInFind
 */
function isUserAllowedToEvent(eventId) {
	var found = false;

	if (currentuserid_to_users_events.find()) {
		var tot = currentuserid_to_users_events.search();
	}

	if (tot > 0) {
		for (var index = 1; index <= currentuserid_to_users_events.getSize(); index++) {
			var recFound = currentuserid_to_users_events.getRecord(index);

			if (recFound.event_id == eventId) {
				found = true;
				break;
			}
		}
	}
	return found;
}

/**
 * @AllowToRunInFind
 * @param {Number} userId
 * @param {Number} profitCenterId
 * @return {Number[]}
 *
 * @properties={typeid:24,uuid:"78EF0176-06B0-4218-BF3A-960747959485"}
 */
function getUserClientsForPCSelected(userId, profitCenterId) {
	var listClients = [];
	/** @type {JSFoundSet<db:/geco/comm_supp_customers>} */
	var comm_supp_customers = databaseManager.getFoundSet('geco', 'comm_supp_customers');
	if (comm_supp_customers.find()) {
		comm_supp_customers.comm_supp_customers_to_comm_supp_profit_centers.user_id = userId;
		comm_supp_customers.comm_supp_customers_to_comm_supp_profit_centers.profit_center_id = profitCenterId;

		if (comm_supp_customers.search() > 0) {
			//			comm_supp_profit_centers.sort('profit_center_type_id desc, profit_center_name asc')
			for (var index = 1; index <= comm_supp_customers.getSize(); index++) {
				var rec = comm_supp_customers.getRecord(index);
				listClients.push(rec.customer_id);
			}
		}
	}
	return listClients;
}

/**
 * @param {JSEvent} headlessCallback
 * @properties={typeid:24,uuid:"A5D8472C-1D32-45FA-92DD-0F2ED0B76DDD"}
 */
function enqueueMailLogForInsuredUsers(headlessCallback) {
	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		var messageMail = '-------------SENDING MAIL----------------------' + '\ntimestamp: ' + headlessCallback.getTimestamp() + '\nrecipient: ' + headlessCallback.data.recipient + '\nbcc: ' + headlessCallback.data.bcc + '\nsubject: ' + headlessCallback.data.subject + '\nmessage: ' + headlessCallback.data.message + '\nsuccess: ' + headlessCallback.data.success.toString().toUpperCase() + '\n-----------------------------------------------\n';
		application.output(globals.messageLog + 'globals.enqueueMailLog() ' + messageMail, LOGGINGLEVEL.DEBUG);
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output(globals.messageLog + 'globals.enqueueMailLog() exception callback, name: ' + headlessCallback.data, LOGGINGLEVEL.DEBUG);
	}
	if (client != null && client.isValid()) {
		application.output('chiudo client ' + client.getClientID());
		client.shutdown();
	}
}


/**
 * JStaffa metodo per avere la data in formato stringa ANNO-MESE-GIORNO
 * @param {Date} date
 * @return {String} dateStr
 * @properties={typeid:24,uuid:"32FE6CB9-86E2-46C9-8E0D-960F6DF1FFEA"}
 */
function getDateStringFormat(date) {
//	application.output(globals.messageLog + 'START geco_core.globals getDateStringFormat(date) date: ' + date, LOGGINGLEVEL.INFO);
	var dayStr = '' + (date.getDate());
	var monthStr = '' + (date.getMonth() + 1);
	var dateStr = date.getFullYear() + '-' + (monthStr.length > 1 ? monthStr : '0' + monthStr) + '-' + (dayStr.length > 1 ? dayStr : '0' + dayStr);
//	application.output(globals.messageLog + 'STOP geco_core.globals getDateStringFormat(date) dateStr: ' + dateStr, LOGGINGLEVEL.INFO);
	return dateStr;
}
//FS variabile per segnalare se la coppia user_id,group_id è già stata inserita nella tabella user_groups
/**@type {Boolean}
 * @properties={typeid:35,uuid:"DAEF44B1-4BAB-4387-B445-A362A925400D",variableType:-4}
 */
var isApproversAddtoUserGroupTable = false;

/**@type {Number}
 * @properties={typeid:35,uuid:"347AC175-25E7-4265-B36F-6DCD49DC69BE",variableType:-4}
 */
var allowedJobOrderSelection = false;

/**
 * Ritorna la lista di tutti i PC di cui l'utente è responsabile o vice o supporto
 * @AllowToRunInFind
 * @param {Number} userId
 * @return {Number[]}
 * @properties={typeid:24,uuid:"30BCE813-3F0B-44F8-B6AA-952969827ED4"}
 */
function getAllProfitCenterByUserManager(userId) {
	var listPC = [];
	/** @type {JSFoundSet<db:/geco/profit_centers>} */
	var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');
	if (profit_centers.find()) {
		//se responsabile mercato o vice responsabile o suporto commerciale
		profit_centers.profit_centers_to_view_user_pc_mgm.user_id = userId;
		
		if (profit_centers.search() > 0) {
			profit_centers.sort('profit_center_type_id desc, profit_center_name asc');
			for (var index = 1; index <= profit_centers.getSize(); index++) {
				var rec = profit_centers.getRecord(index);
				listPC.push(rec.profit_center_id);
			}
		}
	}
	application.output(listPC)
	return listPC;
}

/**
* Ritorna la lista dei responsabili di tutti i PC di cui l'utente è responsabile o vice o supporto
 * @AllowToRunInFind
 * @param {Number} userId
 * @return {Number[]}
 * @properties={typeid:24,uuid:"51FF09D2-B878-44DE-B725-F351410C28E1"}
 */
function getAllPcOwnerByUserManager(userId) {
	var listPCOwner = [];
	/** @type {JSFoundSet<db:/geco/profit_centers>} */
	var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');
	if (profit_centers.find()) {
		//se responsabile mercato o vice responsabile o suporto commerciale
		profit_centers.profit_centers_to_view_user_pc_mgm.user_id = userId;
		
		if (profit_centers.search() > 0) {
			profit_centers.sort('profit_center_type_id desc, profit_center_name asc');
			for (var index = 1; index <= profit_centers.getSize(); index++) {
				var rec = profit_centers.getRecord(index);
				listPCOwner.push(rec.profit_centers_to_owners_markets.user_id);
			}
		}
	}
	application.output(listPCOwner)
	return listPCOwner;
}


/**
* Ritorna la lista dei responsabili di tutti i PC di cui l'utente è responsabile o vice e il ruolo è solo supporto solo quelli per cui è supporto
 * @AllowToRunInFind
 * @param {Number} userId
 * @return {Number[]}
 * @properties={typeid:24,uuid:"8F7DED9F-EF7B-458F-ABFE-0274F567B3CF"}
 */
function getPcOwnerByUserManager(userId) {
	var listPCOwner = [];
	/** @type {JSFoundSet<db:/geco/profit_centers>} */
	var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');
	if (profit_centers.find()) {
		if (scopes.globals.hasRole('Supp. Commerciale') && !globals.hasRole('Resp. Mercato') && !globals.hasRole('Vice Resp. Mercato')) {
			profit_centers.profit_centers_to_comm_support.user_id = userId;
		} else {
			//responsabile o vice responsabile
			profit_centers.profit_centers_to_owners_markets.user_id = userId;
			profit_centers.newRecord();
			profit_centers.profit_centers_to_vice_owner.user_id = userId;
		}
		if (profit_centers.search() > 0) {
			profit_centers.sort('profit_center_type_id desc, profit_center_name asc');
			for (var index = 1; index <= profit_centers.getSize(); index++) {
				var rec = profit_centers.getRecord(index);
				//listPCOwner.push(rec.profit_centers_to_owners_markets.user_id);
				for (var t= 1; t<= rec.profit_centers_to_owners_markets.getSize(); t++){
					var pcOw = rec.profit_centers_to_owners_markets.getRecord(t);
					//listPCOwner.push(rec.profit_centers_to_owners_markets.user_id);
					listPCOwner.push(pcOw.user_id);
				}
				
			}
		}
	}
	application.output(listPCOwner)
	return listPCOwner;
}


/**
 * Ritorna la lista dei  PC di cui l'utente supporto
 * @AllowToRunInFind
 * @param {Number} userId
 * @return {Number[]}
 * @properties={typeid:24,uuid:"699F6195-24AA-4EAB-B06D-0C9210890999"}
 */
function getProfitCenterSupportByUserManager(userId) {
	var listPC = [];
	/** @type {JSFoundSet<db:/geco/profit_centers>} */
	var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');
	if (profit_centers.find()) {
		profit_centers.profit_centers_to_comm_support.user_id = userId;
		
		if (profit_centers.search() > 0) {
			databaseManager.getSQL(profit_centers);
			profit_centers.sort('profit_center_type_id desc, profit_center_name asc');
			for (var index = 1; index <= profit_centers.getSize(); index++) {
				var rec = profit_centers.getRecord(index);
				listPC.push(rec.profit_center_id);
			}
		}
	}
	application.output(listPC)
	return listPC;
}

/**
 * Ritorna la lista dei responsabili di tutti i PC di cui l'utente è supporto
 * @AllowToRunInFind
 * @param {Number} userId
 * @return {Number[]}
 * @properties={typeid:24,uuid:"A47EE18E-9370-4895-A527-18E3A443DE96"}
 */
function getPcOwnerSupportByUserManager(userId) {
	application.output(userId)
	var listPCOwner = [];
	application.output(listPCOwner)
	/** @type {JSFoundSet<db:/geco/profit_centers>} */
	var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');
	if (profit_centers.find()) {
		profit_centers.profit_centers_to_comm_support.user_id = userId;
		if (profit_centers.search() > 0) {
			application.output(profit_centers.getSize())
			profit_centers.sort('profit_center_type_id desc, profit_center_name asc');
			for (var index = 1; index <= profit_centers.getSize(); index++) {
				var rec = profit_centers.getRecord(index);
				application.output(rec)
				listPCOwner.push(rec.profit_centers_to_owners_markets.user_id);
			}
		}
	}
	application.output(listPCOwner)
	return listPCOwner;
}