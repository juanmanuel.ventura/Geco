/**
 * @param {JSRecord<db:/geco/users_job_orders>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"EB585D21-C27C-43F0-89C6-91C91CE0ECFB"}
 */
function processPreventDeletion(record) {
	var totEvents = 0;
	var totExpenses = 0;
	var realName = record.users_job_orders_to_users.users_to_contacts.real_name;
	
	// trova tutti gli eventi per user_id-job_order_id in stato diverso da approvato o chiuso
	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	events_log.loadAllRecords();
	if (events_log.find()){
		//events_log.event_log_date = '>' + new Date();
		events_log.user_id = record.user_id;
		events_log.job_order_id = record.job_order_id;
//		events_log.event_log_status = [1,3];
		events_log.event_log_status = 1;
//		events_log.newRecord();
//		events_log.user_id = record.user_id;
//		events_log.job_order_id = record.job_order_id;
//		events_log.event_log_status = 3;
		totEvents = events_log.search();
	}
//	application.output('totEvents in stato 1 o 3 per utente ' + totEvents);
	application.output('totEvents in stato 1 per utente ' + totEvents);
	// trova tutte le note spese per user_id-job_order_id in stato diverso da approvato o chiuso
	/** @type {JSFoundSet<db:/geco/expenses_log>} */
	var expenses_log = databaseManager.getFoundSet('geco', 'expenses_log');
	
	if (expenses_log.find()){
		expenses_log.user_id = record.user_id;
		expenses_log.job_order_id = record.job_order_id;
//		expenses_log.expense_log_status = [1,3];
		expenses_log.expense_log_status = 1;
//		expenses_log.newRecord();
//		expenses_log.user_id = record.user_id;
//		expenses_log.job_order_id = record.job_order_id;
//		expenses_log.expense_log_status = 3;
		totExpenses = expenses_log.search();
	}
//	application.output('totExpenses in stato 1 o 3 per utente ' + totExpenses);
	application.output('totExpenses in stato 1 per utente ' + totExpenses);
	//blocca la cancellazione se trova gli eventi 
	if (totEvents > 0 ) throw new Error('- Non puoi disallocare ' + realName+ ' perchè ha eventi consuntivati in sospeso ');
	
	if (totExpenses > 0 ) throw new Error('- Non puoi disallocare ' + realName+ ' perchè ha note spese in sospeso');
}