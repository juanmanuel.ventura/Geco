/**
 * @param {JSRecord<db:/geco/owners_profit_centers>} record
 * @properties={typeid:24,uuid:"A351F672-57EB-49A1-B815-0B3A8439C781"}
 */
function validateOwnerPC(record) {
	if (globals.isEmpty(record.user_id)) {
		throw new Error('- Il campo responsabile è obbligatorio');
	}
}


/**
 * @param {JSRecord<db:/geco/vice_owner_profit_center>} record
 * @properties={typeid:24,uuid:"20D05F32-00CC-4C46-8100-52C698CC7886"}
 * @AllowToRunInFind
 */
function validateOwnerNotVicePC(record) {
	// cercare nella tabella vice_owner che non sia responsabile dello stesso centro profitto
	/** @type {JSFoundSet<db:/geco/vice_owner_profit_center>} */
	var ownerPC = databaseManager.getFoundSet('geco', 'vice_owner_profit_center');
	if (ownerPC.find()) {
		ownerPC.user_id = record.user_id;
		ownerPC.profit_center_id = record.profit_center_id;
		if (ownerPC.search() > 0) throw new Error('- La persona selezionata è Vice Responsabile del centro profitto');
	}
	
	var viceEdited = databaseManager.getEditedRecords(record.vice_owner_to_profit_centers.profit_centers_to_owners_markets);
	for (var i = 0; i< viceEdited.length; i++){
		/** @type {JSRecord<db:/geco/vice_owner_profit_center>} */
		var recVice = viceEdited[i];
		if (recVice.user_id == record.user_id && recVice.profit_center_id == record.profit_center_id) {
			throw new Error('- La persona selezionata è Vice Responsabile del centro profitto');
		}
	}
}


/**
 * @param {JSRecord<db:/geco/vice_owner_profit_center>} record
 * @properties={typeid:24,uuid:"D23CA391-F054-4072-9D42-52F6727E3717"}
 * @AllowToRunInFind
 */
function validateOwnerNotSuppPC(record) {
	//cercare nella tabella supporto che non sia supporto per lo stesso centro profitto
	/** @type {JSFoundSet<db:/geco/comm_supp_profit_centers>} */
	var suppPC = databaseManager.getFoundSet('geco', 'comm_supp_profit_centers');
	if (suppPC.find()) {
		suppPC.user_id = record.user_id;
		suppPC.profit_center_id = record.profit_center_id;
		if (suppPC.search() > 0) throw new Error('- La persona selezionata è Suporto Commerciale del centro profitto');
	}

	var suppEdited = databaseManager.getEditedRecords(record.vice_owner_to_profit_centers.profit_centers_to_comm_support);
	for (var i = 0; i< suppEdited.length; i++){
		/** @type {JSRecord<db:/geco/comm_supp_profit_centers>} */
		var recSupp = suppEdited[i];
		if (recSupp.user_id == record.user_id && recSupp.profit_center_id == record.profit_center_id) {
			throw new Error('- La persona selezionata è Suporto Commerciale del centro profitto');
		}
	}
		
}

/**
 * Inserisce user nel gruppo mrket manager
 * @param {JSRecord<db:/geco/owners_profit_centers>} record
 * @properties={typeid:24,uuid:"96A40EE6-963A-4A0F-90EA-6E68D47E18D3"}
 * @AllowToRunInFind
 */
function processAddOwneMarketManagerGroup(record) {
	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
	try {
		//Se tipologia di PC è mercato.
		if (record && record.owners_markets_to_profit_centers && record.owners_markets_to_profit_centers.profit_center_type_id && record.owners_markets_to_profit_centers.profit_center_type_id == 5) {
			//Responsabile di mercato
			if (user_groups.searchUserGroup(record.user_id, 21) == 0 )
					user_groups.addUserGroup(record.user_id, 21);
			//se non è nel gruppo approvatori lo aggiungo
			//FS aggiunta statement per impedire errore del DB se la coppia user_id, group_id è già inserita in precedenza
			if (user_groups.searchUserGroup(record.user_id, 11) == 0 && globals.isApproversAddtoUserGroupTable == false) {
				user_groups.addUserGroup(record.user_id, 11);
			}
		}
	} catch (e) {
		throw new Error("- Impossibile assegnare il ruolo Approver al responsabile\n" + e['message']);
	} finally {
		globals.isApproversAddtoUserGroupTable == false;
	}
}

/**
 * Cancella il user dal gruppo market manager
 * @param {JSRecord<db:/geco/owners_profit_centers>} record *
 * @properties={typeid:24,uuid:"AA1458AA-C200-44B7-AB3F-EB1114D98F95"}
 * @AllowToRunInFind
 */
function processDeleteUserFromGroup(record) {
	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
	application.output('sto per cancellare solo dal gruppo 21');
	//user_groups.deleteUserGroup(record.user_id, [21]);
	var totOPC = 0
	/** @type {JSFoundSet<db:/geco/owners_profit_centers>} */
	var owner_pc = databaseManager.getFoundSet('geco', 'owners_profit_centers');
	if (owner_pc.find()) {
		owner_pc.user_id = record.user_id;
		owner_pc.profit_center_id = '!' + record.profit_center_id;
		totOPC = owner_pc.search();

	}
	//se non è responsabile di altri pc gli tolgo il ruolo di responsabile mercato e delivery manager
	if (totOPC == 0) {
		user_groups.deleteUserGroup(record.user_id, [21, 16]);
	} else {
		if (owner_pc.find()) {
			owner_pc.user_id = record.user_id;
			owner_pc.profit_center_id = '!' + record.profit_center_id;
			owner_pc.owners_markets_to_profit_centers.profit_center_type_id = 5;
			totOPC = owner_pc.search();
		}
		//non è responsabile di altri mercati
		if (totOPC == 0) {
			user_groups.deleteUserGroup(record.user_id, [21]);
		}
	}
	/** @type {JSFoundSet<db:/geco/approvers>} */
	var approver = databaseManager.getFoundSet('geco', 'approvers');
	if (approver.find()) {
		approver.user_approver_id = record.user_id;
		if (approver.search() == 0) user_groups.deleteUserGroup(record.user_id, [11]);
	}
}
