/**
 * @param {JSRecord<db:/geco/bo_orders>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"E4A21CE9-37A8-44FD-9CDD-910703884015"}
 */
function validateOrderNumber(record){
	if (globals.isEmpty(record.order_number)) {
		throw new Error('- Il campo Numero Ordine è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/bo_orders>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"514A80B7-29C7-40C9-AE94-D95C8A5685C4"}
 */
function validateOrderdescription(record){
	if (globals.isEmpty(record.description_order)) {
		throw new Error('- Il campo Descrizione Ordine è obbligatorio');
	}
}


/**
 * @param {JSRecord<db:/geco/bo_orders>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"A9B07236-4064-48E3-AB25-7ED862EEC302"}
 */
function validateOrderAmount(record){
	application.output(record);
	if (globals.isEmpty(record.order_amount)) {
		throw new Error('- Il campo Importo Ordine è obbligatorio');
	}
}


/**
 * @param {JSRecord<db:/geco/bo_orders>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"80371CA9-2C52-47DA-8320-9A324091E0F7"}
 */
function validateOrderDate(record){
	if (globals.isEmpty(record.order_date)) {
		throw new Error('- Il campo Data Ordine è obbligatorio');
	}
}

/**
 * ricalcola il valore del campo order_amount della BO collegata
 * @param {JSRecord<db:/geco/bo_orders>} record 
 * @throws {Error}
 * @properties={typeid:24,uuid:"8AA73E70-ADB7-4593-AE37-E21E5B89E7D5"}
 */
function processChangeAmount(record){
	/** @type {JSDataSet} */
	var dsUpdate = record.getChangedData();
	for( var i = 1; i <= dsUpdate.getMaxRowIndex(); i++ )
	{
		application.output(dsUpdate.getValue(i,1) +' '+ dsUpdate.getValue(i,2) +' '+ dsUpdate.getValue(i,3));
		if (dsUpdate.getValue(i,1) == 'order_amount'){
			//sottroaggo dalla campo amount della BO il vecchio importo e aggiungo il nuovo
			record.bo_orders_to_business_opportunities.order_amount = record.bo_orders_to_business_opportunities.order_amount - dsUpdate.getValue(i,2);
			record.bo_orders_to_business_opportunities.order_amount = record.bo_orders_to_business_opportunities.order_amount + dsUpdate.getValue(i,3);
		}
	}
}