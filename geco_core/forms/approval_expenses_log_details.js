/**
 * Handle record selected.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"03FF546E-1388-4263-89EE-F78C9E83DAF0"}
 */
function onRecordSelection(event) {
	_super.onRecordSelection(event);

	if (foundset.getSize() > 0) {

		// toggle elements based on the expense type
		if (foundset.expenses_log_to_expense_types){
		elements.groupGeneric.visible = !foundset.expenses_log_to_expense_types.has_km;
		elements.groupGeneric.enabled = !foundset.expenses_log_to_expense_types.has_km;

		elements.groupKM.visible = foundset.expenses_log_to_expense_types.has_km;
		elements.groupKM.enabled = foundset.expenses_log_to_expense_types.has_km;
		

//		if (foundset.expenses_log_to_expense_types.has_km) {
//			elements.groupGeneric.visible = false;
//			elements.groupGeneric.enabled = false;
//
//			elements.groupKM.visible = true;
//			elements.groupKM.enabled = true;
//		} else {
//			elements.groupGeneric.visible = true;
//			elements.groupGeneric.enabled = true;
//
//			elements.groupKM.visible = false;
//			elements.groupKM.enabled = false;
//		    }
		}
	}
}


/**
 * Handle changed data.
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"FF7CD733-CF01-4E1F-931D-4725A8B1CAD6"}
 */
function setJobOrderExternalCode(oldValue, newValue, event) {
	if (oldValue != newValue) {
		foundset.external_code = foundset.expenses_log_to_job_orders.external_code_navision;
	}
	return true
}


/**
 * Handle changed data.
 * @param oldValue old value
 * @param newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @AllowToRunInFind *
 * @properties={typeid:24,uuid:"B28F6DDB-6A88-40E8-8596-DE10C1CDA7A2"}
 */
function setExpenseType(oldValue, newValue, event) {
	switchPanels();
	return true;
}

/**
 * @return {Boolean} *
 * @properties={typeid:24,uuid:"8FB0DAF2-5D6B-4690-8EC8-E2DFC94B8B54"}
 */
function switchPanels() {
	// if new record unload panels and exit
	if (foundset.getSize() == 0) {
		elements.groupGeneric.visible = false;
		elements.groupKM.visible = false;
		return false;
	}

	if (foundset.expense_type_id && foundset.expenses_log_to_expense_types.has_km == 0) {
		elements.groupGeneric.visible = true;
		elements.groupGeneric.enabled = true;
		elements.groupKM.visible = false;
		elements.groupKM.enabled = false;
		//elements.km  = null;
		if (isEditing()) {
			foundset.km = null;
			foundset.total_amount = null;
			foundset.total_amount_km = null;
			elements.billable.enabled = true;
			foundset.amount = null;
			foundset.currency = 0;
			foundset.expense_change = 1;
			foundset.total_amount = null;
			foundset.total_amount_money = null;
		}
	}
	if (foundset.expense_type_id && foundset.expenses_log_to_expense_types.has_km == 1) {
		elements.groupGeneric.visible = false;
		elements.groupGeneric.enabled = false;
		elements.groupKM.visible = true;
		elements.groupKM.enabled = true;
		if (isEditing()) {
			foundset.amount = null;
			foundset.currency = 0;
			foundset.expense_change = 1;
			foundset.total_amount = null;
			foundset.total_amount_money = null;
		}
	}
	// default exit
	return true;
}

/**
 * Opens a pop up to select date
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"8404B399-7533-4E64-B366-73E5DC886281"}
 */
function openPopupCopy(event) {
	
	var record = foundset.getSelectedRecord();

	// skip if no record is selected
	if (!record) {
		globals.DIALOGS.showWarningDialog('Attenzione','Selezionare un record','OK')
		application.output("No record selected");
		return;
	}
	var win = application.createWindow("copia", JSWindow.MODAL_DIALOG);
	win.title = "Copia nota spese";
	win.show(forms.expenses_duplicator);
}


/**
 * @param {Date} start
 * @param {Date} end
 * @param {Number} skip
 * @param {Number} year
 * @param {Number} month
 * @properties={typeid:24,uuid:"11BDBDAC-4794-4142-85BF-90CF1EC097CE"}
 * @AllowToRunInFind
 */
function duplicateExpensesLog(start, end, skip, year, month) {
	
	application.output(globals.messageLog + 'START approval_expenses_log_list.duplicateExpensesLog() ',LOGGINGLEVEL.INFO);
	// get the original record
	var record = foundset.getSelectedRecord();

	// skip if no record is selected
	if (!record) {
		application.output("No record selected");
		return;
	}

	// check for disabled month
	if (foundset.expenses_log_to_calendar_months.is_enabled_for_expenses == 0) {
		globals.DIALOGS.showErrorDialog("Errore", "Non è possibile inserire note spese su un mese disabilitato", "OK");
		return;
	}

	// sanitize dates
	start.setHours(0, 0, 0);
	end.setHours(0, 0, 0);

	var indexToDuplicate = foundset.getRecordIndex(record);
	/** @type {JSRecord<db:/geco/expenses_log>} */
	var originalRecord = foundset.getRecord(indexToDuplicate);

	// loop thru dates
	var count = 1;
	for (var date = start; start <= end; date.setDate(date.getDate() + 1)) {
		count++;
		// skip weekends
		if (skip == 1 && globals.isWeekEnd(date)) {
			continue;
		}
		//prendo il record originario
		indexToDuplicate = foundset.getRecordIndex(originalRecord);
		databaseManager.setAutoSave(false);
		indexToDuplicate = foundset.duplicateRecord(indexToDuplicate, true, true);
		var newrecord = foundset.getRecord(indexToDuplicate);
		newrecord.expense_date = date;
		newrecord.expense_log_status = 1;
		newrecord.expense_month = month;
		newrecord.expense_year = year;
		newrecord.actual_approver_id = null;
		if (record.expenses_log_to_expense_types.has_km == 0) {
			newrecord.expense_reference = originalRecord.expense_reference + "_" + count;
		}
		if (!databaseManager.setAutoSave(true)) {
			application.output('record fallito '+databaseManager.getFailedRecords(foundset));
			databaseManager.revertEditedRecords();
			if (globals.validationExceptionMessage) {
				globals.DIALOGS.showErrorDialog('Error', globals.validationExceptionMessage, 'OK');
			}
			databaseManager.setAutoSave(true);
			
		}
	}

	updateUI(new JSEvent);
	application.output(globals.messageLog + 'STOP approval_expenses_log_list.duplicateExpensesLog() ',LOGGINGLEVEL.INFO);
}

/**
 * 
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"92745DDE-A77A-4B06-9C8B-6BA75B9C9C9E"}
 */
function updateUI(event) {
	_super.updateUI(event);
	
	if(globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')){
		elements.buttonCopy.visible = false;
		elements.buttonEdit.visible = false;
	} else {
	//Per far vedere il bottone di copia soltanto a personnel
//	if(globals.hasRole('HR Administrators') || globals.hasRole('HR Expenses')){
//		elements.buttonCopy.visible = !isEditing();
//	}
//	else {
//	    elements.buttonCopy.enabled = false;
//	    elements.buttonCopy.visible = false;  
//	}
	elements.buttonCopy.visible = !isEditing();
	}
}
