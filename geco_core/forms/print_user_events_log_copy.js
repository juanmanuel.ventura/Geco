/**
 * @type {Number}
 * @properties={typeid:35,uuid:"C19F49D5-6B6E-49E2-9A4A-DC9BE298239A",variableType:8}
 */
var totalORD = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"6A8E18C3-F120-4290-A279-47ABDC09FCA4",variableType:8}
 */
var totalSSS = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"D5CF10BC-B7E8-428B-B055-DC583C3F5C9D",variableType:8}
 */
var totalHVI = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"C0B41605-343A-4DFA-A316-BE63BF199FD9",variableType:8}
 */
var totalRFES = 0.0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"41C48DA4-FD51-411D-8846-36284EF0B16B",variableType:8}
 */
var totalRORD = 0.0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"50695E90-F915-49DD-8D4F-34C288374A0F"}
 */
var monthStr = '';

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 *
 * @properties={typeid:24,uuid:"B1E1B438-FEB9-4D26-A415-F85B28467F69"}
 */
function onAction(event) {
//	controller.setPageFormat(297.0,201.0,5,5,5,5,SM_ORIENTATION.LANDSCAPE);
	controller.print(false, true, plugins.pdf_output.getPDFPrinter());
//	controller.print(false, false, plugins.pdf_output.getPDFPrinter());
}

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"DA8C82A8-5F1B-4704-8029-318B44926532"}
 */
function onLoad(event) {
	application.output('CARICATA '+event.getFormName());
}

/**
 * Callback method when form is destroyed.
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"010522B6-F0CA-4602-8F7B-E73005417BB6"}
 */
function onUnload(event) {
	solutionModel.removeForm(event.getFormName());
	application.closeAllWindows();
}

/**
 * Handle hide window.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"7DF35A51-D9E4-466D-BE1C-048DD19F5FC7"}
 */
function onHide(event) {
	solutionModel.removeForm(event.getFormName());
	application.closeAllWindows();
	return true
}
