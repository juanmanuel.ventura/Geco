/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"A549BF89-4D6C-49B0-BF36-03DD6AE020E2",variableType:-4}
 */
var showMenu = false;

/**@type {String}
* @properties={typeid:35,uuid:"0E90DEE0-7F36-473C-95ED-69703BDCFB03"}
*/
var dynamicValuelistMain = null;

/**
* @type {JSDataSet}
* @properties={typeid:35,uuid:"AA37921E-7A0D-4CC8-B703-A029485F6329",variableType:-4}
*/
var dynamicDatasetMain = null;

/**@type {Array<String>}
* @properties={typeid:35,uuid:"57D69CE6-1B81-4B61-A71E-2B2354EEEFCA",variableType:-4}
*/
var solutionsListMain = [];

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"064A956F-3ACF-4B55-9178-BA15B2A3C4AE"}
 * @AllowToRunInFind
 */
function initialize(event) {
	application.output(globals.messageLog + 'START main.initialize() ',LOGGINGLEVEL.INFO);
	//non carica gli eventi festività
	databaseManager.addTableFilterParam('geco', 'events_log', 'event_id', 'not in', [34], 'holidayFilter');
	globals.currentUserRoleList = security.getUserGroups();
	// load rules !!!
	globals.loadRunnableRules();
	// prepare windows
	globals.prepareWindow(event.getFormName());
	// set current user info
	/** @type {JSFoundSet<db:/geco/users>} */
	var fs = databaseManager.getFoundSet('geco', 'users');
	if (fs.find()) {
		fs.user_id = security.getUserUID();
		if (fs.search() != 0) {
			globals.currentUserDisplayName = fs.user_real_name;
			globals.currentUserId = fs.user_id;
		}
	}
	
	/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
	var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
	bo_actual_date.loadAllRecords();
	globals.actualDate = bo_actual_date.actual_date;
	globals.actualMonth = bo_actual_date.actual_month;
	globals.actualYear = bo_actual_date.actual_year;
	globals.isOpenToActual = bo_actual_date.is_open_to_actual;
	
	globals.actualMonthFilterController = bo_actual_date.actual_month;
	globals.actualYearFilterController = bo_actual_date.actual_year
	
	dynamicDatasetMain = databaseManager.createEmptyDataSet();
	application.output(application.getSolutionName());
	if (globals.hasRole('Controllers') && application.getSolutionName() != 'geco_controller') solutionsListMain.push('Controller');
	if ((globals.hasRole('HR Administrators') || globals.hasRole('HR Expenses') || globals.hasRole('Admin Readers')) && application.getSolutionName() != 'geco_personnel') solutionsListMain.push('Personnel');
	if ((globals.currentUserDisplayName == 'STAFFA JONATHAN' || globals.currentUserDisplayName == 'FRAU DANIELA') && application.getSolutionName() != 'geco_configurator') solutionsListMain.push('Configurator');
	if (globals.hasRole('Approvers') && application.getSolutionName() != 'geco_approver') solutionsListMain.push('Approver');
	if (application.getSolutionName() != 'geco_logger') solutionsListMain.push('Logger');
	if (((globals.hasRole('Delivery Admin') || globals.hasRole('Responsabili Commessa') || globals.hasRole('Controllers') || globals.hasRole('Resp. Mercato') || globals.hasRole('Orders Admin') || globals.hasRole('Planner') || globals.hasRole('Supp. Commerciale')) || globals.hasRole('Vice Resp. Mercato')) && (application.getSolutionName() != 'geco_management')) solutionsListMain.push('Management');
	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	if (users.find()) {
		users.user_id = scopes.globals.currentUserId;
		if (users.search() > 0) {
			var rec = users.getRecord(1);
			if (rec.has_medical_practice == 1 && application.getSolutionName() == 'geco_logger') solutionsListMain.push('Pratiche Sanitarie');
		}
	}
	application.output('solutionsList.length: ' + solutionsListMain.length + '; solutionsList: ' + solutionsListMain);
	if (solutionsListMain.length > 0) {
		solutionsListMain.sort();
		for (var index = 0; index < solutionsListMain.length; index++) {
			dynamicDatasetMain.addRow(new Array(solutionsListMain[index]));
		}
		elements.dynamicSolutionsChoice.setValueListItems(dynamicDatasetMain);
//		elements.lSolutionName.enabled = true;
//		elements.lSolutionName.visible = true;
		elements.dynamicSolutionsChoice.enabled = true;
		elements.dynamicSolutionsChoice.visible = true;
	}
	
	application.output(globals.messageLog + 'STOP main.initialize() ',LOGGINGLEVEL.INFO);

}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"EC61FA4F-5D85-458A-822E-1D3A84E929AC"}
 */
function toggleMenu(event) {
	plugins.window.getMenuBar().setVisible(showMenu);
	showMenu = !showMenu;
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"D0E11FE8-BB2F-4F87-880B-730011792531"}
 * @protected 
 */
function setSelector(event) {
	if (event.getType() == JSEvent.ACTION) {
		// move the selector
		/** @type {RuntimeLabel} */
		var label = event.getSource();
		elements.selector.setLocation(label.getLocationX(), label.getLocationY());
		elements.selector.setSize(label.getWidth(), label.getHeight());
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"036A2D21-2556-478B-9A6C-692634E06A60"}
 */
function logOut(event) {
	application.output(globals.messageLog + 'START logOut() from ' + application.getSolutionName(),LOGGINGLEVEL.INFO);
	var serverUrl = application.getServerURL();
	application.output(globals.messageLog + '_main.logOut() serverUrl ' + serverUrl, LOGGINGLEVEL.DEBUG);
	var solutionName = application.getSolutionName();
	if (!isEditing()) {
		application.closeAllWindows();
		if (serverUrl != 'http://localhost:8080') application.showURL(serverUrl + '/servoy-webclient/application/solution/' + solutionName, '_self');
		security.logout(solutionName);
		application.output(globals.messageLog + 'STOP _main.logOut()', LOGGINGLEVEL.INFO);
	} else {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!","OK");
		application.output(globals.messageLog + 'STOP _main.logOut() tentato logout in editing: annullato', LOGGINGLEVEL.INFO);
		return;
//		if (serverUrl != 'http://localhost:8080') application.showURL(serverUrl + '/servoy-webclient/application/solution/' + solutionName, '_self');
//		security.logout(solutionName);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formName the event that triggered the action
 * @properties={typeid:24,uuid:"59C79DCE-D8FD-4CE8-BC0B-A249909C931F"}
 */
function loadPanel(event, formName) {
	application.output(globals.messageLog + 'START main.loadPanel() ' + formName,LOGGINGLEVEL.INFO);
	// if in editing mode DO NOT move away!
	if (isEditing()) {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!","OK");
		return;
	}

	// move selector
	setSelector(event);
	if (elements.tabless.removeAllTabs()) {
		if (forms[formName]) {
			elements.tabless.addTab(forms[formName], null, null, null, null, null, null, null, -1);
		} else application.output('errore sul form ' + formName);
	}
	application.output(globals.messageLog + 'STOP main.loadPanel() '  + formName ,LOGGINGLEVEL.INFO);

}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"D6256CDE-FF93-465C-9050-77637CF18E7B"}
 */
function openChangePassword(event) {
	application.output(globals.messageLog + 'START main.openChangePassword() ',LOGGINGLEVEL.INFO);
	if (isEditing()) {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!","OK");
		return;
	}
	var win = application.createWindow("selezione", JSWindow.MODAL_DIALOG);
	globals.callerForm = forms[event.getFormName()];
	win.show(forms.change_password);
	application.output(globals.messageLog + 'STOP main.openChangePassword() ',LOGGINGLEVEL.INFO);
}

/**
 * @param {JSEvent} event
 * @param {String} solutionName
 *
 * @properties={typeid:24,uuid:"CDD83BBD-4504-4337-8A00-4ADCE2BD75DD"}
 */
function goToSolution(event, solutionName) {
	application.output(globals.messageLog + 'START main.goToSolution() ',LOGGINGLEVEL.INFO);
	if (isEditing()) {
		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!","OK");
		return;
	}
	application.output(globals.messageLog + 'main.goToSolution() apri Solution ' + solutionName,LOGGINGLEVEL.INFO);
	var serverUrl = application.getServerURL();
	application.output(globals.messageLog + 'main.goToSolution() serverUrl ' + serverUrl,LOGGINGLEVEL.INFO);
	serverUrl = globals.getServerUrlGeco(serverUrl);
	if (solutionName == 'mp_app') serverUrl = serverUrl + '/servoy-webclient/application/solution/' + solutionName;
	else serverUrl = serverUrl + '/servoy-webclient/application/solution/geco_' + solutionName;
	application.output('-----url finale2 '+serverUrl)
	application.showURL(serverUrl, '_self');
	application.output(globals.messageLog + 'main.goToSolution() serverUrl2 ' + serverUrl,LOGGINGLEVEL.INFO);
	try {
		application.closeSolution();
	} catch (e) {
		application.output(globals.messageLog + 'main.goToSolution() ERROR: ' + e.message,LOGGINGLEVEL.ERROR);
		application.output(globals.messageLog + 'main.goToSolution() ' + e,LOGGINGLEVEL.ERROR);
	}
	application.output(globals.messageLog + 'STOP main.goToSolution() ',LOGGINGLEVEL.INFO);
}

/**
 * @properties={typeid:24,uuid:"E886C906-C0D1-4C06-B8DC-94AA998DFA88"}
 * @AllowToRunInFind
 */
function checkPwdExpiration() {
	application.output(globals.messageLog + 'START checkPwdExpiration() ',LOGGINGLEVEL.INFO);
	/** @type {JSFoundSet<db:/geco/users_password_expiration>} */
	var fs = databaseManager.getFoundSet('geco', 'users_password_expiration');
	if (fs.find()) {
		fs.users_id = security.getUserUID();
		var result = fs.search();
		/** @type {Date} */
		var lastDate = null;
		if (result > 0) {
			fs.sort('usr_pwd_exp_id desc');
			for (var index = 1; index <= result; index++) {
				var rec = fs.getRecord(index);
				//if (lastDate == null) 
				lastDate = rec.expiration_date;
				
				break;
			}

			lastDate = new Date(lastDate.getFullYear(), lastDate.getMonth(), lastDate.getDate())
			application.output(globals.messageLog + 'checkPwdExpiration() lastDate '+lastDate,LOGGINGLEVEL.DEBUG);
			var today = new Date();
			today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
			application.output(globals.messageLog + 'checkPwdExpiration() today '+today,LOGGINGLEVEL.DEBUG);

			if (lastDate <= today) {
				application.output(globals.messageLog + 'checkPwdExpiration() password scaduta',LOGGINGLEVEL.INFO);
				//var win = application.getActiveWindow();
				databaseManager.setAutoSave(false);
				application.output(globals.messageLog + 'checkPwdExpiration() isEditing:'+isEditing(),LOGGINGLEVEL.DEBUG);
				application.showForm(forms.main_password);
				
			} else {
				/** @type {Number} */
				var diff = ( (lastDate - today) / 86400000).toFixed(0);
				if (diff <= 5 && diff >= 1) {
					var message = 'ATTENZIONE!!!\nLa tua password scadrà tra ' + diff + ( (diff == 1) ? ' giorno' : ' giorni');
					application.output(globals.messageLog + 'checkPwdExpiration() Password in scadenza',LOGGINGLEVEL.INFO);
					globals.DIALOGS.showInfoDialog('Password in scadenza', message, 'OK');
				} else 
					application.output(globals.messageLog + 'checkPwdExpiration() password valida per altri ' + diff + ' gg',LOGGINGLEVEL.INFO);
			}

		}

	}
	application.output(globals.messageLog + 'STOP checkPwdExpiration() ',LOGGINGLEVEL.INFO);
}

/**
 * Handle changed data.
 * @param oldValue old value
 * @param newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"6C18975A-1130-43ED-8155-0734AB7FCF60"}
 */
function onDataChangeSolutionName(oldValue, newValue, event) {
	application.output(globals.messageLog + 'START main_logger.onDataChangeSolutionName()',LOGGINGLEVEL.INFO);
	if (oldValue != newValue && newValue != null) {
		//se clicco su nome soluzione mi porta alla soluzione corrispondente;
		switch (newValue) {
		case 'Logger':
		application.output(globals.messageLog + 'STOP main_logger.onDataChangeSolutionName() case '+newValue,LOGGINGLEVEL.INFO);
			goToSolution(event, 'logger');
			break;
		case 'Configurator':
		application.output(globals.messageLog + 'STOP main_logger.onDataChangeSolutionName() case '+newValue,LOGGINGLEVEL.INFO);
			goToSolution(event, 'configurator');
			break;
		case 'Controller':
		application.output(globals.messageLog + 'STOP main_logger.onDataChangeSolutionName() case '+newValue,LOGGINGLEVEL.INFO);
			goToSolution(event, 'controller');
			break;
		case 'Approver':
		application.output(globals.messageLog + 'STOP main_logger.onDataChangeSolutionName() case '+newValue,LOGGINGLEVEL.INFO);
			goToSolution(event, 'approver');
			break;
		case 'Management':
		application.output(globals.messageLog + 'STOP main_logger.onDataChangeSolutionName() case '+newValue,LOGGINGLEVEL.INFO);
			goToSolution(event, 'management');
			break;
		case 'Personnel':
		application.output(globals.messageLog + 'STOP main_logger.onDataChangeSolutionName() case '+newValue,LOGGINGLEVEL.INFO);
			goToSolution(event, 'personnel');
			break;
		case 'Pratiche Sanitarie':
		application.output(globals.messageLog + 'STOP main_logger.onDataChangeSolutionName() case '+newValue,LOGGINGLEVEL.INFO);
			goToSolution(event, 'mp_app');
			break;
		default:
		application.output(globals.messageLog + 'STOP main_logger.onDataChangeSolutionName() case default inesistente',LOGGINGLEVEL.INFO);
			break;
		}
	}
}