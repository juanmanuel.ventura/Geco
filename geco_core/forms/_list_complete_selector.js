/**
 * @type {Number}
 * @properties={typeid:35,uuid:"1147BB28-795F-4173-8647-F649B13DFFB8",variableType:4}
 */
var areAllSelected = 0;

/**
 * Select all records
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"1CED51D3-35C4-430F-A957-E40AF19AC47F"}
 */
function selectAllRecords(event) {
	for (var index = 1; index <= foundset.getSize(); index++) {
		var rec = foundset.getRecord(index);
		rec['is_selected'] = areAllSelected;
	}
}

/**
 * Perform the single selection.
 *
 * @param {JSEvent} event the event that triggered the action
 * @protected
 *
 * @properties={typeid:24,uuid:"598689E5-FE10-46BC-9065-E923CA3367FD"}
 */
function deselectOthers(event) {
	var me = foundset.getSelectedIndex();
	for (var index = 1; index <= foundset.getSize(); index++) {
		if (index != me && foundset.getRecord(index)['is_selected']) foundset.getRecord(index)['is_selected'] = 0;
	}
	return true;
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"828327F0-0561-49FD-A1C6-8C11283C5CB3"}
 */
function updateUI(event) {
	_super.updateUI(event);
	
	// buttons
	elements.buttonDelete.visible = false;
//	elements.buttonAdd.visible= isEditing();
	if (!isEditing()) {
		areAllSelected = 0;
		selectAllRecords(event);
	}
	
	return true;
}

/**
 * Check if one record is selected
 * @return {Boolean}
 * @properties={typeid:24,uuid:"91B446FD-A634-447D-9180-57506816D1DA"}
 */
function hasChanged() {
	if (foundset && foundset.getSize() != 0) {
		for (var index = 1; index <= foundset.getSize(); index++) {
			if (foundset['is_selected']) return true;
		}
	}
	return false;
}

/**
 * Delete records selected
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"55CEC880-00CB-40CA-B1C9-DD827B8523C7"}
 */
function deleteRecordsSelected(event) {
	application.output(globals.messageLog + 'START list_complete_selector.deleteRecordsSelected() ', LOGGINGLEVEL.DEBUG);
	if (hasChanged()) {
		/** @type {Array<JSRecord>} */
		var recordsToDelete = []
		if (foundset) {
			for (var index = 1; index <= foundset.getSize(); index++) {
				var rec = foundset.getRecord(index);
				if (rec['is_selected']){
					recordsToDelete.push(rec);
				}
			}
			deleteRecordsAtOnce(event, recordsToDelete)
			//application.output('list_complete delete  '+globals.validationExceptionMessage);
		}
	}
	application.output(globals.messageLog + 'STOP list_complete_selector.deleteRecordsSelected() ', LOGGINGLEVEL.DEBUG);
}
