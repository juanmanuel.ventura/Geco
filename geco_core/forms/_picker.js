/**
 * @param {JSEvent} event the event that triggered the action
 * @protected 
 * @properties={typeid:24,uuid:"AB7B9069-6599-4AD9-B790-0500994C9447"}
 */
function close(event) {
	controller.getWindow().destroy();
}