/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @param {String} entity
 * @properties={typeid:24,uuid:"D68D932B-6C9D-48B0-ADB7-F90051CFF366"}
 */
function clearAndClose(event, entity) {

	if (globals.isEmpty(year)) {
		globals.DIALOGS.showErrorDialog("Errore", "Il mese di consuntivazione non può essere vuoto", 'OK');
		return;
	}

	if (globals.isEmpty(month)) {
		globals.DIALOGS.showErrorDialog("Errore", "L'anno di consuntivazione non può essere vuoto", 'OK');
		return;
	}

	_super.clearAndClose(event, entity);
}