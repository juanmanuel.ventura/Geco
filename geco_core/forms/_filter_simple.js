/**
 * @type {Number}
 * @protected
 * @properties={typeid:35,uuid:"9B94A5CE-A2D8-46B7-8792-9EB1783473D3",variableType:4}
 */
var isEnabled = 1;

/**
 * @properties={typeid:24,uuid:"0E76D7D8-ABFB-4057-BD80-F9661427943D"}
 * @AllowToRunInFind
 */
function filterEnabledRecords() {
	// filter for entities that implement the field "is_enabled"
	if (foundset.alldataproviders.indexOf('is_enabled') != -1) {
		foundset.removeFoundSetFilterParam('enabled');
		foundset.addFoundSetFilterParam('is_enabled', '=', isEnabled, 'enabled');
		foundset.loadAllRecords();
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} [goTop] true if the cursor must be placed at the first index
 * @properties={typeid:24,uuid:"7516F254-9DF3-4A79-9A3C-49042EED7E4F"}
 */
function resetFilter(event, goTop) {
	foundset.loadAllRecords();
	// move to the first record (otherwise the cursor remains at the last selected record)
	if (goTop) foundset.setSelectedIndex(1);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"0644AD6B-0672-4AC8-B67C-69CAB512A8A3"}
 */
function onLoad(event) {
	filterEnabledRecords();
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"C77DD479-A8A2-4847-A85A-790C87320E88"}
 */
function updateUI(event) {
	_super.updateUI(event);
	controller.enabled = !isEditing();
	//elements.imageHeader.visible = !isEditing();
}
