/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"90918297-ADC5-4C8E-858E-9329A748585E"}
 */
function updateUI(event) {
	controller.enabled = true;
	controller.readOnly = !isEditing();

	// buttons
	elements.buttonEdit.visible = !isEditing() && (foundset.getSelectedRecord()) ? true : false;
	elements.buttonCancel.visible = isEditing();
	elements.buttonSave.visible = isEditing();

	elements.buttonAdd.visible = !isEditing();
	elements.buttonDelete.visible = !isEditing() && (foundset.getSelectedRecord()) ? true : false;

	// get the bundle
	var mainForm = forms[controller.getFormContext().getValue(2, 2)] || null;

	if (mainForm) {
		// get the split pane
		/** @type {RuntimeSplitPane} */
		var split = mainForm.elements['split'];

		// call updateUI in contained forms
		if (split) {
			split.getLeftForm()['updateUI']();
			split.getRightForm()['updateUI']();
		}
	}
}
