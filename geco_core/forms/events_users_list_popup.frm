dataSource:"db:/geco/users",
extendsID:"DD9194C5-206D-4FF1-A075-EA268CA83E93",
initialSort:"users_to_contacts.real_name asc",
items:[
{
extendsID:"17ABEB5C-8093-42C2-9522-04AC15F62432",
text:"Seleziona gli utenti per l'evento %%selected_event_to_events.event_name%%",
typeid:7,
uuid:"0ADE0481-411E-46AA-8005-487B9F1113DD"
},
{
extendsID:"6D6DDF53-89FA-4E68-BA5F-1088B0BC0001",
height:200,
typeid:19,
uuid:"0B222EED-E464-4709-AEA7-0245C5C54ED3"
},
{
extendsID:"041188E2-43FB-48F3-B66E-627E32748814",
height:152,
typeid:19,
uuid:"24F80553-C9A5-4EFB-98E0-A05315C6DE92"
},
{
dataProviderID:"user_real_name",
extendsID:"00AF8FA3-F4E3-439B-9614-C478F495969B",
location:"10,124",
typeid:7,
uuid:"368A9A40-E6AB-4388-B9E8-AF4F7EC3D51B"
},
{
dataProviderID:"is_selected",
extendsID:"1CB02C00-C9EC-4483-9ABD-E8881485A3E8",
location:"366,124",
onActionMethodID:"-1",
typeid:4,
uuid:"39966269-B7E2-454E-9C90-E1E159CBC712"
},
{
extendsID:"DE202A8E-133D-4B7C-8A96-F9FBCE65404C",
location:"359,164",
onActionMethodID:"ECA47198-2AEB-4F1F-8DEE-C010FB150A9D",
typeid:7,
uuid:"466804FA-F611-433D-A702-CFBE417BE245"
},
{
extendsID:"FEA7EA63-6CCF-41C0-82C9-2D0FED2A9C63",
formIndex:4,
location:"238,88",
typeid:7,
uuid:"50CFD629-C4E2-46B2-BD71-D28F3D41C6A8"
},
{
extendsID:"EF71FF5B-572C-4871-A155-0DADED5C5006",
formIndex:3,
location:"351,88",
typeid:4,
uuid:"87F0BB07-8BB7-4F8E-923A-0B9F431907FA"
},
{
extendsID:"0094B6DC-EF33-49F2-A2EF-A8B9CE28533B",
height:116,
typeid:19,
uuid:"A119CB24-CA30-4B37-910B-55D9E17D85D3"
},
{
extendsID:"5875382F-5113-499E-A282-9BECE90ED1AF",
location:"364,164",
typeid:7,
uuid:"A6BEF16F-196D-44BA-8631-3EEBFB32132A"
},
{
extendsID:"13E7D48D-42CC-42CF-BCFB-180E8223DAF0",
location:"0,77",
typeid:7,
uuid:"C0ED860E-561C-4AF3-9BD0-2896CACA828D"
},
{
dataProviderID:"selectedContract",
extendsID:"9171A290-AB32-4FFA-844F-592B12C1497D",
location:"41,46",
onActionMethodID:"0D517812-6AD7-482C-BA74-EEAF91E2AE1C",
size:"140,20",
text:"Filtra per tipo di contratto",
transparent:false,
typeid:4,
uuid:"C6314CF4-B55A-422C-AD89-5F7892AD0A31",
valuelistID:"0EB8D3B8-8AAA-419F-B3CB-E0667514CAAF"
},
{
dataProviderID:"selectedProfitCenter",
displayType:2,
editable:false,
formIndex:3,
location:"235,46",
name:"cbProfitCenter",
onActionMethodID:"0D517812-6AD7-482C-BA74-EEAF91E2AE1C",
size:"140,20",
text:"Filtra per Profit Center",
typeid:4,
uuid:"D47332F2-D438-418A-8624-7C328AB18D2C",
valuelistID:"73312BF3-989F-48F4-85E1-AB96BDEA9A3F"
},
{
extendsID:"F068A998-61CD-49A1-9C20-3140AB5216E9",
location:"318,164",
onActionMethodID:"067EF201-6484-4535-8AB1-8FF88706A0BB",
typeid:7,
uuid:"D7494437-45F1-4E2E-B126-94FE9CD9D783"
},
{
extendsID:"3D006472-EAC7-4E81-BED9-40145EAC4252",
location:"15,46",
onActionMethodID:"3CFE2C6D-1A36-49E9-A83A-A32FFB793FBE",
typeid:7,
uuid:"FAA8626E-092C-4286-8486-82F1D7D3A3F8"
}
],
name:"events_users_list_popup",
namedFoundSet:"separate",
styleName:"GeCo",
titleText:"Utenti",
typeid:3,
uuid:"4432EAF9-B82C-4832-8783-828EC16B8BDD"