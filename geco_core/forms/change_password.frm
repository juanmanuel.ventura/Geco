extendsID:"496DEEE0-F8EF-4D5A-8F7E-303EA89EACA1",
items:[
{
extendsID:"A6A9C3D4-C72F-4B01-AE06-119D630A5887",
height:258,
typeid:19,
uuid:"0165AB86-05DF-4D0B-B247-8F940AECA37C"
},
{
formIndex:8,
horizontalAlignment:4,
location:"15,88",
size:"145,28",
text:"Vecchia Password",
transparent:true,
typeid:7,
uuid:"03063215-697D-48A0-9BE1-FD419C378E4E"
},
{
anchors:11,
dataProviderID:"oldPassword",
displayType:6,
formIndex:2,
horizontalAlignment:2,
location:"170,88",
name:"oldPassword",
onActionMethodID:"-1",
onDataChangeMethodID:"-1",
size:"278,28",
typeid:4,
uuid:"0A6869FD-0BC0-462E-9FA0-24D5C3F70F30"
},
{
extendsID:"584852A3-48F1-4566-90D3-FF43FE280208",
height:307,
typeid:19,
uuid:"0BA429C4-62C2-46DA-B46B-9677F1A8E545"
},
{
extendsID:"7D3B8205-CC20-43C3-989D-1A127CF8CFD6",
formIndex:2,
location:"388,268",
onActionMethodID:"A40DB8FA-9279-4096-AECD-3BE87FD37B41",
typeid:7,
uuid:"2DECA301-2149-4A2B-B8D2-CD3525430859"
},
{
displaysTags:true,
fontType:"Calibri,0,13",
foreground:"#ff0000",
formIndex:4,
horizontalAlignment:0,
location:"15,231",
size:"433,22",
text:"%%result%%",
transparent:true,
typeid:7,
uuid:"40B3E088-4F4D-4F7D-A37C-AA3687633B52"
},
{
anchors:11,
background:"#ffffff",
dataProviderID:"username",
editable:false,
enabled:false,
formIndex:2,
horizontalAlignment:2,
location:"170,52",
name:"username",
onActionMethodID:"-1",
onDataChangeMethodID:"-1",
size:"278,28",
transparent:true,
typeid:4,
uuid:"6B07281C-D07C-43A1-ACBB-8E80949CEFF3"
},
{
formIndex:8,
horizontalAlignment:4,
location:"15,124",
size:"145,28",
text:"* Nuova Password",
transparent:true,
typeid:7,
uuid:"83B10662-F739-4BFF-86A3-C0A83CB2D0C2"
},
{
extendsID:"D2DC6F09-3211-46F8-BB43-D3D79D329349",
formIndex:1,
location:"15,268",
typeid:7,
uuid:"A7B48D70-C526-4F9F-A3FC-24DC467757E3"
},
{
formIndex:8,
horizontalAlignment:4,
location:"15,52",
size:"145,28",
text:"Username",
transparent:true,
typeid:7,
uuid:"A9133778-076D-47CD-B337-95F5B1F82FCB"
},
{
anchors:11,
dataProviderID:"newPassword",
displayType:6,
formIndex:2,
horizontalAlignment:2,
location:"170,124",
name:"newPassword",
onActionMethodID:"-1",
onDataChangeMethodID:"-1",
size:"278,28",
typeid:4,
uuid:"BA404023-A7E6-46B2-907B-4BBA91CFD82C"
},
{
extendsID:"B649AC0D-00D5-4039-9F29-473D6E55F601",
formIndex:3,
location:"429,268",
onActionMethodID:"0A8A776F-22B7-4FAA-9833-70B994AF3599",
typeid:7,
uuid:"CEF7ED72-8AB8-4011-9B3C-BD14548A2AF0"
},
{
extendsID:"47E9646D-4062-4DEB-A83B-66C9D94FFCB9",
horizontalAlignment:0,
size:"433,22",
text:"Cambio Password",
typeid:7,
uuid:"D1D62D06-07E7-400E-9491-94E60FF5C013"
},
{
formIndex:8,
horizontalAlignment:4,
location:"15,160",
size:"145,28",
text:"* Nuova Password",
transparent:true,
typeid:7,
uuid:"DBCDE425-386B-4B71-96C5-FB4A13CEDECA"
},
{
displaysTags:true,
fontType:"Calibri,0,12",
formIndex:4,
horizontalAlignment:0,
location:"15,198",
size:"433,22",
text:"* deve avere minimo 8 caratteri fra cui almeno un numero e una maiuscola",
transparent:true,
typeid:7,
uuid:"DDBD9490-2E3A-4084-BF42-452F455EC8B9"
},
{
anchors:11,
dataProviderID:"repeatNewPassword",
displayType:6,
formIndex:2,
horizontalAlignment:2,
location:"170,160",
name:"repeatNewPassword",
onActionMethodID:"-1",
onDataChangeMethodID:"-1",
size:"278,28",
typeid:4,
uuid:"F0D49A83-0E01-4522-8F38-F8B3850499C1"
},
{
extendsID:"E7451E69-4EF0-4E48-8702-0E01DD3AABF7",
formIndex:0,
location:"0,258",
size:"464,48",
typeid:7,
uuid:"F7046E9B-F624-49E0-8474-90EF2CEF4CF7"
}
],
name:"change_password",
onShowMethodID:"-1",
size:"463,254",
styleName:"GeCo",
typeid:3,
uuid:"5D15D9C8-0E6D-440D-9EDC-7FF187FAA094"