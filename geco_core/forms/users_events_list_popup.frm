dataSource:"db:/geco/events",
extendsID:"DD9194C5-206D-4FF1-A075-EA268CA83E93",
initialSort:"event_name asc",
items:[
{
extendsID:"F068A998-61CD-49A1-9C20-3140AB5216E9",
location:"318,160",
onActionMethodID:"C10551E0-E54D-4C12-9974-C035ACA5704E",
typeid:7,
uuid:"00A30674-2AD1-4B43-A545-1FB1B092F80F"
},
{
extendsID:"13E7D48D-42CC-42CF-BCFB-180E8223DAF0",
location:"2,73",
typeid:7,
uuid:"259C7BE7-7AD9-4330-82F0-D21958199757"
},
{
extendsID:"5875382F-5113-499E-A282-9BECE90ED1AF",
location:"364,152",
typeid:7,
uuid:"35B79A03-FEC6-4B41-B09D-7242DE2C2684"
},
{
dataProviderID:"is_selected",
extendsID:"1CB02C00-C9EC-4483-9ABD-E8881485A3E8",
formIndex:3,
location:"366,120",
onActionMethodID:"-1",
typeid:4,
uuid:"3C4B80B4-62A2-42D8-A45A-85C0FD77ED9E"
},
{
extendsID:"041188E2-43FB-48F3-B66E-627E32748814",
height:148,
typeid:19,
uuid:"43165DA3-8B2F-4599-876C-70C6B04A3E4D"
},
{
dataProviderID:"selectedEventType",
editable:false,
enabled:true,
extendsID:"9171A290-AB32-4FFA-844F-592B12C1497D",
location:"60,46",
onActionMethodID:"6851C947-2D2F-4258-9E95-7B8D300E49AC",
selectOnEnter:false,
typeid:4,
uuid:"4425E201-6CB5-4C1B-8BC1-4E7684B7034B",
valuelistID:"E648EAFC-6173-466A-B643-FEE951F57F40",
visible:true
},
{
extendsID:"6D6DDF53-89FA-4E68-BA5F-1088B0BC0001",
height:196,
typeid:19,
uuid:"788E3D2A-4033-4BF0-9608-47AE5448588E"
},
{
extendsID:"EF71FF5B-572C-4871-A155-0DADED5C5006",
formIndex:4,
location:"351,84",
typeid:4,
uuid:"90703717-0323-4E2D-AE87-35A0A083D9B5"
},
{
dataProviderID:"event_name",
extendsID:"00AF8FA3-F4E3-439B-9614-C478F495969B",
formIndex:2,
location:"10,120",
size:"349,20",
typeid:7,
uuid:"91F8FA98-AB6C-48A1-A5EC-F01A3AEC19B3"
},
{
extendsID:"0094B6DC-EF33-49F2-A2EF-A8B9CE28533B",
height:112,
typeid:19,
uuid:"AA2EB5BA-FAC6-426C-80BE-9D4B324A2976"
},
{
extendsID:"DE202A8E-133D-4B7C-8A96-F9FBCE65404C",
location:"359,160",
onActionMethodID:"97F93B10-7380-4397-A1CC-D119D902FAE3",
typeid:7,
uuid:"BA9FC2A7-2C92-4A99-9AD2-B3B345A534C5"
},
{
extendsID:"17ABEB5C-8093-42C2-9522-04AC15F62432",
formIndex:3,
size:"380,20",
text:"Seleziona gli eventi da associare",
typeid:7,
uuid:"DA9DD544-BCE1-489D-9182-9EEEB5504BB6"
},
{
extendsID:"3D006472-EAC7-4E81-BED9-40145EAC4252",
location:"15,46",
onActionMethodID:"6E2B6E7D-05E6-45C4-AF5F-C3F5D764A5B6",
typeid:7,
uuid:"E9CE1859-C38B-4C47-AB77-AADD0B08B258"
},
{
extendsID:"FEA7EA63-6CCF-41C0-82C9-2D0FED2A9C63",
formIndex:5,
location:"238,84",
typeid:7,
uuid:"F3D65E5D-6DB5-48C9-91B6-DAD8DA8776B7"
}
],
name:"users_events_list_popup",
onShowMethodID:"8B45FDB3-5CFD-408D-9E58-1D4342D42A6D",
styleName:"GeCo",
titleText:"Eventi",
transparent:false,
typeid:3,
uuid:"F7A5802A-9CCE-420B-B249-81BF634EBCF1"