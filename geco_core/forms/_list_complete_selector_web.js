/**
 * @type {Number}
 * @properties={typeid:35,uuid:"4D187F86-CA53-4D37-BE85-B45C856811B2",variableType:4}
 */
var areAllSelected = 0;

/**
 * Select all records
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"01683FF1-EB38-4F24-8735-AB00E8A41789"}
 */
function selectAllRecords(event) {
	for (var index = 1; index <= foundset.getSize(); index++) {
		var rec = foundset.getRecord(index);
		rec['is_selected'] = areAllSelected;
	}
}

/**
 * Perform the single selection.
 *
 * @param {JSEvent} event the event that triggered the action
 * @protected
 *
 * @properties={typeid:24,uuid:"110C2911-6E5D-4329-BA04-9A46F675E0F4"}
 */
function deselectOthers(event) {
	var me = foundset.getSelectedIndex();
	for (var index = 1; index <= foundset.getSize(); index++) {
		if (index != me && foundset.getRecord(index)['is_selected']) foundset.getRecord(index)['is_selected'] = 0;
	}
	return true;
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"91D0283C-E075-41C9-A6BF-6ACD3B073284"}
 */
function updateUI(event) {
	_super.updateUI(event);
	
	// buttons
	elements.buttonDelete.visible = false;
	elements.buttonAdd.visible= isEditing();
	if (!isEditing()) {
		areAllSelected = 0;
		selectAllRecords(event);
	}
	
	return true;
}

/**
 * Check if one record is selected
 * @return {Boolean}
 * @properties={typeid:24,uuid:"004A0D97-6D0A-49A7-898C-5B837A193A6C"}
 */
function hasChanged() {
	if (foundset && foundset.getSize() != 0) {
		for (var index = 1; index <= foundset.getSize(); index++) {
			if (foundset['is_selected']) return true;
		}
	}
	return false;
}

/**
 * Delete records selected
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"BA4408B8-23A4-4893-8CF4-FE79E91BE959"}
 */
function deleteRecordsSelected(event) {
	if (hasChanged()) {
		/** @type {Array<JSRecord>} */
		var recordsToDelete = []

		if (foundset) {
			for (var index = 1; index <= foundset.getSize(); index++) {
				var rec = foundset.getRecord(index);
				if (rec['is_selected']) recordsToDelete.push(rec);
			}
			deleteRecordsAtOnce(event, recordsToDelete)
			//application.output('list_complete delete  '+globals.validationExceptionMessage);
		}
	}
}
