/**
 * @type {Number}
 * @properties={typeid:35,uuid:"1E886873-AA06-4F20-94BC-841DCCF6130C",variableType:8}
 */
var selectedStatus = 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"EFC271CF-B4FA-426C-9976-98EA6F2EDE44",variableType:8}
 */
var selectedProfitCenter = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"29D2E571-BC19-4822-89A2-359645F0615B",variableType:8}
 */
var selectedUser = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"1C13E0CF-073D-4DF1-B1A0-9948721AB545",variableType:8}
 */
var selectedJobOrder = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"9C8EE0D6-6844-40A9-8A91-A4534067ACE9",variableType:8}
 */
var isDirectApprover = 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"437A301D-1BF1-4A45-80D7-1D3B861B5637",variableType:8}
 */
var approver = security.getUserUID();

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"BA58E7C3-35EF-4525-BF00-EB7226C060A7",variableType:8}
 */
var selectedMonth = new Date().getMonth() + 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"80B693A5-91D4-44FD-A45B-7DA699BE71BB",variableType:8}
 */
var selectedYear = new Date().getFullYear();

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"BE424E0E-07AC-4548-BF6C-E6A088AE21A7",variableType:8}
 */
var selectedApprover = null;

/**
 * @properties={typeid:24,uuid:"DADB1F4A-85D7-4150-A955-5338549B1A9F"}
 * @AllowToRunInFind
 */
function applyFilter() {

	if (foundset.find()) {
		foundset.expense_month = selectedMonth;
		foundset.expense_year = selectedYear;
		foundset.job_order_id = selectedJobOrder;
		foundset.expenses_log_to_job_orders.profit_center_id = selectedProfitCenter;
		foundset.expense_log_status = selectedStatus;
		foundset.user_id = selectedUser;
		foundset.user_approver_id = selectedApprover;
		// Direct approver
		if (isDirectApprover == 1) foundset.user_approver_id = approver;

		// Not direct approver
		if (isDirectApprover == 0) {

			// Job Order's Profit Center Owner
			//foundset.expenses_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_owners_markets.user_id = approver;
			foundset.expenses_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_view_user_pc_mgm.user_id = approver;
			foundset.expenses_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_view_user_pc_mgm.group_letter = '!S';
			//foundset.expenses_log_to_job_orders.job_orders_to_profit_centers.user_owner_id = approver;
			// Not the job order approver itself
			//foundset.expenses_log_to_job_orders.job_orders_to_approvers.user_approver_id = "!=" + approver;
			foundset.user_approver_id = "!=" + approver;
			foundset.newRecord();
			//foundset.expenses_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_owners_markets.user_id = "!=" + approver;
			foundset.expenses_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_view_user_pc_mgm.user_id = "!=" + approver;
			foundset.expenses_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_view_user_pc_mgm.group_letter = '!S';
			//foundset.expenses_log_to_job_orders.job_orders_to_profit_centers.user_owner_id = "!=" + approver;
			foundset.user_approver_id = "!=" + approver;
			foundset.expenses_log_to_job_orders.user_owner_id = approver;
			foundset.expense_month = selectedMonth;
			foundset.expense_year = selectedYear;
			foundset.job_order_id = selectedJobOrder;
			foundset.expenses_log_to_job_orders.profit_center_id = selectedProfitCenter;
			foundset.expense_log_status = selectedStatus;
			foundset.user_id = selectedUser;
			
		}
		if (isDirectApprover == 2) {
			foundset.user_approver_id = "!=" + approver;
			// Not the approver itself
			//foundset.expenses_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_owners_markets.user_id = "!=" + approver;
			foundset.expenses_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_view_user_pc_mgm.user_id = approver;
			foundset.expenses_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_view_user_pc_mgm.group_letter = '!S';
			//foundset.expenses_log_to_job_orders.job_orders_to_profit_centers.user_owner_id = "!=" + approver;			
		}
		 foundset.search();
		 foundset.sort('expenses_log_to_users.users_to_contacts.real_name asc,expense_date asc');
		//application.output('foundset ' + result);
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"A8AF7B72-49CF-4D49-AE4B-3EB3CF8625DB"}
 */
function resetFilter(event) {
	selectedApprover = null;
	selectedProfitCenter = null;
	selectedUser = null;
	selectedJobOrder = null;
	selectedStatus = 1;
	selectedMonth = new Date().getMonth() + 1;
	selectedYear = new Date().getFullYear();
	applyFilter();
	toggleButton(selectedStatus != null);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"EB5DCBF5-3B45-4FBB-A795-C2877D9FBAD4"}
 */
function onLoad(event) {
	_super.onLoad(event);
	applyFilter();
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @protected
 * @properties={typeid:24,uuid:"5198F374-439E-45BC-918E-16D467CD73E0"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter();
	toggleButton(selectedStatus != null);
	return true;
}

/**
 * @param {Boolean} status
 * @properties={typeid:24,uuid:"5F051CDE-AFD3-483B-815D-14DD5CBB086D"}
 */
function toggleButton(status) {
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		if(globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')){
			bundle.elements['split']['getRightForm']().elements['buttonAdd'].visible = false;
			bundle.elements['split']['getRightForm']().elements['buttonDelete'].visible = false;
			bundle.elements['split']['getRightForm']().elements['buttonClose'].visible = false;
			bundle.elements['split']['getRightForm']().elements['buttonApprove'].visible = false;
			bundle.elements['split']['getRightForm']().elements['buttonReject'].visible = false;
			bundle.elements['split']['getRightForm']().elements['buttonShift'].visible = false;
			
		} else {
		bundle.elements['split']['getRightForm']().elements['buttonApprove'].visible = status;
		bundle.elements['split']['getRightForm']().elements['buttonReject'].visible = status;
		bundle.elements['split']['getRightForm']().elements['buttonClose'].visible = status;
		}
	}
}

