/**
 * @type {Number}
 * @properties={typeid:35,uuid:"1E338657-35D6-43D6-8CC6-8200FFD462DA",variableType:8}
 */
var selectedMonth = new Date().getMonth() + 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"3EA65658-0A24-4067-B034-9FCCD2A33142",variableType:8}
 */
var selectedYear = new Date().getFullYear();

/**
 * Filter records
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"18636FFE-7F45-45CC-9B17-32005708C521"}
 */
function applyFilter(event) {
	if (foundset.find()) {
		foundset.month_year = selectedYear; 
		foundset.search();
	}
}

/**
 * Clear selection, revert db changes and close popup
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"810CE28A-F2CF-4BC7-B25E-0E77208BBBF9"}
 */
function rollbackAndClose(event) {
	databaseManager.revertEditedRecords(foundset)
	databaseManager.setAutoSave(true);
	var currentWin = controller.getWindow();
	if(currentWin)
		currentWin.destroy();
}

/**
 * Save edits and close popup
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"038CEC06-DE54-42D3-8FC4-73E4FF7D5EB4"}
 */
function saveEdits(event) {
	_super.saveEdits(event);
	controller.getWindow().destroy();
}

/**
 * Callback method when form is (re)loaded.
 * @param {JSEvent} event the event that triggered the action
 * @protected 
 * @properties={typeid:24,uuid:"B9ED6145-6098-4636-9AC3-5B6A521EF09F"}
 */
function onLoad(event) {
	foundset.loadAllRecords()
	applyFilter(event);
	databaseManager.setAutoSave(false)
}

/**
 * Handle hide window.
 * @param event
 *
 * @properties={typeid:24,uuid:"1C8774A7-739D-4333-9176-E1B2F90962BE"}
 */
function onHide(event) {
	rollbackAndClose(event)
}
