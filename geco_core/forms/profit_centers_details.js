/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"7DC9178F-49FE-4C2A-8C17-021B4FA60654",variableType:-4}
 */
var added = false;

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"403B9974-B573-4CD5-A393-B486F752DEE1"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
	application.output(globals.messageLog + 'START profit_centers_details.saveEdits() ', LOGGINGLEVEL.INFO);
	var changeRPC = false;
	var record = foundset.getSelectedRecord();
	if (added == false && !record.isNew()) {
		//se ho cambiato il campo RCP

		application.output('record ' + record);
		var dataset = record.getChangedData();
		application.output('dataset ' + dataset);
		for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
			//  se nel dataset il campo precedente user_owner_id è stato cambiato
			if (dataset.getValue(i, 1) == 'user_owner_id' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
				application.output(globals.messageLog + ' profit_centers_details.saveEdits() user_owner_id cambiato', LOGGINGLEVEL.INFO);
				changeRPC = true;
				break;
			}
		}
		var answer = 'No'
		if (changeRPC)
			answer = globals.DIALOGS.showWarningDialog("Conferma", "Vuoi aggiungere come nuovo approvatore?", "Si", "No");
		if (answer == 'Si') {
			application.output(globals.messageLog + ' profit_centers_details.saveEdits() user_owner_id aggiunto come approvatore ', LOGGINGLEVEL.INFO);
			var dateFrom = new Date();
			var day = dateFrom.getDate();
			var month = dateFrom.getMonth();
			var year = dateFrom.getFullYear();
			//			var date = new Date();
			//			var month = '' + (date.getMonth() + 1);
			//			var day = '' + (date.getDate());
			//			var dateStr = (day.length > 1? day : '0' + day) + '-' + (month.length > 1 ? month : '0' + month) + '-' + (date.getFullYear());
			foundset.profit_centers_to_approvers.newRecord(false);
			foundset.profit_centers_to_approvers.user_approver_id = foundset.user_owner_id;
			foundset.profit_centers_to_approvers.user_approver_from = new Date(year, month, day);
			added = true;
			return;
		}
	}

	//delete 'Resp. Mercato'
	for (var c = 1; c <= record.profit_centers_to_owners_markets.getSize(); c++) {
		var ownerPC = record.profit_centers_to_owners_markets.getRecord(c);
		if (record.user_owner_id == ownerPC.user_id && ownerPC.is_selected == 1) {
			//lo trovo e lo deseleziono
			ownerPC.is_selected = 0;
			globals.DIALOGS.showErrorDialog('Error', 'Non è possibile cancellare l\'utente ' + ownerPC.owners_markets_to_users.user_real_name + '.', 'OK');
			break;
		}
	}
	application.output('owner da cancellare ' + record.profit_centers_to_owners_markets);
	forms.profit_centers_owners_list.deleteRecordsSelected(event);

	
	//delete 'Supporto Commerciale'
	application.output('Lista SUPPORTO COMM ' + record.profit_centers_to_comm_support.getSize());
	for (var sc = 1; sc <= record.profit_centers_to_comm_support.getSize(); sc++) {
		var commSupp = record.profit_centers_to_comm_support.getRecord(sc);
		application.output(commSupp);
		application.output('Selezionato pc ' + commSupp.profit_center_id + ' utente ' + commSupp.user_id + ' PK ' + commSupp.comm_supp_profit_center_id)
	}

	forms.profit_centers_support_list.deleteRecordsSelected(event);

	//delete 'Customer'
	if (record.profit_centers_to_comm_support.comm_supp_pc_to_comm_supp_customers) {
		for (var cust = 1; cust <= record.profit_centers_to_comm_support.comm_supp_pc_to_comm_supp_customers.getSize(); cust++) {
			var customer = record.profit_centers_to_comm_support.comm_supp_pc_to_comm_supp_customers.getRecord(cust);
			application.output('Selezionato ID ' + customer.comm_supp_customer_id + 'cliente' + customer.customer_id + ' ID SUPP COMM PK ' + customer.comm_supp_pc_id)
		}

		forms.comm_supp_customers.deleteRecordsSelected(event);
	}
	
	//delete 'Vice Resp. Mercato'
	for (var v = 1; v <= record.profit_centers_to_vice_owner.getSize(); v++) {
		var vicePC = record.profit_centers_to_vice_owner.getRecord(v);
		application.output(vicePC);
		application.output('Selezionato pc ' + vicePC.profit_center_id + ' utente ' + vicePC.user_id )
	}
	forms.profit_centers_vice_list.deleteRecordsSelected(event);
	
	//delete approver
	var fd = foundset.profit_centers_to_approvers;
	try {
		if (fd) {
			for (var index = 1; index <= fd.getSize(); index++) {
				var rec = fd.getRecord(index);
				if (rec.is_selected) {
					application.output(globals.messageLog + 'START profit_centers_details.saveEdits() cancellazione approvatore centro profitto ' + rec.approvers_to_users.users_to_contacts.real_name, LOGGINGLEVEL.INFO);
					var totEventsLog = 0;
					//controllo che approver non abbia eventi da approvare per il profit center
					/** @type {JSFoundSet<db:/geco/events_log>} */
					var events_log = databaseManager.getFoundSet('geco', 'events_log');
					if (events_log.find()) {
						// se profit center ---> tipologia di approvazione dell'evento = responsabile del profit center
						if (rec.entity == 'profit_centers') {
							events_log.events_log_to_events.approver_figure = 2;
							//events_log.events_log_to_users.users_to_profit_centers.user_owner_id = rec.user_approver_id;
							events_log.events_log_to_users.users_to_profit_centers.profit_center_id = rec.entity_id;
						}
						events_log.user_approver_id = rec.user_approver_id;
						events_log.event_log_status = '1||3';
						totEventsLog = events_log.search();
					}

					if (totEventsLog > 0) {
						application.output(globals.messageLog + ' profit_centers_details.saveEdits() Non è possibile cancellare approvatore ci sono eventi in sospeso', LOGGINGLEVEL.INFO);
						application.output(globals.messageLog + 'STOP profit_centers_details.saveEdits() cancellazione approvatore centro profitto KO ' + rec.approvers_to_users.users_to_contacts.real_name, LOGGINGLEVEL.INFO);
						globals.DIALOGS.showErrorDialog('Error', 'Non è stato cancellato l\'approvatore, \nha consuntivazioni in sospeso per questo centro profitto', 'OK');
						return;
					} else {
						try {
							fd.deleteRecord(index);
							application.output(globals.messageLog + 'STOP profit_centers_details.saveEdits() cancellazione approvatore centro profitto OK ' + rec.approvers_to_users.users_to_contacts.real_name, LOGGINGLEVEL.INFO);
						} catch (e) {
							application.output(e.message);
							globals.DIALOGS.showErrorDialog('Error', globals.validationExceptionMessage, 'OK');
							updateUI(event);
							// Failed to save data
							return;
						}
					}
				}
			}
		}

		//prima di salvare, controllo che se non è stato scelto mercato ma il pc è stato messo nella lista
		//di quelli da includere, allora automaticamente gli rimetto il flag a 0
		if (record.profit_center_type_id != 5 && record.is_to_check_market_competence == 1) 
			record.is_to_check_market_competence = 0;
		_super.saveEdits(event);
		added = false;
		application.output(globals.messageLog + 'STOP profit_centers_details.saveEdits() ', LOGGINGLEVEL.INFO);
	} catch (e) {
		application.output(e);
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"9D9B6FBF-FA3A-4217-AFF2-C5672AAAD2C6"}
 */
function updateUI(event) {
	_super.updateUI(event);

	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
		elements.buttonEdit.visible = false;
	}

	if (!isEditing()) foundset.sort('profit_center_name asc');

	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		bundle.elements['split'].visible = !isEditing();
		bundle['focus'] = foundset.profit_center_name;
		bundle.elements['focus'].visible = isEditing();
	}
	
	//flag isToCheck enabled se record è mercato
//	elements.isToCheck.enabled = (foundset && foundset.getSelectedRecord() && foundset.getSelectedRecord().profit_center_type_id == 5)?true:false;
	elements.isToCheck.enabled = (foundset && foundset.getSelectedRecord() && (foundset.getSelectedRecord().profit_center_type_id == 5 || foundset.getSelectedRecord().isNew()))?true:false;
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"461D7BB3-6DFC-40EF-AD16-007627EC0D5B"}
 */
function onDataChange(oldValue, newValue, event) {
	if (oldValue != newValue && newValue == 5) updateUI(event);
	return true
}
