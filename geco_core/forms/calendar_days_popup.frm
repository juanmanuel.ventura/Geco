dataSource:"db:/geco/calendar_days",
extendsID:"DD9194C5-206D-4FF1-A075-EA268CA83E93",
items:[
{
anchors:3,
displaysTags:true,
enabled:false,
formIndex:9,
location:"291,119",
size:"102,20",
text:"%%calendar_days_to_head_office.head_office_city%%",
transparent:true,
typeid:7,
uuid:"15628044-0988-4D6F-866E-7DB9176A6F38"
},
{
dataProviderID:"monthHoliday",
extendsID:"9171A290-AB32-4FFA-844F-592B12C1497D",
onDataChangeMethodID:"A18EAAC2-D088-4E9E-A5E5-9FFB5F2D3EEA",
typeid:4,
uuid:"212283A8-1C17-40FB-980A-8B8D3CA29D1C",
valuelistID:"BE1C3801-74CD-4446-8F8B-06FF70ACA3D0"
},
{
extendsID:"DE202A8E-133D-4B7C-8A96-F9FBCE65404C",
formIndex:8,
location:"359,160",
onActionMethodID:"A713BE70-00ED-4513-8221-14B1A70010EF",
typeid:7,
uuid:"217E04BC-D3DB-4570-9F1B-015D68AB9253"
},
{
extendsID:"17ABEB5C-8093-42C2-9522-04AC15F62432",
text:"FESTE",
typeid:7,
uuid:"25F07B5B-F2FA-428D-A790-7DA589FBB3D3"
},
{
extendsID:"5875382F-5113-499E-A282-9BECE90ED1AF",
formIndex:1,
location:"364,160",
typeid:7,
uuid:"269578A6-F894-4792-890E-CD2971A65B71"
},
{
enabled:false,
extendsID:"EF71FF5B-572C-4871-A155-0DADED5C5006",
formIndex:1,
typeid:4,
uuid:"2EDDBAEB-75A2-4897-83B9-AD001D777980",
visible:false
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"calendar_weekday",
editable:false,
formIndex:3,
location:"70,119",
size:"118,20",
transparent:true,
typeid:4,
uuid:"33F1AA0E-D96F-4F41-99DD-0D301367855B",
valuelistID:"ED969CF6-500F-4406-9F41-44683BD1DE48"
},
{
extendsID:"F068A998-61CD-49A1-9C20-3140AB5216E9",
formIndex:5,
location:"318,160",
onActionMethodID:"87533271-AD5E-4EB6-8641-18B92D862918",
typeid:7,
uuid:"4FD7B6FF-0FE4-4D1E-BA43-D80C8522E3DE"
},
{
extendsID:"0094B6DC-EF33-49F2-A2EF-A8B9CE28533B",
height:109,
typeid:19,
uuid:"608410AC-3557-459D-9BF6-B1476B20BE53"
},
{
extendsID:"3D006472-EAC7-4E81-BED9-40145EAC4252",
onActionMethodID:"A3C993B8-5993-4046-984E-07BB5B5ED4C8",
typeid:7,
uuid:"7395A2AF-5834-4AEA-9ED2-D85201A6ECB6"
},
{
anchors:9,
dataProviderID:"is_holiday",
extendsID:"1CB02C00-C9EC-4483-9ABD-E8881485A3E8",
formIndex:4,
location:"226,119",
onActionMethodID:"AC0A7814-6C9E-437D-92B9-A492CDE8B00E",
typeid:4,
uuid:"7D78EDD1-FCDB-4308-8BBF-A0E8751DB186"
},
{
anchors:3,
formIndex:3,
location:"264,84",
size:"117,20",
text:"Patrono Sede",
transparent:true,
typeid:7,
uuid:"A25021C8-4381-41E1-A16B-DD545B3B4420"
},
{
discardRemainderAfterBreak:false,
extendsID:"041188E2-43FB-48F3-B66E-627E32748814",
height:148,
typeid:19,
uuid:"B932EB5C-E8FB-4635-BB1D-6D2AC1EB021D"
},
{
extendsID:"00AF8FA3-F4E3-439B-9614-C478F495969B",
formIndex:1,
location:"10,119",
typeid:7,
uuid:"C5CCEA27-2E29-43A3-9C32-7A7E72163AA7"
},
{
extendsID:"6D6DDF53-89FA-4E68-BA5F-1088B0BC0001",
height:196,
typeid:19,
uuid:"D864EC1C-7050-43DF-89CA-095BCCBCAD73"
},
{
anchors:9,
enabled:true,
extendsID:"FEA7EA63-6CCF-41C0-82C9-2D0FED2A9C63",
formIndex:2,
location:"142,84",
text:"Festa Nazionale",
typeid:7,
uuid:"D935EFE5-EA6B-41FE-A2B9-689EEE948986",
visible:true
},
{
dataProviderID:"yearHoliday",
displayType:2,
editable:false,
formIndex:4,
location:"214,46",
name:"cbFilterc",
onDataChangeMethodID:"A18EAAC2-D088-4E9E-A5E5-9FFB5F2D3EEA",
size:"140,22",
transparent:true,
typeid:4,
uuid:"E5E905D2-7042-4436-9926-7B7524EDD415",
valuelistID:"075E9257-460A-403C-B979-C6A3C3402907"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"calendar_day",
editable:false,
formIndex:2,
location:"11,119",
size:"39,22",
transparent:true,
typeid:4,
uuid:"E8C5DB2D-4DE0-4190-BF24-7F93AF946ABD"
}
],
name:"calendar_days_popup",
onLoadMethodID:"A18EAAC2-D088-4E9E-A5E5-9FFB5F2D3EEA",
onRecordEditStopMethodID:"5CAA4819-E609-41BB-9AF5-1EC523FC19DB",
onRecordSelectionMethodID:"B4DA9FD0-0287-4BA4-A6DD-21B9321AE891",
styleName:"GeCo",
typeid:3,
uuid:"7E2F545D-2B27-419E-A404-7CC47B40FD15",
view:4