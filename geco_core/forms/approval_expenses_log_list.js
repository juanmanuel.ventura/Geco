/**
 * @type {String}
 * @properties={typeid:35,uuid:"A621FDE0-2345-42E3-9BC7-4C9B95F7E2BC"}
 */
var sortDirection = 'asc';

/**
 * @type {RuntimeLabel}
 * @properties={typeid:35,uuid:"88C46E56-E8DC-4502-8E7F-E0C36B66C709",variableType:-4}
 */
var label;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"84A2344C-159B-4FD5-AADC-776002E32CFC",variableType:4}
 */
var allSelected = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"44A71010-C4FE-44B0-A80A-D3DA9E13DEF8",variableType:8}
 */
var month = null;
/**
 * @type {Number}
 * @properties={typeid:35,uuid:"57CA3221-751C-41BE-B332-B1144953DAC5",variableType:8}
 */
var year = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"C30D5119-B06C-4AEF-B303-1B706A3B42CE",variableType:8}
 */
var userId = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"E6CFCB4F-1CD8-46C8-9E6C-A41B950F3051",variableType:8}
 */
var statusId = null;

/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"2C9DD4F1-2AD7-4CA0-A68A-B82009FF0FA1",variableType:-4}
 */
var expensesToNotify = [];

/**
 * @type {String}
 * @properties={typeid:35,uuid:"D536AAA9-E8E8-4EFE-948D-58920AE32830"}
 */
var labelUser = '';

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"2AECDD19-D124-4759-978E-754BE06838F8"}
 */
function sort(event) {
	// change color back to the original one
	if (label) label.fgcolor = '#727784';
	// (re) set the triggered label
	label = event.getSource();
	// (re) change color to indicate selection
	label.fgcolor = '#990000';
	var sortKey = label.text;

	switch (sortKey) {
	case 'Data':
		controller.sort('expense_date' + ' ' + sortDirection);
		break;
	case 'Utente':
		controller.sort('expenses_log_to_users.users_to_contacts.real_name' + ' ' + sortDirection);
		break;
	case 'Tipo spesa':
		controller.sort('expenses_log_to_expense_types.expense_type' + ' ' + sortDirection);
		break;
	case 'Codice':
		controller.sort('external_code' + ' ' + sortDirection);
		break;
	case 'Commessa':
		controller.sort('expenses_log_to_job_orders.job_order_title' + ' ' + sortDirection);
		break;
	case 'Importo':
		controller.sort('amount' + ' ' + sortDirection);
		break;
	}

	// toggle the sort order
	sortDirection = (sortDirection == 'asc') ? 'desc' : 'asc';
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"4F6B466D-7003-4502-BD7E-79DCA2192953"}
 */
function selectAll(event) {
	for (var index = 1; index <= foundset.getSize(); index++) {
		var record = foundset.getRecord(index);
		record.is_selected = allSelected;
	}
}

/**
 * Handle record selected.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"72148924-6373-4368-838B-B9200454C701"}
 */
function onRecordSelection(event) {
	_super.onRecordSelection(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} status the event that triggered the action
 * @public
 * @properties={typeid:24,uuid:"4A6F5C90-0752-435B-867E-4335BADAD7BA"}
 * @SuppressWarnings(wrongparameters)
 */
function setBulkStatus(event, status) {
	application.output(globals.messageLog + 'START approval_expenses_log_list.setBulkStatus() status ' + status, LOGGINGLEVEL.INFO);
	/** @type {JSFoundSet<db:/geco/expenses_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'expenses_log')

	var hasSelected = false;
	application.output('setBulkStatus  foundset size ' + foundset.getSize());
	for (var i = 1; i <= foundset.getSize(); i++) {
		if (foundset.getRecord(i).is_selected == 1) {
			hasSelected = true;
			break;
		}
	}
	application.output(hasSelected);
	if (!hasSelected) {
		application.output(globals.messageLog + 'approval_expenses_log_list.setBulkStatus() nessun evento selezionato, esce', LOGGINGLEVEL.DEBUG);
		return;
	}
	var rejectionReason = null;
	if (status == 3) rejectionReason = globals.DIALOGS.showInputDialog("Rifiuta", "Inserisci il motivo del rifiuto (obbligatorio)");
	//if (rejectionReason != '' && !globals.isEmpty(rejectionReason) && !rejectionReason.equals(null)) {
		if ((status == 3 && !globals.isEmpty(rejectionReason)) || status == 2 || status==4) {
			application.output('DENTRO');
		var selfApproving = false;
		// start in-memory transaction
		startEditing(event);

		for (var index = 1; index <= foundset.getSize(); index++) {
			var record = foundset.getRecord(index);
			if (record.is_selected) {
				if (record.user_id != security.getUserUID()) {
					events_log.setSatus(record, status, rejectionReason);
					record.is_selected = 0;
					application.output('record_id ' + record.expense_log_id);
					if (status == 3) expensesToNotify.push(record.expense_log_id);
				} else {
					selfApproving = true;
				}
			}
		}
		if (selfApproving) globals.DIALOGS.showInfoDialog('Approving', 'Lo stato dei tuoi eventi non può essere cambiato da te stesso', 'OK')
		// commit transaction
		saveEdits(event);
		allSelected = 0;
	} 
	
	// call applyFilter on filter form
	//fix per webclient approver la 6.1.3. non supporta lo splitpane
	//04-05-2013 commentata la fix, sembra funzionare
	//	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
	//		forms.approval_expenses_log_filter.applyFilter();
	//	} else {
	//		var bundle = forms[controller.getFormContext().getValue(2, 2)];
	//		if (bundle.elements['split']) bundle.elements['split']['getLeftForm']()['applyFilter']();
	//	}
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) bundle.elements['split']['getLeftForm']()['applyFilter']();
	if (expensesToNotify.length > 0)
		sendMailForExpenses(status);
	application.output(globals.messageLog + 'STOP approval_expenses_log_list.setBulkStatus() status ' + status, LOGGINGLEVEL.INFO);
}

/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"2250746F-213A-42CB-B3B8-3AC2E6CCFB3B"}
 */
function updateUI(event) {

	_super.updateUI(event);

	// hide the header and footer images when editing
	//elements.headerImage.visible = elements.footerImage.visible = !isEditing();

	//abilitazione bottoni chiudi/approva/rigetta
	var status = null;
	var is_admin = (globals.hasRole('Controllers') || globals.hasRole('HR Administrators') || globals.hasRole('HR Expenses'))? true:false;

	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle)
		if (bundle.elements['split'])
			status = bundle.elements['split']['getLeftForm']()['selectedStatus'];

	switch (status) {
	case 1:
		elements.buttonApprove.enabled = true;
		elements.buttonReject.enabled = true;
		elements.buttonClose.enabled = false;
		elements.buttonDelete.enabled = is_admin;
		break;
	case 2:
		elements.buttonApprove.enabled = false;
		elements.buttonReject.enabled = true;
		elements.buttonClose.enabled = true
		elements.buttonDelete.enabled = is_admin;
		break;
	case 3:
		elements.buttonApprove.enabled = true;
		elements.buttonReject.enabled = false;
		elements.buttonClose.enabled = false;
		elements.buttonDelete.enabled = is_admin;
		break;
	case 4:
		if (globals.hasRole('Controllers') || globals.hasRole('HR Administrators') || globals.hasRole('HR Expenses')) {
			elements.buttonApprove.enabled = true;
			elements.buttonReject.enabled = true;
			elements.buttonClose.enabled = false;
			elements.buttonDelete.enabled = is_admin;
		} else {
			elements.buttonApprove.enabled = false;
			elements.buttonReject.enabled = false;
			elements.buttonClose.enabled = false;
			elements.buttonDelete.enabled = is_admin;
		}
		break;
	}

	if (status != null) {
		elements.buttonApprove.visible = !isEditing();
		elements.buttonReject.visible = !isEditing();
		elements.buttonClose.visible = !isEditing();
	}
	else elements.buttonDelete.enabled = is_admin;
	
	elements.buttonAdd.visible = !isEditing();
	elements.buttonDelete.visible = !isEditing();

	elements.buttonShift.visible = false;
	//se il foundset esiste ed esiste la relazione
	if (foundset && foundset.expenses_log_to_calendar_months) {
		//		se il mese è chiuso
		if (foundset.expenses_log_to_calendar_months.is_enabled_for_expenses == 0) {
			// se il record è respinto o apert  è possibile spostare la nota spesa e non sono in editing
			if (status == 3 || status == 1) {
				elements.buttonShift.visible = !isEditing();
			}
		}
	}
}

/**
 * Export to file txt
 * @param {JSEvent} event the event that triggered the action
 * @param {String} functionToCall name of function to call in core.globals
 * @protected
 * @properties={typeid:24,uuid:"48812B64-46DF-4524-9913-E6ADAE3FC91B"}
 */
function exportFile(event, functionToCall) {
	//mese e anno di filter
	month = null;
	year = null;
	// get variable  di filter
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) {
		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
		statusId = bundle.elements['split']['getLeftForm']()['selectedStatus'];
	}

	globals[functionToCall](event, year, month, statusId);
}

/**
 * Export to file txt
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"E3F06605-21A2-4A67-B167-2666C65041E8"}
 */
function openUserExpenses(event) {
	//	//mese e anno di filter
	//	var month = null;
	//	var year = null;
	//	var user = null;
	// get variable  di filter
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) {
		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
		userId = bundle.elements['split']['getLeftForm']()['selectedUser'];
		statusId = bundle.elements['split']['getLeftForm']()['selectedStatus'];
	}
	if (month == null || year == null || userId == null) {
		globals.DIALOGS.showErrorDialog('Error', 'Selezionare i campi Anno Mese e Utente', 'OK');
		return;
	}
	//globals[functionToCall](event,year,month);
	var win = application.createWindow("export", JSWindow.MODAL_DIALOG);
	globals.callerForm = forms[controller.getName()];
	win.show(forms.print_user_expenses_log);
}

/**
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"89F3E136-24DD-431A-8609-36F081B89E9C"}
 */
function printExpenses() {
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) {
		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
		userId = bundle.elements['split']['getLeftForm']()['selectedUser'];
		statusId = bundle.elements['split']['getLeftForm']()['selectedStatus'];
	}
	if (month == null || year == null || userId == null) {
		globals.DIALOGS.showErrorDialog('Error', 'Selezionare i campi Anno Mese e Utente', 'OK');
		return;
	}

	if (forms.print_user_expenses_log.foundset.find()) {
		forms.print_user_expenses_log.foundset.user_id = userId;
		forms.print_user_expenses_log.foundset.expense_month = month;
		forms.print_user_expenses_log.foundset.expense_year = year;
		forms.print_user_expenses_log.foundset.expense_log_status = statusId;
		forms.print_user_expenses_log.foundset.search();
		forms.print_user_expenses_log.foundset.sort("expense_date asc, expense_reference asc");
	}

	var win = application.createWindow('print', JSWindow.WINDOW);
	var print_form = forms.print_user_expenses_log;
	win.setInitialBounds(200,100,1000,500)
	win.show(print_form);
}

/**
 * @param {Number} status
 * @AllowToRunInFind *
 * @properties={typeid:24,uuid:"0E3855E6-AB76-4801-A152-638623D90422"}
 */
function sendMailForExpenses(status) {
	application.output(globals.messageLog + 'START approval_expenses_log_list.sendMailForEvents() ', LOGGINGLEVEL.INFO);
	application.output('#### mailApproverExpenses ####');
	var expensesToSearch = expensesToNotify.join("||");
	application.output('ID APPROVATI ' + expensesToSearch.toString() + '');

	/** @type {JSFoundSet<db:/geco/expenses_log>} */
	var expenses_log_approved = databaseManager.getFoundSet('geco', 'expenses_log');
	if (expenses_log_approved.find()) {
		expenses_log_approved.expense_log_id = expensesToSearch;
		expenses_log_approved.search();
		expenses_log_approved.sort('user_id asc, expense_year asc, expense_month asc');
	}
	var mailTypeId = 0;
	if (status == 3) mailTypeId = 7;
	var currentUser = '';
	var period = '';
	for (var index = 1; index <= expenses_log_approved.getSize(); index++) {
		var expense = expenses_log_approved.getRecord(index);
		//		application.output('------- evento -----');
		//		application.output('user_id = ' + expense.user_id);
		//		application.output('data ' + expense.expense_date.toLocaleDateString());
		//		application.output('tipo spesa' + expense.expense_type_id);
		//		application.output('--------------------');
		var expenseNext = (index + 1 <= expenses_log_approved.getSize()) ? expenses_log_approved.getRecord(index + 1) : null;
		if (expenseNext != null) {
			//			application.output('------- next -----');
			//			application.output('user_id = ' + expenseNext.user_id);
			//			application.output('event = ' + expenseNext.expense_date.toLocaleDateString());
			//			application.output('tipo spesa' + expenseNext.expense_type_id);
			//			application.output('--------------------');
		} else {
			//			application.output('nessuna spesa successivo dovrebbe uscire');
		}

		currentUser = expense.user_id
		period = period + expense.expense_month + '/' + expense.expense_year + ' - ';
		var rejectReason = expense.note;
		if (expenseNext != null && (expense.expense_month + '' + expense.expense_year) != expenseNext.expense_month + '' + expenseNext.expense_year)
			period = period + expense.expense_month + '/' + expense.expense_year + ' - ';
		application.output(period);
		if (expenseNext != null) {
			if (currentUser != expenseNext.user_id) {
				//utente diverso dal successivo
				globals.approverSendMail(mailTypeId, expense.expenses_log_to_users, null, period, rejectReason);
				period = '';
			} else {
				if ( (expense.expense_month + '' + expense.expense_year) == expenseNext.expense_month + '' + expenseNext.expense_year)
					period = '';
			}
		} else {
			//non c'è altra spesa dopo
			globals.approverSendMail(mailTypeId, expense.expenses_log_to_users, null, period, rejectReason);
		}
	}
	expensesToNotify = []
	application.output(globals.messageLog + 'STOP approval_expenses_log_list.sendMailForEvents() ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"77BBCD03-252A-4436-9E0E-535EBF05A89D"}
 */
function shitfMonth(event) {
	//mese e anno di filter
	month = null;
	year = null;
	// get variable  di filter
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) {
		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
	}
	if (month == 12) {
		year = year + 1;
		month = 1;
	} else {
		month = month + 1;
	}
	/** @type {JSFoundSet<db:/geco/expenses_log>} */
	var expenses_log = databaseManager.getFoundSet('geco', 'expenses_log')

	var hasSelected = false;
	application.output('shift size ' + foundset.getSize());
	for (var i = 1; i <= foundset.getSize(); i++) {
		if (foundset.getRecord(i).is_selected == 1) {
			hasSelected = true;
			break;
		}
	}
	application.output(hasSelected);
	if (!hasSelected) {
		application.output('nessun evento selezionato, esce');
		return;
	}
	startEditing(event);
	for (var index = 1; index <= foundset.getSize(); index++) {
		var record = foundset.getRecord(index);
		if (record.is_selected) {
			expenses_log.shiftMonth(record, month, year);
			record.is_selected = 0;
			application.output('record_id ' + record.expense_log_id);
		}
	}
	saveEdits(event);
	allSelected = 0;
	//	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
	//		forms.approval_expenses_log_filter.applyFilter();
	//	} else {
	//		if (bundle.elements['split']) bundle.elements['split']['getLeftForm']()['applyFilter']();
	//	}
	bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) bundle.elements['split']['getLeftForm']()['applyFilter']();
}


/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"7B66406C-FCEF-4ECC-959D-402F8EB722E9"}
 */
function newRecord(event) {
	application.output(globals.messageLog + 'START approval_expenses_log_list.newRecord() ', LOGGINGLEVEL.DEBUG);
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) {
		userId = bundle.elements['split']['getLeftForm']()['selectedUser'];
		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
	}
	if (userId == null) {
		globals.DIALOGS.showErrorDialog('Error', 'Selezionare l\'utente ', 'Chiudi');
		return;
	}
	if (startEditing(event)) {
		foundset.newRecord(true);
		foundset.user_id = userId;
		foundset.expense_month = month;
		foundset.expense_year = year;
	}
	application.output(globals.messageLog + 'STOP approval_expenses_log_list.newRecord() ', LOGGINGLEVEL.DEBUG);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @properties={typeid:24,uuid:"C1DF0941-74D4-4BD3-ABF4-3E833425218A"}
 */
function deleteRecord(event, index) {
	application.output(globals.messageLog + 'START approval_expenses_log_list.deleteRecord() ', LOGGINGLEVEL.INFO);
	var record = foundset.getSelectedRecord();
	var mailTypeId = 12;
	var currentUser = record.expenses_log_to_users;
	var currentExpenseType = record.expenses_log_to_expense_types.expense_type;
	var period = record.expense_date.toLocaleDateString() + ' nella Nota spese di ' + record.expense_month + '-' + record.expense_year;
	application.output(globals.messageLog + 'approval_expenses_log_list.deleteRecord() currentUser ' + currentUser + ' currentExpenseType ' + currentExpenseType + ' period ' + period, LOGGINGLEVEL.INFO);

	if (_super.deleteRecord(event, index)) {
		application.output(globals.messageLog + 'approval_expenses_log_list.deleteRecord() OK ', LOGGINGLEVEL.INFO);
		var deleteReason = globals.DIALOGS.showInputDialog("Cancellazione", "Inserisci il motivo della cancellazione");
		globals.approverSendMail(mailTypeId, currentUser, currentExpenseType, period, deleteReason);
		application.output(globals.messageLog + 'approval_expenses_log_list.deleteRecord() inviata mail ', LOGGINGLEVEL.INFO);
	} else application.output(globals.messageLog + 'approval_events_log_list.deleteRecord() ERROR ', LOGGINGLEVEL.INFO);

	application.output(globals.messageLog + 'STOP approval_expenses_log_list.deleteRecord() ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B0678AB9-42CF-489F-87C7-40F71DF252FE"}
 */
function openPopupPeriodEnable(event) {
	//application.createWindow("period enable", JSWindow.MODAL_DIALOG).show(forms.period_popup_expenses_log);
	var win = application.createWindow("period enable", JSWindow.MODAL_DIALOG);
	win.setInitialBounds(400, 0, 430, 525);
	win.title = 'Disabilita mesi'
	win.resizable = true;
	databaseManager.setAutoSave(true);
	win.show(forms.period_popup_expenses_log);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"D8047864-5276-4C35-A39C-99D4DF1B52AF"}
 * @AllowToRunInFind
 */
function deleteExpenses(event) {
	application.output(globals.messageLog + 'START approval_expenses_log_list.deleteExpenses ', LOGGINGLEVEL.INFO);
	/** @type {JSRecord<db:/geco/expenses_log>} */	
	var expensesLogToDelete = [];
	var hasSelected = false;
	var index = 0;
	application.output(globals.messageLog + 'approval_expenses_log_list.deleteExpenses foundset size ' + foundset.getSize(), LOGGINGLEVEL.DEBUG);
	for (var i = 1; i <= foundset.getSize(); i++) {
		if (foundset.getRecord(i).is_selected == 1) {
			hasSelected = true;
			break;
		}
	}
	if (!hasSelected) {
		application.output(globals.messageLog + 'approval_expenses_log_list.deleteExpenses() nessun evento selezionato, esce', LOGGINGLEVEL.DEBUG);
		return;
	}
	var success = false;
	var answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai per eliminare dei record.\nQuesta operazione non può essere annullata", "Elimina", "Annulla");
	//var answer = 'Elimina'
	if (answer == "Elimina") {

		var deleteReason = globals.DIALOGS.showInputDialog("Cancellazione", "Inserisci il motivo della cancellazione");
		//var deleteReason = 'cancellzione spesa'
		if (deleteReason != null && deleteReason != '') {

			for (index = 1; index <= foundset.getSize(); index++) {
				var record = foundset.getRecord(index);
				if (record.is_selected) {
					expensesToNotify.push(record.expense_log_id);
					expensesLogToDelete.push(record);
				}
			}
			var eventsToSearch = expensesToNotify.join("||");
			//application.output('da cancellare '  +eventsToSearch );
			/** @type {JSFoundSet<db:/geco/expenses_log>} */
			var expenses_log_to_mail = databaseManager.getFoundSet('geco', 'expenses_log');
			if (expenses_log_to_mail.find()) {
				expenses_log_to_mail.expense_log_id = eventsToSearch;
				expenses_log_to_mail.search();
				expenses_log_to_mail.sort('user_id asc, expense_year asc, expense_month asc');
			}

			var notifyList = [];

			var mailTypeId = 12;
			var currentUser = '';
			var period = '';
			for (var del = 1; del <= expenses_log_to_mail.getSize(); del++) {

				var expense = expenses_log_to_mail.getRecord(del);

				var expenseNext = (del + 1 <= expenses_log_to_mail.getSize()) ? expenses_log_to_mail.getRecord(del + 1) : null;

				currentUser = expense.user_id
				period = period + expense.expense_month + '/' + expense.expense_year + ' - ';

				if (expenseNext != null && (expense.expense_month + '' + expense.expense_year) != expenseNext.expense_month + '' + expenseNext.expense_year)
					period = period + expense.expense_month + '/' + expense.expense_year + ' - ';
				//application.output('periodo ' + period);
				var objNotify = {
					usr: null,
					type: null,
					period: null,
					reason: null
				}
				objNotify.usr = expense.expenses_log_to_users;
				objNotify.type = expense.expenses_log_to_expense_types.expense_type;
				objNotify.period = period
				objNotify.reason = deleteReason;
				//application.output(objNotify.usr + ' ' + objNotify.type + ' ' + objNotify.period + ' ' + objNotify.reason);
				if (expenseNext != null) {
					if (currentUser != expenseNext.user_id) {
						//utente diverso dal successivo
						notifyList.push(objNotify);
						period = '';
					} else {
						if ( (expense.expense_month + '' + expense.expense_year) == expenseNext.expense_month + '' + expenseNext.expense_year) {
							//evento diverso dal successivo
							period = '';
						}
					}
				} else {
					//non c'è altro evento dopo
					notifyList.push(objNotify);
				}
			}

			application.output(globals.messageLog + 'approval_expenses_log_list.deleteExpenses() eventi da cancellare ' + expensesLogToDelete.length, LOGGINGLEVEL.DEBUG);
			//application.output('numero record ' + foundset.getSize());
			for (index = 0; index < expensesLogToDelete.length; index++) {
				application.output(index);
				//var recordToDel = expensesLogToDelete[index];
				//application.output(recordToDel);
				success = foundset.deleteRecord(expensesLogToDelete[index]);
				application.output(globals.messageLog + 'approval_expenses_log_list.deleteExpenses() record cancellato ' + success, LOGGINGLEVEL.DEBUG);
			}
			application.output(globals.messageLog + 'approval_expenses_log_list.deleteExpenses() invio mail ', LOGGINGLEVEL.DEBUG);

			for (var idx = 0; idx < notifyList.length; idx++) {
				objNotify = notifyList[idx];
				globals.approverSendMail(mailTypeId, objNotify.usr, objNotify.type, objNotify.period, objNotify.reason);
			}

			expensesToNotify = []
			allSelected = 0;
		}
	}
	application.output(globals.messageLog + 'STOP approval_events_log_list.deleteExpenses() ', LOGGINGLEVEL.INFO);
}
