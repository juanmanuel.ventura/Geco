/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"AE4F210C-1579-447A-BF19-4A0E424E3B1C"}
 */
function updateUI(event) {
	_super.updateUI(event);
	controller.readOnly = !isEditing();

	// buttons
	elements.buttonEdit.visible = !isEditing();
	elements.buttonSave.visible = isEditing();
	elements.buttonCancel.visible = isEditing();

	// get the bundle
	var mainForm = forms[controller.getFormContext().getValue(2, 2)] || null;
	if (mainForm) {
		// get the split pane
		/** @type {RuntimeSplitPane} */
		var split = mainForm.elements['split'];
		// call updateUI in contained forms
		if (split!=null) {
			split.getLeftForm()['updateUI']();
			split.getRightForm()['updateUI']();
		}
	}
}
