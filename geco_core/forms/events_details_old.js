/**
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"A0E92F00-53A5-4CA4-9C71-DF350718D5AC"}
 */
function enableExternalCode(event) {

	if (!foundset.has_job_order) {
		elements.fldExternal_code.enabled = true;
		elements.fldExternal_code.bgcolor = '#ffffff';
	} else {
		elements.fldExternal_code.enabled = false;
		elements.fldExternal_code.bgcolor = '#f8f8f8';
		foundset.external_code_navision = null;
	}

	return true
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C780B6E1-F89A-4D76-AEFC-A76BCA353F29"}
 */
function onRecordSelection(event) {
	globals.selectedEvent = foundset.event_id;
	enableExternalCode(event);
	toggleControls(event);
	return _super.onRecordSelection(event)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"4DB66F4D-0494-4972-8BA2-57D06D9BC73F"}
 */
function saveEdits(event) {
	forms.events_users_list.deleteRecordsSelected(event);
	_super.saveEdits(event)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C40C72E9-EDBB-4B6C-BD37-FD3E6A728F48"}
 */
function stopEditing(event) {
	forms.events_users_list.areAllSelected = 0;
	forms.events_users_list.selectAllRecords(event);
	_super.stopEditing(event)
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"382A838D-FFF7-4949-A952-BCDAD45B505C"}
 */
function onDataChange(oldValue, newValue, event) {
	toggleControls(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"8E2E9D86-CC02-47B9-A63A-2B9D0014CF5A"}
 */
function toggleControls(event) {

	switch (foundset.approver_figure) {
	case 1:
		elements.group_profit_center.visible = true;
		foundset.has_job_order = 0;
		break;
	case 3:
		elements.group_profit_center.visible = false;
		foundset.has_job_order = 1;
		break;
	default:
		elements.group_profit_center.visible = false;
		foundset.has_job_order = 0;
		break;
	}

	enableExternalCode(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"B59F847F-088D-4E62-BA71-E36348BBD8AD"}
 */
function updateUI(event){
	_super.updateUI(event);
	
	if(globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')){
		elements.buttonEdit.visible = false;
	}
	
	if (!isEditing()) foundset.sort('event_name asc');
	
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		bundle.elements['split'].visible = !isEditing();
		bundle['focus'] = foundset.event_name;
		bundle.elements['focus'].visible = isEditing();
	}
}
