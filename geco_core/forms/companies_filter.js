/**
 * @type {Number}
 * @properties={typeid:35,uuid:"07B1FA8F-6CF6-4080-83A5-0FA40CF867C7",variableType:8}
 */
var company = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"8151D885-7A27-4DA6-9CCB-F6E5E15E0859",variableType:8}
 */
var companyType = null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"DCD517BD-76B9-47FE-A392-6C4E97A1875E"}
 */
function onDataChange(oldValue, newValue, event) {
	if (newValue |= oldValue) {
		applyFilter(event);
	}
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"BCC7E38D-DE85-4556-B1C8-C8266983A345"}
 */
function resetFilter(event, goTop) {
	company = null;
	companyType = null;
	_super.resetFilter(event, goTop);
	foundset.sort('company_type_id asc, company_name asc')
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"B060ADE7-C3B0-4D27-ABDE-BFD5573D1C12"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.company_id = company;
		foundset.company_type_id = companyType;
		foundset.search();
		foundset.sort('company_name asc')
	}
}
