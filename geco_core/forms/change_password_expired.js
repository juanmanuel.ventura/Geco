/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"39D9C674-2F38-49E9-8B3C-A5760007B17B"}
 */
var resultMessage = null;

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @protected 
 *
 * @properties={typeid:24,uuid:"E3E556C6-DADA-4259-9F9B-408088833CFB"}
 * @AllowToRunInFind
 */
function savePasswordExpired(event) {
	application.output(globals.messageLog + 'START change_password.savePasswordExpired() ',LOGGINGLEVEL.INFO);
	resultMessage = null;
	if (!newPassword) {
		resultMessage = 'La nuova password non può essere vuota';
		
	} else if (!repeatNewPassword || repeatNewPassword != newPassword) {
		resultMessage = 'Le due password non corrispondono';
		
	}
	if (resultMessage != null) {
		application.output(globals.messageLog + 'STOP change_password.savePasswordExpired() error message: '+ resultMessage,LOGGINGLEVEL.INFO);
		globals.DIALOGS.showInfoDialog('Errore',resultMessage,'OK');
		return;
	}
	try {
		_super.hasUserMedicalPractice();
		globals.changeExpiredPassword(username, oldPassword, newPassword);
		var serverUrl = application.getServerURL();
		var solutionName = application.getSolutionName();
		application.output(globals.messageLog + 'change_password.savePasswordExpired() serverUrl ' + application.getServerURL(), LOGGINGLEVEL.DEBUG);
		application.output(globals.messageLog + 'change_password.savePasswordExpired() solutionName ' + solutionName, LOGGINGLEVEL.DEBUG);
		application.showURL(serverUrl + '/servoy-webclient/application/solution/' + solutionName, '_self');
		security.logout(application.getSolutionName());
	} catch (e) {
		resultMessage = e['message'];
		globals.DIALOGS.showInfoDialog('Errore', resultMessage, 'OK');
		application.output(globals.messageLog + 'change_password.savePasswordExpired() ERROR ' + resultMessage, LOGGINGLEVEL.ERROR);
	}
	application.output(globals.messageLog + 'STOP change_password.savePasswordExpired() ', LOGGINGLEVEL.INFO);

}
