/**
 * @type {Number}
 * @properties={typeid:35,uuid:"48134810-D6CE-4BDE-945D-97C92EDFD9C5",variableType:4}
 */
var areAllSelected = 0;

/**
 * Select all records
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"D1331D97-83A6-4961-B589-4D5CF987CD45"}
 */
function selectAllRecords(event) {
	for (var index = 1; index <= foundset.getSize(); index++) {
		var rec = foundset.getRecord(index);
		rec['is_selected'] = areAllSelected;
	}
}

/**
 * Perform the single selection.
 *
 * @param {JSEvent} event the event that triggered the action
 * @protected
 *
 * @properties={typeid:24,uuid:"E7D47431-1665-4112-8BF4-82312DD772AC"}
 */
function deselectOthers(event) {
	var me = foundset.getSelectedIndex();
	for (var index = 1; index <= foundset.getSize(); index++) {
		if (index != me && foundset.getRecord(index)['is_selected']) foundset.getRecord(index)['is_selected'] = 0;
	}
	return true;
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"11FC60A4-C19F-42EF-92BD-468B74AAA4FA"}
 */
function updateUI(event) {
	_super.updateUI(event);
	// buttons
	if (!isEditing()) {
		areAllSelected = 0;
		selectAllRecords(event);
	}
	return true;
}

/**
 * Check if one record is selected
 * @return {Boolean}
 * @properties={typeid:24,uuid:"20B7CD55-478A-4DFC-8CD5-DD2A83E4E799"}
 */
function hasChanged() {
	if (foundset && foundset.getSize() != 0) {
		for (var index = 1; index <= foundset.getSize(); index++) {
			if (foundset['is_selected']) return true;
		}
	}
	return false;
}

/**
 * Delete records selected
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"B3134708-7A98-49E1-9956-444342E08C67"}
 */
function deleteRecordsSelected(event) {
	if (hasChanged()) {
		/** @type {Array<JSRecord>} */
		var recordsToDelete = []

		if (foundset) {
			for (var index = 1; index <= foundset.getSize(); index++) {
				var rec = foundset.getRecord(index);
				if (rec['is_selected']) recordsToDelete.push(rec);
			}
			deleteRecordsAtOnce(event, recordsToDelete)
		}
	}
}
