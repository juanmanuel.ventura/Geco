/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"135EF5D7-DA75-486D-BAD6-400412C8D828"}
 */
function resetFilter(event) {
	 _super.resetFilter(event);
	selectedYear = new Date().getFullYear();
	applyFilter(event);
}

/**
 * @param firstShow
 * @param event
 *
 * @properties={typeid:24,uuid:"2BAD65C1-D2D6-44B7-B941-F99D5AA4C15F"}
 */
function onShow(firstShow, event) {
	databaseManager.setAutoSave(false);
	return _super.onShow(firstShow, event)
}