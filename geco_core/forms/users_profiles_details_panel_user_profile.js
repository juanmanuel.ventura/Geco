/**
 * @type {String}
 * @properties={typeid:35,uuid:"A21E0E6F-AA94-42AE-B215-E0D795C2BB11"}
 */
var sortDirection = 'asc';

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"927FED12-3506-45D9-9E6E-45B32CF51A19"}
 */
function doSort(event) {
	if (foundset.getSize() > 1) {
		var label = event.getSource();
		// change color back to the original one
		if (label) label.fgcolor = '#727784';
		// (re) set the triggered label
		label = event.getSource();
		// (re) change color to indicate selection
		label.fgcolor = '#990000';
		var sortKey = label.text;

		switch (sortKey) {
		case 'Data dal':
			controller.sort('profile_user_start_date' + ' ' + sortDirection);
			break;
		case 'Matricola':
			controller.sort('profiles_users_to_users.users_to_contacts.real_name' + ' ' + sortDirection);
			break;
		case 'Costo giornaliero':
			controller.sort('prof_cost_schedule' + ' ' + sortDirection);
			break;
		case 'Costo orario':
			controller.sort('profiles_users_to_profiles.profiles_to_profiles_costs.prof_cost_daily' + ' ' + sortDirection);
			break;
		}

		// toggle the sort order
		sortDirection = (sortDirection == 'asc') ? 'desc' : 'asc';
	}
}
