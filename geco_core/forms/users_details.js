/**
 * @type {Number}
 * @properties={typeid:35,uuid:"72EEE030-019F-4ABB-9C6E-E3E71869A2D5",variableType:4}
 */
var oldValueMedicalPractice = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"E100DF28-D47C-481F-8DDA-81F71CFAB50C"}
 */
var resultMessage = null;

/**
 * @properties={typeid:35,uuid:"C632B3C0-3D31-4D0E-B751-D1B004C85061",variableType:-4}
 */
var client = null;

/**
 * Handle record selected.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"2DF652A5-5EAD-4097-BF2B-1D6AFBCEDEB9"}
 */
function onRecordSelection(event) {
	globals.selectedUser = foundset.user_id;
	return _super.onRecordSelection(event)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"B999EF86-49F2-4877-86BD-C02B5A33669A"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
	application.output(globals.messageLog + 'START users_details.saveEdits() ', LOGGINGLEVEL.INFO);
	var changeDate = false;
	var changeContract = false;
	var changeEnabled = false;
	var record = foundset.getSelectedRecord();
	var dataset = record.getChangedData();
	/** @type Date */
	var newDate = null;
	/** @type Date */
	var oldDate = null;
	var tot = 0;
	for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
		//  se nel dataset il campo precedente ore lavorative è stato cambiato
		if (dataset.getValue(i, 1) == 'working_hours_start_date' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			application.output(globals.messageLog + 'users_details.saveEdits() data cambiata ', LOGGINGLEVEL.DEBUG);

			changeDate = true;
			oldDate = dataset.getValue(i, 2);
			newDate = dataset.getValue(i, 3);
			break;
		}
		if (dataset.getValue(i, 1) == 'contract_type_id' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			application.output(globals.messageLog + 'users_details.saveEdits() tipo contratto cambiato ', LOGGINGLEVEL.DEBUG);

			changeContract = true;
			break;
		}
		if (dataset.getValue(i, 1) == 'is_enabled' && (dataset.getValue(i, 2) != dataset.getValue(i, 3)) && (dataset.getValue(i, 2) == 1 && dataset.getValue(i, 3) == 0)) {
			application.output(globals.messageLog + 'users_details.saveEdits() is_enabled cambiato ', LOGGINGLEVEL.DEBUG);

			changeEnabled = true;
			break;
		}
	}
	if (changeEnabled) {
		/** @type {JSFoundSet<db:/geco/users_job_orders>} */
		var usersJobOrders = record.users_to_users_job_orders;
		var msg = 'Non è possibile disabilitare l\'utente, ha eventi o NS per le commesse:\n';
		var notDelete = false;
		var total = usersJobOrders.getSize();
		/** @type {JSRecord<db:/geco/users_job_orders>} */		
		var recToDel = [];
		for (var x = 1; x <= total; x++) {
			application.output('totale ' + total);
			var rec = usersJobOrders.getRecord(x);
			if (checkDeletion(rec)) recToDel.push(rec);
			else {
				msg = msg + ' - ' + rec.users_job_orders_to_job_orders.external_code_navision;
				notDelete = true;
			}
		}
		for (var y = 0; y < recToDel.length; y++) {
			try {
				application.output(globals.messageLog + 'users_details.saveEdits() cancellazione ' + recToDel[y], LOGGINGLEVEL.DEBUG);
				usersJobOrders.deleteRecord(recToDel[y]);
			} catch (e) {
				application.output(globals.messageLog + 'ERROR users_details.saveEdits() CANCELLAZIONE FATTITA  ', LOGGINGLEVEL.ERROR);
				//msg = globals.validationExceptionMessage + ' - ' + rec.users_job_orders_to_job_orders.external_code_navision + ' - ';
			}
		}
		if (notDelete) {
			application.output(globals.messageLog + 'STOP users_details.saveEdits() Disabilitazione fallita', LOGGINGLEVEL.INFO);
			globals.DIALOGS.showErrorDialog('Error', msg, 'OK');
			return;
		}
	}
	var date = record.working_hours_start_date;
	if (date != null) {
		var day = '' + (date.getDate());
		var month = '' + (date.getMonth() + 1);
		var dateStr = date.getFullYear() + '-' + (month.length > 1 ? month : '0' + month) + '-' + (day.length > 1 ? day : '0' + day);
		if (changeDate || changeContract) {
			/** @type {JSFoundSet<db:/geco/events_log>} */
			var events_log = databaseManager.getFoundSet('geco', 'events_log');
			if (events_log.find()) {
				events_log.user_id = record.user_id;
				events_log.event_log_status = 1;
				events_log.event_id = '!' + 34;
				if (newDate != null) {
					if (newDate > oldDate) {
						events_log.event_log_date = oldDate + '...' + newDate + '|yyyy-MM-dd';
					} else {
						events_log.event_log_date = newDate + '...' + oldDate + '|yyyy-MM-dd';
					}
				} else {
					events_log.event_log_date = '>=' + dateStr + '|yyyy-MM-dd';
				}
				tot = events_log.search();
			}
			//if (newDate < oldDate || newDate > oldDate) {
			if (tot > 0) {
				var answer = null;
				answer = globals.DIALOGS.showWarningDialog("Conferma", "Ci sono già eventi consuntivati per quest'utente,proseguire?", "Si", "No");
				if (answer == 'No') return;
			}
		}
	}
	forms.users_events_list.deleteRecordsSelected(event);
	forms.users_contact_details_panel.deleteRecordsSelected(event);
	forms.users_job_order_list.deleteRecordsSelected(event);
	if (globals.deleteExceptionMessage != null && globals.deleteExceptionMessage != '') {
		application.output('1job_order_save ' + globals.deleteExceptionMessage);
		globals.DIALOGS.showErrorDialog('Error', globals.deleteExceptionMessage, 'OK');
	}
	globals.deleteExceptionMessage = null;

	//	JStaffa inizio controlli su profiles_users
	var recsEditedPU = databaseManager.getEditedRecords(foundset.users_to_profiles_users);
	if (recsEditedPU.length > 0) {
		for (var index = 0; index < recsEditedPU.length; index++) {
			/** @type {JSRecord<db:/geco/profiles_users>} */
			var recEdited = recsEditedPU[index];
			var ds = recsEditedPU[index].getChangedData();
			for (var n = 1; n <= ds.getMaxRowIndex(); n++) {
				if (!recEdited.isNew()) {
					var currentID = recEdited.profile_user_id;
					var lastID = foundset.users_to_profiles_users.getLastID(recEdited.user_id);
					//se non sto modificando l'ultimo
					if (currentID != lastID) {
						globals.DIALOGS.showErrorDialog('Error', '- Impossibile modificare il record selezionato, non è il più recente', 'OK');
						return;
					}
					//se è stata modificata la data
					if (ds.getValue(n, 1) == 'profile_user_start_date' && ds.getValue(n, 2) != ds.getValue(n, 3)) {
						//					if (ds.getValue(n, 2) != null) {
						application.output('----------DATA MODIFICATA; VECCHIA: ' + globals.getDateStringFormat(ds.getValue(n, 2)) + '; NUOVA: ' + ds.getValue(n, 3));
						//valida che la nuova data inserita non sia minore dell'ultima
						if (ds.getValue(n, 3) < ds.getValue(n, 2)) {
							globals.DIALOGS.showErrorDialog('Error', '- Il campo Data inizio validità del record da modificare deve essere superiore rispetto all\'ultima Data inizio validità esistente', 'OK');
							return;
						}
						//						/** @type {JSFoundSet<db:/geco/profiles_users>} */
						//						var profiles_users = databaseManager.getFoundSet('geco', 'profiles_users');
						//						if (profiles_users.find()) {
						//							profiles_users.user_id = recEdited.user_id;
						//							profiles_users.search();
						//
						//							//valida che la nuova data inserita non sia minore dell'ultima
						//							if (recEdited.profile_user_start_date < profiles_users.max_profile_user_start_date) {
						//								globals.DIALOGS.showErrorDialog('Error', '- Il campo Data inizio validità del record da modificare deve essere superiore rispetto all\'ultima Data inizio validità esistente', 'OK');
						//								return;
						//							}
						//						}
						//					}
					}//se è stato modificato il profilo
					//				else if (ds.getValue(n, 1) == 'profile_id' && ds.getValue(n, 2) != ds.getValue(n, 3)) {
					//					if (ds.getValue(n, 2) != null) {
					//						application.output('----------PROFILO MODIFICATO; VECCHIO: ' + ds.getValue(n, 2) + '; NUOVO: ' + ds.getValue(n, 3));
					//					}
					//				}
				}
			}
		}
	}
	//FINE controlli profiles_users

	//	var z = 1;
	//	if (z == 1) return;

	//se non c'è il check su pratiche sanitarie salvo liscio, visto che sono arrivato qui senza errori.
	if (foundset.has_medical_practice == 0 || foundset.has_medical_practice == null) {
		_super.saveEdits(event);
		application.output(globals.messageLog + 'STOP users_details.saveEdits() ', LOGGINGLEVEL.INFO);
	}//JS CONTROLLI PRATICHE SANITARIE\\
	else {
		if (forms.users_details_panel_other.valMedicalPractice != null) {
			//selezionata CHECKBOX ma mancano una o entrambe le date
			if (scopes.globals.isEmpty(foundset.mp_start_date) || scopes.globals.isEmpty(foundset.mp_end_date)) {
				scopes.globals.DIALOGS.showErrorDialog('Error', 'Hai selezionato Pratiche Sanitarie, inserisci entrambe le date.', 'OK');
				return;
			}
			//le date ci sono ma la data di fine copertura è inferiore all'inizio
			if (foundset.mp_end_date < foundset.mp_start_date) {
				scopes.globals.DIALOGS.showErrorDialog('Error', 'La data di fine copertura assicurativa non può essere minore dell\'inizio della copertura stessa.', 'OK');
				return;
			}
			var response = globals.DIALOGS.showInfoDialog('ATTENZIONE', 'Procedendo, verrà creata l\'utenza sull\'applicazione "Pratiche Sanitarie".\nUna volta confermato, non sarà possibile togliere il flag dall\'utente ma solo modificare la data di Fine Copertura Assicurativa.\n\nProcedere con l\'operazione?', 'PROCEDI', 'ANNULLA');
			if (response != 'PROCEDI') return;

			forms.users_details_panel_other.valMedicalPractice = null;
			var startDate = foundset.mp_start_date;
			var endDate = foundset.mp_end_date;
			application.output(globals.messageLog + '---------------START DATE: ' + startDate + '; END DATE: ' + endDate, LOGGINGLEVEL.DEBUG);

			var details = {
				user_name: foundset.user_name,
				start_date: startDate,
				end_date: endDate,
				real_name: foundset.users_to_contacts.real_name,
				first_name: foundset.users_to_contacts.first_name,
				last_name: foundset.users_to_contacts.last_name,
				company_name: 'SPINDOX',
				user_password: foundset.user_password,
				fep_code: foundset.fep_code,
				e_mail: foundset.users_to_contacts.contacts_channels.channel_value,
				iban: foundset.users_to_contacts.iban,
				geco_user_id: foundset.user_id,
				fiscal_code: foundset.users_to_contacts.national_insurance_number
			};

			client = plugins.headlessclient.createClient("mp_queue", null, null, null);
			if (client != null && client.isValid()) {
				application.output(globals.messageLog + 'users_details.saveEdits() Cient OK ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
				client.queueMethod(null, "createMPUserFromGecoPersonnel", [details, application.getServerURL()], enqueueMPUserFromGecoPersonnel);
			} else {
				application.output(globals.messageLog + 'users_details.saveEdits() Client is not valid ', LOGGINGLEVEL.DEBUG);
				globals.DIALOGS.showErrorDialog('Error', 'Client is not valid. Please contact the support at: geco_helpdesk@spindox.it', 'OK');
				return;
			}
			application.output(globals.messageLog + 'STOP users_details.saveEdits() ', LOGGINGLEVEL.INFO);
		} else {
			if (forms.users_details_panel_other.endValidDate == true) {
				var newAnswer = globals.DIALOGS.showInfoDialog('ATTENZIONE', 'Procedendo modificherai la data di Fine Copertura Assicurativa.\n\nProcedere con l\'operazione?', 'PROCEDI', 'ANNULLA');
				if (newAnswer != 'PROCEDI') return;
				var detailsToModify = {
					user_name: foundset.user_name,
					end_date: foundset.mp_end_date
				};

				client = plugins.headlessclient.createClient("mp_queue", null, null, null);
				if (client != null && client.isValid()) {
					application.output(globals.messageLog + 'users_details.saveEdits() Cient OK ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
					client.queueMethod(null, "modifyMPUserFromGecoPersonnel", [detailsToModify, application.getServerURL()], enqueueModifiedMPUserFromGecoPersonnel);
				} else {
					application.output(globals.messageLog + 'users_details.saveEdits() Client is not valid ', LOGGINGLEVEL.DEBUG);
					globals.DIALOGS.showErrorDialog('Error', 'Client is not valid. Please contact the support at: geco_helpdesk@spindox.it', 'OK');
					return;
				}
				application.output(globals.messageLog + 'STOP users_details.saveEdits() ', LOGGINGLEVEL.INFO);
			} else {
				if (forms.users_details_panel_other.endDateNull == 1) {
					globals.DIALOGS.showErrorDialog('Error', 'La data di fine copertura assicurativa non può essere vuota', 'OK');
					return;
				}
				_super.saveEdits(event);
			}
		}
	}
}

/**
 * @param {JSEvent} headlessCallback
 * @properties={typeid:24,uuid:"E0995E63-CE1D-4DBA-BE04-42B04C2B593C"}
 */
function enqueueMPUserFromGecoPersonnel(headlessCallback) {
	application.output(globals.messageLog + 'START users_details.enqueueMPUserFromGecoPersonnel() ', LOGGINGLEVEL.INFO);
	var strOK = '';
	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		if (headlessCallback.data.success == false) {
			//c'è stato un errore, svuoto i campi delle date e rimetto il check sulle pratiche a 0;
			foundset.has_medical_practice = 0;
			mp_start_date = null;
			mp_end_date = null;
			resultMessage = 'Errore nella creazione dell\'utente:\n' + headlessCallback.data.message;
			application.output(globals.messageLog + 'users_details.enqueueMPUserFromGecoPersonnel() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			strOK = globals.DIALOGS.showErrorDialog('ERROR', resultMessage, 'OK');
			if (strOK == 'OK') {
				if (client != null && client.isValid()) {
					application.output(globals.messageLog + 'users_details.saveEdits() close client ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
					client.shutdown();
				}
				application.output(globals.messageLog + 'STOP users_details.enqueueMPUserFromGecoPersonnel() ', LOGGINGLEVEL.INFO);
				return;
			}
		} else {
			application.output(globals.messageLog + 'users_details.enqueueMPUserFromGecoPersonnel() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			if (headlessCallback.data.message == 'Utente creato con successo.') {
				var objMail = globals.mpGetMailTypeFromGecoPersonnel(1);
				var mailSubs = {
					nome: '',
					hrName: '',
					link: ''
				};
				var recipient = foundset.users_to_contacts.contacts_channels.channel_value;
				//var recipient = objMail.distrList;
				var subject = objMail.subject;
				var hostname = application.getServerURL();
				var link = hostname + '/servoy-webclient/application/solution/mp_app';
				//				application.output(link);
				mailSubs.nome = foundset.users_to_contacts.first_name;
				mailSubs.hrName = scopes.globals.currentUserDisplayName;
				mailSubs.link = (link != null) ? link : '';
				var messageMail = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubs);
				application.output(globals.messageLog + 'globals.mpSendMailForNewInsuredUser() SENDING MAIL to distribution list  subject: ' + subject + 'messageMail: ' + messageMail, LOGGINGLEVEL.INFO);

				client.queueMethod(null, "mpSendMail", [recipient, subject, messageMail], scopes.globals.enqueueMailLogForInsuredUsers);
				//Wait 5 seconds before shutting down the client
				application.sleep(5000);
				if (client != null && client.isValid()) {
					application.output(globals.messageLog + 'users_details.enqueueMPUserFromGecoPersonnel() chiudo client ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
					client.shutdown();
				}
			}
			strOK = globals.DIALOGS.showInfoDialog('INFO', headlessCallback.data.message, 'OK');
			if (strOK == 'OK') _super.saveEdits(headlessCallback);
		}
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output(globals.messageLog + 'users_details.saveEdits() exception callback, name: ' + headlessCallback.data + headlessCallback.data.message, LOGGINGLEVEL.DEBUG);
		//c'è stato un errore, svuoto i campi delle date e rimetto il check sulle pratiche a 0;
		foundset.has_medical_practice = 0;
		mp_start_date = null;
		mp_end_date = null;
		resultMessage = 'Errore nella creazione dell\'utente:\n' + headlessCallback.data + ';\n' + headlessCallback.data.message;
		application.output(globals.messageLog + 'users_details.saveEdits() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
		strOK = globals.DIALOGS.showErrorDialog('ERROR', resultMessage, 'OK');
		if (strOK == 'OK') {
			if (client != null && client.isValid()) {
				application.output(globals.messageLog + 'users_details.saveEdits() close client ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
				client.shutdown();
			}
			application.output(globals.messageLog + 'STOP users_details.enqueueMPUserFromGecoPersonnel() ', LOGGINGLEVEL.INFO);
			return;
		}
	}
}
/**
 * @param {JSEvent} headlessCallback
 * @properties={typeid:24,uuid:"CFACB0C5-502A-4CFF-9E99-7ED11580689E"}
 */
function enqueueModifiedMPUserFromGecoPersonnel(headlessCallback) {
	application.output(globals.messageLog + 'START users_details.enqueueModifiedMPUserFromGecoPersonnel() ', LOGGINGLEVEL.INFO);
	var strOK = '';
	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		if (headlessCallback.data.success == false) {
			//c'è stato un errore
			resultMessage = 'Errore nella modifica della data di Fine Copertura Assicurativa:\n' + headlessCallback.data.message;
			application.output(globals.messageLog + 'users_details.enqueueModifiedMPUserFromGecoPersonnel() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			strOK = globals.DIALOGS.showErrorDialog('ERROR', resultMessage, 'OK');
			if (strOK == 'OK') {
				if (client != null && client.isValid()) {
					application.output(globals.messageLog + 'users_details.saveEdits() close client ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
					client.shutdown();
				}
				application.output(globals.messageLog + 'STOP users_details.enqueueModifiedMPUserFromGecoPersonnel() ', LOGGINGLEVEL.INFO);
				return;
			}
		} else {
			application.output(globals.messageLog + 'users_details.enqueueModifiedMPUserFromGecoPersonnel() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			if (headlessCallback.data.message == 'Data di Fine Copertura Assicurativa modificata con successo.') {
				var objMail = globals.mpGetMailTypeFromGecoPersonnel(2);
				var mailSubs = {
					nome: '',
					hrName: ''
				};
				var recipient = foundset.users_to_contacts.contacts_channels.channel_value;
				//				var recipient = objMail.distrList;
				var subject = objMail.subject;
				mailSubs.nome = foundset.users_to_contacts.first_name;
				mailSubs.hrName = scopes.globals.currentUserDisplayName;
				var messageMail = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubs);
				application.output(globals.messageLog + 'globals.mpSendMailForNewInsuredUser() SENDING MAIL to distribution list  subject: ' + subject + 'messageMail: ' + messageMail, LOGGINGLEVEL.INFO);

				client.queueMethod(null, "mpSendMail", [recipient, subject, messageMail], scopes.globals.enqueueMailLogForInsuredUsers);
				//Wait 5 seconds before shutting down the client
				application.sleep(5000);
				if (client != null && client.isValid()) {
					application.output(globals.messageLog + 'users_details.enqueueModifiedMPUserFromGecoPersonnel() chiudo client ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
					client.shutdown();
				}
			}
			strOK = globals.DIALOGS.showInfoDialog('INFO', headlessCallback.data.message, 'OK');
			if (strOK == 'OK') _super.saveEdits(headlessCallback);
		}
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output(globals.messageLog + 'users_details.saveEdits() exception callback, name: ' + headlessCallback.data + headlessCallback.data.message, LOGGINGLEVEL.DEBUG);
		//c'è stato un errore
		resultMessage = 'Errore nella modifica della data di Fine Copertura Assicurativa:\n' + headlessCallback.data + ';\n' + headlessCallback.data.message;
		strOK = globals.DIALOGS.showErrorDialog('ERROR', resultMessage, 'OK');
		if (strOK == 'OK') {
			application.output(globals.messageLog + 'users_details.saveEdits() resultMessage: ' + resultMessage, LOGGINGLEVEL.DEBUG);
			if (client != null && client.isValid()) {
				application.output(globals.messageLog + 'users_details.saveEdits() close client ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
				client.shutdown();
			}
			application.output(globals.messageLog + 'STOP users_details.enqueueModifiedMPUserFromGecoPersonnel() ', LOGGINGLEVEL.INFO);
			return;
		}
	}
}
/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"B26D2F9B-B827-493C-A18E-78A2761090DA"}
 */
function stopEditing(event) {
	forms.users_events_list.areAllSelected = 0;
	forms.users_events_list.selectAllRecords(event);
	_super.stopEditing(event)
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"C0325D90-E52A-4E84-B660-731A9491B5F9"}
 */
function onAction(event) {
	foundset.deleteRecord()
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"31B16188-4D6E-46CE-9C21-ADD048767C40"}
 */
function updateUI(event) {
	_super.updateUI(event);

	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
		elements.buttonEdit.visible = false;
		elements.buttonResetPassword.visible = false;
	}
	if (globals.hasRole('Controllers') && !globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators')) {
		elements.buttonContractTypes.visible = false;
	}

	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		bundle.elements['split'].visible = !isEditing();
		if (foundset.users_to_contacts != null)
			bundle['focus'] = foundset.users_to_contacts.last_name + ' ' + foundset.users_to_contacts.first_name;
		bundle.elements['focus'].visible = isEditing();
	}
	elements.buttonResetPassword.enabled = !isEditing();
	elements.buttonContractTypes.enabled = !isEditing();
	
	
	//JStaffa serve per nascondere il tab dei profili in Personnel/Controller
	//TODO DA TOGLIERE QUANDO CI SARA' RILASCIO SUI COSTI STANDARD
	elements.tabs.removeTabAt(9);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"679353AF-8817-4B2F-AED1-C94F95F19FD0"}
 */
function resetPassword(event) {
	//var record = foundset.getSelectedRecord();
	//databaseManager.setAutoSave(false);
	application.output(globals.messageLog + 'START users_details.resetPassword() ', LOGGINGLEVEL.INFO);
	if (!isEditing()) {
		var answer = globals.DIALOGS.showWarningDialog("Conferma", "Vuoi reimpostare la password predefinita per quest'utente?", "Si", "No");
		if (answer == 'Si') {
			//application.output(foundset);
			//application.output(foundset.getSelectedIndex() + foundset.getSelectedRecord());
			//application.output(' prima ' + foundset.user_password);
			foundset.user_password = globals.sha1('spindox');
			databaseManager.saveData(foundset);
			application.output(globals.messageLog + 'users_details.resetPassword() OK ', LOGGINGLEVEL.INFO);

			/** @type {JSFoundSet<db:/geco/users_password_expiration>} */
			var userspwdex = databaseManager.getFoundSet('geco', 'users_password_expiration');
			var insertResutl = userspwdex.insertNewPassword(foundset.user_id, foundset.user_password, 0, true);
			application.output('esito reset password ' + insertResutl);
			//databaseManager.setAutoSave(true);
			//application.output(' dopo ' + foundset.user_password);
			//application.output(foundset.user_id);
		}
	}
	application.output(globals.messageLog + 'STOP users_details.resetPassword() ', LOGGINGLEVEL.INFO);
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"E9FC7123-B4D0-4AB5-8322-179D619CCA22"}
 */
function onDataChange(oldValue, newValue, event) {
	var locked = oldValue;
	if (locked == 1) {
		foundset.failed_login_attempts = 0;
		//databaseManager.saveData(foundset);
	}
	return true
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"0E46752C-516F-4171-90B9-F4B896692F3A"}
 */
function openPopupSelect(event) {
	var win = application.createWindow("contratto", JSWindow.MODAL_DIALOG);
	globals.callerForm = forms[event.getFormName()];
	win.setInitialBounds(400, 50, 580, 500);
	win.resizable = true;
	databaseManager.setAutoSave(true);
	win.show(forms.users_contract_types_list);
}

/**
 * @param {JSRecord<db:/geco/users_job_orders>} record
 * @return {Boolean}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"38FEDD93-3986-4572-A206-8E8A3018CE48"}
 */
function checkDeletion(record) {
	var totEvents = 0;
	var totExpenses = 0;

	// trova tutti gli eventi per user_id-job_order_id in stato diverso da approvato o chiuso
	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	events_log.loadAllRecords();
	if (events_log.find()) {
		//events_log.event_log_date = '>' + new Date();
		events_log.user_id = record.user_id;
		events_log.job_order_id = record.job_order_id;
		events_log.event_log_status = [1, 3];
		//		events_log.newRecord();
		//		events_log.user_id = record.user_id;
		//		events_log.job_order_id = record.job_order_id;
		//		events_log.event_log_status = 3;
		totEvents = events_log.search();
	}
	application.output('totEvents in stato 1 o 3 per utente ' + totEvents);
	// trova tutte le note spese per user_id-job_order_id in stato diverso da approvato o chiuso
	/** @type {JSFoundSet<db:/geco/expenses_log>} */
	var expenses_log = databaseManager.getFoundSet('geco', 'expenses_log');

	if (expenses_log.find()) {
		expenses_log.user_id = record.user_id;
		expenses_log.job_order_id = record.job_order_id;
		expenses_log.expense_log_status = [1, 3];
		//		expenses_log.newRecord();
		//		expenses_log.user_id = record.user_id;
		//		expenses_log.job_order_id = record.job_order_id;
		//		expenses_log.expense_log_status = 3;
		totExpenses = expenses_log.search();
	}
	application.output('totExpenses in stato 1 o 3 per utente ' + totExpenses);
	//blocca la cancellazione se trova gli eventi
	if (totEvents > 0 || totExpenses > 0) return false;

	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"D3745A7C-6281-4ED5-BB18-657EA9AE832A"}
 */
function startEditing(event) {
	oldValueMedicalPractice = null;
	if (foundset.has_medical_practice == 1 && (!scopes.globals.isEmpty(mp_start_date) && !scopes.globals.isEmpty(mp_end_date))) {
		forms.users_details_panel_other.elements.fMedicalPractices.enabled = false;
		forms.users_details_panel_other.elements.fStartDate.enabled = false;
		forms.users_details_panel_other.elements.fEndDate.enabled = true;
		oldValueMedicalPractice = 1; //se è 1, significa che sto effettuando una modifica e non una nuova creazione
	} else {
		forms.users_details_panel_other.elements.fMedicalPractices.enabled = true;
		forms.users_details_panel_other.elements.fStartDate.enabled = false;
		forms.users_details_panel_other.elements.fEndDate.enabled = false;
	}
	_super.startEditing(event);
}

/**
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"7B3D51D4-CCBA-4E8B-B6CC-DED258C5873C"}
 */
function addMpUserIdToUser() {
	//	application.output(globals.messageLog + 'START users.details.addMpUserIdToUser', LOGGINGLEVEL.INFO);
	//	/** @type {JSFoundSet<db:/geco/users>} */
	//	var users = databaseManager.getFoundSet('geco', 'users');
	//	users.loadAllRecords();
	//	if (users.find()) {
	//		users.user_id = foundset.user_id;
	//		var tot = users.search();
	//		application.output(globals.messageLog + 'users.details.addMpUserIdToUser CERCO PER USER_ID ' + foundset.user_id + '; TOT: ' + tot, LOGGINGLEVEL.DEBUG);
	//	}
	//	if (tot > 0) {
	//		var rec = users.getRecord(1);
	//		if (rec.mp_user_id == null) {
	//			databaseManager.setAutoSave(false);
	//			rec.mp_user_id = globals.mpUserId;
	//			databaseManager.saveData(rec);
	//			application.output(globals.messageLog + 'users.details.addMpUserIdToUser ASSEGNO AL RECORD TROVATO VALORE: ' + globals.mpUserId + '; rec.mp_user_id: ' + rec.mp_user_id, LOGGINGLEVEL.DEBUG);
	//		}
	//	}
	//	application.output(globals.messageLog + 'STOP users.details.addMpUserIdToUser', LOGGINGLEVEL.INFO);
}
