/**
 * @type {String}
 * @properties={typeid:35,uuid:"B216C4C4-786C-41BE-96F3-5066CE657A56"}
 */
var focus = null;

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} [formFilterName]
 * @param {String} formListName
 * @param {String} formDetailsName
 * @param {String} [relation]
 * @protected
 * @properties={typeid:24,uuid:"4952ADDC-10AC-4168-B33A-748C72C9C302"}
 */
function initialize(event, formFilterName, formListName, formDetailsName, relation) {
	var h_filter = 134;
	if (formFilterName && forms[formFilterName]){

		var form = solutionModel.getForm(formFilterName);;
		h_filter = form.getPart(JSPart.BODY).height
	}
	try {
		// set navigator
		if (elements.split.dividerLocation) {
			elements.split.dividerSize = 0;
			elements.split.dividerLocation = h_filter;
		}
	} catch (e) {
//		application.output(e);
//		application.output('errore initialize');
	}
	// load the filter
	if (formFilterName && forms[formFilterName]) {
		elements.split.setLeftForm(forms[formFilterName]);
		// force to apply filter (seems not to work on form's onLoad event)
		if (forms[formFilterName].applyFilter) forms[formFilterName].applyFilter();
	}

	// load the list
	elements.split.setRightForm(forms[formListName]);
	if (elements.tabless.removeAllTabs()) {
		elements.tabless.addTab(forms[formDetailsName], null, null, null, null, null, null, relation || null, null);
	}

}
