/**
 * @type {Number}
 * @protected
 * @properties={typeid:35,uuid:"7A7A74E2-9584-4761-A5A0-FB3F1E6B2630",variableType:4}
 */
var isEnabled = 1;


/**
 * @properties={typeid:24,uuid:"9CE5BB54-F2BC-413B-9E1A-100490612921"}
 * @AllowToRunInFind
 */
function filterEnabledRecords() {
	// filter for entities that implement the field "is_enabled"
	if (foundset.alldataproviders.indexOf('is_enabled') != -1) {
		foundset.removeFoundSetFilterParam('enabled');
		foundset.addFoundSetFilterParam('is_enabled', '=', isEnabled, 'enabled');
		foundset.loadAllRecords();
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} [goTop] true if the cursor must be placed at the first index
 * @properties={typeid:24,uuid:"613EE013-DCA6-41A9-827B-95C07220F8F9"}
 */
function resetFilter(event, goTop) {
	foundset.loadAllRecords();
	// move to the first record (otherwise the cursor remains at the last selected record)
	if (goTop) foundset.setSelectedIndex(1);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"3AF99E25-D7A8-437B-9935-15F9651FDD1C"}
 */
function onLoad(event) {
	filterEnabledRecords();
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"4494CA0D-9231-44D5-97F6-C710F0D098A5"}
 */
function updateUI(event) {
	_super.updateUI(event);
	controller.enabled = !isEditing();
	//elements.imageHeader.visible = !isEditing();
}
