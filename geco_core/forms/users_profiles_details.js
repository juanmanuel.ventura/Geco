/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"74F5B600-69FB-4660-B537-960AC72AFCB4"}
 */
function updateUI(event) {
	_super.updateUI(event);

	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
		elements.buttonEdit.visible = false;
	}

	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		bundle.elements['split'].visible = !isEditing();
		if (foundset.prof_name != null) bundle['focus'] = foundset.prof_name;
		else bundle['focus'] = 'Nuovo Prof. Professionale';
		bundle.elements['focus'].visible = isEditing();
	}

	//Se nuovo Prof. Professionale, disabilito pulsante "Attivo" in basso a destra
	elements.is_enabled.enabled = (foundset && foundset.getSelectedRecord().isNew()) ? false : true;

	//Se filtrato per "Non attivi", disabilito il pulsante "+"
	forms.users_profiles_list.elements.buttonAdd.enabled = forms.users_profiles_filter.foundset.is_enabled == 1 ? true : false;
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"8F3C035A-16FE-4A18-A79A-C64669E90976"}
 */
function saveEdits(event) {
	if (foundset && foundset.getSelectedRecord()) {
		/** @type {JSRecord} */ //Salvo il record da salvare in una variabile
		var recToSave = foundset.getSelectedRecord();
		var strError = '';
		//E' stato messo is_enabled a 0 quando il profilo è assegnato ad almeno un utente, ERRORE
		if (recToSave.is_enabled == 0 && recToSave.profiles_to_profiles_users.getSize() > 0)
			strError = strError + '- Impossibile disabilitare il profilo selezionato perchè associato ad almeno un utente\n';

		//		JStaffa inizio controlli su profiles_costs
		var recsEditedPC = databaseManager.getEditedRecords(foundset.profiles_to_profiles_costs);
		if (recsEditedPC.length > 0) {
			for (var index = 0; index < recsEditedPC.length; index++) {
				/** @type {JSRecord<db:/geco/profiles_costs>} */
				var recEdited = recsEditedPC[index];
				var ds = recsEditedPC[index].getChangedData();
				for (var n = 1; n <= ds.getMaxRowIndex(); n++) {
					if (!recEdited.isNew()) {
						var currentID = recEdited.profile_cost_id;
						var lastID = foundset.profiles_to_profiles_costs.getLastID(recEdited.profile_id);
						//se non sto modificando l'ultimo
						if (currentID != lastID) {
							globals.DIALOGS.showErrorDialog('Error', '- Impossibile modificare il record selezionato, non è il più recente', 'OK');
							return;
						}

						//se è stata modificata la data
						if (ds.getValue(n, 1) == 'prof_cost_start_date' && ds.getValue(n, 2) != ds.getValue(n, 3)) {
								application.output('----------DATA MODIFICATA; VECCHIA: ' + globals.getDateStringFormat(ds.getValue(n, 2)) + '; NUOVA: ' + ds.getValue(n, 3));
								//valida che la nuova data inserita non sia minore dell'ultima
								if (ds.getValue(n, 3) < ds.getValue(n, 2)) {
									globals.DIALOGS.showErrorDialog('Error', '- Il campo Data inizio validità del record da modificare deve essere superiore rispetto all\'ultima Data inizio validità esistente', 'OK');
									return;
								}
						}
					}
				}
			}
		}
		//FINE controlli profiles_costs

		if (!globals.isEmpty(strError)) {
			globals.DIALOGS.showErrorDialog('Errore', strError, 'OK');
			return;
		}

//		var listRecToDel = [];
//		for (var index1 = 1; index1 <= foundset.profiles_to_profiles_costs.getSize(); index1++) {
//			var recUP = foundset.profiles_to_profiles_costs.getRecord(index1);
//			if (recUP.is_selected == 1)
//				listRecToDel.push(recUP);
//		}
//		var retSave = false;
//		retSave = _super.saveEdits(event);
		_super.saveEdits(event);
		foundset.sort('prof_name asc');
//		if (retSave) {
//			for (var del = 0; del < listRecToDel.length; del++) {
//				/** @type {JSRecord<db:/geco/profiles_costs>} */
//				var r = listRecToDel[del];
//				application.output(r);
//				foundset.profiles_to_profiles_costs.deleteRecord(r);
//			}
//		}
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A95857EE-42B5-4595-8E66-FCC263F4930F"}
 */
function stopEditing(event) {
	if (foundset.getSelectedRecord().isNew()) foundset.setSelectedIndex(1);
	_super.stopEditing(event);
}
