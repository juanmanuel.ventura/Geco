/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"077B8D9F-CCD2-4B08-B582-07AA192DF9D1"}
 */
function updateUI(event){
	_super.updateUI(event);
	
	if(globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')){
		elements.buttonEdit.visible = false;
	}
	if (!isEditing()) foundset.sort('company_type_id asc, company_name asc');
	
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		bundle.elements['split'].visible = !isEditing();
		bundle['focus'] = foundset.company_name;
		bundle.elements['focus'].visible = isEditing();
	}
}
