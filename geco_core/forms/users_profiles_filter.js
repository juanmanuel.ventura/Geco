/**
 * @type {Number}
 * @properties={typeid:35,uuid:"E666D4AE-1950-4356-AEE1-1F4BDDDB4D04",variableType:8}
 */
var profileId = null;

/**
 * @param oldValue old value
 * @param newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"9A799B15-AE52-4194-B663-B99E36A67893"}
 */
function onDataChange(oldValue, newValue, event) {
	if (oldValue != newValue) applyFilter();
}

/**
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"E07E0233-87A2-43D7-A0F2-03BB575C790C"}
 */
function applyFilter(){
	if (foundset.find()) {
		foundset.profile_id = profileId;
		foundset.search();
	}
}
/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 * @properties={typeid:24,uuid:"437D11F5-0E59-4205-A644-8FD46A08DC9F"}
 */
function resetFilter(event, goTop) {
	profileId = null;
	return _super.resetFilter(event, goTop);
//	applyFilter();
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"749A921B-2DC4-4008-BCEA-DB08A199F305"}
 */
function onShow(firstShow, event) {
	_super.onShow(firstShow, event);
	if (firstShow) profileId = null;
	applyFilter();
}

/**
 * Handle changed data.
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"E88A8F0D-12B2-4881-8C8F-30112BADB8CF"}
 */
function onDataChangeEnabled(oldValue, newValue, event) {
//	isEnabled = newValue;
//	if(isEnabled == 0){
//		elements.usersc.visible = true;
//		elements.usersc.enabled = true;
//		userId = null;
//		
//	}
//	else {
//		elements.usersc.visible = false;
//		elements.usersc.enabled = false;
//		userId = null;
//	}
//	return true
}
