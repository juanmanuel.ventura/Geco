/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"6B5A7CF4-82DC-4402-A77C-0A372652432D"}
 */
function updateUI(event) {
	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
		elements.buttonAdd.visible = false;
	}
}