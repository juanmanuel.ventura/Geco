/**
 * ID of old RDC
 * @type {Number}
 * @properties={typeid:35,uuid:"78DB905F-342B-4FDA-8A72-63CDA59C7B07",variableType:8}
 */
var oldRDC = null;

/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"F4034DE3-F2CB-4AA1-8A43-251557BF0509",variableType:-4}
 */
var added = false;

/**
 * @type {Array}
 * @properties={typeid:35,uuid:"3C3A9C47-7EF2-49A9-BBD9-2652B14B4452",variableType:-4}
 */
var userList = [];

/**
 * Handle record selected.
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"506A077C-5399-4A88-9A58-F296AE291C45"}
 */
function onRecordSelection(event) {
	scopes.globals.selectedJobOrder = foundset.job_order_id;
	_super.onRecordSelection(event);
}

/**
 * Perform the element default action. *
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"D44954B1-8814-4CD5-8123-75DEAC9C650F"}
 */
function startEditing(event) {
	//set the old RDC
	oldRDC = foundset.user_owner_id;
	globals.userListAddedToJO = [];
	_super.startEditing(event);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"C18F4380-8584-4F8C-B59B-2557605E712C"}
 */
function cancelEditRecord(event) {

	_super.stopEditing(event)
	oldRDC = null;
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"80A7D858-F7AA-449F-A282-A1BF76954D86"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
	application.output(globals.messageLog + 'START job_orders_details.saveEdits() ', LOGGINGLEVEL.INFO);
	var changeRDC = false;
	var changeBO = false;
	var isLoggable = false;
	//var newBoNumber = null;
	var record = foundset.getSelectedRecord();
	if (added == false && !record.isNew()) {
		//se ho cambiato il campo RDC

		//application.output('record ' + record);
		var dataset = record.getChangedData();
		//application.output('dataset ' + dataset);

		for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
			//  se nel dataset il campo precedente user_owner_id è stato cambiato
			if (dataset.getValue(i, 1) == 'user_owner_id' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
				application.output('user_owner_id cambiato');
				changeRDC = true;
				break;
			}
			application.output('cambiato ' + dataset.getValue(i, 1) + ' vecchio valore ' + dataset.getValue(i, 2) + ' nuovo valore ' + dataset.getValue(i, 3));
			if (dataset.getValue(i, 1) == 'customer_order_code' && dataset.getValue(i, 2) != null && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
				application.output('customer_order_code cambiato vecchio valore ' + dataset.getValue(i, 2) + ' nuovo valore ' + dataset.getValue(i, 3));
				changeBO = true;
				/** @type {JSFoundSet<db:/geco/business_opportunities>} */
				var business_opportunities = databaseManager.getFoundSet('geco', 'business_opportunities');

				// se la bo nuova è associata un'altra commessa non posso sceglierla.
				if (business_opportunities.find()) {
					business_opportunities.bo_number = dataset.getValue(i, 3);
					business_opportunities.job_order_id = '!' + record.job_order_id;
					business_opportunities.search();
				}
				if (business_opportunities.getSize() > 0) {
					globals.DIALOGS.showErrorDialog('Error', 'Non è possibile cambiare il codice B.O. La B.O. inserita è già associata ad un\'altra commessa. ', 'OK');
					return;
				}
				//se ha più BO collegate
				if (foundset.job_orders_to_business_opportunities.getSize() > 1) {
					globals.DIALOGS.showErrorDialog('Error', 'Non è possibile cambiare la B.O. Altre B.O. sono collegate a questa commessa ', 'OK');
					return;
				}

				break;
			}

			// se la commessa gestionale diventa consuntivabile
			if (dataset.getValue(i, 1) == 'is_loggable' && dataset.getValue(i, 2) != dataset.getValue(i, 3) && dataset.getValue(i, 3) == 1 && record.job_order_type == 4) {
				application.output('is_loggable della commessa ' + record.job_order_id + ' cambiato');
				isLoggable = true;
				break;
			}
		}
		var answer = 'No'
		if (changeRDC)
			answer = globals.DIALOGS.showWarningDialog("Conferma", "Vuoi aggiungere il nuovo approvatore?", "Si", "No");
		if (answer == 'Si') {
			var dateFrom = new Date();
			var day = dateFrom.getDate();
			var month = dateFrom.getMonth();
			var year = dateFrom.getFullYear();
			foundset.job_orders_to_approvers.newRecord(false);
			foundset.job_orders_to_approvers.user_approver_id = foundset.user_owner_id;
			foundset.job_orders_to_approvers.user_approver_from = new Date(year, month, day);
			added = true;
			return;
		}
		answer = 'No';
		if (changeBO) {
			if (foundset.job_orders_to_jo_details.getSize() > 0)
				answer = globals.DIALOGS.showWarningDialog("Conferma", "Hai modificato il codice B.O. associato. Verrà eliminata la pianificazione esistente. Procedere?", "Si", "No");
		}
		if (answer == 'Si') {

			//			foundset.job_orders_to_business_opportunities.job_order_id = null;
			//			foundset.job_orders_to_business_opportunities.cod_commessa = null;
			if (foundset.job_orders_to_jo_planning.deleteAllRecords())
				foundset.job_orders_to_jo_details.deleteAllRecords();
		}
	}

	forms.job_order_users_list.deleteRecordsSelected(event);
	if (globals.deleteExceptionMessage != null && globals.deleteExceptionMessage != '') {
		application.output('1job_order_save ' + globals.deleteExceptionMessage);
		globals.DIALOGS.showErrorDialog('Error', globals.deleteExceptionMessage, 'OK');
	}
	globals.deleteExceptionMessage = null;
	//delete approver
	var fd = foundset.job_orders_to_approvers;
	try {
		if (fd) {
			for (var index = 1; index <= fd.getSize(); index++) {
				var rec = fd.getRecord(index);
				if (rec.is_selected) {
					application.output(globals.messageLog + 'START job_orders_details.saveEdits() cancellazione approvatore ' + rec.approvers_to_users.users_to_contacts.real_name, LOGGINGLEVEL.INFO);
					var totEvent = 0;
					var totExpenses = 0;
					//controllo che approver non abbia eventi da approvare per il job_order
					/** @type {JSFoundSet<db:/geco/events_log>} */
					var events_log = databaseManager.getFoundSet('geco', 'events_log');
					if (events_log.find()) {
						// distinguere per entity:
						//se job_order ---> job_order_id = record.entity_id
						if (rec.entity == 'job_orders') {
							events_log.job_order_id = rec.entity_id;
						}
						// se profit center ---> tipologia di approvazione dell'evento = responsabile del profit center
						if (rec.entity == 'profit_centers') {
							events_log.events_log_to_events.approver_figure = 2;
						}
						events_log.user_approver_id = rec.user_approver_id;
						//'[1,3]'
						events_log.event_log_status = [1, 3];
						totEvent = events_log.search();
					}
					application.output('totEvent assegnati ad approvatore ' + totEvent);
					// se entity = job_order cerco le NS
					if (rec.entity == 'job_orders') {
						/** @type {JSFoundSet<db:/geco/expenses_log>} */
						var expenses_log = databaseManager.getFoundSet('geco', 'expenses_log');
						if (expenses_log.find()) {
							expenses_log.user_approver_id = rec.user_approver_id;
							expenses_log.expense_log_status = [1, 3];
							expenses_log.job_order_id = rec.entity_id;
							//							expenses_log.newRecord()
							//							expenses_log.user_approver_id = rec.user_approver_id;
							//							expenses_log.expense_log_status = 3;
							//							expenses_log.job_order_id = rec.entity_id;
							totExpenses = expenses_log.search();
						}
						application.output('totExpenses assegnate ad approvatore ' + totExpenses);
					}
					if (totEvent + totExpenses > 0) {
						application.output('non è possibile cancellare approvatore ci sono eventi in sospeso');
						application.output(globals.messageLog + 'STOP job_orders_details.saveEdits() cancellazione approvatore KO ' + rec.approvers_to_users.users_to_contacts.real_name, LOGGINGLEVEL.INFO);
						globals.DIALOGS.showErrorDialog('Error', 'Non è stato cancellato l\'approvatore, \nha consuntivazioni in sospeso per questa commessa', 'OK');
						return;
					} else {
						try {
							fd.deleteRecord(index);
							application.output(globals.messageLog + 'STOP job_orders_details.saveEdits() cancellazione approvatore OK ' + rec.approvers_to_users.users_to_contacts.real_name, LOGGINGLEVEL.INFO);
						} catch (e) {
							application.output(e.message);
							globals.DIALOGS.showErrorDialog('Error', globals.validationExceptionMessage, 'OK');
							updateUI(event);
							// Failed to save data
							return;
						}
					}
				}
			}
		}
		_super.saveEdits(event);
		if (isLoggable == true && !globals.hasRole('Controllers')) {
			globals.sendJobOrdersIsLoggableMail(17, record.external_code_navision);
			application.output('sto inviando la mail..');
		}
		added = false;
		//JS invia mail agli utenti che sono stati aggiunti alla commessa scelta
		if (globals.userListAddedToJO.length > 0)
			sendMailToUserAddedToJO(globals.currentUserDisplayName, record);
		application.output(globals.messageLog + 'STOP job_orders_details.saveEdits() ', LOGGINGLEVEL.INFO);
	} catch (e) {
		application.output(e.message);
	}
}

/**
 * Add new RDC to users_job_orders
 * @AllowToRunInFind
 *
 * @param {Object} event
 * @param {Number} userIdToAdd the user_owner_id
 * @param {Number} jobIdToAdd the job_order_id
 * @protected
 * @properties={typeid:24,uuid:"C4918340-59C5-4ACF-B1A3-FB853D9CA65A"}
 */
function addNewRDC(event, userIdToAdd, jobIdToAdd) {
	application.output("addNewRdc " + userIdToAdd + " " + jobIdToAdd);
	var fd = forms.job_order_users_list.foundset;
	var index = null;
	var trovatoNew = 0;
	application.output("addNewRdc " + userIdToAdd + " " + oldRDC);
	if (oldRDC != null && oldRDC != userIdToAdd) {

		//search for new RDC
		if (fd.find()) {
			fd.user_id = userIdToAdd
			fd.job_order_id = jobIdToAdd
			trovatoNew = fd.search(true)
		}
	}

	//if new RDC not found
	if (index == null && trovatoNew != null && trovatoNew == 0) {
		//add
		fd.newRecord();
		fd.user_id = userIdToAdd;
		fd.job_order_id = jobIdToAdd
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"340DB2EC-6E43-4120-A87A-02B811F3501D"}
 */
function updateUI(event) {
	_super.updateUI(event);
	if (!isEditing()) foundset.sort('external_code_navision asc');

	//	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
	//		elements.buttonEdit.visible = false;
	//	}

	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		bundle.elements['split'].visible = !isEditing();
		bundle['focus'] = foundset.external_code_navision;
		bundle.elements['focus'].visible = isEditing();
	}

	elements.date_approver_from.visible = isEditing();
	elements.date_approver_from.enabled = isEditing() && globals.hasRole('Controllers');
	elements.fieldDateAppr.visible = isEditing();
}

/**
 * @param {String} approverName
 * @param record
 * @properties={typeid:24,uuid:"586DE608-FB9D-488C-8FDA-01EB33B492E7"}
 * @AllowToRunInFind
 */
function sendMailToUserAddedToJO(approverName, record) {
	application.output(globals.messageLog + 'START job_orders_details.sendMailToUserAddedToJO() lista utenti da aggiungere: ' + globals.userListAddedToJO, LOGGINGLEVEL.INFO);
	if (globals.userListAddedToJO.length > 0) {
		var userEmailBcc = [];
		globals.userListAddedToJO.length != 0 ? globals.userListAddedToJO.join('||') : [];
		/** @type {JSFoundSet<db:/geco/users>} */
		var users = databaseManager.getFoundSet('geco', 'users');
		if (users.find()) {
			//users.user_id =userList;
			users.user_id = globals.userListAddedToJO;
			if (users.search() > 0) {
				for (var index = 1; index <= users.getSize(); index++) {
					var rec = users.getRecord(index);
					userEmailBcc.push(rec.users_to_contacts.contacts_channels.channel_value);
				}
			}
		}
		var objMail = globals.getMailType(18);
		userEmailBcc != null ? userEmailBcc : '';
		var bcc = userEmailBcc.join(',');
		application.output(globals.messageLog + 'START job_orders_details.sendMailToUserAddedToJO() bcc: ' + bcc, LOGGINGLEVEL.INFO);
		//var subject = objMail.subject;
		var mailSubs = {
			approvatore: '',
			commessa: '',
			codeNavision: ''
		};
		mailSubs.approvatore = approverName;
		mailSubs.commessa = record.title_display;
		mailSubs.codeNavision = record.external_code_navision;
		try {
			var message = plugins.VelocityReport.evaluateWithContext(objMail.text, mailSubs);
			var subjectEvaluated = plugins.VelocityReport.evaluateWithContext(objMail.subject, mailSubs);
			application.output(globals.messageLog + 'job_orders_details.sendMailToUserAddedToJO() SENDING MAIL to distribution list - subject: ' + subjectEvaluated + ', bcc: ' + bcc + ', message: ' + message, LOGGINGLEVEL.INFO);
		} catch (error) {
			application.output(globals.messageLog + 'KO job_orders_details.sendMailToUserAddedToJO() error: ' + error, LOGGINGLEVEL.INFO);
		}
		scopes.globals.enqueueMailReminderTimesheet('[GECO] <noreply@spindox.it>', subjectEvaluated, message, bcc);
		globals.userListAddedToJO = [];
		application.output(globals.messageLog + 'STOP job_orders_details.sendMailToUserAddedToJO() ', LOGGINGLEVEL.INFO);
	}
}
