/**
 * @type {Number}
 * @properties={typeid:35,uuid:"473F70E9-C292-4BB3-A973-FD0F37FF7132",variableType:8}
 */
var year = new Date().getFullYear();

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"C051167E-B18C-4EF1-A66B-C2BDF80C1DBC",variableType:8}
 */
var month = new Date().getMonth() + 1;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"1B3AE9A6-6D93-4106-980D-BA5732C3A782",variableType:93}
 */
var startDate = new Date();

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"441C97FB-4D8F-4DC1-978E-8C2825577396",variableType:93}
 */
var endDate = new Date();

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"0576C4DD-D655-4D69-A3A7-8178AEC43D24",variableType:8}
 */
var skipWeekend = 1;

/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"0C319AF8-FD78-4F62-82D2-4DDE054D8BBF"}
 */
function updateUI(event) {
	controller.readOnly = false;
	elements.buttonCancel.enabled = true;
	elements.buttonSave.enabled = true;
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @param {String} entity
 * @properties={typeid:24,uuid:"795B25D5-B209-4EA1-82C2-BF079B485A67"}
 */
function clearAndClose(event, entity) {

	if (globals.isEmpty(startDate)) {
		globals.DIALOGS.showErrorDialog("Errore", "La data di inizio non può essere vuota", 'OK');
		return;
	}

	if (globals.isEmpty(endDate)) {
		globals.DIALOGS.showErrorDialog("Errore", "La data di fine non può essere vuota", 'OK');
		return;
	}

	if (startDate.setHours(0, 0, 0) > endDate.setHours(0, 0, 0)) {
		globals.DIALOGS.showErrorDialog("Errore", "La data di inizio non può essere maggiore alla data di fine", 'OK');
		return;
	}

	// close the form
	controller.getWindow().destroy();

	// call the proper function
	if (entity == 'events_log') {
		// WEB
		forms.approval_events_log_details.duplicateEventLog(startDate, endDate, skipWeekend);
	}

	if (entity == 'expenses_log') {
		// WEB
		forms.approval_expenses_log_details.duplicateExpensesLog(startDate, endDate, skipWeekend, year, month);
	}

}

/**
 * @param {Date} oldValue old value
 * @param {Date} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"AD5FF9EE-6895-46C6-BE53-08CB6D1E6762"}
 */
function onDataChange(oldValue, newValue, event) {
	endDate = newValue;
}
