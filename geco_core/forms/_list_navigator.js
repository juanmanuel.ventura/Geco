/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"BF5FB73F-40F5-432C-BB10-863EC357E5C5"}
 */
function updateUI(event) {
	_super.updateUI(event);
	
	elements.buttonAdd.visible = !isEditing();
	elements.buttonDelete.visible = !isEditing();
	elements.footer.visible = !isEditing();
	controller.enabled = !isEditing();
}
