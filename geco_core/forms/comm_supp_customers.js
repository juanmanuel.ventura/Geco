/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"1CC9E30D-0D0E-4005-802D-5BE765EA3494"}
 */
function newRecord(event) {
	//	var form = forms.comm_supp_customers.controller.
	//	dataset columns: [containername(1),formname(2),tabpanel or beanname(3),tabname(4),tabindex(5)]
	//	dataset rows: mainform(1) -> parent(2)  -> current form(3) (when 3 forms deep)
	/** @type {JSDataSet} */
	var dataset = controller.getFormContext();
	var parentFormName = '';
	application.output(dataset)
	if (dataset.getMaxRowIndex() > 1) {
		// form is in a tabpanel
		parentFormName = dataset.getValue(4, 2)
	}
	application.output('form padre ' + parentFormName);

	_super.newRecord(event)
		/** @type {JSRecord<db:/geco/comm_supp_profit_centers>} */
	var rec = forms.profit_centers_support_list.foundset.getSelectedRecord();
	if (rec.isNew()) {
		application.output('record padre nuovo ' + rec.comm_supp_profit_center_id)
		foundset.comm_supp_pc_id = rec.comm_supp_profit_center_id;
		
	} else {
		application.output('record padre vecchio ' + rec.comm_supp_profit_center_id)
	}
	
	application.output('dopo setting ' + foundset.comm_supp_pc_id);
}
