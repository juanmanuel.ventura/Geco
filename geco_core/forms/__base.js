/**
 * @properties={typeid:35,uuid:"C04F1B14-71B8-418D-BEF2-86F2B1544852",variableType:-4}
 */
var saveResult = true;

/**
 * Base method to begin an in-memory transaction
 *
 * @param {JSEvent} event The event that triggered the action.
 * @return {Boolean} true if started the transaction, false if not
 * @properties={typeid:24,uuid:"59DD7269-3917-4240-A175-8A17719533B0"}
 */
function startEditing(event) {
	application.output(globals.messageLog + 'START _base.startEditing() ',LOGGINGLEVEL.DEBUG);
	// begin in-memory transaction
	if (databaseManager.setAutoSave(false)) {
		updateUI(event);
		application.output(globals.messageLog + 'STOP _base.startEditing() OK',LOGGINGLEVEL.DEBUG);
		return true;
	}
	application.output(globals.messageLog + 'STOP _base.startEditing() KO',LOGGINGLEVEL.DEBUG);
	return false;
}

/**
 * Base method to save outstanding edits. This action does NOT close the in-memory transaction.
 * A form's validate() method is called Prior to saving data. Data is not saved when validation fails.
 *
 * @param {JSEvent} event The event that triggered the action.
 * @returns {Boolean} true if form was validated and edits were saved.
 * @properties={typeid:24,uuid:"4EFE3557-7731-4C65-80A3-8EBA08C3BF04"}
 */
function saveEdits(event) {
	application.output(globals.messageLog + 'START _base.saveEdits',LOGGINGLEVEL.DEBUG);
	// validation occurs at entity level
	if (databaseManager.saveData()) {
		// force to recalculate all calculated fields. IMPORTANT!
		databaseManager.recalculate(foundset);
		// close transactions
		stopEditing(event);
		// record was saved successfully
		application.output(globals.messageLog + 'STOP _base.saveEdits',LOGGINGLEVEL.DEBUG);
		return true;
	} else {
		application.output(globals.messageLog + 'STOP _base.saveEdits ERROR saveEdits failed ' + globals.validationExceptionMessage,LOGGINGLEVEL.DEBUG);
		globals.DIALOGS.showErrorDialog('Error', globals.validationExceptionMessage, 'OK');
		updateUI(event);
		// Failed to save data
		return false;
	}
}

/**
 * @param event
 * @properties={typeid:24,uuid:"730CD063-AC1D-49A7-BFD8-37D3600029BF"}
 */
function processSave(event) {
	application.output(globals.messageLog + 'START _base.processSave()',LOGGINGLEVEL.DEBUG);
	// dismiss any remaining busy dialog
	application.updateUI();

	try {
		// validation occurs at entity level
		if (databaseManager.saveData()) {
			// force to recalculate all calculated fields. IMPORTANT!
			databaseManager.recalculate(foundset);
			// close transactions
			stopEditing(event);
			// record was saved successfully
			saveResult = true;
		} else {
			application.output(globals.messageLog + ' _base.processSave() ERROR',LOGGINGLEVEL.DEBUG);
			throw new Error
		}
	} catch (e) {
		application.output(globals.messageLog + ' _base.processSave() ERROR ' +e.message,LOGGINGLEVEL.DEBUG);
		application.output(globals.messageLog + ' _base.processSave() ERROR ' +e,LOGGINGLEVEL.DEBUG);
		updateUI(event);
		saveResult = false; // Failed to save data
	} finally {
		//plugins.busy.unblock();
	}
	application.output(globals.messageLog + 'STOP _base.processSave()',LOGGINGLEVEL.DEBUG);
}

/**
 * Base method to close an in-memory transaction.
 * All edits are rolled back, so a save should have already been called
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"EC0EA81E-2415-4CB4-8567-2C5C9575F224"}
 */
function stopEditing(event) {
	application.output(globals.messageLog + 'START _base.stopEditing()',LOGGINGLEVEL.DEBUG);
	// revert edits only if in editing
	if (isEditing()) databaseManager.revertEditedRecords();
	// close in-memory transaction
	databaseManager.setAutoSave(true);
	// MVC: update the view based on the model
	updateUI(event);
	application.output(globals.messageLog + 'STOP _base.stopEditing()',LOGGINGLEVEL.DEBUG);
}

/**
 * Handle record selected. Invokes the updateUI() method
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"B4DA9FD0-0287-4BA4-A6DD-21B9321AE891"}
 */
function onRecordSelection(event) {
	updateUI(event);
}

/**
 * Callback method for when form is shown. Invokes the updateUI() method
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"327B0422-4DCE-4609-B8C5-742DF2BA1DCB"}
 */
function onShow(firstShow, event) {
//	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
//		plugins.busy.prepare();
//	}
	updateUI(event);
}

/**
 * Default handler for record edit stop event. (record save)
 * Invokes the updateUI() method.
 * @param {JSRecord} record that record was being edited
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"5CAA4819-E609-41BB-9AF5-1EC523FC19DB"}
 */
function onRecordEditStop(record, event) {
	updateUI(event);
}

/**
 * Deletes a record and optionally closes an in-memory transaction.
 * Implementations should probably confirm the delete in the UI.
 * Default action is to close the transaction
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} [index] The index of the record to delete. Default is selected index.
 * @returns {Boolean} true when record was deleted from database
 *
 * @properties={typeid:24,uuid:"23DF4B40-3BF9-4277-A9FB-EEDA7CBCD90C"}
 */
function deleteRecord(event, index) {
	application.output(globals.messageLog + 'START _base.deleteRecord() ',LOGGINGLEVEL.DEBUG);
	index = (index) ? index : foundset.getSelectedIndex();
	try{
		foundset.deleteRecord(index);
		 //close in-memory transaction
		stopEditing(event);
		application.output(globals.messageLog + 'STOP _base.deleteRecord() ',LOGGINGLEVEL.DEBUG);
		return true;
	}
	catch(e){
		application.output(globals.messageLog + 'STOP _base.deleteRecords() ERROR ' + e.message,LOGGINGLEVEL.DEBUG);
		globals.DIALOGS.showErrorDialog('Error', globals.validationExceptionMessage, 'OK');
		return false;
	
	}
//	if (foundset.deleteRecord(index)) {
//		// close in-memory transaction
//		stopEditing(event);
//		application.output(globals.messageLog + 'STOP _base.deleteRecord() ',LOGGINGLEVEL.DEBUG);
//		return true;
//	} else {
//	application.output(globals.messageLog + 'STOP _base.deleteRecord() Delete failed ',LOGGINGLEVEL.DEBUG);
//	return false;
//	}
}

/**
 * Delete record
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Array<JSRecord>} records and array containing the records to delete
 * @returns {Boolean} true when record was deleted from database
 *
 * @properties={typeid:24,uuid:"BD7781A4-525F-4500-BA5F-637C1413CC23"}
 */
function deleteRecordsAtOnce(event, records) {
	application.output(globals.messageLog + 'START _base.deleteRecordsAtOnce()',LOGGINGLEVEL.DEBUG);
	if (records && records.length > 0) {
		databaseManager.setAutoSave(false);
		for (var index = 0; index < records.length; index++) {
			//workaround to avoid exception when deleting
			try {
				foundset.deleteRecord(records[index]);
			} catch (e) {
				globals.deleteExceptionMessage +='\n'+globals.validationExceptionMessage;
				application.output(globals.messageLog + 'STOP _base.deleteRecordsAtOnce() ERROR ' + e.message,LOGGINGLEVEL.DEBUG);
				application.output(globals.messageLog + 'STOP _base.deleteRecordsAtOnce() ERROR ' + e,LOGGINGLEVEL.DEBUG);
			}

		}
		databaseManager.setAutoSave(true);
		application.output(globals.messageLog + 'STOP _base.deleteRecordsAtOnce() OK',LOGGINGLEVEL.DEBUG);
		return true
	}
	application.output(globals.messageLog + 'STOP _base.deleteRecordsAtOnce() KO',LOGGINGLEVEL.DEBUG);
	return false;
}

/**
 * Will not enter find if already editing or record is invalid
 *
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean} true when the form entered find mode
 * @properties={typeid:24,uuid:"BA297A8A-FAB9-417A-BBCA-40CD90E95DB0"}
 */
function startFind(event) {
	if (!isEditing() && foundset.find()) {
		return updateUI(event); // updateUI always returns true
	}
	application.output("Find failed");
	return false;
}

/**
 * Adds a new record. It opens an in-memory transaction.
 * New records cannot be added if an invalid state exists outside of a transaction
 *
 * @param {JSEvent} event the event that triggered the action
 * @returns {Number} The index of the record that was added
 *
 * @properties={typeid:24,uuid:"5E85197C-4A42-4D56-B1EE-7C833A2AB724"}
 */
function newRecord(event) {
	// start in-memory transaction
	if (startEditing(event)) {
		// create a new record
		return foundset.newRecord(false);
	}
	return null;
}

/**
 * Base method to perform search.
 *
 * @param {Boolean} [clear] clear last results
 * @param {Boolean} [reduce] reduce search
 * @param {JSEvent} event the event that triggered the action
 * @returns {Number} The number of records cached from the result.
 *
 * @properties={typeid:24,uuid:"7625D542-09EE-4894-9ACA-1B9357FC70AB"}
 * @AllowToRunInFind
 */
function search(clear, reduce, event) {
	var results = 0;
	if (foundset.isInFind()) {
		results = controller.search(clear, reduce);
		updateUI(event);
	}
	return results;
}

/**
 * Shows all records.
 * Action is disallowed when inside an in-memory transaction or when form is not valid.
 *
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean} True when the show-all was successful
 *
 * @properties={typeid:24,uuid:"46A1BE88-04B8-4160-8098-FA52016AD49C"}
 */
function showAllRecords(event) {
	if (!isEditing()) {
		return foundset.loadAllRecords();
	}
	application.output("Show all records failed")
	return false;
}

/**
 * Sorts the foundset based on the dataprovider and sort string
 * Action is disallowed when inside an in-mem transaction or when form is not valid
 *
 * @param {String} dataProviderID element data provider
 * @param {Boolean} asc sort acscending [true] or descending [false]
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"791DA006-0449-434C-B9F8-E860A1A58CA6"}
 */
function onSort(dataProviderID, asc, event) {
	if (!isEditing()) {
		foundset.sort(dataProviderID + (asc ? ' asc' : ' desc'), false);
	}
}

/**
 * Base method to determine if a form is editing.
 * This is a convenience method for checking auto-save.
 *
 * @returns {Boolean} True if the form is editing
 * @properties={typeid:24,uuid:"C79C02F4-49CE-4249-9FF2-5A53EA8E4DCE"}
 */
function isEditing() {
	// Check Auto-Save state
	return !databaseManager.getAutoSave();
}

/**
 * This method updates the form appearance based on the state of the model.
 * Called any time the state of the model may have changed.
 * Including: onRecordSelection, onShow, onRecordEditStop
 * Implementation forms should override this method to update the user interface.
 * Implementation forms should call this method when the data model is programmatically changed.
 *
 * Updates visible forms in tabpanels and splitpanes
 *
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"9D9BD4F7-306C-46F9-929C-BF4B76CB52F9"}
 */
function updateUI(event) {
	var form = null;

	// Iterate over elements
	for (var i = 0; i < elements.length; i++) {
		var e = elements[i];
		// get the element type
		var type = (e.getElementType) ? e.getElementType() : null;
		
		switch (type) {
		// tab panels
		case ELEMENT_TYPES.TABPANEL:
			// get the selected tab form
			form = forms[elements[i].getTabFormNameAt(elements[i].tabIndex)];
			// update its UI
			if (form && form['updateUI']) form['updateUI']();
			break;

		case ELEMENT_TYPES.SPLITPANE:
			// update left form UI
			form = elements[i].getLeftForm();
			if (form && form['updateUI']) form['updateUI']();
			// update right form UI
			form = elements[i].getRightForm();
			if (form && form['updateUI']) form['updateUI']();
			break;
		}
	}
}
