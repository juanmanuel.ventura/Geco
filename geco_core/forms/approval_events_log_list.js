/**
 * @type {String}
 * @properties={typeid:35,uuid:"C3B3459B-6C0D-494E-809E-F51ABC35AD92"}
 */
var sortDirection = 'asc';

/**
 * @type {RuntimeLabel}
 * @properties={typeid:35,uuid:"75D658C3-4CF2-4732-8F9B-225FF033DFE0",variableType:-4}
 */
var label;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"3F4D0EF4-6262-486C-BB8F-975D44D7DD66",variableType:4}
 */
var allSelected = 0;

/**
 * type {Number}
 * @properties={typeid:35,uuid:"9D61ED03-259F-46E5-8951-16679AF17433",variableType:-4}
 */
var month = null;
/**
 * type {Number}
 * @properties={typeid:35,uuid:"398D980B-C626-4B3B-AE1D-7F17BD6C8860",variableType:-4}
 */
var year = null;
/**
 * type {Number}
 * @properties={typeid:35,uuid:"9C7C0DA1-466E-474C-A59B-14D087CFD01F",variableType:-4}
 */
var stato = null;
/**
 * type {Number}
 * @properties={typeid:35,uuid:"2F4AFFBE-B98F-40F8-BA6A-B0B1D0A2A1BE",variableType:-4}
 */
var userId = null;

/**
 * type {Number}
 * @properties={typeid:35,uuid:"B1EA1D07-EC6D-4662-861E-0C5604B3FB28",variableType:-4}
 */
var jobOrder = null;

/**
 * type {Number}
 * @properties={typeid:35,uuid:"6A98ECA9-3217-4668-B09D-AF6B2ABE841F",variableType:-4}
 */
var fepCode = null;
/**
 * @type {Number[]}
 * @properties={typeid:35,uuid:"8DF6CE7D-6E32-4D74-A48F-37ADDE1F9500",variableType:-4}
 */
var eventsToNotify = [];

/**
 * @type {String}
 * @properties={typeid:35,uuid:"D829F07D-106A-441D-B237-ECAEC03ABAA1"}
 */
var labelUser = '';

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"432CD5C0-32C9-4BAA-B0E2-D38CF9D5E739"}
 */
function sort(event) {
	// change color back to the original one
	if (label) label.fgcolor = '#727784';
	// (re) set the triggered label
	label = event.getSource();
	// (re) change color to indicate selection
	label.fgcolor = '#990000';
	var sortKey = label.text;

	switch (sortKey) {
	case 'Data':
		controller.sort('event_log_date' + ' ' + sortDirection);
		break;
	case 'Utente':
		controller.sort('events_log_to_users.users_to_contacts.real_name' + ' ' + sortDirection);
		break;
	case 'Evento':
		controller.sort('events_log_to_events.event_name' + ' ' + sortDirection);
		break;
	case 'Codice':
		controller.sort('external_code_job_order' + ' ' + sortDirection);
		break;
	case 'Fep':
		controller.sort('external_code_fep' + ' ' + sortDirection);
		break;
	case 'Commessa':
		controller.sort('events_log_to_job_orders.job_order_title' + ' ' + sortDirection);
		break;
	case 'Da':
		controller.sort('time_start_display' + ' ' + sortDirection);
		break;
	case 'A':
		controller.sort('time_stop_display' + ' ' + sortDirection);
		break;
	case 'Durata':
		controller.sort('duration_display' + ' ' + sortDirection);
		break;
	}

	// toggle the sort order
	sortDirection = (sortDirection == 'asc') ? 'desc' : 'asc';
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"09E8F289-2438-4381-AAD4-27E5648557FC"}
 */
function selectAll(event) {
	for (var index = 1; index <= foundset.getSize(); index++) {
		var record = foundset.getRecord(index);
		record.is_selected = allSelected;
	}
}

/**
 * Handle record selected.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"786D0150-A72F-4552-983A-219E1964E180"}
 */
function onRecordSelection(event) {
	globals.selectedStartTime = foundset.time_start;
	_super.onRecordSelection(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} status the event that triggered the action
 * @public
 * @properties={typeid:24,uuid:"BFCF5CD7-1082-41E9-B714-D0E8BF70EF61"}
 *
 * @AllowToRunInFind
 */
function setBulkStatus(event, status) {
	application.output(globals.messageLog + 'START approval_events_log_list.setBulkStatus() status ' + status, LOGGINGLEVEL.INFO);
	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
	var events_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');
	/** @type {JSRecord<db:/geco/events_log>[]} */
	var tochangeReject = [];
	/** @type {JSRecord<db:/geco/events_log>[]} */
	var tochangeApproved = [];
	/** @type {JSRecord<db:/geco/events_log>[]} */
	var tochangeRecReject = [];
	/** @type {JSRecord<db:/geco/events_log>[]} */
	var tochangeRecApproved = [];
	/** @type {Boolean} */
	var hasSelected = false;
	/** @type {Boolean} */
	var hasRelatedRecoveryRejected = false;
	//DF era dichiata come array vuoto ma poi lo usi come foundset ho modificato la dicharazione in null e non []
	//	/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
	//	var additionalChargeFoundSet = null;
	application.output(globals.messageLog + 'approval_events_log_list.setBulkStatus() foundset size ' + foundset.getSize(), LOGGINGLEVEL.DEBUG);
	for (var i = 1; i <= foundset.getSize(); i++) {
		if (foundset.getRecord(i).is_selected == 1) {
			hasSelected = true;
			break;
		}
	}
	if (!hasSelected) {
		application.output(globals.messageLog + 'approval_events_log_list.setBulkStatus() nessun evento selezionato, esce', LOGGINGLEVEL.DEBUG);
		return;
	}
	var rejectionReason = null;
	if (status == 3) rejectionReason = globals.DIALOGS.showInputDialog("Rifiuta", "Inserisci il motivo del rifiuto (obbligatorio)");
	//rejectionReason = 'SVLP - rifiuto strao';
	//application.output('FUORI*' + rejectionReason + '*');
	try {
		//DF aggiunto try catch se succede una qualunque eccezione non esce dall'editing e blocca l'utente
		// cambiato con uno stopEditing nel catch per uscire e non salvare i record editati
		if ( (status == 3 && !globals.isEmpty(rejectionReason)) || status == 2 || status == 4) {
			//	application.output('DENTRO');
			var selfApproving = false;
			// start in-memory transaction
			startEditing(event);
			for (var index = 1; index <= foundset.getSize(); index++) {
				var record = foundset.getRecord(index);
				if (record.is_selected) {
					//se lo user e l'approvatore sono diversi oppure se lo stato è 4 e il ruolo è Controller o hrAdmin
					if (record.user_id != security.getUserUID() || (status == 4 && (globals.hasRole('Controllers') || globals.hasRole('HR Administrators')))) {
						var prev_status = record.event_log_status;
						events_log.setSatus(record, status, rejectionReason);
						record.is_selected = 0;
						if (status != 4) //non invia mail per stato chiuso
							eventsToNotify.push(record.event_log_id);
						//rifiuto di strao con recupero
						if (status == 3 && record.event_id == 28) {
							//metto un array e processo dopo
							application.output('tochangeReject 1 : ' + tochangeReject);
							//somma delle ore degli straordinari rifiutati
							//totalOvertimeDurationRejectedAC = totalOvertimeDurationRejectedAC + record.duration;
							tochangeReject.push(record);
							application.output('tochangeReject 2 : ' + tochangeReject);
							//application.output('totalOvertimeDurationRejectedAC 2 ' + totalOvertimeDurationRejectedAC);
						}
						//approvazione di strao con recupero rifiutati
						else if (status == 2 && prev_status == 3 && record.event_id == 28) {
							//events_charge.updateAdditionalCharge(events_log,2);
							application.output('tochangeApproved 1 : ' + tochangeApproved);
							tochangeApproved.push(record);
							application.output('tochangeApproved 2 : ' + tochangeApproved);
						}
						//rifiuto recupero strao
						if (status == 3 && record.event_id == 25) {
							//metto un array e processo dopo
							application.output('tochangeRecReject 1 : ' + tochangeRecReject);
							tochangeRecReject.push(record);
							application.output('tochangeRecReject 2 : ' + tochangeRecReject);
						}
						//approvazione di recupero rifiutati
						if (status == 2 && prev_status == 3 && record.event_id == 25) {
							//metto un array e processo dopo
							application.output('tochangeRecApproved 1 : ' + tochangeRecApproved);
							tochangeRecApproved.push(record);
							application.output('tochangeRecApproved 2 : ' + tochangeRecApproved);
						}
					} else {
						selfApproving = true;
					}
				}
			}
			if (selfApproving) globals.DIALOGS.showInfoDialog('Approving', 'Lo stato dei tuoi eventi non può essere cambiato da te stesso', 'OK')
			//application.output('Lo stato dei tuoi eventi non può essere cambiato da te stesso');
			// commit transaction
			application.output('tochangeReject ' + tochangeReject);
			var addChargeID = [];
			/** @type {Object} */
			var charge = {
				id: null,
				el_year: null,
				el_month: null,
				el_user: null,
				dur: 0, //durata strao totale rigettata per utente mese e anno
				durRetRej: 0,
				durWorked: 0,
				durRem: 0,
				durRet: 0

			}
			/** @type {charge[]} */
			var listObjRej = [];
			var idx = -1;
			//lista degli additionl che hanno recuperato dagli strao rifiutati con indicazione del mese, anno e utente
			for (var ir = 0; ir < tochangeReject.length; ir++) {
				var rej = tochangeReject[ir];
				application.output('Durata rifiutata ' + rej.duration);
				var stringToCompareWith = rej.event_year + '-' + rej.event_month;
				application.output('stringToCompareWith ' + stringToCompareWith);
				var query = "select el_additional_charge_id as id, el_year, el_month, duration_worked_rejected as work_rej ,duration_retrieved_rejected as ret_rej,\
					duration_worked as worked, duration_retrieved as ret, duration_remained rem, history_payments \
					from events_log_additional_charge where user_id = ? and history_payments like '%" + stringToCompareWith + "%' and \
					substring_index(substring(history_payments,locate('" + stringToCompareWith + "' , history_payments)+LENGTH('" + stringToCompareWith + "')+1),'|', 1) > 0";
				//application.output(query);
				var ds = databaseManager.getDataSetByQuery(globals.gecoDb, query, [rej.user_id], -1);
				application.output(ds);
				idx = -1;
				for (var x = 1; x <= ds.getMaxRowIndex(); x++) {
					application.output('ID add charge DS ' + ds.getValue(x, 1));
					application.output(addChargeID);
					var indS = addChargeID.indexOf(ds.getValue(x, 1));
					if (indS == -1) {
						addChargeID.push(ds.getValue(x, 1));
						/** @type {charge} */
						var obj = {
							id: ds.getValue(x, 1),
							el_year: ds.getValue(x, 2),
							el_month: ds.getValue(x, 3),
							el_user: rej.user_id,
							dur: rej.duration,
							durRetRej: ds.getValue(x, 5),
							durWorked: ds.getValue(x, 6),
							durRet: ds.getValue(x, 7),
							durRem: ds.getValue(x, 8)
						}
						listObjRej.push(obj);
						indS = addChargeID.indexOf(ds.getValue(x, 1));
					} else {
						listObjRej[indS].dur = listObjRej[indS].dur + rej.duration;
					}
					application.output(listObjRej[indS]);
				}
			}//fine for rifiutati;
			application.output('dopo il ciclo addChargeID' + addChargeID);
			//estrarre i record 25 da rigettare
			var evId25 = [];
			/** @type {Object} */
			var objEv25 = {
				id_event: null,
				dur: 0,
				status: null,
				el_user: null,
				el_month: null,
				el_year: null,
				result: 0, // 0 = da rifiutare 1 = rifiutato in approvazione 2 = già rifiutato
				id_charge: null
			}
			/** @type {objEv25[]} */
			var listObj25 = [];
			idx = -1;
			//lista degli eventi 25 da verificare con indicazione di user mese anno e durata evento
			for (var id = 0; id < listObjRej.length; id++) {
				var obRej = listObjRej[id];
				var tmpDurRem = obRej.durRem;
				var tmpDurRej=obRej.durRetRej;
				var qEv25 = 'select event_log_id, duration, event_log_status, user_id, event_month, event_year from events_log where user_id = ? and event_month= ? and event_year = ? and event_id = 25';
				var dsEv25 = databaseManager.getDataSetByQuery(globals.gecoDb, qEv25, [obRej.el_user, obRej.el_month, obRej.el_year], -1);
				for (x = 1; x <= dsEv25.getMaxRowIndex(); x++) {

					//application.output("L'UTENTE " + obRej.el_user + " NEL MESE DI " + obRej.el_month + " ORE LAVORATE : " + obRej.durWorked / 60 + " ORE RECUPERATE : " + obRej.durRet / 60 + " ORE RIMANENTI " + obRej.durRem / 60 + " TOTALE ORE RIFIUTATE " + totRetRej / 60);
					application.output("DURATA ORE STRAORDINARIO CHE STO RIFIUTANDO " + obRej.dur / 60);
					application.output("ORE RIMANENTI AGGIORNATO " + tmpDurRem / 60+ " ORE RIFIUTATE AGGIORNATO "+tmpDurRej);
					var indR = evId25.indexOf(dsEv25.getValue(x, 1));
					//FS inserire controllo per ogni utente se ore rimanenti-(durata strao che sto rifiutando)>totale ore già rifiutate
					if ((indR == -1) && (tmpDurRem-tmpDurRej>obRej.dur)) {
						
						evId25.push(dsEv25.getValue(x, 1));
						/** @type {objEv25} */
						var obj25 = {
							id_event: dsEv25.getValue(x, 1),
							dur: dsEv25.getValue(x, 2),
							status: dsEv25.getValue(x, 3),
							el_user: dsEv25.getValue(x, 4),
							el_month: dsEv25.getValue(x, 5),
							el_year: dsEv25.getValue(x, 6),
							result: (dsEv25.getValue(x, 3) == 3) ? 2 : 0,
							id_charge: listObjRej[id].id
						}	
						tmpDurRem = tmpDurRem - obRej.dur;
						tmpDurRej=tmpDurRej+obRej.dur;
							listObj25.push(obj25);
							indR = evId25.indexOf(dsEv25.getValue(x, 1));						
					}
					application.output(listObj25[indR]);
				}
			}
			for (ir = 0; ir < tochangeReject.length; ir++) {
				rej = tochangeReject[ir];
				events_charge.updateAdditionalCharge(rej, 1);
			}
			//strao rifiutati-->approvati
			application.output('tochangeApproved ' + tochangeApproved);
			for (var ia = 0; ia < tochangeApproved.length; ia++) {
				var app = tochangeApproved[ia];
				events_charge.updateAdditionalCharge(app, 2);
			}
			//recuperi rifiutati
			application.output('tochangeRecReject ' + tochangeRecReject);
			for (var irr = 0; irr < tochangeRecReject.length; irr++) {
				var rejRec = tochangeRecReject[irr];
				for (id = 0; id < listObj25.length; id++) {
					//verifico che negli eventi 25 da controllare non ci sia qualcosa che viene rifiutato in questa fase
					//se c'è non lo devo fare io già fatto sopra
					//decremento la durata strao rifiutata da controllare e tolgo l'evento dalla lista
					if (rejRec.event_log_id == listObj25[id].id_event) {
						//rifiutato da approvatore togliere dalla lista
						listObj25[id].result = 1;
					}
				}
				events_charge.updateAdditionalCharge(rejRec, 1);
			}
			//recueperi rifiutati -->approvati
			application.output('tochangeRecApproved ' + tochangeRecApproved);
			for (var iar = 0; iar < tochangeRecApproved.length; iar++) {
				var appRec = tochangeRecApproved[iar];
				events_charge.updateAdditionalCharge(appRec, 2);
			}
			var totalDurationToReject = 0;
			var oldUser = 0;
			/** @type {Array<Number>} */
			var idRecToReject = [];
			//ciclo su tutti i charge e metto in un array i recuperi da rifiutare
			for (var idn = 0; idn < listObjRej.length; idn++) {
				var chargeToPass = listObjRej[idn];
				if (idn == 0 || oldUser != chargeToPass.el_user) totalDurationToReject = chargeToPass.dur;
				for (id = 0; id < listObj25.length; id++) {
					// cerco gli event_log_id da rifiutare per lo stesso utente e decremento la durata totale per fermarmi quando ho trovato tutti i recuperi da aggiornare
					application.output(listObj25[id]);
					if (chargeToPass.id == listObj25[id].id_charge && chargeToPass.el_user == listObj25[id].el_user && 
							listObj25[id].result == 0 && totalDurationToReject > 0) {
						//aggiorno che lo
						listObj25[id].result = 2;
						idRecToReject.push(listObj25[id].id_event);
						totalDurationToReject = totalDurationToReject - listObj25[id].dur;
					} else if (chargeToPass.id == listObj25[id].id_charge && chargeToPass.el_user == listObj25[id].el_user && listObj25[id].result == 1 && totalDurationToReject > 0) {
						totalDurationToReject = totalDurationToReject - listObj25[id].dur;
					}
					oldUser = listObj25[id].el_user;
					application.output(idRecToReject);
				}
			}
			application.output('Finale ' + idRecToReject);
			//DF chiamata la setSatus per gli eventlog con id in idRecToReject
			for (index = 0; index < idRecToReject.length; index++) {
				application.output('idRecToReject[index]: ' + idRecToReject[index]);
				/** @type {JSFoundSet<db:/geco/events_log>} */
				var eventsLog = databaseManager.getFoundSet('geco', 'events_log');
				eventsLog.loadRecords();
				/** @type {JSRecord<db:/geco/events_log>} */
				var recRej = getElRecordFromEventsLogId(idRecToReject[index]);
				if (recRej != null) {
					application.output('recRej.event_id: ' + recRej.event_id + ', recRej.event_log_id: ' + recRej.event_log_id + '\nrecord: ' + recRej);
					//inserisco nei recuperi la nota del rifiuto e lo stato a rifiutato (3)
					eventsLog.setSatus(recRej, 3, rejectionReason);
					//DF add nella events_to_notofy
					eventsToNotify.push(recRej.event_log_id);
					//JStaffa faccio fare l'update delle retrieved_rejected per il record 25
					events_charge.updateAdditionalCharge(recRej, 1);
					application.output('recRej: ' + idRecToReject[index]);
				}
			}
			saveEdits(event);
			allSelected = 0;
		}
		var bundle = null;
		bundle = forms[controller.getFormContext().getValue(2, 2)];
		if (bundle.elements['split']) bundle.elements['split']['getLeftForm']()['applyFilter']();
		if (eventsToNotify.length > 0) {
			if (hasRelatedRecoveryRejected) {
				sendMailForEvents(status, true);
				hasRelatedRecoveryRejected = false;
			} else {
				sendMailForEvents(status);
			}
		}
	} catch (e) {
		application.output(e.message);
		// se va in eccezione revert dei record editati e non salva
		application.output(globals.messageLog + 'ERROR STOP approval_events_log_list.setBulkStatus() ', LOGGINGLEVEL.ERROR);
		stopEditing(event);
	}
	application.output(globals.messageLog + 'STOP approval_events_log_list.setBulkStatus() ', LOGGINGLEVEL.INFO);
}

/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"819D4AE6-0FB6-492E-AE9D-0C7F8923B857"}
 */
function updateUI(event) {
	_super.updateUI(event);

	// hide the header and footer images when editing
	//elements.headerImage.visible = elements.footerImage.visible = !isEditing();

	//abilitazione bottoni chiudi/approva/rigetta
	var status = null;
	var is_admin = (globals.hasRole('Controllers') || globals.hasRole('HR Administrators')) ? true : false;

	var bundle = forms[controller.getFormContext().getValue(2, 2)] || null;
	if (bundle)
		if (bundle.elements['split'])
			status = bundle.elements['split']['getLeftForm']()['selectedStatus'];

	switch (status) {
	case 1:
		elements.buttonApprove.enabled = true;
		elements.buttonReject.enabled = true;
		elements.buttonClose.enabled = false;
		elements.buttonDelete.enabled = is_admin;
		break;
	case 2:
		elements.buttonApprove.enabled = false;
		elements.buttonReject.enabled = true;
		elements.buttonClose.enabled = true
		elements.buttonDelete.enabled = is_admin;
		break;
	case 3:
		elements.buttonApprove.enabled = true;
		elements.buttonReject.enabled = false;
		elements.buttonClose.enabled = false;
		elements.buttonDelete.enabled = is_admin;
		break;
	case 4:
		if (globals.hasRole('Controllers') || globals.hasRole('HR Administrators')) {
			elements.buttonApprove.enabled = true;
			elements.buttonReject.enabled = true;
			elements.buttonClose.enabled = false;
			elements.buttonDelete.enabled = is_admin;
		} else {
			elements.buttonApprove.enabled = false;
			elements.buttonReject.enabled = false;
			elements.buttonClose.enabled = false;
			elements.buttonDelete.enabled = is_admin;
		}
		break;
	}

	if (status != null) {
		elements.buttonApprove.visible = !isEditing();
		elements.buttonReject.visible = !isEditing();
		elements.buttonClose.visible = !isEditing();
	} else
		elements.buttonDelete.enabled = is_admin;
	elements.buttonAdd.visible = !isEditing();
	elements.buttonDelete.visible = !isEditing();

}

/**
 * Export to file txt
 * @param {JSEvent} event the event that triggered the action
 * @param {String} functionToCall name of function to call in core.globals
 * @protected
 *
 * @properties={typeid:24,uuid:"D5BDBA66-D2A1-49C0-BF9B-863F401E4B83"}
 */
function exportFile(event, functionToCall) {
	//mese e anno e status di filter
	//	var month = null;
	//	var year = null;
	// get variable  di filter
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) {
		month = bundle.elements['split']['getLeftForm']()['selectedMonth'];
		year = bundle.elements['split']['getLeftForm']()['selectedYear'];
		stato = bundle.elements['split']['getLeftForm']()['selectedStatus'];
	}

	application.output(functionToCall + ' month ' + month + ' year ' + year + ' stato ' + stato);
	globals[functionToCall](event, year, month, stato);
}

/**
 * @param {Number} status
 * @param {Boolean} [hasRecoveryRelatedRejected]
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"3C5EB7A9-8144-4E48-8FD3-842A81427F73"}
 */
function sendMailForEvents(status, hasRecoveryRelatedRejected) {
	application.output('#### mailApproverEvents ####');
	application.output(globals.messageLog + 'START approval_events_log_list.sendMailForEvents() ', LOGGINGLEVEL.INFO);

	var eventsToSearch = eventsToNotify.join("||");
	application.output('ID APPROVATI ' + eventsToSearch.toString() + '');

	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log_approved = databaseManager.getFoundSet('geco', 'events_log');
	if (events_log_approved.find()) {
		events_log_approved.event_log_id = eventsToSearch;
		if (status == 2) {
			events_log_approved.events_log_to_events.is_requestable = 1;
		}
		events_log_approved.search();
		events_log_approved.sort('user_id asc, event_id asc, event_log_date asc');
	}
	var mailTypeId = 0;
	if (status == 2) mailTypeId = 5;
	if (status == 3) {
		mailTypeId = 6;
	}
	var currentUser = '';
	var currentEvent = '';
	var period = '';
	var rejectReason = '';

	for (var index = 1; index <= events_log_approved.getSize(); index++) {
		var evento = events_log_approved.getRecord(index);
		var eventoNext = (index + 1 <= events_log_approved.getSize()) ? events_log_approved.getRecord(index + 1) : null;
		if (eventoNext != null) {
		} else {
		}
		currentUser = evento.user_id
		currentEvent = evento.event_id;
		period = period + evento.event_log_date.toLocaleDateString() + ' - ';
		//se è strao con riposo e la globale è a true, invio mail con messaggio cambiato
		if (evento.event_id == 28 && hasRecoveryRelatedRejected) {
			rejectReason = evento.note + ", i recuperi associati allo straordinario rifiutato sono stati automaticamente rifiutati";
		} else rejectReason = evento.note;

		application.output('periodo ' + period);
		if (eventoNext != null) {
			if (currentUser != eventoNext.user_id) {
				//utente diverso dal successivo
				globals.approverSendMail(mailTypeId, evento.events_log_to_users, evento.events_log_to_events.event_name, period, rejectReason);
				period = '';
			} else {
				if (currentEvent != eventoNext.event_id) {
					//evento diverso dal successivo
					globals.approverSendMail(mailTypeId, evento.events_log_to_users, evento.events_log_to_events.event_name, period, rejectReason);
					period = '';
				}
			}
		} else {
			//non c'è altro evento dopo
			globals.approverSendMail(mailTypeId, evento.events_log_to_users, evento.events_log_to_events.event_name, period, rejectReason);
		}
	}
	eventsToNotify = []
	application.output(globals.messageLog + 'STOP approval_events_log_list.sendMailForEvents() ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @properties={typeid:24,uuid:"595FE7E5-AACA-4E06-AC81-359195794402"}
 */
function deleteRecord(event, index) {
	application.output(globals.messageLog + 'START approval_events_log_list.deleteRecord() ', LOGGINGLEVEL.INFO);
	var record = foundset.getSelectedRecord();
	var mailTypeId = 11;
	var currentUser = record.events_log_to_users;
	var currentEventName = record.events_log_to_events.event_name;
	var period = record.event_log_date.toLocaleDateString()
	application.output(globals.messageLog + 'approval_events_log_list.deleteRecord() currentUser ' + currentUser + ' currentEventName ' + currentEventName + ' period ' + period, LOGGINGLEVEL.INFO);

	if (_super.deleteRecord(event, index)) {
		application.output(globals.messageLog + 'approval_events_log_list.deleteRecord() OK ', LOGGINGLEVEL.INFO);

		var deleteReason = globals.DIALOGS.showInputDialog("Cancellazione", "Inserisci il motivo della cancellazione");
		globals.approverSendMail(mailTypeId, currentUser, currentEventName, period, deleteReason);

		application.output(globals.messageLog + 'approval_events_log_list.deleteRecord() inviata mail ', LOGGINGLEVEL.INFO);
	} else
		application.output(globals.messageLog + 'approval_events_log_list.deleteRecord() ERROR ', LOGGINGLEVEL.INFO);

	application.output(globals.messageLog + 'STOP approval_events_log_list.deleteRecord() ', LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"168CBD08-4531-412C-B0AA-136958043C00"}
 */
function newRecord(event) {
	application.output(globals.messageLog + 'START approval_events_log_list.newRecord() ', LOGGINGLEVEL.DEBUG);
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle.elements['split']) {
		userId = bundle.elements['split']['getLeftForm']()['selectedUser'];
	}
	if (userId == null) {
		globals.DIALOGS.showErrorDialog('Error', 'Selezionare l\'utente ', 'Chiudi');
		return;
	}
	if (startEditing(event)) {
		foundset.newRecord(true);
		foundset.user_id = userId;
	}
	application.output(globals.messageLog + 'STOP approval_events_log_list.newRecord() ', LOGGINGLEVEL.DEBUG);
}

/**
 * @param event
 *
 * @properties={typeid:24,uuid:"DB2525F8-167C-4555-AEA0-8AC1965D4A82"}
 * @AllowToRunInFind
 */
function deleteEvents(event) {
	application.output(globals.messageLog + 'START approval_events_log_list.deleteEvents ', LOGGINGLEVEL.INFO);
	/** @type {JSRecord<db:/geco/events_log>[]} */
	var eventsLogToDelete = [];
	var hasSelected = false;
	var index = 0;
	application.output(globals.messageLog + 'approval_events_log_list.deleteEvents foundset size ' + foundset.getSize(), LOGGINGLEVEL.DEBUG);
	for (var i = 1; i <= foundset.getSize(); i++) {
		if (foundset.getRecord(i).is_selected == 1) {
			hasSelected = true;
			break;
		}
	}
	if (!hasSelected) {
		application.output(globals.messageLog + 'approval_events_log_list.deleteEvents() nessun evento selezionato, esce', LOGGINGLEVEL.DEBUG);
		return;
	}

	var answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai per eliminare dei record.\nQuesta operazione non può essere annullata", "Elimina", "Annulla");
	if (answer == "Elimina") {

		var deleteReason = globals.DIALOGS.showInputDialog("Cancellazione", "Inserisci il motivo della cancellazione");
		if (deleteReason != null && deleteReason != '') {

			for (index = 1; index <= foundset.getSize(); index++) {
				var record = foundset.getRecord(index);
				if (record.is_selected) {
					eventsToNotify.push(record.event_log_id);
					eventsLogToDelete.push(record);
				}
			}
			var eventsToSearch = eventsToNotify.join("||");

			/** @type {JSFoundSet<db:/geco/events_log>} */
			var events_log_to_mail = databaseManager.getFoundSet('geco', 'events_log');
			if (events_log_to_mail.find()) {
				events_log_to_mail.event_log_id = eventsToSearch;
				events_log_to_mail.search();
				events_log_to_mail.sort('user_id asc, event_id asc, event_log_date asc');
			}
			var notifyList = [];

			var mailTypeId = 11;
			var currentUser = '';
			var currentEvent = '';
			var period = '';
			for (var del = 1; del <= events_log_to_mail.getSize(); del++) {

				var evento = events_log_to_mail.getRecord(del);

				var eventoNext = (del + 1 <= events_log_to_mail.getSize()) ? events_log_to_mail.getRecord(del + 1) : null;

				currentUser = evento.user_id
				currentEvent = evento.event_id;
				period = period + evento.event_log_date.toLocaleDateString() + ' - ';
				//application.output('periodo ' + period);
				var objNotify = {
					usr: null,
					event: null,
					period: null,
					reason: null
				}
				objNotify.usr = evento.events_log_to_users;
				objNotify.event = evento.events_log_to_events.event_name;
				objNotify.period = period
				objNotify.reason = deleteReason;
				//application.output(objNotify.usr + ' ' + objNotify.event + ' ' + objNotify.period + ' ' + objNotify.reason);
				if (eventoNext != null) {
					if (currentUser != eventoNext.user_id) {
						//utente diverso dal successivo
						notifyList.push(objNotify);
						period = '';
					} else {
						if (currentEvent != eventoNext.event_id) {
							//evento diverso dal successivo
							notifyList.push(objNotify);
							period = '';
						}
					}
				} else {
					//non c'è altro evento dopo
					notifyList.push(objNotify);
				}
			}
			application.output(globals.messageLog + 'approval_events_log_list.deleteEvents() eventi da cancellare ' + eventsLogToDelete.length, LOGGINGLEVEL.DEBUG);
			//application.output('numero record ' + foundset.getSize());
			for (index = 0; index < eventsLogToDelete.length; index++) {
				//var recordToDel = eventsLogToDelete[index];

				//if (recordToDel.is_selected) {
				//application.output(recordToDel);
				var success = _super.deleteRecordNoMessage(event, index);
				//var success = foundset.deleteRecord(eventsLogToDelete[index]);
				application.output(globals.messageLog + 'approval_events_log_list.deleteEvents() record cancellato ' + success, LOGGINGLEVEL.DEBUG);
			}
			application.output(globals.messageLog + 'approval_events_log_list.deleteEvents() invio mail ', LOGGINGLEVEL.DEBUG);
			for (var idx = 0; idx < notifyList.length; idx++) {
				objNotify = notifyList[idx];
				globals.approverSendMail(mailTypeId, objNotify.usr, objNotify.event, objNotify.period, objNotify.reason);
			}
			eventsToNotify = []
			allSelected = 0;
		}
	}
	application.output(globals.messageLog + 'STOP approval_events_log_list.deleteEvents() ', LOGGINGLEVEL.INFO);
}
/**
 * @param {Number} eventLogId
 * @return {JSRecord<db:/geco/events_log>}
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"E371D2F9-D8E7-4CF2-9F2A-AB76359D030B"}
 */
function getElRecordFromEventsLogId(eventLogId) {
	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	events_log.loadRecords();
	/** @type {JSRecord<db:/geco/events_log>} */
	var retRec = null;
	//cerco tutti i record che combaciano sia con l'anno che con il mese e che non sia già stato rifiutato in precedenza
	if (events_log.find()) {
		events_log.event_log_id = eventLogId;
		var tot = events_log.search();

		if (tot == 1) {
			//piglio il primo
			retRec = events_log.getRecord(1);
		}
	}
	application.output('totSearch getElRecordFromEventsLogId: ' + tot + ', rec: ' + retRec);
	return retRec;
}
