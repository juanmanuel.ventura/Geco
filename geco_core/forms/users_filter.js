/**
 * @type {Number}
 * @properties={typeid:35,uuid:"3FA99A09-F962-46A7-BAED-4C7790FA742F",variableType:8}
 */
var userType = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"3EEB7113-FFF0-4129-8520-2C09BBB1A298",variableType:8}
 */
var profitCenter = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F5AABB2B-5201-4601-B076-F00C421144C7",variableType:8}
 */
var userId = null;

/**
 * @type {Number}
 * @protected
 * @properties={typeid:35,uuid:"736E0611-027C-444A-9603-B42C2167C45A",variableType:4}
 */
var isLocked = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"B03EAC8A-6C1B-4BAA-AA44-98B93BB0C742",variableType:8}
 */
var profile = null;




/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"F893DE66-802E-4085-A749-1FCABBA84279"}
 */
function onDataChange(oldValue, newValue, event) {
	if (newValue != oldValue) {
		applyFilter(event);
	}
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"51004536-EF24-4F25-9139-5F7030FC0069"}
 */
function resetFilter(event, goTop) {
	profile = null;
	userType = null;
	profitCenter = null;
	userId = null;
	isLocked = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"8892E4B9-00DD-45D8-ABCA-C51DBB5061F2"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.user_id = userId;
		foundset.user_type_id = userType;
		foundset.profit_center_id = profitCenter;
		foundset.users_to_view_users_profiles_with_max_date.profile_id = profile;
		// profile
		if(isLocked == 0){
			foundset.is_locked = null;
		}else
		foundset.is_locked = isLocked;
		foundset.search();
		foundset.sort("users_to_contacts.last_name asc, users_to_contacts.first_name asc");
	}
}


/**
 * Handle changed data.
 *
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"F5B58709-0887-4703-8108-2334E922D2E9"}
 */
function onDataChangeEnabled(oldValue, newValue, event) {
	isEnabled = newValue;
	if(isEnabled == 0){
		elements.usersc.visible = true;
		elements.usersc.enabled = true;
		userId = null;
		
	}
	else {
		elements.usersc.visible = false;
		elements.usersc.enabled = false;
		userId = null;
	}
	return true
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"9047F434-2F2B-4B97-A6D4-143DF9475830"}
 */
function onLoad(event) {
	_super.onLoad(event);
	elements.usersc.visible = false;
	elements.usersc.enabled = false;
}


/**
 * @properties={typeid:24,uuid:"C35DC420-324C-4253-8742-C2A6861CF9FD"}
 */
function filterEnabledRecords() {
//	setValuelist('usersList');
	_super.filterEnabledRecords();
}

