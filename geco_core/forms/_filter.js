/**
 * @type {Boolean}
 * @private
 * @properties={typeid:35,uuid:"F23C97E2-549A-4107-84CE-D2AD29AFDE0A",variableType:-4}
 */
var isExpanded = false;

/**
 * @type {Number}
 * @protected
 * @properties={typeid:35,uuid:"95A3DC7E-68B5-44A3-A9DE-B3AD7850FB4D",variableType:4}
 */
var isEnabled = 1;

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} reset true to reset the form
 * @protected
 * @properties={typeid:24,uuid:"AAE77DE4-6CCA-48A6-8E0E-62BD372289FE"}
 */
function toggleHeader(event, reset) {
	
	// get the bundle
	var mainForm = forms[controller.getFormContext().getValue(2, 2)] || null;

	// get the split pane
	/** @type {RuntimeSplitPane} */
	var split = mainForm.elements['split'];

	// reset and quit
	if (reset && split) {
		split.dividerLocation = 48;
		elements.buttonToggle.imageURL = "media:///filter/arrow_down.png";
		application.updateUI();
		return;
	}

	if (split && split.getElementType() == ELEMENT_TYPES.SPLITPANE) {
		if (isExpanded) {
			split.dividerLocation = 48;
			elements.buttonToggle.imageURL = "media:///filter/arrow_down.png";
		} else {
			split.dividerLocation = controller.getPartHeight(JSPart.BODY);
			elements.buttonToggle.imageURL = "media:///filter/arrow_up.png";
		}
		isExpanded = !isExpanded;
	}
}

/**
 * @properties={typeid:24,uuid:"AED5860A-7DA3-4144-808B-1937EF630637"}
 * @AllowToRunInFind
 */
function filterEnabledRecords() {
	// filter for entities that implement the field "is_enabled"
	if (foundset.alldataproviders.indexOf('is_enabled') != -1) {
		foundset.removeFoundSetFilterParam('enabled');
		foundset.addFoundSetFilterParam('is_enabled', '=', isEnabled, 'enabled');
		foundset.loadAllRecords();
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} [goTop] true if the cursor must be placed at the first index
 * @properties={typeid:24,uuid:"1216718A-1A54-4BF7-B028-C80C4AC56627"}
 */
function resetFilter(event, goTop) {
	foundset.loadAllRecords();
	// move to the first record (otherwise the cursor remains at the last selected record)
	if (goTop) foundset.setSelectedIndex(1);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"A0CCE018-CAF6-4283-83DB-37D61B7685D6"}
 */
function onLoad(event) {
	filterEnabledRecords();
}

/**
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"D1BA8A9C-E092-457A-9DA1-3E3915385F24"}
 */
function onShow(firstShow, event) {
	toggleHeader(event, true);
	_super.onShow(firstShow, event);
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"9C1F8BC7-CEE2-42EC-916A-0929520E9DBB"}
 */
function updateUI(event) {
	_super.updateUI(event);
	controller.enabled = !isEditing();
	elements.imageBody.visible = !isEditing();
	elements.imageHeader.visible = !isEditing();
}
