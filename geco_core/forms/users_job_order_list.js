/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @protected 
 *
 * @properties={typeid:24,uuid:"4EF95025-566E-4621-83AC-73BE9280BF62"}
 */
function openPopupSelect(event) {
	var win = application.createWindow("selezione", JSWindow.MODAL_DIALOG);
	globals.callerForm = forms[event.getFormName()];
	win.setInitialBounds(1000, 1000, 500, 500);
	win.show(forms.users_job_orders_list_popup);
}
