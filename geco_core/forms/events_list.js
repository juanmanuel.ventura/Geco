/**
 * @properties={typeid:24,uuid:"33C5F2E2-D615-4D0E-8B23-85AC8103F1A1"}
 */
function updateUI(event) {
	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
		elements.buttonAdd.visible = false;
	}
}
/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F8D81071-4D70-44EF-A8DA-8647DFA2064B"}
 */
function newRecord(event) {
	_super.newRecord(event);
	forms.events_details.elements.lEventCost.visible = true;
	forms.events_details.elements.fEventCost.visible = true;
}
