/**
 * @type {String}
 * @properties={typeid:35,uuid:"1531B8AB-4DE7-421E-9052-6A513A088359"}
 */
var entity = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"1B7F76E9-47E7-451B-BAE8-C906304ED07F",variableType:8}
 */
var selectedMonth = new Date().getMonth() + 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"18A92B73-4B8C-4ECC-988A-7E385175E257",variableType:8}
 */
var selectedYear = new Date().getFullYear();

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} enable
 *
 * @private
 *
 * @properties={typeid:24,uuid:"C01207DA-2C4A-405F-9D8C-03292491A4F5"}
 */
function setEnabledValue(event, enable) {
	//chiama il motodo della tabella
	/** @type {JSFoundSet<db:/geco/calendar_days>} */
	var calendar_days = databaseManager.getFoundSet('geco', 'calendar_days')
	
	try {
		calendar_days.setPeriodStatus(selectedMonth, selectedYear, entity, enable);
	} catch (e) {
		globals.DIALOGS.showErrorDialog('Errore', e['message'], 'OK');
	}
}
