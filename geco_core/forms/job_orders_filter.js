/**
 * @type {Number}
 * @properties={typeid:35,uuid:"55D07912-CDF5-413C-AC69-EB70B32DE792",variableType:8}
 */
var userOwner = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"997B5F9C-FDD1-4F72-B5C8-EFADB8912EAA",variableType:8}
 */
var company = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DB0ACCA5-167F-4ED4-B904-97EDA85DBA37"}
 */
var codeNavision = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"890B6267-D683-4361-BDEB-857CA60183D1",variableType:8}
 */
var profitCenter = null;
/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"BBC1C1EB-E5B6-4CAD-BF86-FDC020846A9D"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 * @properties={typeid:24,uuid:"4924294A-C5FE-4199-9389-C0D308D46C8F"}
 */
function resetFilter(event, goTop) {
	userOwner = null;
	company = null;
	profitCenter = null;
	codeNavision = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"43D1178E-7336-44C7-8E88-E5AD7E18055D"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.user_owner_id = userOwner;
		foundset.company_id = company;
		foundset.external_code_navision = codeNavision;
		foundset.profit_center_id = profitCenter;
		foundset.search();
	}
}
