/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"692EFFB7-33C9-4330-A3EB-0CA5F8E5A15C"}
 */
function updateUI(event) {
	_super.updateUI(event);
	elements.buttonAdd.visible = !isEditing();
	elements.buttonDelete.visible = !isEditing();
	elements.footer.visible = !isEditing();

}
