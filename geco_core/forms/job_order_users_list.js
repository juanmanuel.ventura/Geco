/**
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"1BBC1E76-424B-4761-AA96-18FFA5D6D32B"}
 */
function openPopupSelect(event) {
  var win = application.createWindow("selezione", JSWindow.MODAL_DIALOG);
  globals.callerForm = forms[event.getFormName()];
  win.setInitialBounds(1000, 1000, 500, 500);
  win.show(forms.job_order_users_list_popup);
}
