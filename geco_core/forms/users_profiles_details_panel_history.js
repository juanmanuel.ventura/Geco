/**
 * @type {String}
 * @properties={typeid:35,uuid:"CD7F87CC-00C6-4DBB-8016-F0680F981B9F"}
 */
var sortDirection = 'asc';

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"86633216-AB45-43DE-A55F-342BA2F8F99A"}
 */
function doSort(event) {
	if (foundset.getSize() > 1) {
		var label = event.getSource();
		// change color back to the original one
		if (label) label.fgcolor = '#727784';
		// (re) set the triggered label
		label = event.getSource();
		// (re) change color to indicate selection
		label.fgcolor = '#990000';
		var sortKey = label.text;

		switch (sortKey) {
		case 'Data inizio validità':
			controller.sort('prof_cost_start_date' + ' ' + sortDirection);
			break;
		case 'Costo giornaliero':
			controller.sort('prof_cost_daily' + ' ' + sortDirection);
			break;
		case 'Costo orario':
			controller.sort('prof_cost_schedule' + ' ' + sortDirection);
			break;
		}

		// toggle the sort order
		sortDirection = (sortDirection == 'asc') ? 'desc' : 'asc';
	}
}
/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"8BA41C11-8E20-4F72-8A54-B9A95DD4DE74"}
 */
function newRecord(event) {
	_super.newRecord(event);
	foundset.prof_cost_start_date = new Date();
}


/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"766B5003-5833-4486-9ED4-085F70713DF6"}
 */
function updateUI(event) {
	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
		elements.buttonAdd.visible = false;
		elements.buttonDelete.visible = false;
	} else {
		elements.buttonAdd.visible = true;
		elements.buttonAdd.enabled = isEditing();
		elements.buttonDelete.visible = foundset.getSize() > 0;
		elements.buttonDelete.enabled = !isEditing() && foundset.getSize() > 0;
	}
}
/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 * @properties={typeid:24,uuid:"A4878FCF-48E9-42AD-A9D8-14EE4607B7D3"}
 */
function deleteRecord(event, index) {
	if (foundset && foundset.getSelectedRecord()) {
		var currentID = foundset.getSelectedRecord().profile_cost_id;
		var lastID = foundset.getLastID(foundset.getSelectedRecord().profile_id);
		//se c'è
		if (lastID != null) {
			//se sono uguali OK
			if (currentID == lastID)
				_super.deleteRecord(event, index);
			else { //altrimenti errore; non lascio cancellare i precedenti
				globals.DIALOGS.showErrorDialog('Error', '- Impossibile eliminare il record selezionato, record troppo vecchio.', 'OK');
				return;
			}
		} //precauzione.. in teoria non dovrebbe verificarsi
		else {
			globals.DIALOGS.showErrorDialog('Error', '- Impossibile eliminare il record selezionato.', 'OK');
			return;
		}
	}
}
