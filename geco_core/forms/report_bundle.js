/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} [formFilterTopName]
 * @param {String} formFilterBottomName
 * @param {String} [relation]
 * @protected
 * @properties={typeid:24,uuid:"44761C9E-FDDF-4058-8B46-AB33D8178DE3"}
 */
function initialize(event, formFilterTopName, formFilterBottomName, relation) {

	try {
		if (elements.split.dividerLocation) {
			elements.split.dividerSize = 0;
			elements.split.dividerLocation = 100;
		}
	} catch (e) {

	}
	if (formFilterTopName && forms[formFilterTopName]) {
		elements.split.setLeftForm(forms[formFilterTopName]);
		// force to apply filter (seems not to work on form's onLoad event)
		if (forms[formFilterTopName].applyFilter) forms[formFilterTopName].applyFilter();
		// load the list
		elements.split.setRightForm(forms[formFilterBottomName]);
	}
	
}