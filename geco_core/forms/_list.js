/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @properties={typeid:24,uuid:"273B6D3B-0CD6-4130-8CEA-4884F63B4F8E"}
 */
function deleteRecord(event, index) {
	var answer = globals.DIALOGS.showWarningDialog("Conferma", "Stai per eliminare un record.\nQuesta operazione non può essere annullata", "Elimina", "Annulla");
	if (answer == "Elimina") {
		return _super.deleteRecord(event);
	}
	return false;
}


/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 *
 * @properties={typeid:24,uuid:"47E37554-A68D-43C8-A923-4B462C40F5A0"}
 */
function deleteRecordNoMessage(event, index) {
		return _super.deleteRecord(event);
}

