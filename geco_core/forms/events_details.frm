dataSource:"db:/geco/events",
extendsID:"7D147E9E-8F99-4CAE-8E3B-6C45741464B5",
items:[
{
dataProviderID:"is_overtime",
displayType:4,
formIndex:1,
horizontalAlignment:2,
location:"532,191",
name:"hasJobOrdercc",
onDataChangeMethodID:"-1",
size:"27,20",
transparent:true,
typeid:4,
uuid:"04125B35-CDBF-4060-BCC0-929CBF13D6A9"
},
{
location:"30,232",
size:"346,28",
styleClass:"bold",
text:"Utenti abilitati alla consuntivazione",
transparent:true,
typeid:7,
uuid:"15C5E143-F9A9-458E-B20A-D3BD70D67E1A"
},
{
dataProviderID:"is_enabled",
displayType:4,
formIndex:1,
location:"15,502",
name:"hasJobOrderc",
onDataChangeMethodID:"38257D7F-B31E-44A0-B9B5-E48504FFABFC",
size:"69,20",
text:"Attivo",
transparent:true,
typeid:4,
uuid:"1BB50F35-5450-4BEC-A3FA-98D02E26B4F3"
},
{
location:"30,19",
size:"111,20",
text:"Nome",
transparent:true,
typeid:7,
uuid:"23606C69-CC1D-42C3-9B9B-DEF75CB6E52B"
},
{
location:"30,204",
name:"lEventCost2",
size:"111,20",
text:"Costo",
transparent:true,
typeid:7,
uuid:"241AC6ED-AA59-4350-AB5A-1B2C0D1815F7",
visible:false
},
{
location:"406,232",
size:"367,28",
styleClass:"bold",
text:"Approvatori",
transparent:true,
typeid:7,
uuid:"30954844-690B-42F4-8967-96CC628A44B6"
},
{
location:"30,93",
size:"111,20",
text:"Conteggia",
transparent:true,
typeid:7,
uuid:"37704012-2726-40EE-AF2C-89CFEF981680"
},
{
dataProviderID:"has_job_order",
displayType:4,
formIndex:1,
horizontalAlignment:2,
location:"532,161",
name:"hasJobOrder",
onDataChangeMethodID:"38257D7F-B31E-44A0-B9B5-E48504FFABFC",
size:"27,20",
transparent:true,
typeid:4,
uuid:"421F2CFA-3A74-4101-9171-61F8C58640C0"
},
{
anchors:11,
dataProviderID:"external_code_navision",
location:"532,56",
name:"fldExternal_code",
size:"238,20",
typeid:4,
uuid:"4AAB3EA8-D64D-4840-9DAC-E25DC5DCBAEF"
},
{
location:"30,56",
size:"111,20",
text:"Tipo",
transparent:true,
typeid:7,
uuid:"50D0303E-0D3A-4E79-94A6-5D720726D8D4"
},
{
location:"30,168",
mnemonic:"",
name:"lEventCost",
size:"111,20",
text:"Costo",
transparent:true,
typeid:7,
uuid:"5266F3DE-7C0F-42BE-B330-E6294F963335",
visible:false
},
{
location:"406,56",
size:"112,20",
text:"Codice Navision",
transparent:true,
typeid:7,
uuid:"5909FADE-6FD9-4A50-B33B-0CE70D057A87"
},
{
anchors:15,
borderType:"LineBorder,1,#cacaca",
formIndex:4,
lineSize:1,
location:"406,261",
shapeType:1,
size:"367,195",
transparent:true,
typeid:21,
uuid:"59F0DE2A-081E-4FFC-903E-6E802245E643"
},
{
extendsID:"7D3B8205-CC20-43C3-989D-1A127CF8CFD6",
onActionMethodID:"19C8FCFF-4C5A-47B3-BB00-AFEDF4C85483",
typeid:7,
uuid:"6249C021-F2A2-4E89-A6C1-C828A9D8D663"
},
{
location:"30,130",
size:"111,20",
text:"Approva",
transparent:true,
typeid:7,
uuid:"62FFAB8D-B154-4DAA-BF5B-F72FA2269BEF"
},
{
location:"406,93",
size:"112,20",
text:"Codice FEP",
transparent:true,
typeid:7,
uuid:"643CA5FE-E720-4BA2-852C-A05A74C5A221"
},
{
formIndex:2,
location:"406,130",
size:"112,20",
text:"Richiede",
transparent:true,
typeid:7,
uuid:"682BCEFC-E5A6-45F8-9505-A367DFDFDABC"
},
{
anchors:11,
location:"559,161",
size:"211,20",
text:"Commessa operativa",
transparent:true,
typeid:7,
uuid:"7AE9AE00-AFEF-4BA2-997C-3F1FFB05CB44"
},
{
dataProviderID:"event_type_id",
displayType:2,
editable:false,
location:"151,56",
name:"Type",
onDataChangeMethodID:"-1",
size:"225,20",
text:"PC Type ID",
typeid:4,
uuid:"7F68C626-BF0A-4CAD-B97B-3C6734A0589C",
valuelistID:"E648EAFC-6173-466A-B643-FEE951F57F40"
},
{
anchors:13,
borderType:"LineBorder,1,#c0c0c0",
items:[
{
containsFormID:"203601D6-A2F7-4D31-8F18-166773B569DB",
location:"79,334",
name:"events_users_list",
relationName:"events_to_users_events",
text:"events_users_list",
typeid:15,
uuid:"6107B75A-0227-4F72-A8E2-C4D933ED0DF2"
}
],
location:"30,261",
name:"tabUserEvent",
printable:false,
size:"346,195",
tabOrientation:-1,
transparent:true,
typeid:16,
uuid:"80437D75-2CD8-41CA-B8FF-E718C251C1F6"
},
{
dataProviderID:"unit_measure",
displayType:2,
editable:false,
location:"151,93",
name:"unit_measure",
size:"225,20",
typeid:4,
uuid:"855848E8-EB00-4D7B-AA0F-FD4D335A4DA5",
valuelistID:"AA305A7E-720C-4499-B0A9-6EE279659F2E"
},
{
dataProviderID:"event_cost",
format:",##0.00",
location:"151,204",
name:"fEventCost2",
size:"225,20",
typeid:4,
uuid:"89D1E87A-174A-458C-B75C-7F2362EDA37A",
visible:false
},
{
dataProviderID:"event_cost",
format:",##0.00",
location:"151,168",
name:"fEventCost",
size:"225,20",
typeid:4,
uuid:"8F305B37-4E7E-47BD-B429-7B73CDC3534E",
visible:false
},
{
anchors:15,
formIndex:5,
horizontalAlignment:0,
location:"406,334",
size:"367,27",
text:"Da implementare [approvers] in read-only",
transparent:true,
typeid:7,
uuid:"A77D84A5-E514-4BDD-B188-1290972E8568"
},
{
anchors:11,
dataProviderID:"event_name",
location:"151,19",
name:"Name",
size:"619,20",
text:"Title",
typeid:4,
uuid:"CEDC7ACF-247E-49D5-B579-211F4EE3AA15"
},
{
anchors:11,
formIndex:2,
location:"559,131",
size:"211,20",
text:"Approvazione a preventivo",
transparent:true,
typeid:7,
uuid:"D82DFDAE-0CFF-4E09-8444-62A3BFB3B7D1"
},
{
formIndex:1,
groupID:"group_profit_center",
location:"30,168",
size:"111,20",
text:"Centro Profitto",
transparent:true,
typeid:7,
uuid:"EECA7E65-855B-4700-92BF-1B89AD9A5E38",
visible:false
},
{
anchors:11,
dataProviderID:"external_code_fep",
location:"532,93",
name:"fepCode",
size:"238,20",
typeid:4,
uuid:"F0876AC8-8C68-4F6C-99C8-1BDC72E4BA5E"
},
{
anchors:11,
location:"559,191",
size:"211,20",
text:"Tipo Straordinario",
transparent:true,
typeid:7,
uuid:"F0FFF230-D0BA-4939-81AD-6845775EF4E8"
},
{
dataProviderID:"approver_profit_center_id",
displayType:2,
editable:false,
groupID:"group_profit_center",
location:"151,168",
size:"225,20",
typeid:4,
uuid:"F64B0189-FB9F-46B4-95CC-742CB2F1EF65",
valuelistID:"73312BF3-989F-48F4-85E1-AB96BDEA9A3F",
visible:false
},
{
dataProviderID:"approver_figure",
displayType:2,
editable:false,
location:"151,130",
onDataChangeMethodID:"AE27FD04-86C5-4CC1-B1E9-681BEADCBBD3",
size:"225,20",
typeid:4,
uuid:"FA62A366-8345-431F-8A45-10C17723E72F",
valuelistID:"A462FFBB-8250-417C-B6BE-A7A965AB0ECD"
},
{
dataProviderID:"is_requestable",
displayType:4,
formIndex:3,
horizontalAlignment:2,
location:"532,131",
name:"isRequestable",
onDataChangeMethodID:"-1",
size:"27,20",
transparent:true,
typeid:4,
uuid:"FF67FB50-5B03-4401-A08C-23ED52D632B3"
}
],
name:"events_details",
navigatorID:"-1",
onLoadMethodID:"-1",
onNewRecordCmdMethodID:"0",
onRecordEditStartMethodID:"-1",
onRecordEditStopMethodID:"5CAA4819-E609-41BB-9AF5-1EC523FC19DB",
showInMenu:true,
size:"800,576",
styleName:"GeCo",
typeid:3,
uuid:"E5054175-94C1-40B5-A45E-E98003FC3E38",
view:0