
/**
 * Handle changed data.
 *
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"651CCB77-84B0-4164-A5A8-9B8B164D7C0B"}
 */
function onDataChange(event) {
	if (foundset.users_to_users_cars.user_car_type == 0) {
		elements.corporateRefound.enabled = false;	
		elements.cilindrata.enabled = true;
		//application.output(foundset.users_to_users_cars.users_cars_to_refound_km.refound_value);
	}
	else if (foundset.users_to_users_cars.user_car_type == 1) {
		elements.cilindrata.enabled = false;
		elements.corporateRefound.enabled = true;	
	}
}
