/**
 * Perform the default action. This method should be overridden using this template
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * 	for (var index = 1; index <= foundset.getSize(); index++) {
 *		var rec = foundset.getRecord(index);
 *		if (rec['is_selected']) {
 *
 *		}
 *	}
 *
 * @properties={typeid:24,uuid:"C10551E0-E54D-4C12-9974-C035ACA5704E"}
 * @AllowToRunInFind
 */
function processSelection(event) {
	clearAndClose(event);
}

/**
 * Implementation of the updateUI Hook
 *
 * @param {JSEvent} event The event that triggered the action.
 *
 * @properties={typeid:24,uuid:"0B25E3F0-BD9E-49CC-AB96-E5F38B108C1F"}
 */
function updateUI(event) {
	//_super.updateUI(event);
	controller.readOnly = false;
	elements.buttonCancel.enabled = true;
	elements.buttonRefresh.enabled = true;
	elements.buttonSave.enabled = true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"97F93B10-7380-4397-A1CC-D119D902FAE3"}
 */
function clearAndClose(event) {
	areAllSelected = 0;
	selectAllRecords(event);
	controller.getWindow().destroy()
}

/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"312E5AA0-0747-4DA9-89DC-5DE7522D6438"}
 */
function selectAllRecords(event) {
	for (var index = 1; index <= foundset.getSize(); index++) {
		var rec = foundset.getRecord(index);
		rec['is_selected'] = areAllSelected;
	}
}


/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F586D8A6-EC26-4840-8301-FD1EF1E18A1F"}
 */
function resetFilter(event) {
  foundset.loadAllRecords();
  // move to the first record (otherwise the cursor remains at the last selected record)
  foundset.setSelectedIndex(1);
}
