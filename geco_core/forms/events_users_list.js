/**
 * Opens popoup to add new users to the selected event
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"832392E9-29DF-4406-AA94-23B612FC1868"}
 */
function openPopupSelect(event) {
	var win = application.createWindow("selezione", JSWindow.MODAL_DIALOG);
	globals.callerForm = forms[event.getFormName()];
	win.setInitialBounds(1000, 1000, 500, 500);
	win.show(forms.events_users_list_popup);
}