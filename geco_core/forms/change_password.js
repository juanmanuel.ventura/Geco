/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8A0E6E9C-D55C-47F4-ABCC-84C6C915726D"}
 */
var username = globals.currentuserid_to_users.user_name;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"4B30EF86-5004-4955-BF3D-986319EA1C14"}
 */
var oldPassword = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2C18D9B7-59C4-4090-8F49-E426692563C9"}
 */
var newPassword = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E4FD0793-788B-45D1-946A-A47A90EEA9A6"}
 */
var repeatNewPassword = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"35F60ED7-7826-445F-BD10-7019E1655F1B"}
 */
var result = null;

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @protected 
 *
 * @properties={typeid:24,uuid:"A40DB8FA-9279-4096-AECD-3BE87FD37B41"}
 * @AllowToRunInFind
 */
function saveNewPassword(event) {
	application.output(globals.messageLog + 'START change_password.saveNewPassword() '+username,LOGGINGLEVEL.INFO);
	result = null;
	if (!oldPassword) {
		result = 'Errore: La password non può essere vuota';
	} else if (!newPassword) {
		result = 'Errore: La nuova password non può essere vuota';
	} else if (!repeatNewPassword || repeatNewPassword != newPassword) {
		result = 'Errore: Le due password non corrispondono';
	}
	if (result != null) {
		application.output(globals.messageLog + 'STOP change_password.saveNewPassword() '+result,LOGGINGLEVEL.INFO);
		return;
	}
	try {
		hasUserMedicalPractice();
		globals.changePassword(username, oldPassword, newPassword);
		if (globals.hasMedicalPractice == false) {
			close(event);
			var serverUrl = application.getServerURL();
			var solutionName = application.getSolutionName();
			application.output(globals.messageLog + 'change_password.saveNewPassword() serverUrl ' + application.getServerURL(), LOGGINGLEVEL.DEBUG);
			application.showURL(serverUrl + '/servoy-webclient/application/solution/' + solutionName, '_self');
			security.logout(application.getSolutionName());
		}
	} catch (e) {
		result = e['message'];
		application.output(globals.messageLog + 'change_password.saveNewPassword() ERROR: ' + e.message, LOGGINGLEVEL.ERROR);
	}
	application.output(globals.messageLog + 'STOP change_password.saveNewPassword() ' + username, LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"0A8A776F-22B7-4FAA-9833-70B994AF3599"}
 */
function close(event) {
	application.output(globals.messageLog + 'START change_password.close() ', LOGGINGLEVEL.DEBUG);
	application.output('close popup  serverUrl ' + application.getServerURL());
	result = null;
	repeatNewPassword = null;
	newPassword = null;
	oldPassword = null;
	application.output(globals.messageLog + 'STOP change_password.close() ', LOGGINGLEVEL.DEBUG);
	return _super.close(event)
}

/**
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"857AB55F-D0BF-4591-A2A2-548192F35D69"}
 */
function hasUserMedicalPractice() {
	globals.hasMedicalPractice = false;
	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');

	if (users.find()) {
		users.user_id = globals.currentUserId;
		var totUsers = users.search();
		if (totUsers > 0) {
			var recUser = users.getRecord(1);
			if (recUser.has_medical_practice == 1 && recUser.mp_start_date != null && recUser.mp_end_date != null) {
				globals.hasMedicalPractice = true;
			}
		}
	}
	application.output('----------------currentUserId: ' + globals.currentUserId + ' hasMedicalPractice? ' + globals.hasMedicalPractice);
}
