/**
 * @param {JSEvent} event the event that triggered the action
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"38257D7F-B31E-44A0-B9B5-E48504FFABFC"}
 */
function enableExternalCode(event) {

	if (!foundset.has_job_order) {
		elements.fldExternal_code.enabled = true;
		elements.fldExternal_code.bgcolor = '#ffffff';
	} else {
		elements.fldExternal_code.enabled = false;
		elements.fldExternal_code.bgcolor = '#f8f8f8';
		foundset.external_code_navision = null;
	}

	return true
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"9CBD909B-9EA9-4C25-BBE0-AA8651CC09DF"}
 */
function onRecordSelection(event) {
	globals.selectedEvent = foundset.event_id;
	enableExternalCode(event);
	toggleControls(event);
	return _super.onRecordSelection(event)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"19C8FCFF-4C5A-47B3-BB00-AFEDF4C85483"}
 */
function saveEdits(event) {
	forms.events_users_list.deleteRecordsSelected(event);
	_super.saveEdits(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"59035345-C183-4F84-B4FC-5D91EB64B68C"}
 */
function stopEditing(event) {
	forms.events_users_list.areAllSelected = 0;
	forms.events_users_list.selectAllRecords(event);
	if (foundset.getSelectedRecord().isNew()) foundset.setSelectedIndex(1);
	_super.stopEditing(event);
	toggleControls(event);
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"AE27FD04-86C5-4CC1-B1E9-681BEADCBBD3"}
 */
function onDataChange(oldValue, newValue, event) {
	toggleControls(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"11FBAA2A-BFCF-450D-A720-AF09735C4FAB"}
 */
function toggleControls(event) {

	switch (foundset.approver_figure) {
	case 1:
		elements.group_profit_center.visible = true;
		foundset.has_job_order = 0;
		break;
	case 3:
		elements.group_profit_center.visible = false;
		foundset.has_job_order = 1;
		break;
	default:
		elements.group_profit_center.visible = false;
		foundset.has_job_order = 0;

		break;
	}

	miniUpdateUI(event)
	enableExternalCode(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"D2FBCCDB-E63A-4654-82F0-5A4F6F6967D8"}
 */
function updateUI(event) {
	_super.updateUI(event);

	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
		elements.buttonEdit.visible = false;
	}

	if (!isEditing()) foundset.sort('event_name asc');

	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		bundle.elements['split'].visible = !isEditing();
		bundle['focus'] = foundset.event_name ? foundset.event_name : 'Nuovo evento';
		bundle.elements['focus'].visible = isEditing();
	}
	
	elements.buttonEdit.enabled = foundset.getSize() > 0;
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"CEE80011-4AFA-4F44-85AF-09A58FBE7309"}
 */
function miniUpdateUI(event) {
	elements.lEventCost.visible = (foundset && foundset.approver_figure) && (foundset.approver_figure == 2 || foundset.approver_figure == 3) ? true : false;
	elements.fEventCost.visible = (foundset && foundset.approver_figure) && (foundset.approver_figure == 2 || foundset.approver_figure == 3) ? true : false;
	elements.lEventCost2.visible = (foundset && foundset.approver_figure) && (foundset.approver_figure == 1) ? true : false;
	elements.fEventCost2.visible = (foundset && foundset.approver_figure) && (foundset.approver_figure == 1) ? true : false;
}
