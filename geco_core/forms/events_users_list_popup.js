/**
 * @type {Number}
 * @properties={typeid:35,uuid:"35781881-EDDD-4387-80A2-1FF043563932",variableType:4}
 */
var selectedProfitCenter = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"40AF8FEF-D712-47CC-94F3-4AC35FB7948E",variableType:4}
 */
var selectedContract = null;

/**
 * @properties={typeid:24,uuid:"0D517812-6AD7-482C-BA74-EEAF91E2AE1C"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	areAllSelected = 0;
	selectAllRecords(event);
	searchValidRecord();
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"3CFE2C6D-1A36-49E9-A83A-A32FFB793FBE"}
 */
function resetFilter(event) {
	selectedProfitCenter = null;
	selectedContract = null;
	searchValidRecord();
	areAllSelected = 0;
	selectAllRecords(event);

}

/**
 * Perform the element default action. *
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"067EF201-6484-4535-8AB1-8FF88706A0BB"}
 */
function processSelection(event) {
	if (foundset) {
		for (var index = 1; index <= foundset.getSize(); index++) {
			var rec = foundset.getRecord(index);
			if (rec.is_selected) {
				globals.callerForm.foundset.newRecord(false)
				globals.callerForm.foundset['user_id'] = rec.user_id;
				globals.callerForm.foundset['event_id'] = globals.selectedEvent;

			}
		}
	}
	resetFilter(event);
	_super.processSelection(event);

}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"ECA47198-2AEB-4F1F-8DEE-C010FB150A9D"}
 */
function clearAndClose(event) {
	resetFilter(event);
	foundset.removeFoundSetFilterParam('filterUsersForEvent');
	_super.clearAndClose(event)
}

/**
 *
 * @properties={typeid:24,uuid:"A9387BFF-149B-4825-AC8F-01CB0D091607"}
 * @AllowToRunInFind
 */
function searchValidRecord() {

	if (foundset.find()) {
		foundset.profit_center_id = selectedProfitCenter;
		foundset.user_type_id = selectedContract;
		foundset.is_enabled = "1";
		foundset.search();
	}
}

/**
 *  Callback method for when form is shown.
 * @param {Boolean} firstShow
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"59DFDBCB-E1AA-43F4-98E7-5931662D8652"}
 */
function onShow(firstShow, event) {
	foundset.addFoundSetFilterParam('user_id', 'not in', 'select user_id from users_events where event_id = "' + globals.selectedEvent + '"', 'filterUsersForEvent');
	searchValidRecord();
	return _super.onShow(firstShow, event)
}
