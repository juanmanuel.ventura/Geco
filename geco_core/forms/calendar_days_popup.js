/**
 * @type {Number}
 * @properties={typeid:35,uuid:"3BE0F02C-C706-4B09-9213-DF469806A6EF",variableType:4}
 */
var monthHoliday = 1;
/**
 * @type {Number} *
 * @properties={typeid:35,uuid:"F3614CB9-82CD-462E-BC2D-D669D4B565A5",variableType:-4}
 */
var yearHoliday = new Date().getFullYear();

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FE2C5913-8D4B-48C8-9515-3C4E42F66AFA"}
 */
var headOffice = null;

/**
 * Filter records
 * @param {JSEvent} event the event that triggered the action
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"A18EAAC2-D088-4E9E-A5E5-9FFB5F2D3EEA"}
 */
function applyFilter(event) {
	databaseManager.setAutoSave(true);
	if (foundset.find()) {
		foundset.calendar_year = yearHoliday;
		foundset.calendar_month = monthHoliday
		foundset.search();
	}
	databaseManager.setAutoSave(false);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"A3C993B8-5993-4046-984E-07BB5B5ED4C8"}
 */
function clear(event) {
	monthHoliday = 1;
	yearHoliday = new Date().getFullYear();
	applyFilter(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"A713BE70-00ED-4513-8221-14B1A70010EF"}
 */
function close(event) {
	databaseManager.setAutoSave(true);
	controller.getWindow().destroy();
}

///**
// * @param {JSEvent} event the event that triggered the action
// * @properties={typeid:24,uuid:"87533271-AD5E-4EB6-8641-18B92D862918"}
// */
//function saveHoliday(event){
//	globals.saveHoliday();
//	close(event);
//}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"87533271-AD5E-4EB6-8641-18B92D862918"}
 */
function saveHoliday(event) {

	//processHoliday(event);
	processHoliday2(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * Perform the element default action.
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"46549947-43DD-418A-BA8D-00E66BB07BA1"}
 */
function processHoliday(event) {
	updateUI(event);
	foundset.is_working_day = (foundset.is_holiday == 1) ? 0 : 1;
	var event_id;
	try {
		var success = databaseManager.removeTableFilterParam('geco', 'holidayFilter');
		if (foundset) {
			/** @type {JSFoundSet<db:/geco/events>} */
			var events = databaseManager.getFoundSet('geco', 'events');
			if (events.find()) {
				events.events_to_event_types.event_type_id = 6;
				if (events.search() == 1) event_id = events.event_id;
			}
			if (globals.isEmpty(event_id)) {
				application.output('Non è stato trovato evento');
				return;
			}
			//application.output(event_id);
			/** @type {JSFoundSet<db:/geco/users>} */
			var users = databaseManager.getFoundSet('geco', 'users');
			if (users.find()) {
				users.user_type_id = 7;
				users.is_enabled = 1;
				var result = users.search();
				application.output('dipendenti ' + result);
			}

			//ciclo per tutti i dipendenti abilitati
			for (var x = 1; x <= users.getSize(); x++) {
				var recordUser = users.getRecord(x);

				//cerco eventi  per l'utente e il giorno se non esiste lo creo
				/** @type {JSFoundSet<db:/geco/events_log>} */
				var events_log = databaseManager.getFoundSet('geco', 'events_log');

				//calendar_days
				for (var index = 1; index <= foundset.getSize(); index++) {
					//databaseManager.setAutoSave(false);
					var rec = foundset.getRecord(index);
					//application.output("*** "+rec.calendar_date + ' festa ' + rec.is_holiday);
					//se festivo inserisco evento
					if (rec.is_holiday == 1) {
						//						rec.is_working_day = 0;
						events_log.loadAllRecords();
						if (events_log.find()) {
							events_log.event_log_date = rec.calendar_date;
							events_log.user_id = recordUser.user_id;
							events_log.external_code_fep = events.external_code_fep;
							if (events_log.search() == 0) {
								//application.output('++++ inserisco evento ' + rec.calendar_date + ' ' + recordUser.user_name);
								events_log.newRecord();
								events_log.user_id = recordUser.user_id;
								events_log.event_log_date = rec.calendar_date;
								events_log.event_log_status = 4;
								events_log.external_code_fep = events.external_code_fep;
								events_log.event_id = event_id;
							}
						}
					}
					//altrimenti cancello evento se non è festivo per la sede di appartenza
					else {
						//						if (rec.calendar_weekday !=6 && rec.calendar_weekday !=0) rec.is_working_day = 1;
						//var date = new Date(yearHoliday, recordUser.users_to_head_office.head_office_month_holiday, recordUser.users_to_head_office.head_office_day_holiday);
						//application.output('festivo per user ' + recordUser.user_name + ' ' + date);
						events_log.loadAllRecords();
						if (events_log.find()) {
							events_log.event_log_date = rec.calendar_date;
							//events_log.event_log_date = '!'+date;
							events_log.user_id = recordUser.user_id;
							events_log.external_code_fep = events.external_code_fep;
							if (events_log.search() > 0) {
								for (var ev = 1; ev <= events_log.getSize(); ev++) {
									//application.output('---- cancello evento ' + events_log.event_log_id + ' ' + rec.calendar_date + ' ' + recordUser.user_name);
									events_log.deleteRecord(ev);
								}
							}
						}
					}

					//cerco evento festivo per il patrono per l'utente
					if (rec.calendar_days_to_head_office.head_office_id != null && recordUser.head_office_id == rec.calendar_days_to_head_office.head_office_id) {
						//						application.output('festivo ' + rec.calendar_days_to_head_office.head_office_day_holiday + ' ' +
						//						rec.calendar_days_to_head_office.head_office_month_holiday +' ' +rec.calendar_days_to_head_office.head_office_city);
						events_log.loadAllRecords();
						if (events_log.find()) {
							events_log.event_log_date = rec.calendar_date;
							events_log.user_id = recordUser.user_id;
							events_log.external_code_fep = events.external_code_fep;
							if (events_log.search() == 0) {
								//								application.output('inserisco festivo ' + rec.calendar_days_to_head_office.head_office_day_holiday + ' ' +
								//								rec.calendar_days_to_head_office.head_office_month_holiday + ' user ' +recordUser.user_name);
								events_log.newRecord();
								events_log.user_id = recordUser.user_id;
								events_log.event_log_date = rec.calendar_date;
								events_log.event_log_status = 4;
								events_log.external_code_fep = events.external_code_fep;
								events_log.event_id = event_id;
							}
						}
					}
				}
			}

		}
	} catch (e) {
		application.output(e);
		application.output("Holiday failed");
	} finally {
		//databaseManager.setAutoSave(true);
		success = databaseManager.addTableFilterParam('geco', 'events_log', 'event_id', 'not in', [34], 'holidayFilter');
		//plugins.busy.unblock();
		close(event);
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * Perform the element default action.
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"0B76A9CA-B026-4907-AC5D-5411B96A0370"}
 */
function processHoliday2(event) {
	//updateUI(event);
	try {
		if (foundset) {
			databaseManager.setAutoSave(false);
			for (var index = 1; index <= foundset.getSize(); index++) {
				var rec = foundset.getRecord(index);
				application.output("*** " + rec.calendar_date + ' festa ' + rec.is_holiday);
				var maxReturnedRows = 10000;
				var sql_query = 'call sp_events_holiday(?,?,?,?)';

				var dataset = null;
				//var line = '';
				var riga = null;
				var rigaStr = null;

				var arguments = new Array();

				var mm = rec.calendar_month;
				if (mm.toString().length == 1) mm = '0' + mm;
				var yyyy = rec.calendar_year;
				var dd = rec.calendar_day;
				if (dd.toString().length == 1) dd = '0' + dd;
				arguments[0] = yyyy + '-' + mm + '-' + dd;
				arguments [1] = scopes.globals.currentUserId;
				//se festivo inserisco evento (1) altrimenti cancello evento (0)
				arguments[2] = rec.is_holiday;
				arguments[3] = 0;
				if (rec.calendar_days_to_head_office.head_office_id != null) {
					arguments[2] = 1;
					arguments[3] = rec.calendar_days_to_head_office.head_office_id;
				}
				application.output('input ' + arguments);

				dataset = databaseManager.getDataSetByQuery('geco', sql_query, arguments, maxReturnedRows)

				for (var insertIndex = 1; insertIndex <= dataset.getMaxRowIndex(); insertIndex++) {
					riga = dataset.getRowAsArray(insertIndex);
					//application.output('riga array' + riga);
					rigaStr = riga.join('|');
					//line = line + rigaStr + '\n';
					application.output('riga ' + rigaStr);
				}
			}
			databaseManager.setAutoSave(true);
			databaseManager.getFoundSet('geco','events_log').loadAllRecords();
		}
	} catch (e) {
		application.output(e);
		application.output("Holiday failed");
	} finally {
		
		close(event);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"AC0A7814-6C9E-437D-92B9-A492CDE8B00E"}
 */
function onAction(event) {
	application.output('click ' + foundset.is_holiday);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"6A08D946-91AA-420E-B731-FD1BFB6A7D03"}
 */
function updateUI(event) {
		//application.output('updateUI');
}

