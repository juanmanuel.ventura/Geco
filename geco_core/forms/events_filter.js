/**
 * @type {Number}
 * @properties={typeid:35,uuid:"16CEF9F3-82DE-4872-A2AE-7908730FA6D6",variableType:8}
 */
var eventType = null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"C8630CD9-D52C-460F-A389-4C1577DA23D9"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter(event);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"28EA828A-1199-4E4F-A603-710ED9F5E903"}
 */
function resetFilter(event, goTop) {
	eventType = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"E4FB7E99-9315-4F0E-A74C-293852EBF156"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.event_type_id = eventType;
		foundset.search();
		foundset.sort('event_name asc');
	}
}
