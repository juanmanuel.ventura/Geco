/**
 * @type {Number}
 * @properties={typeid:35,uuid:"60C4DD98-E21A-4BAF-8AA2-73D9360B1CDF",variableType:8}
 */
var valMedicalPractice = null;
	
/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"4893863D-5F13-4D19-B508-CDFE6556B7C3",variableType:-4}
 */
var endValidDate = false;

/**
 * @properties={typeid:35,uuid:"9526233B-C47C-43A0-BD43-3900ED9C225C",variableType:-4}
 */
var endDateNull = null;

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"5AD8BF87-5948-4A96-A75E-303ADDA3F02C"}
 */
function onDataChange(oldValue, newValue, event) {
	if(foundset){
		var rec = foundset.getSelectedRecord();
		rec.working_hours = rec.users_to_contract_types.daily_workable_hours;
	}
	newValue = rec.users_to_contract_types.is_weekly;
	if(newValue == 1){
		rec.shift_start = null;
		rec.shift_stop = null;
		rec.break_start = null;
		rec.break_stop = null;
		elements.shift_start.enabled = false;
		elements.shift_stop.enabled = false;
		elements.shift_startc.enabled = false;
		elements.shift_stopc.enabled = false;
	} else  {
		rec.shift_start = 540;
		rec.shift_stop = 1080;
		rec.break_start = 780;
		rec.break_stop = 840;
		elements.shift_start.enabled = true;
		elements.shift_stop.enabled = true;
		elements.shift_startc.enabled = true;
		elements.shift_stopc.enabled = true;
	}
		 
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"0C27BA42-0577-4AB9-A373-93C9C875B7A6"}
 */
function onDataChangeShiftWork(oldValue, newValue, event) {
	var shiftWork = newValue;
	var rec = foundset.getSelectedRecord();
	if(shiftWork == 0){
		rec.shift_work_type_id = null;
	}
	return true
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"A902ED70-73E0-4225-A92B-9B146199302D"}
 */
function onDataChangeHasMedicalPractice(oldValue, newValue, event) {
	valMedicalPractice = null;
	if(oldValue != newValue && newValue == 1){
		valMedicalPractice = newValue;
		elements.fStartDate.enabled = true;
		elements.fEndDate.enabled = true;
		//setto la data fine copertura al valore di default 31/12 dell'anno in corso.
		var nowDate=new Date();
		foundset.mp_end_date=new Date(nowDate.getFullYear(),11,31);
	}else if (oldValue != newValue && newValue == 0){
		foundset.mp_end_date = null;
		foundset.mp_start_date = null;
		elements.fStartDate.enabled = false;
		elements.fEndDate.enabled = false;
	}
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A05B6F31-9758-4640-AF4A-E63D01AF85EC"}
 */
function onShow(firstShow, event) {
	valMedicalPractice = null;
}

/**
 * Handle changed data.
 *
 * @param {Date} oldValue old value
 * @param {Date} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"E2057EF8-C1B9-4408-8BDB-D7F7CD12E98D"}
 */
function onDataChangeEndValidDate(oldValue, newValue, event) {
	endValidDate = false;
	endDateNull = null;
	if(oldValue != newValue && newValue != null) {
		endValidDate = true;
	} else endDateNull = 1;
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"2902BCAB-B6FC-4EAB-A786-146FCFD3835A"}
 */
function onDataChangeHasDailyLunch(oldValue, newValue, event) {
	if (oldValue != newValue && newValue == 0) {
		foundset.ticket_company_id = null;
		foundset.daily_launch_rate = null;
	}
	
	updateUI(event);
}

/**
 * @param event
 * @properties={typeid:24,uuid:"66164289-2A8C-46D6-95E5-B2503FB4B286"}
 */
function updateUI(event) {
//	Controlli sui buoni pasto
	elements.daily_lunch.enabled = forms.users_details.isEditing() && foundset.has_daily_lunch == 1;
	elements.fTicketCompany.enabled = forms.users_details.isEditing() && foundset.has_daily_lunch == 1;

}