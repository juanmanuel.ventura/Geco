dataSource:"db:/geco/users",
extendsID:"DD9194C5-206D-4FF1-A075-EA268CA83E93",
initialSort:"users_to_contacts.last_name asc, users_to_contacts.first_name asc",
items:[
{
formIndex:5,
location:"10,108",
size:"108,20",
text:"Contratto",
transparent:true,
typeid:7,
uuid:"02E3CAF0-AA46-400F-9461-F56800E94053"
},
{
anchors:3,
extendsID:"3D006472-EAC7-4E81-BED9-40145EAC4252",
location:"365,50",
onActionMethodID:"8566F51E-97C6-414E-8FA7-F33C1C609B3E",
typeid:7,
uuid:"21E5833D-C0ED-4BE8-85D0-4047484AF8E5"
},
{
extendsID:"F068A998-61CD-49A1-9C20-3140AB5216E9",
location:"318,220",
onActionMethodID:"2883D449-FB28-4BD4-9549-BAE7D0B7F580",
typeid:7,
uuid:"2334175F-ECA8-4CE0-BBFF-246E3E1008A3"
},
{
extendsID:"17ABEB5C-8093-42C2-9522-04AC15F62432",
location:"10,11",
typeid:7,
uuid:"24545CB3-C1BE-41F4-9D52-64E05DB59DCD",
visible:false
},
{
dataProviderID:"user_real_name",
extendsID:"00AF8FA3-F4E3-439B-9614-C478F495969B",
location:"95,180",
size:"245,20",
typeid:7,
uuid:"71A8B50B-9606-4A57-9447-354533E84843",
visible:true
},
{
anchors:11,
dataProviderID:"selectedProfitCenter",
displayType:2,
editable:false,
formIndex:3,
location:"123,79",
name:"cbProfitCenter",
onActionMethodID:"059CC9AC-B94B-4692-8EA4-88AEBFF7B49A",
size:"218,22",
text:"Filtra per Profit Center",
typeid:4,
uuid:"78F0B1A9-89E5-4F37-B109-2438B2F954C0",
valuelistID:"73312BF3-989F-48F4-85E1-AB96BDEA9A3F"
},
{
anchors:11,
dataProviderID:"selectedContract",
displayType:2,
editable:false,
formIndex:3,
location:"123,108",
name:"cbContractType",
onActionMethodID:"059CC9AC-B94B-4692-8EA4-88AEBFF7B49A",
size:"218,22",
toolTipText:"Filtra per  Contratto",
typeid:4,
uuid:"89D9B633-9A9C-4A8B-B737-4C38540AC830",
valuelistID:"0EB8D3B8-8AAA-419F-B3CB-E0667514CAAF"
},
{
formIndex:5,
location:"10,79",
size:"108,20",
text:"Profit Center",
transparent:true,
typeid:7,
uuid:"8F4D7586-E2D4-4C6A-BC7C-F6F7C21009E7"
},
{
extendsID:"6D6DDF53-89FA-4E68-BA5F-1088B0BC0001",
height:256,
typeid:19,
uuid:"97471BD9-D47C-40C0-9A06-3F4700E83EDD"
},
{
anchors:11,
dataProviderID:"selectedUser",
displayType:10,
formIndex:1,
location:"123,50",
name:"cbUsers",
onActionMethodID:"-1",
onDataChangeMethodID:"059CC9AC-B94B-4692-8EA4-88AEBFF7B49A",
size:"218,22",
text:"Filtra per Utente",
typeid:4,
uuid:"97951A64-A826-44B4-97E5-D11BC2AA4352",
valuelistID:"3AB8DD5C-F02E-40DF-879B-9BFBCE22AF8D"
},
{
extendsID:"9171A290-AB32-4FFA-844F-592B12C1497D",
formIndex:0,
location:"133,50",
onActionMethodID:"059CC9AC-B94B-4692-8EA4-88AEBFF7B49A",
size:"51,22",
typeid:4,
uuid:"ADE07031-5216-43B6-9C14-2ED145F35F24",
valuelistID:"0"
},
{
dataProviderID:"is_selected",
extendsID:"1CB02C00-C9EC-4483-9ABD-E8881485A3E8",
location:"361,180",
onActionMethodID:"-1",
typeid:4,
uuid:"AEDA5F4D-893E-4DFF-BA16-678EE43AC155"
},
{
anchors:15,
dataProviderID:"enrollment_number",
formIndex:2,
location:"10,180",
name:"defaultLabelc",
onRenderMethodID:"-1",
showClick:false,
showFocus:false,
size:"80,20",
transparent:true,
typeid:7,
uuid:"B90CB48D-913A-469D-834B-B24AF895096A"
},
{
extendsID:"EF71FF5B-572C-4871-A155-0DADED5C5006",
formIndex:3,
location:"341,144",
typeid:4,
uuid:"B9A04F45-183F-4D63-8203-109C3C43274B"
},
{
extendsID:"041188E2-43FB-48F3-B66E-627E32748814",
height:208,
typeid:19,
uuid:"BE99C1C7-5012-4C8A-A9FD-6A5C9295D332"
},
{
anchors:11,
displaysTags:true,
formIndex:4,
horizontalAlignment:0,
location:"10,11",
size:"380,28",
text:"Seleziona utenti per la commessa %%selected_job_orders_to_job_orders.job_order_title%%",
transparent:true,
typeid:7,
uuid:"C05568B8-6DD8-4E13-A6D7-57CD198147CB"
},
{
formIndex:5,
location:"10,50",
size:"108,20",
text:"Utente",
transparent:true,
typeid:7,
uuid:"D5416458-5EAC-4CCB-98B7-3C19B472846A"
},
{
extendsID:"FEA7EA63-6CCF-41C0-82C9-2D0FED2A9C63",
formIndex:4,
location:"228,144",
typeid:7,
uuid:"DBB8C5E4-DB23-4863-AE61-A0D8BCC72CC6"
},
{
extendsID:"5875382F-5113-499E-A282-9BECE90ED1AF",
location:"364,220",
typeid:7,
uuid:"DC896101-76B4-4511-8C5D-548F1A1DCF72"
},
{
extendsID:"DE202A8E-133D-4B7C-8A96-F9FBCE65404C",
location:"359,220",
onActionMethodID:"FA54E23D-5FA1-4A5D-95AB-6590834421A5",
typeid:7,
uuid:"E827CC97-C6EB-49FE-B578-B4838E02BEEC"
},
{
extendsID:"13E7D48D-42CC-42CF-BCFB-180E8223DAF0",
location:"2,135",
typeid:7,
uuid:"E962BC30-06CD-4F8E-A752-33AB7883B311"
},
{
extendsID:"0094B6DC-EF33-49F2-A2EF-A8B9CE28533B",
height:172,
typeid:19,
uuid:"EE283B02-EC19-4E1B-B49E-DF3949810913"
}
],
name:"job_order_users_list_popup",
namedFoundSet:"separate",
size:"400,256",
styleName:"GeCo",
titleText:"Utenti",
typeid:3,
uuid:"76BFC58B-4BF1-49E2-BA00-354563743AAA"