/**
 * @type {Number}
 * @properties={typeid:35,uuid:"5771E398-4FB1-47A1-B9DA-43382B59BC4A",variableType:4}
 */
var selectedEventType = null;

/**
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"6851C947-2D2F-4258-9E95-7B8D300E49AC"}
 */
function applyFilter(event) {
	areAllSelected = 0;
	selectAllRecords(event);
	searchValidRecord();
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"6E2B6E7D-05E6-45C4-AF5F-C3F5D764A5B6"}
 */
function resetFilter(event) {
	selectedEventType = null;
	searchValidRecord();
	areAllSelected = 0;
	selectAllRecords(event);
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"C4A4E83B-8F97-4830-8471-D44B0033B641"}
 */
function processSelection(event) {
	for (var index = 1; index <= foundset.getSize(); index++) {
		var rec = foundset.getRecord(index);
		if (rec.is_selected) {
			globals.callerForm.foundset.newRecord(false)
			globals.callerForm.foundset['user_id'] = globals.selectedUser;
			globals.callerForm.foundset['event_id'] = rec.event_id;
		}
	}
	resetFilter(event);
	_super.processSelection(event);

}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"CB2F3B01-7086-4DA6-BEB1-8421A81FACCA"}
 */
function clearAndClose(event) {
	resetFilter(event);
	foundset.removeFoundSetFilterParam('filterEventsForUser');
	_super.clearAndClose(event)
}

/**
 * @properties={typeid:24,uuid:"9103330A-755B-4CBB-9DCE-970C3CB49EB3"}
 * @AllowToRunInFind
 */
function searchValidRecord() {

	if (foundset.find()) {
		foundset.event_type_id = selectedEventType;
		foundset.is_enabled = "1";
		foundset.search();
	}

}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8B45FDB3-5CFD-408D-9E58-1D4342D42A6D"}
 */
function onShow(firstShow, event) {
	foundset.addFoundSetFilterParam('event_id', 'not in', 'select event_id from users_events where user_id = "' + globals.selectedUser + '"', 'filterEventsForUser');
	searchValidRecord();
	return _super.onShow(firstShow, event)
}
