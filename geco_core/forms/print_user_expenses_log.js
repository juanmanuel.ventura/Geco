/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"A825B4AD-D0F9-4C8E-A9F2-D09C7D9753FC"}
 */
function onAction(event) {
	controller.print();
}

