/**
 * @type {Number}
 * @properties={typeid:35,uuid:"1ADF8D8B-569B-4696-9944-42E5737157B9",variableType:8}
 */
var selectedUser = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"5879F6B9-B008-4D52-BDF5-775C7DD94CD3",variableType:4}
 */
var selectedProfitCenter = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"672B9B37-7311-406B-8731-4C40E699B355",variableType:4}
 */
var selectedContract = null;

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"059CC9AC-B94B-4692-8EA4-88AEBFF7B49A"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	areAllSelected = 0;
	selectAllRecords(event);
	searchValidRecord();
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"8566F51E-97C6-414E-8FA7-F33C1C609B3E"}
 */
function resetFilter(event) {
	selectedUser = null;
	selectedProfitCenter = null;
	selectedContract = null;
	searchValidRecord();
	areAllSelected = 0;
	selectAllRecords(event);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"2883D449-FB28-4BD4-9549-BAE7D0B7F580"}
 */
function processSelection(event) {
	if (foundset) {

		for (var index = 1; index <= foundset.getSize(); index++) {
			var rec = foundset.getRecord(index);
			if (rec.is_selected) {
				globals.callerForm.foundset.newRecord(false)
				globals.callerForm.foundset['user_id'] = rec.user_id;
				globals.callerForm.foundset['job_order_id'] = scopes.globals.selectedJobOrder;
				//Aggiungo a una lista globale tutti gli utenti aggiunti alla commessa "X"
				/** @type {Array} */
				globals.userListAddedToJO.push(rec.user_id.toString());
				application.output('scopes.globals.selectedJobOrder ' + scopes.globals.selectedJobOrder);
				application.output(globals.callerForm.foundset['user_id'] + ' ' + globals.callerForm.foundset['job_order_id'])
			}
		}
	}
	resetFilter(event);
	_super.processSelection(event);

}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"FA54E23D-5FA1-4A5D-95AB-6590834421A5"}
 */
function clearAndClose(event) {
	resetFilter(event);
	foundset.removeFoundSetFilterParam('filterUsersForJobOrder');
	_super.clearAndClose(event)
}

/**
 *
 * @properties={typeid:24,uuid:"1012343C-0E1D-4FA5-8005-75219A88638F"}
 * @AllowToRunInFind
 */
function searchValidRecord() {
	;
	if (foundset.find()) {
		foundset.user_id = selectedUser;
		foundset.profit_center_id = selectedProfitCenter;
		foundset.user_type_id = selectedContract;
		foundset.is_enabled = "1";
		foundset.search();
	}

}

/**
 *  Callback method for when form is shown.
 * @param {Boolean} firstShow
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"0102E827-2783-4F64-B622-ADF2159B54ED"}
 */
function onShow(firstShow, event) {
	foundset.addFoundSetFilterParam('user_id', 'not in', 'select user_id from users_job_orders where job_order_id = "' + globals.selectedJobOrder + '"', 'filterUsersForJobOrder');
	searchValidRecord();
	return _super.onShow(firstShow, event)
}
