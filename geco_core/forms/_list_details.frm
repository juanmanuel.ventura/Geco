borderType:"SpecialMatteBorder,0.0,0.0,0.0,1.0,#000000,#000000,#000000,#cacaca,0.0,",
extendsID:"DBF1E072-AF41-4792-9E99-0EEF010E6E9A",
initialSort:null,
items:[
{
extendsID:"00AF8FA3-F4E3-439B-9614-C478F495969B",
location:"10,52",
size:"140,28",
typeid:7,
uuid:"0CB10A7E-2133-4C56-84C4-78DC25857F82"
},
{
extendsID:"BB4EE0E7-CE2E-4CAF-A378-0815B908EFF8",
formIndex:3,
location:"0,84",
size:"32,23",
toolTipText:"Nuovo",
typeid:7,
uuid:"0F954D44-B0D3-4D62-92E3-7B99510CB7E8"
},
{
anchors:6,
formIndex:2,
imageMediaID:"F6D12F9D-63AF-4F91-91C9-4B3846D4D807",
location:"839,276",
name:"buttonCancel",
onActionMethodID:"EC0EA81E-2415-4CB4-8567-2C5C9575F224",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
rolloverCursor:12,
rolloverImageMediaID:"C575BC66-B077-4824-B406-9B4705FD6EBC",
showClick:false,
showFocus:false,
size:"26,26",
text:"Annulla",
toolTipText:"Annulla",
transparent:true,
typeid:7,
uuid:"2023D882-FA32-4807-A84D-FF6B9128206A",
visible:false
},
{
extendsID:"8E29638D-D959-4540-94FA-FFBD8BF10BA8",
formIndex:1,
imageMediaID:"CBEAA728-4C67-4153-AD52-DA9ABF3630ED",
location:"32,84",
size:"32,23",
toolTipText:"Cancella",
typeid:7,
uuid:"8840BAAE-B924-4379-A18F-D137FFABD376"
},
{
background:"#ffffff",
extendsID:"740B59A7-711F-4E02-B80B-DE8C9563C14C",
height:312,
typeid:19,
uuid:"9B354286-0B5D-44CD-803A-5719B06416CA"
},
{
anchors:15,
borderType:"SpecialMatteBorder,1.0,0.0,1.0,0.0,#cacaca,#000000,#cacaca,#000000,0.0,",
imageMediaID:"9F1B2BB3-06A7-498D-96A3-74BE2E3CBCF1",
location:"0,264",
mediaOptions:6,
name:"footerBackground",
size:"880,48",
typeid:7,
uuid:"9EA9D161-5EDB-4A3F-86DE-2E00D67A4F23"
},
{
anchors:6,
formIndex:3,
imageMediaID:"6CE946CC-802B-4720-BEA8-83113989DEA4",
location:"839,276",
name:"buttonEdit",
onActionMethodID:"59DD7269-3917-4240-A175-8A17719533B0",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
rolloverCursor:12,
rolloverImageMediaID:"977B61AE-CE6B-4EC7-8DAA-EEA3126000B3",
showClick:false,
showFocus:false,
size:"26,26",
text:"Modifica",
toolTipText:"Modifica",
transparent:true,
typeid:7,
uuid:"AF8AFBE0-A324-4765-A378-04C691EB2CA3"
},
{
anchors:6,
formIndex:1,
imageMediaID:"56D10CC3-627D-4B5C-A04F-EF1B95CBE26D",
location:"798,276",
name:"buttonSave",
onActionMethodID:"4EFE3557-7731-4C65-80A3-8EBA08C3BF04",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
rolloverCursor:12,
rolloverImageMediaID:"12C599E0-550F-4F03-8748-47D95EC29AB3",
showClick:false,
showFocus:false,
size:"26,26",
text:"Salva",
toolTipText:"Salva",
transparent:true,
typeid:7,
uuid:"BB2C18FC-F3C8-4326-94F3-EC732AE61425",
visible:false
},
{
extendsID:"0094B6DC-EF33-49F2-A2EF-A8B9CE28533B",
height:48,
typeid:19,
uuid:"BFF4A91B-603A-4049-9465-8AA4DCC57181"
},
{
borderType:"SpecialMatteBorder,1.0,0.0,1.0,0.0,#cccccc,#000000,#cccccc,#000000,0.0,",
extendsID:"82CA51AB-C8D0-45AB-A180-859FF3648C83",
formIndex:0,
location:"0,84",
size:"880,24",
typeid:7,
uuid:"F345D3CE-B0E9-4A5A-BED9-8DFB23F7419E"
},
{
extendsID:"041188E2-43FB-48F3-B66E-627E32748814",
height:84,
typeid:19,
uuid:"FCFB1594-0DFF-48EE-A5E4-45887253C38C"
}
],
name:"_list_details",
onRecordSelectionMethodID:"B4DA9FD0-0287-4BA4-A6DD-21B9321AE891",
scrollbars:0,
size:"880,301",
styleName:"GeCo",
typeid:3,
uuid:"DA9D51BF-BB8C-4845-B48A-468C963FA46D"