/**
 * @type {Number}
 * @properties={typeid:35,uuid:"21708371-CC15-4289-B44C-9C4AAF022A06",variableType:8}
 */
var selectedStatus = 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"25819881-079C-4973-8108-D5139B03ACB8",variableType:8}
 */
var selectedProfitCenter = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"0CEE5B1B-F783-401E-AB9E-7FCD5222E75C",variableType:8}
 */
//var selectedEvent = null;
/**
 * @type {Number}
 * @properties={typeid:35,uuid:"A31C55D7-BC17-48AE-94DD-B0730899E054",variableType:8}
 */
var selectedUser = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"95E78E00-2B06-42C1-853B-78E696E7AA45",variableType:8}
 */
var selectedJobOrder = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"2D9848CA-F343-4551-A737-6762DC6408EE",variableType:8}
 */
var selectedFepCode = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"0D9754D9-207B-4985-BE8E-B004167D42D2",variableType:8}
 */
var isDirectApprover = 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"84EF95A1-F870-4E10-A5AC-EF1357BB10DF",variableType:8}
 */
var isRequestable = 0;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"85BBE860-B53C-4213-A446-9F691AB260A3",variableType:8}
 */
var approver = security.getUserUID();

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F67D15AC-729B-4C1D-8B53-0E597583A1DF",variableType:8}
 */
var selectedMonth = new Date().getMonth() + 1;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"C2214439-1BE3-451B-BAAF-F9752538AC83",variableType:8}
 */
var selectedYear = new Date().getFullYear();

/**
 * @type {String[]}
 * @properties={typeid:35,uuid:"DBE9464E-2BF5-421D-9C7D-5C9470FEB001",variableType:-4}
 */
var userType = null;


/**
 * @type {Number}
 * @properties={typeid:35,uuid:"3B789671-3051-44E9-95DB-1256656DD57B",variableType:8}
 */
var userTypeForFilter = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"44DB87AC-F436-4F6B-A17E-59AD365930D5",variableType:8}
 */
var selectedApprover = null;
/**
 * @properties={typeid:24,uuid:"A1197740-A75D-4D2A-BE04-B4B80A3F4003"}
 * @AllowToRunInFind
 */
function applyFilter() {
	if (foundset.find()) {

		foundset.events_log_to_calendar_days.calendar_month = selectedMonth;
		foundset.events_log_to_calendar_days.calendar_year = selectedYear;
		foundset.job_order_id = selectedJobOrder;
		foundset.events_log_to_events.event_id = selectedFepCode;
		foundset.events_log_to_job_orders.profit_center_id = selectedProfitCenter;
		foundset.event_log_status = selectedStatus;
		foundset.events_log_to_events.is_requestable = isRequestable;
		foundset.user_id = selectedUser;
		foundset.user_approver_id = selectedApprover;
		//filtro per tipologia di contratto, non viene risettato nel rest del filtro, per personel deve essere sempre filtrato
		if( userType != null){
		foundset.events_log_to_users.user_type_id = userType;
		} else {
			foundset.events_log_to_users.user_type_id = userTypeForFilter;
		}
		
		// Direct approver
		if (isDirectApprover == 1) foundset.user_approver_id = approver;

		// Not direct approver
		if (isDirectApprover == 0) {

			// Is a request
			if (isRequestable == 1) {
				// User's Profit Center Owner
				//foundset.events_log_to_users.users_to_profit_centers.user_owner_id = approver;
				foundset.events_log_to_users.users_to_profit_centers.profit_centers_to_view_user_pc_mgm.user_id = approver;
				foundset.events_log_to_users.users_to_profit_centers.profit_centers_to_view_user_pc_mgm.group_letter = '!S';
				// Not the approver itself
				foundset.user_approver_id = "!=" + approver;
			}

			// Is a log
			if (isRequestable == 0) {
				// Job Order's Profit Center Owner
				//foundset.events_log_to_job_orders.job_orders_to_profit_centers.user_owner_id = approver;
				//foundset.events_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_owners_markets.user_id = approver;
				foundset.events_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_view_user_pc_mgm.user_id = approver;
				foundset.events_log_to_users.users_to_profit_centers.profit_centers_to_view_user_pc_mgm.group_letter = "!S";
				// Not the job order approver itself
				//foundset.events_log_to_job_orders.job_orders_to_approvers.user_approver_id = "!=" + approver;
				foundset.user_approver_id = "!=" + approver;
				foundset.newRecord();
				//foundset.events_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_owners_markets.user_id = "!=" + approver;
				foundset.events_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_view_user_pc_mgm.user_id = "!=" +approver;
				foundset.events_log_to_users.users_to_profit_centers.profit_centers_to_view_user_pc_mgm.group_letter = '!S';
				//foundset.events_log_to_job_orders.job_orders_to_profit_centers.user_owner_id = "!=" + approver;
				foundset.user_approver_id = "!=" + approver;
				foundset.events_log_to_job_orders.user_owner_id = approver;
				foundset.events_log_to_calendar_days.calendar_month = selectedMonth;
				foundset.events_log_to_calendar_days.calendar_year = selectedYear;
				foundset.job_order_id = selectedJobOrder;
				foundset.external_code_fep = selectedFepCode;
				foundset.events_log_to_job_orders.profit_center_id = selectedProfitCenter;
				foundset.event_log_status = selectedStatus;
				foundset.events_log_to_events.is_requestable = isRequestable;
				foundset.user_id = selectedUser;
				//filtro per tipologia di contratto, non viene risettato nel rest del filtro, per personel deve essere sempre filtrato
				if( userType != null){
					foundset.events_log_to_users.user_type_id = userType;
					} else {
						foundset.events_log_to_users.user_type_id = userTypeForFilter;
					}
					
			}
		}
		//NOT approver
		if (isDirectApprover == 2){
			foundset.events_log_to_users.users_to_profit_centers.profit_centers_to_owners_markets.user_id = approver;
			//foundset.events_log_to_users.users_to_profit_centers.user_owner_id = approver;
			// Not the approver itself
			foundset.user_approver_id = "!=" + approver;
			foundset.events_log_to_job_orders.job_orders_to_profit_centers.profit_centers_to_owners_markets.user_id = "!=" + approver;
			//foundset.events_log_to_job_orders.job_orders_to_profit_centers.user_owner_id = "!=" + approver;
		}
		foundset.search();
		foundset.sort('events_log_to_users.users_to_contacts.real_name asc,event_log_date asc,time_start asc');
		globals.tot_duration= globals.formatMinutes(foundset.total_duration);
		globals.tot_duration_regular= globals.formatMinutes(foundset.total_duration_regular);
		globals.tot_duration_overtime= globals.formatMinutes(foundset.total_duration_overtime);
		
		
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"99A40190-993A-467C-B44B-27BA10CEDCC9"}
 */
function resetFilter(event) {
	selectedApprover = null;
	selectedProfitCenter = null;
	selectedUser = null;
	selectedJobOrder = null;
	selectedStatus = 1;
	selectedMonth = new Date().getMonth() + 1;;
	selectedYear = new Date().getFullYear();;
	selectedFepCode = null;
	isDirectApprover = 1;
	applyFilter();
	toggleButton(selectedStatus != null);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"6A32412A-CD60-41DE-B7E2-9575C0148C96"}
 */
function onLoad(event) {
	_super.onLoad(event);
	applyFilter();
}

/**
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"5F11F68E-E92B-4810-9A65-1960AC36542D"}
 */
function onDataChange(oldValue, newValue, event) {
	applyFilter();
	if (isDirectApprover == 2) toggleButton(false)
	else
		toggleButton(selectedStatus != null);
}

/**
 * @param {Boolean} status
 * @properties={typeid:24,uuid:"C1F31208-E5D7-4C58-96EF-61212DB9E26E"}
 */
function toggleButton(status) {
	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
	if(globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators')){
		bundle.elements['split']['getRightForm']().elements['buttonAdd'].visible = false;
		bundle.elements['split']['getRightForm']().elements['buttonDelete'].visible = false;
		bundle.elements['split']['getRightForm']().elements['buttonClose'].visible = false;
		bundle.elements['split']['getRightForm']().elements['buttonApprove'].visible = false;
		bundle.elements['split']['getRightForm']().elements['buttonReject'].visible = false;
		
	} else {
	
		bundle.elements['split']['getRightForm']().elements['buttonApprove'].visible = status;
		bundle.elements['split']['getRightForm']().elements['buttonReject'].visible = status;
		bundle.elements['split']['getRightForm']().elements['buttonClose'].visible = status;
	   }
	}
}
