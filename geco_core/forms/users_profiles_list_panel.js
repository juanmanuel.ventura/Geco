/**
 * @type {String}
 * @properties={typeid:35,uuid:"84E8E97B-3743-4724-B4BD-E3059D659905"}
 */
var sortDirection = 'asc';

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"0C777B17-5BD8-4DE9-8E31-C1EBEE0CD095"}
 */
function doSort(event) {
	if (foundset.getSize() > 1) {
		var label = event.getSource();
		// change color back to the original one
		if (label) label.fgcolor = '#727784';
		// (re) set the triggered label
		label = event.getSource();
		// (re) change color to indicate selection
		label.fgcolor = '#990000';
		var sortKey = label.text;

		switch (sortKey) {
		case 'Data inizio validità':
			controller.sort('profile_user_start_date' + ' ' + sortDirection);
			break;
		case 'Nome Profilo':
			controller.sort('profile_id' + ' ' + sortDirection);
			break;
		}

		// toggle the sort order
		sortDirection = (sortDirection == 'asc') ? 'desc' : 'asc';
	}
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"E297E10D-CA51-40F4-A619-BB5D1CB2A133"}
 */
function updateUI(event) {
	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
		elements.buttonAdd.visible = false;
		elements.buttonDelete.visible = false;
	} else {
		elements.buttonAdd.visible = true;
		elements.buttonAdd.enabled = isEditing();
		elements.buttonDelete.visible = foundset.getSize() > 0;
		elements.buttonDelete.enabled = !isEditing() && foundset.getSize() > 0;
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"35E92E00-EE33-41A8-AFAE-21C4BEC1847C"}
 */
function newRecord(event) {
	_super.newRecord(event);
	foundset.profile_user_start_date = new Date();
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {Number} index
 * @properties={typeid:24,uuid:"0BBA4700-49FB-49A5-BE90-5A7294F49BEC"}
 */
function deleteRecord(event, index) {
	if (foundset && foundset.getSelectedRecord()) {
		var currentID = foundset.getSelectedRecord().profile_user_id;
		var lastID = foundset.getLastID(foundset.getSelectedRecord().user_id);
		//se c'è
		if (lastID != null) {
			//se sono uguali OK
			if (currentID == lastID)
				_super.deleteRecord(event, index);
			else { //altrimenti errore; non lascio cancellare i precedenti
				globals.DIALOGS.showErrorDialog('Error', '- Impossibile eliminare il record selezionato, record troppo vecchio.', 'OK');
				return;
			}
		} //precauzione.. in teoria non dovrebbe verificarsi
		else {
			globals.DIALOGS.showErrorDialog('Error', '- Impossibile eliminare il record selezionato.', 'OK');
			return;
		}
	}
}
