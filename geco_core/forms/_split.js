/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {String} formLeftName
 * @param {String} formRightName
 * @protected
 * @properties={typeid:24,uuid:"FA4E45DD-E431-44DE-AFE2-9544E1F0000A"}
 */
function initialize(event, formLeftName, formRightName) {

	// set navigator
	elements.split.dividerLocation = 48;
	elements.split.dividerSize = 0;

	// load the filter
	if (formLeftName && forms[formLeftName]) {
		elements.split.setLeftForm(forms[formLeftName]);
	}

	// load the filter
	if (formRightName && forms[formRightName]) {
		elements.split.setRightForm(forms[formRightName]);
	}
}
