/**
 * @type {Number}
 * @properties={typeid:35,uuid:"38F0B6E5-1D01-4901-AADC-8959390B20A0",variableType:8}
 */
var totalDuration = 0;

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"5C98ECD8-4FCD-4FC7-A325-538E48905B40"}
 */
function updateUI(event) {
	//_super.updateUI(event);
	
	// buttons
	//elements.buttonDelete.visible = false;
	elements.buttonAdd.visible = isEditing();
	getTotalDuration(event);
	return true;
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"90F41DFC-4665-4359-9E9F-E45C3BDD1BAF"}
 */
function getTotalDuration(event) {
	/** @type {Number} */
	var sum = 0;
//	/** @type {JSFoundSet<db:/geco/working>} */
//	var usersDoc = databaseManager.getFoundSet('geco', 'users');
		if(foundset){
			for(var index = 1; index <= foundset.getSize(); index++){
				var rec = foundset.getRecord(index);
				sum = sum + rec.duration_work;		
			}
		}
		totalDuration = sum;
}


