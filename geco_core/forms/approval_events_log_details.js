/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"A5098A71-A76F-4EFE-BC33-BFBB22BD5F95"}
 */
function onDataChangeEvent(event) {
	// enable job order selector
	if (foundset.events_log_to_events.has_job_order == 0) {
		elements.job.enabled = false;
		foundset.job_order_id = null;
	}
	
	if (foundset.events_log_to_events.has_job_order == 1) {
		elements.job.enabled = isEditing();
	}

	// enable time slots selectors
	if (foundset.events_log_to_events.unit_measure == 1) {
		foundset.time_start = null;
		foundset.time_stop = null;
		foundset.time_start_display = null;
		foundset.time_stop_display = null;
		foundset.duration = null;
		foundset.duration_display = null;
		globals.selectedStartTime = foundset.time_start;
	}
	return true
}

/**
 * @param oldValue
 * @param newValue
 * @param event
 *
 * @properties={typeid:24,uuid:"B6F1BC57-C179-4F76-9675-20D13541C7A3"}
 */
function onDataChangeStartTime(oldValue, newValue, event) {
	if (oldValue != newValue) {
		globals.selectedStartTime = foundset.time_start;
		return true;
	}
	return false;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"381A4187-7940-473B-A6F1-E6C0BECE3A8B"}
 */
function tempOnAction(event) {
	if (foundset.getSelectedRecord() && foundset.getSelectedRecord().event_id == 27 || foundset.getSelectedRecord().event_id == 28) {
		globals.DIALOGS.showErrorDialog('Error', 'Non è possibile modificare gli straordinari.\nCancellare e inserire nuovamente.', 'OK');
	} else if (foundset.getSelectedRecord() && foundset.getSelectedRecord().event_id == 25){
		globals.DIALOGS.showErrorDialog('Error', 'Non è possibile modificare l\'evento riposo compensativo straordinario.\nCancellare e inserire nuovamente.', 'OK');
	} else {
		startEditing(event);
		onDataChangeEvent(event);
	}
}

/**
 * Implementation of the updateUI Hook
 * @param {JSEvent} event The event that triggered the action.
 *
 * @properties={typeid:24,uuid:"5412E6A0-CB95-4FD5-9C1E-8C645DA5E07A"}
 */
function updateUI(event) {

	// call parent first
	_super.updateUI(event);
	
	if(globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')){
		elements.buttonCopy.visible = false;
		elements.buttonEdit.visible = false;
	} else {

	var hasJobOrder = false;

	
	if (foundset.events_log_to_events) hasJobOrder = (foundset.events_log_to_events.has_job_order == 1);

	// fields
	elements.eventId.enabled = isEditing();
	elements.job.enabled = isEditing() && hasJobOrder;
	elements.buttonCopy.visible = !isEditing();
	}
}


/**
 * Opens a pop up to select date
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"FD5DC14F-97BD-4A1A-9A1C-5E802F76E716"}
 */
function openPopupCopy(event) {
	
	var record = foundset.getSelectedRecord();

	// skip if no record is selected
	if (!record) {
		globals.DIALOGS.showWarningDialog('Attenzione','Selezionare un record','OK')
		return;
	}
	var win = application.createWindow("copia", JSWindow.MODAL_DIALOG);
	win.title = "Copia evento";
	win.show(forms.events_duplicator);
}


/**
 * @param {Date} start
 * @param {Date} end
 * @param {Number} skip
 * @properties={typeid:24,uuid:"23DED6CA-F5C2-497C-9B72-52A1DD2ECE8D"}
 * @AllowToRunInFind
 */

function duplicateEventLog(start, end, skip) {

	// get the original record
	var record = foundset.getSelectedRecord();

	// skip if no record is selected
	if (!record) {
		application.output("No record selected");
		return;
	}

	// sanitize dates
	start.setHours(0, 0, 0);
	end.setHours(0, 0, 0);
	//var indexToDuplicate = foundset.getRecordIndex(record);
	var indexToDuplicate = foundset.getRecordIndex(record);
	/** @type {JSRecord<db:/geco/events_log>} */
	var originalRecord = foundset.getRecord(indexToDuplicate);
	//var originalRecord = forms.calendar_days_list.foundset.calendar_days_to_events_log.getRecord(indexToDuplicate);

	for (var date = start; start <= end; date.setDate(date.getDate() + 1)) {

		// skip weekends
		if (skip == 1 && globals.isWeekEnd(date)) {
			continue;
		}
		//prendo il record originario
		indexToDuplicate = foundset.getRecordIndex(originalRecord);
		//indexToDuplicate = forms.calendar_days_list.foundset.calendar_days_to_events_log.getRecordIndex(originalRecord);
		databaseManager.setAutoSave(false);
		indexToDuplicate = foundset.duplicateRecord(indexToDuplicate, true, true);
		//indexToDuplicate = forms.calendar_days_list.foundset.calendar_days_to_events_log.duplicateRecord(indexToDuplicate, true, true);
		var newrecord = foundset.getRecord(indexToDuplicate);
		//var newrecord = forms.calendar_days_list.foundset.calendar_days_to_events_log.getRecord(indexToDuplicate);
		newrecord.event_log_date = date;
		newrecord.event_log_status = 1;
		newrecord.actual_approver_id = null;
		if (!databaseManager.setAutoSave(true)) {
			application.output(databaseManager.getFailedRecords(foundset));
			databaseManager.revertEditedRecords();
			databaseManager.setAutoSave(true);
			if (globals.validationExceptionMessage) {
				globals.DIALOGS.showErrorDialog('Error', globals.validationExceptionMessage, 'OK');
			}
		}
	}

	// force loading the new records
	updateUI(new JSEvent);
	//forms.calendar_days_list.updateUI(new JSEvent);
	application.updateUI();
	//forms.calendar_days_filter.filterByMonth();
}
