/**
 * @type {Boolean}
 * @properties={typeid:35,uuid:"5BFDD1B0-3B12-40ED-943D-2E84918B51A4",variableType:-4}
 */
var added = false;

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"69F093D9-CB82-45C0-91B8-29951A2F00EC"}
 * @AllowToRunInFind
 */
function saveEdits(event) {
	application.output(globals.messageLog + 'START profit_centers_details.saveEdits() ', LOGGINGLEVEL.INFO);
	var changeRPC = false;
	var record = foundset.getSelectedRecord();
	if (added == false && !record.isNew()) {
		//se ho cambiato il campo RCP

		application.output('record ' + record);
		var dataset = record.getChangedData();
		application.output('dataset ' + dataset);
		for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
			//  se nel dataset il campo precedente user_owner_id è stato cambiato
			if (dataset.getValue(i, 1) == 'user_owner_id' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
				application.output(globals.messageLog + ' profit_centers_details.saveEdits() user_owner_id cambiato', LOGGINGLEVEL.INFO);
				changeRPC = true;
				break;
			}
		}
		var answer = 'No'
		if (changeRPC)
			answer = globals.DIALOGS.showWarningDialog("Conferma", "Vuoi aggiungere come nuovo approvatore?", "Si", "No");
		if (answer == 'Si') {
			application.output(globals.messageLog + ' profit_centers_details.saveEdits() user_owner_id aggiunto come approvatore ', LOGGINGLEVEL.INFO);
			var dateFrom = new Date();
			var day = dateFrom.getDate();
			var month = dateFrom.getMonth();
			var year = dateFrom.getFullYear();
			//			var date = new Date();
			//			var month = '' + (date.getMonth() + 1);
			//			var day = '' + (date.getDate());
			//			var dateStr = (day.length > 1? day : '0' + day) + '-' + (month.length > 1 ? month : '0' + month) + '-' + (date.getFullYear());
			foundset.profit_centers_to_approvers.newRecord(false);
			foundset.profit_centers_to_approvers.user_approver_id = foundset.user_owner_id;
			foundset.profit_centers_to_approvers.user_approver_from = new Date(year, month, day);
			added = true;
			return;
		}
	}

	//delete 'Resp. Mercato'
	for (var c = 1; c <= record.profit_centers_to_owners_markets.getSize(); c++) {
		var ownerPC = record.profit_centers_to_owners_markets.getRecord(c);
		if (record.user_owner_id == ownerPC.user_id && ownerPC.is_selected == 1) {
			//lo trovo e lo deseleziono
			ownerPC.is_selected = 0;
			globals.DIALOGS.showErrorDialog('Error', 'Non è possibile cancellare l\'utente '+ ownerPC.owners_markets_to_users.user_real_name + '.', 'OK');
			break;
		}
	}
	application.output(record.profit_centers_to_owners_markets);
	forms.profit_centers_owners_list.deleteRecordsSelected(event);

	//delete approver
	var fd = foundset.profit_centers_to_approvers;
	try {
		if (fd) {
			for (var index = 1; index <= fd.getSize(); index++) {
				var rec = fd.getRecord(index);
				if (rec.is_selected) {
					application.output(globals.messageLog + 'START profit_centers_details.saveEdits() cancellazione approvatore centro profitto ' + rec.approvers_to_users.users_to_contacts.real_name, LOGGINGLEVEL.INFO);
					var totEventsLog = 0;
					//controllo che approver non abbia eventi da approvare per il profit center
					/** @type {JSFoundSet<db:/geco/events_log>} */
					var events_log = databaseManager.getFoundSet('geco', 'events_log');
					if (events_log.find()) {
						// se profit center ---> tipologia di approvazione dell'evento = responsabile del profit center
						if (rec.entity == 'profit_centers') {
							events_log.events_log_to_events.approver_figure = 2;
							//events_log.events_log_to_users.users_to_profit_centers.user_owner_id = rec.user_approver_id;
							events_log.events_log_to_users.users_to_profit_centers.profit_center_id = rec.entity_id;
						}
						events_log.user_approver_id = rec.user_approver_id;
						events_log.event_log_status = '1||3';
						totEventsLog = events_log.search();
					}

					if (totEventsLog > 0) {
						application.output(globals.messageLog + ' profit_centers_details.saveEdits() Non è possibile cancellare approvatore ci sono eventi in sospeso', LOGGINGLEVEL.INFO);
						application.output(globals.messageLog + 'STOP profit_centers_details.saveEdits() cancellazione approvatore centro profitto KO ' + rec.approvers_to_users.users_to_contacts.real_name, LOGGINGLEVEL.INFO);
						globals.DIALOGS.showErrorDialog('Error', 'Non è stato cancellato l\'approvatore, \nha consuntivazioni in sospeso per questo centro profitto', 'OK');
						return;
					} else {
						try {
							fd.deleteRecord(index);
							application.output(globals.messageLog + 'STOP profit_centers_details.saveEdits() cancellazione approvatore centro profitto OK ' + rec.approvers_to_users.users_to_contacts.real_name, LOGGINGLEVEL.INFO);
						} catch (e) {
							application.output(e.message);
							globals.DIALOGS.showErrorDialog('Error', globals.validationExceptionMessage, 'OK');
							updateUI(event);
							// Failed to save data
							return;
						}
					}
				}
			}
		}

		_super.saveEdits(event);
		added = false;
		application.output(globals.messageLog + 'STOP profit_centers_details.saveEdits() ', LOGGINGLEVEL.INFO);
	} catch (e) {
		application.output(e);

	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"54B12344-9B18-47AB-96CA-2EA9160C3C64"}
 */
function updateUI(event) {
	_super.updateUI(event);

	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
		elements.buttonEdit.visible = false;
	}

	if (!isEditing()) foundset.sort('profit_center_name asc');

	var bundle = forms[controller.getFormContext().getValue(2, 2)];
	if (bundle) {
		bundle.elements['split'].visible = !isEditing();
		bundle['focus'] = foundset.profit_center_name;
		bundle.elements['focus'].visible = isEditing();
	}
}
