extendsID:"4B14C36B-EFD3-4634-BB63-330D8329AF0F",
items:[
{
enabled:false,
extendsID:"D2DC6F09-3211-46F8-BB43-D3D79D329349",
typeid:7,
uuid:"0D7ED7EE-EBC3-49A5-ADD7-DCF31DC6CAF2"
},
{
extendsID:"584852A3-48F1-4566-90D3-FF43FE280208",
height:250,
typeid:19,
uuid:"54653BE7-38A5-4643-AB49-47CDFD99B690"
},
{
customProperties:"methods:{
onActionMethodID:{
arguments:[
null,
\"'expenses_log'\"
]
}
}",
extendsID:"7D3B8205-CC20-43C3-989D-1A127CF8CFD6",
location:"423,218",
onActionMethodID:"795B25D5-B209-4EA1-82C2-BF079B485A67",
typeid:7,
uuid:"662958E2-EAC0-42E5-BFEB-9E2607AE2ED9"
},
{
extendsID:"E7451E69-4EF0-4E48-8702-0E01DD3AABF7",
location:"0,202",
size:"500,22",
typeid:7,
uuid:"6F94C717-8C65-4A77-81B1-2BC15829C043"
},
{
extendsID:"A6A9C3D4-C72F-4B01-AE06-119D630A5887",
height:202,
typeid:19,
uuid:"AD0FD2E9-6C71-423A-A8BE-0EB8FFFF6C49"
},
{
formIndex:33,
horizontalAlignment:0,
location:"25,88",
size:"470,28",
text:"Selezionare il mese di consuntivazione",
transparent:true,
typeid:7,
uuid:"B55F0889-9D78-4C85-9C64-36CA8E785DE6"
},
{
extendsID:"B649AC0D-00D5-4039-9F29-473D6E55F601",
location:"464,218",
typeid:7,
uuid:"B592BB7E-0BDE-4775-BBC8-F8727EFE1290"
},
{
formIndex:34,
location:"310,144",
size:"42,20",
text:"Anno",
transparent:true,
typeid:7,
uuid:"D5DEA511-8FFD-4C24-8331-D17C5325D954"
},
{
formIndex:34,
location:"31,146",
size:"42,20",
text:"Mese",
transparent:true,
typeid:7,
uuid:"DEA97BBE-3A09-4937-B8F8-703BA91ABA94"
},
{
extendsID:"73E3E719-8486-4BF5-9605-6F9B895FB4FB",
location:"30,216",
typeid:4,
uuid:"E220FF31-74D2-42A1-9101-C7D1F9D8F3D0"
},
{
dataProviderID:"month",
displayType:2,
editable:false,
formIndex:33,
location:"87,146",
size:"115,20",
typeid:4,
uuid:"E3FA8CE2-A24B-42C3-BFFF-D68D8A802C00",
valuelistID:"3656A91E-9B4A-4E32-956B-C29D30CAFEBC"
},
{
dataProviderID:"year",
displayType:2,
editable:false,
formIndex:33,
format:"####",
location:"362,146",
size:"115,20",
typeid:4,
uuid:"ED5E5A2C-1CD4-4F3A-AAC5-C2334B1E9ABB",
valuelistID:"075E9257-460A-403C-B979-C6A3C3402907"
}
],
name:"expenses_duplicator",
styleName:"GeCo",
typeid:3,
uuid:"7CA752FC-A0A0-478E-99D4-31D4CC5B15DF"