/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"8BC095D2-6EFF-4E80-863C-9F066C2B2053"}
 */
function updateUI(event) {
	elements.buttonAdd.visible = isEditing();
	elements.buttonDelete.visible = isEditing();
}
