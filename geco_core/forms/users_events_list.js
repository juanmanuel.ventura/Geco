/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"5EE75264-9CED-458E-B68F-8F26BE85A66F"}
 */
function openPopupSelect(event) {
	var win = application.createWindow("selezione", JSWindow.MODAL_DIALOG);
	globals.callerForm = forms[event.getFormName()];
	win.setInitialBounds(1000, 1000, 500, 500);
	win.show(forms.users_events_list_popup);
}
