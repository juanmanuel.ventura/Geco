/**
 * @type {Number}
 * @properties={typeid:35,uuid:"820AE829-3CCC-42EC-86B8-FBED46A47E09",variableType:4}
 */
var selectedCompany = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"46BB9212-4EA4-4DF7-AD32-AFEC72BADD87",variableType:8}
 */
var selectedJobOrder = null;

/**
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"51753098-B2CD-4314-A307-0F0765A3CDAA"}
 */
function applyFilter(event) {
	areAllSelected = 0;
	selectAllRecords(event);
	searchValidRecord();
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"49E43F03-C4D3-47DE-B08D-4CF621E00832"}
 */
function resetFilter(event) {
	selectedCompany = null;
	selectedJobOrder = null;
	searchValidRecord();
	areAllSelected = 0;
	selectAllRecords(event);
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"A28C8D8C-97AA-4408-AA4B-ED794089DF93"}
 */
function processSelection(event) {
	for (var index = 1; index <= foundset.getSize(); index++) {
		var rec = foundset.getRecord(index);
		if (rec.is_selected) {
			globals.callerForm.foundset.newRecord(false)
			globals.callerForm.foundset['user_id'] = globals.selectedUser;
			globals.callerForm.foundset['job_order_id'] = rec.job_order_id;
		}
	}
	resetFilter(event);
	_super.processSelection(event);

}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"83C07355-BB26-4788-B082-09A5BAE526BF"}
 */
function clearAndClose(event) {
	resetFilter(event);
	_super.clearAndClose(event)
}

/**
 * @properties={typeid:24,uuid:"A2283792-8C14-4C69-A3C7-E32662CA2FA5"}
 * @AllowToRunInFind
 */
function searchValidRecord() {

	if (foundset.find()) {
		foundset.company_id = selectedCompany;
		foundset.job_order_id = selectedJobOrder;
		foundset.is_enabled = "1";
		foundset.search();
	}

}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"37A13773-DC7C-4F5A-A889-DA4E37CC8730"}
 */
function onShow(firstShow, event) {
	
	foundset.addFoundSetFilterParam('job_order_id', 'not in', 'select job_order_id from users_job_orders where user_id = "' + globals.selectedUser + '"', 'filterJobOrdersForUser');

	searchValidRecord();
	return _super.onShow(firstShow, event)
}
