/**
 * @type {Number}
 * @properties={typeid:35,uuid:"F39F3A9A-091C-4BDD-B112-16075F75EC04",variableType:8}
 */
var userOwner = null;

/**
 * @type {Number}
 * @properties={typeid:35,uuid:"D7FF9042-24D1-426B-AE6F-AC5A69F8EBBC",variableType:8}
 */
var profitCenterType = null;
/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"1108093D-DAC7-4192-A728-99E35FC0C8C2"}
 */
function onDataChange(oldValue, newValue, event) {
	if (newValue |= oldValue) {
		applyFilter(event);
	}
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * @param {Boolean} goTop
 *
 * @private
 *
 * @properties={typeid:24,uuid:"2B60BEE2-480B-48B5-A7BB-E887FB301E72"}
 */
function resetFilter(event, goTop) {
	userOwner = null;
	profitCenterType = null;
	return _super.resetFilter(event, goTop)
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"BA12E797-FCF9-4B7A-8F14-8FAE7E4B78AE"}
 * @AllowToRunInFind
 */
function applyFilter(event) {
	filterEnabledRecords();
	if (foundset.find()) {
		foundset.user_owner_id = userOwner;
		foundset.profit_center_type_id = profitCenterType;
		foundset.search();
		foundset.sort('profit_center_name asc')
	}
}
