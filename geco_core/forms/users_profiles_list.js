/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"2347DE03-D975-4C77-B86C-8BA98EAEDE73"}
 */
function updateUI(event) {
	if (globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')) {
		elements.buttonAdd.visible = false;
	}
}