/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 *
 * @properties={typeid:24,uuid:"17221808-6146-4732-B336-26B354DE7248"}
 */
function onAction(event) {
//	controller.setPageFormat(297.0,201.0,5,5,5,5,SM_ORIENTATION.LANDSCAPE);
	controller.print(false, true, plugins.pdf_output.getPDFPrinter());
//	controller.print(false, false, plugins.pdf_output.getPDFPrinter());
}
