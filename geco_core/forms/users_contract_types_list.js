/**
 * Adds a new record. It opens an in-memory transaction.
 * New records cannot be added if an invalid state exists outside of a transaction
 *
 * @param {JSEvent} event the event that triggered the action
 * @returns {Number} The index of the record that was added
 *
 * @properties={typeid:24,uuid:"503E0D87-312E-4B27-9485-5B0B11FFD671"}
 */
function newRecord(event) {
	// start in-memory transaction
	if (startEditing(event)) {
		// create a new record
		return foundset.newRecord(true);
	}
	return null;
}

/**
 *
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"7BB41D3D-540E-4173-A555-C29E65FED022"}
 */
function stopEditing(event) {
	_super.stopEditing(event);
	updateUI(event);
	onRecordSelection(event);
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"8BEE22C5-6290-462D-BFEA-D59C0C09C191"}
 */
function updateUI(event) {
	_super.updateUI(event);
	// buttons
	//elements.buttonDelete.visible = false;
	
	elements.buttonEdit.visible = !isEditing();
	elements.buttonAdd.enabled = !isEditing();
	elements.buttonSave.visible = isEditing();
	elements.buttonCancel.visible = isEditing();
	
	if(globals.hasRole('Admin Readers') && !globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses')){
		elements.buttonEdit.visible = false;
		elements.buttonAdd.visible = false;
	}
	
	if(!isEditing()){
		foundset.sort('contract_type_id asc');
	}
	forms.users_working_hours_week.updateUI(event);

	return true;
}

/**
 * Clear selection, revert db changes and close popup
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"3261060E-00F8-4712-9D10-677813DE4084"}
 */
function rollbackAndClose(event) {
	databaseManager.revertEditedRecords();
	databaseManager.setAutoSave(true);
	controller.getWindow().destroy();
}

/**
 * Save edits and close popup
 * @param {JSEvent} event the event that triggered the action
 * @protected
 * @properties={typeid:24,uuid:"6A320400-4927-4C12-BA95-83E70F42D917"}
 */
function saveEdits(event) {

	if (foundset.getSelectedRecord().is_weekly == 1 && foundset.contract_types_to_working_hours_week.getSize() == 0) {
		globals.DIALOGS.showInfoDialog('Attenzione', 'Scegliere almeno un giorno per il settimanale', 'OK');
		return;
	}
	var changeWH = false;
	var changeCT = false;
	// se esistono utenti con la tipologia di contratto selezionata
	if (foundset.contract_types_to_users.getSize() > 0) {
		var recordCT = foundset.getSelectedRecord();
		//se esiste un record selezionato nella form settimanale
		if (foundset.contract_types_to_working_hours_week.getSelectedRecord()) {
			var recordWH = foundset.contract_types_to_working_hours_week.getSelectedRecord();
			var dataset = recordWH.getChangedData();
			for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
				//se viene modificato qualche valore nella form working_hours_week
				if (dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
					changeWH = true;
					break;
				}
			}
		}
		var dataset1 = recordCT.getChangedData();

		for (var index = 1; index <= dataset1.getMaxRowIndex(); index++) {
			//se viene modificato o il valore Settimanale o Ore lavorative o Abilitato nella form contract_types
			if ( (dataset1.getValue(index, 1) == 'is_weekly' || dataset1.getValue(index, 1) == 'daily_workable_hours') && dataset1.getValue(index, 2) != dataset1.getValue(index, 3)) {
				changeCT = true;
				break;
			}
		}
		if (changeWH || changeCT) {
			globals.DIALOGS.showInfoDialog('Attenzione', 'Impossibile modificare - ci sono utenti assegnati a questa tipologia', 'OK');
			return;
		}
	}
	_super.saveEdits(event);

}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"55681FC8-C0B7-4F0E-AC7F-92FD1009197B"}
 */
function onRecordSelection(event) {
	if (foundset && foundset.getSize() > 0) {
		elements.tabless.visible = foundset.getSelectedRecord().is_weekly == 1 ? true : false;
	}
	return _super.onRecordSelection(event);
}

/**
 * Handle changed data.
 *
 * @param {Number} oldValue old value
 * @param {Number} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected
 *
 * @properties={typeid:24,uuid:"18AAAC90-B475-413C-A707-6F4C2810B529"}
 */
function onDataChange(oldValue, newValue, event) {
	var isWeek = oldValue;
	if ( (isWeek == 0 || isWeek == null) && newValue == 1) {
		elements.tabless.visible = true;
	} else if (isWeek == 1 && newValue == 0) {
		elements.tabless.visible = false;
	}
	return true
}


/**
 * Handle hide window.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"AA338AE1-EE6C-47A6-9F5E-0547F7202149"}
 */
function onHide(event) {
	stopEditing(event);
	return true
}
