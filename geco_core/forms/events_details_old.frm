dataSource:"db:/geco/events",
extendsID:"7D147E9E-8F99-4CAE-8E3B-6C45741464B5",
items:[
{
location:"406,93",
size:"112,20",
text:"Codice FEP",
transparent:true,
typeid:7,
uuid:"02EBA0D9-3894-44DB-9B14-C91BEAE89F14"
},
{
location:"30,56",
size:"111,20",
text:"Tipo",
transparent:true,
typeid:7,
uuid:"03E3671D-2483-4B70-AB16-04D2EA1BCAB9"
},
{
anchors:13,
borderType:"LineBorder,1,#c0c0c0",
items:[
{
containsFormID:"203601D6-A2F7-4D31-8F18-166773B569DB",
location:"79,334",
name:"events_users_list",
relationName:"events_to_users_events",
text:"events_users_list",
typeid:15,
uuid:"3BA4F1FC-DAA4-4F6E-836B-0C62BB219A45"
}
],
location:"30,261",
name:"tabUserEvent",
printable:false,
size:"346,195",
tabOrientation:-1,
transparent:true,
typeid:16,
uuid:"0B2827A9-0480-4173-AB34-0BBC24A5788B"
},
{
dataProviderID:"has_job_order",
displayType:4,
formIndex:1,
horizontalAlignment:2,
location:"532,161",
name:"hasJobOrder",
onDataChangeMethodID:"A0E92F00-53A5-4CA4-9C71-DF350718D5AC",
size:"27,20",
transparent:true,
typeid:4,
uuid:"0B66AF34-1C2C-4A93-901D-3CD4D76F47A0"
},
{
location:"30,19",
size:"111,20",
text:"Nome",
transparent:true,
typeid:7,
uuid:"1E58A37D-26EC-484A-845D-E8020D632967"
},
{
formIndex:2,
location:"406,130",
size:"112,20",
text:"Richiede",
transparent:true,
typeid:7,
uuid:"25FC7604-F606-4506-87D5-1BB488849269"
},
{
anchors:11,
location:"559,191",
size:"211,20",
text:"Tipo Straordinario",
transparent:true,
typeid:7,
uuid:"26094053-6B5A-4908-B6BD-0D02FA9997C7"
},
{
dataProviderID:"event_type_id",
displayType:2,
editable:false,
location:"151,56",
name:"Type",
size:"225,20",
text:"PC Type ID",
typeid:4,
uuid:"3A9319C4-CCE2-46D8-8595-E5E978952B97",
valuelistID:"E648EAFC-6173-466A-B643-FEE951F57F40"
},
{
extendsID:"7D3B8205-CC20-43C3-989D-1A127CF8CFD6",
onActionMethodID:"4DB66F4D-0494-4972-8BA2-57D06D9BC73F",
typeid:7,
uuid:"3D5D3CD4-FD41-4673-BD72-24BF390A0829"
},
{
anchors:11,
dataProviderID:"external_code_navision",
location:"532,56",
name:"fldExternal_code",
size:"238,20",
typeid:4,
uuid:"43EC3B2F-AD41-4D9D-BC9F-9D49108BF064"
},
{
dataProviderID:"unit_measure",
displayType:2,
editable:false,
location:"151,93",
name:"unit_measure",
size:"225,20",
typeid:4,
uuid:"4C693FF6-6DB8-4AFD-92C3-D7612ED913A0",
valuelistID:"AA305A7E-720C-4499-B0A9-6EE279659F2E"
},
{
dataProviderID:"is_overtime",
displayType:4,
formIndex:1,
horizontalAlignment:2,
location:"532,191",
name:"hasJobOrdercc",
onDataChangeMethodID:"-1",
size:"27,20",
transparent:true,
typeid:4,
uuid:"4EB10604-B073-47C3-9AAB-DF1DF585150C"
},
{
anchors:15,
borderType:"LineBorder,1,#cacaca",
formIndex:4,
lineSize:1,
location:"406,261",
shapeType:1,
size:"367,195",
transparent:true,
typeid:21,
uuid:"58B35D83-8F8B-4D0C-ABC1-1E949CE5314F"
},
{
anchors:11,
formIndex:2,
location:"559,131",
size:"211,20",
text:"Approvazione a preventivo",
transparent:true,
typeid:7,
uuid:"5DFECE2D-1782-45EE-8477-CA1445C98602"
},
{
location:"406,232",
size:"367,28",
styleClass:"bold",
text:"Approvatori",
transparent:true,
typeid:7,
uuid:"60BC06B5-D9B8-4597-AE16-4193E11815C8"
},
{
formIndex:1,
groupID:"group_profit_center",
location:"30,168",
size:"111,20",
text:"Centro Profitto",
transparent:true,
typeid:7,
uuid:"63CA0059-8C62-4CBB-AE6A-E48B7ED7C4B1",
visible:false
},
{
location:"30,232",
size:"346,28",
styleClass:"bold",
text:"Utenti abilitati alla consuntivazione",
transparent:true,
typeid:7,
uuid:"71D17FB6-2FC7-46FF-9D8A-B0BD1683767E"
},
{
anchors:11,
dataProviderID:"event_name",
location:"151,19",
name:"Name",
size:"619,20",
text:"Title",
typeid:4,
uuid:"731DAEA7-7775-4757-A3DF-1091BF4A829E"
},
{
anchors:15,
formIndex:5,
horizontalAlignment:0,
location:"406,334",
size:"367,27",
text:"Da implementare [approvers] in read-only",
transparent:true,
typeid:7,
uuid:"781E89A7-AABB-4C55-A840-A7B5D6674FE7"
},
{
location:"30,130",
size:"111,20",
text:"Approva",
transparent:true,
typeid:7,
uuid:"801EB235-B685-4E72-8169-964B76871CBD"
},
{
location:"406,56",
size:"112,20",
text:"Codice Navision",
transparent:true,
typeid:7,
uuid:"8124262E-03FC-485C-A019-1295DA6D29A9"
},
{
dataProviderID:"is_enabled",
displayType:4,
formIndex:1,
location:"15,502",
name:"hasJobOrderc",
onDataChangeMethodID:"A0E92F00-53A5-4CA4-9C71-DF350718D5AC",
size:"69,20",
text:"Attivo",
transparent:true,
typeid:4,
uuid:"A57EBB61-6587-41C1-9CFB-B4AA8B09D994"
},
{
location:"30,93",
size:"111,20",
text:"Conteggia",
transparent:true,
typeid:7,
uuid:"AC2079E6-46B4-44AD-A156-24B1F7FA2F39"
},
{
dataProviderID:"approver_figure",
displayType:2,
editable:false,
location:"151,130",
onDataChangeMethodID:"382A838D-FFF7-4949-A952-BCDAD45B505C",
size:"225,20",
typeid:4,
uuid:"BFD4AA15-08F9-41E4-BE54-C5871DBF17FF",
valuelistID:"A462FFBB-8250-417C-B6BE-A7A965AB0ECD"
},
{
dataProviderID:"is_requestable",
displayType:4,
formIndex:3,
horizontalAlignment:2,
location:"532,131",
name:"isRequestable",
onDataChangeMethodID:"-1",
size:"27,20",
transparent:true,
typeid:4,
uuid:"C1D0CF1B-7F45-40CF-B64D-141950E75ED2"
},
{
anchors:11,
location:"559,161",
size:"211,20",
text:"Commessa operativa",
transparent:true,
typeid:7,
uuid:"EBCAA82C-40BE-4463-880E-F86CEAAF06D2"
},
{
dataProviderID:"approver_profit_center_id",
displayType:2,
editable:false,
groupID:"group_profit_center",
location:"151,168",
size:"225,20",
typeid:4,
uuid:"F27A77BA-B382-4C11-B731-622517E77125",
valuelistID:"73312BF3-989F-48F4-85E1-AB96BDEA9A3F",
visible:false
},
{
anchors:11,
dataProviderID:"external_code_fep",
location:"532,93",
name:"fepCode",
size:"238,20",
typeid:4,
uuid:"F68A6AE4-A1EF-4AF2-ABAD-18ABF8807C0D"
}
],
name:"events_details_old",
navigatorID:"-1",
onLoadMethodID:"-1",
onNewRecordCmdMethodID:"0",
onRecordEditStartMethodID:"-1",
onRecordEditStopMethodID:"5CAA4819-E609-41BB-9AF5-1EC523FC19DB",
showInMenu:true,
size:"800,576",
styleName:"GeCo",
typeid:3,
uuid:"48C4161A-86CF-41F4-8B61-E95BB2BC1CAE",
view:0