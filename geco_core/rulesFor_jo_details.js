/**
 * @param {JSRecord<db:/geco/jo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"9EADCF45-FED1-4EC2-A2EA-826C52E47A47"}
 */
function validateProfitCostType(record) {
	if (globals.isEmpty(record.profit_cost_type_id)) {
		throw new Error('- Il campo tipologia è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/jo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"20BF8620-7FAE-4FE4-A6D7-B71279D3C982"}
 * @AllowToRunInFind
 */
function validateFigure(record) {
	//personale
	if (record.profit_cost_type_id == 5) {
		if (globals.isEmpty(record.standard_figure) && globals.isEmpty(record.real_figure)) {
			throw new Error('- Il campo persona è obbligatorio per la tipologia scelta, indicare la figura standard o la figura reale');
		}
	}
	//TM
	else if (record.profit_cost_type_id == 9) {
		if (globals.isEmpty(record.standard_figure) && globals.isEmpty(record.real_tm_figure)) {
			throw new Error('- Il campo persona è obbligatorio per la tipologia scelta, indicare la figura standard o la figura TM');
		}
	}
	//TK
	else if (record.profit_cost_type_id == 8) {
		if (globals.isEmpty(record.real_tk_supplier)) {
			throw new Error('- Il campo fornitore è obbligatorio per la tipologia scelta');
		}
	}
	//spese
	else if (record.profit_cost_type_id == 10) {
		if (globals.isEmpty(record.standard_figure) && globals.isEmpty(record.real_figure)) {
			throw new Error('- Il campo persona è obbligatorio per la tipologia scelta, indicare la figura professionale o la figura reale');
		}
	}

	if (record.jo_details_to_profit_cost_types.profit_cost_acr == 'C' && record.description != null && [5, 9, 10].indexOf(record.profit_cost_type_id) > -1) {
		application.output('conrollo duplicati')
		/** @type {JSFoundSet<db:/geco/jo_details>} */
		var jo_detailsC = databaseManager.getFoundSet('geco', 'jo_details');
		var totSameTypeC = 0;
		if (jo_detailsC.find()) {
			jo_detailsC.jo_id = record.jo_id;
			jo_detailsC.jo_details_id = '!' + record.jo_details_id;
			jo_detailsC.profit_cost_type_id = record.profit_cost_type_id;
			jo_detailsC.real_figure = record.real_figure;
			jo_detailsC.real_tm_figure = record.real_tm_figure;
			jo_detailsC.real_tk_supplier = record.real_tk_supplier;
			//jo_detailsC.days_import = record.days_import;
			jo_detailsC.description = record.description;
			jo_detailsC.enrollment_number = record.enrollment_number;
			totSameTypeC = jo_detailsC.search();
		}
		if (totSameTypeC > 0) {
			//controllo aggiunto per permettere più inserimenti di dettagli "Personale" a figura generica
			if (!(record.profit_cost_type_id == 5 && record.user_id == null && record.standard_figure != null && record.real_figure == null))
				throw new Error('- Esiste già un costo con la stessa tipologia e la stessa descrizione');
		}
	}
	//ricavo
	else if (record.jo_details_to_profit_cost_types.profit_cost_acr == 'R' && record.description != null) {
		/** @type {JSFoundSet<db:/geco/jo_details>} */
		var jo_details = databaseManager.getFoundSet('geco', 'jo_details');
		var totSameType = 0;
		if (jo_details.find()) {
			jo_details.jo_id = record.jo_id;
			jo_details.jo_details_id = '!' + record.jo_details_id;
			jo_details.profit_cost_type_id = record.profit_cost_type_id;
			jo_details.description = record.description;
			totSameType = jo_details.search();
		}
		if (totSameType > 0) {
			throw new Error('- Esiste già un ricavo con la stessa tipologia e la stessa descrizione, modifcare la descrizione');
		}
	}
}

/**
 * @param {JSRecord<db:/geco/jo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"E8DA38AA-14B9-4B5B-90A1-74D62508891D"}
 */

function validateDaysNumber(record) {
	application.output(record)
	//valida la presenza campo giorni se è stato scelta la tipologia figura professionale
	if ( (record.profit_cost_type_id == 5 || record.profit_cost_type_id == 9) && globals.isEmpty(record.days_number)) {
		throw new Error('- Il campo giorni è obbligatorio per la tipologia scelta');
	}
}

/**
 * @param {JSRecord<db:/geco/jo_details>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"FC8A7AC0-2709-477B-92DB-899610E807D3"}
 */
function processBO_id(record) {
	if (record.bo_id == null) {
		if (record.jo_details_to_job_orders.job_orders_to_business_opportunities.countBO == 1)
			record.bo_id = record.jo_details_to_job_orders.bo_id;
		else {
			//cerco tra tutte le BO per data
			var boId = globals.getBoForJoDetails(record);
			record.bo_id = boId != null ? boId : record.jo_details_to_job_orders.bo_id;
		}
	}
}

/**
 * @param {JSRecord<db:/geco/jo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"730DC7DE-EBB2-4EC2-A395-383F029045DA"}
 * @AllowToRunInFind
 */
function processFigureValue(record) {
	//inserisce il valore del campo figure in base alla tipologia scelta
	var real_personnel = null;
	var real_tm = null;
	var real_tk = null;
	var figure = null;
	var uid = null;
	//peronale
	if ( (record.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || record.jo_details_to_profit_cost_types.profit_cost_types_id == 10) && record.jo_details_to_real_figure != null) {
		figure = record.jo_details_to_real_figure.users_to_contacts.real_name;
		real_personnel = record.real_figure;
		uid = record.real_figure;
	}
	// forniture TM
	else if (record.jo_details_to_profit_cost_types.profit_cost_types_id == 9 && record.jo_details_to_tm_figure != null) {
		figure = record.jo_details_to_tm_figure.users_to_contacts.real_name;
		real_tm = record.real_tm_figure;
		uid = record.real_tm_figure;
	}
	//forniture TK
	else if (record.jo_details_to_profit_cost_types.profit_cost_types_id == 8 && record.jo_details_to_suppliers != null) {
		figure = record.jo_details_to_suppliers.company_name;
		real_tk = record.real_tk_supplier
	}

	if ( (record.jo_details_to_profit_cost_types.profit_cost_types_id == 5 || record.jo_details_to_profit_cost_types.profit_cost_types_id == 9 || record.jo_details_to_profit_cost_types.profit_cost_types_id == 10) && figure == null) {
		figure = record.jo_details_to_standard_professional_figures.description;
	}
	var idProfCost = [1, 2, 3, 4, 12, 6, 11];
	//if ( (record.jo_details_to_profit_cost_types.profit_cost_types_id == 1 || record.jo_details_to_profit_cost_types.profit_cost_types_id == 2 || record.jo_details_to_profit_cost_types.profit_cost_types_id == 3 || record.jo_details_to_profit_cost_types.profit_cost_types_id == 4) && figure == null) {
	if (idProfCost.indexOf(record.jo_details_to_profit_cost_types.profit_cost_types_id) != -1) {
		figure = record.description;
	}

	if ( (record.jo_details_to_profit_cost_types.profit_cost_acr == 'R' && record.description == null) || (record.jo_details_to_profit_cost_types.profit_cost_acr == 'C' && (record.profit_cost_type_id == 6 || record.profit_cost_type_id == 11) && record.description == null)) {
		var tipo = '';
		switch (record.profit_cost_type_id) {
		case 1:
			tipo = 'competenze';
			break;
		case 2:
			tipo = 'spese';
			break;
		case 3:
			tipo = 'credito';
			break;
		case 4:
			tipo = 'allineamentoRicavi';
			break;
		case 6:
			tipo = 'fatturaPassiva';
			break;
		case 11:
			tipo = 'allineamentoCosti';
			break;
		case 12:
			tipo = 'fatturaAttiva';
			break;
		case 13:
			tipo = 'SpeseSPX';
			break;
		}
		/** @type {JSFoundSet<db:/geco/jo_details>} */
		var jo_details = databaseManager.getFoundSet('geco', 'jo_details');
		var totSameTypeProfit = 0;
		if (jo_details.find()) {
			jo_details.jo_id = record.jo_id;
			jo_details.jo_details_id = '!' + record.jo_details_id;
			jo_details.profit_cost_type_id = record.profit_cost_type_id;
			jo_details.description = tipo + '%';
			totSameTypeProfit = jo_details.search();
		}
		if (totSameTypeProfit > 0) {
			var description = '';
			var descrAr = [];
			var found = false;
			jo_details.sort('jo_details_id desc')
			for (var index = 1; index <= jo_details.getSize(); index++) {
				description = jo_details.getRecord(index).description;
				application.output('descrizone ricavo ' + description);
				descrAr = description.split(' ');
				application.output('descrizone ricavo ' + description);
				if (descrAr.length > 1 && descrAr[0] == tipo && !isNaN(descrAr[1])) {
					/** @type {Number} */
					var numero = parseInt(descrAr[1])
					var progr = numero + 1;
					application.output(progr);
					figure = descrAr[0] + ' ' + progr;
					found = true;
					break;
				} else {
					descrAr[0] = tipo;
				}
			}
			if (found == false) {
				figure = descrAr[0] + ' ' + 1;
			}

		} else {
			switch (record.profit_cost_type_id) {
			case 1:
				figure = 'competenze';
				break;
			case 2:
				figure = 'spese';
				break;
			case 3:
				figure = 'credito';
				break;
			case 4:
				figure = 'allineamento';
				break;
			case 6:
				figure = 'fatturaPassiva';
				break;
			case 11:
				figure = 'allineamentoCosti';
				break;
			case 12:
				figure = 'fatturaAttiva';
				break;
			case 13:
				figure = 'speseSPX';
				break;
			default:
				figure = '';
				break;
			}
		}
	}
	application.output('descrizione figura: ' + figure + ' real_personnel: ' + real_personnel + ' real_tm: ' + real_tm + ' real_tk : ' + real_tk)

	record.description = figure;
	record.real_figure = real_personnel;
	record.real_tm_figure = real_tm;
	record.real_tk_supplier = real_tk;
	record.user_id = uid;
}

/**
 * @param {JSRecord<db:/geco/jo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"928282F9-FBAB-42DD-BCB5-23CBBA787B64"}
 */
function processCostReturnImport(record) {
	//inserisce il valore dei campi return_import o cost_import in base alla tipologia scelta e ai valori inseriti
	if (record.jo_details_to_profit_cost_types.profit_cost_acr == 'R' && !globals.isEmpty(record.total_amount)) {
		if (record.profit_cost_type_id == 12) {
			record.invoice_active = record.total_amount
			record.return_amount = 0;
			record.invoice_payable = 0;
		} else {
			record.return_amount = record.total_amount;
		}
		record.cost_amount = 0;
	} else if (record.jo_details_to_profit_cost_types.profit_cost_acr == 'C' && !globals.isEmpty(record.total_amount)) {
		if (record.profit_cost_type_id == 6) {
			record.invoice_payable = record.total_amount
			record.cost_amount = 0;
			record.invoice_active = 0;
		} else {
			record.cost_amount = record.total_amount;
		}
		record.return_amount = 0;
	}
}

/**
 * ricalcola il valore del campo cost o profit del JO collegato
 * @param {JSRecord<db:/geco/jo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"4A7F5CAE-EC5C-40E7-ACB5-08E8BAFBD0BE"}
 */
function processChangeAmount(record) {
	//	/** @type {JSDataSet} */
	//	var dsUpdate = record.getChangedData();
	//	for( var i = 1; i <= dsUpdate.getMaxRowIndex(); i++ )
	//	{
	//		application.output(dsUpdate.getValue(i,1) +' '+ dsUpdate.getValue(i,2) +' '+ dsUpdate.getValue(i,3));
	//		//cambio costo
	//		if (dsUpdate.getValue(i,1) == 'cost_amount'){
	//			//sottroaggo dal campo cost del JO il vecchio importo e aggiungo il nuovo
	//			record.jo_details_to_job_orders.cost = record.jo_details_to_job_orders.cost - dsUpdate.getValue(i,2);
	//			record.jo_details_to_job_orders.cost = record.jo_details_to_job_orders.cost + dsUpdate.getValue(i,3);
	//		}
	//		if (dsUpdate.getValue(i,1) == 'return_amount'){
	//			//sottroaggo dal campo profit della BO il vecchio importo e aggiungo il nuovo
	//			record.jo_details_to_job_orders.profit = record.jo_details_to_job_orders.profit - dsUpdate.getValue(i,2);
	//			record.jo_details_to_job_orders.profit = record.jo_details_to_job_orders.profit + dsUpdate.getValue(i,3);
	//		}
	//		if (dsUpdate.getValue(i,1) == 'cost_actual'){
	//			//sottroaggo dal campo cost_actual del JO il vecchio importo e aggiungo il nuovo
	//			record.jo_details_to_job_orders.cost_actual = record.jo_details_to_job_orders.cost_actual - dsUpdate.getValue(i,2);
	//			record.jo_details_to_job_orders.cost_actual = record.jo_details_to_job_orders.cost_actual + dsUpdate.getValue(i,3);
	//			record.jo_details_to_job_orders.date_actual = record.date_actual;
	//			record.jo_details_to_job_orders.user_actual = record.user_actual;
	//			record.competence_month = scopes.globals.actualDate;
	//			record.jo_details_to_job_orders.competence_month = scopes.globals.actualDate;
	//		}
	//		if (dsUpdate.getValue(i,1) == 'return_actual'){
	//			//sottroaggo dal campo profit_actual della BO il vecchio importo e aggiungo il nuovo
	//			record.jo_details_to_job_orders.profit_actual = record.jo_details_to_job_orders.profit_actual - dsUpdate.getValue(i,2);
	//			record.jo_details_to_job_orders.profit_actual = record.jo_details_to_job_orders.profit_actual + dsUpdate.getValue(i,3);
	//			record.jo_details_to_job_orders.date_actual = record.date_actual;
	//			record.jo_details_to_job_orders.user_actual = record.user_actual;
	//			record.competence_month = scopes.globals.actualDate;
	//			record.jo_details_to_job_orders.competence_month = scopes.globals.actualDate;
	//		}
	//	}
}
