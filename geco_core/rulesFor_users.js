/**
 * @param {JSRecord<db:/geco/users>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"BC79EA8E-28FE-419B-AD53-92EF0F4F8926"}
 */
function validateFirstName(record) {
	if (globals.isEmpty(record.users_to_contacts.first_name)) {
		throw new Error('- Il campo nome è obbligatorio');
	}
	record.users_to_contacts.first_name = globals.trimString(record.users_to_contacts.first_name);
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"57F59156-5DAF-47F1-ACB8-6D6C454C70AD"}
 */
function validateLastName(record) {
	if (globals.isEmpty(record.users_to_contacts.last_name)) {
		throw new Error('- Il campo cognome è obbligatorio');
	}
	record.users_to_contacts.last_name = globals.trimString(record.users_to_contacts.last_name);
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"832D7142-A62E-4196-9D63-0F7A98974419"}
 */
function validateEnrollmentStartTime(record) {
	if (globals.isEmpty(record.enrollment_start_date)) {
		throw new Error('- Il campo data inizio è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"D195EFB9-3A56-441E-AFDF-C68011ED4001"}
 */
function validateEnrollmentEndTime(record) {
	if (!globals.isEmpty(record.enrollment_start_date) && !globals.isEmpty(record.enrollment_end_date)) {
		if(record.enrollment_end_date < record.enrollment_start_date){
			throw new Error('- La data di fine rapporto non può essere precedente alla data di inizio rapporto');
		}
		
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"A43FA4C9-985C-49E6-A93E-CDA835DF4066"}
 */

function validateEnrollmentNumber(record) {
	if (globals.isEmpty(record.enrollment_number)) {
		throw new Error('- Il campo matricola è obbligatorio');
	}
	record.enrollment_number = globals.trimString(record.enrollment_number);
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"DEDD3358-4D5B-4CF1-A008-737AC4AC30EA"}
 */
function validateProfitCenter(record) {
	if (globals.isEmpty(record.profit_center_id)) {
		throw new Error('- Il campo Centro Profitto è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"BEC3C119-7CBD-4945-B5A8-DD8CEFB679B7"}
 * @AllowToRunInFind
 */
function validateIsUniqueFepCode(record) {
	//cerco se già esiste lo stesso fepCode per un utente diverso
	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	if (!globals.isEmpty(record.fep_code) && record.fep_code != -1) {
		if (users.find()) {
			users.fep_code = '#' + record.fep_code;
			users.user_id = "!=" + record.user_id;
			var count = users.search();
			if (count > 0) {
				throw new Error('- Esiste già un utente con lo stesso codice Fep');
			}
		}
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"809B7377-CD22-4190-B2CB-F019EBFDB312"}
 * @AllowToRunInFind
 */
function validateIsUniqueNavisionCode(record) {
	//cerco se già esiste lo stesso codice Navision per un utente diverso
	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	if (!globals.isEmpty(record.enrollment_number)) {
		if (users.find()) {
			users.enrollment_number = '#' + record.enrollment_number;
			users.user_id = "!=" + record.user_id;
			var count = users.search();
			if (count > 0) {
				throw new Error('- Esiste già un utente con lo stesso codice Navision');
			}
		}
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"F5719834-4094-4412-BF30-0D85581283AB"}
 */
function validateContractType(record) {
	if (globals.isEmpty(record.user_type_id)) {
		throw new Error('- Il campo Contratto è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"ED112B28-539F-47EC-B849-F4AAF12B910D"}
 */
function validateWorkingHours(record) {
	if (record.users_to_user_types.external_code == 'IMPX' && globals.isEmpty(record.contract_type_id)) {
		throw new Error('- Il campo Ore Lavorative è obbligatorio');
	}
	
}


/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"4383C33C-AEAE-4437-9E1E-BE6136EDB1FA"}
 */
function validateWorkingHoursDate(record) {
	var dataset = record.getChangedData()
	for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
		if (dataset.getValue(i, 1) == 'contract_type_id' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			if (record.working_hours_start_date == null)
				throw new Error('- Il campo data del cambio ore lavorative è obbligatorio');
			/** @type {Date} */
			var startDate = new Date(record.enrollment_start_date.getFullYear(), record.enrollment_start_date.getMonth(), record.enrollment_start_date.getDate());
			if (record.working_hours_start_date < startDate)
				throw new Error('- Il campo data del cambio ore lavorative non può essere minore della data di inizio rapporto');
		}
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"12266951-3792-4CCF-824A-DB7CB9641B8F"}
 */
function validateProfitCenterDate(record) {
	var dataset = record.getChangedData()
	for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
		if (dataset.getValue(i, 1) == 'profit_center_id' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			if (record.prof_center_start_date == null)
				throw new Error('- Il campo data del cambio profit center è obbligatorio');
			/** @type {Date} */
			var startDate = new Date(record.enrollment_start_date.getFullYear(), record.enrollment_start_date.getMonth(), record.enrollment_start_date.getDate());
			if (record.prof_center_start_date < startDate)
				throw new Error('-  Il campo data del cambio profit center non può essere minore della data di inizio rapporto');
		}
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"922BC00A-5F58-417B-BB21-3B5E5C2F7EDF"}
 */
function validateStartStopTimes(record) {
	//if(record.users_to_contract_types != null && record.users_to_contract_types.is_weekly != 1){
	
		if (record.users_to_user_types.external_code == 'IMPX' && record.users_to_contract_types != null && record.users_to_contract_types.is_weekly != 1) {
		//ora inizio e fine turno
		if (record.shift_start != null && record.shift_stop != null) {
			//ora inizio turno >= ora fine turno
			if (record.shift_start >= record.shift_stop)
				throw new Error('- L\'ora di inizio turno deve essere minore dell\'ora di fine turno');

			//se prensente pausa pranzo
			if (record.break_start != null && record.break_stop != null) {
				//inizio pranzo - fine pranzo
				if (record.break_start >= record.break_stop)
					throw new Error('- L\'ora di inizio pranzo deve essere minore dell\'ora di fine pranzo');
				//inizio turno - inizio pranzo
				if (record.shift_start >= record.break_start)
					throw new Error('- L\'ora di inizio turno deve essere minore dell\'ora di inizio pranzo');
				//fine pranzo - fine turno
				if (record.break_stop >= record.shift_stop)
					throw new Error('- L\'ora di fine pranzo deve essere minore dell\'ora di fine turno');
				//conteggio ore inserite
				if ( (record.shift_stop - record.shift_start) / 60 - (record.break_stop - record.break_start) / 60 != record.working_hours) {
					throw new Error('- Gli intervalli inseriti non coincidono con il campo Ore Lavorative');
				}
			}
			//le ore di break sono null, controllo le ore di inizio e fine turno
			else if ( (record.shift_stop - record.shift_start) / 60 != record.working_hours) {
				throw new Error('- Gli intervalli inseriti non coincidono con il campo ore lavorative');
			}
		} else
			throw new Error('- L\'ora di inizio e di fine devono essere valorizzate');
	}
	//non IMPX
	else {
		//ora inizio e fine turno
		if (record.shift_start != null && record.shift_stop != null) {
			//ora inizio turno >= ora fine turno
			if (record.shift_start >= record.shift_stop)
				throw new Error('- L\'ora di inizio turno deve essere minore dell\'ora di fine turno');
			if (record.break_start != null && record.break_stop != null) {
				if (record.break_start >= record.break_stop)
					throw new Error('- L\'ora di inizio pranzo deve essere minore dell\'ora di fine pranzo');
				//inizio turno - inizio pranzo
				if (record.shift_start >= record.break_start)
					throw new Error('- L\'ora di inizio turno deve essere minore dell\'ora di inizio pranzo');
				//fine pranzo - fine turno
				if (record.break_stop >= record.shift_stop)
					throw new Error('- L\'ora di fine pranzo deve essere minore dell\'ora di fine turno');
			}
		}
	}
 // }
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"B6C011E3-E1D0-4E77-9930-B69139651BC4"}
 */
function validateCar(record) {
	if (record.users_to_user_types.external_code == 'IMPX' && !globals.isEmpty(record.users_to_users_cars.user_car_type)) {
		//se la macchina è personale deve scegliere la cilindrata
		if (record.users_to_users_cars.user_car_type == 0 && !record.users_to_users_cars.personal_car_refound_km) {
			record.users_to_users_cars.corporate_car_refound_km = null;
			throw new Error('- Il campo Cilindrata è obbligatorio per la macchina personale');
		}
		if (record.users_to_users_cars.user_car_type == 1 && !record.users_to_users_cars.corporate_car_refound_km) {
			record.users_to_users_cars.personal_car_refound_km = null;
			throw new Error('- Il campo Rimborso è obbligatorio per la macchina aziendale');
		}
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"3D37CB46-5539-4274-A844-7EFCF864ADFD"}
 */
function processUniqueUsername(record) {
	var username;
	//insert username is null
	if (record.user_name == null) {
		var first = record.users_to_contacts.first_name.toLowerCase();
		first = first.replace(' ', '');

		var last = record.users_to_contacts.last_name.toLowerCase();
		last = last.replace(' ', '');

		username = first + "." + last;
		// if consultant username is <firstname>.<lastname>.consulente
		if (record.user_type_id == 8 || record.user_type_id == 9)
			username = username + ".consulente";
	}
	//update username not null
	else username = record.user_name;

	//cerco se già esiste lo stesso username per un utente diverso
	//(non dovrebbe trovarne mai in update)
	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');
	if (users.find()) {
		users.user_name = username;
		users.user_id = "!=" + record.user_id;
		var recordCount = users.search();
		if (recordCount > 0) {
			record.user_name = username;
			throw new Error('- esiste già un utente con lo stesso username, modifica il campo');

		} else {
			record.user_name = username;
		}
	}

}

/**
 * @param {JSRecord<db:/geco/users>} record
 * @properties={typeid:24,uuid:"7A3B1B4D-CA45-4BC7-82FE-7B156C95DA63"}
 */

function processCompany(record) {
	if (record.users_to_user_types.external_code == 'IMPX' || record.users_to_user_types.external_code == 'CCCX' || record.users_to_user_types.external_code == 'STGX') {
		record.users_to_contacts.company_id = 0;
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 *
 * @properties={typeid:24,uuid:"3C394066-D040-4B77-AEB0-ED80656B6F71"}
 * @AllowToRunInFind
 */
function processUserGroups(record) {
	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var user_groups = record.users_to_user_groups;

	if (record.is_enabled == 1) {
		//Users
		user_groups.addUserGroup(record.user_id, 8);

		switch (record.user_type_id) {
		case 6: //CCCX
			user_groups.addUserGroup(record.user_id, 13); //contratto a progetto
//			JS ora Contratto a Progetto PUO' essere RDC
//			processDeleteUserGroups(record, [12, 11, 10, 9]);
			processDeleteUserGroups(record, [12, 10, 9]);
			break;
		case 7: //IMPX
			user_groups.addUserGroup(record.user_id, 9);
			processDeleteUserGroups(record, [13, 12, 10]); //employees
			break;
		case 8: //CIVAX
			user_groups.addUserGroup(record.user_id, 10); //consultants iva
			processDeleteUserGroups(record, [13, 12, 11, 9]);
			break;
		case 9: //SOCX
			user_groups.addUserGroup(record.user_id, 10); //consultants soc
			processDeleteUserGroups(record, [13, 12, 11, 9]);
			break;
		case 10: //STGX
			user_groups.addUserGroup(record.user_id, 12); //intern
			processDeleteUserGroups(record, [13, 11, 10, 9]);
			break;

		default:
			break;
		}
	}
	if (record.is_enabled == 0) {
		processDeleteUserGroups(record, [13, 12, 11, 10, 9, 8]);
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 * @param {Number[]} groupId
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"4A7929D9-BEEF-4FBA-A112-903CFEE969E5"}
 */
function processDeleteUserGroups(record, groupId) {
	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var user_groups = record.users_to_user_groups;
	user_groups.deleteUserGroup(record.user_id, groupId);

}

/**
 * @param {JSRecord<db:/geco/users>} record
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"39308C80-9A1E-430B-84D1-D7630DA458E8"}
 */
function processUserHoliday(record) {

	var startDate = new Date(record.enrollment_start_date.getFullYear(), record.enrollment_start_date.getMonth(), record.enrollment_start_date.getDate())
	application.output(startDate);
	if (record.user_type_id == 7) {
		/** @type {JSFoundSet<db:/geco/calendar_days>} */
		var calendars_days = databaseManager.getFoundSet('geco', 'calendar_days');

		if (calendars_days.find()) {
			//'>=01/01/2001|MM/dd/yyyy'
			//calendars_days.calendar_date = '>=' + startDate + '|yyyy-MM-dd';
			//calendars_days.calendar_date = '>=' + startDate + '|yyyy-MM-dd';
			calendars_days.is_holiday = 1;
			calendars_days.search()
		}

		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log = databaseManager.getFoundSet('geco', 'events_log');

		for (var index = 1; index <= calendars_days.getSize(); index++) {
			/** @type {JSRecord<db:/geco/calendar_days>} */
			var recCalendar = calendars_days.getRecord(index);
			application.output(recCalendar.calendar_date);
			if (recCalendar.calendar_date >= startDate) {
				application.output('inserisco ' + recCalendar.calendar_date);
				events_log.newRecord();
				events_log.user_id = record.user_id;
				events_log.event_log_date = recCalendar.calendar_date;
				events_log.event_log_status = 4;
				events_log.external_code_fep = 'FES';
				events_log.event_id = '34';
			}

		}
		if (!globals.isEmpty(record.head_office_id)) {

			var year = new Date().getFullYear();
			var giornoPatrono = new Date(year, record.users_to_head_office.head_office_month_holiday - 1, record.users_to_head_office.head_office_day_holiday);
			application.output(giornoPatrono);

			if (giornoPatrono >= record.enrollment_start_date) {
				events_log.newRecord();
				events_log.user_id = record.user_id;
				events_log.event_log_date = giornoPatrono;
				events_log.event_log_status = 4;
				events_log.external_code_fep = 'FES';
				events_log.event_id = '34';
			}

		}
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"CE9655CC-15CB-4659-B259-65DE737DD238"}
 */
function processUserHeadHoliday(record) {
	application.output('da scrivere');
}
/**
 * @param {JSRecord<db:/geco/users>} record
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"D81C80FB-B4CE-492C-898C-827D27FE19C8"}
 */
function validateWorkingHoursChange(record) {
	var dataset = record.getChangedData();
	var totHistory = 0;
	var changeWH = false;
	var changeDataWH = false;
	for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
		if (dataset.getValue(i, 1) == 'contract_type_id' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			changeWH = true;
		}
		if (dataset.getValue(i, 1) == 'working_hours_start_date' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			changeDataWH = true;
		}
	}

	/** @type {JSFoundSet<db:/geco/users_working_hours_history>} */
	var user_wh = record.users_to_users_working_hours_history;
	if (user_wh.find()) {
		user_wh.user_id = record.user_id;
		totHistory = user_wh.search();
	}
	user_wh.sort('start_change_date desc, created_at desc');

	/** @type {Date} */
	var startDate = null;
	// se ha cambiato solo la data di cambioWH
	// sto modificando il primo record dello storico
	if (changeDataWH && !changeWH) {
		startDate = new Date(record.enrollment_start_date.getFullYear(), record.enrollment_start_date.getMonth(), record.enrollment_start_date.getDate());
		if(record.working_hours_start_date < startDate){
			throw new Error('-  Il campo data del cambio ore lavorative non può essere minore della data di inizio rapporto');
		}
		
		//se non ho niente nello storico,controllo solo che la data non sia minore di quella dell'assunzione
//		if (totHistory == 0) {
//			if (record.working_hours_start_date < startDate)
//				throw new Error('-  Il campo data del cambio ore lavorative non può essere minore della data di inizio rapporto');
//		} 
////           else if (totHistory == 1) {
//			//TODO controllare perchè non può cambiare solo la data se ha un solo storico
//			var rec = user_wh.getSelectedRecord();
//			var date = rec.start_change_date;
//			if (record.working_hours_start_date < date) {
//				throw new Error('-  Non è possibile modificare la data del cambio ore lavorative inserendone una inferiore al cambio precedente');
//			}
		 if (totHistory > 1) {
			var rec2 = user_wh.getRecord(2);
			var date2 = rec2.start_change_date;
			if (record.working_hours_start_date < date2) {
				throw new Error('-  Non è possibile modificare la data del cambio ore lavorative inserendone una inferiore al cambio precedente');
			}
		}
	}
	// se ha cambiato sia working hours che la data di cambio di working hours
	//sto aggiungendo un nuovo record nello storico
	if (changeWH && changeDataWH) {
		startDate = new Date(record.enrollment_start_date.getFullYear(), record.enrollment_start_date.getMonth(), record.enrollment_start_date.getDate());
		//se non ho niente nello storico,controllo solo che la data non sia minore di quella dell'assunzione
		if (totHistory == 0) {
			if (record.working_hours_start_date < startDate)
				throw new Error('-  Il campo data del cambio ore lavorative non può essere minore della data di inizio rapporto');
		}
		//se ho qualcosa nello storico,controllo che la data non sia minore della data dell'ultimo cambio
		if (totHistory > 0) {
			//se la data inserita è inferiore alla data dell'ultimo cambio
			if (record.working_hours_start_date < user_wh.start_change_date) {
				throw new Error('- Non è possibile modificare le ore lavorative inserendo una data inferiore al cambio precedente');
			}

		}
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"AE180001-4E89-450B-A838-F65583523A09"}
 */
function processWorkingHoursChange(record) {
	var dataset = record.getChangedData();
	var totChange = 0;
	var changheWH = false;
	var changeDataWH = false;
	var oldCT = 0;
	
	var WHold = 0;
	var startOld = record.shift_start;
	var stopOld = record.shift_stop;
	var breakStartOld = record.break_start;
	var breakStopOld = record.break_stop;
	
	for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
		if (dataset.getValue(i, 1) == 'contract_type_id' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			changheWH = true;
			oldCT = dataset.getValue(i, 2);
			
			/** @type {JSFoundSet<db:/geco/contract_types>} */
			var CT = databaseManager.getFoundSet('geco','contract_types');
			if(CT.find()){
				CT.contract_type_id = oldCT;
				CT.search();
				WHold = CT.daily_workable_hours;
			}
			
		}
		if (dataset.getValue(i, 1) == 'working_hours_start_date' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			changeDataWH = true;
		}
		if (dataset.getValue(i, 1) == 'shift_start' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			startOld = dataset.getValue(i, 2);
		}
		if (dataset.getValue(i, 1) == 'shift_stop' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			stopOld = dataset.getValue(i, 2);
		}
		if (dataset.getValue(i, 1) == 'break_start' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			breakStartOld = dataset.getValue(i, 2);
		}
		if (dataset.getValue(i, 1) == 'break_stop' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			breakStopOld = dataset.getValue(i, 2);
		}
	}

	/** @type {JSFoundSet<db:/geco/users_working_hours_history>} */
	var user_wh = record.users_to_users_working_hours_history;
	if (user_wh.find()) {
		user_wh.user_id = record.user_id;
		totChange = user_wh.search();
	}
	user_wh.sort('start_change_date desc, created_at desc');
	//se ho cambiato sia la data che contract_type

	var dateChange = null;
	if (changheWH && changeDataWH) {
		dateChange = new Date(record.working_hours_start_date.getFullYear(), record.working_hours_start_date.getMonth(), record.working_hours_start_date.getDate());
		//creo il record sulla tabella hisotry
		user_wh.newRecord();
		user_wh.user_id = record.user_id;
		user_wh.contract_type_id = record.contract_type_id;
		user_wh.previous_working_hours = WHold;
		user_wh.actual_working_hours = record.users_to_contract_types.daily_workable_hours;
		user_wh.start_change_date = dateChange;
		user_wh.previous_contract_type = oldCT;
		user_wh.previous_start_time = startOld;
		user_wh.previous_stop_time = stopOld;
		user_wh.previous_start_break = breakStartOld;
		user_wh.previous_stop_break = breakStopOld;		
	}
	//se ho cambiato solo la data o solo contract_type
	else if ( (changheWH && !changeDataWH) || (!changheWH && changeDataWH)) {

		if (totChange > 0) {
			//modifico l'ultimo record sulla tabella history
			if (changheWH) {
				user_wh.actual_working_hours = record.users_to_contract_types.daily_workable_hours;
//				user_wh.previous_contract_type = oldCT;
				user_wh.contract_type_id = record.contract_type_id;
			}
//			user_wh.actual_working_hours = (changheWH) ? record.users_to_contract_types.daily_workable_hours : user_wh.actual_working_hours;
//			user_wh.previous_contract_type = (changheWH) ? oldCT : user_wh.previous_contract_type;
//			user_wh.contract_type_id = (changheWH) ?  record.contract_type_id : user_wh.contract_type_id;
			//user_wh.previous_working_hours = (changheWH) ? WHold : user_wh.previous_working_hours;
			if (changeDataWH)
				dateChange = new Date(record.working_hours_start_date.getFullYear(), record.working_hours_start_date.getMonth(), record.working_hours_start_date.getDate());
			else
				dateChange = user_wh.start_change_date;

			user_wh.start_change_date = dateChange;
		}
	}

}

/**
 * date di default per working_hours e profit_center
 * @param {JSRecord<db:/geco/users>} record
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"A7FD6CC4-72CE-4623-9509-6DEC9593947D"}
 */
function processSetDate(record) {
	var startDate = new Date(record.enrollment_start_date.getFullYear(), record.enrollment_start_date.getMonth(), record.enrollment_start_date.getDate());
	//Aggiungo le date di default per i campi prof_center_start_date e working_hours_start_date
	if (globals.isEmpty(record.prof_center_start_date)) {
		record.prof_center_start_date = startDate;
	}
	if (globals.isEmpty(record.working_hours_start_date)) {
		record.working_hours_start_date = startDate;
	}
	record.enrollment_start_date = startDate;
}

/**
 * @param {JSRecord<db:/geco/users>} record
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"BC0351E3-606E-4508-A60D-2AD5B8C8BCCC"}
 */
function validateProfCenterChange(record) {
	var dataset = record.getChangedData();
	var totChange = 0;
	var changePC = false;
	var changeDataPC = false;
	for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
		if (dataset.getValue(i, 1) == 'profit_center_id' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			changePC = true;
		}
		if (dataset.getValue(i, 1) == 'prof_center_start_date' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			changeDataPC = true;
		}
	}

	/** @type {JSFoundSet<db:/geco/users_profit_centers_history>} */
	var user_profit_center = record.users_to_users_profit_centers_history;
	if (user_profit_center.find()) {
		user_profit_center.user_id = record.user_id;
		totChange = user_profit_center.search();
	}
	user_profit_center.sort('start_change_date desc, created_at desc');

	/** @type {Date} */
	var startDate = null;
	// se ha cambiato solo la data di cambioPC
	// sto modificando il primo record dello storico
	if (changeDataPC && !changePC) {
		startDate = new Date(record.enrollment_start_date.getFullYear(), record.enrollment_start_date.getMonth(), record.enrollment_start_date.getDate());
		//se non ho niente nello storico,controllo solo che la data non sia minore di quella dell'assunzione
		if (totChange == 0) {
			if (record.prof_center_start_date < startDate)
				throw new Error('-  Il campo data del cambio profit center non può essere minore della data di inizio rapporto');
		} else if (totChange == 1) {
			// uno storico sto cambiando la data di cambio
			var rec = user_profit_center.getSelectedRecord();
			var date = rec.start_change_date;
			if (record.prof_center_start_date < date) {
				//throw new Error('-  Non è possibile modificare la data del cambio profit center inserendone una inferiore al cambio precedente');
			}
		} else if (totChange > 1) {
			//più storici cambia il secondo record
			var rec2 = user_profit_center.getRecord(2);
			var date2 = rec2.start_change_date;
			if (record.prof_center_start_date < date2) {
				throw new Error('-  Non è possibile modificare la data del cambio profit center inserendone una inferiore al cambio precedente');
			}
		}
	}
	// se ha cambiato sia il profit center che la data di cambio
	//sto aggiungendo un nuovo record nello storico
	if (changePC && changeDataPC) {
		startDate = new Date(record.enrollment_start_date.getFullYear(), record.enrollment_start_date.getMonth(), record.enrollment_start_date.getDate());
		//se non ho niente nello storico,controllo solo che la data non sia minore di quella dell'assunzione
		if (totChange == 0) {
			if (record.prof_center_start_date < startDate)
				throw new Error('-  Il campo data del cambio profit center non può essere minore della data di inizio rapporto');
		}
		//se ho qualcosa nello storico,controllo che la data non sia minore della data dell'ultimo cambio
		if (totChange > 0) {
			//se la data inserita è inferiore alla data dell'ultimo cambio
			if (record.prof_center_start_date < user_profit_center.start_change_date) {
				throw new Error('- Non è possibile modificare il profit center inserendo una data inferiore al cambio precedente');
			}

		}
	}
}

/**
 * @param {JSRecord<db:/geco/users>} record
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"0B940A35-AF76-4CFA-85E4-771FB38E24D4"}
 */
function processProfCenterChange(record) {
	var dataset = record.getChangedData();
	var totChange = 0;
	var changhePC = false;
	var changeDataPC = false;
	//var event_to_change = false;
	var oldPC = null;
	for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
		if (dataset.getValue(i, 1) == 'profit_center_id' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			changhePC = true;
			oldPC = dataset.getValue(i, 2);
		}
		if (dataset.getValue(i, 1) == 'prof_center_start_date' && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
			changeDataPC = true;
		}
	}

	/** @type {JSFoundSet<db:/geco/users_profit_centers_history>} */
	var user_profit_center = record.users_to_users_profit_centers_history;
	if (user_profit_center.find()) {
		user_profit_center.user_id = record.user_id;
		totChange = user_profit_center.search();
	}
	user_profit_center.sort('start_change_date desc, created_at desc');
	//se ho cambiato sia la data che il PC

	var dateChange = null;
	if (changhePC && changeDataPC) {
		dateChange = new Date(record.prof_center_start_date.getFullYear(), record.prof_center_start_date.getMonth(), record.prof_center_start_date.getDate());
		//creo il record sulla tabella hisotry
//		user_profit_center.newRecord();
//		user_profit_center.user_id = record.user_id;
//		user_profit_center.previous_prof_center_id = oldPC;
//		user_profit_center.actual_prof_center_id = record.profit_center_id;
//		user_profit_center.start_change_date = dateChange;
		user_profit_center.insertHistory(record.user_id,oldPC,record.profit_center_id,dateChange);
		
		//event_to_change = true;
	}
	//se ho cambiato solo la data o solo il PC
	else if ( (changhePC && !changeDataPC) || (!changhePC && changeDataPC)) {
		if (totChange > 0) {
			//modifico l'ultimo record sulla tabella history
			user_profit_center.actual_prof_center_id = (changhePC) ? record.profit_center_id : user_profit_center.actual_prof_center_id;
			if (changeDataPC)
				dateChange = new Date(record.prof_center_start_date.getFullYear(), record.prof_center_start_date.getMonth(), record.prof_center_start_date.getDate());
			else
				dateChange = user_profit_center.start_change_date;

			user_profit_center.start_change_date = dateChange;
		}
		//event_to_change = true;
	}
	
//	if (event_to_change) {
//		/** @type {JSFoundSet<db:/geco/events_log>} */
//		var events_log = databaseManager.getFoundSet('geco', 'events_log');
//		if (events_log.find()) {
//			events_log.user_id = record.user_id;
//			events_log.event_log_status = '1';
//			events_log.events_log_to_events.approver_figure = '2';
//			//modifico l'approvatore dell'evneto in modo che quando salva gli eventi lo ricalcoli
//			if (events_log.search() > 0) {
//				for (var index = 1; index <= events_log.getSize(); index++) {
//					if (events_log.event_log_date >=dateChange){
//						var rec = events_log.getRecord(index);
//						rec.user_approver_id = 0;
//					}
//				}
//			}
//		}
//	}

}

/**
 * @param {JSRecord<db:/geco/users>} record
 * @properties={typeid:24,uuid:"69E1F2FE-6B34-4831-8E19-AED9C71E8CE8"}
 */
function validateShiftWork(record) {
	//no consulenti, verificare stgx e cccx
	if ((record.user_type_id == 8 || record.user_type_id == 9) && record.is_shift_worker == 1)
		throw new Error('- Non è possibile selezionare lavoro a turni per un consulente');
	if (record.is_shift_worker == 1 && globals.isEmpty(record.shift_work_type_id)) {
		throw new Error('- L\'utente fa un lavoro a turni, selezionare il tipo di turno');
	}
}


/**
 * JStaffa Valida inserimento Azienda se utente ha diritto ai buoni pasto
 * @param {JSRecord<db:/geco/users>} record
 * @properties={typeid:24,uuid:"B97949C9-429F-405C-BC9C-C8A6E330F9B1"}
 */
function validateUserDailyLunch(record) {
	if (record.has_daily_lunch == 1 && globals.isEmpty(record.ticket_company_id)) {
		throw new Error('- L\'utente ha diritto ai buoni pasto, selezionare l\'Azienda erogatrice');
	} else if (record.has_daily_lunch == 1 && !globals.isEmpty(record.ticket_company_id)) {
		if (globals.isEmpty(record.daily_launch_rate)) record.daily_launch_rate = 0;
	}
}