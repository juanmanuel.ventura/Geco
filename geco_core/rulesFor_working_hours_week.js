/**
 * @param {JSRecord<db:/geco/working_hours_week>} record
 *
 * @properties={typeid:24,uuid:"EA39BA7D-8EA6-470D-8EE0-192A1613F5B2"}
 */
function validateStartStopTimes(record) {
		//ora inizio e fine turno
		if (record.time_start != null && record.time_stop != null) {
			//ora inizio turno >= ora fine turno
			if (record.time_start >= record.time_stop)
				throw new Error('- L\'ora di inizio turno deve essere minore dell\'ora di fine turno');
			if (record.break_start != null && record.break_stop != null) {
				if (record.break_start >= record.break_stop)
					throw new Error('- L\'ora di inizio pranzo deve essere minore dell\'ora di fine pranzo');
				//inizio turno - inizio pranzo
				if (record.time_start >= record.break_start)
					throw new Error('- L\'ora di inizio turno deve essere minore dell\'ora di inizio pranzo');
				//fine pranzo - fine turno
				if (record.break_stop >= record.time_stop)
					throw new Error('- L\'ora di fine pranzo deve essere minore dell\'ora di fine turno');
				
			}
			if((record.break_start != null && record.break_stop == null) || record.break_start == null && record.break_stop != null)
				throw new Error('- Valorizzare sia inizio che fine pranzo.');
		}
//		else
//			throw new Error('- L\'ora di inizio e di fine devono essere valorizzate');
}

/**
 * @param {JSRecord<db:/geco/working_hours_week>} record
 *
 * @properties={typeid:24,uuid:"9F304AA5-54A4-427D-A6B8-1CF0F663C6E3"}
 */
function validateWeekDay(record) {
	if (globals.isEmpty(record.calendar_weekday)) {
		throw new Error('- Il campo Giorno è obbligatorio');
	}
}


/**
 * @param {JSRecord<db:/geco/working_hours_week>} record
 *
 * @properties={typeid:24,uuid:"009421C6-44F8-4B41-B5D5-335A1CFDDA89"}
 * @AllowToRunInFind
 */
function validateNotSameDay(record) {
	/** @type {JSFoundSet<db:/geco/working_hours_week>} */
	var hours = databaseManager.getFoundSet('geco', 'working_hours_week');
	if (!globals.isEmpty(record.calendar_weekday)){
		if(hours.find()){
			hours.calendar_weekday = record.calendar_weekday;
			hours.working_hours_week_id = "!=" + record.working_hours_week_id;
			hours.contract_type_id = record.contract_type_id;
			var count = hours.search();
			if (count > 0) {
				throw new Error('- Esiste già un giorno della settimana con lo stesso nome');
		}
	}
}
}

/**
 * @param {JSRecord<db:/geco/working_hours_week>} record
 * * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"7830587D-82C1-4348-86B8-A4F88C13E0ED"}
 */
function processTotalDuration(record) {
	var durBreak = 0;
	var durWork = 0;
	if (record.time_start != null && record.time_stop != null){
		durWork = (record.time_stop - record.time_start)/60;
	}
	
	if(record.break_start != null && record.break_stop != null){
		durBreak = (record.break_stop - record.break_start)/60;
	}	
	if (durWork >0 ) durWork = durWork - durBreak;
	
		record.duration_break = durBreak;
		record.duration_work = durWork;	
}