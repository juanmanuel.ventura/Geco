/**
 * @param {JSRecord<db:/geco/bo_users_cost>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"17F02A5B-5E2C-4621-BDD6-506FC30576AC"}
 */
function validateDateFrom(record) {
	if (globals.isEmpty(record.user_cost_date_from)) {
		throw new Error('- Il campo Data è obbligatorio');
	}
	var dateFirst = new Date(record.user_cost_date_from.getFullYear(),record.user_cost_date_from.getMonth(),1);
	record.user_cost_date_from = dateFirst;
}

/**
 * @param {JSRecord<db:/geco/bo_users_cost>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"AB3C4912-987E-4983-91B6-262EE00C31E3"}
 */
function validateCost(record) {
	if (globals.isEmpty(record.user_cost)) {
		throw new Error('- Il campo Costo è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/bo_users_cost>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"4375012E-7265-43C9-8475-367F8AEA4FCE"}
 */
function processEnrollmentNumber(record) {
	if (record.enrollment_number == null) record.enrollment_number = record.bo_users_cost_to_users.enrollment_number;
	else {
		record.enrollment_number = record.enrollment_number.toUpperCase();
	}
}