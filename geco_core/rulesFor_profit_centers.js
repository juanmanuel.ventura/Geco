/**
 * @param {JSRecord<db:/geco/profit_centers>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"ADC18A75-6852-47AF-8852-A6D91DEDCCEA"}
 */
function validateProfitCenterName(record) {
	if (globals.isEmpty(record.profit_center_name)) {
		throw new Error('- Il campo Nome è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/profit_centers>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"703D1FC4-97A3-4EC0-9287-D6D35D8195A0"}
 */
function validateProfitCenterOwner(record) {
	if (globals.isEmpty(record.user_owner_id)) {
		throw new Error('- Il campo Responsabile è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/profit_centers>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"44741280-E8A5-43E8-80CC-38577B5D175A"}
 */
function validateProfitCenterType(record) {
	if (globals.isEmpty(record.profit_center_type_id)) {
		throw new Error('- Il campo Tipo è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/profit_centers>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"F8A370DE-41AA-4FDC-AD1F-98F008582B2C"}
 */
function validateCheckBeforeDelete(record) {
	if (record.profit_centers_to_users.getSize() > 0 && record.is_enabled == 0) {
		throw new Error('- A questo centro profitto sono associati utenti, per disabilitarlo devi prima cambiare il centro profitto degli utenti.')
	}
}

/**
 * @param {JSRecord<db:/geco/profit_centers>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"B361BBE3-3157-408E-9C7E-AE291F549E5C"}
 */
function processDefaultApporver(record) {
	/** @type {JSFoundSet<db:/geco/approvers>} */
	var approvers = databaseManager.getFoundSet('geco', 'approvers');
//	/** @type {JSFoundSet<db:/geco/user_groups>} */
//	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');

	try {
		approvers.addApprover('profit_centers', record.profit_center_id, record.user_owner_id,null);
		//Commentata aggunta nel gruppo, lo fa quando aggiunge in tabella approvers
		//user_groups.addUserGroup(record.user_owner_id, 11);
	} catch (e) {
		throw new Error("- Impossibile determinare l'approvatore di default\n" + e['message']);
	} finally {
		databaseManager.setAutoSave(true);
	}
}

/**
 * @param {JSRecord<db:/geco/profit_centers>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"1BC2B2EB-7B29-4A9D-8942-722729B0E6BB"}
 */
function processAddApproverUserGroup(record) {
	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
	try {
		//Commentata aggunta nel gruppo, lo fa quando aggiunge in tabella approvers
//		user_groups.addUserGroup(record.user_owner_id, 11);
		//delivery manager
		user_groups.addUserGroup(record.user_owner_id, 16);

		//Se tipologia di PC è mercato.
		//TODO DF togliere sopra laggiunta come delivery manager e fare il controllo sulla tipologia del pc
//		if (record.profit_center_type_id == 5) {
//			//Responsabile di mercato
//			user_groups.addUserGroup(record.user_owner_id, 21);
//		}
	} catch (e) {
		throw new Error("- Impossibile assegnare il ruolo Approver al responsabile\n" + e['message']);
	}

}


/**
 * Cancella il vecchio user_owner_id dal gruppo delivery manager
 * @param {JSRecord<db:/geco/profit_centers>} record
 *
 * @properties={typeid:24,uuid:"A916C290-B646-4D7C-8302-92129DC67DE9"}
 * @AllowToRunInFind
 */
function processDeleteUserFromGroup(record) {
	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
//	/** @type {JSFoundSet<db:/geco/profit_centers>} */
//	var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');
	/** @type {JSFoundSet<db:/geco/owners_profit_centers>} */
	var owner_pc = databaseManager.getFoundSet('geco', 'owners_profit_centers');
	/** @type {JSDataSet} */
	var dataset = record.getChangedData()
	var totOPC = 0
	for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
		if (dataset.getValue(i, 1) == 'user_owner_id') {
			var oldOwner = dataset.getValue(i, 2);
			
			if (owner_pc.find()){
				owner_pc.user_id = oldOwner;
				owner_pc.profit_center_id = '!' + record.profit_center_id;
				totOPC = owner_pc.search();	
			}
			//se non è responsabile di altri pc gli tolgo il ruolo di responsabile mercato e delivery manager
			if (totOPC == 0) {
				user_groups.deleteUserGroup(oldOwner, [21,16]);
			}
			else {
				if (owner_pc.find()){
					owner_pc.user_id = oldOwner;
					owner_pc.profit_center_id = '!' + record.profit_center_id;
					owner_pc.owners_markets_to_profit_centers.profit_center_type_id = 5;
					totOPC = owner_pc.search();
				}
				//non è responsabile di altri mercati
				if (totOPC == 0) {
					user_groups.deleteUserGroup(oldOwner, [21]);
				}
			}		
			
//			if (profit_centers.find()) {
//				profit_centers.user_owner_id = oldOwner;
//				profit_centers.profit_center_id = '!' + record.profit_center_id;
//				totPC = profit_centers.search();
//				if (totPC == 0) {
//					application.output('sto per cancellare dal gruppo 16,21');
//					user_groups.deleteUserGroup(oldOwner, [16, 21]);
//				}
//			}
//
//			if (totPC > 0) {
//				//se è responsabile di altri PC di tipo mercato
//				if (profit_centers.find()) {
//					profit_centers.user_owner_id = oldOwner;
//					profit_centers.profit_center_id = '!' + record.profit_center_id;
//					profit_centers.profit_center_type_id = 5;
//					if (profit_centers.search() == 0) {
//						application.output('sto per cancellare solo dal gruppo 21');
//						user_groups.deleteUserGroup(oldOwner, [21]);
//					}
//				}
//			}
//			break;
		}
		else if (dataset.getValue(i, 1) == 'profit_center_type_id' && dataset.getValue(i, 2) == 5){
			if (owner_pc.find()){
				owner_pc.user_id = record.user_owner_id;
				owner_pc.profit_center_id = '!' + record.profit_center_id;
				owner_pc.owners_markets_to_profit_centers.profit_center_type_id = 5;
				totOPC = owner_pc.search();
			}
			//non è responsabile di altri mercati
			if (totOPC == 0) {
				user_groups.deleteUserGroup(record.user_owner_id, [21]);
			}
		}
		else if (dataset.getValue(i, 1) == 'profit_center_type_id' && dataset.getValue(i, 3) == 5){
			if (owner_pc.find()){
				owner_pc.user_id = record.user_owner_id;
				owner_pc.profit_center_id = '!' + record.profit_center_id;
				owner_pc.owners_markets_to_profit_centers.profit_center_type_id = 5;
				totOPC = owner_pc.search();
			}
			//non è responsabile di altri mercati
			if (totOPC == 0) {
				user_groups.addUserGroup(record.user_owner_id, 21);
			}
		}
	}
}

/**
 * Inserisce/cancella il record user_id profit_center_id nella tabella owner_profit_center
 * @param {JSRecord<db:/geco/profit_centers>} record
 *
 * @properties={typeid:24,uuid:"4BE08781-33FD-43C5-AA6E-1CF6D9909A8D"}
 * @AllowToRunInFind
 */
function processAddOwnerProfitCenter(record) {
	/** @type {JSDataSet} */
	var dataset = record.getChangedData();
	/** @type {JSFoundSet<db:/geco/owners_profit_centers>} */
	var ownerProfitCenter = databaseManager.getFoundSet('geco', 'owners_profit_centers');

	for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
		if (dataset.getValue(i, 1) == 'user_owner_id') {
			if (dataset.getValue(i, 2) == null) {
				//nuovo PC (perchè valore vecchio non esiste)

				ownerProfitCenter.addOwnerProfitCenter(record.user_owner_id, record.profit_center_id);
			} else {
				//sto modificando un record esistente
				//cancello quello vecchio, inserisco quello nuovo
				ownerProfitCenter.deleteOwnerProfitCenter(dataset.getValue(i, 2), record.profit_center_id);
				ownerProfitCenter.addOwnerProfitCenter(record.user_owner_id, record.profit_center_id)
			}
		}
	}
}

///**
// * @param {JSRecord<db:/geco/profit_centers>} record
// * @throws {Error}
// * @properties={typeid:24,uuid:"FA7F2D59-18CE-4202-81D1-6732A40BE09F"}
// */
//function processAddNewProfitCenterApprover(record) {
//	//se ho cambiato il campo Responsabile centro profitto
//	var dataset = record.getChangedData()
//	for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
//		//  se nel dataset il campo precedente user_owner_id è stato cambiato
//		if (dataset.getValue(i, 1) == 'user_owner_id' && dataset.getValue(i, 2) != dataset.getValue(i, 3))
//		/** @type {JSFoundSet<db:/geco/approvers>} */
//			var user_approver = record.profit_centers_to_approvers;
//	}
//
//	/** @type {JSFoundSet<db:/geco/approvers>} */
//	var approvers = databaseManager.getFoundSet('geco', 'approvers');
//	/** @type {JSFoundSet<db:/geco/user_groups>} */
//	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
//
//	try {
//		approvers.addApprover('profit_centers', record.profit_center_id, record.user_owner_id);
//		user_groups.addUserGroup(record.user_owner_id, 11);
//	} catch (e) {
//		throw new Error("- Impossibile determinare l'approvatore di default\n" + e['message']);
//	} finally {
//		databaseManager.setAutoSave(true);
//	}
//}

