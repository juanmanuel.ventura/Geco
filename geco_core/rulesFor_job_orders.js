/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @properties={typeid:24,uuid:"397379ED-2CAA-4420-B912-C4E8679C5712"}
 */
function validateJobOrderTitle(record) {
	if (globals.isEmpty(record.job_order_title)) {
		throw new Error('- Il campo Titolo è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @properties={typeid:24,uuid:"EEC4E839-DA0C-4606-877E-9096B3560594"}
 */
function validateJobOrderType(record) {
	if (globals.isEmpty(record.job_order_type)) {
		throw new Error('- Il campo Tipo è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @properties={typeid:24,uuid:"C08CFDFE-8E92-4BC6-AE59-38223BB4B113"}
 */
function validateRDC(record) {
	if (globals.isEmpty(record.user_owner_id)) {
		throw new Error('- Il campo RDC è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @properties={typeid:24,uuid:"2E168B10-7E0B-4E9F-B5E7-36A5314FA560"}
 * @AllowToRunInFind
 */
function validateExternalCode(record) {
	if (globals.isEmpty(record.external_code_navision)) {
		throw new Error('- Il campo External Code è obbligatorio');
	}
	//	while (record.external_code_navision.search(' ')>0) {
	//		record.external_code_navision = record.external_code_navision.replace(' ', '');
	//	}
	record.external_code_navision = globals.trimString(record.external_code_navision);
	//record.external_code_navision = record.external_code_navision.replace(' ', '');
	application.output('-' + record.external_code_navision + '-');
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @properties={typeid:24,uuid:"D9B9BDF6-FCCC-4388-A8AB-CAE68FC14149"}
 * @AllowToRunInFind
 */
function validateOrderCode(record) {
	if (record.job_order_type != 0 && globals.isEmpty(record.customer_order_code)) {
		throw new Error('- Il campo B.O. è obbligatorio');
	}
	if (!globals.isEmpty(record.customer_order_code)) {
		record.customer_order_code = globals.trimString(record.customer_order_code);
		if (isNaN(record.customer_order_code))
			throw new Error('- Il campo B.O. non è valido, inserire un numero');
	}
	//record.customer_order_code = record.customer_order_code.replace(' ', '');
	application.output('-' + record.customer_order_code + '-');
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @properties={typeid:24,uuid:"70E6662B-5D32-478F-811F-CD09EBFFF170"}
 */
function validateProfitCenter(record) {
	if (globals.isEmpty(record.profit_center_id)) {
		throw new Error('- Il campo Profit Center è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @properties={typeid:24,uuid:"8AFD1E49-9E9D-4461-9C01-E4BD362BCCF5"}
 */
function validateCustomer(record) {
	if (globals.isEmpty(record.company_id)) {
		throw new Error('- Il campo Cliente è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @AllowToRunInFind *
 * @properties={typeid:24,uuid:"C3E86E17-64EA-4F52-9389-A5D4D65E19C0"}
 */
function validateUniqueExternalCode(record) {
	if (!globals.isEmpty(record.external_code_navision)) {
		var code = record.external_code_navision;
		while (record.external_code_navision.search(' ') > 0) {
			code = record.external_code_navision.replace(' ', '');
		}
		/** @type {JSFoundSet<db:/geco/job_orders>} */
		var job_orders = databaseManager.getFoundSet('geco', 'job_orders');
		if (job_orders.find()) {
			job_orders.external_code_navision = '#' + code;
			job_orders.job_order_id = '!' + record.job_order_id;
			if (job_orders.search() > 0) throw new Error('- Esiste già una commessa con lo stesso codice Navision');
		}
	}
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @properties={typeid:24,uuid:"9D97ADDF-F56E-4DC6-8498-591BEA83ED67"}
 */
function validateJobOrderTypeAndBillable(record) {
//	if (!globals.isEmpty(record.job_order_type)) {
//		if (record.is_billable == 0 && record.job_order_type != 4) 
//			throw new Error('- Una commessa di tipo ' + record.job_orders_to_job_orders_types.job_order_type_description + ' deve essere Billable', 'OK');
//	}
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @properties={typeid:24,uuid:"5FDF0D23-CF78-4CE9-8E99-ADED0AE6C068"}
 * @AllowToRunInFind
 */
function processAllowOwnerToLog(record) {
	/** @type {JSFoundSet<db:/geco/users_job_orders>} */
	var users_job_orders = databaseManager.getFoundSet('geco', 'users_job_orders');

	// check the user existence
	if (users_job_orders.find()) {
		users_job_orders.user_id = record.user_owner_id;
		users_job_orders.job_order_id = record.job_order_id;

		// create a new entry if the user if not present
		if (users_job_orders.search() == 0) {
			users_job_orders.newRecord();
			users_job_orders.user_id = record.user_owner_id;
			users_job_orders.job_order_id = record.job_order_id;
			databaseManager.saveData(users_job_orders);
		}
	}
}

/**
 * solo in insert
 * @param {JSRecord<db:/geco/job_orders>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"86A81D16-61BA-4339-860D-3937585BB1E2"}
 */
function processAddDefaultApprover(record) {
	/** @type {JSFoundSet<db:/geco/approvers>} */
	var approvers = databaseManager.getFoundSet('geco', 'approvers');
	try {
		approvers.addApprover('job_orders', record.job_order_id, record.user_owner_id, record.date_approver_from);
//		approvers.addApprover('job_orders', record.job_order_id, record.user_owner_id, record.date_approver_from?record.date_approver_from:new Date());
		//add user to groups approvers
		//		application.output('JOB_ORDERS processAddDefaultApprover chiama user_groups.addUserGroup ');
		//		user_groups.addUserGroup(record.user_owner_id, 11);
	} catch (e) {
		throw new Error("- Impossibile determinare l'approvatore di default\n" + e['message']);
	}
}

/**
 * deactivation of the job order, the fields is_loggable and is_to_close are set to 0
 *
 * @param {JSRecord<db:/geco/job_orders>} record
 * @properties={typeid:24,uuid:"7147E102-F80F-4788-8DD7-EE2C1F79B7CE"}
 */
function processJobOrderEnable(record) {
	//disabilita la commessa
	if (globals.isEmpty(record.is_enabled) || record.is_enabled == 0) {
		record.is_loggable = 0;
		record.is_to_close = 0;
	}
}

/**
 * request the closure of job order, the field is_loggable is set to 0
 *
 * @param {JSRecord<db:/geco/job_orders>} record
 * @properties={typeid:24,uuid:"A9C2C992-4C37-4BDA-9E38-63BB763EEE33"}
 */
function processJobOrderClose(record) {
	//richiesta chiusura della commessa
	if (globals.isEmpty(record.is_to_close) || record.is_to_close == 1) {
		record.is_loggable = 0;
	}
}

/**
 * Aggiunge il nuovo rdc al gruppo
 * @param {JSRecord<db:/geco/job_orders>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"F3A617A7-1526-4798-9B4A-8F2DF60C999F"}
 */
function processAddApproverUserGroup(record) {
	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
	try {
		user_groups.addUserGroup(record.user_owner_id, 2);
	} catch (e) {
		throw new Error("- Impossibile assegnare il ruolo Approver al responsabile\n" + e['message']);
	}
}

/**
 *
 * @param {JSRecord<db:/geco/job_orders>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"9B0CC22F-D3BE-420D-BF6F-6D0384BF78B2"}
 * @AllowToRunInFind
 */
function processAddNewRDCApprover(record) {
	//TODO Daniela da disbilitare in locale
	// se ho cambiato il campo RDC
	var dataset = record.getChangedData()
	for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
		//  se nel dataset il campo precedente user_owner_id è stato cambiato
		if (dataset.getValue(i, 1) == 'user_owner_id' && dataset.getValue(i, 2) != dataset.getValue(i, 3))
		/** @type {JSFoundSet<db:/geco/approvers>} */
			var user_approver = record.job_orders_to_approvers;
		if (user_approver.find()) {
			user_approver.user_approver_id = record.user_owner_id;

			// se non esiste tra gli approvatori lo aggiungo,altrimenti lascio come è
			if (user_approver.search() == 0) {
				//					user_approver.newRecord();
				//					user_approver.user_approver_id = record.user_owner_id;
				//					user_approver.user_approver_from = new Date();
				try {
					user_approver.addApprover('job_orders', record.job_order_id, record.user_owner_id, record.date_approver_from);
//					user_approver.addApprover('job_orders', record.job_order_id, record.user_owner_id, record.date_approver_from?record.date_approver_from:new Date());
					//add user to groups approvers
					/** @type {JSFoundSet<db:/geco/user_groups>} */
					var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
					user_groups.addUserGroup(record.user_owner_id, 11);
					user_approver.loadAllRecords();
				} catch (e) {
					throw new Error("- Impossibile aggiungere il nuovo responsabile di commessa come nuovo approvatore\n" + e['message']);
				}
			}
		}

		var dateStart = user_approver.user_approver_from;
		/** @type {String} */
		var month = '' + (dateStart.getMonth() + 1);
		var dateStr = dateStart.getFullYear() + '-' + (month.length > 1 ? month : '0' + month) + '-' + dateStart.getDate();

		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log = databaseManager.getFoundSet('geco', 'events_log');

		if (events_log.find()) {
			//'>=01/01/2001|MM/dd/yyyy'
			events_log.event_log_date = '>=' + dateStr + '|yyyy-MM-dd';
			events_log.event_log_status = '1';
			events_log.events_log_to_events.approver_figure = '3';
			events_log.job_order_id = record.job_order_id
			if (events_log.search() > 0) {
				for (var index = 1; index <= events_log.getSize(); index++) {
					var rec = events_log.getRecord(index);
					application.output('prima ' + rec.user_approver_id + '     ' + rec.event_log_date);
					events_log.setNewApprover(rec, record.user_owner_id);

				}
			}

		}
	}

	/** @type {JSFoundSet<db:/geco/approvers>} */
	//	var approvers = databaseManager.getFoundSet('geco', 'approvers');
	/** @type {JSFoundSet<db:/geco/user_groups>} */
	//	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
	//	try {
	//		approvers.addApprover('job_orders', record.job_order_id, record.user_owner_id);
	//		//add user to groups approvers
	//		user_groups.addUserGroup(record.user_owner_id, 11);
	//	} catch (e) {
	//		throw new Error("- Impossibile determinare l'approvatore di default\n" + e['message']);
	//	}
}

/**
 * Cancella il vecchio user_owner_id dal gruppo rdc
 * @param {JSRecord<db:/geco/job_orders>} record
 *
 * @properties={typeid:24,uuid:"7602993D-4A6E-41C5-8E31-600BF9B726F9"}
 * @AllowToRunInFind
 */
function processDeleteUserFromGroup(record) {
	/** @type {JSDataSet} */
	var dataset = record.getChangedData()
	for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
		if (dataset.getValue(i, 1) == 'user_owner_id') {
			var oldOwner = dataset.getValue(i, 2);
			/** @type {JSFoundSet<db:/geco/user_groups>} */
			var user_groups = databaseManager.getFoundSet('geco', 'user_groups');

			/** @type {JSFoundSet<db:/geco/job_orders>} */
			var job_orders = databaseManager.getFoundSet('geco', 'job_orders');
			if (job_orders.find()) {
				job_orders.user_owner_id = oldOwner;
				job_orders.job_order_id = '!' + record.job_order_id;
				if (job_orders.search() == 0) {
					user_groups.deleteUserGroup(oldOwner, [2]);
				}
			}
			break;
		}
	}
}

/**
 * se ha cambiato il codice BO associato inserisce la pianificazione
 * @param {JSRecord<db:/geco/job_orders>} record
 * @properties={typeid:24,uuid:"6A931E57-3DD1-4F50-B7E5-7AA82C97CF7A"}
 * @AllowToRunInFind
 */
function processNewBoCode(record) {
	try {
		var changeBO = false
		var boIdOld = null;
		var dataset = record.getChangedData();
		/** @type {JSFoundSet<db:/geco/business_opportunities>} */
		var bo_old = databaseManager.getFoundSet('geco', 'business_opportunities');

		//application.output('dataset ' + dataset);
		for (var idx = 1; idx <= dataset.getMaxRowIndex(); idx++) {
			//  se nel dataset il campo precedente user_owner_id è stato cambiato
			application.output('cambiato ' + dataset.getValue(idx, 1) + ' vecchio valore ' + dataset.getValue(idx, 2) + ' nuovo valore ' + dataset.getValue(idx, 3));
			if (dataset.getValue(idx, 1) == 'customer_order_code' && dataset.getValue(idx, 2) != dataset.getValue(idx, 3)) {
				application.output('customer_order_code cambiato vecchio valore ' + dataset.getValue(idx, 2) + ' nuovo valore ' + dataset.getValue(idx, 3));
				changeBO = true;
				boIdOld = dataset.getValue(idx, 2);
				break;
			}
		}
		//se il record non è nuovo, se ha camiato la BO e se non esiste pianificazione la crea dalla nuova BO
		if (!record.isNew() && changeBO == true && record.job_orders_to_jo_details.getSize() == 0 && record.job_orders_to_jo_planning.getSize() == 0) {
			var result = 0;
			var boNum = 0;
			/** @type {JSFoundSet<db:/geco/business_opportunities>} */
			var business_opportunity = databaseManager.getFoundSet('geco', 'business_opportunities');
			if (business_opportunity.find()) {
				business_opportunity.bo_number = record.customer_order_code;
				boNum = business_opportunity.search();
			}

			if (bo_old.find()) {
				bo_old.bo_number = boIdOld;
				bo_old.search();
			}
			
			//DF chiamare metodo di entity che skippa le rules
			//bo_old.updateRefJofromJo(bo_old.getSelectedRecord(),'updatereferenceTOJo',null,null);
			bo_old.job_order_id = null;
			bo_old.cod_commessa = null;
			bo_old.action = 'updateReferenceTOJo';

			/** @type {JSFoundSet<db:/geco/bo_details>} */
			var bo_details = databaseManager.getFoundSet('geco', 'bo_details');
			if (bo_details.find()) {
				bo_details.bo_details_to_bo.bo_number = record.customer_order_code;
				result = bo_details.search();
			}
			if (result == 0) {
				application.output('Esito ricerca Dettagli BO ' + result);
			}

			/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
			var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
			bo_actual_date.loadRecords();
			var actual_date = bo_actual_date.actual_date;
			var isOpenToActual = bo_actual_date.is_open_to_actual;

			if (boNum == 1) {
				application.output('date bo ' + business_opportunity.start_date + ' fine ' + business_opportunity.end_date)
				if (record.valid_from == null) record.valid_from = business_opportunity.start_date;
				if (record.valid_to == null || record.valid_to < business_opportunity.end_date) record.valid_to = business_opportunity.end_date;
				application.output('date commessa ' + record.valid_from + ' fine ' + record.valid_to)
				record.bo_id = business_opportunity.bo_id;
				//		record.cost = business_opportunity.cost;
				//		record.profit = business_opportunity.profit;
				record.lob_id = business_opportunity.lob_id;
				record.tow_id = business_opportunity.tow_id;
				record.job_order_year = business_opportunity.bo_year;
				//DF chiamare metodo di entity che skippa le rules
				business_opportunity.job_order_id = record.job_order_id;
				business_opportunity.cod_commessa = record.external_code_navision;
				bo_old.action = 'updateReferenceTOJo';
				application.output('bo_details trovati ' + result)
				for (var index = 1; index <= bo_details.getSize(); index++) {
					//crea il dettaglio relativo alla BO
					var rec = bo_details.getRecord(index);
					application.output(rec.bo_details_id + ' - ' + rec.bo_id);
					application.output('creo record jo_details')
					record.job_orders_to_jo_details.newRecord();
					record.job_orders_to_jo_details.bo_id = rec.bo_id;
					record.job_orders_to_jo_details.bo_details_id = rec.bo_details_id;
					record.job_orders_to_jo_details.cost_amount = rec.cost_amount;
					record.job_orders_to_jo_details.days_import = rec.days_import;
					record.job_orders_to_jo_details.days_number = rec.days_number;
					record.job_orders_to_jo_details.description = rec.figure;
					record.job_orders_to_jo_details.jo_id = record.job_order_id;
					record.job_orders_to_jo_details.profit_cost_type_id = rec.profit_cost_type_id;
					record.job_orders_to_jo_details.real_figure = rec.real_figure;//
					record.job_orders_to_jo_details.real_tk_supplier = rec.real_tk_supplier;
					record.job_orders_to_jo_details.real_tm_figure = rec.real_tm_figure;//
					record.job_orders_to_jo_details.return_amount = rec.return_amount;
					record.job_orders_to_jo_details.standard_figure = rec.standard_figure;
					record.job_orders_to_jo_details.standard_import = rec.standard_import;
					record.job_orders_to_jo_details.total_amount = rec.total_amount;
					//user_id per tipologia se TM o personale
					var uid = (rec.real_figure != null) ? rec.real_figure : rec.real_tm_figure;
					if (rec.bo_details_to_bo_planning != null && rec.bo_details_to_bo_planning.getSize() > 0) {
						//crea pianificazione relativa al dettaglio
						var totDays = 0;
						var totAmount = 0;
						var totCost = 0;
						var totReturn = 0;
						for (var i = 1; i <= rec.bo_details_to_bo_planning.getSize(); i++) {
							var rec_plain = rec.bo_details_to_bo_planning.getRecord(i);
							application.output(rec_plain.bo_planning_id + ' - ' + rec.bo_details_id + ' - ' + rec.bo_id);
							application.output('creo record jo_plain')
							var dataPlain = new Date(rec_plain.bo_year, rec_plain.bo_month, 0)
							application.output('Data Piano ' + dataPlain);
							if (dataPlain < actual_date || (dataPlain == actual_date && isOpenToActual == 0)) {
								//sommo gli importi del piano
								totDays = scopes.globals.roundNumberWithDecimal( (totDays + rec_plain.days_number), 1);
								totAmount = scopes.globals.roundNumberWithDecimal( (totAmount + rec_plain.total_amount), 2);
								totCost = scopes.globals.roundNumberWithDecimal( (totCost + rec_plain.cost_amount), 2);
								totReturn = scopes.globals.roundNumberWithDecimal( (totReturn + rec_plain.return_amount), 2);

							} else {
								//inserisco il piano
								record.job_orders_to_jo_details.jo_details_to_jo_planning.newRecord();
								record.job_orders_to_jo_details.jo_details_to_jo_planning.days_import = rec_plain.days_import;
								record.job_orders_to_jo_details.jo_details_to_jo_planning.bo_details_id = rec_plain.bo_details_id;
								record.job_orders_to_jo_details.jo_details_to_jo_planning.bo_id = rec_plain.bo_id;
								record.job_orders_to_jo_details.jo_details_to_jo_planning.jo_details_id = record.job_orders_to_jo_details.jo_details_id;
								record.job_orders_to_jo_details.jo_details_to_jo_planning.jo_id = record.job_order_id;
								record.job_orders_to_jo_details.jo_details_to_jo_planning.jo_month = rec_plain.bo_month;
								record.job_orders_to_jo_details.jo_details_to_jo_planning.jo_year = rec_plain.bo_year;
								record.job_orders_to_jo_details.jo_details_to_jo_planning.user_id = uid;
								if (totCost > 0 || totReturn > 0) {
									record.job_orders_to_jo_details.jo_details_to_jo_planning.cost_amount = scopes.globals.roundNumberWithDecimal( (totCost + rec_plain.cost_amount), 2);
									record.job_orders_to_jo_details.jo_details_to_jo_planning.days_number = scopes.globals.roundNumberWithDecimal( (totDays + rec_plain.days_number), 1);
									record.job_orders_to_jo_details.jo_details_to_jo_planning.return_amount = scopes.globals.roundNumberWithDecimal( (totReturn + rec_plain.return_amount), 2);
									record.job_orders_to_jo_details.jo_details_to_jo_planning.total_amount = scopes.globals.roundNumberWithDecimal( (totAmount + rec_plain.total_amount), 2);
								} else {
									record.job_orders_to_jo_details.jo_details_to_jo_planning.cost_amount = rec_plain.cost_amount;
									record.job_orders_to_jo_details.jo_details_to_jo_planning.days_number = rec_plain.days_number;
									record.job_orders_to_jo_details.jo_details_to_jo_planning.return_amount = rec_plain.return_amount;
									record.job_orders_to_jo_details.jo_details_to_jo_planning.total_amount = rec_plain.total_amount;
								}
								totDays = 0;
								totAmount = 0;
								totCost = 0;
								totReturn = 0;
							}
						}
					}

				}

			} else {
				application.output('BO non trovata');
			}
		}
	} catch (e) {
		throw new Error("- Errore nel cambio BO associata: " + e['message']);
	}
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"71805074-57B1-4DDC-B00F-7ADC5EC6B8DE"}
 */
function processDate(record) {
	if (record.valid_from != null)
		record.valid_from = new Date(record.valid_from.getFullYear(), record.valid_from.getMonth(), 1);
	if (record.valid_to != null)
		record.valid_to = new Date(record.valid_to.getFullYear(), record.valid_to.getMonth() + 1, 0);
	application.output('start date ' + record.valid_from + ' record.end_date ' + record.valid_to)
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"2127E9F5-F0C9-4C05-9D28-1E521672664D"}
 */
function validateDate(record) {
	if (!globals.hasRole('Controllers')) {
		var dateFrom = null;
		var dateTo = null;
		if (record.valid_from != null) dateFrom = new Date(record.valid_from.getFullYear(), record.valid_from.getMonth(), 1);
		if (record.valid_to != null) dateTo = new Date(record.valid_to.getFullYear(), record.valid_to.getMonth() + 1, 0);
		application.output('---------record start date ' + dateTo + '\n---------record.end_date ' + dateFrom);
		for (var index = 1; index <= record.job_orders_to_jo_planning.getSize(); index++) {
			var recPl = record.job_orders_to_jo_planning.getRecord(index);
			application.output(new Date(recPl.jo_year, recPl.jo_month - 1, 1));
			var dateJoPl = new Date(recPl.jo_year, recPl.jo_month - 1, 1);
			if (dateFrom != null && dateJoPl < dateFrom) {
				throw new Error('- Non è possibile modificare la data di inizio, esistono piani relativi a mesi precedenti alla data inserita');
			}
			if (dateTo != null && dateTo < dateJoPl) {
				throw new Error('- Non è possibile modificare la data di fine, esistono piani relativi a mesi successivi alla data inserita');
			}
		}
	}
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"581FA989-92CD-4A51-A7DB-A96C275E4C19"}
 * @AllowToRunInFind
 */
function processChangeType(record) {
	application.output(globals.messageLog + 'START processChangeType ', LOGGINGLEVEL.INFO);
	var dataset = record.getChangedData();
	var changeType = false;
	for (var idxChange = 1; idxChange <= dataset.getMaxRowIndex(); idxChange++) {
		//  se nel dataset il campo precedente user_owner_id è stato cambiato
		application.output('cambiato ' + dataset.getValue(idxChange, 1) + ' vecchio valore ' + dataset.getValue(idxChange, 2) + ' nuovo valore ' + dataset.getValue(idxChange, 3));
		//se cambia il tipo commessa ed era getionale
		if (dataset.getValue(idxChange, 1) == 'job_order_type' && dataset.getValue(idxChange, 2) != dataset.getValue(idxChange, 3) && dataset.getValue(idxChange, 2) == 4) {
			application.output('job_order_type cambiato vecchio valore ' + dataset.getValue(idxChange, 2) + ' nuovo valore ' + dataset.getValue(idxChange, 3));
			changeType = true;
			break;
		}
	}
	application.output('modificato da gestionale ad altro  ' + changeType);
	if (changeType) {
		try {
			//cerco BO
			var boNum = 0;
			/** @type {JSFoundSet<db:/geco/business_opportunities>} */
			var business_opportunity = databaseManager.getFoundSet('geco', 'business_opportunities');
			if (business_opportunity.find()) {
				business_opportunity.bo_number = record.customer_order_code;
				boNum = business_opportunity.search();
			}
			/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
			var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
			bo_actual_date.loadRecords();
			var dateActual = bo_actual_date.getRecord(1).actual_date;
			//		var monthActual = bo_actual_date.getRecord(1).actual_month;
			//		var yearActual = bo_actual_date.getRecord(1).actual_year;
			var isOpenActual = bo_actual_date.getRecord(1).is_open_to_actual;

			/** @type {Number[]} */
			var arrayStaus = [0, 1, 6, 7, 9];

			if (boNum == 1) {
				/** @type {JSFoundSet<db:/geco/jo_details>} */
				var jobDetails = record.job_orders_to_jo_details;

				/** @type {JSFoundSet<db:/geco/bo_planning>} */
				var listBoPlain = null;
				/** @type {JSFoundSet<db:/geco/jo_planning>} */
				var listJobPlain = null;

				var recBoPlain = null;
				var dateBoPl = null;
				var pl = 0;

				var totJOPlain = 0;
				var totAmountBOPl = 0;
				var totReturnBOPl = 0;
				var totCostBOPl = 0;
				var totDayNumBOPl = 0;

				/** @type {JSRecord<db:/geco/bo_details>[]} */
				var detailsNew = [];
				/** @type {JSRecord<db:/geco/bo_details>[]} */
				var detailsUpdate = [];
				/** @type {JSRecord<db:/geco/jo_details>[]} */
				var detailsUpdateJO = [];

				var bo_details = business_opportunity.bo_to_bo_details;

				for (var index = 1; index <= bo_details.getSize(); index++) {
					//record
					var recordBoDet = bo_details.getRecord(index);
					//pianificazione del dettaglio
					application.output('------\nrecord dettaglio ' + recordBoDet)
					var tot = 0;
					//cerco il dettaglio della Commessa
					jobDetails.loadRecords();
					application.output('--------\njodetails ' + jobDetails)
					application.output('cerco tipo ' + recordBoDet.profit_cost_type_id + ' real_figure ' + recordBoDet.real_figure + ' real_tk_supplier ' + recordBoDet.real_tk_supplier + ' real_tm_figure ' + recordBoDet.real_tm_figure + ' figure ' + recordBoDet.figure + ' days_import ' + recordBoDet.days_import)
					if (jobDetails.find()) {
						jobDetails.jo_id = record.job_order_id;
						jobDetails.profit_cost_type_id = recordBoDet.profit_cost_type_id;
						if (recordBoDet.bo_details_to_profit_cost_types.profit_cost_acr == 'C') {
							jobDetails.real_figure = recordBoDet.real_figure;
							jobDetails.real_tk_supplier = recordBoDet.real_tk_supplier;
							jobDetails.real_tm_figure = recordBoDet.real_tm_figure;
							//jobDetails.days_import = recordBoDet.days_import;
							jobDetails.enrollment_number = recordBoDet.enrollment_number;
						} else {
							jobDetails.description = recordBoDet.figure;
						}
						tot = jobDetails.search();
					}
					if (tot == 1) {
						detailsUpdate.push(recordBoDet);
						detailsUpdateJO.push(jobDetails.getRecord(1))
					} else if (tot == 0) {
						detailsNew.push(recordBoDet);
					}
				}
				application.output('BO DETAILS TO UPDATE ' + detailsUpdate);
				application.output('JO DETAILS TO UPDATE ' + detailsUpdateJO);
				application.output('BO DETAILS TO NEW ' + detailsNew);
				application.output('--------------------------------------DETTAGLIO ESISTENTE----------------------------------------------');
				jobDetails.loadRecords();
				for (var idx = 0; idx < detailsUpdate.length; idx++) {
					/** @type {JSRecord<db:/geco/bo_details>} */
					var recBoDet = detailsUpdate[idx];
					/** @type {JSRecord<db:/geco/jo_details>} */
					var jobDetailsToUpdate = detailsUpdateJO[idx];
					application.output('DETTAGLIO BO ID ' + recBoDet.bo_details_id)
					application.output('###############dettagli trovati ' + tot + ':  ' + jobDetailsToUpdate)
					//if (tot == 1) {
					//esiste solo un dettaglio lo modifico
					application.output('-----------Esiste un dettaglio lo modifico ' + jobDetailsToUpdate.jo_details_id);
					jobDetailsToUpdate.cost_amount = jobDetailsToUpdate.cost_amount + recBoDet.cost_amount;
					jobDetailsToUpdate.return_amount = jobDetailsToUpdate.return_amount + recBoDet.return_amount;
					jobDetailsToUpdate.total_amount = jobDetailsToUpdate.total_amount + recBoDet.total_amount;
					jobDetailsToUpdate.days_number = jobDetailsToUpdate.days_number + recBoDet.days_number;

					//PIANIFICAZIONE MENSILE
					listBoPlain = recBoDet.bo_details_to_bo_planning;
					listJobPlain = jobDetailsToUpdate.jo_details_to_jo_planning;

					application.output('Lista piani size ' + listBoPlain.getSize());
					application.output('Dettaglio JO ' + jobDetailsToUpdate.jo_details_id)
					for (pl = 1; pl <= listBoPlain.getSize(); pl++) {
						application.output('*****Pianificazione mensile ' + pl);
						recBoPlain = listBoPlain.getRecord(pl);
						application.output('recBoPlain ' + recBoPlain.bo_month + ' ' + recBoPlain.bo_year)
						dateBoPl = new Date(recBoPlain.bo_year, recBoPlain.bo_month, 0);
						application.output('Data Piano ' + dateBoPl + ' recBoPlain.bo_year ' + recBoPlain.bo_year + ' recBoPlain.bo_month ' + recBoPlain.bo_month);
						application.output('Data actual ' + dateActual);
						if (dateBoPl < dateActual || (dateBoPl == dateActual && isOpenActual == 0)) {
							application.output('Piano minore della data actual o mese chiuso per actual');
							//piano precedente all'ultima data actual
							//sommo gli importi per poi copiarli nel primo piano utile
							totAmountBOPl = scopes.globals.roundNumberWithDecimal( (totAmountBOPl + recBoPlain.total_amount), 2);
							totReturnBOPl = scopes.globals.roundNumberWithDecimal( (totReturnBOPl + recBoPlain.return_amount), 2);
							totCostBOPl = scopes.globals.roundNumberWithDecimal( (totCostBOPl + recBoPlain.cost_amount), 2);
							totDayNumBOPl = scopes.globals.roundNumberWithDecimal( (totDayNumBOPl + recBoPlain.days_number), 1);
							application.output('somme totAmountBOPl ' + totAmountBOPl + ' totReturnBOPl ' + totReturnBOPl + ' totCostBOPl ' + totCostBOPl + ' totDayNumBOPl ' + totDayNumBOPl);
						} else {
							application.output('Data valida ');
							totAmountBOPl = scopes.globals.roundNumberWithDecimal( (totAmountBOPl + recBoPlain.total_amount), 2);
							totReturnBOPl = scopes.globals.roundNumberWithDecimal( (totReturnBOPl + recBoPlain.return_amount), 2);
							totCostBOPl = scopes.globals.roundNumberWithDecimal( (totCostBOPl + recBoPlain.cost_amount), 2);
							totDayNumBOPl = scopes.globals.roundNumberWithDecimal( (totDayNumBOPl + recBoPlain.days_number), 2);
							application.output('somme totAmountBOPl ' + totAmountBOPl + ' totReturnBOPl ' + totReturnBOPl + ' totCostBOPl ' + totCostBOPl + ' totDayNumBOPl ' + totDayNumBOPl);
							//piano bo dopo data actual
							//cercare la pianificazione eventualmente aggiungerla
							if (listJobPlain != null && listJobPlain.find()) {
								listJobPlain.jo_id = record.job_order_id;
								listJobPlain.jo_details_id = jobDetails.jo_details_id;
								listJobPlain.jo_month = recBoPlain.bo_month;
								listJobPlain.jo_year = recBoPlain.bo_year;
								//listJobPlain.is_actual = 0;
								//0 inserito da rdc, 1 confermato da rdc, 6 inserito da rpc, 7 respinto da rpc,9 rifiutato da controller
								//listJobPlain.status = '[0,1,6,7,9]'
								totJOPlain = listJobPlain.search();
							}
							if (totJOPlain == 0) {
								//non ho trovato il piano lo creo
								application.output('Nessun piano per il mese lo creo ')
								listJobPlain.newRecord();
								listJobPlain.jo_id = jobDetails.jo_id;
								listJobPlain.jo_details_id = jobDetails.jo_details_id
								listJobPlain.jo_month = recBoPlain.bo_month;
								listJobPlain.jo_year = recBoPlain.bo_year;
								listJobPlain.bo_id = recBoPlain.bo_id;
								listJobPlain.bo_details_id = recBoPlain.bo_details_id;
								listJobPlain.return_amount = totReturnBOPl;
								listJobPlain.cost_amount = totCostBOPl;
								listJobPlain.days_import = recBoPlain.days_import;
								listJobPlain.days_number = totDayNumBOPl;
								listJobPlain.total_amount = totAmountBOPl;
								listJobPlain.user_id = jobDetails.user_id;
								totAmountBOPl = 0;
								totReturnBOPl = 0;
								totCostBOPl = 0;
								totDayNumBOPl = 0;
							} else {
								//trovato lo modifico
								if (listJobPlain.is_actual == 1 || arrayStaus.indexOf(listJobPlain.status) != -1) {
									//il piano esiste ed è actual non lo modifico
									application.output('Piano trovato ACTUAL non posso modificarlo ')
								} else {
									application.output('Piano trovato lo modifico ')
									listJobPlain.return_amount = scopes.globals.roundNumberWithDecimal( (listJobPlain.return_amount + totReturnBOPl), 2);
									listJobPlain.cost_amount = scopes.globals.roundNumberWithDecimal( (listJobPlain.cost_amount + totCostBOPl), 2);
									listJobPlain.days_import = recBoPlain.days_import;
									listJobPlain.days_number = scopes.globals.roundNumberWithDecimal( (listJobPlain.days_number + totDayNumBOPl), 2);
									listJobPlain.total_amount = scopes.globals.roundNumberWithDecimal( (listJobPlain.total_amount + totAmountBOPl), 2);
									application.output('somme Amount ' + listJobPlain.total_amount + ' Return ' + totReturnBOPl + ' Costo ' + listJobPlain.cost_amount + ' NumGiorni ' + listJobPlain.days_number);
									totAmountBOPl = 0;
									totReturnBOPl = 0;
									totCostBOPl = 0;
									totDayNumBOPl = 0;
								}
							}//fine trovato piano mensile JO

						}
					}
				}// fine for record to update
				application.output('--------------------------------------NUOVO DETTAGLIO----------------------------------------------')
				for (var idxNew = 0; idxNew < detailsNew.length; idxNew++) {
					/** @type {JSRecord<db:/geco/bo_details>} */
					var recBoDetNew = detailsNew[idxNew];

					//non esiste nessun dettaglio lo creo
					application.output("-----------Non esiste il dettaglio lo creo ID DET BO " + recBoDetNew.bo_details_id);
					jobDetails.newRecord();
					jobDetails.profit_cost_type_id = recBoDetNew.profit_cost_type_id;
					jobDetails.bo_id = recBoDetNew.bo_id;
					jobDetails.bo_details_id = recBoDetNew.bo_details_id;
					jobDetails.jo_id = record.job_order_id;
					jobDetails.cost_amount = recBoDetNew.cost_amount;
					jobDetails.total_amount = recBoDetNew.total_amount;
					jobDetails.return_amount = recBoDetNew.return_amount;
					if (recBoDetNew.bo_details_to_profit_cost_types.profit_cost_acr == 'C') {
						jobDetails.real_figure = recBoDetNew.real_figure;
						jobDetails.real_tk_supplier = recBoDetNew.real_tk_supplier;
						jobDetails.real_tm_figure = recBoDetNew.real_tm_figure;
						jobDetails.description = recBoDetNew.figure;
						jobDetails.days_import = recBoDetNew.days_import;
						jobDetails.days_number = recBoDetNew.days_number;
						jobDetails.standard_figure = recBoDetNew.standard_figure;
						jobDetails.standard_import = recBoDetNew.standard_import;
						jobDetails.enrollment_number = recBoDetNew.enrollment_number;
					}
					databaseManager.saveData(jobDetails);
					application.output('Nuovo dettaglio JO ' + jobDetails.jo_details_id + ' commessa ' + jobDetails.jo_id)
					listBoPlain = recBoDetNew.bo_details_to_bo_planning;
					application.output('Lista piani size ' + listBoPlain.getSize())
					for (pl = 1; pl <= listBoPlain.getSize(); pl++) {
						application.output('*****Pianificazione mensile ' + pl);
						recBoPlain = listBoPlain.getRecord(pl);
						application.output('recBoPlain ' + recBoPlain.bo_month + ' ' + recBoPlain.bo_year)
						dateBoPl = new Date(recBoPlain.bo_year, recBoPlain.bo_month, 0);
						application.output('Data Piano ' + dateBoPl + ' recBoPlain.bo_year ' + recBoPlain.bo_year + ' recBoPlain.bo_month ' + recBoPlain.bo_month);
						application.output('Data actual ' + dateActual);
						if (dateBoPl < dateActual || (dateBoPl == dateActual && isOpenActual == 0)) {
							//piano precedente all'ultima data actual
							//sommo gli importi per poi copiarli nel primo piano utile
							application.output('Piano minore della data actual ');
							totAmountBOPl = scopes.globals.roundNumberWithDecimal( (totAmountBOPl + recBoPlain.total_amount), 2);
							totReturnBOPl = scopes.globals.roundNumberWithDecimal( (totReturnBOPl + recBoPlain.return_amount), 2);
							totCostBOPl = scopes.globals.roundNumberWithDecimal( (totCostBOPl + recBoPlain.cost_amount), 2);
							totDayNumBOPl = scopes.globals.roundNumberWithDecimal( (totDayNumBOPl + recBoPlain.days_number), 2);
							application.output('somme totAmountBOPl ' + totAmountBOPl + ' totReturnBOPl ' + totReturnBOPl + ' totCostBOPl ' + totCostBOPl + ' totDayNumBOPl ' + totDayNumBOPl);
						} else {
							application.output('Data valida ');
							totAmountBOPl = scopes.globals.roundNumberWithDecimal( (totAmountBOPl + recBoPlain.total_amount), 2);
							totReturnBOPl = scopes.globals.roundNumberWithDecimal( (totReturnBOPl + recBoPlain.return_amount), 2);
							totCostBOPl = scopes.globals.roundNumberWithDecimal( (totCostBOPl + recBoPlain.cost_amount), 2);
							totDayNumBOPl = scopes.globals.roundNumberWithDecimal( (totDayNumBOPl + recBoPlain.days_number), 2);
							application.output('somme totAmountBOPl ' + totAmountBOPl + ' totReturnBOPl ' + totReturnBOPl + ' totCostBOPl ' + totCostBOPl + ' totDayNumBOPl ' + totDayNumBOPl);

							//piano bo dopo data actual
							//cercare la pianificazione eventualmente aggiungerla
							//non ho trovato il piano lo creo
							application.output('Nuovo dettaglio JO ' + jobDetails.jo_details_id + ' commessa ' + jobDetails.jo_id)
							var idxJP = jobDetails.jo_details_to_jo_planning.newRecord();
							application.output('indice piano creato ' + idxJP)
							jobDetails.jo_details_to_jo_planning.jo_id = jobDetails.jo_id;
							jobDetails.jo_details_to_jo_planning.jo_details_id = jobDetails.jo_details_id
							jobDetails.jo_details_to_jo_planning.jo_month = recBoPlain.bo_month;
							jobDetails.jo_details_to_jo_planning.jo_year = recBoPlain.bo_year;
							jobDetails.jo_details_to_jo_planning.bo_id = recBoPlain.bo_id;
							jobDetails.jo_details_to_jo_planning.bo_details_id = recBoPlain.bo_details_id;
							jobDetails.jo_details_to_jo_planning.return_amount = totReturnBOPl;
							jobDetails.jo_details_to_jo_planning.cost_amount = totCostBOPl;
							jobDetails.jo_details_to_jo_planning.days_import = recBoPlain.days_import;
							jobDetails.jo_details_to_jo_planning.days_number = totDayNumBOPl;
							jobDetails.jo_details_to_jo_planning.total_amount = totAmountBOPl;
							jobDetails.jo_details_to_jo_planning.user_id = jobDetails.user_id;
							totAmountBOPl = 0;
							totReturnBOPl = 0;
							totCostBOPl = 0;
							totDayNumBOPl = 0;
						}
					}
				}//fine for New plain
				//DF chiamare metdo di entity che skippa le rules
//				business_opportunity.updateBoTypeFormJo(business_opportunity.getSelectedRecord(),'updateTypeBo',record.job_order_type);
				business_opportunity.is_editable = 0;
				business_opportunity.job_order_type_id = record.job_order_type;
				business_opportunity.action = 'updateReferenceTOJo';
				var event = new JSEvent();
				var excel = scopes.globals.getBusinessCase(event, business_opportunity.bo_id);
				/**@type {Array<byte>}*/
				var bytes = new Array();
				var dateReq = new Date();
				/** @type {String} */
				var monthStr = '' + (dateReq.getMonth() + 1);
				var dayStr = '' + dateReq.getDate()
				var dateReqStr = dateReq.getFullYear() + ( (monthStr.length > 1) ? monthStr : '0' + monthStr) + ( (dayStr.length > 1) ? dayStr : '0' + dayStr)
				var fileExcel = plugins.file.createFile('BC_' + business_opportunity.bo_code + '_' + dateReqStr + '.xls');
				var result = plugins.file.appendToTXTFile(fileExcel, excel);
				application.output(fileExcel.getBytes());
				bytes = fileExcel.getBytes();
				application.output(fileExcel.size());
				application.output('file excel ' + result);
				application.output(fileExcel.getContentType());

				if (!plugins.file.writeFile(fileExcel, bytes, 'application/vnd.ms-excel'))
					application.output('Failed to write the file.');
				application.output(fileExcel.getAbsolutePath());

				scopes.globals.insertBCtoBO(event, business_opportunity.bo_id, fileExcel);
				if (fileExcel.getName() != null);
				plugins.file.deleteFile(fileExcel.getName());
			}
			//copio dettagli
			//copio piani
		} catch (e) {
			application.output(globals.messageLog + 'ERROR processChangeType ' + e.message, LOGGINGLEVEL.ERROR);
			throw new Error('- Non è possibile copiare i dati della pianificazione della BO nella commessa');
		}
	}
	application.output(globals.messageLog + 'STOP processChangeType ', LOGGINGLEVEL.INFO);
}

