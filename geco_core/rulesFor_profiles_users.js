/**
 * PER ORA DISABILITATA
 * JStaffa Valida inserimento matricola per il profilo selezionato
 * @param {JSRecord<db:/geco/profiles_users>} record
 * @properties={typeid:24,uuid:"FC2B399F-F44F-459F-932B-D5251B380C22"}
 */
function validateProfileUserId(record) {
//	if (globals.isEmpty(record.user_id)) {
//		throw new Error('- Il campo Matricola è obbligatorio');
//	}
}

/**
 * JStaffa Valida INSERIMENTO data di inizio della matricola per il profilo selezionato
 * @param {JSRecord<db:/geco/profiles_users>} record
 * @properties={typeid:24,uuid:"8550876E-7B78-4656-8C2C-9972D5581797"}
 * @AllowToRunInFind
 */
function validateProfileUserStartDate(record) {
	var errorMsg = '';
	var tot = 0;
	//valida che ci sia la data
	if (globals.isEmpty(record.profile_user_start_date))
		errorMsg = errorMsg + '- Il campo Data inizio validità è obbligatorio\n';
	else {
		var dateStr = globals.getDateStringFormat(record.profile_user_start_date);
		//valida che non sia una data già usata
		/** @type {JSFoundSet<db:/geco/profiles_users>} */
		var profiles_users = databaseManager.getFoundSet('geco', 'profiles_users');
		if (profiles_users.find()) {
			profiles_users.profile_user_id = '!' + record.profile_user_id; //diverso dal record che arriva
			profiles_users.profile_user_start_date = dateStr + ' |yyyy-MM-dd';
			profiles_users.user_id = record.user_id; //importantissimo! ovviamente per l'utente selezionato

			tot = profiles_users.search();
			if (tot > 0)
				errorMsg = errorMsg + '- Esiste già un profilo nella stessa Data, modificare il record esistente';
		}
		profiles_users.loadRecords(); //ricarico i record
		//		rientro in find; mi serve la data maggiore per l'utente selezionato
		if (profiles_users.find()) {
			profiles_users.profile_user_id = '!' + record.profile_user_id; //diverso dal record che arriva
			profiles_users.user_id = record.user_id; //importantissimo! ovviamente per l'utente selezionato

			tot = profiles_users.search();
		}
		//valida che la data inserita sia maggiore dell'ultima
		if (record.profile_user_start_date < profiles_users.max_profile_user_start_date)
			errorMsg = !globals.isEmpty(errorMsg) ? errorMsg + '\n- Il campo Data inizio validità del nuovo record deve essere superiore rispetto all\'ultima Data inizio validità esistente' : '- Il campo Data inizio validità del nuovo record deve essere superiore rispetto all\'ultima Data inizio validità esistente';
	}
	if (!globals.isEmpty(errorMsg)) throw new Error(errorMsg);
}

/**
 * JStaffa Valida inserimento profilo per l'utente selezionato
 * @param {JSRecord<db:/geco/profiles_users>} record
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"B6D9C704-E5F1-448D-8F60-1EC1773E32AD"}
 */
function validateProfileForUser(record) {
	if (!globals.isEmpty(record.user_id)) {
		if (globals.isEmpty(record.profile_id)) 
			throw new Error('- Il profilo è obbligatorio per l\'associazione Utente-Profilo Professionale');
	}
}
