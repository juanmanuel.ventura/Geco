/**
 * @param {JSRecord<db:/geco/contract_types>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"A23163A7-AA31-44F1-BAC5-40A429ACB904"}
 */
function validateRequireContractType(record) {
	if (globals.isEmpty(record.contract_type_name)) throw new Error('- Il campo Tipo contratto non può essere vuoto');
}


/**
 * @param {JSRecord<db:/geco/contract_types>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"0D998426-0E4C-4B67-8B9E-DEF57CD875A5"}
 */
function validateContractTypeHours(record) {
	if (globals.isEmpty(record.daily_workable_hours) && record.is_weekly != 1) {
		throw new Error('-Inserire le ore lavorative o selezionare il campo settimanale');
	}
	if (!globals.isEmpty(record.daily_workable_hours) && record.is_weekly != 1){
		if(record.daily_workable_hours == 0){
			throw new Error('-Non è possibile inserire il valore 0 nel campo Ore lavorative');
		}
	}
		
}

/**
 * @param {JSRecord<db:/geco/contract_types>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"4FC3CE4C-9259-4F0A-8762-95ABD6A5BD0D"}
 */
function validateWorkingHours(record) {
	if (!globals.isEmpty(record.daily_workable_hours) && record.is_weekly == 1 && record.daily_workable_hours != 0){
		throw new Error('Non si possono inserire entrambi i campi Ore lavorative e Settimanale');
	}
	
	if(globals.isEmpty(record.daily_workable_hours) && (record.is_weekly != 1)){
		throw new Error('Inserire almeno uno tra i campi Ore lavorative e Settimanale');
	}
}


/**
 * @param {JSRecord<db:/geco/contract_types>} record
 *
 * @properties={typeid:24,uuid:"FDB142AD-ADD7-4263-BA29-BAE8B5AC6547"}
 * @AllowToRunInFind
 */
function validateNotSameContractType(record) {
	/** @type {JSFoundSet<db:/geco/contract_types>} */
	var types = databaseManager.getFoundSet('geco', 'contract_types');
	
		if(types.find()){
			types.contract_type_name = '#' + record.contract_type_name;
			types.daily_workable_hours =  record.daily_workable_hours;
			types.is_weekly = record.is_weekly;
			types.is_enabled = record.is_enabled;
			types.contract_type_id = "!=" + record.contract_type_id;
			var count = types.search();
			if (count > 0) {
				throw new Error('- Esiste già un tipo di contratto uguale');
		}
	}
}



/**
 * @param {JSRecord<db:/geco/contract_types>} record
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"C98A1F7B-F8E5-4CF8-AC64-43955CB23F08"}
 */
function processWorkableHours(record) {
	if(record.is_weekly == 1 && globals.isEmpty(record.daily_workable_hours)){
		record.daily_workable_hours = 0;
	}
}