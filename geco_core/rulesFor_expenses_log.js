/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"5A4593D6-0C4A-4978-B3DE-D1814FCFF252"}
 */
function validateExpenseType(record) {
	if (globals.isEmpty(record.expense_type_id)) {
		throw new Error("- Il campo tipo di nota spesa non può essere vuoto");
	}
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"86CC777B-1B89-439D-B9EA-8456C86E2987"}
 */
function validateExpenseJobOrder(record) {
	if (globals.isEmpty(record.job_order_id)) {
		throw new Error("- Il campo commessa per la nota spesa non può essere vuoto");
	}
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"5C5BE428-9A13-45D7-87C8-357F8C59CBAF"}
 */
function validateExpenseDate(record) {
	if (globals.isEmpty(record.expense_date)) {
		throw new Error("- Il campo data della nota spesa non può essere vuoto");
	}

}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"82F59A2E-3398-4670-8D12-FC1C677D28F6"}
 */
function validateExpenseKm(record) {
	if (record.expenses_log_to_expense_types != null && record.expenses_log_to_expense_types.has_km == 1) {
		if (globals.isEmpty(record.km)) {
			throw new Error("- Il campo km non può essere vuoto");
		}
	}
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"B92AE101-57EC-4F95-B769-B96099D362D0"}
 */
function validateExpenseAmount(record) {
	if (record.expenses_log_to_expense_types != null && record.expenses_log_to_expense_types.has_km == 0) {
		if (globals.isEmpty(record.amount)) {
			throw new Error("- Il campo importo non può essere vuoto");
		}
	}
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"D816D89D-FDAD-4ED7-AF14-39FB0351E501"}
 */
function validateExpenseChange(record) {
	// se ha selezionato una valuta diversa dall'euro
	if (!globals.isEmpty(record.currency) && record.currency != 0) {
		if (globals.isEmpty(record.expense_change)) {
			throw new Error("- Il campo cambio valuta non può essere vuoto");
		}
	} else if (record.currency == 0) {
		record.expense_change = 1;
	}
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"074FB7C0-0CFA-4E0F-BDB5-6CB1D9B88E70"}
 */
function validateExpenseAttach(record) {
	if (record.expenses_log_to_expense_types != null && record.expenses_log_to_expense_types.has_km == 0) {
		if (globals.isEmpty(record.expense_reference)) {
			throw new Error("- Il campo riferimento non può essere vuoto");
		}
	}
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"94B8DE28-1319-40FF-975B-E3DF09BC764F"}
 */
function validateExpenseNoteDate(record) {
	// se la data della nota spese esiste,deve essere maggiore della start date
	if (record.expenses_log_to_users.enrollment_start_date != null && record.expense_date < record.expenses_log_to_users.enrollment_start_date)
		throw new Error('- Non è possibile inserire note spese in giorni precedenti alla data di inizio rapporto');
	// se la data della nota spese esiste,deve essere minore della end date
	if (record.expenses_log_to_users.enrollment_end_date != null && record.expense_date > record.expenses_log_to_users.enrollment_end_date)
		throw new Error('- Non è possibile inserire note spese in giorni successivi alla data di fine rapporto');
	//controllo sul mese e l'anno della nota spese
	if (!globals.isEmpty(record.expense_date)) {
		if (record.expenses_log_to_users.enrollment_start_date != null) {
			var startMonth = record.expenses_log_to_users.enrollment_start_date.getMonth() + 1;
			var startYear = record.expenses_log_to_users.enrollment_start_date.getFullYear();
		}

		if (record.expenses_log_to_users.enrollment_end_date != null) {
			var endMonth = record.expenses_log_to_users.enrollment_end_date.getMonth() + 1;
			var endYear = record.expenses_log_to_users.enrollment_end_date.getFullYear();
		}

		if (record.expense_year < startYear)
			throw new Error('- Non è possibile inserire note spese in giorni precedenti alla data di inizio rapporto');
		if (record.expense_year == startYear && record.expense_month < startMonth)
			throw new Error('- Non è possibile inserire note spese in giorni precedenti alla data di inizio rapporto');
		if (record.expense_year > endYear)
			throw new Error('- Non è possibile inserire note spese in giorni successivi alla data di fine rapporto');
		if (record.expense_year == endYear && record.expense_month > endMonth)
			throw new Error('- Non è possibile inserire note spese in giorni successivi alla data di fine rapporto');
	}
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"57B913AD-8381-45E7-ABEB-5B8E24F9BB2B"}
 */
function validateExpenseDateMonthSelected(record) {
	if (!globals.isEmpty(record.expense_date)) {
		var month = record.expense_date.getMonth() + 1;
		var year = record.expense_date.getFullYear();
		if ( (record.expenses_log_to_calendar_months.month_year < year) || (record.expenses_log_to_calendar_months.month_year == year && record.expenses_log_to_calendar_months.month_number < month)) {
			throw new Error("- La data della spesa non può essere successiva al mese-anno selezionato per la consuntivazione");
		}
	}

}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"DD6252AD-71D7-4BA0-8D40-7712FEF841CD"}
 */
function validateExpenseDateAfterNow(record) {
	if (!globals.isEmpty(record.expense_date)) {
		var now = new Date();
		application.output('Data: ' + now + '; Data spesa: ' + record.expense_date);

		if (record.expense_date > now) {
			throw new Error("- La data della spesa non può essere successiva alla data odierna.");
		}
	}
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 *
 * @properties={typeid:24,uuid:"71EFADD8-EC50-4940-AD54-5A49BF86BBE6"}
 */
function validateMonthIsEnable(record) {
	if (!globals.hasRole('HR Administrators') && !globals.hasRole('HR Expenses') && !globals.hasRole('Controllers'))
		if (record.expenses_log_to_calendar_months.is_enabled_for_expenses == 0) {
			throw new Error("- Non è possibile inserire note spese per mesi disabilitati");
		}
}

/**
 * Valida se commessa è abilitata per la consuntivazione 
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"293FB866-62FA-47BA-B778-AA428B4B81B4"}
 * @AllowToRunInFind
 */
function validateJobOrderEnabled(record) {
	if(record.expenses_log_to_job_orders && record.expenses_log_to_job_orders.is_enabled == 0){
		throw new Error('- Nel giorno ' + record.expense_date.toLocaleDateString() + ' non è possibile inserire spese per una commessa disabilitata.');
	}
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 *
 * @properties={typeid:24,uuid:"39449709-0FF5-4942-96E2-2A5C97D4599B"}
 */
function validateDescription(record) {
	if (globals.isEmpty(record.description) || globals.isEmpty(scopes.globals.trimString(record.description))) {
		throw new Error("- Il campo Descrizione è obbligatorio");
	}
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"6533FBFC-81BB-405E-ABFE-3FF7AD047180"}
 */
function processTotalAmount(record) {
	//km expense
	if (record.expenses_log_to_expense_types.has_km == 1) {
		if (!record.expenses_log_to_users.users_to_users_cars)
			throw new Error("- Impossibile calcolare il totale, non sono presenti i dati della macchina. Chiedere in amministrazione");

		if (globals.isEmpty(record.expenses_log_to_users.users_to_users_cars.user_car_type)) {
			throw new Error("- Impossibile calcolare il totale, non sono presenti i dati della macchina. Chiedere in amministrazione");
		}
		//personal car ACI refound
		if (record.expenses_log_to_users.users_to_users_cars.user_car_type == 0) {
			//not specified ACI refound amount
			if (globals.isEmpty(record.expenses_log_to_users.users_to_users_cars.users_cars_to_refound_km.refound_value)) {
				throw new Error("- Impossibile calcolare il totale, non sono presenti i dati della macchina. Chiedere in amministrazione");
			}
			record.total_amount = record.km * record.expenses_log_to_users.users_to_users_cars.users_cars_to_refound_km.refound_value;
			record.total_amount_km = record.total_amount;
		}
		//corporate car refound
		else if (record.expenses_log_to_users.users_to_users_cars.user_car_type == 1) {
			//not specified fuel refound amount
			if (globals.isEmpty(record.expenses_log_to_users.users_to_users_cars.corporate_car_refound_km)) {
				throw new Error("- Impossibile calcolare il totale, non sono presenti i dati della macchina. Chiedere in amministrazione");
			}
			record.total_amount = record.km * record.expenses_log_to_users.users_to_users_cars.corporate_car_refound_km;
			record.total_amount_km = record.total_amount;
		}
		record.total_amount_money = null;
	}
	//no km expense and currecy != euro
	//	else if (record.expenses_log_to_expense_types.has_km == 0 && !globals.isEmpty(record.currency) && record.expense_change != 0) {
	//		record.total_amount = record.amount * record.expense_change;
	//		record.total_amount_money = record.total_amount;
	//	}
	//no km expense and currency == euro
	else {
		if (record.expenses_log_to_expense_types.has_km == 0 && !globals.isEmpty(record.currency) && record.expense_change != 0) {
			record.total_amount = record.amount * record.expense_change;
			record.total_amount_money = record.total_amount;

		} else {
			record.total_amount = record.amount;
			record.total_amount_money = record.total_amount
			record.currency = null;
		}
		record.total_amount_km = 0;
	}

	if (!record.total_amount) throw new Error("- Impossibile calcolare il totale");
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"BE50C9A2-FA8C-4618-B119-B28BE4407CB4"}
 */
function processApprover(record) {
	/** @type {JSFoundSet<db:/geco/approvers>} */
	var approvers = databaseManager.getFoundSet('geco', 'approvers')

	var eventApprover = null;
	// find out who's the approver figure
	eventApprover = approvers.getApprover('job_orders',
		record.job_order_id,
		record.expense_date
	);
	if (eventApprover == record.user_id) {
		//utente loggato è approvatore devo cercare il RDC
		eventApprover = record.expenses_log_to_job_orders.user_owner_id;
		if (eventApprover == record.user_id) {
			//utente loggato è RDC salgo al responsabile del PC
			eventApprover = record.expenses_log_to_job_orders.job_orders_to_profit_centers.user_owner_id;
			if (eventApprover == record.user_id) {
				//utente loggato è responsabile del PC della commessa risalgo al responsabile dell'utente
				eventApprover = record.expenses_log_to_users.users_to_profit_centers.user_owner_id;
			}
		}
	}
	application.output('RDC ' + eventApprover);
	if (eventApprover) {
		record.user_approver_id = eventApprover;
	} else {
		throw new Error("- Impossibile assegnare l'approvatore dell'evento");
	}
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"BEA94950-A66C-4D71-8AB7-E25B618424F4"}
 */

function processResubmission(record) {

	var changedData = record.getChangedData();

	// exit if approving or rejecting
	for (var index = 1; index <= changedData.getMaxRowIndex(); index++) {
		if (changedData.getValue(index, 1) == 'expense_log_status') return;
	}

	if (record.expense_log_status == 3) {
		record.expense_log_status = 1;
		record.note = null;
	}
}

