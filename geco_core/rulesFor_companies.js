/**
 * @param {JSRecord<db:/geco/companies>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"98A36360-A57C-425C-B2E3-DE36331EA121"}
 */
function validateCompanyName(record) {
	if (globals.isEmpty(record.company_name)) {
		throw new Error('- Il campo nome è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/companies>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"B9212C81-AB44-4791-8CFF-0905BC2F41A3"}
 */
function validateUniqueCompanyName(record) {
	/** @type {JSFoundSet<db:/geco/companies>} */
	var companies = databaseManager.getFoundSet('geco', 'companies');
	if (companies.find()){
		companies.company_name = '#'+record.company_name;
		companies.company_id = '!'+ record.company_id;
		if (companies.search() > 0){
			throw new Error('- esiste già un cliente con lo stesso nome');
		}
	}
}

/**
 * @param {JSRecord<db:/geco/companies>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"F220EE6E-FA1B-4369-9992-1A8768979905"}
 */
function validateUniqueCompanyCode(record) {
	if (!globals.isEmpty(record.external_code)) {
		/** @type {JSFoundSet<db:/geco/companies>} */
		var companies = databaseManager.getFoundSet('geco', 'companies');
		if (companies.find()) {
			companies.external_code = '#' + record.external_code;
			companies.company_id = '!'+ record.company_id;
			if (companies.search() > 0) {
				throw new Error('- esiste già un cliente con lo stesso codice');
			}
		}
	}
	else {
		throw new Error('- Il campo codice è obbligatorio');
	}
}


/**
 * @param {JSRecord<db:/geco/companies>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"8D7B6F02-1320-4251-9453-74897E3CB7F6"}
 */
function validateCompanyType(record) {
	if (globals.isEmpty(record.company_type_id)) {
		throw new Error('- Il campo tipologia è obbligatorio');
	}
}