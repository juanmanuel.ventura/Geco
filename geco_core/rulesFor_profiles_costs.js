/**
 * JStaffa 3 Controlli in una rule sola
 * Valida inserimento della data di inizio del costo del profilo selezionato
 * Valida inserimento stessa data
 * Valida inserimento costo profilo con data superiore all'ultimo attuale
 *
 * @param {JSRecord<db:/geco/profiles_costs>} record
 * @properties={typeid:24,uuid:"C9685519-4140-4FD0-97FF-9A62F12BD22C"}
 * @AllowToRunInFind
 */
function validateProfileCostStartDate(record) {
	var errorMsg = '';
	var tot = 0;
	//valida che ci sia la data
	if (globals.isEmpty(record.prof_cost_start_date))
		errorMsg = errorMsg + '- Il campo Data inizio validità è obbligatorio\n';
	else {
		var dateStr = globals.getDateStringFormat(record.prof_cost_start_date);
		//valida che non sia una data già usata
		/** @type {JSFoundSet<db:/geco/profiles_costs>} */
		var profiles_costs = databaseManager.getFoundSet('geco', 'profiles_costs');
		if (profiles_costs.find()) {
			profiles_costs.profile_cost_id = '!' + record.profile_cost_id; //diverso dal record che arriva
			profiles_costs.prof_cost_start_date = dateStr + ' |yyyy-MM-dd';
			profiles_costs.profile_id = record.profile_id; //importantissimo! ovviamente sul profilo selezionato

			tot = profiles_costs.search();
			if (tot > 0)
				errorMsg = errorMsg + '- Esiste già un costo con Data inizio validità inserita, modificare il record esistente';
		}
		profiles_costs.loadRecords(); //ricarico i record
		//		rientro in find; mi serve la data maggiore per il profilo selezionato
		if (profiles_costs.find()) {
			profiles_costs.profile_cost_id = '!' + record.profile_cost_id; //sempre diverso dal record che arriva
			profiles_costs.profile_id = record.profile_id; //importantissimo! ovviamente sul profilo selezionato

			tot = profiles_costs.search();
		}
		//valida che la data inserita sia maggiore dell'ultima
		if (record.prof_cost_start_date < profiles_costs.max_prof_cost_start_date)
			errorMsg = !globals.isEmpty(errorMsg) ? errorMsg + '\n- Il campo Data inizio validità del nuovo record deve essere superiore rispetto all\'ultima Data inizio validità esistente' : '- Il campo Data inizio validità del nuovo record deve essere superiore rispetto all\'ultima Data inizio validità esistente';
	}
	if (!globals.isEmpty(errorMsg)) throw new Error(errorMsg);
}

/**
 * JStaffa 3 controlli:
 * obbligatorietà campi; che min sia minore o uguale a max; che max sia maggiore o uguale a min
 * @param {JSRecord<db:/geco/profiles_costs>} record
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"158091B4-0552-47FA-AFF5-169A54742D50"}
 */
function validateProfileCostMinAndMax(record) {
	//		che ci siano
	if (globals.isEmpty(record.profile_cost_min) || globals.isEmpty(record.profile_cost_max))
		throw new Error('- I campi Costo minimo / Costo massimo sono obbligatori');
	else {
		//		che min sia minore o uguale a max / che max sia maggiore o uguale a min
		if (record.profile_cost_min > record.profile_cost_max)
			throw new Error('- Il Costo minimo non può essere superiore al Costo massimo');
	}
}

/**
 * JStaffa Valida inserimento costo giornaliero del profilo selezionato
 * @param {JSRecord<db:/geco/profiles_costs>} record
 * @properties={typeid:24,uuid:"DF4B08F6-B6A4-4554-B95D-6CE22BC26D3E"}
 */
function validateProfileDailyCost(record) {
	if (globals.isEmpty(record.prof_cost_daily)) {
		throw new Error('- Il campo Costo giornaliero è obbligatorio');
	} else {
		//		verificare che stia dentro i valori min e max
		if (!((record.profile_cost_min <= record.prof_cost_daily) && 
				(record.prof_cost_daily <= record.profile_cost_max))) {
					throw new Error('- Il campo Costo giornaliero è oltre i range imposti da Costo minimo e Costo massimo');
		}
	}
}
