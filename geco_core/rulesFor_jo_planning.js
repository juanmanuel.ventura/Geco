/**
 * @param {JSRecord<db:/geco/jo_planning>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"D5408E97-D9B5-49A1-9E32-19E0482C8FCB"}
 */
function processJO_id(record) {
	record.jo_id = record.jo_planning_to_jo_details.jo_id;
}

/**
 * imposta i valori di costo e ricavo in base alla tipologia del dettaglio
 * @param {JSRecord<db:/geco/jo_planning>} record
 *
 * @properties={typeid:24,uuid:"EEC710D1-25E0-4857-AAA1-6C6282386213"}
 */
function processChangeAmountActual(record) {
	/** @type {JSDataSet} */
//	var dsUpdate = record.getChangedData();
//	for (var i = 1; i <= dsUpdate.getMaxRowIndex(); i++) {
//		application.output(dsUpdate.getValue(i, 1) + ' ' + dsUpdate.getValue(i, 2) + ' ' + dsUpdate.getValue(i, 3));
//		//cambio actual
//		if (dsUpdate.getValue(i, 1) == 'cost_actual') {
//			application.output('Modifico per cost actual')
//			//sottroaggo dal campo cost_actual del JO_details il vecchio importo e aggiungo il nuovo
//			record.jo_planning_to_jo_details.cost_actual = scopes.globals.roundNumberWithDecimal( (record.jo_planning_to_jo_details.cost_actual - dsUpdate.getValue(i, 2)), 2);
//			record.jo_planning_to_jo_details.cost_actual = scopes.globals.roundNumberWithDecimal( (record.jo_planning_to_jo_details.cost_actual + dsUpdate.getValue(i, 3)), 2);
//			record.jo_planning_to_jo_details.total_actual = record.jo_planning_to_jo_details.cost_actual
//			record.jo_planning_to_jo_details.date_actual = record.date_actual;
//			record.jo_planning_to_jo_details.user_actual = record.user_actual;
//			record.competence_month = scopes.globals.actualDate;
//			application.output('details ID  ' + record.jo_planning_to_jo_details.jo_details_id + ' dopo modifica costo costo actual ' + record.jo_planning_to_jo_details.cost_actual + ' totale actual ' + record.jo_planning_to_jo_details.total_actual)
//		}
//		if (dsUpdate.getValue(i, 1) == 'return_actual') {
//			application.output('Modifico per return_actual')
//			//sottroaggo dal campo profit_actual del JO_details il vecchio importo e aggiungo il nuovo
//
//			record.jo_planning_to_jo_details.return_actual = scopes.globals.roundNumberWithDecimal( (record.jo_planning_to_jo_details.return_actual - dsUpdate.getValue(i, 2)), 2);
//			record.jo_planning_to_jo_details.return_actual = scopes.globals.roundNumberWithDecimal( (record.jo_planning_to_jo_details.return_actual + dsUpdate.getValue(i, 3)), 2);
//			record.jo_planning_to_jo_details.total_actual = record.jo_planning_to_jo_details.return_actual
//			record.jo_planning_to_jo_details.date_actual = record.date_actual;
//			record.jo_planning_to_jo_details.user_actual = record.user_actual;
//			record.competence_month = scopes.globals.actualDate;
//			application.output('details ID  ' + record.jo_planning_to_jo_details.jo_details_id + ' dopo modifica costo return actual ' + record.jo_planning_to_jo_details.return_actual + ' totale actual ' + record.jo_planning_to_jo_details.total_actual)
//		}
//	}
}
