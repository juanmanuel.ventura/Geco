/**
 * @param {JSRecord<db:/geco/bo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"70AD7E5A-0C29-41EA-BD36-9C70BFE53418"}
 */
function validateTotalAmount(record) {
	if (globals.isEmpty(record.total_amount) || record.total_amount == null || record.total_amount == 0) {
		throw new Error('- Il campo totale è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/bo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"0633A53A-D21A-4427-ADC6-F279A4E20649"}
 */
function validateProfitCostType(record) {
	if (globals.isEmpty(record.profit_cost_type_id)) {
		throw new Error('- Il campo tipologia è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/bo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"699A2299-E777-40E3-8CE1-7C9A253B5445"}
 * @AllowToRunInFind
 */
function validateFigure(record) {
	//personale
	if (record.profit_cost_type_id == 5) {
		if (globals.isEmpty(record.standard_figure) && globals.isEmpty(record.real_figure)) {
			throw new Error('- Il campo persona è obbligatorio per la tipologia scelta, indicare la figura professionale o la figura reale');
		}
	}
	//TM
	else if (record.profit_cost_type_id == 9) {
		if (globals.isEmpty(record.standard_figure) && globals.isEmpty(record.real_tm_figure)) {
			throw new Error('- Il campo persona è obbligatorio per la tipologia scelta, indicare la figura professionale o la figura TM');
		}
	}
	//TK
	else if (record.profit_cost_type_id == 8) {
		if (globals.isEmpty(record.real_tk_supplier)) {
			throw new Error('- Il campo fornitore è obbligatorio per la tipologia scelta');
		}
	} 
	//spese
	else if (record.profit_cost_type_id == 10) {
		if (globals.isEmpty(record.standard_figure) && globals.isEmpty(record.real_figure)) {
			throw new Error('- Il campo persona è obbligatorio per la tipologia scelta, indicare la figura professionale o la figura reale');
		}
	}
	
	if (record.bo_details_to_profit_cost_types.profit_cost_acr == 'C' && record.figure != null) {
		application.output('conrollo duplicati')
		/** @type {JSFoundSet<db:/geco/bo_details>} */
		var bo_detailsC = databaseManager.getFoundSet('geco', 'bo_details');
		var totSameTypeC = 0;
		if (bo_detailsC.find()) {
			bo_detailsC.bo_id = record.bo_id;
			bo_detailsC.bo_details_id = '!' + record.bo_details_id;
			bo_detailsC.profit_cost_type_id = record.profit_cost_type_id;
			bo_detailsC.real_figure = record.real_figure;
			bo_detailsC.real_tm_figure = record.real_tm_figure;
			bo_detailsC.real_tk_supplier = record.real_tk_supplier;
			//bo_detailsC.days_import = record.days_import;
			bo_detailsC.enrollment_number = record.enrollment_number;
			bo_detailsC.figure = record.figure;
			totSameTypeC = bo_detailsC.search();
		}
		if (totSameTypeC > 0) {
			//controllo aggiunto per permettere più inserimenti di dettagli "Personale" a figura generica
			if (!(record.profit_cost_type_id == 5 && record.standard_figure != null && record.real_figure == null))
				throw new Error('- Esiste già un costo con la stessa tipologia e la stessa descrizione, modifcare la descrizione');
		}
	}
	
	//ricavo
	else if (record.bo_details_to_profit_cost_types.profit_cost_acr == 'R' && record.figure != null) {
		/** @type {JSFoundSet<db:/geco/bo_details>} */
		var bo_details = databaseManager.getFoundSet('geco', 'bo_details');
		var totSameType = 0;
		if (bo_details.find()) {
			bo_details.bo_id = record.bo_id;
			bo_details.bo_details_id = '!' + record.bo_details_id;
			bo_details.profit_cost_type_id = record.profit_cost_type_id;
			bo_details.figure = record.figure;
			totSameType = bo_details.search();
		}
		if (totSameType > 0) {
			throw new Error('- Esiste già un ricavo con la stessa tipologia e la stessa descrizione, modifcare la descrizione');
		}
	}
}

/**
 * @param {JSRecord<db:/geco/bo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"CE77B599-3824-4AB1-A221-A125FD7DC685"}
 */

function validateDaysNumber(record) {
	//valida la presenza campo giorni se è stato scelta la tipologia figura professionale
	if ( (record.profit_cost_type_id == 5 || record.profit_cost_type_id == 9) && globals.isEmpty(record.days_number)) {
		throw new Error('- Il campo giorni è obbligatorio per la tipologia scelta');
	}
}

/**
 * @param {JSRecord<db:/geco/bo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"D0B0E627-711B-4AF0-9146-EDA798BC86B9"}
 * @AllowToRunInFind
 */
function processFigureValue(record) {
	//inserisce il valore del campo figure in base alla tipologia scelta
	var real_personnel = null;
	var real_tm = null;
	var real_tk = null;
	var figure = record.figure;
	//peronale
	if ((record.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || record.bo_details_to_profit_cost_types.profit_cost_types_id == 10) && record.bo_details_to_real_figure != null) {
		figure = record.bo_details_to_real_figure.users_to_contacts.real_name;
		real_personnel = record.real_figure;
	}
	// forniture TM
	else if (record.bo_details_to_profit_cost_types.profit_cost_types_id == 9 && record.bo_details_to_tm_figure != null) {
		figure = record.bo_details_to_tm_figure.users_to_contacts.real_name;
		real_tm = record.real_tm_figure;
	}
	//forniture TK
	else if (record.bo_details_to_profit_cost_types.profit_cost_types_id == 8 && record.bo_details_to_suppliers != null) {
		figure = record.bo_details_to_suppliers.company_name;
		real_tk = record.real_tk_supplier
	}

	if ( (record.bo_details_to_profit_cost_types.profit_cost_types_id == 5 || record.bo_details_to_profit_cost_types.profit_cost_types_id == 9 || record.bo_details_to_profit_cost_types.profit_cost_types_id == 10) && figure == null) {
		figure = record.bo_details_to_standard_professional_figures.description;
	}

	if ( (record.bo_details_to_profit_cost_types.profit_cost_acr == 'R') && record.figure == null) {
		var tipo = '';
		switch (record.profit_cost_type_id) {
		case 1:
			tipo = 'competenze';
			break;
		case 2:
			tipo = 'spese';
			break;
		case 3:
			tipo = 'credito';
			break;
		case 4:
			tipo = 'allineamento';
			break;
		}
		/** @type {JSFoundSet<db:/geco/bo_details>} */
		var bo_details = databaseManager.getFoundSet('geco', 'bo_details');
		var totSameType = 0;
		if (bo_details.find()) {
			bo_details.bo_id = record.bo_id;
			bo_details.bo_details_id = '!' + record.bo_details_id;
			bo_details.profit_cost_type_id = record.profit_cost_type_id;
			bo_details.figure = tipo + '%';
			totSameType = bo_details.search();
		}
		if (totSameType > 0) {
			var description = '';
			var descrAr = [];
			var found = false;
			bo_details.sort('bo_details_id desc')
			for (var index = 1; index <= bo_details.getSize(); index++) {
				description = bo_details.getRecord(index).figure;
				application.output('descrizone ricavo ' + description);
				descrAr = description.split(' ');
				application.output('descrizone ricavo ' + description);
				if (descrAr.length > 1 && descrAr[0] == tipo && !isNaN(descrAr[1])) {
					/** @type {Number} */
					var numero = parseInt(descrAr[1])
					var progr = numero + 1;
					application.output(progr);
					figure = descrAr[0] + ' ' + progr;
					found = true;
					break;
				} else {
					descrAr[0] = tipo;
				}
			}
			if (found == false) {
				figure = descrAr[0] + ' ' + 1;
			}

		} else {
			switch (record.profit_cost_type_id) {
			case 1:
				figure = 'competenze';
				break;
			case 2:
				figure = 'spese';
				break;
			case 3:
				figure = 'credito';
				break;
			case 4:
				figure = 'allineamento';
				break;
			default:
				figure = '';
				break;
			}
		}
	}
	application.output('descrizione figura: ' + figure + ' real_personnel: ' + real_personnel + ' real_tm: ' + real_tm + ' real_tk : ' + real_tk)
	record.figure = figure;
	record.real_figure = real_personnel;
	record.real_tm_figure = real_tm;
	record.real_tk_supplier = real_tk;
}

/**
 * @param {JSRecord<db:/geco/bo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"19832C35-A360-425B-941A-BF90C824618B"}
 */
function processCostReturnImport(record) {
	//inserisce il valore dei campi return_import o cost_import in base alla tipologia scelta e ai valori inseriti
	if (record.bo_details_to_profit_cost_types.profit_cost_acr == 'R' && !globals.isEmpty(record.total_amount)) {
		record.return_amount = record.total_amount;
		record.cost_amount = 0;
	} else if (record.bo_details_to_profit_cost_types.profit_cost_acr == 'C' && !globals.isEmpty(record.total_amount)) {
		record.cost_amount = record.total_amount;
		record.return_amount = 0;
	}
}

/**
 * ricalcola il valore del campo cost o profit della BO collegata
 * @param {JSRecord<db:/geco/bo_details>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"77B81B87-1DD1-48EB-A2DA-B9C8E3E78AD2"}
 */
function processChangeAmount(record) {
	/** @type {JSDataSet} */
//	var dsUpdate = record.getChangedData();
//	for (var i = 1; i <= dsUpdate.getMaxRowIndex(); i++) {
//		application.output(dsUpdate.getValue(i, 1) + ' ' + dsUpdate.getValue(i, 2) + ' ' + dsUpdate.getValue(i, 3));
//		//cambio costo
//		if (dsUpdate.getValue(i, 1) == 'cost_amount') {
//			//sottroaggo dal campo cost della BO il vecchio importo e aggiungo il nuovo
//			record.bo_details_to_bo.cost = record.bo_details_to_bo.cost - dsUpdate.getValue(i, 2);
//			record.bo_details_to_bo.cost = record.bo_details_to_bo.cost + dsUpdate.getValue(i, 3);
//		}
//		if (dsUpdate.getValue(i, 1) == 'return_amount') {
//			//sottroaggo dal campo profit della BO il vecchio importo e aggiungo il nuovo
//			record.bo_details_to_bo.profit = record.bo_details_to_bo.profit - dsUpdate.getValue(i, 2);
//			record.bo_details_to_bo.profit = record.bo_details_to_bo.profit + dsUpdate.getValue(i, 3);
//		}
//	}
}
