/**
 * @properties={type:8,typeid:36,uuid:"9358D3B2-2E5A-4D18-BF68-704C3B8E0593"}
 */
function is_selected() {
	return 0;
}

/**
 * @properties={type:12,typeid:36,uuid:"FBE352DB-685B-4247-8147-8A5BAD78F315"}
 */
function is_actual_confirmed_display() {
	if (is_actual_confirmed == 1) return 'SI';
	else return '';
}

/**
 * @properties={type:12,typeid:36,uuid:"0795C9D3-B171-4EEF-846D-9E6E9D742737"}
 */
function is_actual_display() {
	if (is_actual == 1) return 'Actual';
	else return '';
}

/**
 * @properties={type:8,typeid:36,uuid:"58B8DF57-D5D6-4AA6-8720-1185FAA32587"}
 */
function mol_per_display() {
	var mol = 0;
	mol = (total_return - total_cost) * 100 /  Math.abs(total_return);
	mol = Math.round(mol * Math.pow(10, 2)) / Math.pow(10, 2);
	return mol;
}

/**
 * @properties={type:8,typeid:36,uuid:"97959330-534A-4B1E-9BB3-0ADE6F2C3CAD"}
 */
function mol_display() {
	var mol = 0;	
	//application.output('piano ID '+jo_planning_id+' joID '+jo_id+ ' joDetID ' + jo_details_id + ' mese '+jo_month + ' anno ' +jo_year+ ' return '+total_return+ ' cost ' + total_cost + ' numero ' + record_count)
	mol = (total_return - total_cost);
	mol = Math.round(mol * Math.pow(10, 2)) / Math.pow(10, 2);
	return mol;
}

/**
 * @properties={type:8,typeid:36,uuid:"5978987A-3E57-4E97-ABD1-F7A7C8B99052"}
 */
function eac() {
	//actual + pianificato futuro(non actual)
	return (sum_total_amount - sum_total_amount_actual);
}

/**
 * @properties={type:8,typeid:36,uuid:"22366F40-9FAD-4C27-94C7-A8E36730ED00"}
 */
function mol_actual_per_display() {
	var mol = 0;
	mol = (total_return_actual - total_cost_actual) * 100 / Math.abs(total_return_actual);
	mol = Math.round(mol * Math.pow(10, 2)) / Math.pow(10, 2);
	return mol;
}

/**
 * @properties={type:8,typeid:36,uuid:"AE250B71-85E1-4D16-A534-322980E346FB"}
 */
function mol_actual_display() {
	var mol = 0;
	mol = (total_return_actual - total_cost_actual);
	mol = Math.round(mol * Math.pow(10, 2)) / Math.pow(10, 2);
	return mol;
}

