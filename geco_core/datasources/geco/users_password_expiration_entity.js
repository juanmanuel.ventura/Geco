/**
 * @param {Number} uid
 * @param {String} newPassword
 * @param {Number} numDays
 * @param {Boolean} skip = true evita controllo su ultime 3 password per reset da controller o personnel
 * @return {Boolean}
 * @properties={typeid:24,uuid:"1ED63137-19CE-46F8-ACD9-260245B52A80"}
 * @AllowToRunInFind
 */
function insertNewPassword(uid, newPassword, numDays, skip) {
	application.output('user_id ' + uid + ' newpwd - giorni ' +numDays );
	/** @type {JSFoundSet<db:/geco/users_password_expiration>} */
	var user_pwd_exp = databaseManager.getFoundSet('geco', 'users_password_expiration');

	var today = new Date();
	today = new Date(today.getFullYear(), today.getMonth(), today.getDate() + numDays);
	var existPwd = false;
	var pwdList = new Array();
	if (user_pwd_exp.find()) {
		user_pwd_exp.users_id = uid;
		user_pwd_exp.sort('usr_pwd_exp_id desc');
		var result = user_pwd_exp.search();
		var recordTodelete = null;
		for (var index = 1; index <= result; index++) {
			var rec = user_pwd_exp.getRecord(index);
			if (index >= 3) {
				recordTodelete = rec;
				pwdList.push(recordTodelete);
			}
			if (newPassword == rec.password_history) {
				existPwd = true
			}
		}
	}
	if (existPwd == true && skip == false) return false;
	application.output(' aggiungo record per nuova pasword ' );
	user_pwd_exp.newRecord(false);
	user_pwd_exp.users_id = uid;
	user_pwd_exp.expiration_date = today;
	user_pwd_exp.password_history = newPassword;
	databaseManager.saveData(user_pwd_exp);
	application.output(' cancello terza password ' );
	for (var i = 0; i<pwdList.length; i++){
		user_pwd_exp.deleteRecord(pwdList[i]);
	}
	application.output('cancellata la terza pasword history');

	return true;
}
