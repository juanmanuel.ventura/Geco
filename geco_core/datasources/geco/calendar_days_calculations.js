/**
 * @return {Number}
 * @properties={type:12,typeid:36,uuid:"CA1C4141-0FBE-40BA-B3F8-66980CAD236E"}
 */
function workingDays()
{
	var lavorativi = sumWorkingDays
	
	if (calendar_weekday != 6 && calendar_weekday!=0 && is_holiday == 1 && is_working_day == 0) {
		lavorativi= lavorativi -1;
	}
	lavorativi = Math.round(lavorativi * Math.pow(10, 0)) / Math.pow(10, 0);
	return lavorativi;
}

/**
 * @return {Number}
 * @properties={type:4,typeid:36,uuid:"E1E81E97-F727-4F45-8C94-0689011B25D8"}
 */
function statusApproval() {
	
	if (calendar_days_to_events_logs_open.record_count>0)return 1;

	// all approved
	if (calendar_days_to_events_log.record_count >0 && calendar_days_to_events_log.record_count == calendar_days_to_events_logs_approved.record_count 
			//&& is_working_day == 1 && is_holiday == 0
			) return 2;

	// all approved overtime
	//if (calendar_days_to_events_log.record_count >0 && calendar_days_to_events_log.record_count == calendar_days_to_events_logs_approved.record_count && is_holiday == 1) return 2;

	// at least one rejected
	if (calendar_days_to_events_logs_rejected.record_count) return 3;
	
	//all closed
	if (calendar_days_to_events_log.record_count >0 &&  calendar_days_to_events_log.record_count == calendar_days_to_events_logs_closed.record_count) return 4;

	if (calendar_days_to_events_log.record_count >0 && 
		(calendar_days_to_events_log.record_count != calendar_days_to_events_logs_closed.record_count && 
		calendar_days_to_events_logs_rejected.record_count == 0 && 
		calendar_days_to_events_log.record_count != calendar_days_to_events_logs_approved.record_count)) return 1;
	// default to null
	return null;
	//return 1;
}

/**
 * @properties={type:4,typeid:36,uuid:"47A2CEA3-4DBE-4408-B6CC-F93848125C66"}
 */
function statusQuota() {
	
	// not filled (check it first as it loads all records in cache)
	if (!calendar_days_to_events_log.record_count && is_working_day == 1 && is_holiday == 0) return 3;
	if (!calendar_days_to_events_log.record_count) return null;
	
	//var userWorkingHours = currentuserid_to_users.working_hours * 60;
	var userWorkingHours = 0 * 60;
	//application.output('statusQuota '+userWorkingHours);
	if (calendar_days_to_events_log.record_count) {
		//application.output(calendar_days_to_events_log.event_log_date);
		var objTimeUser = globals.getUserTimeWorkByDay(globals.currentUserId,calendar_days_to_events_log.event_log_date);
		userWorkingHours = objTimeUser.workingHours * 60;
//		userWorkingHours = globals.getUserWorkingHoursByDay(globals.currentUserId,calendar_days_to_events_log.event_log_date, currentuserid_to_users.working_hours) *60;

	}
	//application.output('statusQuota dopo set  '+userWorkingHours);
	var userActualHours = (calendar_days_to_events_log.getSize() > 0) ? calendar_days_to_events_log.total_duration_regular : null;

	// under filled
	if (userActualHours < userWorkingHours) return 1;
	
	// completely filled
	if (userActualHours == userWorkingHours) return 2;
	
	// over filled
	if (userActualHours > userWorkingHours) return 4;
	
	// default to null
	return null;
}

/**
 * @properties={type:12,typeid:36,uuid:"429B5F34-02E5-4C6D-A452-205854754942"}
 */
function is_selected() {
	return 0;
}
