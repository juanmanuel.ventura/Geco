/**
 * @properties={type:4,typeid:36,uuid:"466AED94-6AD8-4698-8A3F-2ED9E23B7DC7"}
 */
function is_selected() {
	return 0;
}

/**
 * @properties={type:4,typeid:36,uuid:"4B29BB6F-A952-4BD0-A7F2-7F798BCA448F"}
 */
function current_approver_id() {
	return profit_centers_to_approvers.getApprover('profit_centers', profit_center_id, new Date)
}
