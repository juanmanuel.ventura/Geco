/**
 * Record after-create trigger.
 *
 * @param {JSRecord<db:/geco/users>} record record that is created
 * @private
 * @properties={typeid:24,uuid:"41833C01-B4C6-4547-A326-FDFE8D6D89C3"}
 */
function afterFoundSetRecordCreate(record) {
	//default password on insert
	record.user_password = globals.sha1('spindox');
	
	/** @type {JSFoundSet<db:/geco/contacts>} */
	var contacts = databaseManager.getFoundSet('geco', 'contacts');
	contacts.newRecord()
	record.contact_id = contacts.contact_id;
	contacts.has_user = 1;
}

/**
 * @param {JSRecord<db:/geco/users>} record record that will be inserted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"3959647F-4ED3-4141-9BD4-A4BB10A7CD61"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert') && setDisplays(record);
}

/**
 * @param {JSRecord<db:/geco/users>} record record that will be updated
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"33F238DA-E15C-4BC6-B601-C450D117FF53"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate') && 
		setDisplays(record);
}

/**
 * @param {JSRecord<db:/geco/users>} record record that will be updated
 * @private
 * @return {Boolean}
 * @properties={typeid:24,uuid:"AD77398F-1871-48FD-A09E-4D0D0BAD9233"}
 */
function setDisplays(record) {
	enrollment_number = enrollment_number.toUpperCase();
	return true;
}
