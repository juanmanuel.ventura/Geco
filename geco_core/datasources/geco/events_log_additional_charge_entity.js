/**
 * Passo il record che sta per essere cancellato(events_log)
 * e action
 * @param {JSRecord<db:/geco/events_log>} record
 *
 * @param {Number} action  1= strao; 2= riposo
 * @properties={typeid:24,uuid:"50768FDC-AF6A-405D-B232-702A32AF3C7A"}
 * @SuppressWarnings(wrongparameters)
 * @AllowToRunInFind
 */
function deleteAdditionalCharge(record, action) {
	application.output(globals.messageLog + 'START events_log_additional_charge deleteAdditionalCharge - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
	var result = 0;
	var resultToModify = 0;

	if (find()) {
		user_id = record.user_id;
		el_month = record.event_month;
		el_year = record.event_year;
		result = search();
	}

	application.output(globals.messageLog + 'events_log_additional_charge deleteAdditionalCharge - record trovati: ' + result, LOGGINGLEVEL.DEBUG);
	try {
		if (result > 0) {
			databaseManager.setAutoSave(false);
			for (var index = 1; index <= result; index++) {
				var recFound = getRecord(index);
				application.output("INDICE CICLO FOR " + index);
				application.output("duration_worked DA DB " + recFound.duration_worked);
				application.output("duration_remained DA DB " + recFound.duration_remained);
				application.output("duration_retrieved DA DB " + recFound.duration_retrieved);
				application.output("duration_worked_rejected DA DB " + recFound.duration_worked_rejected);
				application.output("duration_retrieved_rejected DA DB " + recFound.duration_retrieved_rejected);

				application.output(globals.messageLog + 'events_log_additional_charge deleteAdditionalCharge - for: ' + index + ' recordTrovato ' + recFound, LOGGINGLEVEL.DEBUG);
				if (action == 1) {
					application.output("ACTION == 1")
					//cancellazione evento 28: lavoro straordinario con riposo
					//sottraggo le ore fatte, ricalcolo le ore rimanenti
					application.output('Record da eliminare (event_id = 28): ' + recFound);

					//FS
					recFound.duration_worked = recFound.duration_worked - record.duration;
					recFound.duration_remained = recFound.duration_remained - record.duration;
					// se cancello un record riufiutato sottraggo le ore del record dalla rifiutate

					if (record.event_log_status == 3 && recFound.duration_worked_rejected > 0) {
						recFound.duration_worked_rejected = recFound.duration_worked_rejected - record.duration;
					}
					//sottraggo ore duration_remained

				} else if (action == 2) {
					application.output("ACTION == 2")
					//cancellazione evento 25: riposo compensativo straordinario
					//sottraggo le ore recuperate ---> DF lo fa sotto
					//recFound.duration_retrieved = recFound.duration_retrieved - record.duration;
					// se cancello un record riufiutato sottraggo le ore del record dalla recuperate rifiutate
					//FS
					if (record.event_log_status == 3) {

						recFound.duration_retrieved_rejected = recFound.duration_retrieved_rejected - record.duration;
					}
					application.output("ID del recupero da eliminare : " + record.event_log_id);
					//data inserita sul record trovato in formato stringa
					var dateToConfront = recFound.el_competence_month;
					//var dateToConfront = additionalChargeRecord.el_competence_month;
					var dayTC = '' + (dateToConfront.getDate());
					var monthTC = '' + (dateToConfront.getMonth() + 1);
					var dateTC = dateToConfront.getFullYear() + '-' + (monthTC.length > 1 ? monthTC : '0' + monthTC) + '-' + (dayTC.length > 1 ? dayTC : '0' + dayTC);
					application.output('dateToConfront: ' + dateTC);
					//data massima precedente su cui posso cercare; scopes.globals.elapsedMaxMonth = numero di mesi per poter recuperare le ore di straordinario [mi dice di quanto posso tornare indietro].
					var elapsedMaxDate1 = new Date(record.event_year, record.event_month - scopes.globals.elapsedMaxMonth, 0);
					var dayEMD1 = '' + (elapsedMaxDate1.getDate());
					var monthEMD1 = '' + (elapsedMaxDate1.getMonth() + 1);
					var dateEMD1 = elapsedMaxDate1.getFullYear() + '-' + (monthEMD1.length > 1 ? monthEMD1 : '0' + monthEMD1) + '-' + (dayEMD1.length > 1 ? dayEMD1 : '0' + dayEMD1);
					application.output('dateElapsedMaxDate1: ' + dateEMD1);

					/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
					var el_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');

					//cerco per expired a 0, nel range di date per utente
					if (el_additional_charge.find()) {
						el_additional_charge.is_expired = 0;
						el_additional_charge.el_competence_month = ' ' + dateEMD1 + '...' + dateTC + ' |yyyy-MM-dd';
						el_additional_charge.user_id = record.user_id;
						resultToModify = el_additional_charge.search();
					}
					//ordinamento dal record più recente a quello più vecchio
					el_additional_charge.sort('el_year desc, el_month desc');

					/** @type {Number} */
					var recDuration = record.duration;
					/** @type {Array<String>} */
					var arrStrHistory = [];
					/** @type {Array<String>} */
					var elementArrStrHistoryMonth = [];
					/** @type {String} */
					var strHistoryPayments = '';
					/** @type {String} */
					var strToFind = '';
					var found = false;
					var numHistoryPayments = 0;

					//ciclo sui risultati trovati
					if (resultToModify > 0) {
						//durata da cancellare : a ogni ciclo sottraggo quanto ho già eliminato di recuero cancellato
						var durationToDelete = recDuration;
						for (var y = 1; y <= resultToModify; y++) {
							var recToModify = el_additional_charge.getRecord(y);
							strToFind = recToModify.el_year + '-' + recToModify.el_month;
							application.output(y + ' record additionalCharge da modificare: data ' + recToModify.el_year + recToModify.el_month + ' lav. ' + recToModify.duration_worked + ' rec. ' + recToModify.duration_retrieved + ' rim. ' + recToModify.duration_remained + ' H. ' + recToModify.history_payments)

							//caso migliore: durata rimasta + record.durata <= lavorate
							if (recToModify.duration_remained + durationToDelete <= recToModify.duration_worked) {
								//TODO verificare coi mesi successivi se non sono già stati inseriti recuperi che hanno consumato le ore di questo mese
								//sommo la durata del record alle ore rimanenti
								recToModify.duration_remained = recToModify.duration_remained + durationToDelete;
								//sottraggo la durata del record dalle ore recuperate
								recFound.duration_retrieved = recFound.duration_retrieved - durationToDelete
								application.output('rimanenti ' + recToModify.duration_remained + ' usate ' + recFound.duration_retrieved);
								//controllo history

								arrStrHistory = recFound.history_payments.split('|');
								application.output('arrStrHistoryPayments: ' + (arrStrHistory) + ' ; lunghezza: ' + arrStrHistory.length);
								found = false;
								for (var e = 0; e < arrStrHistory.length; e++) {
									elementArrStrHistoryMonth = arrStrHistory[e].split(':');
									application.output('posizione: ' + e + ' elementArrStrHistoryPayments: ' + (elementArrStrHistoryMonth));
									if (elementArrStrHistoryMonth != []) {
										application.output("STRINGA DA CERCARE : " + strToFind);
										if (elementArrStrHistoryMonth.indexOf(strToFind) != -1) {
											//faccio quello che devo fare
											found = true;
											strHistoryPayments = elementArrStrHistoryMonth[1];
											numHistoryPayments = utils.stringToNumber(strHistoryPayments);
											//sistemo l'array togliendo le ore che posso cancellare da quelle che ho come "recuperate per il mese"
											numHistoryPayments = numHistoryPayments - durationToDelete;
											application.output('numHistoryPayments - durationToDelete: ' + (numHistoryPayments));
											elementArrStrHistoryMonth[1] = numHistoryPayments;
											application.output('elementArrStrHistoryMonth[0]:[1]: ' + (elementArrStrHistoryMonth[0]) + ':' + (elementArrStrHistoryMonth[1]));
											arrStrHistory[e] = (elementArrStrHistoryMonth[0]) + ':' + (elementArrStrHistoryMonth[1]);
											application.output('arrStrHistory[e]: ' + (arrStrHistory[e]));
											application.output('arrStrHistory: ' + (arrStrHistory));
											break;
										}
									}
								}
								if (found == false) {
									application.output('---------- ERRORE history non trovata.');
								} else {
									recFound.history_payments = arrStrHistory.join('|');
								}
								break;
							}
							//Tolgo tutto quello che c'è sul record in corso e vado al prossimo record
							else {
								//se duration_worked è <= 0 vuol dire che non ci sono lavorate per quel mese (rimanenti = rimanenti + lavorate), e quindi salto a quello dopo.
								if (recToModify.duration_worked > 0) {
									//posso cancellare: ore lavorate - durata rimanente per la logica che
									//le ore rimanenti non possono superare le lavorate sul record che sto valutando in questa iterazione
									var durationCanDelete = recToModify.duration_worked - recToModify.duration_remained;
									application.output('durationCanDelete = ' + (durationCanDelete));
									//detraggo dal record da cui sto togliendo le ore di recupero, la durata parziale massima detraibile al record dell'iterazione
									recFound.duration_retrieved = recFound.duration_retrieved - durationCanDelete;
									application.output('recFound.duration_retrieved: ' + recFound.duration_retrieved);
									arrStrHistory = recFound.history_payments.split('|');
									application.output('arrStrHistoryPayments: ' + (arrStrHistory) + ' ; lunghezza: ' + arrStrHistory.length);
									found = false;
									for (e = 0; e < arrStrHistory.length; e++) {
										elementArrStrHistoryMonth = arrStrHistory[e].split(':');
										application.output('posizione: ' + e + ' elementArrStrHistoryPayments: ' + (elementArrStrHistoryMonth));
										//if (elementArrStrHistoryMonth != '[]') {
										if (elementArrStrHistoryMonth != []) {

											if (elementArrStrHistoryMonth.indexOf(strToFind).toString() != -1) {
												//faccio quello che devo fare
												found = true;
												strHistoryPayments = elementArrStrHistoryMonth[1];
												numHistoryPayments = utils.stringToNumber(strHistoryPayments);
												//ripritino il mese in cui avevo recuperato cancellando il massimo numero di ore che posso cancellare come cancellazione recupero
												numHistoryPayments = numHistoryPayments - durationCanDelete;
												application.output('numHistoryPayments - durata rimanante: ' + (numHistoryPayments));
												elementArrStrHistoryMonth[1] = numHistoryPayments;
												application.output('elementArrStrHistoryMonth[0]:[1]: ' + (elementArrStrHistoryMonth[0]) + ':' + (elementArrStrHistoryMonth[1]));
												arrStrHistory[e] = (elementArrStrHistoryMonth[0]) + ':' + (elementArrStrHistoryMonth[1]);
												application.output('arrStrHistory[e]: ' + (arrStrHistory[e]));
												application.output('arrStrHistory: ' + (arrStrHistory));
												break;
											}
										}
									}

									if (found == false) {
										application.output('---------- ERRORE history non trovata.');
									} else {
										recFound.history_payments = arrStrHistory.join('|');
									}
									//ore che devono ancora essere cancellate ai prossimi giri di for
									durationToDelete = durationToDelete - durationCanDelete;
									//alle ore da recuperare sommo quelle che ho effettiavamente cancellato come recuperate per la cancellazione del recupero strao
									recToModify.duration_remained = recToModify.duration_remained + durationCanDelete;
								}
							}
						}
					}
				}
				application.output("duration_worked DOPO MODIFICA " + recFound.duration_worked);
				application.output("duration_remained DOPO MODIFICA " + recFound.duration_remained);
				application.output("duration_retrieved DOPO MODIFICA " + recFound.duration_retrieved);
				application.output("duration_worked_rejected  DOPO MODIFICA " + recFound.duration_worked_rejected);
				application.output("duration_retrieved_rejected DOPO MODIFICA " + recFound.duration_retrieved_rejected);
			}
			databaseManager.saveData();
		}
	} catch (e) {
		application.output('Exception: ' + e);
	} finally {
		application.output(globals.messageLog + 'STOP events_log_additional_charge deleteAdditionalCharge - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
		databaseManager.setAutoSave(true);
	}
}

/**
 * @param {JSRecord<db:/geco/events_log>} record
 * @param {Number} action 1= strao; 2= riposo
 * @properties={typeid:24,uuid:"8EC45156-04A6-4AEF-A46C-2C473EF6DF58"}
 * @SuppressWarnings(wrongparameters)
 * @AllowToRunInFind
 */
function insertAdditionalCharge(record, action) {
	application.output(globals.messageLog + 'START events_log_additional_charge insertAdditionalCharge - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
	var result = 0; //record trovati prima ricerca riposo compensantivo e lavoro strao (mese anno utente)
	var resultNew = 0; //record trovati seconda ricerca per riposo compensantivo (range date, utente, expired a 0)
	var resultToModify = 0; //record trovati terza ricerca per riposo compensantivo

	//cerco se c'è già un record su events_log_additional_charge
	if (find()) {
		el_month = record.event_month;
		el_year = record.event_year;
		user_id = record.user_id;
		result = search();
	}
	application.output(globals.messageLog + 'events_log_additional_charge insertAdditionalCharge - Record trovati: ' + result, LOGGINGLEVEL.DEBUG);
	try {
		//record non trovato
		if (result == 0) {
			//sto inserendo lavoro strao con riposo compensativo
			if (action == 1) {
				//application.output('Sto creando il nuovo record per event_id = 28');
				application.output(globals.messageLog + 'events_log_additional_charge insertAdditionalCharge - Record da inserire (event_id = 28)', LOGGINGLEVEL.DEBUG);
				newRecord(false);
				el_competence_month = new Date(record.event_year, record.event_month, 0);
				el_year = record.event_year;
				el_month = record.event_month;
				user_id = record.user_id;
				duration_worked = record.duration;
				duration_remained = duration_remained + record.duration;
				is_expired = 0;
			}//sto inserendo riposo compensantivo
			else if (action == 2) {
				//application.output('Sto creando il nuovo record per event_id = 25');
				application.output(globals.messageLog + 'events_log_additional_charge insertAdditionalCharge - Record da inserire (event_id = 25)', LOGGINGLEVEL.DEBUG);
				var idx = newRecord(false);
				el_competence_month = new Date(record.event_year, record.event_month, 0);
				el_year = record.event_year;
				el_month = record.event_month;
				user_id = record.user_id;
				duration_retrieved = record.duration;
				is_expired = 0;

				//mi salvo il record da escludere nella ricerca
				var newAdditChargeRec = getRecord(idx);

				//mi calcolo la data inserita sul record in formato stringa
				var dateNewAdditChargeRec = newAdditChargeRec.el_competence_month;
				var dayNewAdditCharge = '' + (dateNewAdditChargeRec.getDate());
				var monthNewAdditCharge = '' + (dateNewAdditChargeRec.getMonth() + 1);
				var dateNewAdditCharge = dateNewAdditChargeRec.getFullYear() + '-' + (monthNewAdditCharge.length > 1 ? monthNewAdditCharge : '0' + monthNewAdditCharge) + '-' + (dayNewAdditCharge.length > 1 ? dayNewAdditCharge : '0' + dayNewAdditCharge);
				//application.output('dateNewAdditCharge: ' + dateNewAdditCharge);

				//mi calcolo la data massima precedente su cui posso cercare; scopes.globals.elapsedMaxMonth mi dice di quanto posso tornare indietro.
				var elapsedMaxDate = new Date(record.event_year, record.event_month - scopes.globals.elapsedMaxMonth, 0);
				var dayElapMaxDate = '' + (elapsedMaxDate.getDate());
				var monthElapMaxDate = '' + (elapsedMaxDate.getMonth() + 1);
				var dateElapMaxDate = elapsedMaxDate.getFullYear() + '-' + (monthElapMaxDate.length > 1 ? monthElapMaxDate : '0' + monthElapMaxDate) + '-' + (dayElapMaxDate.length > 1 ? dayElapMaxDate : '0' + dayElapMaxDate);
				//application.output('dateElapMaxDate: ' + dateElapMaxDate);

				application.output(globals.messageLog + 'events_log_additional_charge insertAdditionalCharge - Record (event_id = 25) Range date: ' + dateElapMaxDate + ' - ' + dateNewAdditCharge, LOGGINGLEVEL.DEBUG);
				//application.output('Range date: ' + dateElapMaxDate + ' - ' + dateNewAdditCharge);

				/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
				var events_log_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');

				//cerco tra tutti i record quelli con expired a 0 entro i 2 mesi dalla data del record appena inserito, escludendo quest'ultimo:
				//se sto inserendo come nuovo ovviamente non ho ore lavorate ergo escludo a prescindere il record appena inserito
				if (events_log_additional_charge.find()) {
					events_log_additional_charge.is_expired = 0;
					events_log_additional_charge.el_competence_month = ' ' + dateElapMaxDate + '...' + dateNewAdditCharge + ' |yyyy-MM-dd';
					events_log_additional_charge.user_id = record.user_id;
					events_log_additional_charge.el_additional_charge_id = '!' + newAdditChargeRec.el_additional_charge_id;
					resultNew = events_log_additional_charge.search();
				}//ordino i risultati trovati dal più vecchio al più recente
				events_log_additional_charge.sort('el_year asc, el_month asc');

				/** @type {Number} */
				var duration = record.duration;

				//ciclo sui record trovati
				if (resultNew > 0) {
					application.output("ACCIPIGNA SONO ENTRATO ");
					for (var index = 1; index <= resultNew; index++) {
						var recEL = events_log_additional_charge.getRecord(index);
						application.output(index + ' record additionalCharge: ' + recEL)
						//verifico se le ore rimaste del record a quella iterazione sono superiori o uguali alla durata del record inserito
						//caso migliore: posso detrarre dalle ore rimaste tutta la durata per intero
						//if (recEL.duration_remained >= duration) {
						if (recEL.duration_remained - recEL.duration_worked_rejected >= duration) {
							recEL.duration_remained = recEL.duration_remained - duration;
							//verifico se il nuovo record ha una history; se non ce l'ha la creo, altrimenti aggiungo i pezzi mancanti
							//evito ulteriori controlli perchè questo è il caso in cui sto inserendo un nuovo record, ergo all'iterazione
							//successiva (se c'è) avrò la detrazione delle ore rimaste su un altro mese
							if (globals.isEmpty(newAdditChargeRec.history_payments)) {
								newAdditChargeRec.history_payments = '' + recEL.el_year + '-' + recEL.el_month + ':' + duration + '|';
								application.output('newAdditChargeRec.history_payments: ' + (newAdditChargeRec.history_payments));
							} else {
								newAdditChargeRec.history_payments = '' + newAdditChargeRec.history_payments + recEL.el_year + '-' + recEL.el_month + ':' + duration + '|';
								application.output('newAdditChargeRec.history_payments: ' + (newAdditChargeRec.history_payments));
							}
							break;
						}//Tolgo tutto quello che c'è sul record dell'iterazione attuale e tolgo dal prossimo fino a rientrare nel primo caso
						else {
							//							if (recEL.duration_remained > 0) {
							if (recEL.duration_remained - recEL.duration_worked_rejected > 0) {
								if (globals.isEmpty(newAdditChargeRec.history_payments)) {
									newAdditChargeRec.history_payments = '' + recEL.el_year + '-' + recEL.el_month + ':' + recEL.duration_remained + '|';
									application.output('newAdditChargeRec.history_payments: ' + (newAdditChargeRec.history_payments));
								} else {
									newAdditChargeRec.history_payments = '' + newAdditChargeRec.history_payments + recEL.el_year + '-' + recEL.el_month + ':' + recEL.duration_remained + '|';
									application.output('newAdditChargeRec.history_payments: ' + (newAdditChargeRec.history_payments));
								}
								duration = duration - recEL.duration_remained;
								recEL.duration_remained = 0;
							}
						}
					}
				}
			}
		}//record trovato
		else if (result > 0) {
			//mi salvo il record trovato nella variabile recFound
			for (var index1 = 1; index1 <= result; index1++) {
				var recFound = getRecord(index1);
				if (action == 1) { //event_id = 28 | sommo le ore lavorative
					application.output('Record da modificare (event_id = 28): ' + recFound);
					recFound.duration_worked = recFound.duration_worked + record.duration;
					recFound.duration_remained = recFound.duration_remained + record.duration;
				}//event_id = 25 | inserisco ore di recupero, ricalcolo il resto
				else if (action == 2) { //event_id = 25 | sommo le ore da recuperare
					application.output('Record da modificare (event_id = 25): ' + recFound);
					//aggiungo al record da recuperare, visto che sto aggiungendo recupero
					recFound.duration_retrieved = recFound.duration_retrieved + record.duration;

					//mi calcolo la data inserita sul record trovato in formato stringa
					var dateToConfront = recFound.el_competence_month;
					var dayTC = '' + (dateToConfront.getDate());
					var monthTC = '' + (dateToConfront.getMonth() + 1);
					var dateTC = dateToConfront.getFullYear() + '-' + (monthTC.length > 1 ? monthTC : '0' + monthTC) + '-' + (dayTC.length > 1 ? dayTC : '0' + dayTC);
					application.output('dateToConfront: ' + dateTC);

					//mi calcolo la data massima precedente su cui posso cercare; scopes.globals.elapsedMaxMonth mi dice di quanto posso tornare indietro.
					var elapsedMaxDate1 = new Date(record.event_year, record.event_month - scopes.globals.elapsedMaxMonth, 0);
					var dayEMD1 = '' + (elapsedMaxDate1.getDate());
					var monthEMD1 = '' + (elapsedMaxDate1.getMonth() + 1);
					var dateEMD1 = elapsedMaxDate1.getFullYear() + '-' + (monthEMD1.length > 1 ? monthEMD1 : '0' + monthEMD1) + '-' + (dayEMD1.length > 1 ? dayEMD1 : '0' + dayEMD1);
					application.output('dateElapsedMaxDate1: ' + dateEMD1);

					/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
					var el_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');

					//cerco per expired a 0, nel range di date per utente
					if (el_additional_charge.find()) {
						el_additional_charge.is_expired = 0;
						el_additional_charge.el_competence_month = ' ' + dateEMD1 + '...' + dateTC + ' |yyyy-MM-dd';
						el_additional_charge.user_id = record.user_id;
						resultToModify = el_additional_charge.search();
					}//riordino dal record più vecchio a quello più recente

					el_additional_charge.sort('el_year asc, el_month asc');

					/** @type {Number} */
					var recDuration = record.duration;
					/** @type {Array<String>} */
					var arrStrHistory = [];
					/** @type {Array<String>} */
					var elementArrStrHistoryMonth = [];
					/** @type {String} */
					var strHistoryPayments = '';
					/** @type {String} */
					var strToFind = '';
					var found = false;
					var numHistoryPayments = 0;

					//ciclo sui risultati trovati
					if (resultToModify > 0) {
						for (var y = 1; y <= resultToModify; y++) {
							var recToModify = el_additional_charge.getRecord(y);
							strToFind = recToModify.el_year + '-' + recToModify.el_month;
							application.output(y + ' record additionalCharge da modificare: ' + recToModify)

							//caso migliore: posso detrarre dalle ore rimaste tutta la durata per intero
							//							if (recToModify.duration_remained >= recDuration) {
							if (recToModify.duration_remained - recToModify.duration_worked_rejected >= recDuration) {
								recToModify.duration_remained = recToModify.duration_remained - recDuration;

								if (globals.isEmpty(recFound.history_payments)) {
									recFound.history_payments = '' + recToModify.el_year + '-' + recToModify.el_month + ':' + recDuration + '|';
									application.output('recFound.history_payments: ' + (recFound.history_payments));
								}//il record da modificare ha già una sua storia, vedere se sul record in corso c'è qualcosa:
								//se è già presente bisogna sommare la durata a quello che già c'è altrimenti inserire come nuovo
								else {
									arrStrHistory = recFound.history_payments.split('|');
									application.output('arrStrHistoryPayments: ' + (arrStrHistory) + ' ; lunghezza: ' + arrStrHistory.length);
									found = false;

									for (var e = 0; e < arrStrHistory.length; e++) {
										elementArrStrHistoryMonth = arrStrHistory[e].split(':');
										application.output('posizione: ' + e + ' elementArrStrHistoryPayments: ' + (elementArrStrHistoryMonth));

										if (elementArrStrHistoryMonth != '[]') {
											if (elementArrStrHistoryMonth.indexOf(strToFind).toString() != -1) {
												//faccio quello che devo fare
												found = true;
												strHistoryPayments = elementArrStrHistoryMonth[1];
												numHistoryPayments = utils.stringToNumber(strHistoryPayments);
												numHistoryPayments = numHistoryPayments + recDuration;
												application.output('numHistoryPayments + recDuration: ' + (numHistoryPayments));
												elementArrStrHistoryMonth[1] = numHistoryPayments;
												application.output('elementArrStrHistoryMonth[0]:[1]: ' + (elementArrStrHistoryMonth[0]) + ':' + (elementArrStrHistoryMonth[1]));

												arrStrHistory[e] = (elementArrStrHistoryMonth[0]) + ':' + (elementArrStrHistoryMonth[1]);
												application.output('arrStrHistory[e]: ' + (arrStrHistory[e]));
												application.output('arrStrHistory: ' + (arrStrHistory));
												break;
											}
										}
									}

									if (found == false) {
										recFound.history_payments = '' + recFound.history_payments + recToModify.el_year + '-' + recToModify.el_month + ':' + recDuration + '|';
										//aggiungo in coda al record.
									} else {
										recFound.history_payments = arrStrHistory.join('|');
									}
								}

								break;
							}//Tolgo tutto quello che c'è sul record in corso e vado al prossimo record
							else {
								//verifico che la durata rimanente sia maggiore di 0, sennò salto al prossimo

								//								if (recToModify.duration_remained > 0) {
								if (recToModify.duration_remained - recToModify.duration_worked_rejected > 0) {
									if (globals.isEmpty(recFound.history_payments)) {
										recFound.history_payments = '' + recToModify.el_year + '-' + recToModify.el_month + ':' + recToModify.duration_remained + '|';
										application.output('recFound.history_payments: ' + (recFound.history_payments));
									} else {
										//										recFound.history_payments = '' + recFound.history_payments + recToModify.el_year + '-' + recToModify.el_month + ':' + recToModify.duration_remained + '|';
										//										application.output('recFound.history_payments: ' + (recFound.history_payments));
										arrStrHistory = recFound.history_payments.split('|');
										application.output('arrStrHistoryPayments: ' + (arrStrHistory) + ' ; lunghezza: ' + arrStrHistory.length);
										found = false;

										for (var f = 0; f < arrStrHistory.length; f++) {
											elementArrStrHistoryMonth = arrStrHistory[f].split(':');
											application.output('posizione: ' + f + ' elementArrStrHistoryPayments: ' + (elementArrStrHistoryMonth));

											if (elementArrStrHistoryMonth != '[]') {
												if (elementArrStrHistoryMonth.indexOf(strToFind).toString() != -1) {
													//faccio quello che devo fare
													found = true;
													strHistoryPayments = elementArrStrHistoryMonth[1];
													numHistoryPayments = utils.stringToNumber(strHistoryPayments);
													numHistoryPayments = numHistoryPayments + recToModify.duration_remained;
													application.output('numHistoryPayments + recDuration: ' + (numHistoryPayments));
													elementArrStrHistoryMonth[1] = numHistoryPayments;
													application.output('elementArrStrHistoryMonth[0]:[1]: ' + (elementArrStrHistoryMonth[0]) + ':' + (elementArrStrHistoryMonth[1]));

													arrStrHistory[f] = (elementArrStrHistoryMonth[0]) + ':' + (elementArrStrHistoryMonth[1]);
													application.output('arrStrHistory[f]: ' + (arrStrHistory[f]));
													application.output('arrStrHistory: ' + (arrStrHistory));
													break;
												}
											}
										}

										if (found == false) {
											//recFound.history_payments = '' + recFound.history_payments + recToModify.el_year + '-' + recToModify.el_month + ':' + recDuration + '|';
											recFound.history_payments = '' + recFound.history_payments + recToModify.el_year + '-' + recToModify.el_month + ':' + recToModify.duration_remained + '|';
											//aggiungo in coda al record.
										} else {
											recFound.history_payments = arrStrHistory.join('|');
										}

									}
									recDuration = recDuration - recToModify.duration_remained;
									recToModify.duration_remained = 0;
								}
							}
						}
					}

				}
			}
		}
	} catch (e) {
		application.output('Eccezione: ' + e);
	} finally {
		application.output(globals.messageLog + 'STOP events_log_additional_charge insertAdditionalCharge - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
	}
}

/**
 * REJECT EVENTS
 * @param {JSRecord<db:/geco/events_log>} record
 * @param {Number} action 1= reject; 2 approved from rejected
 * @properties={typeid:24,uuid:"361D38FE-783F-44EB-9C54-DC3B1BBFE356"}
 * @SuppressWarnings(wrongparameters)
 * @AllowToRunInFind
 */
function updateAdditionalCharge(record, action) {
	//	application.output('updateAdditionalCharge ' + action);
	//	var result = 0;
	//	loadAllRecords();
	//	if (find()) {
	//		el_month = record.event_month;
	//		el_year = record.event_year;
	//		user_id = record.user_id;
	//		result = search();
	//	}
	//
	//	if (result > 0) {
	//		var hoursWorkedRejected = duration_worked_rejected;
	//		var hoursRetrievedRejected = duration_retrieved_rejected;
	//		// strao
	//		if (record.event_id == 28) {
	//			//reject by approver
	//			if (action == 1)
	//				duration_worked_rejected = hoursWorkedRejected + record.duration;
	//
	//			//cerco i recuperi che hanno consumato le ore inserite e rifiuto fino ad avere ore recuperato <= inerite strao-rejected_worked
	//			//approved after reject
	//			if (action == 2)
	//				duration_worked_rejected = hoursWorkedRejected - record.duration;
	//		}
	//		//recupero
	//		else if (record.event_id == 25) {
	//			//reject by approver
	//			if (action == 1)
	//				duration_retrieved_rejected = hoursRetrievedRejected + record.duration;
	//
	//			//approved after reject
	//			if (action == 2)
	//				duration_retrieved_rejected = hoursRetrievedRejected - record.duration;
	//		}
	//	}
	//	//ultima operazione
	//	clear();
	application.output('updateAdditionalCharge ' + action);
	/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
	var additionalChargeFoundset = databaseManager.getFoundSet('geco', 'events_log_additional_charge');
	additionalChargeFoundset.clear();
	application.output("Record ID :  " + record.event_log_id);
	var result = 0;
	if (additionalChargeFoundset.find()) {
		additionalChargeFoundset.el_month = record.event_month;
		additionalChargeFoundset.el_year = record.event_year;
		additionalChargeFoundset.user_id = record.user_id;
		result = additionalChargeFoundset.search();
	}
	//ne dovrebbe trovare solo uno
	if (result > 0) {
		application.output("trovati : " + result);
		for (var index = 1; index <= additionalChargeFoundset.getSize(); index++) {
			var rec = additionalChargeFoundset.getRecord(index);
			application.output("ID RECORD SU ADDITIONAL CHARGE " + rec.el_additional_charge_id);
			var hoursWorkedRejected = rec.duration_worked_rejected;
			var hoursRetrievedRejected = rec.duration_retrieved_rejected;
			// strao
			if (record.event_id == 28) {
				//reject by approver
				if (action == 1)
					rec.duration_worked_rejected = hoursWorkedRejected + record.duration;

				//cerco i recuperi che hanno consumato le ore inserite e rifiuto fino ad avere ore recuperato <= inerite strao-rejected_worked
				//approved after reject
				if (action == 2)
					rec.duration_worked_rejected = hoursWorkedRejected - record.duration;
			}
			//recupero
			else if (record.event_id == 25) {
				//reject by approver
				if (action == 1)
					rec.duration_retrieved_rejected = hoursRetrievedRejected + record.duration;
				//approved after reject
				if (action == 2)
					rec.duration_retrieved_rejected = hoursRetrievedRejected - record.duration;
			}
		}
	}
}
//FS
/**
 *Aggiorna il campo duration_retrieved_rejected della tabella events_log_additional_charge
 * @param {JSRecord<db:/geco/events_log_additional_charge>} recordToUpdate
 * @param {Number}	hoursRetrievedRejected
 * @properties={typeid:24,uuid:"2176C19C-0B26-4A26-806C-782FB7DD5566"}
 */
function updateDurationRetrievedRejected(recordToUpdate, hoursRetrievedRejected) {
	if (hoursRetrievedRejected >= recordToUpdate.duration_retrieved) {
		application.output('-----updateDurationRetrievedRejected() hoursRetrievedRejected >= recordToUpdate.duration_retrieved');
		recordToUpdate.duration_retrieved_rejected = recordToUpdate.duration_retrieved;
	} else {
		application.output('-----updateDurationRetrievedRejected() hoursRetrievedRejected MINORE di recordToUpdate.duration_retrieved');
		recordToUpdate.duration_retrieved_rejected = hoursRetrievedRejected;
	}
}
//Setter duration_worked_rejected
/**
 * @param {JSRecord<db:/geco/events_log_additional_charge>} recordToUpdate
 * @param {Number}	hoursWorkedRejected
 *
 * @properties={typeid:24,uuid:"B34BC564-B499-43B1-92AC-B1CEF40689A1"}
 */
function setDurationWorkedRejected(recordToUpdate, hoursWorkedRejected) {
	//	if (hoursWorkedRejected >= recordToUpdate.duration_worked) {
	//		application.output('-----setDurationWorkedRejected() hoursRetrievedRejected >= di recordToUpdate.duration_retrieved');
	//		recordToUpdate.duration_worked_rejected = recordToUpdate.duration_worked;
	//	}
	//	else {
	application.output('-----setDurationWorkedRejected() hoursRetrievedRejected MINORE di recordToUpdate.duration_retrieved');
	recordToUpdate.duration_worked_rejected = hoursWorkedRejected;
	//	}
}
/**
 * @param {JSRecord<db:/geco/events_log_additional_charge>} recordToUpdate
 * @return {Number} duration_worked_rejected
 *
 * @properties={typeid:24,uuid:"04C013C4-2951-49E3-BEC1-2E816DD81AC3"}
 */
function getDurationWorkedRejected(recordToUpdate) {

	return recordToUpdate.duration_worked_rejected;
}
