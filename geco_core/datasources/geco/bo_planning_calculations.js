/**
 * @properties={type:8,typeid:36,uuid:"EFF2FADE-DF5B-4FEE-9EDF-549C80DCC478"}
 */
function mol_per_display()
{
	var mol = 0;
	mol = (total_return - total_cost)*100/total_return;
	mol = Math.round(mol*Math.pow(10,2))/Math.pow(10,2);
	return mol;
}

/**
 * @properties={type:8,typeid:36,uuid:"CED006D9-7BC3-4FBE-AD7E-9DA80982C13F"}
 */
function mol_display()
{
	var mol = 0;
	mol = (total_return - total_cost);
	mol = Math.round(mol*Math.pow(10,2))/Math.pow(10,2);
	return mol;
}
