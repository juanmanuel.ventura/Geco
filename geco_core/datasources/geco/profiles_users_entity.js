/**
 * @SuppressWarnings(wrongparameters)
 * @properties={typeid:24,uuid:"C91CCB21-4949-4BD9-9E17-78CC3E32D037"}
 */
function onSort() {
	sort('profile_user_start_date desc');
}

/**
 * JStaffa dato lo userID dell'utente, restituisce l'id del record con data maggiore
 * @param {Number} userID
 * @return recID
 * @properties={typeid:24,uuid:"E4C65929-9106-4EB3-AE99-5612483F2088"}
 * @AllowToRunInFind
 */
function getLastID(userID) {
	var recID = null;
	if (!globals.isEmpty(userID)) {
		/** @type {JSFoundSet<db:/geco/profiles_users>} */
		var profiles_users = databaseManager.getFoundSet('geco', 'profiles_users');
		if (profiles_users.find()) {
			profiles_users.user_id = userID;
			if (profiles_users.search() > 0) {
				profiles_users.sort('profile_user_start_date desc');
				recID = profiles_users.getRecord(1).profile_user_id;
			}
		}
	}
	return recID;
}

/**
 * @param {JSRecord<db:/geco/profiles_users>} record record that will be inserted
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"F6C9F3C9-1066-4E9E-BD43-1C1C0529EAEB"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}

/**
 * @param {JSRecord<db:/geco/profiles_users>} record record that will be updated
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"A1B6F8EC-F69D-4110-8D71-20EEFA84B375"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate');
}

/**
 * @param {JSRecord<db:/geco/profiles_users>} record record that will be deleted
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"C07249BF-193A-4B80-A1D3-76F829F3218F"}
 */
function onRecordDelete(record) {
	return globals.applyAllRules(record, 'onDelete');
}
