/**
 * Record pre-delete trigger.
 * Validate the record to be deleted.
 * When false is returned the record will not be deleted in the database.
 * When an exception is thrown the record will also not be deleted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/rules>} record record that will be deleted
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"15755926-BF16-4CD4-9661-C4827A3A6C3F"}
 */
function onRecordDelete(record) {
	if (record.working_entity) {
		globals.DIALOGS.showErrorDialog("Impossibile eliminare", "Questa regola è associata all'entity [" + record.working_entity + "] quindi non può essere eliminata", 'OK')
		return false;
	}
	return true;
}


/**
 * Record after-insert trigger.
 *
 * @param {JSRecord<db:/geco/rules>} record record that is inserted
 *
 * @private
 *
 * @properties={typeid:24,uuid:"9C85620E-2FBC-4D51-BDCB-9AEEB5B121A6"}
 */
function afterRecordInsert(record) {
	scopes.globals.loadRunnableRules();
}

/**
 * Record after-update trigger.
 *
 * @param {JSRecord<db:/geco/rules>} record record that is updated
 *
 * @private
 *
 * @properties={typeid:24,uuid:"8EDFBD7C-1CE3-49F8-8847-62F5FF8AEE1D"}
 */
function afterRecordUpdate(record) {
	scopes.globals.loadRunnableRules();
}
