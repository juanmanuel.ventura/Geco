
/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/events>} record record that will be inserted
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"62E1F102-50AD-4A48-81B4-9AF524D672DB"}
 */
function onRecordInsert(record) {
  return globals.applyAllRules(record, 'onInsert') && setDisplays(record);
}

/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/events>} record record that will be updated
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"6B9D85BE-4ACC-4CB3-8EFB-9F0232299D58"}
 */
function onRecordUpdate(record) {
  return globals.applyAllRules(record, 'onUpdate') && setDisplays(record);
}


/**
 * @param {JSRecord<db:/geco/events>} record record that will be updated
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"22C9727E-5AA7-4EBA-A81D-87DA223ACED9"}
 */
function setDisplays(record) {
	record.fep_name_display = record.external_code_fep + " - " +record.event_name;
	record.navision_name_display = record.external_code_navision + " - " +record.event_name;
	return true;
}
