/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/jo_planning>} record record that will be updated
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"E221461A-D10B-4A48-94C8-EA563BBA5732"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate');
}

/**
 * Record after-update trigger.
 *
 * @param {JSRecord<db:/geco/jo_planning>} record record that is updated
 *
 * @properties={typeid:24,uuid:"2FD119CD-B48E-41B1-A18A-A89A0354D036"}
 */
function updateJoDetailsActual(record) {
	application.output(globals.messageLog + 'START jo_planning_entity.updateJoDetailsActual() ', LOGGINGLEVEL.DEBUG);
	if (record.is_actual == 1) {
		var totPlain = record.jo_planning_to_jo_details.jo_details_to_jo_planning;
		//	application.output(totPlain.getSize())
		totPlain.loadRecords();
		//	application.output(totPlain.getSize())
		totPlain.sort('date_actual desc');
		var lastUserActual = totPlain.user_actual;
		//	application.output('piano update costo actual ' + globals.roundNumberWithDecimal(record.jo_planning_to_jo_details.jo_details_to_jo_planning.total_cost_actual, 2) + ' profit actual ' + globals.roundNumberWithDecimal(record.jo_planning_to_jo_details.jo_details_to_jo_planning.total_return_actual, 2))

		record.jo_planning_to_jo_details.setProfitCostActual(record.jo_planning_to_jo_details.getRecord(1),
			globals.roundNumberWithDecimal(record.jo_planning_to_jo_details.jo_details_to_jo_planning.total_return_actual, 2),
			globals.roundNumberWithDecimal(record.jo_planning_to_jo_details.jo_details_to_jo_planning.total_cost_actual, 2),
			globals.roundNumberWithDecimal(record.jo_planning_to_jo_details.jo_details_to_jo_planning.sum_total_days_actual, 2),
			record.days_import_actual,
			record.jo_planning_to_jo_details.jo_details_to_jo_planning.last_competence_month,
			record.jo_planning_to_jo_details.jo_details_to_jo_planning.last_actual_date,
			lastUserActual)

		record.jo_planning_to_jo_details.updateJoAmount(record.jo_planning_to_jo_details.getRecord(1));
		databaseManager.saveData(record.jo_planning_to_jo_details);
		databaseManager.saveData(record.jo_planning_to_job_orders)
		databaseManager.recalculate(record.jo_planning_to_jo_details);
		//application.output(record.jo_planning_to_jo_details)
		//application.output('DOPO piano update costo actual ' + globals.roundNumberWithDecimal(record.jo_planning_to_jo_details.jo_details_to_jo_planning.total_cost_actual, 2) + ' profit actual ' + globals.roundNumberWithDecimal(record.jo_planning_to_jo_details.jo_details_to_jo_planning.total_return_actual, 2))
		//application.output('piani C ' + record.jo_planning_to_job_orders.job_orders_to_jo_planning.total_cost_actual)
		//application.output('piani R ' + record.jo_planning_to_job_orders.job_orders_to_jo_planning.total_return_actual)
		//application.output('dettagli C ' + record.jo_planning_to_job_orders.job_orders_to_jo_details.total_cost_actual)
		//application.output('dettagli R ' + record.jo_planning_to_job_orders.job_orders_to_jo_details.total_return_actual)

	}
	application.output(globals.messageLog + 'STOP jo_planning_entity.updateJoDetailsActual() ', LOGGINGLEVEL.DEBUG);
}

/**
 * Record after-insert trigger.
 *
 * @param {JSRecord<db:/geco/jo_planning>} record record that is inserted
 *
 * @properties={typeid:24,uuid:"D8C57761-8B71-4370-9434-90B74E10F182"}
 */
function insertJoDetailsActual(record) {
	application.output(globals.messageLog + 'START jo_planning_entity.insertJoDetailsActual() ', LOGGINGLEVEL.DEBUG);
	if (record.is_actual == 1) {
		var totPlain = record.jo_planning_to_jo_details.jo_details_to_jo_planning;
		//application.output(totPlain.getSize())

		totPlain.loadRecords();
		//application.output(totPlain.getSize())
		totPlain.sort('date_actual desc');
		var lastUserActual = totPlain.user_actual;
		//application.output('piano update costo actual ' + globals.roundNumberWithDecimal(record.jo_planning_to_jo_details.jo_details_to_jo_planning.total_cost_actual, 2) + ' profit actual ' + globals.roundNumberWithDecimal(record.jo_planning_to_jo_details.jo_details_to_jo_planning.total_return_actual, 2))
		record.jo_planning_to_jo_details.setProfitCostActual(record.jo_planning_to_jo_details.getRecord(1),
			globals.roundNumberWithDecimal(record.jo_planning_to_jo_details.jo_details_to_jo_planning.total_return_actual, 2),
			globals.roundNumberWithDecimal(record.jo_planning_to_jo_details.jo_details_to_jo_planning.total_cost_actual, 2),
			globals.roundNumberWithDecimal(record.jo_planning_to_jo_details.jo_details_to_jo_planning.sum_total_days_actual, 2),
			record.days_import_actual,
			record.jo_planning_to_jo_details.jo_details_to_jo_planning.last_competence_month,
			record.jo_planning_to_jo_details.jo_details_to_jo_planning.last_actual_date,
			lastUserActual);
		
		record.jo_planning_to_jo_details.updateJoAmount(record.jo_planning_to_jo_details.getRecord(1));
	}
	application.output(globals.messageLog + 'STOP jo_planning_entity.insertJoDetailsActual() ', LOGGINGLEVEL.DEBUG);
}
