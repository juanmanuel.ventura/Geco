
/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/users_events>} record record that will be inserted
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"C8368AA1-9F32-43CE-AD48-91605559FAB4"}
 */
function onRecordInsert(record) {
//	var totUserEvents = 0;
//	application.output('inserimento user_id = '+ record.user_id +' evento ' +record.event_id);
//	try {
//		if(find()){
//			user_id = record.user_id;
//			event_id = record.event_id;
//			totUserEvents = search(true,true);
//		}
//		application.output('eventi trovati  ' + totUserEvents);
//	} catch (e) {
//		application.output('ERRORE inserisco user_id = '+ record.user_id +' evento ' +record.event_id + '  ' +e );
//	}
//	application.output('FINE inserimento user_id = '+ record.user_id +' evento ' +record.event_id);
	return globals.applyAllRules(record, 'onInsert');
}
