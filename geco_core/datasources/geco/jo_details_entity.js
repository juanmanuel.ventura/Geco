/**
 * @param {JSRecord<db:/geco/jo_details>} record record that will be inserted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"0819D8DD-AE17-4437-BC2A-4FB986EA776A"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}

/**
 * @param {JSRecord<db:/geco/jo_details>} record record that will be updated
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"093E96B2-09DC-4137-8EC8-F14B5F192ED4"}
 */
function onRecordUpdate(record) {
	if (record.action) {
		application.output("Skipping rules as '" + record.action + "' action detected");
		return true;
	}
	return globals.applyAllRules(record, 'onUpdate')
}

/**
 * Record after-insert trigger.
 * @param {JSRecord<db:/geco/jo_details>} record record that is inserted
 * @private
 * @properties={typeid:24,uuid:"8F635900-5654-4361-A4C1-6A836A089C75"}
 */
function addJoAmount(record) {
	application.output(globals.messageLog + 'START jo_details_entity.addJoAmount() ', LOGGINGLEVEL.DEBUG);
	record.jo_details_to_job_orders.job_orders_to_jo_details.loadRecords();
//	JStaffa non deve ricalcolare il budget all'inserimento
//	record.jo_details_to_job_orders.setProfitCost(record.jo_details_to_job_orders.getRecord(1), 'updateProfitCostJO', globals.roundNumberWithDecimal(record.jo_details_to_job_orders.job_orders_to_jo_details.total_return_d, 2), globals.roundNumberWithDecimal(record.jo_details_to_job_orders.job_orders_to_jo_details.total_cost_d, 2))
	application.output(globals.messageLog + 'STOP jo_details_entity.addJoAmount()  JO profit= ' + record.jo_details_to_job_orders.profit + ' JO cost= ' + record.jo_details_to_job_orders.cost, LOGGINGLEVEL.DEBUG);

	//	application.output(globals.messageLog + 'START jo_details_entity.addJoAmount() ' , LOGGINGLEVEL.DEBUG);
	//	application.output('prima----- costo JO = ' + record.jo_details_to_job_orders.cost + ' ricavo JO = ' + record.jo_details_to_job_orders.profit);
	//	scopes.globals.roundNumberWithDecimal((record.jo_details_to_job_orders.profit + record.return_amount),2);
	//	record.jo_details_to_job_orders.cost = scopes.globals.roundNumberWithDecimal((record.jo_details_to_job_orders.cost + record.cost_amount),2);
	//	record.jo_details_to_job_orders.profit = scopes.globals.roundNumberWithDecimal((record.jo_details_to_job_orders.profit + record.return_amount),2);
	//	application.output('dopo------ costo JO = ' + record.jo_details_to_job_orders.cost + ' ricavo JO = ' + record.jo_details_to_job_orders.profit)
	//	//updateMol(record);
	//	application.output(globals.messageLog + 'STOP jo_details_entity.addJoAmount() ' , LOGGINGLEVEL.DEBUG);
}

/**
 * Record after-delete trigger.
 * @param {JSRecord<db:/geco/jo_details>} record record that is deleted
 * @private
 * @properties={typeid:24,uuid:"092F0FE2-27A6-427D-85BD-484F61E6B19C"}
 */
function delJoAmount(record) {
	application.output(globals.messageLog + 'START jo_details_entity.delJoAmount() ', LOGGINGLEVEL.DEBUG);
	record.jo_details_to_job_orders.job_orders_to_jo_details.loadRecords();
//	JStaffa non deve ricalcolare il budget dopo la cancellazione
//	record.jo_details_to_job_orders.setProfitCost(record.jo_details_to_job_orders.getRecord(1), 'updateProfitCostJO', globals.roundNumberWithDecimal(record.jo_details_to_job_orders.job_orders_to_jo_details.total_return_d, 2), globals.roundNumberWithDecimal(record.jo_details_to_job_orders.job_orders_to_jo_details.total_cost_d, 2))
	databaseManager.saveData(record.jo_details_to_job_orders);
	application.output(globals.messageLog + 'STOP jo_details_entity.delJoAmount()  JO profit= ' + record.jo_details_to_job_orders.profit + ' JO cost= ' + record.jo_details_to_job_orders.cost, LOGGINGLEVEL.DEBUG);

	//	application.output(globals.messageLog + 'START jo_details_entity.delJOAmount() ' , LOGGINGLEVEL.DEBUG);
	//	application.output('prima----- costo JO = ' + record.jo_details_to_job_orders.cost + ' ricavo JO = ' + record.jo_details_to_job_orders.profit)
	//	record.jo_details_to_job_orders.cost = scopes.globals.roundNumberWithDecimal((record.jo_details_to_job_orders.cost - record.cost_amount),2);
	//	record.jo_details_to_job_orders.profit = scopes.globals.roundNumberWithDecimal((record.jo_details_to_job_orders.profit - record.return_amount),2);
	//	application.output('dopo------ costo JO = ' + record.jo_details_to_job_orders.cost + ' ricavo JO = ' + record.jo_details_to_job_orders.profit)
	//	application.output(globals.messageLog + 'STOP jo_details_entity.delJOAmount() ' , LOGGINGLEVEL.DEBUG);
	//	//updateMol(record);
	//databaseManager.saveData(record.jo_details_to_job_orders);
}

/**
 * Record after-update trigger.
 * @param {JSRecord<db:/geco/jo_details>} record record that is updated
 *
 * @properties={typeid:24,uuid:"E5B008A3-5433-49D9-8ECE-031012E53744"}
 */
function updateJoAmount(record) {
	application.output(globals.messageLog + 'START jo_details_entity.updateJoAmount() ' + record.jo_details_id, LOGGINGLEVEL.DEBUG);
	record.jo_details_to_job_orders.job_orders_to_jo_details.loadRecords();
//	JStaffa non deve ricalcolare il budget alla modifica
//	record.jo_details_to_job_orders.setProfitCost(record.jo_details_to_job_orders.getRecord(1), 'updateProfitCostJO',
//		globals.roundNumberWithDecimal(record.jo_details_to_job_orders.job_orders_to_jo_details.total_return_d, 2),
//		globals.roundNumberWithDecimal(record.jo_details_to_job_orders.job_orders_to_jo_details.total_cost_d, 2))

	record.jo_details_to_job_orders.setProfitCostActual(record.jo_details_to_job_orders.getRecord(1), 'updateProfitCostActualJO',
		globals.roundNumberWithDecimal(record.jo_details_to_job_orders.job_orders_to_jo_details.total_return_actual_d, 2),
		globals.roundNumberWithDecimal(record.jo_details_to_job_orders.job_orders_to_jo_details.total_cost_actual_d, 2),
		record.jo_details_to_job_orders.job_orders_to_jo_details.jo_details_to_jo_planning.last_competence_month,
		record.jo_details_to_job_orders.job_orders_to_jo_details.jo_details_to_jo_planning.last_competence_month,
		record.jo_details_to_job_orders.job_orders_to_jo_details.jo_details_to_jo_planning.user_actual);
	application.output(globals.messageLog + 'STOP jo_details_entity.updateJoAmount()  ' + record.jo_details_id + ' JO profit= ' + record.jo_details_to_job_orders.profit + ' JO cost= ' + record.jo_details_to_job_orders.cost, LOGGINGLEVEL.DEBUG);
}

/**
 * @param {JSRecord<db:/geco/jo_details>} record record that will be updated
 * @param {Number} newProfitActual
 * @param {Number} newCostActual
 * @param {Number} daysNumActual
 * @param {Number} dayImportActual
 * @param {Date} lastCompetenceMonth
 * @param {Date} lastActualDate
 * @param {Number} userIdActual
 * @properties={typeid:24,uuid:"FEABE7FC-AB59-4BA9-8FA7-B5D4E27EF31D"}
 */
function setProfitCostActual(record, newProfitActual, newCostActual, daysNumActual, dayImportActual , lastCompetenceMonth, lastActualDate, userIdActual) {
	application.output(globals.messageLog + 'START jo_details_entity.setProfitCostActual() ' + record.jo_details_id, LOGGINGLEVEL.DEBUG);
	application.output(newProfitActual + ' | '+ newCostActual+ ' | '+ lastCompetenceMonth+ ' | '+ lastActualDate+ ' | '+ userIdActual)
	record.action = 'updateJoDetailsProfitCostActual';
	record.return_actual = newProfitActual;
	record.cost_actual = newCostActual;
	if (record.jo_details_to_profit_cost_types.profit_cost_acr == 'R') record.total_actual = newProfitActual;
	if (record.jo_details_to_profit_cost_types.profit_cost_acr == 'C') record.total_actual = newCostActual;
	record.days_import_actual = dayImportActual;
	record.days_number_actual = daysNumActual;
	record.competence_month = lastCompetenceMonth;
	record.date_actual = lastActualDate;
	record.user_actual = userIdActual
	application.output(record.return_actual + ' | ' + record.cost_actual + ' | ' + record.competence_month + ' | ' + record.date_actual + ' | ' + record.user_actual)
	if (record.jo_details_to_job_orders.job_order_type == 0) {
		application.output('COMMESSA INTERNA');
		record.total_amount = newCostActual;
		record.cost_amount = newCostActual;
		record.days_import = dayImportActual;
		record.days_number = daysNumActual;
	}
	var saved = databaseManager.saveData(record);
	record.jo_details_to_job_orders.job_orders_to_jo_details.loadRecords();

	application.output('salvato ' + saved)
	application.output(record.return_actual + ' | ' + record.cost_actual + ' | ' + record.competence_month + ' | ' + record.date_actual + ' | ' + record.user_actual + ' | ' + record.cost_amount + ' | ' + record.total_amount)
//	application.output('Dettaglio actual profit ' + globals.roundNumberWithDecimal(record.jo_details_to_job_orders.job_orders_to_jo_details.total_return_actual, 2) + ' costo actual ' + globals.roundNumberWithDecimal(record.jo_details_to_job_orders.job_orders_to_jo_details.total_cost_actual, 2))
	application.output(globals.messageLog + 'STOP jo_details_entity.setProfitCostActual() ' + record.jo_details_id, LOGGINGLEVEL.DEBUG);
}
