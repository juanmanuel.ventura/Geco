
/**
 * @param {JSRecord<db:/geco/comm_supp_profit_centers>} record record that will be inserted
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"DA6B04B1-DD98-484D-8F8D-47453AE79530"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}

/**
 * @param {JSRecord<db:/geco/comm_supp_profit_centers>} record record that will be updated
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"92EB51BE-F1FD-4568-A20A-1A77B15B80CC"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate');
}

/**
 * @param {JSRecord<db:/geco/comm_supp_profit_centers>} record record that will be deleted
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"68944CC8-A72B-413F-8973-2DA7F42AF502"}
 */
function onRecordDelete(record) {
	return globals.applyAllRules(record, 'onDelete');
}
