/**
 * @properties={typeid:24,uuid:"CD866177-458A-4743-9591-D22FA751AD41"}
 * @AllowToRunInFind
 */
function updateEventsLogUser()
{
	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	if (events_log.find()) {
		events_log.user_id = user_id;
		events_log.event_log_status = '1';
		events_log.events_log_to_events.approver_figure = '2';
		//modifico l'approvatore dell'evneto in modo che quando salva gli eventi lo ricalcoli
		if (events_log.search() > 0) {
			for (var index = 1; index <= events_log.getSize(); index++) {
				//if (events_log.event_log_date >=dateChange){
					var rec = events_log.getRecord(index);
					rec.user_approver_id = 0;
				//}
			}
		}
	}
}

/**
 * @param {Number} userid
 * @param {Number} oldPc
 * @param {Number} newPc
 * @param {Date} changeDate
 * @properties={typeid:24,uuid:"EFC41ED7-986F-4040-9CE8-4069CEFE96D0"}
 * @SuppressWarnings(wrongparameters)
 */
function insertHistory(userid, oldPc, newPc, changeDate) {
	try {
		//databaseManager.setAutoSave(false);
		newRecord();
		user_id = userid;
		previous_prof_center_id = oldPc;
		actual_prof_center_id = newPc;
		start_change_date = changeDate;
	//	databaseManager.saveData();
	} catch (e) {
		throw e;
	} finally {
	//	databaseManager.setAutoSave(true);
	}
}
