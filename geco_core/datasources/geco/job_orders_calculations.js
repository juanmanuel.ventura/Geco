/**
 * @properties={type:8,typeid:36,uuid:"9ACC55DE-3D4B-41CC-8172-5B0DCD2FB85B"}
 */
function jo_budget_mol_percentage()
{
	var mol = 0;
	mol = (profit - cost )*100/Math.abs(profit);
	mol = Math.round(mol*Math.pow(10,2))/Math.pow(10,2);
	return mol;
}

/**
 * @properties={type:8,typeid:36,uuid:"48DA8D6C-F7D4-4246-91FB-2FB1E4A8BB7A"}
 */
function jo_budget_mol()
{
	return profit - cost;
}

///**
// * @properties={type:93,typeid:36,uuid:"E051F5AB-DE5A-49BE-AB69-3A9928ACC512"}
// * @param {Date} date_approver_from
// */
//function date_approver_from()
//{
//	return null;
//}

/**
 * @properties={type:8,typeid:36,uuid:"17D51C70-472F-44C6-B725-C5197D5A61A0"}
 */
function total_offer()
{
	var offerValue = 0
	if (job_orders_to_business_opportunities){
		for (var i = 1; i <= job_orders_to_business_opportunities.getSize(); i++){
			offerValue = offerValue + (job_orders_to_business_opportunities.last_order_amount == 1 ? job_orders_to_business_opportunities.order_amount : job_orders_to_business_opportunities.offer_amount)
		}
	}
	return offerValue;
}

/**
 * @properties={type:8,typeid:36,uuid:"BE31488F-C033-4773-86DD-C6851D962598"}
 */
function is_actual_reject()
{

	if (jo_to_jo_planning_reject_by_controller.record_count > 0 || jo_to_jo_planning_reject_by_rpc.record_count > 0)
		return 1;
	else return 0;
}

/**
 * @properties={type:4,typeid:36,uuid:"7BDD0988-8108-4DD7-B5DD-3EACEC6941C4"}
 */
function status_actual_planning(){
	if (jo_to_jo_planning_reject_by_controller.record_count > 0 || jo_to_jo_planning_reject_by_rpc.record_count > 0)
		return 3;
	else if ((jo_to_jo_planning_confirmed_rdc.record_count + jo_to_jo_planning_confirmed_rpc.record_count) > 0)
		return 2;
	else if ((jo_to_jo_planning_confirmed_controller.record_count + jo_to_jo_planning_confirmed_posting.record_count) > 0)
		return 4;
	else return 1;
}

///**
// *
// * @properties={type:4,typeid:36,uuid:"F802F1E8-5634-4CE2-90B0-F3E196DDF2FD"}
// */
//function status_actual_planning_contr(){
//	if (jo_to_jo_planning_contr_reject_by_controller.record_count > 0 || jo_to_jo_planning_contr_reject_by_rpc.record_count > 0)
//		return 3;
//	else if ((jo_to_jo_planning_contr_confirmed_rdc.record_count + jo_to_jo_planning_contr_confirmed_rpc.record_count) > 0)
//		return 2;
//	else if ((jo_to_jo_planning_contr_confirmed_controller.record_count + jo_to_jo_planning_contr_confirmed_posting.record_count) > 0)
//		return 4;
//	else return 1;
//}

/**
 * @properties={type:12,typeid:36,uuid:"8A52D3AD-1908-4F86-AE25-F9E26C303BD9"}
 */
function action()
{
	return null;
}

/**
 * @properties={type:8,typeid:36,uuid:"DFA3F161-5FD8-4EAC-B228-DF7972EFE137"}
 */
function eac_return()
{
	//EAC = Actual+Pianificato non actual
	return (job_orders_to_jo_planning_actual.total_return_actual + job_orders_to_jo_planning_not_actual.total_return);
	//return (job_orders_to_jo_planning.total_return - job_orders_to_jo_planning.total_return_actual);
}

/**
 * @properties={type:8,typeid:36,uuid:"49E2C7F2-4BBE-44EE-985F-C12C1D8024D9"}
 */
function eac_cost(){
	//actual + pianificato futuro(non actual)
	//EAC = Actual+Pianificato non actual
	return (job_orders_to_jo_planning_actual.total_cost_actual + job_orders_to_jo_planning_not_actual.total_cost);
	//return (job_orders_to_jo_planning.total_cost - job_orders_to_jo_planning.total_cost_actual);
}

/**
 * @properties={type:4,typeid:36,uuid:"7A97DD32-CF84-4663-9FA7-2DE5A7E16DB1"}
 */
function is_selected() {
	return 0;
}

/**
 * @properties={type:4,typeid:36,uuid:"4C0EF9BC-9B0C-4780-85B7-C4F6FE6F4668"}
 */
function current_approver_id() {
	return profit_centers_to_approvers.getApprover('job_orders', job_order_id, new Date)
}

/**
 * @properties={typeid:36,uuid:"69F359BC-7834-46AE-82A2-65E112643BA7"}
 */
function eac_mol_display(){
	var mol = 0;
	mol = (eac_return - eac_cost);
	mol = Math.round(mol*Math.pow(10,2))/Math.pow(10,2);
	return mol;
}

/**
 * @properties={typeid:36,uuid:"23A814AD-C7F3-464E-84FD-B9126E099CE1"}
 */
function eac_mol_per_display()
{
	var mol = 0;
	if (eac_return!=null)
		mol = (eac_return - eac_cost)*100/Math.abs(eac_return);
	mol = Math.round(mol*Math.pow(10,2))/Math.pow(10,2);
	return mol;
}

/**
 * @properties={type:8,typeid:36,uuid:"7B287C8C-261D-4CE6-B50C-897B7E37191D"}
 */
function mol_forecast_display()
{
	//application.output('mol forecast display ' + job_orders_to_jo_planning_not_actual.total_return +'  ' + job_orders_to_jo_planning_not_actual.total_cost + ' ' + job_orders_to_jo_planning_not_actual.record_count)
	return (job_orders_to_jo_planning_not_actual.total_return - job_orders_to_jo_planning_not_actual.total_cost);
}

/**
 * @properties={type:8,typeid:36,uuid:"913F0292-A717-4FEB-9C90-D30CA1A7F559"}
 */
function mol_forecast_per_display(){
	var mol = 0;
	if (job_orders_to_jo_planning_not_actual.total_return!=null) 
		mol = (job_orders_to_jo_planning_not_actual.total_return - job_orders_to_jo_planning_not_actual.total_cost)*100/Math.abs(job_orders_to_jo_planning_not_actual.total_return);
	mol = Math.round(mol*Math.pow(10,2))/Math.pow(10,2);
	return mol;
}
