/**
 * @properties={type:12,typeid:36,uuid:"526974D0-4906-45D3-AC12-553A75A884C5"}
 */
function action()
{
	return null;
}

/**
 * @properties={type:8,typeid:36,uuid:"DB4DA4BE-202F-40FA-815F-1255727E689D"}
 */
function mol_per_display()
{
	var mol_per = 0;
	mol_per = (profit - cost)*100/profit;
	mol_per = Math.round(mol_per*Math.pow(10,2))/Math.pow(10,2);
	return mol_per;
}

/**
 * @properties={type:8,typeid:36,uuid:"91F77AAD-1DB9-449E-A184-BFFB00833F7E"}
 */
function mol_display()
{
	var mol_disp = 0;
	mol_disp = (profit - cost);
	mol_disp = Math.round(mol_disp*Math.pow(10,2))/Math.pow(10,2);
	return mol_disp;
}

/**
 * @properties={type:8,typeid:36,uuid:"0D2A353F-B44B-4B64-90BA-AED7BB2A1761"}
 */
function has_documents()
{
	if (business_opportunities_to_bo_bo_documents 
		&& business_opportunities_to_bo_bo_documents.getSize()>0
		&& business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents != null 
		&& business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.getSize()>0){
		return 1;
	}
	else return 0;
}
