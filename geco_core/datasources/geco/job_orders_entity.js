/**
 * @param {JSRecord<db:/geco/job_orders>} record record that will be updated
 * @param {String} act
 * @param {Number} newProfit
 * @param {Number} newCost
 * @properties={typeid:24,uuid:"B7C58725-3E5D-4925-A67C-49E046113E93"}
 */
function setProfitCost(record, act, newProfit, newCost )
{
	application.output(globals.messageLog + 'START job_orders_entity.setProfitCost() ' +record.job_order_id , LOGGINGLEVEL.DEBUG);
	application.output(newProfit + ' | '+ newCost);
	record.action = act;
	record.profit = newProfit;
	record.cost = newCost;
	application.output(newProfit + ' | '+ newCost);
	application.output(globals.messageLog + 'STOPjob_orders_entity.setProfitCost() ' +record.job_order_id , LOGGINGLEVEL.DEBUG);
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record record that will be updated
 * @param {String} act
 * @param {Number} newProfitActual
 * @param {Number} newCostActual
 * @param {Date} lastCompetenceMonth
 * @param {Date} lastActualDate
 * @param {Number} userIdActual
 * @properties={typeid:24,uuid:"8D13D31E-F807-4EDF-8CAE-945AD1A841E2"}
 */
function setProfitCostActual(record, act, newProfitActual, newCostActual, lastCompetenceMonth, lastActualDate, userIdActual )
{
	application.output(globals.messageLog + 'START job_orders_entity.setProfitCostActual() ' +record.job_order_id , LOGGINGLEVEL.DEBUG);
	application.output(newProfitActual + ' | '+ newCostActual+ ' | '+ lastCompetenceMonth+ ' | '+ lastActualDate+ ' | '+ userIdActual)
	record.action = 'updateProfitCostActualJO';
	record.profit_actual = newProfitActual;
	record.cost_actual = newCostActual;
	record.competence_month = lastCompetenceMonth;
	record.date_actual = lastActualDate;
	record.user_actual = userIdActual;
	application.output(newProfitActual + ' | '+ newCostActual+ ' | '+ lastCompetenceMonth+ ' | '+ lastActualDate+ ' | '+ userIdActual)
	application.output(globals.messageLog + 'STOP job_orders_entity.setProfitCostActual() ' +record.job_order_id , LOGGINGLEVEL.DEBUG);
}

/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/job_orders>} record record that will be inserted
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"52AE333F-2FB6-4E51-9243-417BB8BD1BB8"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert') && setDisplays(record);
}

/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/job_orders>} record record that will be updated
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"FD2CB20C-7C57-4B0C-910B-42C23D637F21"}
 */
function onRecordUpdate(record) {
	if (record.action) {
		application.output("Skipping rules as '" + record.action + "' action detected");
		record.action= null;
		return true;
	}
	return globals.applyAllRules(record, 'onUpdate') && setDisplays(record);
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record record that will be updated
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"1BBC91B6-8F66-4BB9-82E7-F6B0A7AD80DE"}
 */
function setDisplays(record) {
	record.title_display = record.external_code_navision + " - " + record.job_order_title;
	record.external_code_navision = record.external_code_navision.replace(' ', '');
	return true;
}

/**
 * Record after-insert trigger.
 * @param {JSRecord<db:/geco/job_orders>} record record that is inserted
 * @private
 * @properties={typeid:24,uuid:"522182F5-FAB0-46EA-8121-006473F3B82D"}
 * @AllowToRunInFind
 */
function afterRecordInsert(record) {
	//inserire la copia dei dettagli della BO nella tabella jo_details e la copia della pianificazione nella tabella jo_planning
	application.output(globals.messageLog + 'START job_order_entity.afterRecordInsert() ', LOGGINGLEVEL.INFO);
	if (record.customer_order_code != null)
		insertDetailsPlanning(record);
	application.output(globals.messageLog + 'STOP job_order_entity.afterRecordInsert() ', LOGGINGLEVEL.INFO);
}

/**
 * @param {JSRecord<db:/geco/job_orders>} record record that is updated/inserted
 * @properties={typeid:24,uuid:"699D61F7-B3D6-4D26-9BFE-D1AA569D3D6C"}
 * @AllowToRunInFind
 */
function insertDetailsPlanning(record) {
	application.output(globals.messageLog + 'START job_order_entity.insertDetailsPlanning() ', LOGGINGLEVEL.INFO);
	var result = 0;
	var boNum = 0;

	/** @type {JSFoundSet<db:/geco/business_opportunities>} */
	var business_opportunity = databaseManager.getFoundSet('geco', 'business_opportunities');
	if (business_opportunity.find()) {
		business_opportunity.bo_number = record.customer_order_code;
		boNum = business_opportunity.search();
	}

	/** @type {JSFoundSet<db:/geco/bo_details>} */
	var bo_details = databaseManager.getFoundSet('geco', 'bo_details');
	if (bo_details.find()) {
		bo_details.bo_details_to_bo.bo_number = record.customer_order_code;
		result = bo_details.search();
	}
	if (result == 0) {
		application.output('Esito ricerca Dettagli BO ' + result);
	}
	
	/** @type {JSFoundSet<db:/geco/bo_actual_date>} */
	var bo_actual_date = databaseManager.getFoundSet('geco', 'bo_actual_date');
	bo_actual_date.loadRecords();
	var actual_date = bo_actual_date.actual_date;
	var isOpenToActual = bo_actual_date.is_open_to_actual;

	if (boNum == 1) {
		application.output('date bo ' + business_opportunity.start_date + ' fine ' + business_opportunity.end_date)
		if (record.valid_from == null) record.valid_from = business_opportunity.start_date;
		if (record.valid_to == null || record.valid_to < business_opportunity.end_date) record.valid_to = business_opportunity.end_date;
		application.output('date commessa ' + record.valid_from + ' fine ' + record.valid_to)
		record.bo_id = business_opportunity.bo_id;
		//		record.cost = business_opportunity.cost;
		//		record.profit = business_opportunity.profit;
		record.lob_id = business_opportunity.lob_id;
		record.tow_id = business_opportunity.tow_id;
		record.job_order_year = business_opportunity.bo_year;
		record.profit = business_opportunity.profit;
		record.cost = business_opportunity.cost;
		business_opportunity.job_order_id = record.job_order_id;
		business_opportunity.cod_commessa = record.external_code_navision;
		business_opportunity.action = 'new_job_order';
		application.output('bo_details trovati ' + result)
		if (record.job_order_type == 4 || record.job_order_type == 0){
			//gestionale o interna non copio i dettagli BO
			application.output(globals.messageLog + 'STOP job_order_entity.insertDetailsPlanning() progetto GESTIONELAE o INTERNO non copia i dettagli della BO ', LOGGINGLEVEL.INFO);
			return
		}
		for (var index = 1; index <= bo_details.getSize(); index++) {
			//crea il dettaglio relativo alla BO
			var rec = bo_details.getRecord(index);
			application.output(rec.bo_details_id + ' - ' + rec.bo_id);
			application.output('creo record jo_details')
			record.job_orders_to_jo_details.newRecord();
			record.job_orders_to_jo_details.bo_id = rec.bo_id;
			record.job_orders_to_jo_details.bo_details_id = rec.bo_details_id;
			record.job_orders_to_jo_details.cost_amount = rec.cost_amount;
			record.job_orders_to_jo_details.days_import = rec.days_import;
			record.job_orders_to_jo_details.days_number = rec.days_number;
			record.job_orders_to_jo_details.description = rec.figure;
			record.job_orders_to_jo_details.jo_id = record.job_order_id;
			record.job_orders_to_jo_details.profit_cost_type_id = rec.profit_cost_type_id;
			record.job_orders_to_jo_details.real_figure = rec.real_figure;//
			record.job_orders_to_jo_details.real_tk_supplier = rec.real_tk_supplier;
			record.job_orders_to_jo_details.real_tm_figure = rec.real_tm_figure;//
			record.job_orders_to_jo_details.return_amount = rec.return_amount;
			record.job_orders_to_jo_details.standard_figure = rec.standard_figure;
			record.job_orders_to_jo_details.standard_import = rec.standard_import;
			record.job_orders_to_jo_details.total_amount = rec.total_amount;
			record.job_orders_to_jo_details.enrollment_number = rec.enrollment_number;
			//user_id per tipologia se TM o personale
			var uid = (rec.real_figure != null) ? rec.real_figure : rec.real_tm_figure;
			if (rec.bo_details_to_bo_planning != null && rec.bo_details_to_bo_planning.getSize() > 0) {
				//crea pianificazione relativa al dettaglio
				var totDays = 0;
				var totAmount = 0;
				var totCost = 0;
				var totReturn = 0;
				for (var i = 1; i <= rec.bo_details_to_bo_planning.getSize(); i++) {
					var rec_plain = rec.bo_details_to_bo_planning.getRecord(i);
					application.output(rec_plain.bo_planning_id + ' - ' + rec.bo_details_id + ' - ' + rec.bo_id);
					application.output('creo record jo_plain')
					var dataPlain = new Date(rec_plain.bo_year, rec_plain.bo_month, 0)
					application.output('Data Piano ' + dataPlain);
					if (dataPlain < actual_date || (dataPlain == actual_date && isOpenToActual == 0)) {
						//sommo gli importi del piano
						totDays = scopes.globals.roundNumberWithDecimal( (totDays + rec_plain.days_number), 1);
						totAmount = scopes.globals.roundNumberWithDecimal( (totAmount + rec_plain.total_amount), 2);
						totCost = scopes.globals.roundNumberWithDecimal( (totCost + rec_plain.cost_amount), 2);
						totReturn = scopes.globals.roundNumberWithDecimal( (totReturn + rec_plain.return_amount), 2);

					} else {
						//inserisco il piano
						record.job_orders_to_jo_details.jo_details_to_jo_planning.newRecord();
						record.job_orders_to_jo_details.jo_details_to_jo_planning.days_import = rec_plain.days_import;
						record.job_orders_to_jo_details.jo_details_to_jo_planning.bo_details_id = rec_plain.bo_details_id;
						record.job_orders_to_jo_details.jo_details_to_jo_planning.bo_id = rec_plain.bo_id;
						record.job_orders_to_jo_details.jo_details_to_jo_planning.jo_details_id = record.job_orders_to_jo_details.jo_details_id;
						record.job_orders_to_jo_details.jo_details_to_jo_planning.jo_id = record.job_order_id;
						record.job_orders_to_jo_details.jo_details_to_jo_planning.jo_month = rec_plain.bo_month;
						record.job_orders_to_jo_details.jo_details_to_jo_planning.jo_year = rec_plain.bo_year;
						record.job_orders_to_jo_details.jo_details_to_jo_planning.user_id = uid;
						if (totCost > 0 || totReturn > 0) {
							record.job_orders_to_jo_details.jo_details_to_jo_planning.cost_amount = scopes.globals.roundNumberWithDecimal( (totCost + rec_plain.cost_amount), 2);
							record.job_orders_to_jo_details.jo_details_to_jo_planning.days_number = scopes.globals.roundNumberWithDecimal( (totDays + rec_plain.days_number), 2);
							record.job_orders_to_jo_details.jo_details_to_jo_planning.return_amount = scopes.globals.roundNumberWithDecimal( (totReturn + rec_plain.return_amount), 2);
							record.job_orders_to_jo_details.jo_details_to_jo_planning.total_amount = scopes.globals.roundNumberWithDecimal( (totAmount + rec_plain.total_amount), 2);
						} else {
							record.job_orders_to_jo_details.jo_details_to_jo_planning.cost_amount = rec_plain.cost_amount;
							record.job_orders_to_jo_details.jo_details_to_jo_planning.days_number = rec_plain.days_number;
							record.job_orders_to_jo_details.jo_details_to_jo_planning.return_amount = rec_plain.return_amount;
							record.job_orders_to_jo_details.jo_details_to_jo_planning.total_amount = rec_plain.total_amount;
						}
						totDays = 0;
						totAmount = 0;
						totCost = 0;
						totReturn = 0;
					}
				}
			}

		}

	} else {
		application.output(globals.messageLog + 'BO non trovata '+record.customer_order_code, LOGGINGLEVEL.INFO);
	}
	application.output(globals.messageLog + 'STOP job_order_entity.insertDetailsPlanning() ', LOGGINGLEVEL.INFO);
}
