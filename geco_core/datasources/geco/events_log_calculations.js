/**
 * @properties={type:8,typeid:36,uuid:"E03A2676-C850-473D-9330-D9086A8E30A5"}
 */
function total_day_display()
{
	var w_h = events_log_to_users.working_hours;
	if (w_h == 0) w_h = 8;
	var total_days = (total_duration/60)/w_h;
	return total_days;
}

/**
 * @properties={type:12,typeid:36,uuid:"60D04A08-2CDE-43B0-9922-1EDBDFB703CD"}
 */
function total_duration_overtime_display() {
	return globals.formatMinutes(total_duration_overtime);
}

/**
 * @properties={type:12,typeid:36,uuid:"0DEFAC91-B4CE-4413-845E-DFC391897D7A"}
 */
function total_duration_regular_display() {
	return globals.formatMinutes(total_duration_regular);
}

/**
 * @properties={type:12,typeid:36,uuid:"F1F7799D-F38A-448E-AD43-0F76D1199164"}
 */
function total_duration_display() {
	return globals.formatMinutes(total_duration);
}

/**
 * Virtual Record field to determine the action performed.
 * Returns null by default.
 *
 * Possible cases are:
 * 		"approving"
 * 		"rejecting"
 * 		"closing"
 *
 * @return {String}
 * @properties={type:12,typeid:36,uuid:"E29B9F9A-B49A-4515-A28A-36198F740B16"}
 */
function action() {
	return null;
}

/**
 * @return {Number}
 * @properties={type:4,typeid:36,uuid:"61621FAE-FEB0-49DD-96E1-17CA9A0A4A1B"}
 */
function is_selected() {
	return 0;
}
