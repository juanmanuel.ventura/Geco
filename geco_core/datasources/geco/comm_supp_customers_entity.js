/**
 * @param {JSRecord<db:/geco/comm_supp_customers>} record record that will be inserted
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"D9B94854-34C8-4752-85C2-A2F2FE2A7E98"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}

/**
 * @param {JSRecord<db:/geco/comm_supp_customers>} record record that will be updated
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"0A23C5D4-4385-44D1-8EAE-C9CE3E73339A"}
 */
function onRecordUpdate(record) {

	return globals.applyAllRules(record, 'onUpdate');
	
//	// if an specific action was triggered don't run the whole rule set
//	if (record.action) {
//		application.output("Skipping rules as '" + record.action + "' action detected");
//		return true;
//	}
//
//	// run all rules
//	return scopes.globals.applyAllRules(record, 'onUpdate') && setDisplays(record);
}