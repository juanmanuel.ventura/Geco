/**
 * @param {JSRecord<db:/geco/profit_centers>} record record that will be inserted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"08673A69-68BE-4251-90EC-D44DE12BB2C3"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}

/**
 * @param {JSRecord<db:/geco/profit_centers>} record record that will be updated
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"59E7DD57-23A4-4438-8C12-56205AE68617"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate');
}

/**
 * Record pre-delete trigger.
 * Validate the record to be deleted.
 * When false is returned the record will not be deleted in the database.
 * When an exception is thrown the record will also not be deleted in the database
 * but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/profit_centers>} record record that will be deleted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"4124CD03-6086-41AC-B265-C4A15E781A79"}
 */
function onRecordDelete(record) {
	return globals.applyAllRules(record, 'onDelete');
}
