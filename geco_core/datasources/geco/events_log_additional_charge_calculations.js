/**
 * @properties={type:8,typeid:36,uuid:"1742AE6E-258B-4323-8913-D1F861291716"}
 */
function durationRemained()
{
	/** @type {Number} */
	var durRem = (duration_remained/60);
//	application.output('duration_remained: ' + (scopes.globals.roundNumberWithDecimal(durRem,2)));
	return scopes.globals.roundNumberWithDecimal(durRem,2);
}

/**
 * @properties={type:8,typeid:36,uuid:"C08D432B-6FDF-49F1-A26C-F8CEDB1250DB"}
 */
function sumDurationRemained()
{
	/** @type {Number} */
	var durRem = (hoursRemained/60);
//	application.output('sum duration_remained: ' + (scopes.globals.roundNumberWithDecimal(durRem,2)));
	return scopes.globals.roundNumberWithDecimal(durRem,2);
}

/**
 * @properties={type:8,typeid:36,uuid:"E10B80C6-02CA-4CE3-A9D1-48E492B5F9E0"}
 */
function durationRetrieved()
{
	/** @type {Number} */
	var durRet = (duration_retrieved/60);
//	application.output('duration_retrieved: ' + (scopes.globals.roundNumberWithDecimal(durRet,2)));
	return scopes.globals.roundNumberWithDecimal(durRet,2);
}

/**
 * @properties={type:8,typeid:36,uuid:"47B09FC6-7CD1-4863-80A6-107A0CC67084"}
 */
function sumDurationRetrieved()
{
	/** @type {Number} */
	var durRet = (hoursRetrieved/60);
//	application.output('sum duration_retrieved: ' + (scopes.globals.roundNumberWithDecimal(durRet,2)));
	return scopes.globals.roundNumberWithDecimal(durRet,2);
}

/**
 * @properties={type:8,typeid:36,uuid:"BC40965A-D9C8-4DA5-B654-436BC5A55071"}
 */
function durationWorked()
{
	/** @type {Number} */
	var durWor = (duration_worked/60);
//	application.output('duration_worked: ' + (scopes.globals.roundNumberWithDecimal(durWor,2)));
	return scopes.globals.roundNumberWithDecimal(durWor,2);
}

/**
 * @properties={type:8,typeid:36,uuid:"6428EE4F-5CFE-4C25-AAE6-3BBD24D181B2"}
 */
function sumDurationWorked()
{
	/** @type {Number} */
	var durWor = (hoursWorked/60);
//	application.output('sum duration_worked: ' + (scopes.globals.roundNumberWithDecimal(durWor,2)));
	return scopes.globals.roundNumberWithDecimal(durWor,2);
}

/**
 * @properties={type:12,typeid:36,uuid:"CA14154B-9373-4456-BD4B-D2940051D9CF"}
 */
function month_to_name()
{
	return (scopes.globals.monthName[el_month-1]);
}

/**
 * @properties={type:8,typeid:36,uuid:"7C09E006-33C7-4D90-81B9-9C6DE574739B"}
 */
function durationRejectedWorked()
{
	/** @type {Number} */
	var durWor = (duration_worked_rejected/60);
//	application.output('duration_worked: ' + (scopes.globals.roundNumberWithDecimal(durWor,2)));
	return scopes.globals.roundNumberWithDecimal(durWor,2);
}

/**
 * @properties={type:8,typeid:36,uuid:"0FFE3BF1-EB13-4067-BAE0-EA67FEECFCA1"}
 */
function sumDurationRejectedWorked()
{
	/** @type {Number} */
	var durWor = (hoursRejectedWork/60);
//	application.output('sum duration_worked: ' + (scopes.globals.roundNumberWithDecimal(durWor,2)));
	return scopes.globals.roundNumberWithDecimal(durWor,2);
}


/**
 * @properties={type:8,typeid:36,uuid:"A9143BFD-C5C2-4631-A8D9-7BAE8FD5AD15"}
 */
function durationRejectedRetr()
{
	/** @type {Number} */
	var durWor = (duration_retrieved_rejected/60);
//	application.output('duration_worked: ' + (scopes.globals.roundNumberWithDecimal(durWor,2)));
	return scopes.globals.roundNumberWithDecimal(durWor,2);
}

/**
 * @properties={type:8,typeid:36,uuid:"9C4ADE61-A3F6-4E72-8C1C-744595745FDA"}
 */
function sumDurationRejectedRetr()
{
	/** @type {Number} */
	var durWor = (hoursRejectedRetr/60);
//	application.output('sum duration_worked: ' + (scopes.globals.roundNumberWithDecimal(durWor,2)));
	return scopes.globals.roundNumberWithDecimal(durWor,2);
}
