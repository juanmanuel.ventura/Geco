/**
 * Virtual Record field to determine the action performed. 
 * Returns null by default.
 * 
 * Possible cases are:
 * 		"approving"
 * 		"rejecting"
 * 		"closing"
 * 
 * @return {String}
 * @properties={type:12,typeid:36,uuid:"8CC1F54E-97DD-4408-ACDB-CD65AF307377"}
 */
function action() {
	return null;
}

/**
 * @properties={type:4,typeid:36,uuid:"97D85B77-BB22-4AB1-9EAC-B2F1EB09E520"}
 */
function is_selected() {
	return 0;
}
