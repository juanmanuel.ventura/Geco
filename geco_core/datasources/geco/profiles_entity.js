/**
 * @param {JSRecord<db:/geco/profiles>} record record that will be inserted
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"8658A194-AB40-4D0B-9ACE-00BB837EDA31"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}

/**
 * @param {JSRecord<db:/geco/profiles>} record record that will be updated
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"C671AAA0-4B0F-4550-9FFB-1F1EF282D269"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate');
}

/**
 * @param {JSRecord<db:/geco/profiles>} record record that will be deleted
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"C4DCFB57-00EA-4F7E-906A-ACE344C89F1B"}
 */
function onRecordDelete(record) {
	return globals.applyAllRules(record, 'onDelete');
}