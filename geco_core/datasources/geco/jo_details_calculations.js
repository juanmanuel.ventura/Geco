/**
 * @properties={type:4,typeid:36,uuid:"8A72BBCC-D0DA-4B37-B7A7-C054B7874CB0"}
 */
 function is_actual_reject()
 {
 	if (jo_details_to_jo_planning_reject_by_controller.record_count > 0 || jo_details_to_jo_planning_reject_by_rpc.record_count > 0)
 		return 1;
 	else return 0;
 }

/**
 * @properties={type:12,typeid:36,uuid:"F9C5334F-2D66-4543-90D5-2779850D3AE1"}
 */
function action()
{
	return null;
}

/**
 * @return {Number}
 * @properties={type:4,typeid:36,uuid:"26050600-8D7B-45FB-BD51-B17E333F3227"}
 */
function pl_status()
{
	if (jo_details_to_jo_planning_reject_by_controller.record_count > 0) return 9;
	if (jo_details_to_jo_planning_reject_by_rpc.record_count > 0 ) return 7;
	if (jo_details_to_jo_planning_confirmed_rdc.record_count > 0) return 1;
	if (jo_details_to_jo_planning_confirmed_respprofcent.record_count > 0) return 2;
	return null;
}

/**
 * @properties={type:8,typeid:36,uuid:"F5DC294D-CC80-481B-B4BC-23E3F8560E8E"}
 */
function forecast_cost()
{
	return (total_cost_d - total_cost_actual_d);
}

/**
 * @properties={type:8,typeid:36,uuid:"D1FCD753-A181-4BF1-BBA7-F4C053B00D72"}
 */
function forecast_return()
{
	return (total_return_d - total_return_actual_d);
}

/**
 * @properties={type:8,typeid:36,uuid:"08DB8F0B-6D67-42F1-BC2D-4C90447B817D"}
 */
function mol_actual_display()
{
	var mol = 0;
	if (total_return_actual_d && total_cost_actual_d)
		mol = (total_return_actual_d - total_cost_actual_d );
	mol = Math.round(mol*Math.pow(10,2))/Math.pow(10,2);
	return mol;
}

/**
 * @properties={type:8,typeid:36,uuid:"50D92E8C-2CDB-448F-8D4A-B46E1F16CB14"}
 */
function mol_per_forecast_display()
{
	var cost_forecast = total_cost_d - total_cost_actual_d;
	var return_forecast = total_return_d - total_return_actual_d;
	var mol = 0;
	if (return_forecast && cost_forecast)
		mol = (return_forecast - cost_forecast)*100/Math.abs(return_forecast);
	mol = Math.round(mol*Math.pow(10,2))/Math.pow(10,2);
	return mol;
}

/**
 * @properties={type:8,typeid:36,uuid:"6533CC52-FFAC-4F1E-8A23-75EB88F0C6C3"}
 */
function mol_forecast_display()
{
	var cost_forecast = total_cost_d - total_cost_actual_d;
	var return_forecast = total_return_d - total_return_actual_d;
	var mol = 0;
	if (return_forecast && cost_forecast)
		mol = (return_forecast - cost_forecast );
	mol = Math.round(mol*Math.pow(10,2))/Math.pow(10,2);
	return mol;
}

/**
 * @properties={type:8,typeid:36,uuid:"4D0E46CE-7E2F-4E89-BEF4-1E75E2FEA3DC"}
 */
function mol_per_actual_display()
{
	var mol = 0;
	if (total_return_actual_d && total_cost_actual_d)
		mol = (total_return_actual_d - total_cost_actual_d)*100/Math.abs(total_return_actual_d);
	mol = Math.round(mol*Math.pow(10,2))/Math.pow(10,2);
	return mol;
}

/**
 * @return {Number}
 * @properties={type:8,typeid:36,uuid:"FF076D8D-5097-4483-8C70-0214FE0A3532"}
 */
function mol_per_display()
{
	var mol = 0;
	mol = (total_return_d - total_cost_d )*100/Math.abs(total_return_d);
	mol = Math.round(mol*Math.pow(10,2))/Math.pow(10,2);
	return mol;
}

/**
 * @properties={type:8,typeid:36,uuid:"3B1D2605-3D3F-4915-957F-5AECFB9D73CB"}
 */
function mol_dispaly()
{
	//application.output('dettaglio ' + total_return_d + ' ' + total_cost_d );
	var mol = 0;
	mol = (total_return_d - total_cost_d );
	mol = Math.round(mol*Math.pow(10,2))/Math.pow(10,2);
	return mol;
}

/**
 * @properties={type:8,typeid:36,uuid:"A6CA6AAE-FAE8-47ED-8846-930878893C8D"}
 */
function eac_return()
{
	//actual + pianificato futuro(non actual)
	//Actual+Pianificato non actual
	return(jo_details_to_jo_planning_actual.total_return_actual + jo_details_to_jo_planning_not_actual.total_return);
	//return (jo_details_to_jo_planning.total_return - jo_details_to_jo_planning.total_return_actual);
}

/**
 * @properties={type:8,typeid:36,uuid:"4B98056A-4E8D-499C-956A-BF83C79801EC"}
 */
function eac_cost(){
	//actual + pianificato futuro(non actual)
	//Actual+Pianificato non actual
	return(jo_details_to_jo_planning_actual.total_cost_actual + jo_details_to_jo_planning_not_actual.total_cost);
	//return (jo_details_to_jo_planning.total_cost - jo_details_to_jo_planning.total_cost_actual);
}

/**
 * @properties={type:8,typeid:36,uuid:"6707A0F3-3451-4024-9B36-154260CE2296"}
 */
function eac_amount(){
	//actual + pianificato futuro(non actual)
	//Actual+Pianificato non actual - il total_amount delle fatture
	var total_amount_not_invoice = jo_details_to_jo_planning_not_actual.sum_total_amount - jo_details_to_jo_planning_not_actual.total_invoice_active - jo_details_to_jo_planning_not_actual.total_invoice_payable
	return(jo_details_to_jo_planning_actual.sum_total_amount_actual + total_amount_not_invoice);
	//return (jo_details_to_jo_planning.total_cost - jo_details_to_jo_planning.total_cost_actual);
}
