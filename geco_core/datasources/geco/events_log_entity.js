/**
 * @param {JSRecord<db:/geco/events_log>} record record that will be updated
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"FBFFD8E5-99C1-4371-970F-CAF816ED9616"}
 */
function onRecordUpdate(record) {

	// if an specific action was triggered don't run the whole rule set
	if (record.action) {
		application.output("Skipping rules as '" + record.action + "' action detected");
		return true;
	}

	// run all rules
	return scopes.globals.applyAllRules(record, 'onUpdate') && setDisplays(record);
}

/**
 * @param {JSRecord<db:/geco/events_log>} record record that will be updated
 * @private
 * @return {Boolean}
 * @properties={typeid:24,uuid:"8F3852AC-3B1D-4939-9AD4-13C6506BA114"}
 */
function setDisplays(record) {
	record.time_start_display = globals.formatMinutes(record.time_start);
	record.time_stop_display = globals.formatMinutes(record.time_stop);
	record.duration_display = globals.formatMinutes(record.duration);

	// separate regular work and overtime for further aggregation
	record.duration_regular = (record.events_log_to_events.is_overtime == 0) ? record.duration : 0;
	record.duration_overtime = (record.events_log_to_events.is_overtime == 1) ? record.duration : 0;

	return true;
}

/**
 * @param {JSRecord<db:/geco/events_log>} record record that will be inserted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"7677D62B-D810-42F9-9D2B-C288853D901D"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert') && setDisplays(record);
}

/**
 * @param {JSRecord<db:/geco/events_log>} record record that will be updated
 * @param {Number} status
 * @param {String} actionNote
 * @properties={typeid:24,uuid:"EA41D93C-7683-4EE5-B29D-8CB3BB2120EF"}
 */
function setSatus(record, status, actionNote) {

	if (status == 2) record.action = "approving";
	if (status == 3) record.action = "rejecting";
	if (status == 4) record.action = "closing";

	record.event_log_status = status;
	if (status == 2 || status == 3) record.actual_approver_id = globals.currentUserId;
	
	if (actionNote) record.note = actionNote;
}

/**
 * Record pre-delete trigger.
 * Validate the record to be deleted.
 * When false is returned the record will not be deleted in the database.
 * When an exception is thrown the record will also not be deleted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/events_log>} record record that will be deleted
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"20C2655B-408A-4179-B030-58B4AB20FA50"}
 */
function onRecordDelete(record) {
	return globals.applyAllRules(record, 'onDelete') && setDisplays(record);
}

/**
 * @param {JSRecord<db:/geco/events_log>} record record
 * return {Object} 
 * @properties={typeid:24,uuid:"C4F829DD-4E07-4D2F-AAA1-60F714716CF7"}
 * @AllowToRunInFind
 */
function getOrdinaryStartStop(record) {
	var objStartStop = {
		startOrd: null,
		stopOrd: null
	};
	var result = 0;
	if (record.events_log_to_events.is_overtime == 1) {
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log = databaseManager.getFoundSet('geco', 'events_log');
		if (events_log.find()) {
			events_log.event_log_date = record.event_log_date;
			events_log.user_id = record.user_id;
			//cerco solo lavoro ordinario
			events_log.event_id = 0;
			result = events_log.search();
			events_log.sort('time_start asc');
		}
		if (result > 0) {
//			var startOrd = null;
//			var stopOrd = null;
			for (var index = 1; index <= events_log.getSize(); index++) {
				var recordOrd = events_log.getRecord(index);
				objStartStop.startOrd = (objStartStop.startOrd != null)? objStartStop.startOrd:recordOrd.time_start;
				objStartStop.stopOrd = recordOrd.time_stop;
			}
		}
		else {
			var objUserTimeWork = globals.getUserTimeWorkByDay(record.user_id,record.event_log_date);
			objStartStop.startOrd = objUserTimeWork.startTime;
			objStartStop.stopOrd = objUserTimeWork.stopTime;
		}

	}
	return objStartStop;
}

/**
 * @param {JSRecord<db:/geco/events_log>} record record that will be updated
 * @param {Number} approverId
 * @properties={typeid:24,uuid:"E5BCEE9C-A9AB-4AC5-9975-9C99C78BA629"}
 */
function setNewApprover(record, approverId) {

	//record.action = "changingApprover";
	record.user_approver_id = approverId;
}
