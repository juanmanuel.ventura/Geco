/**
 * @param {String} entityName
 * @param {Number} entityId
 * @param {Number} approverId
 * @param {Date} dateStart
 * @properties={typeid:24,uuid:"A4121EA8-D2D3-4952-9C8E-CA30B8FB8412"}
 * @SuppressWarnings(wrongparameters)
 */
function addApprover(entityName, entityId, approverId, dateStart) {
	try {
		application.output("La data che viene passata è:"+dateStart);
		var dateFrom = new Date();
		if (dateStart!=null) {
			dateFrom = dateStart
		}		
		var day = dateFrom.getDate();
		var month = dateFrom.getMonth();
		var year = dateFrom.getFullYear();
		application.output(dateFrom.toDateString());
		databaseManager.setAutoSave(false);
		newRecord(false);
		entity = entityName;
		entity_id = entityId;
		user_approver_id = approverId;
		user_approver_from = new Date(year, month, day);
		databaseManager.saveData();
	} catch (e) {
		throw e;
	} finally {
		databaseManager.setAutoSave(true);
	}
}

/**
 * Returns the approver of the entity for the given date
 *
 * @param {String} entityName The entity name
 * @param {Number} entityId The entity id
 * @param {Date} date The date value to get the approver
 * @return {Number}
 *
 * @properties={typeid:24,uuid:"AF564C25-A6E7-4E35-A20D-0FFF22CAC98C"}
 * @SuppressWarnings(wrongparameters)
 */
function getApprover(entityName, entityId, date) {
	var max = null;

	// get the approvers from the given entityId
	if (find()) {
		entity = entityName;
		entity_id = entityId;
		if (search() != 0) {
			// sort the result in order to get first the current approver (higher date from)
			sort('user_approver_from desc')
			// if there is only one approver don not loop, return the id and exit
			if (getSize() == 1) {
				if (date >=getSelectedRecord().user_approver_from)
					return getSelectedRecord().user_approver_id;
				else return null;
			}
			// now find the right approver for that date
			for (var index = 1; index <= getSize(); index++) {
				// if the passed date is greater than the date from which the
				// current approver is in charge then return it and exit
				// will work just the first loop
				if (date >= getRecord(index).user_approver_from && !max) {
					return getRecord(index).user_approver_id;
				}
				// if the date is between means that you got the proper case
				if (date < max && date >= getRecord(index).user_approver_from) {
					return getRecord(index).user_approver_id;
				}
				// set the max date value
				max = getRecord(index).user_approver_from;
			}
		}
	}
	return null;
}

/**
 * @param {Number} userId
 * @param {String} entityName
 * @param {Number} entityId
 *
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"16B7B47C-A96C-43BD-B27E-C81CEC6DA2B3"}
 * @SuppressWarnings(wrongparameters)
 */
function isApproverForAnotherEntity(userId, entityName, entityId) {
	var tot = 0;
	if (find()) {
		user_approver_id = userId;
		entity = entityName;
		entity_id = '!' + entityId;
		newRecord();
		user_approver_id = userId;
		entity = '!' + entityName;
		tot = search();
	}
	//if tot > 0 then user is approver for another entity
	return (tot > 0);
}

/**
 * Return if user is profit center or job order owner
 * @param {Number} userId
 * @return {Boolean}
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"C4C0C06B-FE7A-450A-B00A-F2D39BC6F2DD"}
 */
function isOwnerApprover(userId) {

	var totJobOrders = 0;
	var totProfitCenters = 0;

	/** @type {JSFoundSet<db:/geco/job_orders>} */
	var job_orders = databaseManager.getFoundSet('geco', 'job_orders');

	if (job_orders.find()) {
		job_orders.user_owner_id = userId;
		totJobOrders = job_orders.search();
	}

	/** @type {JSFoundSet<db:/geco/profit_centers>} */
	var profit_centers = databaseManager.getFoundSet('geco', 'profit_centers');

	if (profit_centers.find()) {
		profit_centers.user_owner_id = userId;
		profit_centers.newRecord()
		profit_centers.profit_centers_to_owners_markets.user_id = userId;
		totProfitCenters = profit_centers.search();
	}

	if ( (totJobOrders + totProfitCenters) > 0) {
		return true;
	}
	return false;

}

/**
 * return if user is the only approver for the entity
 * @param {Number} userId
 * @param {String} entityName
 * @param {Number} entityId
 * @param {Date} dateFrom
 * @return {Boolean}
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"8609E8AA-E5B6-4B81-A9B3-558EF6BA642D"}
 */
function checkIsOnlyApprover(userId, entityName, entityId, dateFrom) {
	var totApprovers = 0;
	var date = dateFrom;
	var month = '' + (date.getMonth() + 1);
	/** @type {String} */
	var day = date.getDate().toString();
	var dateStr = date.getFullYear() + '-' + (month.length > 1 ? month : '0' + month) + '-' + (day.length > 1 ? day : '0' + day);
	/** @type {JSFoundSet<db:/geco/approvers>} */
	var approvers = databaseManager.getFoundSet('geco', 'approvers');
	if (approvers.find()) {
		approvers.entity = entityName;
		approvers.entity_id = entityId;
		approvers.user_approver_id = '!' + userId;
		approvers.newRecord();
		approvers.entity = entityName;
		approvers.entity_id = entityId;
		approvers.user_approver_id = userId;
		approvers.user_approver_from = '!#' + dateStr + '|yyyy-MM-dd';
		totApprovers = approvers.search();

	}
	return (totApprovers == 0);
}

///**
// * Count event_log end expenses_log in status open or rejected
// * @AllowToRunInFind
// * @param {JSRecord<db:/geco/approvers>} record
// *
// * @return {Boolean}
// *
// * @properties={typeid:24,uuid:"E0A32DBE-C8E0-4B25-B3D5-8C4951FFB494"}
// */
//function hasEventToApprove(record) {
//	var totEventsLog = 0;
//	var totExpensesLog = 0;
////TODO ANTONIO modificare
//	// distinguere gli eventi per entità profit center o job order
//
//	/** @type {JSFoundSet<db:/geco/events_log>} */
//	var events_log = databaseManager.getFoundSet('geco', 'events_log');
//	if (events_log.find()) {
//		// distinguere per entity:
//		//se job_order ---> job_order_id = record.entity_id
//		if(record.entity == 'job_orders') {
//			events_log.job_order_id = record.entity_id;
//		}
//		// se profit center ---> tipologia di approvazione dell'evento = responsabile del profit center
//		if(record.entity == 'profit_centers') {
//			events_log.events_log_to_events.approver_figure = 2;
//		}
//		events_log.user_approver_id = record.user_approver_id;
//		events_log.event_log_status = [1, 3];
//		totEventsLog = events_log.search();
//	}
//
//	// se entity = job_order
//	if(record.entity == 'job_orders'){
//	/** @type {JSFoundSet<db:/geco/expenses_log>} */
//	var expenses_log = databaseManager.getFoundSet('geco', 'expenses_log');
//	if (expenses_log.find()) {
//		//job_order_id = record.entity_id
//		expenses_log.job_order_id = record.entity_id;
//		expenses_log.user_approver_id = record.user_approver_id;
//		expenses_log.expense_log_status = [1, 3];
//		totExpensesLog = expenses_log.search();
//	}
//	}
//	return (totEventsLog + totExpensesLog > 0);
//}

/**
 * @param {JSRecord<db:/geco/approvers>} record
 *
 *
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"6E2A02C3-E417-4CF8-834B-3394AF8CC501"}
 */
function reAssignApproverAndEvents(record) {
	application.output('reAssignApproverAndEvents START');
	var entityName = record.entity;
	var entityId = record.entity_id;

	application.output(entityName + ' ID ' + entityId);
	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'events_log');

	/** @type {JSFoundSet<db:/geco/expenses_log>} */
	var expenses_log = databaseManager.getFoundSet('geco', 'expenses_log');

	/** @type {JSFoundSet<db:/geco/approvers>} */
	var approvers = databaseManager.getFoundSet('geco', 'approvers');
	var approverFigure = 0;
	if (entityName == 'job_orders') {
		approverFigure = 3;
	} else if (entityName == 'profit_centers') approverFigure = 2;
	var tot = 0
	// cerco gli approvatori e li ordino per data crescente
	if (approvers.find()) {
		approvers.entity = entityName;
		approvers.entity_id = entityId;
		tot = approvers.search();
		approvers.sort('user_approver_from asc');
	}
	application.output('approvatori trovati ' + tot);
	// Se ci sono approvatori, mi prendo la data
	if (tot > 0) {
		for (var i = 1; i <= approvers.getSize(); i++) {
			var recActual = approvers.getRecord(i);
			var date = recActual.user_approver_from;
			var day = '' + (date.getDate());
			var month = '' + (date.getMonth() + 1);
			var dateStr = date.getFullYear() + '-' + (month.length > 1 ? month : '0' + month) + '-' + (day.length > 1 ? day : '0' + day);
			application.output('--------------------------------------------------');
			application.output(i + ' approvatore ' + recActual.approvers_to_users.user_real_name + ' dal ' + dateStr);
			var totEvents = 0;
			// Cerco tra gli eventi
			if (events_log.find()) {
				if (entityName == 'job_orders') {
					events_log.job_order_id = entityId;
				} else if (entityName == 'profit_centers') {
					events_log.events_log_to_users.users_to_profit_centers.profit_center_id = entityId
				}
				if (i == approvers.getSize()) {
					events_log.event_log_date = '>=' + dateStr + '|yyyy-MM-dd';
				} else {
					var recNext = approvers.getRecord(i + 1);
					/** @type {Date} */
					var dataNext = recNext.user_approver_from;
					dataNext.setDate(dataNext.getDate() - 1);
					application.output(dataNext);
					var dayNext = '' + (dataNext.getDate());
					var monthNext = '' + (dataNext.getMonth() + 1);
					var dateStr2 = dataNext.getFullYear() + '-' + (monthNext.length > 1 ? monthNext : '0' + monthNext) + '-' + (dayNext.length > 1 ? dayNext : '0' + dayNext);
					application.output(i + ' data record successivo ' + dateStr2);
					events_log.event_log_date = ' ' + dateStr + '...' + dateStr2 + ' |yyyy-MM-dd';
				}
				events_log.user_approver_id = '!' + recActual.user_approver_id;
				events_log.event_log_status = '1';
				events_log.events_log_to_events.approver_figure = approverFigure;

				totEvents = events_log.search();
				application.output('eventi trovati ' + totEvents);
			}
			// Se ci sono gli eventi in stato aperto e associati a quelle date
			if (totEvents > 0) {
				for (var index = 1; index <= events_log.getSize(); index++) {

					var rec = events_log.getRecord(index);
					application.output('prima ' + rec.user_approver_id + '     ' + rec.event_log_date);
					// Cambio l'approvatore agli eventi

					//sostituita la chiamata sotto con una semplice modifica del dato (anche se poi applicherà tutte le regole per l'evento)
					//rec.user_approver_id = recActual.user_approver_id
					//events_log.setNewApprover(rec, recActual.user_approver_id);
					rec.user_approver_id = recActual.user_approver_id;
					application.output('dopo ' + rec.user_approver_id + '     ' + rec.event_log_date);

				}
			}
			var totExpenses = 0;
			//cerco tra le note spese solo per commesse
			if (entityName == 'job_orders') {
				if (expenses_log.find()) {
					expenses_log.job_order_id = entityId;
					if (i == approvers.getSize()) {
						expenses_log.expense_date = '>=' + dateStr + '|yyyy-MM-dd';
					} else {
						expenses_log.expense_date = ' ' + dateStr + '...' + dateStr2 + ' |yyyy-MM-dd';
					}
					expenses_log.user_approver_id = '!' + recActual.user_approver_id;
					expenses_log.expense_log_status = '1';

					totExpenses = expenses_log.search();
					application.output('note spese trovate ' + totExpenses);
				}
				// Se ci sono note spese in stato aperto e associate a quelle date
				if (totExpenses > 0) {
					for (var index1 = 1; index1 <= expenses_log.getSize(); index1++) {

						var rec1 = expenses_log.getRecord(index1);
						application.output('prima ' + rec1.user_approver_id + '     ' + rec1.expense_date);
						// Cambio l'approvatore alle note spese

						// sostituita la chiamata sotto con una semplice modifica del dato (anche se poi applicherà tutte le regole per l'evento)
						//rec1.user_approver_id = recActual.user_approver_id
						//expenses_log.setNewApprover(rec1, recActual.user_approver_id);
						rec1.user_approver_id = recActual.user_approver_id;
						application.output('dopo ' + rec1.user_approver_id + '     ' + rec1.expense_date);

					}
				}
			}
		}
	}
	application.output('reAssignApproverAndEvents STOP');
}

/**
 * @param {JSRecord<db:/geco/approvers>} record record that will be inserted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"FDD0CF8F-8479-4B59-8D88-6C822618BD9C"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert')
}

/**
 * @param {JSRecord<db:/geco/approvers>} record record that will be updated
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"44FDE8B6-1D93-4F66-8F71-9FF7198FF632"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate')
}

/**
 * @param {JSRecord<db:/geco/approvers>} record record that will be deleted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"EFC9DE84-D1B1-47E0-9D1D-E9BD6AE6C9E4"}
 */
function onRecordDelete(record) {
	return globals.applyAllRules(record, 'onDelete');
}
