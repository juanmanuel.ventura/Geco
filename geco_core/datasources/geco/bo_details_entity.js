/**
 * @param {JSRecord<db:/geco/bo_details>} record record that will be inserted
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"61077461-E1B4-44D2-AFCC-6486E86CC455"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}

/**
 * @param {JSRecord<db:/geco/bo_details>} record record that will be updated
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"EA9F3437-FCC1-4553-8F1F-404793A1D4AA"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate')
}

/**
 * Record after-insert trigger.
 * @param {JSRecord<db:/geco/bo_details>} record record that is inserted/update
 * @private
 * @properties={typeid:24,uuid:"B16F3E58-DD5E-443B-95E8-BBE023F5A071"}
 * @AllowToRunInFind
 */
function addBoAmount(record) {
	application.output(globals.messageLog + 'START bo_details_entity.addBoAmount() ' , LOGGINGLEVEL.DEBUG);
	record.bo_details_to_bo.bo_to_bo_details.loadRecords(); // MOD_CIRO
	record.bo_details_to_bo.setProfitCost(record.bo_details_to_bo.getRecord(1), 'updateProfitCost', globals.roundNumberWithDecimal(record.bo_details_to_bo.bo_to_bo_details.total_return,2), globals.roundNumberWithDecimal(record.bo_details_to_bo.bo_to_bo_details.total_cost,2))
	application.output(globals.messageLog + 'STOP bo_details_entity.addBoAmount() BO profit= ' + record.bo_details_to_bo.profit + ' BO cost= ' + record.bo_details_to_bo.cost , LOGGINGLEVEL.DEBUG);
}

/**
 * Record after-delete trigger.
 * @param {JSRecord<db:/geco/bo_details>} record record that is deleted
 * @private
 * @properties={typeid:24,uuid:"0930F34E-EC51-4C1F-AFF0-A57235325ED2"}
 */
function delBOAmount(record) {
	application.output(globals.messageLog + 'START bo_details_entity.delBOAmount() ' , LOGGINGLEVEL.DEBUG);
	application.output(record)
	record.bo_details_to_bo.bo_to_bo_details.loadRecords();
	record.bo_details_to_bo.setProfitCost(record.bo_details_to_bo.getRecord(1), 'updateProfitCost', globals.roundNumberWithDecimal(record.bo_details_to_bo.bo_to_bo_details.total_return,2), globals.roundNumberWithDecimal(record.bo_details_to_bo.bo_to_bo_details.total_cost,2))
	databaseManager.saveData(record.bo_details_to_bo);
	//record.bo_details_to_bo.action = null;
	application.output(globals.messageLog + 'STOP bo_details_entity.delBOAmount() BO profit= ' + record.bo_details_to_bo.profit + ' BO cost= ' + record.bo_details_to_bo.cost , LOGGINGLEVEL.DEBUG);
}

/**
 * Record after-update trigger.
 *
 * @param {JSRecord<db:/geco/bo_details>} record record that is updated
 *
 * @private
 *
 * @properties={typeid:24,uuid:"59893384-AA2F-4292-A2C4-843AB96463D1"}
 */
function updateBoAmount(record) {
	application.output(globals.messageLog + 'START bo_details_entity.updateBoAmount() ' , LOGGINGLEVEL.DEBUG);
	record.bo_details_to_bo.setProfitCost(record.bo_details_to_bo.getRecord(1), 'updateProfitCost', 
		globals.roundNumberWithDecimal(record.bo_details_to_bo.bo_to_bo_details.total_return,2), 
		globals.roundNumberWithDecimal(record.bo_details_to_bo.bo_to_bo_details.total_cost,2))	
	application.output(globals.messageLog + 'STOP bo_details_entity.updateBoAmount() BO profit= ' + record.bo_details_to_bo.profit + ' BO cost= ' + record.bo_details_to_bo.cost , LOGGINGLEVEL.DEBUG);
}
