/**
 * @properties={type:4,typeid:36,uuid:"5463205F-6C0F-4687-BB25-846E9964434A"}
 */
function is_selected() {
	return 0;
}

/**
 * @properties={type:4,typeid:36,uuid:"9070CC69-EB80-488B-8E58-28F543104AC8"}
 */
function current_approver_id() {
	return profit_centers_to_approvers.getApprover('events', event_id, new Date)
}
