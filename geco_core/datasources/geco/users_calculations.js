/**
 * @properties={type:12,typeid:36,uuid:"7E53BF86-ED7F-4BB8-946A-4F01128990F4"}
 */
function user_real_name() {
	return users_to_contacts.real_name;
}

/**
 * @properties={type:4,typeid:36,uuid:"47F02B3D-4C29-421A-8DAB-ED5233340C71"}
 */
function is_selected() {
	return 0;
}
