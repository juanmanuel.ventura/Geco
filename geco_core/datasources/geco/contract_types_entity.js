/**
 * @param {JSRecord<db:/geco/contract_types>} record record that will be updated
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"4E9ADB26-34EF-4937-A6DF-70608F2EFD34"}
 */
function onRecordUpdate(record) {

	return globals.applyAllRules(record, 'onUpdate');
}

/**
 * @param {JSRecord<db:/geco/contract_types>} record record that will be inserted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"7CFF469D-440F-4776-B2C4-3BAFACC3A6DA"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}
