/**
 * @param {Number} userId
 * @param {Number} groupId
 * @properties={typeid:24,uuid:"F099E11E-C227-4350-BE42-B5A700FFFADB"}
 * @SuppressWarnings(wrongparameters)
 */
function addUserGroup(userId, groupId) {
	application.output('USER_GROUPS userId ' + userId + ' groupId ' + groupId);
	try {
		if (searchUserGroup(userId, groupId) == 0) {
			//databaseManager.setAutoSave(false);
			newRecord(false);
			user_id = userId;
			group_id = groupId;
			//databaseManager.saveData();
		}
	} catch (e) {
		application.output('ERRORE inserimento utente-gruppo '+e);
		throw e;
	} finally {
		//databaseManager.setAutoSave(true);
	}
}

/**
 * @param {Number} userId
 * @param {Number} groupId
 * @return {Number}
 *
 * @SuppressWarnings(wrongparameters)
 * @properties={typeid:24,uuid:"53C88645-FA81-4382-851A-2308557C9036"}
 */
function searchUserGroup(userId, groupId) {
	var result = 0;

	if (find()) {
		user_id = userId;
		group_id = groupId
		result = search();
	}

	return result;
}

/**
 * @param {Number} userId
 * @param {Number[]} groupId
 *
 * @properties={typeid:24,uuid:"BB7002E4-C8DF-49BD-9D66-CB6EEE22C73F"}
 * @SuppressWarnings(wrongparameters)
 */
function deleteUserGroup(userId, groupId) {
//	var tot = 0;
application.output('delete user group UserId: ' + userId + ' groupId: ' + groupId);
	if (find()) {
		user_id = userId;
		if (groupId != null) group_id = groupId;
		search();
		deleteAllRecords();
	}
}


/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/user_groups>} record record that will be inserted
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"7FE55A8D-453C-4813-A1D9-AB2717D5DEAA"}
 */
function onRecordInsert(record) {
	application.output('inserito user_id = '+ record.user_id +' nel gruppo ' +record.group_id);
	return true
}

/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/user_groups>} record record that will be updated
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"E8F8369D-60BD-47E5-AB24-DE621D412DEB"}
 */
function onRecordUpdate(record) {
	application.output('modificato user_id = ' + record.user_id);
	return true
}

/**
 * Record pre-delete trigger.
 * Validate the record to be deleted.
 * When false is returned the record will not be deleted in the database.
 * When an exception is thrown the record will also not be deleted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/user_groups>} record record that will be deleted
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"FB4C5DC1-1603-4349-85E5-EF6F2D6D40CD"}
 */
function onRecordDelete(record) {
	application.output('cancellato user_id = '+ record.user_id +' dal gruppo ' +record.group_id);
	return true
}
