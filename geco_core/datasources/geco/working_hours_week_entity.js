/**
 * @param {JSRecord<db:/geco/working_hours_week>} record record that will be updated
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"530E43EF-98D0-41B0-9D7D-DA4BC3BF5699"}
 */
function onRecordUpdate(record) {

	return globals.applyAllRules(record, 'onUpdate');
}

/**
 * @param {JSRecord<db:/geco/working_hours_week>} record record that will be inserted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"47D9ABE8-1A08-4BD9-A22F-D002E47CA22D"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}
