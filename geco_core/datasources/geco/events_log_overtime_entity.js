/**
 * @param {Number} userId
 * @param {Date} eventsDate
 * @return {Number}
 * @SuppressWarnings(wrongparameters)
 * @properties={typeid:24,uuid:"A181E302-7E13-4A07-9248-5F057BE48C31"}
 */
function getOvertimes(userId, eventsDate) {
	var result = 0;
	if (find()) {
		user_id = userId;
		events_log_overtime_date = eventsDate;
		result = search();
	}
	return result;
}

/**
 *
 * @param {Number} userId
 * @param {Date} eventsDate
 * @param {Number} eventOvertimeId
 * @param {Number} eventLogId
 * @return {Number}
 * @SuppressWarnings(wrongparameters)
 * @properties={typeid:24,uuid:"2FFAF054-B9B7-4C09-A02C-805B018D94E3"}
 */
function getDurationOvertimesByType(userId, eventsDate, eventOvertimeId, eventLogId) {
	var totDuration = 0;
	if (find()) {
		user_id = userId;
		events_log_overtime_date = eventsDate;
		event_log_id = '!' + eventLogId;
		if (eventOvertimeId != null) events_overtime_id = eventOvertimeId
		search();
		if (total_duration != null)
			totDuration = total_duration;
	}

	return totDuration;
}

/**
 * @param {Number} eventsOvertimeId : events_overtime_id
 * @param {JSRecord<db:/geco/events_log>} record
 * @param {Number} timeStart
 * @param {Number} timeStop
 * @properties={typeid:24,uuid:"40CD6C52-F3FA-49A7-AB4E-F7E4CC8DECA7"}
 * @SuppressWarnings(wrongparameters)
 */
function insertOvertime(eventsOvertimeId, record, timeStart, timeStop) {
	try {
		databaseManager.setAutoSave(false);
		newRecord(false);
		event_log_id = record.event_log_id;
		events_overtime_id = eventsOvertimeId;
		external_code_fep = events_log_overtime_to_events_overtime.external_code_fep;
		time_start = timeStart;
		time_stop = timeStop;
		duration = timeStop - timeStart;
		events_log_overtime_date = record.event_log_date;
		user_id = record.user_id;
		databaseManager.saveData();
	} catch (e) {
		throw e;
	} finally {
		databaseManager.setAutoSave(true);
	}
}

/**
 * Update overtime id
 * @param {Number} eventsOvertimeId : events_overtime_id
 * @param {Number} newEventsOvertimeId : events_overtime_id to update
 * @param {JSRecord<db:/geco/events_log>} record
 *
 * @SuppressWarnings(wrongparameters)
 * @properties={typeid:24,uuid:"4DCB33F5-3BD3-4E60-83F2-D3CF4BF782A4"}
 * @AllowToRunInFind
 */
function updateOvertime(eventsOvertimeId, newEventsOvertimeId, record) {
	var result = 0;
	if (find()) {
		user_id = record.user_id;
		events_overtime_id = eventsOvertimeId;
		events_log_overtime_date = record.event_log_date;
		result = search();
	}

	if (result > 0) {
		/** @type {JSFoundSet<db:/geco/events_overtime>} */
		var events_overtime = databaseManager.getFoundSet('geco', 'events_overtime');
		var code_fep = null;
		if (events_overtime.find()) {
			events_overtime.events_overtime_id = newEventsOvertimeId;
			events_overtime.search()
			code_fep = events_overtime.external_code_fep;
		}
		for (var index = 1; index <= result; index++) {

			var events_log_overtime = getRecord(index);
			events_log_overtime.events_overtime_id = newEventsOvertimeId;
			events_log_overtime.external_code_fep = code_fep;
		}
	}

}

/**
 * Delete overtime
 * @param {JSRecord<db:/geco/events_log>} record
 *
 * @SuppressWarnings(wrongparameters)
 * @properties={typeid:24,uuid:"1B94059C-7081-4757-B9DB-C1E2A40CED64"}
 */
function deleteOvertime(record) {
	try {
		if (find()) {
			user_id = record.user_id;
			events_log_overtime_date = record.event_log_date;
			event_log_id = record.event_log_id
			var result = search();
			if (result > 0) deleteAllRecords();
		}
	} catch (e) {
		throw e;
	}
}
/**
 * @param {JSRecord<db:/geco/events_log>} record
 * @param {Number} userId
 * @param {Date} eventLogDate
 * @param {Number} durata
 * @SuppressWarnings(wrongparameters)
 * @properties={typeid:24,uuid:"3ED9CF35-E963-4D85-A65E-1F1497D2C16F"}
 */
function checkOvertimeHoliday(record, userId, eventLogDate, durata) {
	/** @type {Number} */
	var durationTochange = null;
	/** @type {Number} */
	var result = null;
	/** @type {Number} */
	var index = null;

	var durationS50 = getDurationOvertimesByType(record.user_id, record.event_log_date, 3, record.event_log_id);
	var durationS55 = getDurationOvertimesByType(record.user_id, record.event_log_date, 4, record.event_log_id);
	var durationS60 = getDurationOvertimesByType(record.user_id, record.event_log_date, 5, record.event_log_id);
	var durationS75 = getDurationOvertimesByType(record.user_id, record.event_log_date, 6, record.event_log_id);
	var durationSR10 = getDurationOvertimesByType(record.user_id, record.event_log_date, 7, record.event_log_id);
	var durationSR35 = getDurationOvertimesByType(record.user_id, record.event_log_date, 8, record.event_log_id);
	var durationSR55 = getDurationOvertimesByType(record.user_id, record.event_log_date, 9, record.event_log_id);

	if (record.events_log_to_events.event_id != 28) {
		durationTochange = durationS50 + durationS55 + durationS60 + durationSR10 + durationSR35 + durationSR55 + durata - 480;
	} else {
		//durationTochange = durationSR10 + durationSR35 + durationSR55 + durata - 480;
		//durationTochange = durationSR10 + durationSR35 + durata - 480;
		durationTochange = durationSR10 + durationSR35 + durationS55 + durationS60 + durationS75 + durata - 480;
	}
	application.output('durationTochange : ' + durationTochange, LOGGINGLEVEL.DEBUG);
	//durationTochange = durata;
	
	/** @type {JSFoundSet<db:/geco/events_log_overtime>} */
	var events_log_overtime = databaseManager.getFoundSet('geco', 'events_log_overtime');

	result = 0;
	if (durationTochange > 0) {
		//cerco S60 da cambiare
		if (find()) {
			user_id = userId;
			events_log_overtime_date = eventLogDate;
			events_overtime_id = 5;
			result = search();
		}
		application.output('trovati: ' + result, LOGGINGLEVEL.DEBUG);
		for (index = 1; index <= result; index++) {
			events_log_overtime = getRecord(index);
			if (durationTochange >= events_log_overtime.duration) {
				events_log_overtime.events_overtime_id = 6;
				events_log_overtime.external_code_fep = 'S75';
				//updateOvertime(5, 6, events_log_overtime.events_log_overtime_to_events_log.getSelectedRecord());
				durationTochange = durationTochange - events_log_overtime.duration;
				application.output('events_log_overtime modificato: ' + events_log_overtime, LOGGINGLEVEL.DEBUG);
				databaseManager.saveData(events_log_overtime);
				if (durationTochange == 0) break;
			} else if (durationTochange < events_log_overtime.duration) {
				insertOvertime(6, events_log_overtime.events_log_overtime_to_events_log.getSelectedRecord(), events_log_overtime.time_stop - durationTochange, events_log_overtime.time_stop);
				events_log_overtime.time_stop = events_log_overtime.time_stop - durationTochange;
				events_log_overtime.duration = events_log_overtime.time_stop - events_log_overtime.time_start;
				application.output('events_log_overtime modificato: ' + events_log_overtime, LOGGINGLEVEL.DEBUG);
				databaseManager.saveData(events_log_overtime);
				break;
			}
		}
	}

	result = 0;
	if (durationTochange > 0) {
		//cerco SR35 da cambiare
		if (find()) {
			user_id = userId;
			events_log_overtime_date = eventLogDate;
			events_overtime_id = 8;
			time_stop = '<=' + 360;
			newRecord();
			user_id = userId;
			events_log_overtime_date = eventLogDate;
			events_overtime_id = 8;
			time_start = '>=' + 1320;
			result = search();
		}
		application.output('trovati recupero: ' + result, LOGGINGLEVEL.DEBUG);
		for (index = 1; index <= result; index++) {
			events_log_overtime = getRecord(index);
			if (durationTochange >= events_log_overtime.duration) {
				events_log_overtime.events_overtime_id = 9;
				events_log_overtime.external_code_fep = 'SR55';
				durationTochange = durationTochange - events_log_overtime.duration;
				application.output('events_log_overtime modificato: ' + events_log_overtime, LOGGINGLEVEL.DEBUG);
				databaseManager.saveData(events_log_overtime);
				if (durationTochange == 0) break;
			} else if (durationTochange < events_log_overtime.duration) {
				insertOvertime(9, events_log_overtime.events_log_overtime_to_events_log.getSelectedRecord(), events_log_overtime.time_stop - durationTochange, events_log_overtime.time_stop);
				events_log_overtime.time_stop = events_log_overtime.time_stop - durationTochange;
				events_log_overtime.duration = events_log_overtime.time_stop - events_log_overtime.time_start;
				application.output('events_log_overtime modificato: ' + events_log_overtime, LOGGINGLEVEL.DEBUG);
				databaseManager.saveData(events_log_overtime);
				break;
			}
		}

	}
}

/**
 * Search SR35 for day and night
 * @param {Number} userId
 * @param {Date} eventsDate
 * @param {Number} eventOvertimeId
 * @param {Number} eventLogId
 * @param {Boolean} night
 * @return {Number}
 * @SuppressWarnings(wrongparameters)
 *
 * @properties={typeid:24,uuid:"97399799-9301-4BDF-9B0E-E4AE75EA89E4"}
 */
function getDurationOvertimesComp(userId, eventsDate, eventOvertimeId, eventLogId, night) {
	var totDuration = 0;
	
	if (find()) {
		if (night) {
			user_id = userId;
			events_log_overtime_date = eventsDate;
			events_overtime_id = 8;
			time_stop = '<=' + 360;
			newRecord();
			user_id = userId;
			events_log_overtime_date = eventsDate;
			events_overtime_id = 8;
			time_start = '>=' + 1320;
		}
		else {
			user_id = userId;
			events_log_overtime_date = eventsDate;
			events_overtime_id = 8;
			time_start = '>=' + 360;
			time_stop = '<=' + 1320;
		}
		search();
		if (total_duration != null)
			totDuration = total_duration;
	}

	return totDuration;
}

/**
 * @param {JSRecord<db:/geco/events_log>} record
 * @param {Number} userId
 * @param {Date} eventLogDate
 * @param {Number} durata
 * @SuppressWarnings(wrongparameters)
 * @properties={typeid:24,uuid:"6C02FC64-A202-4DAE-9087-E71C0EB7F773"}
 */
function checkOvertimeHolidayRec(record, userId, eventLogDate, durata) {
	application.output("START checkOvertimeHolidayRec",LOGGINGLEVEL.DEBUG);

	/** @type {Number} */
	var durationTochange = null;
	/** @type {Number} */
	var result = null;
	/** @type {Number} */
	var index = null;

	var durationS50 = getDurationOvertimesByType(record.user_id, record.event_log_date, 3, record.event_log_id);
	var durationS55 = getDurationOvertimesByType(record.user_id, record.event_log_date, 4, record.event_log_id);
	var durationSR10 = getDurationOvertimesByType(record.user_id, record.event_log_date, 7, record.event_log_id);
	var durationSR35_D = getDurationOvertimesComp(record.user_id, record.event_log_date, 8, record.event_log_id,false);

	if (record.events_log_to_events.event_id != 28) {
		durationTochange = durationS50 + durationS55 + durationSR10 + durata - 480;
	} 
	application.output('durationTochange : ' + durationTochange, LOGGINGLEVEL.DEBUG);
	//durationTochange = durata;
	
	/** @type {JSFoundSet<db:/geco/events_log_overtime>} */
	var events_log_overtime = databaseManager.getFoundSet('geco', 'events_log_overtime');

	result = 0;
	if (durationTochange > 0) {
		//cerco SR10 da cambiare in SR35
		if (find()) {
			user_id = userId;
			events_log_overtime_date = eventLogDate;
			events_overtime_id = 7;
			result = search();
		}
		application.output('trovati recupero: ' + result, LOGGINGLEVEL.DEBUG);
		for (index = 1; index <= result; index++) {
			events_log_overtime = getRecord(index);
			if (durationTochange >= events_log_overtime.duration) {
				events_log_overtime.events_overtime_id = 8;
				events_log_overtime.external_code_fep = 'SR35';
				durationTochange = durationTochange - events_log_overtime.duration;
				application.output('events_log_overtime modificato: ' + events_log_overtime, LOGGINGLEVEL.DEBUG);
				databaseManager.saveData(events_log_overtime);
				if (durationTochange == 0) break;
			} else if (durationTochange < events_log_overtime.duration) {
				insertOvertime(8, events_log_overtime.events_log_overtime_to_events_log.getSelectedRecord(), events_log_overtime.time_stop - durationTochange, events_log_overtime.time_stop);
				events_log_overtime.time_stop = events_log_overtime.time_stop - durationTochange;
				events_log_overtime.duration = events_log_overtime.time_stop - events_log_overtime.time_start;
				application.output('events_log_overtime modificato: ' + events_log_overtime, LOGGINGLEVEL.DEBUG);
				databaseManager.saveData(events_log_overtime);
				break;
			}
		}
	}
	application.output("STOP checkOvertimeHolidayRec",LOGGINGLEVEL.DEBUG);

}