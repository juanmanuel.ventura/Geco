/**
 * Aggiorna il totale degli ordini nella BO sommando il valore dell'ordine salvato
 * @param {JSRecord<db:/geco/bo_orders>} record
 * @properties={typeid:24,uuid:"44772E0B-51AD-4D93-BDEF-D844186FEE1F"}
 */
function addBoOrderAmount(record)
{
	record.bo_orders_to_business_opportunities.order_amount = record.bo_orders_to_business_opportunities.order_amount + record.order_amount
	
}

/**
 * Aggiorna il totale degli ordini nella BO sottraendo il valore dell'ordine modificato/cancellato
 * @param {JSRecord<db:/geco/bo_orders>} record
 * @properties={typeid:24,uuid:"3AFC6112-17EA-48C9-ABEE-57BE1834A4D0"}
 */
function delBoOrderAmount(record)
{
	record.bo_orders_to_business_opportunities.order_amount = record.bo_orders_to_business_opportunities.order_amount - record.order_amount
	
}

/**
 * @param {JSRecord<db:/geco/bo_orders>} record record that will be inserted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"568FDBC8-62AE-4F24-8C78-50A1C43EE3EB"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}

/**
 * @param {JSRecord<db:/geco/bo_orders>} record record that will be inserted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"22C666ED-6275-47DE-8D2A-78F5142CB1BC"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate');
}
