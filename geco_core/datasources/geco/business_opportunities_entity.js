/**
 * @param {JSRecord<db:/geco/business_opportunities>} record record that will be updated
 * @param {String} act
 * @param {Number} newProfit
 * @param {Number} newCost
 * @properties={typeid:24,uuid:"3B840B14-5F90-471B-A02D-0926D5AA84A4"}
 */
function setProfitCost(record, act, newProfit, newCost)
{
	application.output(globals.messageLog + 'START business_opportunities_entity.setProfitCost() ' +record.bo_id , LOGGINGLEVEL.DEBUG);
	record.action = act;
	record.profit = newProfit;
	record.cost = newCost;
	updateMol(record);
	application.output(globals.messageLog + 'STOP business_opportunities_entity.setProfitCost() ' +record.bo_id , LOGGINGLEVEL.DEBUG);
}


/**
 * @properties={typeid:24,uuid:"FFDD8424-80AD-4D67-A692-5EB21CA7957E"}
 */
function countDoc()
{
	if (business_opportunities_to_bo_bo_documents 
			&& business_opportunities_to_bo_bo_documents.getSize()>0
			&& business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents != null 
			&& business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.getSize()>0){
				application.output(bo_id + ' numero doc ' + business_opportunities_to_bo_bo_documents.bo_bo_documents_to_bo_documents.getSize())
			return 1;
		}
		else return 0;
}

/**
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @properties={typeid:24,uuid:"D3948813-C9C3-4AA7-9D44-794B9BBEBBB1"}
 */
function set_bo_number(record)
{
	/** @type {JSFoundSet<db:/geco/bo_progress_number>} */
	var bo_progress_number = databaseManager.getFoundSet('geco', 'bo_progress_number');
	bo_progress_number.loadAllRecords();
	var id = bo_progress_number.getSize()
	var rc = bo_progress_number.getRecord(id);
	record.bo_number = rc.bo_progress_number_id+1;
	
	var anno = ''+new Date().getFullYear();

	//TODO verificare se il PC non ha codice cosa deve fare
	record.bo_code = anno.slice(2,4)+record.bo_to_profit_centers.profit_center_code+record.bo_number;
	record.bo_year = anno;
	record.bo_probability = 10;
	record.status = 1;
	record.bo_type = 1;
	rc.bo_progress_number_id = record.bo_number;
	databaseManager.saveData(rc);
}

/**
 * @param {JSRecord<db:/geco/business_opportunities>} record record that will be inserted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"777A3E11-D43C-4D47-84EA-AEF90EFF44B8"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}

/**
 * @param {JSRecord<db:/geco/business_opportunities>} record record that will be inserted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"A3559B97-F2C6-42CE-9194-73818704EE0F"}
 */
function onRecordUpdate(record) {
	if (record.action) {
		application.output("Skipping rules as '" + record.action + "' action detected");
		record.action= null;
		return true;
	}
	return globals.applyAllRules(record, 'onUpdate');
}

/**
 * Record after-update trigger.
 * @param {JSRecord<db:/geco/business_opportunities>} record record that is updated
 * @private
 * @properties={typeid:24,uuid:"2E7193CE-91AD-4196-925A-879BF88CD375"}
 */
function updateMol(record) {
	application.output(globals.messageLog + 'START business_opportunities_entity.updateMol() ' +record.bo_id, LOGGINGLEVEL.DEBUG);
	var molEuro = 0;
	var molPer = 0;
	if (record.profit!= null) {
		molEuro = (record.profit - record.cost);
		// forza le 2 cifre decimali
		molEuro = Math.round(molEuro * Math.pow(10, 2)) / Math.pow(10, 2);
		
		molPer = (record.profit - record.cost) * 100 / record.profit
		// forza le 2 cifre decimali
		molPer = Math.round(molPer * Math.pow(10, 2)) / Math.pow(10, 2);
	}
	record.mol_euro = molEuro;
	record.mol = molPer;
	application.output(globals.messageLog + 'STOP business_opportunities_entity.updateMol()', LOGGINGLEVEL.DEBUG);
}
