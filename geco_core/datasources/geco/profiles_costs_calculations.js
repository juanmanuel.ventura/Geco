/**
 * @properties={type:8,typeid:36,uuid:"662CBF0A-8FB6-4FCE-AABA-70C62C9009F4"}
 */
function is_selected()
{
	return 0;
}

/**
 * Restituisce il costo giornaliero in formato numero,2 decimali
 * @properties={type:8,typeid:36,uuid:"26EC2952-A23E-4A23-B5D0-4E7C62044425"}
 */
function prof_cost_daily() {
	if (prof_cost_daily) return globals.roundNumberWithDecimal(prof_cost_daily,2);
	else return null;
}

/**
 * Restituisce, dato il costo giornaliero, il costo orario 
 * @properties={type:8,typeid:36,uuid:"D76281E8-4A71-4F73-BB56-05102BB1F655"}
 */
function prof_cost_schedule() {
	if (prof_cost_daily) return globals.roundNumberWithDecimal(prof_cost_schedule = (prof_cost_daily/8),2);
	else return null;
}

/**
 * Restituisce il costo min in formato numero,2 decimali
 * @properties={type:8,typeid:36,uuid:"0B59F5BB-0467-497D-B408-C75F24BEC334"}
 */
function profile_cost_min() {
	if (profile_cost_min) return globals.roundNumberWithDecimal(profile_cost_min,2);
	else return null;
}

/**
 * Restituisce il costo max in formato numero,2 decimali
 * @properties={type:8,typeid:36,uuid:"0CA263DA-915C-4803-88AC-FC12CA9816EA"}
 */
function profile_cost_max() {
	if (profile_cost_max) return globals.roundNumberWithDecimal(profile_cost_max,2);
	else return null;
}
