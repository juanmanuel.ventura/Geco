/**
 * Record pre-update trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/companies>} record that will be updated
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"BD5C12EC-1C5C-442B-A607-1D3F878571A1"}
 */
function onRecordUpdate(record)
{
	return globals.applyAllRules(record, 'onUpdate')
}

/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/companies>} record record that will be inserted
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"12428BD0-5793-476C-8FFF-FA69949F335E"}
 */
function onRecordInsert(record)
{
	return globals.applyAllRules(record, 'onInsert')
}
