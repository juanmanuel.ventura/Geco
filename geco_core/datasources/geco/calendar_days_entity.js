/**
 *
 * @param {Number} month to enable/disable for logging
 * @param {Number} year to enable/disable for logging
 * @param {String} entity name of column: is_enable_for_expenses or is_enable_for_timesheet
 * @param {Boolean} status_logging true = enable , false = disable
 * @return {Boolean}
 * @throws {Error}
 * @properties={typeid:24,uuid:"2C65E6A4-3C72-4AB4-B7F0-FC4652E91503"}
 */
function setPeriodStatus(month, year, entity, status_logging) {
	//se il mese e l'anno sono null return error
	if (month == null || year == null) throw new Error("- I campi mese e anno non possono essere vuoti");

	// se non è stata specificata l'entità return error
	if (globals.isEmpty(entity)) throw new Error("- Il campo entità non può essere vuoto");

	//se il campo passato non esiste in tabella return error
	application.output("entità " + entity);
	if (entity != "is_enabled_for_expenses" && entity != "is_enabled_for_timesheet") throw new Error("- Il campo entità non esiste");

	//cerca i record con anno e mese in input
	if (find()) {
		calendar_month = month;
		calendar_year = year;
		//se trova almeno un record
		if (search(true, true) > 0) {
			databaseManager.setAutoSave(false);
			application.output("autosave = false");
			for (var index = 1; index <= getSize(); index++) {
				var record = getRecord(index);
				record[entity] = status_logging ? 1 : 0;
			}
			databaseManager.setAutoSave(true);
			application.output("true");
		}
	}
	return true;
}

/**
 * @param {JSRecord<db:/geco/calendar_days>} record record that will be updated
 * @private
 * @return {Boolean}
 * @properties={typeid:24,uuid:"D5C39B1B-2804-450B-BFCC-AB7E66A2AACA"}
 */
function setDisplays(record) {
	// extracts the month from the date, as JavaScript start from 0 the method adds 1 to match the real month number
	record.calendar_month = calendar_date.getMonth() + 1;
	// extract the weekday, start from 0 to 6
	record.calendar_weekday = calendar_date.getDay();
	// extracts the month day
	record.calendar_day = calendar_date.getDate();
	// extracts the year from the date
	record.calendar_year = calendar_date.getFullYear();

	return true;
}

/**
 * @param {JSRecord<db:/geco/calendar_days>} record record that will be inserted
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"0F7EE705-3E7C-4185-AAD0-E7013C1899CD"}
 */
function onRecordInsert(record) {
	return setDisplays(record);
}

/**
 * @param {JSRecord<db:/geco/calendar_days>} record record that will be updated
 * @returns {Boolean}
 * @private
 * @properties={typeid:24,uuid:"E998482D-6A13-432A-95B6-786EC376A45A"}
 */
function onRecordUpdate(record) {
	return setDisplays(record);
}
