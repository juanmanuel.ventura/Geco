/**
 * @properties={typeid:24,uuid:"CAA2B2A2-C99F-4264-8D67-CA1F8694B592"}
 * @SuppressWarnings(wrongparameters)
 */
function onSort() {
	sort('prof_cost_start_date desc');
}

/**
 * JStaffa dato il profileID del profilo, restituisce l'id del record con data maggiore
 * @param {Number} profileID
 * @return recID
 * @properties={typeid:24,uuid:"51834D21-47A9-4E06-9171-C9D29A662328"}
 * @AllowToRunInFind
 */
function getLastID(profileID) {
	var recID = null;
	if (!globals.isEmpty(profileID)) {
		/** @type {JSFoundSet<db:/geco/profiles_costs>} */
		var profiles_costs = databaseManager.getFoundSet('geco', 'profiles_costs');
		if (profiles_costs.find()) {
			profiles_costs.profile_id = profileID;
			if (profiles_costs.search() > 0) {
				profiles_costs.sort('prof_cost_start_date desc');
				recID = profiles_costs.getRecord(1).profile_cost_id;
			}
		}
	}
	return recID;
}

/**
 * @param {JSRecord<db:/geco/profiles_costs>} record record that will be inserted
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"9F1A27A0-3AE9-402A-AB15-CA0FB589D8DE"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}

/**
 * @param {JSRecord<db:/geco/profiles_costs>} record record that will be updated
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"C52351E5-B65A-4056-B5A0-651597DBB435"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate');
}

/**
 * @param {JSRecord<db:/geco/profiles_costs>} record record that will be deleted
 * @returns {Boolean}
 * @private
 *
 * @properties={typeid:24,uuid:"9FF9354C-6FCE-41FC-9334-2EE3E86147B5"}
 */
function onRecordDelete(record) {
	return globals.applyAllRules(record, 'onDelete');
}
