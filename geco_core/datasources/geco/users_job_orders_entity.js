
/**
 * Record pre-delete trigger.
 * Validate the record to be deleted.
 * When false is returned the record will not be deleted in the database.
 * When an exception is thrown the record will also not be deleted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/users_job_orders>} record record that will be deleted
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"87605F54-BDC4-416C-8CC5-7E9C560CFE96"}
 */
function onRecordDelete(record) {
	return globals.applyAllRules(record, 'onDelete');
}
