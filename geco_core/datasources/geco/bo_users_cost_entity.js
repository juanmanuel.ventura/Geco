
/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/bo_users_cost>} record record that will be inserted
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"0003CE73-2C7F-4116-9581-BD0AF1EB0A23"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}

/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/bo_users_cost>} record record that will be updated
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"91FEAC88-B132-418C-ACD6-7267063DE062"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate');
}
