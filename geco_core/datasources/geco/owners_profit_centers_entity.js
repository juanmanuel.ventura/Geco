/**
 * @param {Number} userId
 * @param {Number} profitCenterId
 * @properties={typeid:24,uuid:"C24FEA72-3EDC-427D-BBF1-F700CB35C566"}
 * @SuppressWarnings(wrongparameters)
 */
function addOwnerProfitCenter(userId, profitCenterId) {
	application.output('OWNER_PROFIT_CENTERS userId ' + userId + 'profitCenterId' + profitCenterId);
	try {
		if (searchOwnerProfitCenter(userId, profitCenterId) == 0) {
			databaseManager.setAutoSave(false);
			newRecord(false);
			user_id = userId;
			profit_center_id = profitCenterId;
			databaseManager.saveData();
		}
	} catch (e) {
		application.output(e);
		throw e;
	} finally {
		databaseManager.setAutoSave(true);
	}
}

/**
 * @param {Number} userId
 * @param {Number} profitCenterId
 * @return {Number}
 *
 * @SuppressWarnings(wrongparameters)
 * @properties={typeid:24,uuid:"D58FA7E3-2DD3-42C0-B6AC-F58AA1B99659"}
 */
function searchOwnerProfitCenter(userId, profitCenterId) {
	var result = 0;

	if (find()) {
		user_id = userId;
		profit_center_id = profitCenterId;
		result = search();
	}

	return result;
}

/**
 * @param userId
 * @param profitCenterId
 *
 * @properties={typeid:24,uuid:"816A0103-409B-4998-B484-ACEBAD41E2B2"}
 */
function deleteOwnerProfitCenter(userId, profitCenterId) {
	var totSearch = 0;
	if (find()) {
		user_id = userId;
		if (profitCenterId != null) profit_center_id = profitCenterId;
		totSearch = search();
		if (totSearch > 0) 
			deleteAllRecords();
	}
}

/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/owners_profit_centers>} record record that will be inserted
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"EE7EAAD1-C186-41ED-A515-7F764C2FF50F"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert');
}

/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/owners_profit_centers>} record record that will be updated
 *
 * @private
 * 
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"0803FB86-3D23-4CB7-A49A-170ED0191C43"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate');
}

/**
 * Record pre-delete trigger.
 * Validate the record to be deleted.
 * When false is returned the record will not be deleted in the database.
 * When an exception is thrown the record will also not be deleted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/owners_profit_centers>} record record that will be deleted
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"0E17B2FF-04A8-4E4D-BE19-92D74416E556"}
 */
function onRecordDelete(record) {
	return globals.applyAllRules(record, 'onDelete');
}
