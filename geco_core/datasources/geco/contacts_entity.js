/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/contacts>} record record that will be inserted
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"78C0B42A-0BDC-4008-8B33-387A46D19CE9"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert') && setDisplays(record);
}

/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/contacts>} record record that will be updated
 *
 * @returns {Boolean}
 *
 * @private
 *
 * @properties={typeid:24,uuid:"9C15877B-464E-4EF2-81C7-358B82589A91"}
 */
function onRecordUpdate(record) {
	return globals.applyAllRules(record, 'onUpdate') && setDisplays(record);
}

/**
 * @param {JSRecord<db:/geco/contacts>} record record that will be inserted
 * @return {Boolean}
 * @properties={typeid:24,uuid:"D8BDE737-4BE3-4E70-B689-9D1BE8F49363"}
 */
function setDisplays(record) {
	record.last_name = globals.trimString(record.last_name);
	record.first_name = globals.trimString(record.first_name);
	record.real_name = record.last_name + " " + record.first_name;
	record.first_name = record.first_name.toUpperCase();
	record.last_name = record.last_name.toUpperCase();
	return true;
}