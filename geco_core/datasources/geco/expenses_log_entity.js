/**
 * Record pre-insert trigger.
 * Validate the record to be inserted.
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/expenses_log>} record record that will be inserted
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"A5378A24-46C3-4A6E-81F7-ABAD337A76F2"}
 */
function onRecordInsert(record) {
	return globals.applyAllRules(record, 'onInsert')
}

/**
 * Record pre-update trigger.
 * Validate the record to be updated.
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord<db:/geco/expenses_log>} record record that will be updated
 *
 * @returns {Boolean}
 *
 * @properties={typeid:24,uuid:"C3DA037E-D92B-45E5-8411-491411C76F4C"}
 */
function onRecordUpdate(record) {
	
	// if an specific action was triggered don't run the rules
	if (record.action) {
		application.output("Skipping rules as '" + record.action + "' action detected");
		return true;
	}

	// run the rules
	return scopes.globals.applyAllRules(record, 'onUpdate');
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record record that will be inserted
 * @param {Number} status
 * @param {String} [actionNote]
 * @properties={typeid:24,uuid:"17F1AA24-3F39-40B7-B410-CA0A547C8870"}
 */
function setSatus(record, status, actionNote) {

	if (status == 2) record.action = "approving";
	if (status == 3) record.action = "rejecting";
	if (status == 4) record.action = "closing";

	record.expense_log_status = status;
	if (status == 2 || status == 3) record.actual_approver_id = globals.currentUserId;

	if (actionNote) record.note = actionNote;
}

/**
 * @param {JSRecord<db:/geco/expenses_log>} record to update
 * @param {Number} month
 * @param {Number} year
 * @properties={typeid:24,uuid:"52BCB916-F0BC-4CE2-9480-8083B10FACB3"}
 */
function shiftMonth(record, month, year) {
	record.expense_month = month;
	record.expense_year = year;
}


/**
 * @param {JSRecord<db:/geco/expenses_log>} record record that will be updated
 * @param {Number} approverId
 * @properties={typeid:24,uuid:"D33B82FB-400E-41F6-88DD-0F04E923E214"}
 */
function setNewApprover(record, approverId) {

	record.action = "changingApprover";
	record.user_approver_id = approverId;
}
