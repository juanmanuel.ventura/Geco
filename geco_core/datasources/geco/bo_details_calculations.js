/**
 * @properties={type:8,typeid:36,uuid:"7217631F-7084-40EF-AA4E-B1A58B9A0909"}
 */
function mol_per_display() {
	var mol = 0.0;
	if (total_return!=null && total_return != 0) {
		mol = (total_return - total_cost) * 100 / total_return;
		mol = Math.round(mol * Math.pow(10, 2)) / Math.pow(10, 2);
	}
	else mol = -1
	return mol;
}

/**
 * @return {String}
 * @properties={type:8,typeid:36,uuid:"C095530A-2D56-4AE1-AB17-F8104DFDE896"}
 */
function mol_dispaly() {
	var mol = 0;
	mol = (total_return - total_cost);
	mol = Math.round(mol * Math.pow(10, 2)) / Math.pow(10, 2);
	return mol;
}

/**
 * @return {Number}
 * @properties={type:4,typeid:36,uuid:"7DC3B538-D6C6-4A70-ADB2-2AEEBD1AE359"}
 */
function is_selected() {
	return 0;
}
