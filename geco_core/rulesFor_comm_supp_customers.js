/**
 * @param {JSRecord<db:/geco/comm_supp_customers>} record
 * @properties={typeid:24,uuid:"7E19E991-F5B3-4564-B7DD-144ED622BBF5"}
 * @AllowToRunInFind
 */
function validateCustomer(record) {
	//cliente non può essere vuoto
	if (globals.isEmpty(record.customer_id)) {
		throw new Error('- Il campo cliente è obbligatorio');
	}
	
	//cerco se esiste già la coppia Supp Comm - Cliente
	/** @type {JSFoundSet<db:/geco/comm_supp_customers>} */
	var comm_supp_customers = databaseManager.getFoundSet('geco', 'comm_supp_customers');
	var count = 0;
	
	if (comm_supp_customers.find()){
		comm_supp_customers.comm_supp_customer_id = '!' + record.comm_supp_customer_id;
		comm_supp_customers.comm_supp_pc_id = record.comm_supp_pc_id;
		comm_supp_customers.customer_id = record.customer_id;
		
		count = comm_supp_customers.search();	
	}
	
	if (count > 0){
		throw new Error("- Esiste già la coppia Supporto Commerciale - Cliente, cambiare il cliente scelto.");
	}
}