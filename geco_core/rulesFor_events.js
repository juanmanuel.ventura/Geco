/**
 * @param {JSRecord<db:/geco/events>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"33EA7802-AA1C-43DC-A69A-218119616B1B"}
 */
function validateEventName(record) {
	if (globals.isEmpty(record.event_name)) {
		throw new Error('- Il campo nome è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/events>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"4ED2F76E-32BD-4BB6-A4F6-52F03C2DFEC2"}
 */
function validateEventType(record) {
	if (globals.isEmpty(record.event_type_id)) {
		throw new Error('- Il campo tipo è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/events>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"41EB14FF-F448-4C23-AEDA-D27EE826223E"}
 */
function validateEventApproverFigure(record) {
	if (globals.isEmpty(record.approver_figure)) {
		throw new Error('- Il campo tipo approvatore è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/events>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"4FB2B242-0D88-4F24-AAC1-62FD4E4631AE"}
 */
function validateEventUnitMisure(record) {
	if (globals.isEmpty(record.unit_measure)) {
		throw new Error('- Il campo unità di misura è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/events>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"B53849EC-7407-4514-81C4-99761E659921"}
 */
function validateEventCode(record) {
	if (!globals.isEmpty(record.external_code_fep)) {
		record.external_code_fep = globals.trimString(record.external_code_fep)
	}

	if (!globals.isEmpty(record.external_code_navision)) {
		record.external_code_navision = globals.trimString(record.external_code_navision)
	}
}

/**
 * Valida che venga inserito il costo su alcuni tipi di eventi (7, 8)
 * @param {JSRecord<db:/geco/events>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"F5C21DF3-FCB8-41AD-89F8-CB481450AD6E"}
 */
function validateEventCost(record) {
	if (record && (record.event_type_id == 8 || record.event_type_id == 7)) {
		if (globals.isEmpty(record.event_cost)) {
			throw new Error('- Il campo costo è obbligatorio per l\'evento di tipo ' + record.events_to_event_types.event_type);
		}
	} else if (record && (record.event_type_id != 8 && record.event_type_id != 7)){
		if (!globals.isEmpty(record.event_cost)) {
			throw new Error('- Il campo costo è obbligatorio solo per gli eventi di tipo Trasferta / Reperibilità Gettone');
		}
	}
}

/**
 * Valida inserimento codice FEP per l'evento
 * @param {JSRecord<db:/geco/events>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"AE4257D1-500A-4B3D-8EAC-96EE0A0D1AEB"}
 */
function validateExternalCodeFep(record) {
	if (globals.isEmpty(record.external_code_fep)) {
		throw new Error('- Il campo codice FEP è obbligatorio');
	} else record.external_code_fep = record.external_code_fep.toUpperCase();
}