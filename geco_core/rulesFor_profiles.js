/**
 * JStaffa 2 Controlli in una rule sola
 * Valida l'inserimento del nome del profilo
 * Valida che non sia inserito un nuovo profilo con lo stesso nome
 * @param {JSRecord<db:/geco/profiles>} record
 * @properties={typeid:24,uuid:"3074A255-2EA7-466E-B688-353587554FAF"}
 * @AllowToRunInFind
 */
function validateProfileName(record) {
	var errorMsg = '';
	//valida che ci sia il nome del profilo
	if (globals.isEmpty(record.prof_name)) 
		errorMsg = '- Il campo nome è obbligatorio';
	else {
		//Valida che non sia già stato inserito un profilo con lo stesso nome
		/** @type {JSFoundSet<db:/geco/profiles>} */
		var profiles = databaseManager.getFoundSet('geco', 'profiles');
		if (profiles.find()) {
			profiles.profile_id = '!' + record.profile_id; //diverso dal record che arriva
			profiles.prof_name = record.prof_name;
			var tot = profiles.search();
			if (tot > 0) 
				errorMsg = '- Esiste già un Profilo Professionale con lo stesso nome, modificare quello esistente';
		}
	}
	if (!globals.isEmpty(errorMsg)) throw new Error(errorMsg);
}