/**
 * Controlla che l'ora di inizio inserita sia maggiore dell'ora di fine
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"56B1CBF9-3B2D-4C06-AEC0-782285B7CE84"}
 */
function validatePositiveDuration(record) {
	if (!globals.isEmpty(record.event_id) && record.events_log_to_events.unit_measure == 2 && !globals.isEmpty(record.time_start) && !globals.isEmpty(record.time_stop) && record.time_start > record.time_stop) {
		throw new Error("- L'ora d'inizio non può essere maggiore dell'ora di fine");
	}
}

/**
 * Controlla che sia presente l'ora di inizio
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"D300C5E5-F55E-47C4-AC57-1FE041ADC187"}
 */
function validateRequireStartTime(record) {
	if (!globals.isEmpty(record.event_id) && globals.isEmpty(record.time_start) && record.events_log_to_events.unit_measure == 2) {
		throw new Error('- Il campo ora inizio non può essere vuoto');
	}
}

/**
 * JStaffa rule modificata (17/12/2014) da testare
 * Controlla che non si inseriscano le trasferte più volte per lo stesso giorno
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"9D080CA0-DA56-4021-AF5C-C8E614E5EB97"}
 * @AllowToRunInFind
 */
function validateEventsTypeLog(record) {
	//	/** @type {JSFoundSet<db:/geco/events_log>} */
	//	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	//	//se è stato inserito il campo evento e il campo è trasferta o trasferta giornaliera
	//	//cerchiamo se ci sono già eventi dello stesso tipo trasferte,per quell'utente,nello stesso giorno
	//	if (!globals.isEmpty(record.event_id) && (record.event_id == 31 || record.event_id == 30)) {
	//		if (events_log.find()) {
	//			events_log.event_log_id = '!' + record.event_log_id;
	//			events_log.event_id = '30||31';
	//			events_log.user_id = record.user_id;
	//			events_log.event_log_date = record.event_log_date;
	//			var count = events_log.search();
	//			if (count > 0) {
	//				throw new Error('- Esiste già un evento di tipo trasferta per il giorno ' + record.event_log_date.toLocaleDateString());
	//			}
	//		}
	//	}
	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	//se è stato inserito il campo evento di tipo trasferta
	//cerchiamo se ci sono già eventi dello stesso tipo trasferte,per quell'utente,nello stesso giorno
	if ( (!globals.isEmpty(record.event_id) && record.events_log_to_events && record.events_log_to_events.event_type_id && record.events_log_to_events.event_type_id == 8)) {
		if (events_log.find()) {
			events_log.event_log_id = '!' + record.event_log_id;
			events_log.events_log_to_events.event_type_id = 8; //event_type_id 8 = Trasferta
			events_log.user_id = record.user_id;
			events_log.event_log_date = record.event_log_date;
			var count = events_log.search();
			if (count > 0) {
				throw new Error('- Esiste già un evento di tipo Trasferta per il giorno ' + record.event_log_date.toLocaleDateString());
			}
		}
	}
}

/**
 * JS da TESTARE
 * Controlla che non si inseriscano le reperibilità a gettone FESTIVO/FERIALE più volte per lo stesso giorno
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"482306D4-820B-41E9-B31D-959A9AE74E5C"}
 * @AllowToRunInFind
 */
function validateTokens(record) {
	if (record && record.events_log_to_events && record.events_log_to_events.events_to_event_types && record.events_log_to_events.events_to_event_types.event_type_id && record.events_log_to_events.events_to_event_types.event_type_id == 7) {
		/** @type {String} */
		var extFepCode = record.events_log_to_events.external_code_fep.toString();
		var strFepCode = extFepCode.slice(1, 3);
		application.output('strFepCode: ' + strFepCode);

		/*Formato reperibilità:
		 * R+CODICE(2 Lettere)+VALORE
		 * Esempio: RLV10 --> Reperibilità Lunedì-Venerdì valore 10
		 *
		 * Legenda stringhe:
		 * LV = Lunedì-Venerdì
		 * SB = Sabato
		 * DM = Domenica
		 * SD = Sabato-Domenica-Festivo
		 * FS = Festivo (solo FESTIVI, può essere su qualsiasi giorno FESTIVO)
		 * FL = Flat (qualsiasi giorno)
		 *
		 * NB: decidere se tenere SD o FS, dal momento che ci sono casi per i gettoni SB e DM
		 */

		switch (strFepCode) {
		case 'LV':
			//FERIALE (LUN-VEN): Controllo che non sia nè festivo nè sabato
			if ( (record.event_log_date.getDay() == 0 || record.event_log_date.getDay() == 6 || record.events_log_to_calendar_days.is_working_day == 0 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day))) {
				throw new Error('- Non è possibile inserire un evento di tipo ' + record.events_log_to_events.event_name + ' per il giorno ' + record.event_log_date.toLocaleDateString());
			}
			break;

		case 'SB':
			//SABATO: Controllo che non sia festivo nè un giorno tra 0 e 5 (lunedì-domenica)
			if ( (record.event_log_date.getDay() != 6 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day))) {
				throw new Error('- Non è possibile inserire un evento di tipo ' + record.events_log_to_events.event_name + ' per il giorno ' + record.event_log_date.toLocaleDateString());
			}
			break;

		case 'DM':
			//DOMENICA: Controllo che non sia festivo nè un giorno tra 1 e 6 (lunedì-sabato)
			if ( (record.event_log_date.getDay() != 0 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day))) {
				throw new Error('- Non è possibile inserire un evento di tipo ' + record.events_log_to_events.event_name + ' per il giorno ' + record.event_log_date.toLocaleDateString());
			}
			break;

		case 'SD':
			//FESTIVO (SAB, DOM, PATRONO, IS_HOLIDAY): Controllo che sia festivo o sabato
			if (record.event_log_date.getDay() != 0 && record.event_log_date.getDay() != 6 && record.events_log_to_calendar_days.is_holiday != 1 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
				throw new Error('- Non è possibile inserire un evento di tipo ' + record.events_log_to_events.event_name + ' per il giorno ' + record.event_log_date.toLocaleDateString());
			}
			break;

		case 'FS':
			//FESTIVO (PATRONO, IS_HOLIDAY): Controllo che sia festivo
			if (record.events_log_to_calendar_days.is_holiday != 1 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
				throw new Error('- Non è possibile inserire un evento di tipo ' + record.events_log_to_events.event_name + ' per il giorno ' + record.event_log_date.toLocaleDateString());
			}
			break;

		case 'FL':
			break;

		default:
			throw new Error('- Errore: codice FEP non trovato. Contattare l\'helpdesk.');
			break;
		}

		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log = databaseManager.getFoundSet('geco', 'events_log');

		//cerco se ci sono già eventi dello stesso tipo, per quell'utente, nello stesso giorno
		if (events_log.find()) {
			events_log.event_log_id = '!' + record.event_log_id;
			events_log.events_log_to_events.event_type_id = 7;
			//			non posso mettere 2 flat nello stesso giorno
			//			if (strFepCode == 'FL') {
			//				events_log.external_code_fep = '%FL%';
			//			} else {
			//				events_log.external_code_fep = '!%FL%';
			//			}
			events_log.user_id = record.user_id;
			events_log.event_log_date = record.event_log_date;
			var count = events_log.search();
			if (count > 0) {
				throw new Error('- Esiste già un evento di tipo Reperibilità Gettone per il giorno ' + record.event_log_date.toLocaleDateString());
			}
		}
	}
	//	//FERIALE (LUN-VEN): 41 Controllo che non sia nè festivo nè sabato
	//	if (record.event_id == 41 && (record.event_log_date.getDay() == 0 || record.event_log_date.getDay() == 6 || record.events_log_to_calendar_days.is_working_day == 0 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day))) {
	//		throw new Error('- Non è possibile inserire un evento di tipo reperibilità gettone lun-ven per il giorno ' + record.event_log_date.toLocaleDateString());
	//	}
	//
	//	//FESTIVO (SAB, DOM, PATRONO, IS_HOLIDAY): 42 Controllo che sia festivo o sabato
	//	if (record.event_id == 42 && (record.event_log_date.getDay() != 0 && record.event_log_date.getDay() != 6 && record.events_log_to_calendar_days.is_holiday != 1 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day))) {
	//		throw new Error('- Non è possibile inserire un evento di tipo reperibilità gettone sab-dom per il giorno ' + record.event_log_date.toLocaleDateString());
	//	}
	//
	//	//prendo il foundset
	//	/** @type {JSFoundSet<db:/geco/events_log>} */
	//	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	//
	//	//se è stato inserito il campo evento e il campo è reperibilità gettone festivo o feriale
	//	//cerchiamo se ci sono già eventi dello stesso tipo,per quell'utente,nello stesso giorno
	//	if (events_log.find()) {
	//		events_log.event_log_id = '!' + record.event_log_id;
	//		//[FERIALE (LUN-VEN): 41, FESTIVO (SAB, DOM, PATRONO, IS_HOLIDAY): 42]
	//		events_log.event_id = '41||42';
	//		events_log.user_id = record.user_id;
	//		events_log.event_log_date = record.event_log_date;
	//		var count = events_log.search();
	//		if (count > 0) {
	//			throw new Error('- Esiste già un evento di tipo reperibilità gettone per il giorno ' + record.event_log_date.toLocaleDateString());
	//		}
	//	}
}

/**
 * Controlla che sia stata inserita l'ora di fine
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"56C99756-F992-4982-B9FB-F09428A43859"}
 */
function validateRequireStopTime(record) {
	if (!globals.isEmpty(record.event_id) && globals.isEmpty(record.time_stop) && record.events_log_to_events.unit_measure == 2) {
		throw new Error('- Il campo ora fine non può essere vuoto');
	}
}

/**
 * Controlla che sia stato inserito l'evento
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"42394878-C347-4D2E-A86E-7A87661FAAEA"}
 */
function validateRequireEvent(record) {
	if (globals.isEmpty(record.event_id)) throw new Error('- Il campo evento non può essere vuoto');
}

/**
 * Controlla che la data dell'evento sia compresa fra le date di inizio e fine rapporto lavorativo
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"865F1ADE-03AC-40A5-BED0-86E94145049E"}
 */
function validateEventDate(record) {
	// se la data non esiste ---eccezione
	if (globals.isEmpty(record.event_log_date)) throw new Error('- Il campo data non esiste');
	//se la data esiste,deve essere maggiore della start date
	if (record.events_log_to_users.enrollment_start_date != null) {

		/** @type {Date} */
		var startDate = new Date(record.events_log_to_users.enrollment_start_date.getFullYear(),
				record.events_log_to_users.enrollment_start_date.getMonth(),
				record.events_log_to_users.enrollment_start_date.getDate());
		if (record.event_log_date < startDate)
			throw new Error('- Non è possibile consuntivare in giorni precedenti alla data di inizio rapporto');
	}

	//se la data esiste,deve essere minore della end date
	if (record.events_log_to_users.enrollment_end_date != null) {
		/** @type {Date} */
		var endDate = new Date(record.events_log_to_users.enrollment_end_date.getFullYear(),
				record.events_log_to_users.enrollment_end_date.getMonth(),
				record.events_log_to_users.enrollment_end_date.getDate());
		if (record.event_log_date > endDate)
			throw new Error('- Non è possibile consuntivare in giorni successivi alla data di fine rapporto');
	}
	record.event_month = record.event_log_date.getMonth() + 1;
	record.event_year = record.event_log_date.getFullYear();
}

/**
 * controlla che sia stata selezionata una commessa
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"D303908A-E823-4F0C-97B4-672AEF4CEC76"}
 */
function validateRequireJobOrder(record) {
	if (!globals.isEmpty(record.event_id) && globals.isEmpty(record.job_order_id) && record.events_log_to_events.has_job_order) {
		//FS
		globals.allowedJobOrderSelection = true;
		throw new Error('- Il campo commessa non può essere vuoto per un evento di questo tipo');
	}
}

/**
 * Controlla che quando si cancella lavoro ordinario non sia presente lavoro straordinario nel giorno
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"46308B4C-5068-408B-8F01-D3ABA706CA03"}
 * @AllowToRunInFind
 */
function validateDeletionWhenOvertime(record) {
	//var working_hours = globals.getUserWorkingHoursByDay(record.user_id, record.event_log_date, record.events_log_to_users.working_hours);
	var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
	var working_hours = objTimeUser.workingHours;
	if (record.event_id == 0 || [1, 3, 4].indexOf(record.events_log_to_events.events_to_event_types.event_type_id) > -1) {
		var resultOrd = 0;
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log = databaseManager.getFoundSet('geco', 'events_log');
		if (events_log.find()) {
			events_log.event_log_date = record.event_log_date;
			events_log.user_id = record.user_id;
			events_log.event_log_id = '!' + record.event_log_id;
			events_log.event_id = '!8';
			resultOrd = events_log.search();
		}

		application.output('resultOrd: ' + resultOrd);
		if (resultOrd > 0) {
			//application.output((events_log.total_duration_regular < (working_hours * 60) && events_log.total_duration_overtime > 0));
			//if (events_log.total_duration_regular < 480 && events_log.total_duration_overtime > 0)
			if (events_log.total_duration_regular < (working_hours * 60) && events_log.total_duration_overtime > 0)
				throw new Error('- Impossibile eliminare eventi in presenza di ore straordinarie');
		}
	}
	//if (false) throw new Error('- Impossibile eliminare eventi in presenza di ore straordinarie');
}

/**
 * Controlla che quando si modifica lavoro ordinario non sia presente lavoro straordinario nel giorno
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"3371B74A-937A-4488-86C3-6AE4851E3DD3"}
 * @AllowToRunInFind
 */
function validateUpdateOrdinaryWhenOvertime(record) {
	//var working_hours = globals.getUserWorkingHoursByDay(record.user_id, record.event_log_date, record.events_log_to_users.working_hours);
	var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
	var working_hours = objTimeUser.workingHours;
	if (record.event_id == 0 || [1, 3, 4].indexOf(record.events_log_to_events.events_to_event_types.event_type_id) > -1) {
		var resultOrd = 0;
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log = databaseManager.getFoundSet('geco', 'events_log');
		if (events_log.find()) {
			events_log.event_log_date = record.event_log_date;
			events_log.user_id = record.user_id;
			events_log.event_log_id = '!' + record.event_log_id;
			events_log.event_id = '!8';
			resultOrd = events_log.search();
		}

		application.output('resultOrd: ' + resultOrd);
		if (resultOrd > 0) {
			var dataset = record.getChangedData();
			for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
				// se è stato modificato time_start o time_stop e il vecchio valore differisce dal nuovo
				if ( (dataset.getValue(i, 1) == 'time_start' || (dataset.getValue(i, 1) == 'time_stop')) && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
					if (events_log.total_duration_regular < (working_hours * 60) && events_log.total_duration_overtime > 0)
						throw new Error('- Impossibile modificare eventi in presenza di ore straordinarie');
				}
			}
		}
		//application.output((events_log.total_duration_regular < (working_hours * 60) && events_log.total_duration_overtime > 0));
		//if (events_log.total_duration_regular < 480 && events_log.total_duration_overtime > 0)

	}
	//if (false) throw new Error('- Impossibile eliminare eventi in presenza di ore straordinarie');
}

/**
 * Valida la cancellazione di eventi strao riposo compensativo (event_id in 25,28)
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"151CAD7F-3085-4832-B241-0EB2E46AC321"}
 * @AllowToRunInFind
 */
function validateDeletionAdditionalChargeOvertime(record) {
	if (record.events_log_to_events && record.events_log_to_events.event_id && record.events_log_to_events.event_id == 25 || record.events_log_to_events && record.events_log_to_events.event_id && record.events_log_to_events.event_id == 28) {
		/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
		var events_log_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');
		var result = 0;
		//		/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
		//		var events_log_additional_charge_all = databaseManager.getFoundSet('geco', 'events_log_additional_charge');
		//		var resultAll = 0;

		if (events_log_additional_charge.find()) {
			events_log_additional_charge.el_month = record.event_month;
			events_log_additional_charge.el_year = record.event_year;
			events_log_additional_charge.user_id = record.user_id;
			result = events_log_additional_charge.search();
		}
		//
		//		if (events_log_additional_charge_all.find()) {
		//			events_log_additional_charge_all.is_expired = 0;
		//			events_log_additional_charge_all.user_id = record.user_id;
		//			resultAll = events_log_additional_charge_all.search();
		//		}
		if (result > 0) {
			for (var index = 1; index <= result; index++) {
				var recResult = events_log_additional_charge.getRecord(index);

				//ore rimanenti < ore da togliere non posso farlo
				if (record.event_id == 28) {
					if (recResult.duration_remained < record.duration) {
						throw new Error('- Nel giorno ' + record.event_log_date.toLocaleDateString() + ' non è possibile eliminare eventi di tipo lavoro straordinario con riposo compensativo: esistono eventi di recupero straordinari.');
					}
				}//se per quel mese non ci sono ore da recuperare
				else if (record.event_id == 25) {
					if (record.duration > recResult.duration_retrieved) {
						throw new Error('- Nel giorno ' + record.event_log_date.toLocaleDateString() + ' non è possibile eliminare eventi di tipo riposo compensativo straordinario in assenza di ore lavorate.');
					}
				}
			}
		} //se non ne trovo nessuno
		else {
			if (record.events_log_to_events.event_id == 28) {
				throw new Error('- Nel giorno ' + record.event_log_date.toLocaleDateString() + ' non è possibile eliminare eventi di tipo lavoro straordinario con riposo compensativo.');
			} else if (record.events_log_to_events.event_id == 25) {
				throw new Error('- Nel giorno ' + record.event_log_date.toLocaleDateString() + ' non è possibile eliminare eventi di tipo riposo compensativo straordinario.');
			}
		}
	}
}

/**
 * Controlla che gli eventi non si sovrappongano
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * (StartDate1 <= EndDate2) and (StartDate2 <= EndDate1)
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"C33DD4BE-C290-4BDB-9E30-661DC9A2D766"}
 */
function validateOverlapping(record) {
	// only for events measured in hours
	if (!globals.isEmpty(record.event_id) && record.events_log_to_events.unit_measure == 2) {
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log = databaseManager.getFoundSet('geco', 'events_log');
		if (events_log.find()) {
			events_log.event_log_date = record.event_log_date;
			events_log.user_id = record.user_id;
			events_log.events_log_to_events.unit_measure = 2;
			if (events_log.search() > 0) {
				for (var index = 1; index <= events_log.getSize(); index++) {
					var loggedStart = events_log.getRecord(index).time_start;
					var loggedStop = events_log.getRecord(index).time_stop;
					var record_id = events_log.getRecord(index).event_log_id;
					if (record_id != record.event_log_id && ( (record.time_start < loggedStop && record.time_stop >= loggedStop) || //inserito dopo un evento esistente
					(record.time_stop > loggedStart && record.time_start <= loggedStart) || (record.time_start >= loggedStart && record.time_stop <= loggedStop))) //inserito prima di un evento esistente
					{
						throw new Error("- Nel giorno " + record.event_log_date.toLocaleDateString() + " l'evento inserito si sovrappone ad altri");
					}
				}
			}
		}
	}
}

/**
 * Controlla che siano state inserite tutte le ore ordinarie per poter inserire gli straordinari
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"8DA707CD-2A88-4495-B964-584A001D2EFB"}
 */
function validateOvertime(record) {
	// se si sta inserendo lavoro straordinario
	if (!globals.isEmpty(record.event_id) && (record.event_id == 27 || record.event_id == 28)) {
		var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
		var contract_type_id = 0;
		contract_type_id = objTimeUser.contractId;
		//se il tipo di contratto non è full time per il giorno dell'evento che si sta inserendo
		if (contract_type_id != 1) {
			throw new Error("- Nel giorno " + record.event_log_date.toLocaleDateString() + " non è possibile inserire lavoro straordinario. Inserire lavoro supplementare");
		}

		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log = databaseManager.getFoundSet('geco', 'events_log');

		/** @type {Number} */
		var totEventsLog = 0;

		// get the already logged ordinary (included Maternity, Disease, Holidays/Permissions) events for this day
		if (events_log.find()) {
			events_log.event_log_date = record.event_log_date;
			events_log.user_id = record.user_id;
			events_log.event_id = 0;
			events_log.newRecord();
			events_log.event_log_date = record.event_log_date;
			events_log.user_id = record.user_id;
			events_log.events_log_to_events.events_to_event_types.event_type_id = '1||3||4';
			totEventsLog = events_log.search();
		}

		application.output('totEventsLog = ' + totEventsLog);
		application.output('events_log.total_duration_regular: ' + events_log.total_duration_regular);

		var working_hours = objTimeUser.workingHours;
		//giorni feriali
		if (record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day) && events_log.total_duration_regular < working_hours * 60) {
			throw new Error('- Per inserire ore di lavoro straordinario devono essere prima inserite le ore di lavoro ordinario');
		}
	}
}

/**
 * Valida il lavoro supplementare
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"0500DB37-7135-4B2C-BEB0-8973C6B2BD52"}
 */
function validateSupTime(record) {
	if (!globals.isEmpty(record.event_id) && record.event_id == 37) {
		//controllare che siano fatti prima o dopo il turno per IMPX 7
		var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
		var contract_type_id = 0;
		contract_type_id = objTimeUser.contractId;

		//contract_type_id = record.events_log_to_users.contract_type_id;
		/** @type {JSFoundSet<db:/geco/users_working_hours_history>} */

		//se il tipo di contratto è full time per il giorno dell'evento che si sta inserendo
		if (contract_type_id == 1) {
			throw new Error("- Nel giorno " + record.event_log_date.toLocaleDateString() + " non è possibile inserire lavoro supplementare. Inserire lavoro straordinario");
		}
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log = databaseManager.getFoundSet('geco', 'events_log');

		/** @type {Number} */
		var totEventsLog = 0;

		// get the already logged ordinary (included Maternity, Disease, Holidays/Permissions) events for this day
		if (events_log.find()) {
			events_log.event_log_date = record.event_log_date;
			events_log.user_id = record.user_id;
			events_log.event_id = 0;
			events_log.newRecord();
			events_log.event_log_date = record.event_log_date;
			events_log.user_id = record.user_id;
			events_log.events_log_to_events.events_to_event_types.event_type_id = '1||3||4';
			totEventsLog = events_log.search();
		}

		application.output('totEventsLog = ' + totEventsLog);
		application.output('events_log.total_duration_regular: ' + events_log.total_duration_regular);

		var working_hours = objTimeUser.workingHours;
		//giorni feriali
		if (/*record.events_log_to_calendar_days.is_working_day == 1 && */record.events_log_to_calendar_days.is_holiday == 0 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day) && events_log.total_duration_regular < working_hours * 60) {
			throw new Error('- Per inserire ore di lavoro supplementare devono essere prima inserite le ore di lavoro ordinario');
		}
	}
}

/**
 * Valida la durata ordinaria inserita nella giornata
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"8538B133-F46D-4EA6-B21F-F31E492385D2"}
 * @AllowToRunInFind
 */
function validateDurationOrdinary(record) {
	//FS
	globals.allowedJobOrderSelection = true;

	if (globals.isEmpty(record.event_id)) throw new Error("- Impossibile calcolare la durata dell'evento");
	var duration = 0;
	var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
	application.output('trovato contratto ' + objTimeUser.contractId + ' ' + objTimeUser.startTime + ' ' + objTimeUser.stopTime + ' ' + objTimeUser.breakStart + ' ' + objTimeUser.breakStop + ' ' + objTimeUser.workingHours);

	var break_start = objTimeUser.breakStart;
	var break_stop = objTimeUser.breakStop;

	// se dipendente
	if (record.time_start <= break_start && record.time_stop >= break_stop) {
		duration = (record.time_stop - record.time_start) - (break_stop - break_start);
	} else {
		duration = record.time_stop - record.time_start;
	}

	var working_hours = objTimeUser.workingHours;
	//se per quel giorno l'utente non ha ore lavorative e sta cercando di inserire lavoro ordinario
	if (record.events_log_to_users.user_type_id == 7 && working_hours == 0 && record.event_id == 0) {
		throw new Error("- Nel giorno " + record.event_log_date.toLocaleDateString() + " non è possibile inserire eventi di tipo ordinario. Inserire lavoro supplementare");
	}
	//application.output('validateDurationOrdinary workin_hours = ' + working_hours);

	//se dipendente e durata > working_hours
	if (record.events_log_to_users.user_type_id == 7 && (record.event_id == 0 || record.event_id == 25)) {

		if (duration > (working_hours * 60)) {
			// se full time
			if (objTimeUser.contractId == 1)
				throw new Error("- Nel giorno " + record.event_log_date.toLocaleDateString() + " sono state inserite ore ordinarie in eccesso. Inserire lavoro straordinario");
			else
				throw new Error("- Nel giorno " + record.event_log_date.toLocaleDateString() + " sono state inserite ore ordinarie in eccesso. Inserire lavoro supplementare");

		} else {
			//cerca altri eventi ordinari
			/** @type {JSFoundSet<db:/geco/events_log>} */
			var events_log = databaseManager.getFoundSet('geco', 'events_log');
			var result = 0;
			if (events_log.find()) {
				events_log.event_log_date = record.event_log_date;
				events_log.user_id = record.user_id;
				events_log.event_id = 0;
				events_log.event_log_id = '!' + record.event_log_id;
				result = events_log.search();
			}
			//se dipendente e somma durata eventi >  working_hours
			if (result > 0 && events_log.total_duration + duration > working_hours * 60) {
				if (objTimeUser.contractId == 1)
					throw new Error("- Nel giorno " + record.event_log_date.toLocaleDateString() + " sono state inserite ore ordinarie in eccesso. Inserire gli straordinari");
				else
					throw new Error("- Nel giorno " + record.event_log_date.toLocaleDateString() + " sono state inserite ore ordinarie in eccesso. Inserire lavoro supplementare");

			}
		}
		//se dipendente e durata di tutti gli eventi del giorno non lavoro > working_hours
	} else if (record.events_log_to_users.user_type_id == 7 && record.events_log_to_events.unit_measure == 2 && record.events_log_to_events.event_type_id != 0) {
		if (working_hours > 0 && duration > (working_hours * 60))
			throw new Error("- Nel giorno " + record.event_log_date.toLocaleDateString() + " sono state inserite ore in eccesso.");
		else {
			//cerca altri eventi ordinari
			/** @type {JSFoundSet<db:/geco/events_log>} */
			var events_log1 = databaseManager.getFoundSet('geco', 'events_log');
			var result1 = 0;
			if (events_log1.find()) {
				events_log1.event_log_date = record.event_log_date;
				events_log1.user_id = record.user_id;
				events_log1.event_log_id = '!' + record.event_log_id;
				events_log1.event_id = 0;
				events_log1.newRecord();
				events_log1.event_log_date = record.event_log_date;
				events_log1.user_id = record.user_id;
				events_log1.event_log_id = '!' + record.event_log_id;
				events_log1.events_log_to_events.event_type_id = '!' + 0;

				result1 = events_log1.search();
			}
			//se dipendente e somma durata eventi > 8
			if (result1 > 0 && events_log1.total_duration + duration > working_hours * 60)
				throw new Error("- Nel giorno " + record.event_log_date.toLocaleDateString() + " sono state inserite ore in eccesso");
		}
	}
}

/**
 * valida il lavoro ordinario e le assenze nei giorni feriali e festivi
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"A05D1F44-84DE-48DC-A65F-F78ED0B16150"}
 */
function validateEventTypeByDay(record) {
	if (globals.isEmpty(record.event_id))
		throw new Error("- Impossibile salvare l'evento");
	//lavoro
	if (record.events_log_to_users.user_type_id == 7 && (record.events_log_to_events.event_id == 0 || record.events_log_to_events.event_id == 25)) { //se dipendente e evento lavoro
		if (record.events_log_to_calendar_days.is_holiday == 1 || record.events_log_to_calendar_days.is_working_day == 0) // è festa
			throw new Error("- Il giorno " + record.event_log_date.toLocaleDateString() + " è festivo, non è possibile inserire lavoro ordinario");
		if (record.events_log_to_calendar_days.calendar_weekday == 6 || // sabato
		record.events_log_to_calendar_days.calendar_weekday == 0) //domenica
			throw new Error("- Il giorno " + record.event_log_date.toLocaleDateString() + " è  sabato o domenica, non è possibile inserire lavoro ordinario");
		if (globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) //patrono per user logged
			throw new Error("- Il giorno " + record.event_log_date.toLocaleDateString() + " è festa patronale, non è possibile inserire lavoro ordinario");
	}
	//ferie e permessi esclusi i permessi elettorali e cariche pubbliche
	//Maternità e congedi esclusi il congedo facoltativo e obbligatorio
	if (record.events_log_to_events.event_type_id == 3 && (record.events_log_to_events.event_id != 17 && record.events_log_to_events.event_id != 16) || (record.events_log_to_events.event_type_id == 4 && (record.events_log_to_events.event_id != 14 && record.events_log_to_events.event_id != 15))) {
		if (record.events_log_to_calendar_days.is_holiday == 1 || record.events_log_to_calendar_days.is_working_day == 0) // è festa
			throw new Error("- Il giorno " + record.event_log_date.toLocaleDateString() + " è festivo, non è possibile inserire l\'evento selezionato");
		if (record.events_log_to_calendar_days.calendar_weekday == 6 || // sabato
		record.events_log_to_calendar_days.calendar_weekday == 0) //domenica
			throw new Error("- Il giorno " + record.event_log_date.toLocaleDateString() + " è  sabato o domenica, non è possibile inserire l\'evento selezionato");
		if (globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) //patrono per user logged
			throw new Error("- Il giorno " + record.event_log_date.toLocaleDateString() + " è festa patronale, non è possibile inserire l\'evento selezionato");
	}

	var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
	var working_hours = objTimeUser.workingHours;
	//giorno qualunque utente con pt verticale che non lavora nel gg dell'evento inserito
	if (record.events_log_to_events.event_type_id == 3 && (record.events_log_to_events.event_id != 17 && record.events_log_to_events.event_id != 16) && working_hours == 0 && objTimeUser.contractId != 1) {
		throw new Error("- Nel giorno " + record.event_log_date.toLocaleDateString() + " non è possibile inserire l\'evento selezionato");
	}
}

/**
 * Blocca la modifica di eventi approvati o chiusi per utenti e approvatori(esclusi Controllers e HRAdimn)
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"1F0C9CA7-6211-4E0C-838F-63CF2A8814EC"}
 */
function validateEditEventApprovedClosed(record) {
	if (!globals.hasRole('Controllers') && !globals.hasRole('HR Administrators'))
		if (!globals.isEmpty(record.event_id) && (record.event_log_status == 2 || record.event_log_status == 4))
			throw new Error('- Non è possibile modificare eventi approvati o chiusi');
}

/**
 *
 * Cancellazione lavoro ordinario in presenza di ore di straordinario o supplementare
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"E001605D-F645-48AC-8987-2A3892A9DB99"}
 * @AllowToRunInFind
 */
function validateDeletion(record) {
	var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
	// cancellazione lavoro ordinario in presenza di ore di straordinario o supplementare
	if (record.event_id == 0) {
		var tot = 0;
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log = databaseManager.getFoundSet('geco', 'events_log');
		if (events_log.find()) {
			events_log.event_log_date = record.event_log_date;
			events_log.user_id = record.user_id;
			events_log.event_log_id = '!' + record.event_log_id;
			//events_log.event_id = [27, 37];
			//controllo anche su evento di tipo Straordinario con riposo compensativo
			events_log.event_id = [27, 28, 37];
			tot = events_log.search();
		}
		if (tot > 0) {
			throw new Error('- Non è possibile cancellare lavoro ordinario. Cancellare prima gli eventi straordinario / supplementare');
		}
	}

	if (record.event_id == 0) {
		var tot_hvi = 0;
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log_hvi = databaseManager.getFoundSet('geco', 'events_log');
		if (events_log_hvi.find()) {
			events_log_hvi.event_log_date = record.event_log_date;
			events_log_hvi.user_id = record.user_id;
			events_log_hvi.event_log_id = '!' + record.event_log_id;
			events_log_hvi.event_id = 8;
			tot_hvi = events_log_hvi.search();
		}
		if (tot_hvi > 0) {
			throw new Error('- Non è possibile cancellare lavoro ordinario. Cancellare prima le ore viaggio');
		}
	}

	//cancellazione straordinari o supplementari in un gg festivo oppure utente part time in giorno feriale (nessuna ora lavorativa)
	if ( (record.event_id == 27 || record.event_id == 28 || record.event_id == 37 || record.event_id == 38) && (record.events_log_to_calendar_days.is_working_day == 0 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) || ( (record.event_id == 27 || record.event_id == 37 || record.event_id == 38) && (objTimeUser.workingHours == 0 && record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0))) {
		var totale = 0;
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log1 = databaseManager.getFoundSet('geco', 'events_log');
		if (events_log1.find()) {
			events_log1.event_log_date = record.event_log_date;
			events_log1.user_id = record.user_id;
			events_log1.event_log_id = '!' + record.event_log_id;
			events_log1.event_id = 8;
			totale = events_log1.search();
		}
		var totaleStrao = 0;
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_logStrao = databaseManager.getFoundSet('geco', 'events_log');
		if (events_logStrao.find()) {
			events_logStrao.event_log_date = record.event_log_date;
			events_logStrao.user_id = record.user_id;
			events_logStrao.event_log_id = '!' + record.event_log_id;
			events_logStrao.event_id = [27, 28, 37, 38];
			totaleStrao = events_logStrao.search();
		}

		if (totaleStrao == 0 && totale > 0) {
			throw new Error('- Non è possibile cancellare lavoro straordinario / supplementare. Cancellare prima le ore viaggio');
		}
	}
}

/**
 * Modifica lavoro ordinario in presenza di ore di straordinario o supplementare
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"700BD61D-98BC-4912-8277-034802F56B71"}
 * @AllowToRunInFind
 */
function validateUpdateOrdinary(record) {
	var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
	// cancellazione lavoro ordinario in presenza di ore di straordinario o supplementare
	if (record.event_id == 0) {
		var tot = 0;
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log = databaseManager.getFoundSet('geco', 'events_log');
		if (events_log.find()) {
			events_log.event_log_date = record.event_log_date;
			events_log.user_id = record.user_id;
			events_log.event_log_id = '!' + record.event_log_id;
			//events_log.event_id = [27, 37];
			//controllo anche su evento di tipo Straordinario con riposo compensativo
			events_log.event_id = [27, 28, 37];
			tot = events_log.search();
		}
		if (tot > 0) {
			var dataset = record.getChangedData();
			for (var i = 1; i <= dataset.getMaxRowIndex(); i++) {
				// se è stato modificato time_start o time_stop e il vecchio valore differisce dal nuovo
				if ( (dataset.getValue(i, 1) == 'time_start' || (dataset.getValue(i, 1) == 'time_stop')) && dataset.getValue(i, 2) != dataset.getValue(i, 3)) {
					throw new Error('- Non è possibile modificare lavoro ordinario. Cancellare prima gli eventi straordinario / supplementare');
				}
			}
		}
	}

	if (record.event_id == 0) {
		var tot_hvi = 0;
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log_hvi = databaseManager.getFoundSet('geco', 'events_log');
		if (events_log_hvi.find()) {
			events_log_hvi.event_log_date = record.event_log_date;
			events_log_hvi.user_id = record.user_id;
			events_log_hvi.event_log_id = '!' + record.event_log_id;
			events_log_hvi.event_id = 8;
			tot_hvi = events_log_hvi.search();
		}
		if (tot_hvi > 0) {
			throw new Error('- Non è possibile modificare lavoro ordinario. Cancellare prima le ore viaggio');
		}
	}

	//cancellazione straordinari o supplementari in un gg festivo oppure utente part time in giorno feriale (nessuna ora lavorativa)
	if ( (record.event_id == 27 || record.event_id == 28 || record.event_id == 37 || record.event_id == 38) && (record.events_log_to_calendar_days.is_working_day == 0 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) || ( (record.event_id == 27 || record.event_id == 37 || record.event_id == 38) && (objTimeUser.workingHours == 0 && record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0))) {
		var totale = 0;
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log1 = databaseManager.getFoundSet('geco', 'events_log');
		if (events_log1.find()) {
			events_log1.event_log_date = record.event_log_date;
			events_log1.user_id = record.user_id;
			events_log1.event_log_id = '!' + record.event_log_id;
			events_log1.event_id = 8;
			totale = events_log1.search();
		}
		var totaleStrao = 0;
		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_logStrao = databaseManager.getFoundSet('geco', 'events_log');
		if (events_logStrao.find()) {
			events_logStrao.event_log_date = record.event_log_date;
			events_logStrao.user_id = record.user_id;
			events_logStrao.event_log_id = '!' + record.event_log_id;
			events_logStrao.event_id = [27, 28, 37, 38];
			totaleStrao = events_logStrao.search();
		}

		if (totaleStrao == 0 && totale > 0) {
			throw new Error('- Non è possibile modificare lavoro straordinario / supplementare. Cancellare prima le ore viaggio');
		}
	}
}

/**
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"E6AA6980-9D09-4C3F-8EE0-DBEB527E1852"}
 */
function validateDurationWorkingHours(record) {
	//	var duration = 0;
	//	var break_start = globals.getUserBreakStart(record.user_id, record.event_log_date);
	//	var break_stop = globals.getUserBreakStop(record.user_id, record.event_log_date);
	//
	//	//	if (record.time_start <= record.events_log_to_users.break_start && record.time_stop >= record.events_log_to_users.break_stop) {
	//	//		duration = (record.time_stop - record.time_start) - (record.events_log_to_users.break_stop - record.events_log_to_users.break_start);
	//	//	} else {
	//	//		duration = record.time_stop - record.time_start;
	//	//	}
	//	if (record.time_start <= break_start && record.time_stop >= break_stop) {
	//		duration = (record.time_stop - record.time_start) - (break_stop - break_start);
	//	} else {
	//		duration = record.time_stop - record.time_start;
	//	}
	//	//var working_hours = record.events_log_to_users.working_hours;
	//	var working_hours = globals.getUserWorkingHoursByDay(record.user_id, record.event_log_date, record.events_log_to_users.working_hours);
	//	application.output('validateDurationWorkingHours workin_hours = ' + working_hours);
	//	/** @type {JSFoundSet<db:/geco/users_working_hours_history>} */
	//	var users_working_hours_history = databaseManager.getFoundSet('geco', 'users_working_hours_history');
	//
	//	//cerco lo storico delle working_hours dell'utente
	//	if (users_working_hours_history.find()) {
	//		users_working_hours_history.user_id = record.user_id;
	//		if (users_working_hours_history.search() > 0) {
	//			users_working_hours_history.sort('start_change_date desc, created_at desc');
	//			for (var index = 1; index <= users_working_hours_history.getSize(); index++) {
	//				var rec = users_working_hours_history.getRecord(index);
	//				if (record.event_log_date >= rec.start_change_date) {
	//					working_hours = rec.actual_working_hours;
	//					break;
	//				} else {
	//					working_hours = rec.previous_working_hours;
	//				}
	//			}
	//
	//		}
	//	}

	// only for events measured in hours
	// uguali a lavoro ordinario o diversi da tutti gli altri event_type misurati a ore
	//	if (!globals.isEmpty(record.event_id) && record.events_log_to_users.user_type_id == 7 &&
	//		record.events_log_to_events.unit_measure == 2 &&
	//		(record.event_id == 0 || record.events_log_to_events.event_type_id !=0 )) {
	//
	//
	//		if (duration > (working_hours * 60))
	//				throw new Error("- Nel giorno " + record.event_log_date.toLocaleString() + " sono state inserite ore in eccesso.");
	//		else {
	//			//cerca altri eventi ordinari
	//			/** @type {JSFoundSet<db:/geco/events_log>} */
	//			var events_log = databaseManager.getFoundSet('geco', 'events_log');
	//			var result = 0;
	//			if (events_log.find()) {
	//				events_log.event_log_date = record.event_log_date;
	//				events_log.user_id = record.user_id;
	//				events_log.event_log_id = '!' + record.event_log_id;
	//				events_log.event_id = 0;
	//				if (record.event_id == 0) {
	//					events_log.newRecord();
	//					events_log.event_log_date = record.event_log_date;
	//					events_log.user_id = record.user_id;
	//					events_log.event_log_id = '!' + record.event_log_id;
	//					events_log.events_log_to_events.event_type_id = '!' + 0;
	//				}
	//				result = events_log.search();
	//			}
	//			//se dipendente e somma durata eventi > 8
	//			if (result > 0 && events_log.total_duration + duration > working_hours * 60)
	//				throw new Error("- Nel giorno " + record.event_log_date.toLocaleString() + " sono state inserite ore in eccesso");
	//		}
	//	}
}

/**
 * Controlla che il mese sia abilitato per la consuntivazione
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"9F849DEB-4D71-4474-931F-CB5E9EB1A212"}
 */
function validateEnableMonth(record) {
	if (!globals.hasRole('Controllers') && !globals.hasRole('HR Administrators'))
		if (!globals.isEmpty(record.event_id) && record.events_log_to_calendar_days.calendar_days_to_calendar_months.is_enabled_for_timesheet == 0)
			throw new Error('- Non è possibile inserire eventi in un mese disabilitato');
}

/**
 * Valida inizio e fine lavoro in base all'anagrafica e storico ore lavorative dell'utente
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"28AB63C6-81FB-4913-9800-DA138D10B79F"}
 * @AllowToRunInFind
 */
function validateStartStopTimeEventLog(record) {
	//solo per IMPX
	var startDisplay;
	var stopDispaly;
	var shiftStartDisplay;
	var shiftStopDispaly;
	if (record.events_log_to_users.user_type_id == 7) {
		var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
		application.output('trovato contratto ' + objTimeUser.contractId + ' ' + objTimeUser.startTime + ' ' + objTimeUser.stopTime + ' ' + objTimeUser.breakStart + ' ' + objTimeUser.breakStop + ' ' + objTimeUser.workingHours);

		startDisplay = globals.formatMinutes(objTimeUser.startTime);
		stopDispaly = globals.formatMinutes(objTimeUser.stopTime);

		//NO turnista lavoro ordinario nei giorni feriali
		if (record.events_log_to_users.is_shift_worker != 1) {
			if ( (record.event_id == 0 || record.event_id == 25) && record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
				if (record.time_start < objTimeUser.startTime && objTimeUser.startTime > 0)
					throw new Error('- L\'ora di inizio del lavoro ordinario non può essere minore delle ' + startDisplay);
				if (record.time_stop > objTimeUser.stopTime && objTimeUser.stopTime > 0)
					throw new Error('- L\'ora di fine del lavoro ordinario non può essere maggiore delle ' + stopDispaly);
			}
		}
		//SI turnista, controllo con inizio e fine del turno
		else {
			shiftStartDisplay = globals.formatMinutes(objTimeUser.shiftStartTime);
			shiftStopDispaly = globals.formatMinutes(objTimeUser.shiftStopTime);
			if ( (record.event_id == 0 || record.event_id == 25) && record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
				if (record.time_start < objTimeUser.shiftStartTime)
					throw new Error('- L\'ora di inizio del lavoro ordinario non può essere minore delle ' + shiftStartDisplay);
				if (record.time_stop > objTimeUser.shiftStopTime)
					throw new Error('- L\'ora di fine del lavoro ordinario non può essere maggiore delle ' + shiftStopDispaly);
			}
		}
		//malattia deve essere verificato anche nei giorni non lavorativi
		if (record.events_log_to_users.is_shift_worker != 1) {
			if (record.events_log_to_events && record.events_log_to_events.event_type_id && record.events_log_to_events.event_type_id == 1) {
				if (record.time_start < objTimeUser.startTime && objTimeUser.startTime > 0)
					throw new Error('- L\'ora di inizio dell\'evento non può essere minore delle ' + startDisplay);
				if (record.time_stop > objTimeUser.stopTime && objTimeUser.stopTime > 0)
					throw new Error('- L\'ora di fine dell\'evento non può essere maggiore delle ' + stopDispaly);
			}
		} else {
			shiftStartDisplay = globals.formatMinutes(objTimeUser.shiftStartTime);
			shiftStopDispaly = globals.formatMinutes(objTimeUser.shiftStopTime);
			if (record.events_log_to_events && record.events_log_to_events.event_type_id && record.events_log_to_events.event_type_id == 1) {
				if (record.time_start < objTimeUser.shiftStartTime)
					throw new Error('- L\'ora di inizio dell\'evento non può essere minore delle ' + shiftStartDisplay);
				if (record.time_stop > objTimeUser.shiftStopTime)
					throw new Error('- L\'ora di fine dell\'evento non può essere maggiore delle ' + shiftStopDispaly);
			}
		}
		//ferie e permessi nei giorni lavorativi oppure permesso elettorale o cariche pubbliche ( tutti i gg)
		if (record.events_log_to_users.is_shift_worker != 1) {
			if ( (record.events_log_to_events && record.events_log_to_events.event_type_id && record.events_log_to_events.event_type_id == 3 && record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) || //permesso cariche pubbliche o elettorale
			(record.events_log_to_events && record.events_log_to_events.event_type_id && record.events_log_to_events.event_type_id == 3 && (record.events_log_to_events.event_id == 17 || record.events_log_to_events.event_id == 16))) {
				if (record.time_start < objTimeUser.startTime && objTimeUser.startTime > 0)
					throw new Error('- L\'ora di inizio dell\'evento non può essere minore delle ' + startDisplay);
				if (record.time_stop > objTimeUser.stopTime && objTimeUser.stopTime > 0)
					throw new Error('- L\'ora di fine dell\'evento non può essere maggiore delle ' + stopDispaly);
			}
		} else {
			shiftStartDisplay = globals.formatMinutes(objTimeUser.shiftStartTime);
			shiftStopDispaly = globals.formatMinutes(objTimeUser.shiftStopTime);
			if ( (record.events_log_to_events && record.events_log_to_events.event_type_id && record.events_log_to_events.event_type_id == 3 && record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) || //permesso cariche pubbliche o elettorale
			(record.events_log_to_events && record.events_log_to_events.event_type_id && record.events_log_to_events.event_type_id == 3 && (record.events_log_to_events.event_id == 17 || record.events_log_to_events.event_id == 16))) {
				if (record.time_start < objTimeUser.shiftStartTime)
					throw new Error('- L\'ora di inizio dell\'evento non può essere minore delle ' + shiftStartDisplay);
				if (record.time_stop > objTimeUser.shiftStopTime)
					throw new Error('- L\'ora di fine dell\'evento non può essere maggiore delle ' + shiftStopDispaly);
			}
		}
		//congedi parentali e allattamento nei gg lavorativi oppure congedo astensione facoltativa congedo astensione obbligatoria tutti i gg
		//2 eventi sono permessi nei giorni festivi
		if ( (record.events_log_to_events && record.events_log_to_events.event_type_id && record.events_log_to_events.event_type_id == 4 && record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) || //congedo astensione facoltativa congedo astensione obbligatoria
		(record.events_log_to_events && record.events_log_to_events.event_type_id && record.events_log_to_events.event_type_id == 4 && (record.events_log_to_events.event_id == 14 || record.events_log_to_events.event_id == 15))) {
			if (record.time_start < objTimeUser.startTime && objTimeUser.startTime > 0)
				throw new Error('- L\'ora di inizio dell\'evento non può essere minore delle ' + startDisplay);
			if (record.time_stop > objTimeUser.stopTime && objTimeUser.stopTime > 0)
				throw new Error('- L\'ora di fine dell\'evento non può essere maggiore delle ' + stopDispaly);
		}
		//lavoro strarodinario/straordinario con riposo compensativo/supplemetare/intervento straordinario in reperibilità nei giorni feriali non patrono dell'utente, esclusi i lavoratori turnisti
		if (record.events_log_to_users.is_shift_worker != 1 && (record.event_id == 27 || record.event_id == 28 || record.event_id == 37 || record.event_id == 38) && record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
			if ( (objTimeUser.startTime <= record.time_start && record.time_start < objTimeUser.stopTime) || (objTimeUser.startTime < record.time_stop && record.time_stop <= objTimeUser.stopTime))
				throw new Error('- Il lavoro straordinario / supplementare non può essere inserito nell\'orario ' + startDisplay + ' - ' + stopDispaly);

		}
		//lavoro supplementare nei giorni feriali non patrono dell'utente, esclusi i lavoratori turnisti
		//		if (record.events_log_to_users.is_shift_worker!=1 && record.event_id == 37 && record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)){
		//			if ( objTimeUser.startTime != 0 && objTimeUser.stopTime != 0 &&
		//				(( objTimeUser.startTime <= record.time_start && record.time_start < objTimeUser.stopTime ) ||
		//				 ( objTimeUser.startTime <record.time_stop && record.time_stop <= objTimeUser.stopTime )))
		//				throw new Error('- Il lavoro supplementare non può essere inserito nell\'orario ' + startDisplay + ' - ' + stopDispaly);
		//		}

		//ore viaggio in giorni feriali non patrono dell'utente, esclusi i lavoratori turnisti
		if (record.events_log_to_users.is_shift_worker != 1 && record.event_id == 8 && record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
			if (objTimeUser.startTime != 0 && objTimeUser.stopTime != 0 && ( (objTimeUser.startTime <= record.time_start && record.time_start < objTimeUser.stopTime) || (objTimeUser.startTime < record.time_stop && record.time_stop <= objTimeUser.stopTime)) || (record.time_start <= objTimeUser.startTime && record.time_stop >= objTimeUser.stopTime))
				throw new Error('- Non è possibile inserire ore viaggio nella fascia ' + startDisplay + ' - ' + stopDispaly + '. Inserire lavoro ordinario');
		}

		//ore viaggio,sabato e domenica,festivi o patrono dell'utente,solo se ha inserito straordinario oppure utente part time in giorno feriale (nessuna ora lavorativa)
		if (record.event_id == 8 && (record.events_log_to_calendar_days.is_working_day == 0 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)
		) || (record.event_id == 8 && (objTimeUser.workingHours == 0 && record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0))) {
			/** @type {JSFoundSet<db:/geco/events_log>} */
			var events_log = databaseManager.getFoundSet('geco', 'events_log');
			var result = 0;
			// cerco eventi di tipo straordinario/supplementare
			if (events_log.find()) {
				events_log.event_log_date = record.event_log_date;
				events_log.user_id = record.user_id;
				events_log.event_log_id = '!' + record.event_log_id;
				events_log.event_id = [27, 37, 38];
				//				events_log.newRecord();
				//				events_log.event_log_date = record.event_log_date;
				//				events_log.user_id = record.user_id;
				//				events_log.event_log_id = '!' + record.event_log_id;
				//				events_log.event_id = 37;
				//				events_log.newRecord();
				//				events_log.event_log_date = record.event_log_date;
				//				events_log.user_id = record.user_id;
				//				events_log.event_log_id = '!' + record.event_log_id;
				//				events_log.event_id = 38;
				result = events_log.search();
			}
			//se non ne trovo nessuno
			if (result == 0) {
				throw new Error("- Per poter inserire ore viaggio nel giorno " + record.event_log_date.toLocaleDateString() + " inserire prima le ore di lavoro straordinario / supplementare");
			}
		}

		//ore viaggio in giorni feriali non patrono dell'utente, per i lavoratori turnisti
		if (record.events_log_to_users.is_shift_worker == 1 && record.event_id == 8 && record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
			/** @type {JSFoundSet<db:/geco/events_log>} */
			var events_log_ord = databaseManager.getFoundSet('geco', 'events_log');
			var result_ord = 0;
			// cerco eventi di tipo ORDINARIO
			if (events_log_ord.find()) {
				events_log_ord.event_log_date = record.event_log_date;
				events_log_ord.user_id = record.user_id;
				events_log_ord.event_log_id = '!' + record.event_log_id;
				events_log_ord.event_id = 0;
				result_ord = events_log_ord.search();
			}
			//se non ne trovo nessuno
			if (result_ord == 0 || events_log_ord.total_duration_regular < objTimeUser.workingHours * 60) {
				throw new Error("- Per poter inserire ore viaggio nel giorno " + record.event_log_date.toLocaleDateString() + " inserire prima le ore di lavoro ordinario");
			}
		}
	}

}

/**
 * Valida se commessa è abilitata per la consuntivazione
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"B2BB87A6-F115-46D5-A946-D9F4324E0136"}
 * @AllowToRunInFind
 */
function validateJobOrderEnabled(record) {
	if (record.events_log_to_events && record.events_log_to_events.has_job_order && record.events_log_to_events.has_job_order == 1 && record.events_log_to_job_orders && (record.events_log_to_job_orders.is_enabled == 0 || record.events_log_to_job_orders.is_loggable == 0)) {
		throw new Error('- Nel giorno ' + record.event_log_date.toLocaleDateString() + ' non è possibile inserire eventi per una commessa disabilitata.');
	}
//	
//	if (record.event_log_date< record.events_log_to_job_orders.date_approver_from){
//		throw new Error('- Nel giorno ' + record.event_log_date.toLocaleDateString() + ' non è possibile inserire eventi precedenti alla data di apertura della commessa.');
//	}
}

/**
 * Valida inserimento/modifica evento di tipo riposo compensativo straordinario in presenza di eventi di tipo Lavoro straordinario con riposo compensativo
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"C5BC4ABC-4A8D-44D0-B766-9A5D77CAA911"}
 * @AllowToRunInFind
 */
function validateAdditionalChargeOvertime(record) {

	//verifico che strao con riposo compensativo non sia inserito in giorni feriali o di sabato
	if (record.events_log_to_events && record.events_log_to_events.event_id && record.events_log_to_events.event_id == 28) {
		if (record.events_log_to_calendar_days.calendar_weekday != 0 && record.events_log_to_calendar_days.is_holiday != 1 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
			throw new Error('- Nel giorno ' + record.event_log_date.toLocaleDateString() + ' non è possibile inserire eventi di tipo lavoro straordinario con riposo compensativo');
		}
	}

	//verifico che si possa inserire recupero strao con riposo compensativo
	if (record.events_log_to_events && record.events_log_to_events.event_id && record.events_log_to_events.event_id == 25) {
		//data del record che si sta inserendo
		var dateRecToInsert = new Date(record.event_year, record.event_month, 0);
		var dayRecToInsert = '' + (dateRecToInsert.getDate());
		var monthRecToInsert = '' + (dateRecToInsert.getMonth() + 1);
		var dateRec = dateRecToInsert.getFullYear() + '-' + (monthRecToInsert.length > 1 ? monthRecToInsert : '0' + monthRecToInsert) + '-' + (dayRecToInsert.length > 1 ? dayRecToInsert : '0' + dayRecToInsert);
		application.output('dateRecToInsert: ' + dateRec);

		//data massima su cui posso tornare indietro nei mesi: 2
		var elapsedMaxDate = new Date(record.event_year, record.event_month - scopes.globals.elapsedMaxMonth, 0);
		var dayEMD = '' + (elapsedMaxDate.getDate());
		var monthEMD = '' + (elapsedMaxDate.getMonth() + 1);
		var dateEMD = elapsedMaxDate.getFullYear() + '-' + (monthEMD.length > 1 ? monthEMD : '0' + monthEMD) + '-' + (dayEMD.length > 1 ? dayEMD : '0' + dayEMD);
		application.output('dateElapsedMaxDate: ' + dateEMD);

		/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
		var events_log_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');
		//var result = 0;

		//cerco in tabella per is_expired a 0, in range di 3 mesi da data attuale, voci inserite per quell'utente
		if (events_log_additional_charge.find()) {
			events_log_additional_charge.is_expired = 0;
			events_log_additional_charge.el_competence_month = ' ' + dateEMD + '...' + dateRec + ' |yyyy-MM-dd';
			events_log_additional_charge.user_id = record.user_id;

			events_log_additional_charge.search();
		}
		events_log_additional_charge.sort('el_year asc, el_month asc');
		application.output('hours Remained: ' + events_log_additional_charge.hoursRemained);
		//se la trovo, controllo che l'aggregato delle ore rimanenti per utente e per mese non ancora terminato sia maggiore di 0
		if (events_log_additional_charge.hoursRemained != null && events_log_additional_charge.hoursRemained > 0) {
			//mi calcolo la durata dell'evento che sta per essere inserita e verifico che sia inferiore o uguale alle ore rimanenti
			var duration = 0;
			var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
			var break_start = objTimeUser.breakStart;
			var break_stop = objTimeUser.breakStop;
			if (record.time_start <= break_start && record.time_stop >= break_stop) {
				duration = (record.time_stop - record.time_start) - (break_stop - break_start);
			} else if (record.time_start <= break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
				duration = break_start - record.time_start;
			} else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop >= break_stop) {
				duration = record.time_stop - break_stop;
			} else {
				duration = record.time_stop - record.time_start;
			}

			//			if (record.time_start <= break_start && record.time_stop >= break_stop) {
			//				duration = (record.time_stop - record.time_start) - (break_stop - break_start);
			//			} else {
			//				duration = record.time_stop - record.time_start;
			//			}

			//se sto inserendo più ore di quelle che ho, esco subito
			//			if (duration > events_log_additional_charge.hoursRemained) {
			application.output('rimanenti ' + events_log_additional_charge.hoursRemained)
			application.output('rigettate ' + events_log_additional_charge.hoursRejectedWork)
			application.output('inseite ' + record.duration)
			if (duration > events_log_additional_charge.hoursRemained - events_log_additional_charge.hoursRejectedWork) {
				throw new Error('- Non è possibile inserire eventi di tipo riposo compensativo straordinario: le ore da recuperare eccedono quelle fatte');
			}
			//			for (var index = 1; index <= result; index++) {
			//				var recResult = events_log_additional_charge.getRecord(index);
			//				var duration = 0;
			//				var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
			//				var break_start = objTimeUser.breakStart;
			//				var break_stop = objTimeUser.breakStop;
			//
			//				/** @type {Number} */
			//				var timeStart = 0;
			//				/** @type {Number} */
			//				var timeStartOld = null;
			//				/** @type {Number} */
			//				var timeStartNew = 0;
			//				/** @type {Number} */
			//				var timeStop = 0;
			//				/** @type {Number} */
			//				var timeStopOld = null;
			//				/** @type {Number} */
			//				var timeStopNew = 0;
			//				/** @type {JSDataSet} */
			//				var dsUpdate = record.getChangedData();
			//				for (var i = 1; i <= dsUpdate.getMaxRowIndex(); i++) {
			//					if (dsUpdate.getValue(i, 1) == 'time_start') {
			//						application.output('modificato time_start:\n' + dsUpdate.getValue(i, 1) + ' ' + dsUpdate.getValue(i, 2) + ' ' + dsUpdate.getValue(i, 3));
			//						timeStartOld = dsUpdate.getValue(i, 2);
			//						timeStartNew = dsUpdate.getValue(i, 3);
			//						timeStart = dsUpdate.getValue(i, 3) - dsUpdate.getValue(i, 2);
			//					}
			//
			//					if (dsUpdate.getValue(i, 1) == 'time_stop') {
			//						application.output('modificato time_stop:\n' + dsUpdate.getValue(i, 1) + ' ' + dsUpdate.getValue(i, 2) + ' ' + dsUpdate.getValue(i, 3));
			//						timeStopOld = dsUpdate.getValue(i, 2);
			//						timeStopNew = dsUpdate.getValue(i, 3);
			//						timeStop = dsUpdate.getValue(i, 3) - dsUpdate.getValue(i, 2);
			//					}
			//				}
			//				application.output('TimeStart, Old, New: ' + timeStart + ', ' + timeStartOld + ', ' + timeStartNew);
			//				application.output('TimeStop, Old, New: ' + timeStop + ', ' + timeStopOld + ', ' + timeStopNew);
			//
			//				if (record.time_start <= break_start && record.time_stop >= break_stop) {
			//					duration = (record.time_stop - record.time_start) - (break_stop - break_start);
			//				} else {
			//					duration = record.time_stop - record.time_start;
			//				}
			//				//TODO finire di sistemare
			//				//la durata delle ore da inserire è inferiore alle ore lavorate:
			//				if (duration < recResult.duration_worked) {
			//					if (recResult.duration_retrieved > 0) {
			//						//VUOL DIRE CHE C'E' STATA UNA MODIFICA SU RECORD GIA' INSERITO:
			//						//QUINDI DEVO AGIRE; ALTRIMENTI PRENDO E INSERISCO NUOVO VALORE
			//						if ( (timeStartOld != null && timeStartOld != timeStartNew) || (timeStopOld != null && timeStopOld != timeStopNew)) {
			////							throw new Error('- Non è possibile modificare l\'evento riposo compensativo straordinario.\nCancellare e inserire nuovamente.');
			//						}//SE NON E' STATO MODIFICATO, QUINDI NUOVO INSERIMENTO
			//						else {
			//							if (duration + recResult.duration_retrieved > recResult.duration_worked) {
			//								throw new Error('- Non è possibile inserire eventi di tipo riposo compensativo straordinario: le ore da recuperare eccedono quelle fatte');
			//							}
			//						}
			//					}//se minore o uguale a 0 va bene perchè la durata è comunque inferiore alle ore lavorate, perciò lascio inserire
			//				}//durata ore inserite supera ore lavorate, blocco subito
			//				else {
			//					throw new Error('- Non è possibile inserire eventi di tipo riposo compensativo straordinario: le ore da recuperare eccedono quelle fatte');
			//				}
			//			}
		}//se non ne trovo nessuno
		else {
			throw new Error('- Nel giorno ' + record.event_log_date.toLocaleDateString() + ' non è possibile inserire eventi di tipo riposo compensativo straordinario in assenza di ore da recuperare');
		}

	}
}

/**
 * controlla che non vengano MODIFICATI straordinari per nessun motivo; da testare
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"D7F1152D-9D00-488C-BC3E-C970695C3393"}
 */
function validateUpdateOvertime(record) {
	if (record.events_log_to_events.event_id == 27 || record.events_log_to_events.event_id == 28 || record.events_log_to_events.event_id == 38) {
		throw new Error('- Non è possibile modificare eventi di tipo straordinario; cancellarli e re-inserirli');
	}
}

/**
 * JStaffa da fare
 * Valida inserimento di più eventi tipo trasferta nel giorno
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"059FB8B7-86AA-480B-9580-4CCEDBB575B5"}
 */
function validateTravelsExpenses(record) {
	if (record && record.events_log_to_events && record.events_log_to_events.events_to_event_types && record.events_log_to_events.events_to_event_types.event_type_id && record.events_log_to_events.events_to_event_types.event_type_id == 7) {
		/** @type {String} */
		var extFepCode = record.events_log_to_events.external_code_fep.toString();
		var strFepCode = extFepCode.slice(1, 3);
		application.output('strFepCode: ' + strFepCode);

		/*Formato reperibilità:
		 * R+CODICE(2 Lettere)+VALORE
		 * Esempio: RLV10 --> Reperibilità Lunedì-Venerdì valore 10
		 *
		 * Legenda stringhe:
		 * LV = Lunedì-Venerdì
		 * SB = Sabato
		 * DM = Domenica
		 * SD = Sabato-Domenica-Festivo
		 * FS = Festivo (solo FESTIVI, può essere su qualsiasi giorno FESTIVO)
		 * FL = Flat (qualsiasi giorno)
		 *
		 * NB: decidere se tenere SD o FS, dal momento che ci sono casi per i gettoni SB e DM
		 */

		switch (strFepCode) {
		case 'LV':
			//FERIALE (LUN-VEN): Controllo che non sia nè festivo nè sabato
			if ( (record.event_log_date.getDay() == 0 || record.event_log_date.getDay() == 6 || record.events_log_to_calendar_days.is_working_day == 0 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day))) {
				throw new Error('- Non è possibile inserire un evento di tipo ' + record.events_log_to_events.event_name + ' per il giorno ' + record.event_log_date.toLocaleDateString());
			}
			break;

		case 'SB':
			//SABATO: Controllo che non sia festivo nè un giorno tra 0 e 5 (lunedì-domenica)
			if ( (record.event_log_date.getDay() != 6 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day))) {
				throw new Error('- Non è possibile inserire un evento di tipo ' + record.events_log_to_events.event_name + ' per il giorno ' + record.event_log_date.toLocaleDateString());
			}
			break;

		case 'DM':
			//DOMENICA: Controllo che non sia festivo nè un giorno tra 1 e 6 (lunedì-sabato)
			if ( (record.event_log_date.getDay() != 0 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day))) {
				throw new Error('- Non è possibile inserire un evento di tipo ' + record.events_log_to_events.event_name + ' per il giorno ' + record.event_log_date.toLocaleDateString());
			}
			break;

		case 'SD':
			//FESTIVO (SAB, DOM, PATRONO, IS_HOLIDAY): Controllo che sia festivo o sabato
			if (record.event_log_date.getDay() != 0 && record.event_log_date.getDay() != 6 && record.events_log_to_calendar_days.is_holiday != 1 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
				throw new Error('- Non è possibile inserire un evento di tipo ' + record.events_log_to_events.event_name + ' per il giorno ' + record.event_log_date.toLocaleDateString());
			}
			break;

		case 'FS':
			//FESTIVO (PATRONO, IS_HOLIDAY): Controllo che sia festivo
			if (record.events_log_to_calendar_days.is_holiday != 1 && !globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
				throw new Error('- Non è possibile inserire un evento di tipo ' + record.events_log_to_events.event_name + ' per il giorno ' + record.event_log_date.toLocaleDateString());
			}
			break;

		case 'FL':
			break;

		default:
			throw new Error('- Errore: codice FEP non trovato. Contattare l\'helpdesk.');
			break;
		}

		/** @type {JSFoundSet<db:/geco/events_log>} */
		var events_log = databaseManager.getFoundSet('geco', 'events_log');

		//cerco se ci sono già eventi dello stesso tipo, per quell'utente, nello stesso giorno
		if (events_log.find()) {
			events_log.event_log_id = '!' + record.event_log_id;
			events_log.events_log_to_events.event_type_id = 7;
			events_log.user_id = record.user_id;
			events_log.event_log_date = record.event_log_date;
			var count = events_log.search();
			if (count > 0) {
				throw new Error('- Esiste già un evento di tipo Reperibilità Gettone per il giorno ' + record.event_log_date.toLocaleDateString());
			}
		}
	}
}

/**
 * Calcola la durata dell'evneto
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"85EEA1F7-7DCF-4BFE-A881-C6DAC7EDFBE6"}
 */
function processDuration(record) {
	//	var break_start = globals.getUserBreakStart(record.user_id, record.event_log_date);
	//	var break_stop = globals.getUserBreakStop(record.user_id, record.event_log_date);
	var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
	application.output('trovato contratto ' + objTimeUser.contractId + ' ' + objTimeUser.startTime + ' ' + objTimeUser.stopTime + ' ' + objTimeUser.breakStart + ' ' + objTimeUser.breakStop + ' ' + objTimeUser.workingHours);
	var break_start = objTimeUser.breakStart;
	var break_stop = objTimeUser.breakStop;

	//valorizzo i dati di inizio e fine pausa
	record.break_start = break_start;
	record.break_stop = break_stop;
	//evento misurato in ore
	if (record.events_log_to_events.unit_measure == 2) {
		//diverso da straordinari
		//TODO eliminare il controllo sugli straordinari la pausa pranzo viene sempre eliminata
		//if (record.events_log_to_events.event_id != 27 && record.events_log_to_events.event_id != 37) {
		if (record.time_start <= break_start && record.time_stop >= break_stop) {
			record.duration = (record.time_stop - record.time_start) - (break_stop - break_start);
		} else if (record.time_start <= break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
			record.duration = break_start - record.time_start;
		} else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop >= break_stop) {
			record.duration = record.time_stop - break_stop;
		} else {
			record.duration = record.time_stop - record.time_start;
		}
		//	}
		//se straordinari
		//	else record.duration = record.time_stop - record.time_start;

		if (record.duration == null) throw new Error("- Impossibile calcolare la durata dell'evento");
	} else {
		record.time_start = null;
		record.time_stop = null;
		record.break_start = null;
		record.break_stop = null;
		record.duration = 0;
	}

}

/**
 * Controlla che sia presente il codice FEP
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"BAE39D06-DD0D-4846-ACB3-D1B4634C94F8"}
 */
function processEventExternalFepCode(record) {
	//if (!globals.isEmpty(record.event_id)) record.external_code_fep = record.events_log_to_events.external_code_fep;
	if (globals.isEmpty(record.external_code_fep)) throw new Error('- Il campo codice FEP non può essere vuoto');
}

/**
 * Controlla che sia presente il codice navision della commessa
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"2A13EAA7-F8C6-4623-BD41-7B68850F39D4"}
 */
function processJobOrderExternalNavisionCode(record) {
	if (record.events_log_to_events.has_job_order == 1) {
		record.external_code_job_order = record.events_log_to_job_orders.external_code_navision;
		if (globals.isEmpty(record.external_code_job_order)) throw new Error('- Il campo codice Navision della commessa non può essere vuoto per il tipo di evento scelto');
	}
	if (record.events_log_to_events.has_job_order == 0) {
		record.external_code_navision = record.events_log_to_events.external_code_navision;
		record.external_code_job_order = null;
	}
}

/**
 * Processa la cancellazione del recupero strao riposo compensativo (event_id = 25)
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"9163E13F-4464-4A00-BE71-517FC764B3B2"}
 * @AllowToRunInFind
 */
function processDeletionCompensatoryRest(record) {

	if (record.events_log_to_events && record.events_log_to_events.event_id && record.events_log_to_events.event_id == 25) {
		/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
		var events_log_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');
//		//FS
//		//find su additional_charge per trovare,se c'è, il record corretto legato al record da eliminare
//		if (events_log_additional_charge.find()) {
//
//			events_log_additional_charge.user_id = record.user_id;
//			events_log_additional_charge.el_month = record.event_month;
//			events_log_additional_charge.el_year = record.event_year;
//			var findResult = events_log_additional_charge.search();
//		}
		
		//if (findResult > 0) {
			//FS
			//sorting del foundset filtrato
//			events_log_additional_charge.sort('el_year desc, el_month desc');
//			for (var index = 1; index <= events_log_additional_charge.getSize(); index++) {
//				var additionalChargeRecord = events_log_additional_charge.getRecord(index);
				if (!globals.hasRole('Controllers') && !globals.hasRole('HR Administrators')) {
					if (record.event_log_status != 4 && record.event_log_status != 2) {
						//FS
						events_log_additional_charge.deleteAdditionalCharge(record, 2);
					} else throw new Error('- Non è possibile cancellare eventi approvati o chiusi');
				} else {
					events_log_additional_charge.deleteAdditionalCharge(record, 2);
				}
			//}
		//}
	}
}

/**
 * Calcola l'approvatore dell'evento
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"3F71A78C-1B5E-4B07-BA83-A548A9B645F5"}
 * @AllowToRunInFind
 */
function processApprover(record) {
	application.output('Data ' + record.event_log_date);
	// don't check for the approver when approving or rejecting or not holiday type
	//	if (record.events_log_to_events.events_to_event_types.event_type_id != 6 && record.event_log_status > 1) return

	var eventApprover = null;

	/** @type {JSFoundSet<db:/geco/approvers>} */
	var approvers = databaseManager.getFoundSet('geco', 'approvers')

	// find out who's the approver figure
	switch (record.events_log_to_events.approver_figure) {
	case 1: // A Profit Center
		eventApprover = approvers.getApprover('profit_centers',
			record.events_log_to_events.events_to_profit_centers_approver.profit_center_id,
			record.event_log_date
		);
		application.output('A Profit Center ' + eventApprover);
		break;

	case 2: // User's boss
		//cerco il profit center di appartenenza in base alla data dell'evento che sta inserendo o modificando
		var profCenterId = record.events_log_to_users.users_to_profit_centers.profit_center_id;
		/** @type {JSFoundSet<db:/geco/users_profit_centers_history>} */
		var users_prof_cent_history = databaseManager.getFoundSet('geco', 'users_profit_centers_history');

		//cerco lo storico dei profit_center dell'utente
		if (users_prof_cent_history.find()) {
			users_prof_cent_history.user_id = record.user_id;
			if (users_prof_cent_history.search() > 0) {
				users_prof_cent_history.sort('start_change_date desc, created_at desc');
				for (var index = 1; index <= users_prof_cent_history.getSize(); index++) {
					var rec = users_prof_cent_history.getRecord(index);
					if (record.event_log_date >= rec.start_change_date) {
						profCenterId = rec.actual_prof_center_id;
						break;
					} else {
						profCenterId = rec.previous_prof_center_id;
					}
				}

			}
		}

		eventApprover = approvers.getApprover('profit_centers',
			profCenterId,
			record.event_log_date
		);
		application.output('User\'s boss ' + eventApprover);
		break;

	case 3: // Job Order (RDC)
		eventApprover = approvers.getApprover('job_orders',
			record.job_order_id,
			record.event_log_date
		);
		if (eventApprover == record.user_id) {
			//utente loggato è approvatore devo cercare il RDC
			eventApprover = record.events_log_to_job_orders.user_owner_id;
			if (eventApprover == record.user_id) {
				//utente loggato è RDC salgo al responsabile del PC
				eventApprover = record.events_log_to_job_orders.job_orders_to_profit_centers.user_owner_id;
				if (eventApprover == record.user_id) {
					//utente loggato è responsabile del PC della commessa risalgo al responsabile dell'utente
					eventApprover = record.events_log_to_users.users_to_profit_centers.user_owner_id;
				}
			}
		}
		application.output('RDC ' + eventApprover);
		break;
	}

	// set the approver
	if (eventApprover) {
		record.user_approver_id = eventApprover;
		// update related record calculation (calendar status)
		databaseManager.recalculate(record.events_log_to_calendar_days);
	} else {
		throw new Error("- Impossibile assegnare l'approvatore dell'evento");
	}

}

/**
 * Modifica lo stato dell'evento respinto in caso di modifica
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"DFC30838-2A8A-49D5-B51F-6F00E164F19D"}
 */
function processResubmission(record) {

	var changedData = record.getChangedData();

	// exit if approving or rejecting
	for (var index = 1; index <= changedData.getMaxRowIndex(); index++) {
		if (changedData.getValue(index, 1) == 'event_log_status') return;
	}

	if (record.event_log_status == 3) {
		record.event_log_status = 1;
		record.note = null;
	}
}

/**
 * Cancello gli overtime dalla tabella e li reinserisco per calcolare nuovamente le fasce di pagamento
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"FC21C950-E8CE-4DBD-937E-23B9A1721A4C"}
 * @AllowToRunInFind
 */
function processDeleteOvertime(record) {
	application.output('START processDeleteOvertime - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
	/** @type {JSFoundSet<db:/geco/events_log_overtime>} */
	var events_log_overtime = databaseManager.getFoundSet('geco', 'events_log_overtime');

	/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
	var events_log_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');

	/** @type {JSFoundSet<db:/geco/events_log>} */
	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	var tot = 0;
	//Se ritorna -1 non ha trovato nulla la find

	//cerco tutti i record che combaciano sia con l'anno che con il mese e che non sia già stato rifiutato in precedenza
//	if (events_log_additional_charge.find()) {
//		events_log_additional_charge.user_id = record.user_id;
//		events_log_additional_charge.el_month = record.event_month;
//		events_log_additional_charge.el_year = record.event_year;
//		var findResult = events_log_additional_charge.search();
//	}
	//if (findResult > 0) { 
		//for (var index = 1; index <= events_log_additional_charge.getSize(); index++) {
		//	var recordToUpdate = events_log_additional_charge.getRecord(index);
			if (!globals.hasRole('Controllers') && !globals.hasRole('HR Administrators')) {
				if (record.event_log_status != 4 && record.event_log_status != 2) {
					events_log_overtime.deleteOvertime(record);
					if (record.events_log_to_events.event_id == 28) {
						events_log_additional_charge.deleteAdditionalCharge(record, 1);
					}
				} else throw new Error('- Non è possibile cancellare eventi approvati o chiusi');
			} else {
				events_log_overtime.deleteOvertime(record);
				if ( (record.events_log_to_events.event_id == 28)) {
					events_log_additional_charge.deleteAdditionalCharge(record, 1);
				}
			}
		//}
	//}
	try {
		if (events_log.find()) {
			events_log.user_id = record.user_id;
			events_log.event_log_date = record.event_log_date;
			events_log.event_id = [27, 28, 38]; //lavoro straordinario o intervento straordinario di reperibilità
			events_log.event_log_id = '!' + record.event_log_id;
			tot = events_log.search();
			for (var i = 1; i <= tot; i++) {
				var toDeleteOvertime = events_log.getRecord(i);
				// cancellazione di tutti gli events_overtime_log
				events_log_overtime.deleteOvertime(toDeleteOvertime);
			}
			for (var j = 1; j <= tot; j++) {
				var toReinsertOvertime = events_log.getRecord(j);
				// cancellazione di tutti gli events_overtime_log
				processOvertime(toReinsertOvertime);
				processOvertimeWithCompensatoryRest(toReinsertOvertime);

				//questo serve per risolvere problema di doppi record nella tabella di appoggio
				if (toReinsertOvertime.event_id == 28)
					events_log_additional_charge.deleteAdditionalCharge(toReinsertOvertime,1);
			}
		}
	} catch (e) {
		throw new Error('- Errore nella cancellazione degli straordinari');
	}
	application.output('STOP processDeleteOvertime - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
}

/**
 * Processa gli straordinari in base alle fasce orario
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"F27BA953-F659-4710-8FEE-51B60E393D0E"}
 */
function processOvertime(record) {
	application.output('START processOvertime - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
	application.output('start: ' + record.time_start / 60 + '  stop ' + record.time_stop / 60, LOGGINGLEVEL.DEBUG);
	//00:00 [0] - 06:00 [360]- 09:00 [540]
	//18:00 [1080] - 22:00 [1320] 24:00 [1440]
	/** @type {JSFoundSet<db:/geco/events_log_overtime>} */
	var events_log_overtime = databaseManager.getFoundSet('geco', 'events_log_overtime');
	/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
	//var events_log_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');
	var middle_time = 0;
	var durationForS25 = 0;
	var durationTot = 0;
	var durationForSR10 = 0;
	var durationForSR35 = 0;
	var durationForS55 = 0;
	var durationForS60 = 0;
	var durationForS75 = 0;
	var dur75current = 0;
	var toCheck = false;
	var durationForcheck = 0

	var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
	//application.output('trovato contratto ' + objTimeUser.contractId + ' ' + objTimeUser.startTime + ' ' + objTimeUser.stopTime + ' ' + objTimeUser.breakStart + ' ' + objTimeUser.breakStop + ' ' + objTimeUser.workingHours);
	var break_start = objTimeUser.breakStart;
	var break_stop = objTimeUser.breakStop;
	var durationBreak = 0;
	//if (!record.isNew()) return;
	try {
		// se dipendente e  se straordinario o intervento straordinario in reperibilità
		if (record.events_log_to_users.user_type_id == 7 && (record.events_log_to_events.event_id == 27 || record.events_log_to_events.event_id == 38)) {
			//START sunday - holiday - head office user holiday
			if (record.events_log_to_calendar_days.calendar_weekday == 0 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
				//durata già inserita in tabella straordinari
				durationTot = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, null, record.event_log_id);
				middle_time = 0;
				durationForSR10 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 7, record.event_log_id);
				durationForSR35 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 8, record.event_log_id);
				durationForS55 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 4, record.event_log_id);
				durationForS60 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 5, record.event_log_id);
				durationForS75 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 6, record.event_log_id);
				application.output('durationTOT: ' + durationTot / 60);
				application.output('durationForS55: ' + durationForS55 / 60);
				application.output('durationForS60: ' + durationForS60 / 60);
				application.output('durationForS75: ' + durationForS75 / 60);
				application.output('durationForSR10: ' + durationForSR10 / 60);
				application.output('durationForSR35: ' + durationForSR35 / 60);

				//inizia prima delle 6
				if (record.time_start < 360) {
					// finisce dopo le 22
					if (record.time_stop > 1320) {
						// ha fatto più di 8 ore --> ore tra le 0 e le 6 e  ore tra le 22 e le 24 a S75 quelle tra le 6 e le 22  a s55
						events_log_overtime.insertOvertime(6, record, record.time_start, 360); //fino alle 6
						//escludere la pausa pranzo tra le 9 e le 18 dividere in più record
						events_log_overtime.insertOvertime(4, record, 360, break_start);
						events_log_overtime.insertOvertime(4, record, break_stop, 1320);
						events_log_overtime.insertOvertime(6, record, 1320, record.time_stop); // dopo le 22
						//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore
						dur75current = (360 - record.time_start) + (record.time_stop - 1320);
						toCheck = true;
						durationForcheck = record.duration - dur75current;
					}
					// inizia prima delle 6 finisce prima delle 6
					else if (record.time_stop <= 360) {
						//se durationTot > 8 tutto a S75
						if (durationTot >= 480) events_log_overtime.insertOvertime(6, record, record.time_start, record.time_stop);
						//se durata presente + durata evento non supera le 8 ore tutto in S60
						else if (durationTot + record.duration <= 480) events_log_overtime.insertOvertime(5, record, record.time_start, record.time_stop);
						//se durata presente + durata evento supera le 8h fino ad esautimento 8 ore in fascia S60 oltre in S75
						else if (durationTot + record.duration > 480) {
							middle_time = record.time_start + (record.duration + durationTot - 480);
							if (middle_time < 360) {
								events_log_overtime.insertOvertime(6, record, record.time_start, middle_time);
								events_log_overtime.insertOvertime(5, record, middle_time, record.time_stop);
								//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore (dur75current = middle_time - record.time_start)
								dur75current = middle_time - record.time_start
							} else if (middle_time >= 360) {
								events_log_overtime.insertOvertime(6, record, record.time_start, record.time_stop);
								//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore (dur75current = 360 - record.time_start)
								dur75current = record.time_stop - record.time_start
							}
							//DF aggiunta durata da controllare e flag
							toCheck = true;
							durationForcheck = record.duration - dur75current;
						}

					}// inizia prima delle 6 finisce dopo le 6 e prima delle 22
					else if (record.time_stop <= 1320) {
						// break
						durationBreak = 0;
						if (record.time_stop >= break_stop) {
							durationBreak = break_stop - break_start;
						} else if (record.time_stop > break_start && record.time_stop <= break_stop) {
							durationBreak = record.time_stop - break_start;
						}
						//if (durationTot + (record.time_stop - 360 - durationBreak) >= 480) {

						//ore fatte tra le 0 e le 6
						//se durata presente >8h le ore tra le 0 e le 6 vanno in S75
						if (durationTot >= 480) {
							events_log_overtime.insertOvertime(6, record, record.time_start, 360);
							dur75current = 360 - record.time_start
							toCheck = true;
							durationForcheck = record.duration - dur75current;
						}
						//se durata presente + durata evento non supera le 8 ore tutto in S60
						else if (durationTot + record.duration <= 480) {
							events_log_overtime.insertOvertime(5, record, record.time_start, 360);
						}
						// se durata + record > 8h fino a esaurimento prime 8h in S60 oltre in S75
						else if (durationTot + (record.duration) > 480) {
							middle_time = record.time_stop + durationTot - 480;
							//events_log_overtime.insertOvertime(6, record, record.time_start, 360);
							application.output('fine record a 75% ' + (middle_time / 60));
							if (middle_time < 360) {
								events_log_overtime.insertOvertime(6, record, record.time_start, middle_time);
								events_log_overtime.insertOvertime(5, record, middle_time, 360);
								//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore (dur75current = middle_time - record.time_start)
								dur75current = middle_time - record.time_start
							} else if (middle_time >= 360) {
								events_log_overtime.insertOvertime(6, record, record.time_start, 360);
								//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore (dur75current = 360 - record.time_start)
								dur75current = 360 - record.time_start
							}
							//else events_log_overtime.insertOvertime(6, record, record.time_start, 360);
							//DF aggiunta durata da controllare e flag
							toCheck = true;
							durationForcheck = record.duration - dur75current;
						}

						//ore tra le 6 e le 22
						if (record.time_stop <= break_start) {
							events_log_overtime.insertOvertime(4, record, 360, record.time_stop);
						}
						//finisce a cavallo della pausa pranzo tolgo la pausa pranzo
						else if (record.time_stop > break_start && record.time_stop <= break_stop) {
							events_log_overtime.insertOvertime(4, record, 360, break_start);
						}
						//finsce dopo la pausa pranzo tolgo  la pausa pranzo
						else if (record.time_stop > break_stop) {
							events_log_overtime.insertOvertime(4, record, 360, break_start);
							events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
						}
					}
				}//inizia tra le >= 6 e le 22
				else if (record.time_start < 1320) {
					// se finisce prima delle 22
					if (record.time_stop <= 1320) {
						durationForcheck = record.duration;
						//inizia prima e finisce prima della pausa pranzo o inizia e finisce dopo lapausa pranzo
						if ( (record.time_start < break_start && record.time_stop <= break_start) || (record.time_start >= break_stop && record.time_stop > break_stop)) {
							events_log_overtime.insertOvertime(4, record, record.time_start, record.time_stop);
						}
						//inizia prima della pausa e finisce in mezzo all pausa pranzo
						else if (record.time_start < break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
							events_log_overtime.insertOvertime(4, record, record.time_start, break_start);
							//durationForcheck = record.duration - (record.time_stop - break_start);
						}
						//inizia uguale o prima della pausa e finisce uguale o dopo la pausa pranzo
						else if (record.time_start <= break_start && record.time_stop >= break_stop) {
							if (record.time_start < break_start)
								events_log_overtime.insertOvertime(4, record, record.time_start, break_start);
							if (record.time_stop > break_stop)
								events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
							//durationForcheck = record.duration - (break_stop - break_start);
						}
						//inizia in mezzo alla pausa pranzo e finisce dopo la pausa
						else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop > break_stop) {
							events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
							//durationForcheck = record.duration - (break_stop - record.time_start);
						}

						//durationForcheck = record.duration;
						toCheck = true;
					}
					// inizia dopo le 6 e finisce dopo le 22
					else if (record.time_stop > 1320) {

						//ore dopo le 22
						//durationTot + durata evento fra inizio e 22 > 8 dopo le 22 S75
						durationBreak = 0;
						if (record.time_start <= break_start)
							durationBreak = break_stop - break_start;
						else if (record.time_start >= break_start && record.time_start < break_stop)
							durationBreak = break_stop - record.time_start;

						//							if (durationTot + (1320 - record.time_start - durationBreak) >= 480) {
						//								events_log_overtime.insertOvertime(6, record, 1320, record.time_stop);
						//								toCheck = true;
						//								durationForcheck = record.duration - (record.time_stop - 1320);
						//
						//							} else events_log_overtime.insertOvertime(5, record, 1320, record.time_stop);

						//durata presente >=8 tutte le ore notturne finiscono in S75
						if (durationTot >= 480) {
							events_log_overtime.insertOvertime(6, record, 1320, record.time_stop);
						}
						//durata totale + durata record <= 8 le notturne in S60
						else if (durationTot + record.duration <= 480) events_log_overtime.insertOvertime(5, record, 1320, record.time_stop);
						//durata presente + durata del record >=8 tutte le ore notturne a esaurimento delle 8 ore vanno in S60 le altre in S75
						else if (durationTot + record.duration > 480) {
							middle_time = record.time_start + (480 - durationTot);
							if (middle_time > 1320) {
								events_log_overtime.insertOvertime(5, record, 1320, middle_time);
								events_log_overtime.insertOvertime(6, record, middle_time, record.time_stop);
								//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore (dur75current = record.time_stop-middle_time)
								dur75current = record.time_stop - middle_time
							} else if (middle_time <= 1320) {
								events_log_overtime.insertOvertime(6, record, 1320, record.time_stop);
								//DF durataForCheck è la durata del record - l'eccedenza delle 8 ore (dur75current = record.time_stop-1320)
								dur75current = record.time_stop - 1320
							}
							//else events_log_overtime.insertOvertime(6, record, 1320, record.time_stop);
						}
						//ore ta le 6 e le 22

						//inizia prima della pausa pranzo divido in 2 record
						if (record.time_start < break_start) {
							events_log_overtime.insertOvertime(4, record, record.time_start, break_start);
							events_log_overtime.insertOvertime(4, record, break_stop, 1320);
						}
						//se inizia dopo inizio pausa e prima di fine pausa e finisce dopo le 22
						else if (record.time_start >= break_start && record.time_start <= break_stop) {
							events_log_overtime.insertOvertime(4, record, break_stop, 1320);
						}
						//inizia dopo la pausa pranzo e finisce dopo le 22
						else if (record.time_start >= break_stop) {
							events_log_overtime.insertOvertime(4, record, record.time_start, 1320);
						}
						toCheck = true;
						//DF non si deve passare tutta la durata del record ma solo le ore che non sono in S75
						application.output('dur75current: ' + dur75current);
						durationForcheck = record.duration - dur75current;

					}
				}
				//inizia dopo le >=22
				else {
					// se durataTot > 8 tutte a S75
					if (durationTot >= 480)
						events_log_overtime.insertOvertime(6, record, record.time_start, record.time_stop);
					// evento + durationTot < 8 tutte a S60
					else if (durationTot + record.duration <= 480)
						events_log_overtime.insertOvertime(5, record, record.time_start, record.time_stop);
					// se durata evento + durataTot > 8 divido in S60 e S75
					else if (durationTot + record.duration > 480) {

						middle_time = record.time_start + (480 - durationTot);
						if (middle_time > 1320) {
							events_log_overtime.insertOvertime(5, record, record.time_start, middle_time);
							events_log_overtime.insertOvertime(6, record, middle_time, record.time_stop);
							//DF non si deve passare tutta la durata del record ma solo le ore che non sono in S75
							dur75current = record.time_stop - middle_time
							application.output('dur75current: ' + dur75current);
							durationForcheck = record.duration - dur75current;

						}
						if (middle_time <= 1320) events_log_overtime.insertOvertime(6, record, record.time_start, record.time_stop);
					}

					toCheck = true;
					//durationForcheck = record.duration;
				}
				if (toCheck) {
					application.output('durationForcheck ' + durationForcheck);
					events_log_overtime.checkOvertimeHoliday(record, record.user_id, record.event_log_date, durationForcheck);
					//FS check record SR10 da modificare eventualmente in SR35
					events_log_overtime.checkOvertimeHolidayRec(record, record.user_id, record.event_log_date, durationForcheck);
				}

				
			}//END sunday event_id = 27 - holiday - head hoffice user holiday
			//START workday - not holiday
			else if (record.events_log_to_calendar_days.is_working_day == 1 && record.events_log_to_calendar_days.is_holiday == 0 && record.events_log_to_events.event_id != 28) {
				/** @type {JSFoundSet<db:/geco/events_log>} */
				var events_log_ord = databaseManager.getFoundSet('geco', 'events_log');
				var ordStartStop = events_log_ord.getOrdinaryStartStop(record);
				var startOrd = ordStartStop.startOrd;
				var stopOrd = ordStartStop.stopOrd;
				durationForS25 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 1, record.event_log_id);
				//6-9 or 18-22
				//straordinario prima del turno
				//if (record.events_log_to_users.shift_start >= record.time_stop) {
				if (startOrd >= record.time_stop) {
					//inizia e finisce prima delle 6
					if (record.time_start < 360 && record.time_stop <= 360)
						events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
					else {
						var begin_time = 360;
						//inizia prima delle 6, quello prima delle 6 in S50
						if (record.time_start < 360) events_log_overtime.insertOvertime(3, record, record.time_start, begin_time);
						//finisce dopo le 6 ma prima dell'inizio del turno
						else //inizia dopo le 6
							begin_time = record.time_start;

						//esistono già 2 ore a s25
						if (durationForS25 >= 120) {
							events_log_overtime.insertOvertime(2, record, begin_time, record.time_stop);
						}
						//meno di 2 ore a S25
						else {
							if (durationForS25 > 0) middle_time = begin_time + (120 - durationForS25);
							if (middle_time == 0 && record.time_stop - begin_time <= 120)
								events_log_overtime.insertOvertime(1, record, begin_time, record.time_stop);
							else if (middle_time == 0 && record.time_stop - begin_time > 120) {
								events_log_overtime.insertOvertime(1, record, begin_time, begin_time + 120);
								events_log_overtime.insertOvertime(2, record, begin_time + 120, record.time_stop);
							} else if (middle_time != 0 && middle_time < record.time_stop) {
								events_log_overtime.insertOvertime(1, record, begin_time, middle_time);
								events_log_overtime.insertOvertime(2, record, middle_time, record.time_stop);
							} else if (middle_time >= record.time_stop) events_log_overtime.insertOvertime(1, record, begin_time, record.time_stop);
							else application.output('Errore nel calcolo degli straordinari prima del turno, nessuna condizione verificata');
						}
					}
				}
				//straordinario dopo il turno
				//if (record.events_log_to_users.shift_stop <= record.time_start) {
				if (stopOrd <= record.time_start) {
					//inizia prima delle 22
					if (record.time_start < 1320) {
						var end_time = 1320;
						//finisce dopo le 22
						if (record.time_stop > 1320) {
							//tutto quello dopo le 22 va  a S50
							events_log_overtime.insertOvertime(3, record, 1320, record.time_stop);
						}
						//finisce prima delle 22
						else end_time = record.time_stop;

						//tutto quello prima delle 22
						//esistono già 2 ore a s25 mette tutto a S30
						if (durationForS25 >= 120) {
							events_log_overtime.insertOvertime(2, record, record.time_start, end_time);
						}
						//meno di 2 ore già fatte a S25 divide tra S25 e S30
						else {
							//start + duration esistente
							if (durationForS25 > 0) {
								middle_time = record.time_start + (120 - durationForS25);
							}
							//se uguale a zero (non c'erano altre ore S25) e  durata minore di 2h 1 solo a S25
							if (middle_time == 0 && (end_time - record.time_start <= 120))
								events_log_overtime.insertOvertime(1, record, record.time_start, end_time);
							//se uguale a zero (non c'erano altre ore S25) e  durata maggiore di 2h divido
							else if (middle_time == 0 && (end_time - record.time_start > 120)) {
								events_log_overtime.insertOvertime(1, record, record.time_start, record.time_start + 120);
								events_log_overtime.insertOvertime(2, record, record.time_start + 120, end_time);
							}
							//se maggiore di zero (c'erano altre ore S25) e minore di fine evento divido
							else if (middle_time != 0 && middle_time < end_time) {
								events_log_overtime.insertOvertime(1, record, record.time_start, middle_time);
								events_log_overtime.insertOvertime(2, record, middle_time, end_time);
							}
							//uguale o maggiore di fine
							else if (middle_time >= end_time) events_log_overtime.insertOvertime(1, record, record.time_start, end_time);
							else application.output('Errore nel calcolo degli straordinari dopo il turno, nussuna condizione verificata');
						}
					}
					//inizia dopo le 22 tutto a S50
					else {
						events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
					}
				}
			}
			// END workday - not holiday

			//START saturday - not holiday
			else if (record.events_log_to_calendar_days.calendar_weekday == 6 && record.events_log_to_calendar_days.is_holiday == 0 && record.events_log_to_events.event_id != 28) {
				//durationForS25 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 1);
				durationTot = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, null, record.event_log_id);
				//non ci sono altri eventi
				if (durationTot == 0) {
					//duration > 2 hours --> all hours S50
					if (record.duration > 120) {
						if (record.time_start < break_start && record.time_stop <= break_start) {
							events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
						}
						//se inizia prima della pausa e finisce in mezzo all pausa pranzo
						else if (record.time_start <= break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
							if (record.time_start < break_start)
								events_log_overtime.insertOvertime(3, record, record.time_start, break_start);
							//events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
						}
						//se inizia prima della pausa e finisce dopo la pausa pranzo
						else if (record.time_start <= break_start && record.time_stop > break_stop) {
							if (record.time_start < break_start)
								events_log_overtime.insertOvertime(3, record, record.time_start, break_start);
							events_log_overtime.insertOvertime(3, record, break_stop, record.time_stop);
						}
						//inizia in mezzo alla pausa pranzo e finisce uguale o dopo la pausa
						else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop > break_stop) {
							events_log_overtime.insertOvertime(3, record, break_stop, record.time_stop);
						}
						//inizia dopo la pausa pranzo
						else if (record.time_start >= break_stop && record.time_stop > break_stop) {
							events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
						}
						//events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
					}
					//duration <= 2 hours S25
					else {
						if (record.time_start < break_start && record.time_stop <= break_start) {
							events_log_overtime.insertOvertime(1, record, record.time_start, record.time_stop);
						}
						//se inizia prima della pausa e finisce in mezzo all pausa pranzo
						else if (record.time_start <= break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
							if (record.time_start < break_start)
								events_log_overtime.insertOvertime(1, record, record.time_start, break_start);
							//events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
						}
						//se inizia prima della pausa e finisce dopo la pausa pranzo
						else if (record.time_start <= break_start && record.time_stop > break_stop) {
							if (record.time_start < break_start)
								events_log_overtime.insertOvertime(1, record, record.time_start, break_start);
							events_log_overtime.insertOvertime(1, record, break_stop, record.time_stop);
						}
						//inizia in mezzo alla pausa pranzo e finisce uguale o dopo la pausa
						else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop > break_stop) {
							events_log_overtime.insertOvertime(1, record, break_stop, record.time_stop);
						}
						//inizia dopo la pausa pranzo
						else if (record.time_start >= break_stop && record.time_stop > break_stop) {
							events_log_overtime.insertOvertime(1, record, record.time_start, record.time_stop);
						}
						//events_log_overtime.insertOvertime(1, record, record.time_start, record.time_stop);
					}
				}
				//more overtimes and sum(all durations) > 2hours --> all S50
				else if (durationTot + record.duration > 120) {
					if (record.time_start < break_start && record.time_stop <= break_start) {
						events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
					}
					//se inizia prima della pausa e finisce in mezzo all pausa pranzo
					else if (record.time_start <= break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
						if (record.time_start < break_start)
							events_log_overtime.insertOvertime(3, record, record.time_start, break_start);
					}
					//se inizia prima della pausa e finisce dopo la pausa pranzo
					else if (record.time_start <= break_start && record.time_stop > break_stop) {
						if (record.time_start < break_start)
							events_log_overtime.insertOvertime(3, record, record.time_start, break_start);
						events_log_overtime.insertOvertime(3, record, break_stop, record.time_stop);
					}
					//inizia in mezzo alla pausa pranzo e finisce uguale o dopo la pausa
					else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop > break_stop) {
						events_log_overtime.insertOvertime(3, record, break_stop, record.time_stop);
					}
					//inizia dopo la pausa pranzo
					else if (record.time_start >= break_stop && record.time_stop > break_stop) {
						events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
					}
					//events_log_overtime.insertOvertime(3, record, record.time_start, record.time_stop);
					events_log_overtime.updateOvertime(1, 3, record);
				}
				//ci sono altri eventi e sum(all durations) < 2hours
				else {
					if (record.time_start < break_start && record.time_stop <= break_start) {
						events_log_overtime.insertOvertime(1, record, record.time_start, record.time_stop);
					}
					//se inizia prima della pausa e finisce in mezzo all pausa pranzo
					else if (record.time_start <= break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
						if (record.time_start < break_start)
							events_log_overtime.insertOvertime(1, record, record.time_start, break_start);
					}
					//se inizia prima della pausa e finisce dopo la pausa pranzo
					else if (record.time_start <= break_start && record.time_stop > break_stop) {
						if (record.time_start < break_start)
							events_log_overtime.insertOvertime(1, record, record.time_start, break_start);
						events_log_overtime.insertOvertime(1, record, break_stop, record.time_stop);
					}
					//inizia in mezzo alla pausa pranzo e finisce uguale o dopo la pausa
					else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop > break_stop) {
						events_log_overtime.insertOvertime(1, record, break_stop, record.time_stop);
					}
					//inizia dopo la pausa pranzo
					else if (record.time_start >= break_stop && record.time_stop > break_stop) {
						events_log_overtime.insertOvertime(1, record, record.time_start, record.time_stop);
					}
					//events_log_overtime.insertOvertime(1, record, record.time_start, record.time_stop);
				}
			}
			//END saturday - not holiday
		}
	} catch (e) {
		throw new Error("- Errore inserimento straordinari: " + e);
	}
	application.output('STOP processOvertime - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
}

/**
 * Processa gli straordinari in base alle fasce orario
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @AllowToRunInFind
 * @properties={typeid:24,uuid:"00CC0A54-1E96-4424-891A-448EEB0B772C"}
 */
function processOvertimeWithCompensatoryRest(record) {
	application.output('START processOvertimeWithCompensatoryRest - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
	application.output('start: ' + record.time_start / 60 + ' stop ' + record.time_stop / 60, LOGGINGLEVEL.DEBUG);
	try {
		// se dipendente e  se straordinario con riposo compensantivo
		if (record.events_log_to_users.user_type_id == 7 && record.events_log_to_events.event_id == 28) {
			//00:00 [0] - 06:00 [360]- 09:00 [540]
			//18:00 [1080] - 22:00 [1320] 24:00 [1440]
			/** @type {JSFoundSet<db:/geco/events_log_overtime>} */
			var events_log_overtime = databaseManager.getFoundSet('geco', 'events_log_overtime');
			/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
			var events_log_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');
			var middle_time = 0;
			//var durationForS25 = 0;
			var durationTot = 0;
			var durationForSR10 = 0;
			var durationForSR35 = 0;
			var durationForSR35_N = 0;
			var durationForSR35_D = 0;
			var durationForS55 = 0;
			var durationForSR55 = 0;
			var sumMiddleDuration = 0;
			var durR55current = 0;

			var objTimeUser = globals.getUserTimeWorkByDay(record.user_id, record.event_log_date);
			//application.output('trovato contratto ' + objTimeUser.contractId + ' ' + objTimeUser.startTime + ' ' + objTimeUser.stopTime + ' ' + objTimeUser.breakStart + ' ' + objTimeUser.breakStop + ' ' + objTimeUser.workingHours);
			var break_start = objTimeUser.breakStart;
			var break_stop = objTimeUser.breakStop;
			var durationBreak = 0;

			//START sunday - holiday - head office user holiday
			if (record.events_log_to_calendar_days.calendar_weekday == 0 || record.events_log_to_calendar_days.is_holiday == 1 || globals.isCurrentUserHoliday(record.events_log_to_calendar_days.calendar_month, record.events_log_to_calendar_days.calendar_day)) {
				//durata già inserita in tabella straordinari
				durationTot = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, null, record.event_log_id);
				middle_time = 0;
				durationForSR10 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 7, record.event_log_id);
				durationForSR35 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 8, record.event_log_id);
				durationForSR35_N = events_log_overtime.getDurationOvertimesComp(record.user_id, record.event_log_date, 8, record.event_log_id, true);
				durationForSR35_D = events_log_overtime.getDurationOvertimesComp(record.user_id, record.event_log_date, 8, record.event_log_id, false);
				durationForS55 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 4, record.event_log_id);
				durationForSR55 = events_log_overtime.getDurationOvertimesByType(record.user_id, record.event_log_date, 9, record.event_log_id);
				//durata orario diurna
				sumMiddleDuration = durationForSR10 + durationForSR35_D + durationForS55;
				application.output('durationTOT: ' + durationTot / 60);
				application.output('durationForSR10: ' + durationForSR10 / 60);
				application.output('durationForSR35_D: ' + durationForSR35_D / 60);
				application.output('durationForSR35_N: ' + durationForSR35_N / 60);
				application.output('durationForS55: ' + durationForS55 / 60);
				application.output('durationForSR55: ' + durationForSR55 / 60);
				var toCheck = false;
				var durationForcheck = 0

				//--------------inizia prima delle 6
				if (record.time_start < 360) {
					//**************finisce dopo le 22
					if (record.time_stop > 1320) {
						// ha fatto più di 8 ore --> ore tra le 0 e le 6 e ore tra le 22 e le 24 a SR55
						events_log_overtime.insertOvertime(9, record, record.time_start, 360); //fino alle 6
						events_log_overtime.insertOvertime(9, record, 1320, record.time_stop); // dopo le 22
						durR55current = (360 - record.time_start) + (record.time_stop - 1320);
						durationBreak = 0;
						if (record.time_stop >= break_stop) {
							durationBreak = break_stop - break_start;
						} else if (record.time_stop > break_start && record.time_stop <= break_stop) {
							durationBreak = record.time_stop - break_start;
						}

						//le ore tra le 6 e le 22: le prime 8 a SR10, le altre a SR35
						middle_time = 360 + 480; //ho messo middle_time per non scrivere sempre 360+480
						//se prime 8 ore finiscono prima della pausa
						//se 8 ore finiscono in mezzo alla pausa
						//se 8 ore finiscono dopo la pausa
						if (middle_time <= break_start) {
							events_log_overtime.insertOvertime(7, record, 360, middle_time); //SR10 prime 8 dalle 6
							if (middle_time < break_start) events_log_overtime.insertOvertime(8, record, middle_time, break_start); //SR35 fino a prima della pausa
							events_log_overtime.insertOvertime(8, record, break_stop, 1320);//SR35 dalla pausa alle 22
						} else if (middle_time >= break_stop) {
							events_log_overtime.insertOvertime(7, record, 360, break_start); //SR10 prime 8 dalle 6
							if (middle_time == break_stop) {
								events_log_overtime.insertOvertime(7, record, break_stop, middle_time + durationBreak); //SR10 fine delle prime 8
								events_log_overtime.insertOvertime(8, record, middle_time + durationBreak, 1320);//SR35 restanti fino alle 22
							}
						} else {
							events_log_overtime.insertOvertime(7, record, 360, break_start); //SR10 prime 8 dalle 6
							events_log_overtime.insertOvertime(7, record, break_stop, middle_time + durationBreak);
							events_log_overtime.insertOvertime(8, record, middle_time + durationBreak, 1320)
						}

						//						if (middle_time <= break_start) { //prime 8 ore prima dell'inizio della pausa
						//							events_log_overtime.insertOvertime(7, record, 360, middle_time); //SR10 prime 8 dalle 6
						//							events_log_overtime.insertOvertime(8, record, middle_time + durationBreak, 1320); //rimanente a SR35
						//						}//8 ore maggiori dell'inizio pausa e: o minori uguali alla fine della pausa o maggiori della fine pausa - pausa in mezzo alle prime 8
						//						else if (middle_time > break_start && (middle_time <= break_stop || middle_time > break_stop)) {
						//							events_log_overtime.insertOvertime(7, record, 360, break_start); //SR10
						//							events_log_overtime.insertOvertime(7, record, break_stop, break_stop + (middle_time - break_start)); //SR10
						//							events_log_overtime.insertOvertime(8, record, break_stop + (middle_time - break_start), 1320); //SR35
						//						}

						toCheck = true;
						durationForcheck = record.duration - durR55current;
					}
					// inizia prima delle 6 finisce prima delle 6
					//*****************finisce prima delle 6
					else if (record.time_stop <= 360) {
						//se durationTot > 8 tutto a SR55
						if (durationTot >= 480) {
							events_log_overtime.insertOvertime(9, record, record.time_start, record.time_stop);
							toCheck = true;
							durationForcheck = record.duration;
						}
						//se durata presente + durata evento non supera le 8 ore tutto in SR35
						else if (durationTot + record.duration <= 480) events_log_overtime.insertOvertime(8, record, record.time_start, record.time_stop);
						//se durata presente + durata evento supera le 8h fino ad esaurimento 8 ore in fascia SR35 oltre in SR55
						else if (durationTot + record.duration > 480) {
							//middle_time = record.time_start + (480 - durationTot);
							middle_time = record.time_start + (record.duration + durationTot - 480);
							if (middle_time < 360) {
								events_log_overtime.insertOvertime(9, record, record.time_start, middle_time);
								events_log_overtime.insertOvertime(8, record, middle_time, record.time_stop);
								durR55current = middle_time - record.time_start;
							} else if (middle_time >= 360) {
								events_log_overtime.insertOvertime(9, record, record.time_start, record.time_stop);
								durR55current = record.duration;
							}
							toCheck = true;
							durationForcheck = record.duration - durR55current;
						}
					}
					// inizia prima delle <= 6 finisce dopo le 6 ma entro le 22
					//*****************finisce dopo le 6 e prima delle 22
					else if (record.time_stop <= 1320) {
						//ore fatte tra le 0 e le 6
						durationBreak = 0;
						if (record.time_stop >= break_stop) {
							durationBreak = break_stop - break_start;
						} else if (record.time_stop > break_start && record.time_stop <= break_stop) {
							durationBreak = record.time_stop - break_start;
						}

						//ore fatte tra le 0 e le 6
						//se durata presente > 8h le ore tra le 0 e le 6 vanno in SR55
						if (durationTot >= 480) {
							events_log_overtime.insertOvertime(9, record, record.time_start, 360);
							durR55current = (360 - record.time_start);
							toCheck = true;
						}
						//se durata presente + durata evento non supera le 8 ore tutto in SR35
						else if (durationTot + record.duration <= 480) {
							//fino alle 6
							events_log_overtime.insertOvertime(8, record, record.time_start, 360);
						}
						// se durata + record > 8h fino a esaurimento prime 8h in SR35 oltre in SR55
						else if (durationTot + record.duration > 480) {
							//TODO Verificare la storia della pausa; Sembra funzionare
							if (record.time_stop <= break_start) {
								middle_time = record.time_stop + durationTot - 480;
							} else if (record.time_stop > break_start && record.time_stop <= break_stop) {
								//middle_time = record.time_stop + durationTot - 480 - (record.time_stop - break_start);
								//application.output(record.time_stop + durationTot - 480 - (record.time_stop - break_start))
								application.output('durata + record > 8h : ' + (break_start + durationTot - 480));
								middle_time = break_start + durationTot - 480;
								//events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
							} else if (record.time_stop > break_stop) {
								middle_time = record.time_stop + durationTot - 480 - (break_stop - break_start);
							}

							//middle_time = record.time_stop + durationTot - 480;
							if (middle_time < 360) {
								events_log_overtime.insertOvertime(9, record, record.time_start, middle_time);
								events_log_overtime.insertOvertime(8, record, middle_time, 360);
								durR55current = (middle_time - record.time_start);
								toCheck = true;
							} else if (middle_time >= 360) {
								events_log_overtime.insertOvertime(9, record, record.time_start, 360);
								durR55current = (360 - record.time_start);
								toCheck = true;
							}
						}
						//ore tra le 6 e le 22
						//se durataSR10 + durataSR35_D + durataS55 > 8 tutte a SR35
						if (sumMiddleDuration >= 480) { //sono già state messe 8 ore tra SR10, SR35_d e S55, le rimanenti a SR35
							// se finisce uguale o dopo la pausa pranzo, tolgo la pausa
							if (record.time_stop <= break_start) {
								events_log_overtime.insertOvertime(8, record, 360, record.time_stop);
							} else if (record.time_stop > break_start && record.time_stop <= break_stop) {
								events_log_overtime.insertOvertime(8, record, 360, break_start);
							} else if (record.time_stop > break_stop) {
								events_log_overtime.insertOvertime(8, record, 360, break_start);
								events_log_overtime.insertOvertime(8, record, break_stop, record.time_stop);
							}
						}
						// se durata evento tra le 6 e le 22 + sumMiddleDuration < 8 tutte a SR10
						else if (sumMiddleDuration + (record.time_stop - 360 + durationBreak) <= 480) {
							// se finisce prima della pausa pranzo
							if (record.time_stop <= break_start) {
								events_log_overtime.insertOvertime(7, record, 360, record.time_stop);
							}
							//se finisce in mezzo alla pausa pranzo
							else if (record.time_stop > break_start && record.time_stop <= break_stop) {
								events_log_overtime.insertOvertime(7, record, 360, break_start);
								//events_log_overtime.insertOvertime(3, record, break_stop, record.time_stop);
							}
							//se finisce dopo la pausa pranzo
							else if (record.time_stop > break_stop) {
								events_log_overtime.insertOvertime(7, record, 360, break_start);
								events_log_overtime.insertOvertime(7, record, break_stop, record.time_stop);
							}
							//controllo su un inserimento a posteriori di un evento in orario notturno inserendo una mezzora in più, da capire!
							toCheck = true;
							//if (record.time_stop % 60 != 0)
							//	durationForcheck = -30;
							//else
							durationForcheck = record.duration - durR55current;
						}
						// se durata evento tra le 6 e le 22 + sumMiddleDuration > 8 divito tra SR10 e SR35
						else if (sumMiddleDuration + (record.time_stop - 360 + durationBreak) > 480) {
							middle_time = 360 + (480 - sumMiddleDuration);
							application.output('orario di mezzo ' + middle_time);
							// se ora di mezzo minore di inzio pausa pranzo durata prima dell'ora di mezzo lo inserisco in fascia SR10
							if (middle_time <= break_start) {
								events_log_overtime.insertOvertime(7, record, 360, middle_time);
								// se finisce prima della pausa pranzo
								if (record.time_stop <= break_start) {
									events_log_overtime.insertOvertime(8, record, middle_time, record.time_stop);
								}
								// se finisce in mezzo alla pausa pranzo
								if (record.time_stop > break_start && record.time_stop <= break_stop) {
									if (middle_time < break_start)
										events_log_overtime.insertOvertime(8, record, middle_time, break_start);
									//events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
								}
								// se finisce dopo la pausa pranzo
								else if (record.time_stop > break_stop) {
									// ora di mezzo strettamente minore di inizio pausa
									if (middle_time < break_start)
										events_log_overtime.insertOvertime(8, record, middle_time, break_start);
									events_log_overtime.insertOvertime(8, record, break_stop, record.time_stop);
								}
							}
							// se ora di mezzo cade nella pausa pranzo, la durata prima della pausa pranzo lo inserisco in fascia SR10
							else if (middle_time > break_start && middle_time <= break_stop) {
								events_log_overtime.insertOvertime(7, record, 360, break_start);
								//se stop cade in mezzo alla pausa pranzo non aggiungo nulla; altrimenti:
								if (record.time_stop > break_stop) {
									events_log_overtime.insertOvertime(7, record, break_stop, break_stop + durationBreak);
									if (break_stop + durationBreak < record.time_stop) events_log_overtime.insertOvertime(8, record, break_stop + durationBreak, record.time_stop);
									//if (break_stop + (record.time_stop - middle_time) != record.time_stop) events_log_overtime.insertOvertime(8, record, break_stop + (record.time_stop - middle_time), record.time_stop);
								}
							}
							// se ora di mezzo maggiore di fine pausa pranzo tolgo pausa prima dalla fascia SR10 un'ora
							else if (middle_time > break_stop) {
								events_log_overtime.insertOvertime(7, record, 360, break_start);
								events_log_overtime.insertOvertime(7, record, break_stop, middle_time + durationBreak);
								events_log_overtime.insertOvertime(8, record, middle_time + durationBreak, record.time_stop);
							}
						}
						toCheck = true;
						durationForcheck = record.duration - durR55current;
					}
				}
				//--------------inizia tra le 6 e le 22
				else if (record.time_start < 1320) {
					// se finisce prima delle 22
					if (record.time_stop <= 1320) {
						durationForcheck = record.duration;
						// se sumMiddleDuration > 8 tutte a SR35
						if (sumMiddleDuration >= 480) {
							//se inizia e finisce prima della pausa pranzo
							if (record.time_start < break_start && record.time_stop <= break_start) {
								events_log_overtime.insertOvertime(8, record, record.time_start, record.time_stop);
							}
							//se inizia prima della pausa e finisce in mezzo all pausa pranzo
							else if (record.time_start <= break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
								if (record.time_start < break_start)
									events_log_overtime.insertOvertime(8, record, record.time_start, break_start);
							}
							//se inizia prima della pausa e finisce dopo la pausa pranzo
							else if (record.time_start <= break_start && record.time_stop > break_stop) {
								if (record.time_start < break_start) {
									events_log_overtime.insertOvertime(8, record, record.time_start, break_start);
								}
								events_log_overtime.insertOvertime(8, record, break_stop, record.time_stop);
							}
							//inizia in mezzo alla pausa pranzo e finisce uguale o dopo la pausa
							else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop > break_stop) {
								events_log_overtime.insertOvertime(8, record, break_stop, record.time_stop);
							}
							//inizia dopo la pausa pranzo
							else if (record.time_start >= break_stop && record.time_stop > break_stop) {
								events_log_overtime.insertOvertime(8, record, record.time_start, record.time_stop);
							}
						}
						// evento + sumMiddleDuration < 8 tutte a SR10
						else if (sumMiddleDuration + record.duration <= 480) {
							//se inizia e finisce prima della pausa pranzo
							if (record.time_start < break_start && record.time_stop <= break_start) {
								events_log_overtime.insertOvertime(7, record, record.time_start, record.time_stop);
							}
							//se inizia prima della pausa e finisce in mezzo alla pausa pranzo
							else if (record.time_start <= break_start && record.time_stop > break_start && record.time_stop <= break_stop) {
								if (record.time_start < break_start)
									events_log_overtime.insertOvertime(7, record, record.time_start, break_start);
							}
							//se inizia prima della pausa e finisce dopo la pausa pranzo
							else if (record.time_start <= break_start && record.time_stop > break_stop) {
								if (record.time_start < break_start)
									events_log_overtime.insertOvertime(7, record, record.time_start, break_start);
								events_log_overtime.insertOvertime(7, record, break_stop, record.time_stop);
							}
							//inizia in mezzo alla pausa pranzo e finisce uguale o dopo la fine della pausa
							else if (record.time_start >= break_start && record.time_start < break_stop && record.time_stop > break_stop) {
								events_log_overtime.insertOvertime(7, record, break_stop, record.time_stop);
							}
							//se inizia e finisce dopo la pausa pranzo
							else if (record.time_start >= break_stop && record.time_stop > break_stop) {
								events_log_overtime.insertOvertime(7, record, record.time_start, record.time_stop);
							}
						}
						// se l'evento + sumMiddleDuration > 8 divido in SR10 e SR35
						else if (sumMiddleDuration + record.duration > 480) {
							middle_time = record.time_start + (480 - sumMiddleDuration);

							if (middle_time <= break_start) {
								events_log_overtime.insertOvertime(7, record, record.time_start, middle_time);
								// se finisce prima della pausa pranzo
								if (record.time_stop <= break_start) {
									events_log_overtime.insertOvertime(8, record, middle_time, record.time_stop);
								}
								// se finisce in mezzo alla pausa pranzo
								if (record.time_stop > break_start && record.time_stop <= break_stop) {
									events_log_overtime.insertOvertime(8, record, middle_time, break_start);
								}
								// se finisce dopo la pausa pranzo
								else if (record.time_stop > break_stop) {
									// ora di mezzo strettamente minore di inizio pausa
									if (middle_time < break_start)
										events_log_overtime.insertOvertime(8, record, middle_time, break_start);
									events_log_overtime.insertOvertime(8, record, break_stop, record.time_stop);
								}
							}
							// se ora di mezzo cade nella pausa pranzo, la durata prima della pausa pranzo lo inserisco in fascia S50
							else if (middle_time > break_start && middle_time <= break_stop) {
								events_log_overtime.insertOvertime(7, record, record.time_start, break_start);
								//se stop cade in mezzo alla pausa pranzo non aggiungo nulla, altrimenti:
								if (record.time_stop > break_stop) {
									events_log_overtime.insertOvertime(7, record, break_stop, break_stop + (middle_time - break_start));
									events_log_overtime.insertOvertime(8, record, break_stop + (middle_time - break_start), record.time_stop);
								}
							}
							// se ora di mezzo maggiore di fine pausa pranzo tolgo pausa prima dalla fascia SR10
							else if (middle_time > break_stop) {
								if (record.time_start <= break_start) {
									events_log_overtime.insertOvertime(7, record, record.time_start, break_start);
									events_log_overtime.insertOvertime(7, record, break_stop, break_stop + (middle_time - break_start));
									events_log_overtime.insertOvertime(8, record, break_stop + (middle_time - break_start), record.time_stop);
								} else if (record.time_start > break_start && record.time_start <= break_stop) {
									events_log_overtime.insertOvertime(7, record, break_stop, middle_time + (break_stop - record.time_start));
									events_log_overtime.insertOvertime(8, record, middle_time + (break_stop - record.time_start), record.time_stop);
								} else if (record.time_start > break_stop) {
									events_log_overtime.insertOvertime(7, record, record.time_start, middle_time);
									events_log_overtime.insertOvertime(8, record, middle_time, record.time_stop);
								}
							}
						}
						toCheck = true;
					}
					// inizia dopo le 6 e finisce dopo le 22
					else if (record.time_stop > 1320) {
						application.output('Inizia dopo o uguale a 6, finisce dopo le 22');
						//ore dopo le 22
						//durationTot + durata evento fra inizio e 22 > 8 dopo le 22 SR55
						durationBreak = 0;
						if (record.time_start <= break_start)
							durationBreak = break_stop - break_start;
						else if (record.time_start >= break_start && record.time_start < break_stop)
							durationBreak = break_stop - record.time_start;
						//durata presente >=8 tutte le ore notturne finiscono in SR55
						if (durationTot >= 480) {
							events_log_overtime.insertOvertime(9, record, 1320, record.time_stop);
							toCheck = true;
							durR55current = record.time_stop - 1320;
						}
						//durata totale + durata record <= 8 le notturne in SR35
						else if (durationTot + record.duration <= 480) events_log_overtime.insertOvertime(8, record, 1320, record.time_stop);
						//durata presente + durata del record >=8 tutte le ore notturne a esaurimento delle 8 ore vanno in SR35 le altre in SR55
						else if (durationTot + record.duration > 480) {
							middle_time = record.time_start + (480 - durationTot);
							if (middle_time > 1320) {
								events_log_overtime.insertOvertime(8, record, 1320, middle_time);
								events_log_overtime.insertOvertime(9, record, middle_time, record.time_stop);
								toCheck = true;
								durR55current = record.time_stop - middle_time;
							} else if (middle_time <= 1320) {
								events_log_overtime.insertOvertime(9, record, 1320, record.time_stop);
								toCheck = true;
								durR55current = record.time_stop - 1320;
							}
						}

						//ore tra le 6 e le 22
						// se durationTot > 8  SR35
						if (sumMiddleDuration >= 480) {
							application.output('sumMiddleDuration >= 8: ' + sumMiddleDuration);
							if (record.time_start < break_start) {
								events_log_overtime.insertOvertime(8, record, record.time_start, break_start);
								if (break_stop < 1320) events_log_overtime.insertOvertime(8, record, break_stop, 1320);
							}
							//se inizia dopo inizio pausa e prima di fine pausa e finisce dopo le 22
							else if (record.time_start >= break_start && record.time_start <= break_stop) {
								events_log_overtime.insertOvertime(8, record, break_stop, 1320);
							}
							//inizia dopo la pausa pranzo e finisce dopo le 22
							else if (record.time_start >= break_stop) {
								events_log_overtime.insertOvertime(8, record, record.time_start, 1320);
							}
						}
						// se la durata prima delle 22 + sumMiddleDuration <= 8  SR10 fino alle 22
						else if (sumMiddleDuration + (1320 - record.time_start - durationBreak) <= 480) {
							application.output('durata prima delle 22 + sumMiddleDuration <= 8');
							if (record.time_start < break_start) {
								events_log_overtime.insertOvertime(7, record, record.time_start, break_start);
								events_log_overtime.insertOvertime(7, record, break_stop, 1320);
							}
							//se inizia dopo inizio pausa e prima di fine pausa e finisce dopo le 22
							else if (record.time_start >= break_start && record.time_start <= break_stop) {
								events_log_overtime.insertOvertime(7, record, break_stop, 1320);
							}
							//inizia dopo la pausa pranzo e finisce dopo le 22
							else if (record.time_start >= break_stop) {
								events_log_overtime.insertOvertime(7, record, record.time_start, 1320);
							}
						}// se l'evento + durataSR10 >8 divido in SR10 e SR35
						else if (sumMiddleDuration + record.duration > 480) {
							middle_time = record.time_start + (480 - sumMiddleDuration);
							application.output('evento + durataSR10 >8; middle_time: ' + middle_time);

							if (middle_time <= break_start) {
								events_log_overtime.insertOvertime(7, record, record.time_start, middle_time);
								// se finisce prima della pausa pranzo
								if (record.time_stop <= break_start) {
									events_log_overtime.insertOvertime(8, record, middle_time, 1320);
								}
								// se finisce in mezzo alla pausa pranzo
								if (record.time_stop > break_start && record.time_stop <= break_stop) {
									events_log_overtime.insertOvertime(8, record, middle_time, break_start);
									//events_log_overtime.insertOvertime(4, record, break_stop, record.time_stop);
								}
								// se finisce dopo la pausa pranzo
								else if (record.time_stop > break_stop) {
									// ora di mezzo strettamente minore di inizio pausa
									if (middle_time < break_start)
										events_log_overtime.insertOvertime(8, record, middle_time, break_start);
									if (break_stop != 1320) events_log_overtime.insertOvertime(8, record, break_stop, 1320);
								}
							}
							// se ora di mezzo cade nella pausa pranzo, la durata prima della pausa pranzo lo inserisco in fascia S50
							else if (middle_time > break_start && middle_time <= break_stop) {
								events_log_overtime.insertOvertime(7, record, record.time_start, break_start);
								//se stop cade in mezzo alla pausa pranzo non aggiungo nulla, altrimenti:
								if (record.time_stop > break_stop) {
									events_log_overtime.insertOvertime(7, record, break_stop, break_stop + (middle_time - break_start));
									events_log_overtime.insertOvertime(8, record, break_stop + (middle_time - break_start), 1320);
								}
							}
							// se ora di mezzo maggiore di fine pausa pranzo tolgo pausa prima dalla fascia S50 un ora
							else if (middle_time > break_stop) {
								if (record.time_start < break_start) {
									events_log_overtime.insertOvertime(7, record, record.time_start, break_start);
									events_log_overtime.insertOvertime(8, record, break_stop, 1320);
								} else if (record.time_start >= break_start && record.time_start <= break_stop) {
									events_log_overtime.insertOvertime(7, record, break_stop, middle_time + (break_stop - record.time_start));
									if (middle_time + (break_stop - record.time_start) != 1320) events_log_overtime.insertOvertime(8, record, middle_time + (break_stop - record.time_start), 1320);
								} else if (record.time_start > break_stop) {
									events_log_overtime.insertOvertime(7, record, record.time_start, middle_time);
									events_log_overtime.insertOvertime(8, record, middle_time, 1320);
								}
							}
						}

						durationForcheck = record.duration - durR55current// (durationForSR10 + durationForSR35);
						toCheck = true;
					}
				}
				//--------------inizia dopo le >= 22
				else {
					// se durataTot > 8 tutte a SR55
					if (durationTot >= 480) {
						events_log_overtime.insertOvertime(9, record, record.time_start, record.time_stop);
						durR55current = record.duration;
						toCheck = true;
					}
					// evento + durationTot < 8 tutte a SR35
					else if (durationTot + record.duration <= 480) {
						events_log_overtime.insertOvertime(8, record, record.time_start, record.time_stop);

					}
					// se durata evento + durataTot > 8 divido in SR35 e SR55
					else if (durationTot + record.duration > 480) {
						middle_time = record.time_start + (480 - durationTot);
						if (middle_time > 1320) {
							events_log_overtime.insertOvertime(8, record, record.time_start, middle_time);
							events_log_overtime.insertOvertime(9, record, middle_time, record.time_stop);
							durR55current = record.time_stop - middle_time;
							toCheck = true;
						}
						if (middle_time <= 1320) {
							events_log_overtime.insertOvertime(9, record, record.time_start, record.time_stop);
							durR55current = record.duration;
							toCheck = true;
						}
					}
				}
				//se toCheck è a true, cerco e cambio tutti gli SR35 in SR55 (fasce notturne)
				if (toCheck){
					events_log_overtime.checkOvertimeHoliday(record, record.user_id, record.event_log_date, durationForcheck);
				}
				application.output('Chiamo aggiornamento ore: ', LOGGINGLEVEL.DEBUG);
			
				events_log_additional_charge.insertAdditionalCharge(record, 1);
			}
		}
	} catch (e) {
		throw new Error("- Errore inserimento straordinari: " + e);
	}
	application.output('STOP processOvertimeWithCompensatoryRest - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
}

/**
 * Disabilitata, da chiarire se si implementerà
 * Modifica degli strarodinari
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"D7C66448-D581-4F9F-A424-2BD679B6075A"}
 * @AllowToRunInFind
 */
function processUpdateOvertime(record) {
	//	application.output('START processUpdateOvertime - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
	//	/** @type {JSFoundSet<db:/geco/events_log_overtime>} */
	//	var events_log_overtime = databaseManager.getFoundSet('geco', 'events_log_overtime');
	//
	//	/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
	//	var events_log_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');
	//
	//	/** @type {JSFoundSet<db:/geco/events_log>} */
	//	var events_log = databaseManager.getFoundSet('geco', 'events_log');
	//
	//	var tot = 0;
	//	if (!globals.hasRole('Controllers') && !globals.hasRole('HR Administrators')) {
	//		if (record.event_log_status != 4 && record.event_log_status != 2) {
	//			//cancello gli overtime del record da modificare
	//			events_log_overtime.deleteOvertime(record);
	//			if (record.events_log_to_events.event_id == 28) {
	//				events_log_additional_charge.deleteAdditionalCharge(record, 1);
	//			}
	//		} else throw new Error('- Non è possibile modificare eventi approvati o chiusi');
	//	} else {
	//		//cancello gli overtime del record da modificare
	//		events_log_overtime.deleteOvertime(record);
	//		if (record.events_log_to_events.event_id == 28) {
	//			events_log_additional_charge.deleteAdditionalCharge(record, 1);
	//		}
	//	}
	//
	//	try {
	//		//cerco gli altri eventi di tipo straordinario
	//		if (events_log.find()) {
	//			events_log.user_id = record.user_id;
	//			events_log.event_log_date = record.event_log_date;
	//			events_log.event_id = [27, 28, 38]; //lavoro straordinario o intervento straordinario di reperibilità
	//			events_log.event_log_id = '!' + record.event_log_id;
	//			tot = events_log.search();
	//
	//			if (tot > 0) {
	//				for (var i = 1; i <= tot; i++) {
	//					var toDeleteOvertime = events_log.getRecord(i);
	//					// cancellazione di tutti gli events_overtime_log
	//					events_log_overtime.deleteOvertime(toDeleteOvertime);
	//				}
	//				for (var j = 1; j <= tot; j++) {
	//					var toReinsertOvertime = events_log.getRecord(j);
	//					// cancellazione di tutti gli events_overtime_log
	//					processOvertime(toReinsertOvertime);
	//					if (toReinsertOvertime.event_id == 28)
	//						events_log_additional_charge.deleteAdditionalCharge(toReinsertOvertime, 1);
	//				}
	//			}
	//		}
	//		//richiamo la process per il record modificato
	//		processOvertime(record);
	//	} catch (e) {
	//		throw new Error('- Errore nella modifica degli straordinari');
	//	}
	//	application.output('STOP processUpdateOvertime - record.id: ' + record.event_log_id, LOGGINGLEVEL.DEBUG);
}

/**
 * Processa l'inserimento di eventi strao di riposo compensativo (event_id = 25)
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"99B813F0-A54A-446A-AFD4-E90EC518195F"}
 */
function processCompensatoryRestOnInsert(record) {
	//sto inserendo Riposo compensativo straordinario
	if (record.events_log_to_users.user_type_id == 7 && record.events_log_to_events && record.events_log_to_events.event_id && record.events_log_to_events.event_id == 25) {
		/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
		var events_log_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');
		application.output('Inserimento di ' + (record.duration / 60) + ' ore di recupero per riposo compensantivo straordinario.');
		events_log_additional_charge.insertAdditionalCharge(record, 2);
	}
}

/**
 * Processa la modifica di eventi strao di riposo compensativo (event_id = 25) - disabilitata
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"71CBF894-4CD6-47F9-87A5-2562974293DA"}
 */
function processCompensatoryRestOnUpdate(record) {
	//	//sto inserendo Riposo compensativo straordinario
	//	if (record.events_log_to_users.user_type_id == 7 && record.events_log_to_events.event_id == 25) {
	//		/** @type {JSFoundSet<db:/geco/events_log_additional_charge>} */
	//		var events_log_additional_charge = databaseManager.getFoundSet('geco', 'events_log_additional_charge');
	//		application.output('Inserimento ore di recupero.');
	//		events_log_additional_charge.insertAdditionalCharge(record, 3);
	//	}
}

/**
 * Calcola l'approvatore dell'evento
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"9B7CF7E3-5EB8-4D3B-A952-A061C1A12290"}
 * @AllowToRunInFind
 */
function validateApprover(record) {
	application.output('Data ' + record.event_log_date);
	// don't check for the approver when approving or rejecting or not holiday type
	//	if (record.events_log_to_events.events_to_event_types.event_type_id != 6 && record.event_log_status > 1) return

	var eventApprover = null;

	/** @type {JSFoundSet<db:/geco/approvers>} */
	var approvers = databaseManager.getFoundSet('geco', 'approvers')

	// find out who's the approver figure
	switch (record.events_log_to_events.approver_figure) {
	case 1: // A Profit Center
		eventApprover = approvers.getApprover('profit_centers',
			record.events_log_to_events.events_to_profit_centers_approver.profit_center_id,
			record.event_log_date
		);
		application.output('A Profit Center ' + eventApprover);
		break;

	case 2: // User's boss
		//cerco il profit center di appartenenza in base alla data dell'evento che sta inserendo o modificando
		var profCenterId = record.events_log_to_users.users_to_profit_centers.profit_center_id;
		/** @type {JSFoundSet<db:/geco/users_profit_centers_history>} */
		var users_prof_cent_history = databaseManager.getFoundSet('geco', 'users_profit_centers_history');

		//cerco lo storico dei profit_center dell'utente
		if (users_prof_cent_history.find()) {
			users_prof_cent_history.user_id = record.user_id;
			if (users_prof_cent_history.search() > 0) {
				users_prof_cent_history.sort('start_change_date desc, created_at desc');
				for (var index = 1; index <= users_prof_cent_history.getSize(); index++) {
					var rec = users_prof_cent_history.getRecord(index);
					if (record.event_log_date >= rec.start_change_date) {
						profCenterId = rec.actual_prof_center_id;
						break;
					} else {
						profCenterId = rec.previous_prof_center_id;
					}
				}

			}
		}

		eventApprover = approvers.getApprover('profit_centers',
			profCenterId,
			record.event_log_date
		);
		application.output('User\'s boss ' + eventApprover);
		break;

	case 3: // Job Order (RDC)
		eventApprover = approvers.getApprover('job_orders',
			record.job_order_id,
			record.event_log_date
		);
		if (eventApprover == record.user_id) {
			//utente loggato è approvatore devo cercare il RDC
			eventApprover = record.events_log_to_job_orders.user_owner_id;
			if (eventApprover == record.user_id) {
				//utente loggato è RDC salgo al responsabile del PC
				eventApprover = record.events_log_to_job_orders.job_orders_to_profit_centers.user_owner_id;
				if (eventApprover == record.user_id) {
					//utente loggato è responsabile del PC della commessa risalgo al responsabile dell'utente
					eventApprover = record.events_log_to_users.users_to_profit_centers.user_owner_id;
				}
			}
		}
		application.output('RDC ' + eventApprover);
		break;
	}

	// set the approver
	if (eventApprover) {
		record.user_approver_id = eventApprover;
		// update related record calculation (calendar status)
		databaseManager.recalculate(record.events_log_to_calendar_days);
	} else {
		throw new Error("- Impossibile assegnare l'approvatore dell'evento");
	}

}

/**
 * Controlla che sia presente il codice FEP
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"61E8746D-759D-4146-80AA-63CCE634CF65"}
 */
function validateEventExternalFepCode(record) {
	//if (!globals.isEmpty(record.event_id)) record.external_code_fep = record.events_log_to_events.external_code_fep;
	if (globals.isEmpty(record.external_code_fep)) throw new Error('- Il campo codice FEP non può essere vuoto');
}

/**
 * Controlla che sia presente il codice navision della commessa
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"99E972E6-4BB9-456A-89A8-3E332DA21A80"}
 */
function validateJobOrderExternalNavisionCode(record) {
	if (record.events_log_to_events.has_job_order == 1) {
		record.external_code_job_order = record.events_log_to_job_orders.external_code_navision;
		if (globals.isEmpty(record.external_code_job_order)) throw new Error('- Il campo codice Navision della commessa non può essere vuoto per il tipo di evento scelto');
	}
	if (record.events_log_to_events.has_job_order == 0) {
		record.external_code_navision = record.events_log_to_events.external_code_navision;
		record.external_code_job_order = null;
	}
}


/**
 * valida che si possa cambiare solo la commessa PROVA NON ABILITARE
 * @param {JSRecord<db:/geco/events_log>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"F7CD41B5-BCB9-46FA-8B04-C14210FF8C5C"}
 */
function validateUpdateOvertime2(record) {
//	if (!record.isNew() && (record.event_id == 27 ||  record.event_id == 28)){
//		var changed = record.getChangedData();
//		var changeJobOrder = false;
//		var changeApprover = false;
//		var changeOther = false;
//		var changeDescription = false;
//		for( var i = 1 ; i <= changed.getMaxRowIndex() ; i++ )
//		{ record.description
//			application.output(changed.getValue(i,1) +' '+ changed.getValue(i,2) +' '+ changed.getValue(i,3));
//			if (changed.getValue(i,1) == 'job_order_id' || changed.getValue(i,1) == 'external_code_navision'){
//				changeJobOrder = true;
//			}
//			else if (changed.getValue(i,1) == 'user_approver_id'){
//				changeApprover = true;
//			}
//			else if (changed.getValue(i,1) == 'description'){
//				changeDescription = true;
//			}
//			else {
//				changeOther = true;
//				break;
//			}	
//		}
//		if ((changeDescription || changeJobOrder || changeApprover) && !changeOther) {
//			application.output('cambio commessa');
//		}
//		else throw new Error('- Non è possibile modificare gli orari degli straordinari, cancellare e inserire nuovamente');
//	}
}
