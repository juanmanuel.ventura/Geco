/**
 * Abilitata in insert e update
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"4BC25A89-BF8F-432B-A8C7-D739A25DAD24"}
 */
function validateDescription(record) {
	if (globals.isEmpty(record.bo_description)) {
		throw new Error('- Il campo Descrizione è obbligatorio');
	}
}

/**
 * Abilitata in insert e update
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"0D1AD1D7-FFE7-4168-AD84-F7BDC11FCEDD"}
 */
function validateClientManager(record) {
	//	if (globals.isEmpty(record.client_manager_id)) {
	//		throw new Error('- Il campo Client Manager è obbligatorio');
	//	}
}

/**
 * Abilitata in insert e update
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"8A16257D-2C21-4DF0-BF0F-5EDEA58DA62A"}
 */
function validateMarket(record) {
	if (globals.isEmpty(record.profit_center_id)) {
		throw new Error('- Il campo Mercato è obbligatorio');
	}
	//TODO da togliere quando si definiscono i PC vecchi rispetto alla nuova organizzazione
	record.profit_center_id_old = record.profit_center_id
}

/**
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"6AF297D7-08E7-4D7F-8A9F-D988AA73F34D"}
 */
function validateCustomer(record) {
	if (globals.isEmpty(record.company_id) || globals.isEmpty(record.final_company_id)) {
		throw new Error('- I campi Cliente e Cliente Finale sono obbligatori');
	}
}


/**
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"A275E429-96D8-479F-BFD0-69065E8463D8"}
 */
function validateBOType(record) {
	//	if (globals.isEmpty(record.bo_type)) {
	//		throw new Error('- I campi Tipo BO è obbligatorio');
	//	}
}

/**
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"922CAD4E-60D0-496C-82BB-F122A8FCAC72"}
 */
function validateChangeProbability(record) {
	if (globals.isEmpty(record.bo_probability)) {
		//		throw new Error('- I campo Probabilità è obbligatorio');
	}
	/** @type {JSDataSet} */
	var dsUpdate = record.getChangedData();
	for (var i = 1; i <= dsUpdate.getMaxRowIndex(); i++) {
		application.output(dsUpdate.getValue(i, 1) + ' ' + dsUpdate.getValue(i, 2) + ' ' + dsUpdate.getValue(i, 3));
		if (dsUpdate.getValue(i, 1) == 'bo_probability') {
			//se sta cambiando la probabilità passando al 90% da un valore minore
			if (dsUpdate.getValue(i, 2) < dsUpdate.getValue(i, 3) && dsUpdate.getValue(i, 3) == 90) {
				if (record.bo_to_job_orders != null && record.bo_to_job_orders.job_order_type == 4) {
					if (!globals.hasBusinessCaseProfit(record.bo_id)) {
						throw new Error('- Non puoi cambiare la probabilità al 90%, non è presente il BC per i ricavi');
					}
					if (record.order_amount != record.bo_to_bo_details.total_return) {
						throw new Error('- Non puoi cambiare la probabilità al 90%, il totale dei ricavi inseriti non corrisponde al valore dell\'ordinato');
					}
				} else {
					if (!globals.hasBusinessCase(record.bo_id) && record.job_order_type_id != 4)
						throw new Error('- Non puoi cambiare la probabilità al 90%, non è presente il BC');
				}
				if (record.job_order_type_id != 4 && (globals.isEmpty(record.start_date) || globals.isEmpty(record.end_date))) {
					throw new Error('- Indicare le date di inizio e fine attività');
				}
			}
			if (dsUpdate.getValue(i, 2) < dsUpdate.getValue(i, 3) && dsUpdate.getValue(i, 3) >= 50) {
				if (globals.isEmpty(record.offer_amount) || record.offer_amount == 0)
					throw new Error('- Non puoi cambiare la probabilità al 50% o superiore, non è presente il valore dell\'offerta');
//				if (globals.isEmpty(record.offering_date))
//					throw new Error('- Non puoi cambiare la probabilità al 50% o superiore, non è presente la data dell\'offerta');
			}
			if (dsUpdate.getValue(i, 2) < dsUpdate.getValue(i, 3) && dsUpdate.getValue(i, 3) == 100) {
				if (globals.isEmpty(record.order_amount) || record.order_amount == 0)
					throw new Error('- Non puoi cambiare la probabilità al 100%, non è presente il valore dell\'ordine');
			}
		}
	}
}

/**
 * Abilitata in insert e update
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"7F6F6132-5239-4904-9A7D-FBFEB0DBFE40"}
 */
function validateMarketOwner(record) {
	if (globals.isEmpty(record.pc_owner_id)) {
		throw new Error('- Il campo Responsabile di Mercato è obbligatorio');
	}
}

/**
 * Abilitata in insert e update
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"5680F4FA-CD38-473F-87B7-D24D3753AF22"}
 */
function validateMarketOwnerToMarket(record) {
	if (!globals.isEmpty(record.pc_owner_id) && !globals.isEmpty(record.profit_center_id)) {
		var marketList = scopes.globals.getProfitCenterByUserManagerForMarket(record.pc_owner_id);
		if (marketList.indexOf(record.profit_center_id) < 0)
			throw new Error('- Il campo Responsabile di Mercato e il Mercato scelto non corrispondono');
	}
}

/**
 * Abilitata in insert e update
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"0BBA1FF9-B387-4616-9ECC-E00F19815A64"}
 */
function validateDate(record) {

//		if (!globals.isEmpty(record.start_date) && !globals.isEmpty(record.end_date)) {
//			var start = new Date(record.start_date.getFullYear(), record.start_date.getMonth(), 1);
//			var stop = new Date(record.end_date.getFullYear(), record.end_date.getMonth(), 1);
//			if (start > stop)
//				throw new Error('- La data di Fine Attività non può essere inferiore alla data di Inizio Attività');
//		}

	var dateFrom = null;
	var dateTo = null;
	if (record.start_date != null)
		dateFrom = new Date(record.start_date.getFullYear(), record.start_date.getMonth(), 1);
	if (record.end_date != null)
		dateTo = new Date(record.end_date.getFullYear(), record.end_date.getMonth() + 1, 0);
	application.output('start date ' + dateTo + ' record.end_date ' + dateFrom);
	for (var index = 1; index <= record.bo_to_bo_planning.getSize(); index++) {
		var recPl = record.bo_to_bo_planning.getRecord(index);
		application.output(new Date(recPl.bo_year, recPl.bo_month - 1, 1));
		var dateJoPl = new Date(recPl.bo_year, recPl.bo_month - 1, 1);
		if (dateFrom != null && dateJoPl < dateFrom) {
			throw new Error('- Non è possibile modificare la data di inizio, esistono piani relativi a mesi precedenti alla data inserita');
		}
		if (dateTo != null && dateTo < dateJoPl) {
			throw new Error('- Non è possibile modificare la data di fine, esistono piani relativi a mesi successivi alla data inserita');
		}
	}
	if (dateFrom > dateTo) {
		throw new Error('- La data di Fine Attività non può essere inferiore alla data di Inizio Attività');
	}
}

/**
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"16F2B15C-487F-442E-A10F-3C7AE3F27723"}
 */
function validateGeographicalArea(record) {
	if (globals.isEmpty(record.geographical_area)) {
		throw new Error('- Il campo Area Geografica è obbligatorio');
	}
}

/**
 * Verifica che i clienti iniziale e finale scelti per quel mercato siano effettivamente presenti per quel Supp. Comm
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"BB0D6AC7-CD2D-409E-9BF6-84939F646FC2"}
 * @AllowToRunInFind
 */
function validateClientsMarketSuppComm(record) {
	if (scopes.globals.hasRole('Supp. Commerciale')) {
		/** @type {JSFoundSet<db:/geco/comm_supp_profit_centers>} */
		var comm_supp_pc = databaseManager.getFoundSet('geco', 'comm_supp_profit_centers');
		/** @type {JSFoundSet<db:/geco/comm_supp_customers>} */
		var comm_supp_customers = databaseManager.getFoundSet('geco', 'comm_supp_customers');
		var totPC = 0;
		var totClientStart = 0;
		var totClientEnd = 0;
		var recPC = null;
		
		if (comm_supp_pc.find()){
			comm_supp_pc.user_id = scopes.globals.currentUserId;
			comm_supp_pc.profit_center_id = record.profit_center_id;
			totPC = comm_supp_pc.search();
		}
		
		if (totPC > 0){
			recPC = comm_supp_pc.getRecord(1);
			if (comm_supp_customers.find()){
				comm_supp_customers.comm_supp_pc_id = recPC.comm_supp_profit_center_id;
				comm_supp_customers.customer_id = record.company_id;
				totClientStart = comm_supp_customers.search();
			}
			application.output('ricerca per cliente iniziale: ' + (databaseManager.getSQL(comm_supp_customers)));
			
			//non si sa mai, ricarico i record
			comm_supp_customers.loadRecords();
			
			if (comm_supp_customers.find()){
				comm_supp_customers.comm_supp_pc_id = recPC.comm_supp_profit_center_id;
				comm_supp_customers.customer_id = record.final_company_id;
				totClientEnd = comm_supp_customers.search();
			}
			application.output('ricerca per cliente finale: ' + (databaseManager.getSQL(comm_supp_customers)));
			
			//non trovo nulla? errore
			if ((totClientStart == 0 || totClientEnd == 0)) throw new Error('- I clienti iniziale o finale (o entrambi) non appartengono al mercato selezionato.');
		}
	}
}


/**
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"9082ECBC-513C-4E82-BA6C-73733EE82873"}
 */
function processMol(record) {
	application.output('START BO updateMol');
	var molEuro = 0;
	var molPer = 0;
	if (record.profit != null && record.profit > 0) {
		molEuro = (record.profit - record.cost);
		// forza le 2 cifre decimali
		molEuro = Math.round(molEuro * Math.pow(10, 2)) / Math.pow(10, 2);

		molPer = (record.profit - record.cost) * 100 / record.profit
		// forza le 2 cifre decimali
		molPer = Math.round(molPer * Math.pow(10, 2)) / Math.pow(10, 2);
	}
	record.mol_euro = molEuro;
	record.mol = molPer;
	application.output('STOP BO updateMol');
}

/**
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"D824A0F9-AE59-43FB-8FAA-5A18E7817943"}
 */
function processDate(record) {
	if (record.start_date != null)
		record.start_date = new Date(record.start_date.getFullYear(), record.start_date.getMonth(), 1);
	if (record.end_date != null)
		record.end_date = new Date(record.end_date.getFullYear(), record.end_date.getMonth() + 1, 0);
	application.output('start date ' + record.start_date + ' record.end_date ' + record.end_date)

}

/**
 * @param {JSRecord<db:/geco/business_opportunities>} record
 * @throws {Error}
 * @properties={typeid:24,uuid:"1B6EB95F-7500-4893-9E8F-5B9C24E7EF59"}
 */
function processChangeProbability(record) {
	if (globals.isEmpty(record.bo_probability)) {
		//		throw new Error('- I campo Probabilità è obbligatorio');
	}
	/** @type {JSDataSet} */
	var dsUpdate = record.getChangedData();
	for (var i = 1; i <= dsUpdate.getMaxRowIndex(); i++) {
		application.output(dsUpdate.getValue(i, 1) + ' ' + dsUpdate.getValue(i, 2) + ' ' + dsUpdate.getValue(i, 3));
		if (dsUpdate.getValue(i, 1) == 'bo_probability') {
			//se sta cambiando la probabilità passando al 90% da un valore minore
			if (dsUpdate.getValue(i, 2) < dsUpdate.getValue(i, 3) && dsUpdate.getValue(i, 3) < 50) {
				// se non era persa o annullata la metto in stato aperto
				record.status = (record.status != 2 && record.status != 5) ? 1:record.status;
			}
			if (dsUpdate.getValue(i, 2) < dsUpdate.getValue(i, 3) && dsUpdate.getValue(i, 3) == 50) {
				if (!globals.isEmpty(record.offer_amount) && !record.offer_amount == 0)
					record.status = 4;
			}
			// tra 50 e 100
			if (dsUpdate.getValue(i, 2) < dsUpdate.getValue(i, 3) && dsUpdate.getValue(i, 3) > 50 && dsUpdate.getValue(i, 3)<100) {
				if (!globals.isEmpty(record.offer_amount) && !record.offer_amount == 0)
					record.status = 4;
			}
			if (dsUpdate.getValue(i, 2) < dsUpdate.getValue(i, 3) && dsUpdate.getValue(i, 3) == 100) {
				if (!globals.isEmpty(record.order_amount) && !record.order_amount == 0)
					record.status = 3;
			}
			//da 100% torna sotto
			if (dsUpdate.getValue(i, 2) == 100 && dsUpdate.getValue(i, 3) < dsUpdate.getValue(i, 2) ) {
				if (dsUpdate.getValue(i, 3) >= 50)
					record.status = 4;
				else record.status = 1;
			}
		}
	}
}
