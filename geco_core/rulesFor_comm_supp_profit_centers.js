/**
 * @param {JSRecord<db:/geco/comm_supp_profit_centers>} record
 * @properties={typeid:24,uuid:"A1636A69-7F33-4775-8DBA-86ABD9372BCE"}
 * @AllowToRunInFind
 */
function validateCommSuppPC(record) {
	if (globals.isEmpty(record.user_id)) {
		throw new Error('- Il campo supporto commerciale è obbligatorio');
	} else {
		//cerco se esiste già la coppia Supp Comm - Mercato
		/** @type {JSFoundSet<db:/geco/comm_supp_profit_centers>} */
		var comm_supp_profit_centers = databaseManager.getFoundSet('geco', 'comm_supp_profit_centers');
		var count = 0;

		if (comm_supp_profit_centers.find()) {
			comm_supp_profit_centers.comm_supp_profit_center_id = '!' + record.comm_supp_profit_center_id;
			comm_supp_profit_centers.user_id = record.user_id;
			comm_supp_profit_centers.profit_center_id = record.profit_center_id;

			count = comm_supp_profit_centers.search();
		}

		if (count > 0) {
			throw new Error("- Esiste già la coppia Supporto Commerciale - Mercato, modificare la coppia esistente.");
		}
	}
}

/**
 * @param {JSRecord<db:/geco/comm_supp_profit_centers>} record
 * @properties={typeid:24,uuid:"456E0533-3E36-4155-831D-AF3A60E75C20"}
 * @AllowToRunInFind
 */
function validateCommSuppNotOwnerPC(record) {
	// cercare nella tabella owner che non sia responsabile dello stesso centro profitto
	/** @type {JSFoundSet<db:/geco/owners_profit_centers>} */
	var ownerPC = databaseManager.getFoundSet('geco', 'owners_profit_centers');
	ownerPC.loadRecords();
	var ownerEdited = databaseManager.getEditedRecords(record.comm_supp_to_profit_centers.profit_centers_to_owners_markets);
	for (var i = 0; i< ownerEdited.length; i++){
		/** @type {JSRecord<db:/geco/owners_profit_centers>} */
		var recOwner = ownerEdited[i];
		if (recOwner.user_id == record.user_id && recOwner.profit_center_id == record.profit_center_id) {
			throw new Error('- La persona selezionata è Responsabile del centro profitto');
		}
	}	
	if (ownerPC.find()) {
		ownerPC.user_id = record.user_id;
		ownerPC.profit_center_id = record.profit_center_id;
		if (ownerPC.search() > 0) throw new Error('- La persona selezionata è Responsabile del centro profitto');
	}
}


/**
 * @param {JSRecord<db:/geco/comm_supp_profit_centers>} record
 * @properties={typeid:24,uuid:"C6B989AE-5B26-4C4F-A67E-6A7A9E4D1888"}
 * @AllowToRunInFind
 */
function validateCommSuppNotVicePC(record) {
	// cercare nella tabella vice_owner che non sia responsabile dello stesso centro profitto
	/** @type {JSFoundSet<db:/geco/vice_owner_profit_center>} */
	var ownerPC = databaseManager.getFoundSet('geco', 'vice_owner_profit_center');
	ownerPC.loadRecords();
	var viceEdited = databaseManager.getEditedRecords(record.comm_supp_to_profit_centers.profit_centers_to_vice_owner);
	
	
	for (var i = 0; i< viceEdited.length; i++){
		/** @type {JSRecord<db:/geco/vice_owner_profit_center>} */
		var recVice = viceEdited[i];
		if (recVice.user_id == record.user_id && recVice.profit_center_id == record.profit_center_id) {
			throw new Error('- La persona selezionata è Vice Responsabile del centro profitto');
		}
	}
	
	if (ownerPC.find()) {
		ownerPC.user_id = record.user_id;
		ownerPC.profit_center_id = record.profit_center_id;
		if (ownerPC.search() > 0) throw new Error('- La persona selezionata è Vice Responsabile del centro profitto');
	}
	
	
}

/**
 * Inserisce user nel gruppo supp. commerciale
 * @param {JSRecord<db:/geco/comm_supp_profit_centers>} record
 * @properties={typeid:24,uuid:"A2BB9A80-3AF4-46BB-8D7F-2667A84A2E97"}
 */
function processAddCommSupportGroup(record) {
	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
	try {
		//Se tipologia di PC è mercato.
		if (record.comm_support_to_market.profit_center_type_id == 5) {
			//Supporto Commerciale
			user_groups.addUserGroup(record.user_id, 27);
		}
	} catch (e) {
		throw new Error("- Impossibile assegnare il ruolo Supporto Commerciale all\'utente\n" + e['message']);
	}
}

/**
 * Cancella user dal gruppo supp. commerciale
 * @param {JSRecord<db:/geco/comm_supp_profit_centers>} record
 * @properties={typeid:24,uuid:"30523D94-662D-4F9F-B522-F18FA2500707"}
 * @AllowToRunInFind
 */
function processDeleteUserFromGroup(record) {

	/** @type {JSFoundSet<db:/geco/comm_supp_profit_centers>} */
	var comm_supp = databaseManager.getFoundSet('geco', 'comm_supp_profit_centers');
	var otherSupp = 0;
	if (comm_supp.find()) {
		comm_supp.user_id = record.user_id;
		comm_supp.comm_supp_profit_center_id = '!' + record.comm_supp_profit_center_id;
		otherSupp = comm_supp.search();
	}
	if (otherSupp == 0) {

		/** @type {JSFoundSet<db:/geco/user_groups>} */
		var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
		application.output('sto per cancellare solo dal gruppo 27');
		user_groups.deleteUserGroup(record.user_id, [27]);
	}

}

/**
 * Cancella customer list associata
 * @param {JSRecord<db:/geco/comm_supp_profit_centers>} record
 * @properties={typeid:24,uuid:"A33AD460-B951-4FD4-8273-2B6624C0AA43"}
 * @AllowToRunInFind
 */
function processDeleteCustomerList(record) {
	//DF cancella la lista di clienti associati all'utente per il pc
	record.comm_supp_pc_to_comm_supp_customers.deleteAllRecords();
}
