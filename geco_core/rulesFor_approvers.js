/**
 * @param {JSRecord<db:/geco/approvers>} record
 * @properties={typeid:24,uuid:"880350A4-30AE-4924-A52D-67317586E40E"}
 */
function validateApprover(record) {
	if (globals.isEmpty(record.user_approver_id)) {
		throw new Error('- Il campo approvatore è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/approvers>} record
 * @properties={typeid:24,uuid:"076E988A-7EFB-47B3-B7A3-8747E869ECC9"}
 */
function validateApproverEntityType(record) {
	if (globals.isEmpty(record.entity)) {
		throw new Error('- Il tipo di entità per approvatore è obbligatorio');
	}
}

/**
 * @param {JSRecord<db:/geco/approvers>} record
 * @properties={typeid:24,uuid:"10159E79-19C7-4A65-9C7C-11974B0DF6B4"}
 */
function validateApproverDateFrom(record) {
	if (globals.isEmpty(record.user_approver_from)) {
		throw new Error('- Il campo data approvatore è obbligatorio');
	}
	if (!globals.hasRole('Controllers') && record.user_approver_from < record.approvers_to_job_orders.date_approver_from){
		throw new Error('- La data di inizio approvazione non può essere minore di quella indicata dal Controller');
	}
}


/**
 * @param {JSRecord<db:/geco/approvers>} record
 * @properties={typeid:24,uuid:"67BE440C-9136-41F6-A9CD-66C66ABB7F86"}
 * @AllowToRunInFind
 */
function validateNotSameApprover(record) {
	/** @type {JSFoundSet<db:/geco/approvers>} */
	var approvers = databaseManager.getFoundSet('geco', 'approvers');
	application.output('validateNotSameApprover ' + record);
	//FS
	if(globals.isEmpty(record.user_approver_from)){
		return;
	}
	var tot = 0;
	if(approvers.find()){
		approvers.entity = record.entity;
		approvers.entity_id = record.entity_id;
		approvers.user_approver_id = record.user_approver_id;
		/** @type {String} */
		var year = record.user_approver_from.getFullYear();
		/** @type {String} */
		var month = '';
		month = record.user_approver_from.getMonth()+1;
		/** @type String */
		var month1 = month.toLocaleString();
		/** @type {String} */
		var day = '';
		day = record.user_approver_from.getDate().toString();
		/** @type {String} */
		var date = year + '-' + (month1.length > 1? month1 : '0'+ month1) + '-' + (day.length > 1? day : '0'+day);
		approvers.user_approver_from =  date +'|yyyy-MM-dd';
		tot = approvers.search();
	}
	
	if (tot == 1) {
		
	throw new Error('- Impossibile aggiungere: approvatore già esistente');
	}
}

/**
 * @param {JSRecord<db:/geco/approvers>} record
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"CC700A87-CDEF-422B-BDDC-E0DE3296F388"}
 */
function processAddUserGroup(record) {

	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');
	application.output('APPROVERS processAddUserGroup chiama user_groups.addUserGroup ');
	user_groups.addUserGroup(record.user_approver_id, 11);
	//FS l'id dell'utente è già settato nella tabella user_groups quindi non deve essere aggiunto ancora
	globals.isApproversAddtoUserGroupTable=true;
	
}

/**
 * @param {JSRecord<db:/geco/approvers>} record
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"83234AC0-08E8-4718-931A-1DC2335274EB"}
 */
function processDeleteUserApproverGroup(record) {
	application.output(record.entity);
	/** @type {JSFoundSet<db:/geco/approvers>} */
	var approvers = databaseManager.getFoundSet('geco', 'approvers');
	
	var change = record.getChangedData();
	var userId =  record.user_approver_id;
	var dateFrom = record.user_approver_from;
	var toRevert = false;
	for( var i = 1 ; i <= change.getMaxRowIndex() ; i++ )
	{
		application.output(change.getValue(i,1) +' '+ change.getValue(i,2) +' '+ change.getValue(i,3));
		if (change.getValue(i,1) == 'user_approver_id') userId = change.getValue(i,2);
		if (change.getValue(i,1) == 'user_approver_from') dateFrom = change.getValue(i,2)
		toRevert = true;
	}

	//unico approvatore per l'entity
	if (approvers.checkIsOnlyApprover(userId,record.entity,record.entity_id,dateFrom)) {
		if (toRevert) record.revertChanges();
		throw new Error('- Non è possibile cancellare l\'unico approvatore');
	}

	//consuntivazioni o note spese non approvate
//	if (record.entity == 'profit_centers' && approvers.hasEventToApprove(record)) {
//		throw new Error('- Non è possibile cancellare l\'approvatore, ci sono consuntivazioni aperte di cui è approvatore');
//	}
	
	var totUserGroup = 0;

	/** @type {JSFoundSet<db:/geco/user_groups>} */
	var user_groups = databaseManager.getFoundSet('geco', 'user_groups');

	//find in user_group if exist record user_id-group_id (11=approvers)
	totUserGroup = user_groups.searchUserGroup(record.user_approver_id, 11);

	//delete if user is in approvers group and 
	//if user is not job_order or profit_center owner
	//and if user is not approver for another entity 
	if (totUserGroup == 1 && 
		!approvers.isOwnerApprover(record.user_approver_id) && 
		!approvers.isApproverForAnotherEntity(record.user_approver_id,record.entity,record.entity_id)) {
			user_groups.deleteUserGroup(record.user_approver_id,[11]);
	}
}
