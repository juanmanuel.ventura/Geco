/**
 * @type {String}
 * @properties={typeid:35,uuid:"E7CC4D6E-E24E-470B-9462-0DE566F20322"}
 */
var recipient = "";

/**
 * @type {String}
 * @properties={typeid:35,uuid:"4A459C4D-976C-4F59-AD42-47814420B7A6"}
 */
var username = "";

/**
 * @type {String}
 * @properties={typeid:35,uuid:"04D7F550-AC18-424C-B762-C75FB4F56E10"}
 */
var subject = "";

/**
 * @type {String}
 * @properties={typeid:35,uuid:"8B277AEF-6719-40FF-BBBD-A5D3FE481AC5"}
 */
var messagage = "";

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 * @properties={typeid:24,uuid:"45F3D302-93BC-4DE7-945B-FD2B87B4C165"}
 */
function send(event) {
	if (plugins.mail.isValidEmailAddress(recipient) && 
		subject !=null && messagage != null) {
			globals.enqueueMailReminder(recipient, subject, messagage);
	}
	else {
		application.output('Error sending mail: not valid input');
	}
}
