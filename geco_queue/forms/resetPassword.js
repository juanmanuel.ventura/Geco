/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"FF6CE707-3376-4A5F-9883-4B04988F814D",variableType:4}
 */
var toClose = 0;

/**
 * Handle changed data.

 * @private
 *
 * @properties={typeid:24,uuid:"FECF17DF-FEC8-4B4F-BF12-96E2F8415D4A"}
 */
function checkToClose() {
	if (globals.solutionToGo != '') {
		application.closeSolution(globals.solutionToGo);
	}
	else {
		application.closeAllWindows();
		application.closeSolution();
	}
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"0592C169-2F27-4489-8751-0903A5B2EC2A"}
 */
function onLoad(event) {
	application.output('caricata ' +toClose);
	if (toClose == 1) {
	application.sleep(10000);
	application.output('chiudo');
	application.closeSolution();
	}
}
