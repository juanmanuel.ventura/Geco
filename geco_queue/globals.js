/**
 * @type {String}
 * @properties={typeid:35,uuid:"97E54E47-5943-4F28-8F50-A59C14D90662"}
 */
var resultString = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"5870E3D1-75DB-4BA6-A78A-C6CA557F5FD6"}
 */
var solutionToGo ='';

/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
 * @return {Object}
 * @properties={typeid:24,uuid:"734E8D5A-6AAC-463E-BFC4-A1630D9F6234"}
 */
function sendMail(recipient, subject, message) {
	var result = {
		recipient: recipient,
		subject: subject,
		message: message,
		success: true
	};

	var properties = [];
	properties.push('mail.smtp.host=smtp.gmail.com');
	properties.push('mail.smtp.auth=true');
	properties.push('mail.smtp.username=' + mailOptions.username);
	properties.push('mail.smtp.password=' + mailOptions.password);
	properties.push('mail.smtp.port=587');
	properties.push('mail.smtp.starttls.enable=true');

	var success = plugins.mail.sendMail(recipient, '[GECO] <noreply@spindox.it>', subject, message, null, null, null, properties);
	if (!success) {
		application.output(plugins.mail.getLastSendMailExceptionMsg());
		result.success = false;
		result.message = 'Errore invio mail';
	}
	return result;
}

/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
 * @return {Object}
 * @properties={typeid:24,uuid:"4B2BEB07-E6C1-479E-AA95-6866090DF005"}
 */
function sendMailPassword(recipient, subject, message) {
	var result = {
		recipient: recipient,
		subject: subject,
		message: message,
		success: true
	};

	var properties = [];
	properties.push('mail.smtp.host=smtp.gmail.com');
	properties.push('mail.smtp.auth=true');
	properties.push('mail.smtp.username=' + mailOptions.username);
	properties.push('mail.smtp.password=' + mailOptions.password);
	properties.push('mail.smtp.port=587');
	properties.push('mail.smtp.starttls.enable=true');

	var success = plugins.mail.sendMail(recipient, '[GECO] <noreply@spindox.it>', subject, message, null, null, null, properties);
	if (!success) {
		application.output(plugins.mail.getLastSendMailExceptionMsg());
		result.success = false;
	}
	return result;
}

/**
 * @param {String} username
 * @param {String} urlCaller
 * @param {String} solutionName
 * @return {Object}
 * @properties={typeid:24,uuid:"FFDB833A-5BD2-459F-A70E-D1C7E5FA0DCD"}
 * @AllowToRunInFind
 */
function sendMailResetPassword(username, urlCaller, solutionName) {
	var result = {
		message: '',
		success: true
	};
	application.output(globals.messageLog + ' START queue.sendMailResetPassword() username ' + username + ' serverUrl chiamante ' + urlCaller + ' solution ' + solutionName,LOGGINGLEVEL.INFO);
	var recipient, subject, message = null;
	/** @type {JSFoundSet<db:/geco/users>} */
	var users = databaseManager.getFoundSet('geco', 'users');

	var user = null;
	if (users.find()) {
		users.user_name = username
		if (users.search() == 1) {
			user = users.getSelectedRecord();
			if (user.is_enabled == 0) {
				result.success = false;
				result.message = 'Utente disabilitato';
			}
//			else if (user.is_locked == 1){
//				result.success = false;
//				result.message = 'Utente bloccato';
//			}
			else {
				var text = "";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

				for (var i = 0; i < 8; i++) {
					text += possible.charAt(Math.floor(Math.random() * possible.length));
				}
				application.output(text);
				/** @type {JSFoundSet<db:/geco/user_password_reset>} */
				var user_password_reset = databaseManager.getFoundSet('geco', 'user_password_reset');
				user_password_reset.newRecord();
				user_password_reset.user_id = user.user_id;
				user_password_reset.reset_id = text;

				user.has_requested_reset = 1;
				databaseManager.saveData();
				//var hostname = application.getServerURL();
				var hostname = urlCaller;
				application.output(globals.messageLog + ' queue.sendMailResetPassword() serverUrl ' + hostname,LOGGINGLEVEL.INFO);
				//{solutionName}&m={methodName}&a={value}&{param1}={value1}&{param2}={value2}
				var link = hostname + '/servoy-webclient/solutions/solution/geco_queue/method/metodo/argument/' + user_password_reset.user_id + '|' + user_password_reset.reset_id+'|'+solutionName;
				application.output(link);
				//mail template
				var objMail = globals.getMailType(8);
				recipient = user.users_to_contacts.default_email.channel_value;
				subject = objMail.subject;
				globals.mailSubstitution.nome = user.users_to_contacts.first_name;
				globals.mailSubstitution.link = link;
				message = plugins.VelocityReport.evaluateWithContext(objMail.text, globals.mailSubstitution);

				//message = "Ciao " + user.users_to_contacts.first_name + "\nper ricevere la nuova password segui il link:\n\n " + link + "\n\nHelpdesk Geco";
				application.output(globals.messageLog + ' queue.sendMailResetPassword() sending mail to ' + recipient,LOGGINGLEVEL.INFO);
				result = sendMail(recipient, subject, message);
			}
		} else {
			result.success = false;
			result.message = 'Utente non trovato';
			application.output(globals.messageLog + ' queue.sendMailResetPassword() Utente non trovato ' + username,LOGGINGLEVEL.INFO);
		}
	}
	
	application.output(globals.messageLog + ' STOP queue.sendMailResetPassword() risultato ' + result.success + result.message,LOGGINGLEVEL.INFO);
	return result;
}

/**
 * @properties={typeid:24,uuid:"19C7B8CD-4F12-4B9A-8C73-DF076EF2FACB"}
 * @AllowToRunInFind
 */
function metodo() {
	/** @type {Array} */
	var vArguments = arguments[0].split('|');
	
	var recipient = null;
	var subject = null;
	var message = null;
	/** @type {Object} */
	var result = null;
	application.output(globals.messageLog + ' START queue.metodo() input ' + vArguments,LOGGINGLEVEL.INFO);
	/** @type {JSFoundSet<db:/geco/user_password_reset>} */
	var pwd_reset = databaseManager.getFoundSet('geco', 'user_password_reset');
	//var serverUrl = getServerUrlGeco(application.getServerURL());
	//var addressPage = '/application_server/server/webapps/ROOT/servoy-webclient/templates/default/';
	// pageToRedirect = serverUrl + addressPage + '';
	if (pwd_reset.find()) {
		pwd_reset.user_id = vArguments[0];
		pwd_reset.reset_id = vArguments[1];
		pwd_reset.is_verified = 0;
		if (pwd_reset.search() == 1) {
			/** @type {JSFoundSet<db:/geco/users>} */
			var users = databaseManager.getFoundSet('geco', 'users');
			if (users.find()) {
				users.user_id = pwd_reset.user_id;
				users.is_enabled = 1;
				if (users.search() == 1) {
					var newPwd = "";
					var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

					for (var i = 0; i < 8; i++) {
						newPwd += possible.charAt(Math.floor(Math.random() * possible.length));
					}

					var objMail = globals.getMailType(9);
					recipient = users.users_to_contacts.default_email.channel_value;
					subject = objMail.subject;
					globals.mailSubstitution.nome = users.users_to_contacts.first_name;
					globals.mailSubstitution.newPwd = newPwd;
					message = plugins.VelocityReport.evaluateWithContext(objMail.text, globals.mailSubstitution);
					application.output(globals.messageLog + 'queue.metodo() sending mail to ' + recipient,LOGGINGLEVEL.INFO);
					result = sendMail(recipient, subject, message);

					//					recipient = users.users_to_contacts.default_email.channel_value;
					//					subject = 'Geco invio pwd';
					//					message = 'nuova password ' + newPwd;
					//					result = globals.sendMailPassword(recipient,subject,message);
					if (result.success == true) {

						//inserisco la nuova password nell'history
						/** @type {JSFoundSet<db:/geco/users_password_expiration>} */
						var userspwdex = databaseManager.getFoundSet('geco', 'users_password_expiration');
						var insertResutl = userspwdex.insertNewPassword(users.user_id, globals.sha1(newPwd), 0, true);
						if (insertResutl == false) {
							resultString = 'Geco - Errore nel reset della password';
							application.output(globals.messageLog + 'queue.metodo() errore inserimento history ',LOGGINGLEVEL.INFO);
							//pageToRedirect = serverUrl + addressPage + 'GecoSendMailKO.html'
						}
						users.user_password = globals.sha1(newPwd);
						application.output(globals.messageLog + 'queue.metodo() password cambiata ',LOGGINGLEVEL.INFO);
						users.has_requested_reset = 0;
						if (users.is_locked == 1) {
							application.output(globals.messageLog + 'queue.metodo() utente sbloccato ',LOGGINGLEVEL.INFO);
							users.is_locked = 0;
							users.failed_login_attempts = 0;
						}
						pwd_reset.is_verified = 1;
						databaseManager.saveData(pwd_reset);
						databaseManager.saveData(userspwdex);
						application.output(globals.messageLog + 'queue.metodo() salvata password expiration ',LOGGINGLEVEL.INFO);
						databaseManager.saveData(users);
						resultString = 'Geco - Nuova password inviata';
						if (vArguments.length >2){
							solutionToGo = vArguments[2];
						}
						//pageToRedirect = serverUrl + addressPage + 'GecoSendMailOK.html'
					}
				} else {
					resultString = 'Geco - Errore nel reset della password';
					application.output(globals.messageLog + 'queue.metodo() ERRORE ' + resultString,LOGGINGLEVEL.INFO);
					solutionToGo = '';
					//pageToRedirect = serverUrl + addressPage + 'GecoSendMailKO.html'
				}
			}

		} else {
			resultString = 'Geco - Errore nel reset della passwrod: Token gia utilizzato o non trovato';
			application.output(globals.messageLog + 'queue.metodo() ERRORE ' + resultString,LOGGINGLEVEL.INFO);
			solutionToGo = '';
			//pageToRedirect = serverUrl + addressPage + 'GecoSendMailTokenKO.html'
			//application.output(pageToRedirect);
		}
	}
	forms.resetPassword.toClose = 1;
}

/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
 * @param {String} bcc
 * @return {Object}
 * @properties={typeid:24,uuid:"89861788-8DE3-4AF8-BCCB-CA897A9A7119"}
 */
function sendMailBcc(recipient, subject, message, bcc) {
	var result = {
		recipient: recipient,
		subject: subject,
		message: message,
		bcc: bcc,
		success: true
	};

	var properties = [];
	properties.push('mail.smtp.host=smtp.gmail.com');
	properties.push('mail.smtp.auth=true');
	properties.push('mail.smtp.username=' + mailOptions.username);
	properties.push('mail.smtp.password=' + mailOptions.password);
	properties.push('mail.smtp.port=587');
	properties.push('mail.smtp.starttls.enable=true');

	var success = plugins.mail.sendMail(recipient, '[GECO] <noreply@spindox.it>', subject, message, null, bcc, null, properties);
	if (!success) {
		application.output(plugins.mail.getLastSendMailExceptionMsg());
		result.success = false;
		result.message = 'Errore invio mail';
	}
	return result;
}

// * @param {plugins.mail.Attachment} attach
/**
 * @param {String} recipient
 * @param {String} subject
 * @param {String} message
  * @param {String} ccRecipient
 * @param {String} filePath
 * @param {String} filename
 * @return {Object}
 * @properties={typeid:24,uuid:"1A7E9B1E-79D5-4B44-8A1E-97A3A5150433"}
 */
function sendMailAttach(recipient, subject, message, ccRecipient, filePath, filename) {
	application.output(filename)
	var attachment = null
	if (filename != null)
		attachment = plugins.mail.createBinaryAttachment(filename, plugins.file.readFile(filePath),  'application/vnd.ms-excel');	
	
	application.output('attach '+attachment);
	var result = {
		recipient: recipient,
		subject: subject,
		message: message,
		success: true
	};

	var properties = [];
	properties.push('mail.smtp.host=smtp.gmail.com');
	properties.push('mail.smtp.auth=true');
	properties.push('mail.smtp.username=' + mailOptions.username);
	properties.push('mail.smtp.password=' + mailOptions.password);
	properties.push('mail.smtp.port=587');
	properties.push('mail.smtp.starttls.enable=true');
application.output('prima')
	var success = plugins.mail.sendMail(recipient, '[GECO] <noreply@spindox.it>', subject, message, ccRecipient, null, attachment, properties);
	application.output('sendMailAttach ' + success)
	if (!success) {
		application.output(plugins.mail.getLastSendMailExceptionMsg());
		result.success = false;
		result.message = 'Errore invio mail';
	}
	plugins.file.deleteFile(filePath);
	return result;
}