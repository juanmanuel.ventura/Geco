/**
 * @type {String}
 * @properties={typeid:35,uuid:"FF172E87-1BD4-454F-8B82-203F6711CDDD"}
 */
var password = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"65B6A7B8-2BC0-46D2-BBB5-515129FC3FA2"}
 */
var userName = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E418E82A-7247-4AF9-B5F3-539601525AB5"}
 */
var resultMessage = null;

/**
 * @properties={typeid:35,uuid:"0A1A52A1-7B06-43AA-9F5D-6C2F9B191D42",variableType:-4}
 */
var client = null;

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"B6351611-6E57-4013-A6EB-DF6D8B55327A"}
 */
function login(event) {
	application.output('START login.login() ' + userName, LOGGINGLEVEL.INFO);
//	switch (userName) {
//	case 'df':
//		userName = 'daniela.frau';
//		password = "spindox";
//		break;
//	case 'jm':
//		userName = 'juanmanuel.ventura';
//		password = 'spindox';
//		break;
//	case 'js':
//		userName = 'jonathan.staffa';
//		password = 'spindox';
//		break;
//	case 'js1':
//		userName = 'jonathan.staffa.1';
//		password = 'spindox';
//		break;
//	case 'dr':
//		userName = 'david.romano';
//		password = 'spindox';
//		break;
//	case 'mt':
//	userName = 'massimo.tornato';
//	password = 'spindox';
//	break;
//	case 'lp':
//		userName = 'laura.pirovano';
//		password = 'spindox';
//		break;
//	case 'fb':
//		userName = 'fabrizio.bindi';
//		password = 'spindox';
//		break;
//	case 'mm':
//		userName = 'mauro.marengo';
//		password = 'spindox';
//		break;
//	}
	var result = security.authenticate("geco_authenticator", "loginUser", [userName, password]);
	if (result) {
		if (result['logged']) {
			application.output('STOP login.login() OK ' + userName, LOGGINGLEVEL.INFO);
			globals.isFromGeco = true;
			return true;
		} else {
			resultMessage = result['error'];
			application.output('STOP login.login() KO ' + userName + ' ' + resultMessage, LOGGINGLEVEL.INFO);
			return false;
		}
	} else {
		resultMessage = 'ERRORE APPLICATIVO'
		application.output('STOP login.login() KO APPLICATION ERROR' + +userName, LOGGINGLEVEL.ERROR);
		return false;
	}

}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {String} user the user
 * @private\
 * @properties={typeid:24,uuid:"70897B1C-0D84-47FB-A2E8-A6ACEA5ABCB0"}
 */
function logMeIn(event, user) {
	userName = user;
	login(event);
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @private
 *
 * @properties={typeid:24,uuid:"697A52DD-39F4-49E6-A22D-3647749F8373"}
 */
function onLoad(event) {
	if (application.getApplicationType() == APPLICATION_TYPES.SMART_CLIENT) {
		elements.image.visible = true;
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @private
 *
 * @properties={typeid:24,uuid:"F0BD8800-33D4-4CE5-84B6-7A5DA3D6C38C"}
 */
function callLostPassword2(event) {
	application.output('START login.callLostPassword2() ', LOGGINGLEVEL.INFO);
	if (userName != null) {
		application.output('login.callLostPassword2() ' + userName + ' solution caller ' + application.getSolutionName(), LOGGINGLEVEL.INFO);
		client = plugins.headlessclient.createClient("geco_queue", null, null, null);
		if (client != null && client.isValid()) {
			application.output('START login.callLostPassword2() Cient OK ' + client.getClientID(), LOGGINGLEVEL.DEBUG);
			client.queueMethod(null, "sendMailResetPassword", [userName, application.getServerURL(), application.getSolutionName()], enqueueMailLog);
			//resultMessage = 'Inviata mail';
		} else {
			application.output('START login.callLostPassword2() Client is not valid ', LOGGINGLEVEL.DEBUG);
		}
	} else {
		resultMessage = 'Lo username non può essere vuoto';
		application.output('START login.callLostPassword2() ERROR: ' + resultMessage, LOGGINGLEVEL.DEBUG);
	}
	application.output('STOP login.callLostPassword2 ', LOGGINGLEVEL.INFO);
}

/**
 * @param {JSEvent} headlessCallback
 * @properties={typeid:24,uuid:"18EBF9C2-BF38-402E-B6E2-6B92057234EC"}
 */
function enqueueMailLog(headlessCallback) {
	application.output('START login.enqueueMailLog() ', LOGGINGLEVEL.INFO);
	if (headlessCallback.getType() == JSClient.CALLBACK_EVENT) {
		var outMessege = '-------------SENDING MAIL----------------------' + '\ntimestamp: ' + headlessCallback.getTimestamp() + '\nrecipient: ' + headlessCallback.data.recipient + '\nsubject: ' + headlessCallback.data.subject + '\nmessage: ' + headlessCallback.data.message + '\nsuccess: ' + headlessCallback.data.success.toString().toUpperCase() + '\n-----------------------------------------------\n'
		application.output('login.callLostPassword2 result: ' + outMessege, LOGGINGLEVEL.INFO);
		if (headlessCallback.data.success == false)
			resultMessage = headlessCallback.data.message;
		else
			resultMessage = 'Inviata mail per reset password';
	} else if (headlessCallback.getType() == JSClient.CALLBACK_EXCEPTION_EVENT) {
		application.output('login.callLostPassword2 exception callback, name: ' + headlessCallback.data + outMessege, LOGGINGLEVEL.ERROR);
		resultMessage = 'Errore invio mail'
	}
	application.output('login.callLostPassword2 message: ' + resultMessage, LOGGINGLEVEL.INFO);
	if (client != null && client.isValid()) {
		application.output('login.callLostPassword2 close client ' + client.getClientID(), LOGGINGLEVEL.INFO);
		client.shutdown();
	}
	application.output('STOP login.enqueueMailLog() ', LOGGINGLEVEL.INFO);
}

/**
 * @param {String} formName
 * @properties={typeid:24,uuid:"689AB6ED-C92A-4B75-BB14-53484FEED023"}
 */
function prepareWindow(formName) {
	var form = forms[formName];

	// hide menu and tool bars
	plugins.window.getMenuBar().setVisible(false);
	plugins.window.setToolBarAreaVisible(false);
	plugins.window.setStatusBarVisible(false);

	// set size and center the window
	var window = application.getWindow();
	window.show(form);
	if (application.getOSName() != 'Mac OS X') plugins.window.maximize();
}
